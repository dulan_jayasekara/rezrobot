package dmc_Setup_DataObjects;

public class dmc_Setup_DataObjects_InventoryObject {
	
	String  totalunits;
	String  cutoff;
	String  blockinventory;
	
	
	public String getTotalunits() {
		return totalunits;
	}
	public void setTotalunits(String totalunits) {
		this.totalunits = totalunits;
	}
	public String getCutoff() {
		return cutoff;
	}
	public void setCutoff(String cutoff) {
		this.cutoff = cutoff;
	}
	public String getBlockinventory() {
		return blockinventory;
	}
	public void setBlockinventory(String blockinventory) {
		this.blockinventory = blockinventory;
	}
	
	

}
