package com.rezrobot.pages.web.fixed_packages;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.utill.DriverFactory;

public class Web_FxP_ConfimationPage extends PageObject {
	
	public HashMap<String, String> readRateLabels() {
		
HashMap<String, String> labels = new HashMap<String, String>();
		
		try {
			
			labels.put("Confirmation.SubTotalLabel", getLabelName("lbl_subtotalLabel_xpath"));
			labels.put("Confirmation.taxLabel", getLabelName("lbl_taxLabel_xpath"));
			labels.put("Confirmation.totalpackagecostLabel", getLabelName("lbl_totalcostLabel_xpath"));
			labels.put("Confirmation.finalbookingvalueLabel", getLabelName("lbl_finalbookingvalueLabel_xpath"));
			
	
			System.out.println();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		System.out.println(labels);
		return labels;
		
	}
	
	public void cxlPolicyRead() throws Exception {
		
		System.out.println(getElement("txt_cxl_id").getText());
		
	}
	
	public HashMap<String, String> confirmationStatusRead() throws Exception {
		
		HashMap<String, String> confstatusMap=new HashMap<>();
		confstatusMap.put("BookingReference", getElement("txt_bookingreference_id").getText());
		confstatusMap.put("ResvationNo", getElement("txt_packageConfirmation_id").getText());
		confstatusMap.put("Customeremail", getElement("txt_refMail_class").getText());
		confstatusMap.put("portalWeb", getElement("lnk_portalWeb_xpath").getText());
		confstatusMap.put("portalTel", getElement("txt_PortalTelNo_xpath").getText().replace("Tel : ", ""));
		confstatusMap.put("portalfax", getElement("txt_PortalfaxNo_xpath").getText().replace("Fax : ", ""));

		

		
System.out.println(confstatusMap.get("portalTel"));
return confstatusMap;
		
		
		
	}
	
	public void rateBreakDownRead() throws Exception {
		
		HashMap<String, String> RateMap=new HashMap<>();
		RateMap.put("SellingCurrency", getElement("txt_CurrencyUsed_id").getText());
		RateMap.put("SubTotal", getElement("txt_packagesubTotal_id").getText());

		RateMap.put("TaxAmount", getElement("txt_taxcharges_id").getText());

		RateMap.put("TotalPackageCost", getElement("txt_totalpackagecost_id").getText());

		RateMap.put("BookingValue", getElement("txt_finalbookingvalue_id").getText());

		

		
	}
	
	
	
	/*public static void main(String[] args) throws Exception {
		
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get("http://dev3.rezg.net//omanairholidaysReservations/Confirmation.do?method=confirm&transectionId=omanairholidays-800b1eafd3d749fca2d12da8a8e09ea2");
		
		Web_FxP_ConfimationPage page=new Web_FxP_ConfimationPage();
		page.readRateLabels();
		page.cxlPolicyRead();
		page.confirmationStatusRead();
		page.rateBreakDownRead();
		
		
	}*/

}
