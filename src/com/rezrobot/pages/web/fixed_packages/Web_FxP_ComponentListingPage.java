package com.rezrobot.pages.web.fixed_packages;

import java.util.HashMap;
import java.util.Map;

import com.rezrobot.core.PageObject;
import com.rezrobot.pages.web.common.Web_Com_PaymentsPage;
import com.rezrobot.utill.DriverFactory;

public class Web_FxP_ComponentListingPage extends PageObject {


	/**
	 * @param args
	 * @throws Exception 
	 */

	/*
	 * Flight Inclusive-Ride tracer ID---done
	 * air display---- done
	 * flight details
	 * change flight read
	 * change flight rate change read
	 * 
	 * Hotel-image details read
	 * Hotel dynamic package read
	 * Hotel  predef read
	 * chang hotel
	 * change Room
	 * 
	 * Activity read
	 * 
	 *  rate read
	 *  rate label read
	 *  rate bucket read
	 *  2 rates comparison
	 *  
	 *  search again read
	 *  return button functionslity
	 *  book now button functionality
	 *  

	 * 
	 * */

	public String getRideTraceID() throws Exception {

		return getElement("div_flightresultsBlock_tagname").getAttribute("value");

	}
	
	public void clickonFlightDetails() throws Exception {

		getElement("btn_flightDetails_id").click();
		addobjects();


	}
	

	public Web_Com_PaymentsPage bookThepackage() throws Exception {
		
		

		getElement("btn_booknow_id").click();
		Web_Com_PaymentsPage PaymentsPage=new Web_Com_PaymentsPage();
		
		return PaymentsPage;
		
		

	}

	public void flightDetailReader() throws Exception {
		System.out.println(objectMap);

		System.out.println(getelementAvailability("btn_flightDetails_id"));
		getElement("btn_flightDetails_id").click();
		Thread.sleep(4000);
		addobjects();
		System.out.println(objectMap);

		for (int i = 0; i < 3; i++) {




			if (getelementAvailability("txt_outboundair"+(i+1)+"_id")) {




				System.out.println(getElement("txt_outboundair"+(i+1)+"_id").getText());
				System.out.println(	getElement("txt_outbounddepdate"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_outbounddeploc"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_outboundarrdate"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_outboundarrloc"+(i+1)+"_id").getText());

			}


			if (getelementAvailability("txt_inboundair"+(i+1)+"_id")) {

				System.out.println(getElement("txt_inboundair"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_inbounddepdate"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_inbounddeploc"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_inboundarrdate"+(i+1)+"_id").getText());
				System.out.println(getElement("txt_inboundarrloc"+(i+1)+"_id").getText());

			}



		}





	}

	public void readAirDisplay() {


	}
	public void readPackageRaterateBasket() throws Exception {
		System.out.println(getElement("div_ratedisplaybucket_id").getText());		


	}
	
	public void HotelReader_Dynamic() {
		
	}
	
	public void ActivityReader() {
		
	}


	public void readPackageRatelower() throws Exception {
		System.out.println(getElement("div_packageRate_id").getText());		


	}

	public void returntoExistingPackages() {

		executejavascript("result_wjt_WJ_1.returnToavailablePkgFromMunue()");

	}

	public Map<String, String> airDisplayRead() throws Exception {

		Map<String, String> airDisplayMap=new HashMap();

		airDisplayMap.put("OutBound Flight Logo", getElement("img_departureFlightLogo_xpath").getAttribute("src"));
		airDisplayMap.put("OutBound Flight Number", getElement("txt_outboundflightno_id").getText());
		airDisplayMap.put("OutBound Flight Departure Date", getElement("txt_outbounddepartureDate_id").getText());
		airDisplayMap.put("OutBound Flight Departure Time", getElement("txt_outbounddepartureTime_id").getText());
		airDisplayMap.put("OutBound Flight Departure Airport Code", getElement("txt_outbounddepartureairportcode_id").getText());
		airDisplayMap.put("OutBound Flight Departure City", getElement("txt_outbounddepartureairport_id").getText());
		airDisplayMap.put("OutBound Flight Arrival Time", getElement("txt_outboundarrivalTime_id").getText());
		airDisplayMap.put("OutBound Flight Arrival Airport Code", getElement("txt_outboundarrivalairportCode_id").getText());
		airDisplayMap.put("OutBound Flight Arrival City", getElement("txt_outboundarrivalairport_id").getText());
		airDisplayMap.put("OutBound Flight Duration", getElement("txt_outboundduration_id").getText());
		airDisplayMap.put("OutBound Flight Numberof Stops", getElement("txt_outboundnumberofStops_id").getText());

		airDisplayMap.put("InBound Flight Logo", getElement("img_arrivalFlightLogo_xpath").getAttribute("src"));
		airDisplayMap.put("InBound Flight Number", getElement("txt_inboundflightno_id").getText());
		airDisplayMap.put("InBound Flight Arrival Date", getElement("txt_inboundarrivalDate_id").getText());
		airDisplayMap.put("InBound Flight Arrival Time", getElement("txt_inboundarrivaltime_id").getText());
		airDisplayMap.put("InBound Flight Arrival Airport Code", getElement("txt_inboundarrivalairportCode_id").getText());
		airDisplayMap.put("InBound Flight Arrival City", getElement("txt_inboundarrivalairport_id").getText());
		airDisplayMap.put("InBound Flight Departure Time", getElement("txt_inbounddepartureTime_id").getText());
		airDisplayMap.put("InBound Flight Departure Air Port Code", getElement("txt_inbounddepartureairportCode_id").getText());
		airDisplayMap.put("InBound Flight Departure City", getElement("txt_inbounddepartureairport_id").getText());
		airDisplayMap.put("InBound Flight Duration", getElement("txt_inboundduration_id").getText());
		airDisplayMap.put("InBound Flight Numberof Stops", getElement("txt_inboundnumberofStops_id").getText());


		return airDisplayMap;







	}


/*	public static void main(String[] args) throws Exception {


		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get("http://dev3.rezg.net/omanairholidaysReservations/Search.do?method=loadWithResults&transectionId=omanairholidays-800b1eafd3d749fca2d12da8a8e09ea2&packageId=71&packDate=08-01-2016&numberOfAdults=1&numberOfChilds=0&numberOfInfants=0&numberOfRooms=1&occupancy=1|0|-|%25|%25|0|%25|-@&components=FHA&childAllowed=true&startDate=2016-06-30&endDate=2017-11-01&costedDates=&isDMCPackage=false&flightInclude=Y&cabinClass=Economy&cacheKey=Dubai|DXB|4|Dubai%20International%20Airport||53||United%20Arab%20Emirates@0@Muscat|MCT|872|Muscat%20International%20Airport||231||Oman@@9~OM@-@@0@DC@@@omanairholidays@");

		Web_FxP_ComponentListingPage page=new Web_FxP_ComponentListingPage();
		//page.clickonFlightDetails();
		//page.flightDetailReader();
		//page.returntoExistingPackages();
		page.getRideTraceID();
		System.out.println(page.airDisplayRead().get("InBound Flight Arrival Time"));
		//page.bookThepackage();
		page.readPackageRatelower();
		page.readPackageRaterateBasket();
		//page.bookThepackage();





	}*/



}
