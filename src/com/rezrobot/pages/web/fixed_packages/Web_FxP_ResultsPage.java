package com.rezrobot.pages.web.fixed_packages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;

import sun.management.counter.Units;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.DMCSearchObject;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_SearchObject;
import com.rezrobot.pages.web.dmc.Web_DMC_ComponentListingPage;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Image;
import com.rezrobot.pojo.Link;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;


public class Web_FxP_ResultsPage extends PageObject {

	/**
	 * @param args
	 * @throws Exception
	 * @throws NumberFormatException
	 */

	String packageName = "OAH Reg10";
	String packageID = "";
	int PageCount = 0;
	String pageNumberofThePackage;
	FxP_SearchObject search;
	UIElement resultblock = new UIElement();
	boolean moreinfoLoaded = true;

	public FxP_SearchObject getSearch() {
		return search;
	}

	public void setSearch(FxP_SearchObject search) {
		this.search = search;
		packageName = search.getPackageID();
	}

	public boolean getLogoAvailability() throws Exception {
		try {
			return ((Image) getElement("img_company_logo")).isImageLoaded();
		} catch (Exception e) {
			
			System.out.println(e);
			return false;
		}
	}

	public String getLogoPath() throws Exception {
		return ((Image) getElement("img_company_logo")).getImagePath();
	}

	public boolean getCustomerLoginLinkAvailability() throws Exception {
		return getElementVisibility("link_customer_login");
	}

	public String getCustomerLoginLinkText() throws Exception {
		return ((Link) getElement("link_customer_login")).getText();
	}

	public String getCustomerLoginLinkPath() throws Exception {
		return ((Link) getElement("link_customer_login")).getUrl();
	}

	public boolean doesResultsAvail() {

		return getElementVisibility("div_pagination_class");

	}

	public boolean isSearchAgainBECLoaded() {

		return getElementVisibility("frame_searchAgainbec_id");
	}

	public int getnumberofLoadedResults() throws NumberFormatException, Exception {
		// switchToDefaultFrame();

		addobjects();
		return Integer.parseInt(getElement("input_numberofPackages_xpath").getText().split(" ")[0]);

	}

	public void traverseResults(DMCSearchObject search) throws NumberFormatException, Exception {

		// Map<String,UIElement> objectnewMap = new HashMap<String, UIElement>();
		boolean packageexist = false;
		try {
			switchToDefaultFrame();
		} catch (Exception e) {
			// TODO: handle exception
		}
		int count = (getnumberofLoadedResults() / 10) + 1;
		PageCount = count;

		int t = 1;
		for (int i = 1; i <= count; i++) {

			for (int j = 0; j < 10; j++) {
				try {

					// generating the Object
					UIElement Title = new UIElement();
					Thread.sleep(4000);
					Title.setKey("txt_Title" + t + "_" + i + "_xpath");

					String x = ".//*[@id='pkg_dv_WJ_1']/div[A]/div/div[1]/div[1]/div[2]/div[1]/h1";
					String y = ".//*[@id='pkg_dv_WJ_1']/div[D]/div/div[1]/div[1]/div[2]/div[2]/a";

					int b = (2 * j) + 2;
					Title.setRef(x.replace("A", String.valueOf(b)));
					Title.setRefType("3");

					objectMap.put(Title.getKey(), Title);

					System.out.println(t + "********" + Title.getKey() + "************" + getElement(Title.getKey()).getText());

					if (search.getPackageName().equalsIgnoreCase(getElement(Title.getKey()).getText())) {
						packageexist = true;

						UIElement moreinfo = new UIElement();
						moreinfo.setElementProperties("lnk_moreinfoSelected_xpath", "3", y.replace("D", Integer.toString(b)));
						// moreinfo.setRef(y.replace("D", "b"));
						objectMap.put("Link_moreinfo", moreinfo);
						getElementClick("Link_moreinfo");

						UIElement Selectbutton = new UIElement();
						String Index = moreinfo.getAttribute("href").replace("javascript:result_wjt_WJ_1.moreInfo(", "").replace(",true)", "");

						String z = ".//*[@id='fp-select-btn-G']";

						Selectbutton.setElementProperties("button_selectbutton_xpath", "3", z.replace("G", Index));
						Selectbutton.setRemarks(Index);

						objectMap.put("button_select", Selectbutton);

					}

					// getinnerUI("div_resultblock"+(1+j)+"_class", "div_packageTitle_class");
				} catch (Exception e) {
					// TODO: handle exception
				}
				t++;
			}

			executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':" + (i + 1) + "},'','WJ_3'))");

		}

		// objectMap.putAll(objectnewMap);

		/*
		 * System.out.println(); executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':1},'','WJ_3'))");
		 * 
		 * for (int i = 0; i < 10; i++) { System.out.println(i);
		 * 
		 * System.out.println(getElement("txt_Title"+(i+1)+"_xpath").getText());
		 * 
		 * 
		 * 
		 * }
		 */

		executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':1},'','WJ_3'))");
	}

	public boolean findResultBlock(FxP_SearchObject searchobj) {

		setSearch(searchobj);

		boolean packageexist = false;

		int t = 1;
		for (int i = 1; i <= PageCount; i++) {
			for (int j = 0; j < 10; j++) {

				try {

					String elementkey = "txt_Title" + t + "_" + i + "_xpath";
					System.out.println(getElement(elementkey).getText());
					if (getElement(elementkey).getText().trim().contains(packageName)) {
						packageexist = true;
						pageNumberofThePackage = String.valueOf(i);

						System.out.println("elementkey" + elementkey);
						System.out.println(getElement(elementkey).getRef());

						resultblock.setRef(getElement(elementkey).getRef().replace("/div/div[1]/div[1]/div[2]/div[1]/h1", ""));
						resultblock.setRefType("3");
						resultblock.setKey("div_ResultBlockSelected");
						objectMap.put(resultblock.getKey(), resultblock);

						System.out.println(resultblock.getAttribute("class"));
						packageID = resultblock.getAttribute("class").replace("result-block result-block-", "");
						System.out.println(packageID);

						/* THumbnailImage element */

						UIElement thumbnailSelected = new UIElement();
						String thumbnailRef = getElement(elementkey).getRef().replace("div[2]/div[1]/h1", "div[1]/div/div/img");
						thumbnailSelected.setElementProperties("img_thumbnail_xpath", "3", thumbnailRef);
						addTomap(thumbnailSelected);

						UIElement validityPeriodSelected = new UIElement();
						String validityPeriodSelectedRef = getElement(elementkey).getRef().replace("h1", "p");
						validityPeriodSelected.setElementProperties("txt_validityPeriodSelected_xpath", "3", validityPeriodSelectedRef);
						addTomap(validityPeriodSelected);

						System.out.println(getElement("txt_validityPeriodSelected_xpath").getText());

						UIElement shortDescriptionSelected = new UIElement();
						String shortDescriptionSelectedRef = getElement(elementkey).getRef().replace("h1", "span/div/div");
						shortDescriptionSelected.setElementProperties("txt_shortDescriptionSelected_xpath", "3", shortDescriptionSelectedRef);
						addTomap(shortDescriptionSelected);

						UIElement moreinfoLinkselected = new UIElement();
						String moreinfoLinkselectedref = getElement(elementkey).getRef().replace("div[1]/h1", "div[2]/a");
						moreinfoLinkselected.setElementProperties("lnk_moreinfoselected", "3", moreinfoLinkselectedref);

						addTomap(moreinfoLinkselected);
						// getinnerWebElement(resultblock.getWebElement(), innerelement)

						// getelementAvailability("")

					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				t++;
			}

		}
		return packageexist;

	}

	public boolean DMCfindResultBlock(DMCSearchObject search) {

		boolean packageexist = false;

		int t = 1;
		for (int i = 1; i <= PageCount; i++) {
			for (int j = 0; j < 10; j++) {

				try {

					String elementkey = "txt_Title" + t + "_" + i + "_xpath";
					System.out.println(getElement(elementkey).getText());
					if (getElement(elementkey).getText().trim().contains(search.getPackageName())) {
						packageexist = true;
						pageNumberofThePackage = String.valueOf(i);

						System.out.println("elementkey" + elementkey);
						System.out.println(getElement(elementkey).getRef());

						resultblock.setRef(getElement(elementkey).getRef().replace("/div/div[1]/div[1]/div[2]/div[1]/h1", ""));
						resultblock.setRefType("3");
						resultblock.setKey("div_ResultBlockSelected");
						objectMap.put(resultblock.getKey(), resultblock);

						System.out.println(resultblock.getAttribute("class"));
						packageID = resultblock.getAttribute("class").replace("result-block result-block-", "");
						System.out.println(packageID);

						/* THumbnailImage element */

						UIElement thumbnailSelected = new UIElement();
						String thumbnailRef = getElement(elementkey).getRef().replace("div[2]/div[1]/h1", "div[1]/div/div/img");
						thumbnailSelected.setElementProperties("img_thumbnail_xpath", "3", thumbnailRef);
						addTomap(thumbnailSelected);

						UIElement validityPeriodSelected = new UIElement();
						String validityPeriodSelectedRef = getElement(elementkey).getRef().replace("h1", "p");
						validityPeriodSelected.setElementProperties("txt_validityPeriodSelected_xpath", "3", validityPeriodSelectedRef);
						addTomap(validityPeriodSelected);

						System.out.println(getElement("txt_validityPeriodSelected_xpath").getText());

						UIElement shortDescriptionSelected = new UIElement();
						String shortDescriptionSelectedRef = getElement(elementkey).getRef().replace("h1", "span/div/div");
						shortDescriptionSelected.setElementProperties("txt_shortDescriptionSelected_xpath", "3", shortDescriptionSelectedRef);
						addTomap(shortDescriptionSelected);

						UIElement moreinfoLinkselected = new UIElement();
						String moreinfoLinkselectedref = getElement(elementkey).getRef().replace("div[1]/h1", "div[2]/a");
						moreinfoLinkselected.setElementProperties("lnk_moreinfoselected", "3", moreinfoLinkselectedref);

						addTomap(moreinfoLinkselected);
						// getinnerWebElement(resultblock.getWebElement(), innerelement)

						// getelementAvailability("")

					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				t++;
			}

		}
		return packageexist;

	}

	public String getThumbnailImage() throws Exception {

		return getElement("img_thumbnail_xpath").getAttribute("src");

	}

	public String getvalidityPeriod() throws Exception {

		return getElement("txt_validityPeriodSelected_xpath").getText();

	}

	public String getdefaultDisplayedShortDescription() throws Exception {
		System.out.println("new length");
		System.out.println(("Oman has just what you need for a quick escape direct flights; the best live theater scene on the planet; an inviting pub on every corner and the simple pleasure of conversing with Europeans with").length());

		return getElement("txt_shortDescriptionSelected_xpath").getText();

	}

	public boolean IsReadMoreLinkAvailable() {

		UIElement ReadmoreSelected = new UIElement();
		ReadmoreSelected.setElementProperties("lnk_ReadmoreSelected_id", "1", "displayText_" + packageID);
		addTomap(ReadmoreSelected);

		return getelementAvailability("lnk_ReadmoreSelected_id");

	}

	public boolean isPackageresultsPageAvailable() throws InterruptedException {

		switchToDefaultFrame();
		Thread.sleep(5000);
		return super.isPageAvailable("firstpackagename_xpath");

	}

	public boolean isPackageResultsAvailable() {

		try {
			switchToDefaultFrame();
			return getElementVisibility("firstpackagename_xpath", 100);
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public void readmoreClick() throws Exception {

		getElementClick("lnk_ReadmoreSelected_id");

	}

	public String gethiddenText() throws Exception {

		UIElement hiddentextSelected = new UIElement();
		hiddentextSelected.setElementProperties("txt_hiddentextSelected_id", "1", "toggleText_" + packageID);
		addTomap(hiddentextSelected);

		return getElement("txt_hiddentextSelected_id").getText();

	}

	public boolean MoreInfolinkAvailability() {

		UIElement moreinfoLink = new UIElement();
		moreinfoLink.setKey("lnk_MoreinfoSelected");

		String ref = resultblock.getRef().concat("/div/div[1]/div[1]/div[2]/div[2]/a");

		moreinfoLink.setRef(ref);

		moreinfoLink.setRefType("3");
		addTomap(moreinfoLink);

		moreinfoLoaded = getelementAvailability(moreinfoLink.getKey());

		return moreinfoLoaded;
		// moreinfoLink-------javascript:result_wjt_WJ_1.moreInfo(41,false)

	}

	/*
	 * more info tests link availability-done link click and load-done pop up close package details-Content-Style images-Style and load itinerary-content images-style Terms and conditions-content and
	 * style
	 */

	public boolean moreinfoLoad() throws Exception {
		boolean infoload = false;

		// executejavascript("result_wjt_WJ_1.moreInfo("+packageID+",false)");

		getElementClick("lnk_moreinfoselected");

		// getElementClick("lnk_MoreinfoSelected");
		UIElement packagenamemoreinfo = new UIElement();
		// packagenamemoreinfo.setKey("txt_moreinfoPackageName");
		String packagenamemoreinforef = ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/header/span";
		packagenamemoreinfo.setElementProperties("txt_moreinfoPackageName", "3", packagenamemoreinforef);

		addTomap(packagenamemoreinfo);

		/*
		 * Thread.sleep(8000); System.out.println(packageName); System.out.println(getElement("txt_moreinfoPackageName").getText());
		 */
		getElement("txt_moreinfoPackageName").getText();
		if (getelementAvailability("txt_moreinfoPackageName")) {
			infoload = true;
		}

		return infoload;
	}

	public HashMap<String, String> packageDetailsLoad() throws Exception {

		HashMap<String, String> packageinfoContent = new HashMap<>();

		if (moreinfoLoaded) {

			packageDetailsElementsRead("txt_DestinationDetailsHeader", "1");
			packageDetailsElementsRead("txt_DestinationDetailscontent", "2");
			packageDetailsElementsRead("txt_PackageInclusionHeader", "3");
			packageDetailsElementsRead("txt_PackageInclusionContent", "4");
			packageDetailsElementsRead("txt_PackageExclusionHeader", "5");
			packageDetailsElementsRead("txt_PackageExclusionContent", "6");
			packageDetailsElementsRead("txt_SpecialNotesHeader", "7");
			packageDetailsElementsRead("txt_SpecialNotesContent", "8");
			packageDetailsElementsRead("txt_ContactDetailsonArrivalsHeader", "9");
			packageDetailsElementsRead("txt_ContactDetailsonArrivalsContent", "10");

			System.out.println(getelementAvailability("txt_DestinationDetailscontent"));

			System.out.println(getElement("txt_DestinationDetailscontent").getText());

			packageinfoContent.put("DestinationDetailsHeader", getElement("txt_DestinationDetailsHeader").getText());
			packageinfoContent.put("DestinationDetailsContent", getElement("txt_DestinationDetailscontent").getText());

			packageinfoContent.put("PackageInclusionHeader", getElement("txt_PackageInclusionHeader").getText());

			packageinfoContent.put("PackageInclusionContent", getElement("txt_PackageInclusionContent").getText());

			packageinfoContent.put("PackageExclusionHeader", getElement("txt_PackageExclusionHeader").getText());

			packageinfoContent.put("PackageExclusionContent", getElement("txt_PackageExclusionContent").getText());

			packageinfoContent.put("SpecialNotesHeader", getElement("txt_SpecialNotesHeader").getText());

			packageinfoContent.put("SpecialNotesContent", getElement("txt_SpecialNotesContent").getText());

			packageinfoContent.put("ContactDetailsonArrivalsHeader", getElement("txt_ContactDetailsonArrivalsHeader").getText());

			packageinfoContent.put("ContactDetailsonArrivalsContent", getElement("txt_ContactDetailsonArrivalsContent").getText());

			/*
			 * } catch (Exception e) { // TODO: handle exception }
			 */

		}

		return packageinfoContent;

	}

	public HashMap<String, String> itinerarydaywiseRead() throws Exception {
		HashMap<String, String> itineraryReader = new HashMap<>();
		UIElement itineraryLink = new UIElement();
		itineraryLink.setElementProperties("lnk_itinerarySelected_xpath", "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/nav/ul/li[2]/a");
		addTomap(itineraryLink);

		getElementClick("lnk_itinerarySelected_xpath");

		boolean iti = true;
		int lastindex = 0;

		for (int i = 1; iti; i++) {

			UIElement itineraryTitle_i = new UIElement();
			System.out.println(".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[2]/div/div[" + i + "]/div[1]");
			itineraryTitle_i.setElementProperties("txt_itineraryTitle_" + i + "_xpath", "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[2]/div/div[" + i + "]/div[1]");
			System.out.println(itineraryTitle_i.getRef());
			addTomap(itineraryTitle_i);

			UIElement itineraryimage_i = new UIElement();
			itineraryimage_i.setElementProperties("img_itineraryimage_" + i + "_xpath", "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[2]/div/div[" + i + "]/div[2]/img");
			addTomap(itineraryimage_i);

			UIElement itinerarycontent_i = new UIElement();
			itinerarycontent_i.setElementProperties("txt_itinerarycontent_" + i + "_xpath", "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[2]/div/div[" + i + "]/div[3]/p");
			addTomap(itinerarycontent_i);
			if (getelementAvailability("txt_itineraryTitle_" + i + "_xpath")) {
				iti = true;

			} else {
				iti = false;
			}

			lastindex = (i - 1);

		}

		for (int i = 1; i <= lastindex; i++) {
			System.out.println("Title" + getElement("txt_itineraryTitle_" + i + "_xpath").getText());
			itineraryReader.put("title_Day_" + i, getElement("txt_itineraryTitle_" + i + "_xpath").getText());
			itineraryReader.put("image_Day_" + i, getElement("img_itineraryimage_" + i + "_xpath").getAttribute("src"));
			itineraryReader.put("content_Day_" + i, getElement("txt_itinerarycontent_" + i + "_xpath").getText());

			System.out.println("Titleeee" + itineraryReader.get("title_Day_" + i));

		}

		itineraryReader.put("lastIndex", String.valueOf(lastindex));

		return itineraryReader;

	}

	public String termsAndConditionsRead() throws Exception {
		UIElement termsLink = new UIElement();
		termsLink.setElementProperties("lnk_TermsSelected_xpath", "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/nav/ul/li[3]/a");
		addTomap(termsLink);

		getElementClick("lnk_TermsSelected_xpath");

		UIElement termsContent = new UIElement();
		termsContent.setElementProperties("txt_TermsSelected_xpath", "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[3]");
		addTomap(termsContent);

		System.out.println(getElement("txt_TermsSelected_xpath").getText());

		return getElement("txt_TermsSelected_xpath").getText();

	}

	public void availabilityChecker() throws Exception {

		UIElement searchbuttonSelected = new UIElement();
		searchbuttonSelected.setElementProperties("btn_searchSelected_id", "1", "fp-select-btn-" + packageID);
		addTomap(searchbuttonSelected);

		getElement("btn_searchSelected_id").click();

		UIElement packageDate = new UIElement();
		packageDate.setElementProperties("input_packageDate_id", "1", "packagedate" + packageID);
		addTomap(packageDate);
		System.out.println(getPlaceHolder("input_packageDate_id"));

	}

	public ArrayList<String> imagelistMoreinfo() throws Exception {

		ArrayList<String> imageList = new ArrayList<>();

		boolean imagetest = true;
		if (moreinfoLoaded) {
			for (int i = 1; imagetest; i++) {

				UIElement image = new UIElement();
				image.setKey("img_moreinfoimage_" + i);
				image.setRef(".//*[@id='carousel_" + packageID + "']/div/ul/li[" + i + "]/img");
				image.setRefType("3");

				if (getelementPresence(image)) {

					objectMap.put(image.getKey(), image);
					imageList.add(getElement("img_moreinfoimage_" + i).getAttribute("src"));

				} else {
					imagetest = false;
				}

			}

		}

		return imageList;
	}

	public HashMap<String, Boolean> getAvailabilityOfInputFields_SearchagainBEC() throws Exception {
		HashMap<String, Boolean> fileds = new HashMap<String, Boolean>();

		try {

			switchToFrame("frame_searchAgainbec_id");

			fileds.put("cor", getElementVisibility("combobox_ResidenceCountryFP_id"));
			fileds.put("includingflight", getElementVisibility("radiobtn_PackageTypeF_id"));
			fileds.put("excludingflight", getElementVisibility("radiobtn_PackageTypeL_id"));
			fileds.put("origin", getElementVisibility("input_OriginAir_id"));
			fileds.put("destination", getElementVisibility("input_DestinationAir_id"));
			getElement("radiobtn_PackageTypeL_id").click();
			fileds.put("destination_land", getElementVisibility("input_DestinationLand_id"));
			getElement("radiobtn_PackageTypeF_id").click();
			fileds.put("departuremonth", getElementVisibility("combobox_travelon_id"));
			fileds.put("duration", getElementVisibility("combobox_duration_id"));
			getElement("lnk_Searchmoreoptions_xpath").click();
			fileds.put("package_type", getElementVisibility("combobox_packagetype_id"));
			fileds.put("prefferedcurrency_FP", getElementVisibility("combobox_preferredCurrencyFP_id"));

			switchToDefaultFrame();

		} catch (Exception e) {
			// TODO: handle exception
		}

		return fileds;
	}

	public HashMap<String, String> getSearchagainBEC_Content() throws Exception {
		HashMap<String, String> fileds = new HashMap<String, String>();

		try {
			/* switchToDefaultFrame(); */
			switchToFrame("frame_searchAgainbec_id");

			try {
				fileds.put("cor", getElement("combobox_ResidenceCountryFP_id").getText());

			} catch (Exception e) {
				System.out.println(e);
			}

			fileds.put("Including Flight", getElement("label_PackageTypeF_id").getText());
			fileds.put("excludingflight", getElement("radiobtn_PackageTypeL_id").getText());
			fileds.put("origin", getElement("input_OriginAir_id").getAttribute("value"));
			fileds.put("destination", getElement("input_DestinationAir_id").getAttribute("value"));
			getElement("radiobtn_PackageTypeL_id").click();
			fileds.put("destination_land", getElement("input_DestinationLand_id").getText());
			getElement("radiobtn_PackageTypeF_id").click();
			try {
				fileds.put("departuremonth", getElement("combobox_travelon_id").getText());
				fileds.put("duration", getElement("combobox_duration_id").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			getElement("lnk_Searchmoreoptions_xpath").click();
			fileds.put("package_type", getElement("combobox_packagetype_id").getText());
			fileds.put("prefferedcurrency_FP", getElement("combobox_preferredCurrencyFP_id").getText());

			switchToDefaultFrame();

		} catch (Exception e) {
			// TODO: handle exception
		}

		return fileds;
	}

	public UIElement packageDetailsElementsRead(String Key, String index) {

		UIElement packageDetailElement = new UIElement();

		packageDetailElement.setElementProperties(Key, "3", ".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[1]/div/div[1]/ul/li[" + index + "]");
		System.out.println(".//*[@id='fp-more-info-inner-wrapper-" + packageID + "']/div/div[1]/div/div[1]/ul/li[" + index + "]");
		addTomap(packageDetailElement);
		return packageDetailElement;
	}

	public static void main(String[] args) throws Exception {
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get("http://oman-ssl-stg-app5.iisys.org//omanairholidaysReservations/Search.do?method=loadWithResults&transectionId=omanairholidays-a11f158e836c403392149ef971c2e2d5");

		Web_FxP_ResultsPage page = new Web_FxP_ResultsPage();
		// page.getinnerUI("div_resultblock9_class", "div_packageTitle_class");
		// page.traverseResults();

		/*
		 * page.findResultBlock(); page.MoreInfolinkAvailability(); System.out.println(page.moreinfoLoad()); page.imagelistMoreinfo(); page.packageDetailsLoad();
		 */

		/*
		 * for (int i = 0; i < 10; i++) {
		 * 
		 * //System.out.println(x);
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * }
		 */

	}

	public Web_DMC_ComponentListingPage PackageSelect(DMCSearchObject search) throws Exception {

		try {

			getElementClick("button_select");
			UIElement refChanger = new UIElement();
			Thread.sleep(4000);
			
			 refChanger = getElement("ComboBox_No_of_Rooms_id").changeRefAndGetElement("rooms-62".replace("62",packageID));
			((ComboBox) refChanger).selectOptionByValue(search.getNoofRooms());

			for (int i = 1; i <= Integer.parseInt(search.getNoofRooms()); i++) {
		//		UIElement refChanger = new UIElement();
				int childAge = 0;
				String[] adults = search.getAdult().split("/");
				String child[] = search.getChild().split("/");
				String age[] = search.getAges().split("/");
				try {
					refChanger = ((ComboBox) getElement("combobox_adults_id")).changeRefAndGetElement(getElement("combobox_adults_id").getRef().replace("1", String.valueOf(i)).replace("62", packageID));
					((ComboBox) refChanger).selectOptionByText(adults[i - 1]);
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
				}

				try {
					refChanger = getElement("combobox_children_id").changeRefAndGetElement(getElement("combobox_children_id").getRef().replace("1", String.valueOf(i)).replace("62", packageID));
					((ComboBox) refChanger).selectOptionByText(child[i - 1]);
				} catch (Exception e) {
					// TODO: handle exception
				}

				for (int j = 1; j <= Integer.parseInt(child[i - 1]); j++) {
					try {
						refChanger = getElement("combobox_children_age_id").changeRefAndGetElement(getElement("combobox_children_age_id").getRef().replace("R1", "R".concat(String.valueOf(i))).replace("_1", "_".concat(String.valueOf(j))).replace("62", packageID));
						((ComboBox) refChanger).selectOptionByText(age[childAge]);
						childAge++;
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
				}
			}

			getElement("input_Depature_Date_id").setJavaScript("$('#packagedate" + objectMap.get("button_select").getRemarks() + "').val('" + search.getDepartureDate() + "');");
			refChanger = getElement("btn_DMC_Detailsearch_xpath").changeRefAndGetElement(".//*[@id='fp-search-block-id-162']/div[3]/a".replace("162",packageID));
			refChanger.click();
		//	getElementClick("btn_DMC_Detailsearch_xpath");

			Thread.sleep(5000);

		} catch (Exception e) {
			System.out.println("Error occured while select the page or setting up the no of rooms" + e);
			
			
		}

		Web_DMC_ComponentListingPage DMC_componentlistingpage = new Web_DMC_ComponentListingPage();

		return DMC_componentlistingpage;

		// TODO Auto-generated method stub

	}

	
	public void traverseResults(){
		
	}
}
