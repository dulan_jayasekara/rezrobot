package com.rezrobot.pages.web.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.Activity_WEB_ActivityInfo;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.utill.DriverFactory;

public class Web_Act_ResultsPage extends PageObject{
	
	private Activity_WEB_Search searchObject;
	private String idOnly, idNo2;
	
	public boolean isActivityResultsPageAvailable(){
		switchToDefaultFrame();
		return super.isPageAvailable("inputfield_destinationCity_id");
	}
	
	public boolean getSearchAgainBEAvailability() {
		
		try {
			switchToDefaultFrame();
			return getElementVisibility("inputfield_destinationCity_id", 200);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
	
	public boolean isActivityResultsAvailable() {
		
		try {
			switchToDefaultFrame();
			return getElementVisibility("div_ResultCheckFirst_id", 100);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
	
	public HashMap<String, Boolean> getAvailabilityFieldsForActivityResultsPage() {

		HashMap<String, Boolean> fields = new HashMap<>();
		
		try {
			fields.put("AddHotels", getElementVisibility("label_AddHotels_xpath"));
			fields.put("AddFlights", getElementVisibility("label_AddFlights_xpath"));
			fields.put("AddActivities", getElementVisibility("label_AddActivities_xpath"));
			fields.put("AddCars", getElementVisibility("label_AddCars_xpath"));
			fields.put("AddTransfers", getElementVisibility("label_AddTransfers_xpath"));
			fields.put("destinationCity", getElementVisibility("inputfield_destinationCity_id"));
			fields.put("DateRange", getElementVisibility("inputfield_DateRange_id"));
			fields.put("AdultChildCount", getElementVisibility("inputfield_AdultChildCount_xpath"));
			fields.put("ShowAdditionFilter", getElementVisibility("link_ShowAdditionFilter_xpath"));
			getElement("link_ShowAdditionFilter_xpath").click();
			fields.put("ProgramCategory", getElementVisibility("combobox_ProgramCategory_id"));
			fields.put("PreferCurrency", getElementVisibility("combobox_PreferCurrency_id"));
			fields.put("DiscountCoupon", getElementVisibility("inputfield_DiscountCoupon_id"));
			fields.put("HideAdditionFilter", getElementVisibility("link_HideAdditionFilter_xpath"));
			getElement("link_HideAdditionFilter_xpath").click();
			fields.put("SearchAgainButton", getElementVisibility("button_SearchAgainButton_xpath"));		
			fields.put("ActivitiesAvailable", getElementVisibility("label_ActivitiesAvailable_xpath"));
			fields.put("PriceLabel", getElementVisibility("label_PriceLabel_xpath"));
			fields.put("SearchActivity", getElementVisibility("inputfield_SearchActivity_id"));
			fields.put("ResetSortButton", getElementVisibility("button_ResetSortButton_xpath"));
			fields.put("PriceRangeLabel", getElementVisibility("label_PriceRangeLabel_xpath"));
			fields.put("PriceRangeText", getElementVisibility("label_PriceRangeText_xpath"));
			fields.put("PriceSlider", getElementVisibility("button_PriceSlider_xpath"));
			fields.put("ProgramAvailability", getElementVisibility("label_ProgramAvailability_xpath"));
			fields.put("ProgramAvailabilityDropD", getElementVisibility("combobox_ProgramAvailabilityDropD_xpath"));
			fields.put("ProgramType", getElementVisibility("label_ProgramType_xpath"));
			fields.put("ResetFiltersButton", getElementVisibility("button_ResetFiltersButton_xpath"));
			
						
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fields;
	}
	
	public HashMap<String, String> getActivityResultsPageLabels() {

		HashMap<String, String> labels = new HashMap<>();
				
		try {
			labels.put("AddHotels", getLabelName("label_AddHotels_xpath"));
			labels.put("AddFlights", getLabelName("label_AddFlights_xpath"));
			labels.put("AddActivities", getLabelName("label_AddActivities_xpath"));
			labels.put("AddCars", getLabelName("label_AddCars_xpath"));
			labels.put("AddTransfers", getLabelName("label_AddTransfers_xpath"));		
			labels.put("ShowAdditionFilter", getLabelName("link_ShowAdditionFilter_xpath"));
			getElement("link_ShowAdditionFilter_xpath").click();
			labels.put("ProgramCategory", getLabelName("label_ProgramCategory_xpath"));
			labels.put("PreferCurrency", getLabelName("label_PreferCurrency_xpath"));
			labels.put("DiscountCoupon", getLabelName("label_DiscountCoupon_xpath"));			
			labels.put("HideAdditionFilter", getLabelName("link_HideAdditionFilter_xpath"));
			getElement("link_HideAdditionFilter_xpath").click();
			labels.put("ActivitiesAvailable", getLabelName("label_ActivitiesAvailable_xpath"));
			labels.put("PriceLabel", getLabelName("label_PriceLabel_xpath"));
			labels.put("PriceRangeLabel", getLabelName("label_PriceRangeLabel_xpath"));
			labels.put("ProgramAvailability", getLabelName("label_ProgramAvailability_xpath"));			
			labels.put("ProgramType", getLabelName("label_ProgramType_xpath"));
						
		} catch (Exception e) {
			e.printStackTrace();
		}


		return labels;
	}
	
	
	public HashMap<String, String> getActivityResultsComboBoxDataList() throws Exception {

		HashMap<String, String> comboFields = new HashMap<>();

		getElement("link_ShowAdditionFilter_xpath").click();
		comboFields.put("ProgramCategoryCheck", getComboBoxDataList("combobox_ProgramCategoryLoadedCheck_xpath"));
		comboFields.put("prefercurencyLoadedCheck", getComboBoxDataList("combobox_PreferCurrencyLoadedCheck_xpath"));
		comboFields.put("ActivityProgramCatList", getElement("combobox_ProgramCategory_id").getText().replaceAll("\n", " "));
		getElement("link_HideAdditionFilter_xpath").click();
		comboFields.put("programTypeDefault", ((ComboBox)getElement("combobox_ProgramAvailabilityDropD_xpath")).getDefaultSelectedOption());
		comboFields.put("programType_Onreq", getComboBoxDataList("combobox_ProgramAvailabilityOnreq_xpath"));
		comboFields.put("programType_Available", getComboBoxDataList("combobox_ProgramAvailabilityAvailable_xpath"));
		
		
		return comboFields;
	}
	
	public ActivityDetails selectActivities(Activity_WEB_Search sObject, ActivityDetails activityDetails){
		
		this.searchObject = sObject;
		
		try {
			
			DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
			String current_Date = dateFormatcurrentDate.format(cal.getTime()); 
			activityDetails.setReservationDateToday(current_Date);
			
			int activityCount = Integer.parseInt(getElement("label_ActivityCount_xpath").getText());			
			int wholepages = (activityCount / 10);
			
			ArrayList<Integer> resultsPage_ascendingOrder = new ArrayList<Integer>();
			ArrayList<Integer> resultsPage_descendingOrder = new ArrayList<Integer>();
						
			//descending order
			
			getElement("text_resultsPage_descendingOrder_xpath").click();
			Thread.sleep(1000);
			if (activityCount >= 10) {
				
				int RESULTSINFIRSTPAGE = 10;
				for (int i = 0; i < RESULTSINFIRSTPAGE; i++) {
					UIElement SubTotalElement = getElement("text_resultsPage_SubTotal_xpath").changeRefAndGetElement(getElement("text_resultsPage_SubTotal_xpath").getRef().replace("X", Integer.toString(i)));
					resultsPage_descendingOrder.add(Integer.parseInt(SubTotalElement.getText()));
				}
				
			} else {
				for (int i = 0; i < activityCount; i++) {
					UIElement SubTotalElement = getElement("text_resultsPage_SubTotal_xpath").changeRefAndGetElement(getElement("text_resultsPage_SubTotal_xpath").getRef().replace("X", Integer.toString(i)));
					resultsPage_descendingOrder.add(Integer.parseInt(SubTotalElement.getText()));
				}
			}
			
			activityDetails.setResultsPage_descendingOrder(resultsPage_descendingOrder);
			Collections.sort(resultsPage_descendingOrder, Collections.reverseOrder());
			activityDetails.setResultsPage_After_descendingOrder(resultsPage_descendingOrder);
			
			//ascending order
			
			getElement("text_resultsPage_ascendingOrder_xpath").click();
			Thread.sleep(1000);
			if (activityCount >= 10) {
				
				int RESULTSINFIRSTPAGE = 10;
				for (int i = 0; i < RESULTSINFIRSTPAGE; i++) {
					UIElement SubTotalElement = getElement("text_resultsPage_SubTotal_xpath").changeRefAndGetElement(getElement("text_resultsPage_SubTotal_xpath").getRef().replace("X", Integer.toString(i)));
					resultsPage_ascendingOrder.add(Integer.parseInt(SubTotalElement.getText()));
				}
				
			} else {
				for (int i = 0; i < activityCount; i++) {
					UIElement SubTotalElement = getElement("text_resultsPage_SubTotal_xpath").changeRefAndGetElement(getElement("text_resultsPage_SubTotal_xpath").getRef().replace("X", Integer.toString(i)));
					resultsPage_ascendingOrder.add(Integer.parseInt(SubTotalElement.getText()));
				}
			}
			
			activityDetails.setResultsPage_ascendingOrder(resultsPage_ascendingOrder);
			Collections.sort(resultsPage_ascendingOrder);
			activityDetails.setResultsPage_After_ascendingOrder(resultsPage_ascendingOrder);	
			
			//prize slider
			
			WebElement sliderElement = getElement("button_SliderButton_xpath").getWebElement();
			
			//middle
			Actions move = new Actions(DriverFactory.getInstance().getDriver());
			Action action = move.dragAndDropBy(sliderElement, 10, 0).build();
			action.perform();
			Thread.sleep(1000);
			
			int middle_priceLabel = Integer.parseInt(getElement("text_prizetextlist_xpath").getText().split(" ")[2]);
			int middle_firstActivityPrice = Integer.parseInt(getElement("text_getfirstActivityPrize_xpath").getText());		
			
			if (middle_priceLabel <= middle_firstActivityPrice) {
				activityDetails.setPriceSlider_middle(true);
			}
			
			//last
			Actions move2 = new Actions(DriverFactory.getInstance().getDriver());
			Action action_2 = move2.dragAndDropBy(sliderElement, 200, 0).build();
			action_2.perform();
			Thread.sleep(1000);
			
			int last_priceLabel = Integer.parseInt(getElement("text_prizetextlist_xpath").getText().split(" ")[2]);
			int last_firstActivityPrice = Integer.parseInt(getElement("text_getfirstActivityPrize_xpath").getText());		
			
			if (last_priceLabel == last_firstActivityPrice) {
				activityDetails.setPriceSlider_last(true);
			}
				
			//resetfilter
			getElement("button_ResetFiltersButton_xpath").click();
			Thread.sleep(1500);
			
			if (((activityCount) % 10) == 0) {
				System.out.println("Page count - " + wholepages);
			} else {
				wholepages++;
				System.out.println("Page count - " + wholepages);
			}
			
			Random ran = new Random();
			int x = ran.nextInt(100) + 5;

			if (x % 2 == 0) {
				activityDetails.setAddAndContinue(true);
			} else {
				activityDetails.setAddAndContinue(false);
			}
			
			final short ACTIVITY_PER_PAGE = 10;
			outerloop: for (int actvityPage = 1; actvityPage <= wholepages; actvityPage++) {
				for (int i = 0; i < ACTIVITY_PER_PAGE; i++) {

					if (((actvityPage - 1) * ACTIVITY_PER_PAGE) + (i) >= activityCount) {

						break outerloop;

					} else {
						
						String excelActivityName = searchObject.getActivityName().replaceAll(" ", "");
								
						UIElement nameElement = getElement("label_ActivityName_xpath").changeRefAndGetElement(getElement("label_ActivityName_xpath").getRef().replace("X", Integer.toString(i)));
						String webActivityName = nameElement.getText().replaceAll(" ", "");
						
						if (excelActivityName.equalsIgnoreCase(webActivityName)) {
							
							UIElement mainElement = getElement("button_SelectActivityDate_xpath").changeRefAndGetElement(getElement("button_SelectActivityDate_xpath").getRef().replace("X", Integer.toString(i)));
							idOnly = mainElement.getAttribute("code");
							activityDetails.setResultsPage_ActivityName(webActivityName);
							
							UIElement ActCurrencyElement = getElement("text_resultsPage_ActCurrency_xpath").changeRefAndGetElement(getElement("text_resultsPage_ActCurrency_xpath").getRef().replace("X", Integer.toString(i)));
							activityDetails.setResultsPage_ActCurrency(ActCurrencyElement.getText());
							UIElement SubTotalElement = getElement("text_resultsPage_SubTotal_xpath").changeRefAndGetElement(getElement("text_resultsPage_SubTotal_xpath").getRef().replace("X", Integer.toString(i)));
							activityDetails.setResultsPage_SubTotal(SubTotalElement.getText());
							
							UIElement selectDateElement = getElement("combobox_DateSelectId_id").changeRefAndGetElement(getElement("combobox_DateSelectId_id").getRef().replace("ID", idOnly));							
							((ComboBox)selectDateElement).selectOptionByValue(searchObject.getActivityDate());
							int daysCount = ((ComboBox)selectDateElement).getAvailableOptions().size();								
							activityDetails.setResultsPage_daysCount(daysCount-1);
							
							UIElement actDescriptionElement = getElement("text_resultsPage_ActivityDescription_xpath").changeRefAndGetElement(getElement("text_resultsPage_ActivityDescription_xpath").getRef().replace("X", Integer.toString(i)));							
							activityDetails.setResultsPage_ActivityDescription(actDescriptionElement.getText());
							
							//moredetails button
							try {
								UIElement moreinfoButtElement = getElement("button_MoreInfoButt_xpath").changeRefAndGetElement(getElement("button_MoreInfoButt_xpath").getRef().replace("X", Integer.toString(i)));
								moreinfoButtElement.click();
								Thread.sleep(2000);
								UIElement moreinforLoadedElement = getElement("div_MoreInfoSectionLoaded_xpath").changeRefAndGetElement(getElement("div_MoreInfoSectionLoaded_xpath").getRef().replace("ID", idOnly));
								moreinforLoadedElement.isElementVisible();
								
								activityDetails.setResultsPage_MoreInfoLoaded(true);
								moreinfoButtElement.click();
															
							} catch (Exception e) {
								e.printStackTrace();								
							}
							
							//Cancellation button							
							try {
								UIElement cancelButtElement = getElement("button_CancellationLoadButt_xpath").changeRefAndGetElement(getElement("button_CancellationLoadButt_xpath").getRef().replace("X", Integer.toString(i)));
								cancelButtElement.click();
								activityDetails.setResultsPage_cancellationLoaded(true);
								UIElement cancelCloseButtElement = getElement("button_CancellationCloseButt_xpath").changeRefAndGetElement(getElement("button_CancellationCloseButt_xpath").getRef().replace("X", Integer.toString(i)));
								cancelCloseButtElement.click();
															
							} catch (Exception e) {
								e.printStackTrace();								
							}
							
							//Select Activity types
							
							ArrayList<String> excelActivityTypes = new ArrayList<String>();

							ArrayList<Activity_WEB_ActivityInfo> activityInfoList = searchObject.getInventoryInfo();

							for (Activity_WEB_ActivityInfo activityInfo : activityInfoList) {

								String excelActString = activityInfo.getActivityType();
								excelActivityTypes.add(excelActString);
							}
							
							ArrayList<String> resultsPage_ActivityTypes = new ArrayList<String>();							
							ArrayList<String> resultsPage_Period = new ArrayList<String>();
							ArrayList<String> resultsPage_RateType = new ArrayList<String>();
							ArrayList<String> resultsPage_DailyRate = new ArrayList<String>();
							
							UIElement viewButtElement = getElement("button_ViewActivityTypes_xpath").changeRefAndGetElement(getElement("button_ViewActivityTypes_xpath").getRef().replace("X", Integer.toString(i)));
							viewButtElement.click();
							
							getElementVisibility("div_ActivityTypesList_classname", 120);
														
							UIElement activityTypesElement = getElement("div_ActivityTypesList_classname");
							int divCnt = activityTypesElement.getWebElements().size();
							System.out.println(activityTypesElement.getWebElements().size());
							
							for (int j = 0; j < excelActivityTypes.size(); j++) {

								String excelActivityTypeName = excelActivityTypes.get(j).replaceAll(" ", "");

								for (int k = 1; k <= activityTypesElement.getWebElements().size(); k++) {

									String webActivityTypeName = null;
									
									try {
										UIElement activityType_NameElement = getElement("label_ActivityTypeName_xpath").changeRefAndGetElement(getElement("label_ActivityTypeName_xpath").getRef().replace("ID", idOnly).replace("XX", Integer.toString(k+1)));
										webActivityTypeName = activityType_NameElement.getText().replaceAll(" ", "");
									
										if (excelActivityTypeName.equalsIgnoreCase(webActivityTypeName)) {
											
											resultsPage_ActivityTypes.add(webActivityTypeName);
											
											UIElement activityPeriodElement = getElement("label_ActivityPeriod_xpath").changeRefAndGetElement(getElement("label_ActivityPeriod_xpath").getRef().replace("ID", idOnly).replace("XX", Integer.toString(k+1)));
											resultsPage_Period.add(activityPeriodElement.getText());
											UIElement activityRateTypeElement = getElement("label_ActivityRateType_xpath").changeRefAndGetElement(getElement("label_ActivityRateType_xpath").getRef().replace("ID", idOnly).replace("XX", Integer.toString(k+1)));
											resultsPage_RateType.add(activityRateTypeElement.getText());
											UIElement activityDailyRateElement = getElement("label_ActivityDailyRate_xpath").changeRefAndGetElement(getElement("label_ActivityDailyRate_xpath").getRef().replace("ID", idOnly).replace("XX", Integer.toString(k+1)));
											resultsPage_DailyRate.add(activityDailyRateElement.getText());
											Thread.sleep(1000);
											
											UIElement activityQTY_SelectElement = getElement("combobox_ActivitySelectCombo_classname");
											idNo2 = activityQTY_SelectElement.getAttribute("id").split("_")[6];
											
											int totalPaxCount = Integer.parseInt(searchObject.getAdults()) + Integer.parseInt(searchObject.getChildren());
											UIElement qty_Element = getElement("combobox_QTYSelectCombo_classname").changeRefAndGetElement(getElement("combobox_QTYSelectCombo_classname").getRef().replace("ID", idOnly).replace("XX", idNo2));
											Thread.sleep(1000);
											((ComboBox)qty_Element).selectOptionByText(Integer.toString(totalPaxCount));
											
											Thread.sleep(1000);
											UIElement selectAnActivity_Element = getElement("checkbox_ActivitySelectCheckBox_id").changeRefAndGetElement(getElement("checkbox_ActivitySelectCheckBox_id").getRef().replace("ID", idOnly).replace("XX", idNo2));
											selectAnActivity_Element.click();
											
											Thread.sleep(1000);
											activityDetails.setAddAndContinue(true);
											
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
							
							activityDetails.setResultsPage_ActivityTypes(resultsPage_ActivityTypes);
							activityDetails.setResultsPage_Period(resultsPage_Period);
							activityDetails.setResultsPage_RateType(resultsPage_RateType);
							activityDetails.setResultsPage_DailyRate(resultsPage_DailyRate);
							
							if (activityDetails.isAddAndContinue() == true) {
								//add and continue
								try {
									UIElement addandContinueButton_Element = getElement("button_AddandContinueButton_xpath").changeRefAndGetElement(getElement("button_AddandContinueButton_xpath").getRef().replace("ID", idOnly).replace("XX", Integer.toString(divCnt+2)));
									addandContinueButton_Element.click();
									
									getElementVisibility("button_CloseUpSellingButton_xpath", 60);
									getElementClick("button_CloseUpSellingButton_xpath");
									UIElement resultsPage_Cart_Element = getElement("label_ResultsPage_cartCurrency_xpath");
									activityDetails.setResultsPage_cartCurrencyValue(resultsPage_Cart_Element.getText().replaceAll(" ", ""));
									activityDetails.setResultsPageMybasketLoaded(true);
									
									getElement("button_CheckOutButton_xpath").setJavaScript("loadPaymentPage();");
								} catch (Exception e) {
									e.printStackTrace();
								}
								
							}else{
								//book
								UIElement bookButton_Element = getElement("button_BookButton_xpath").changeRefAndGetElement(getElement("button_BookButton_xpath").getRef().replace("ID", idOnly).replace("XX", Integer.toString(divCnt+2)));
								bookButton_Element.click();																
							}
							
							break outerloop;
							
						}
					}
				}
				
				if(!(actvityPage == wholepages)){
					getElement("button_NextPage_xpath").setJavaScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+(actvityPage+1)+"},'','WJ_3'))");
				}	
				
			}
			
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();		
		}
		
		return activityDetails;
		
	}
	

}