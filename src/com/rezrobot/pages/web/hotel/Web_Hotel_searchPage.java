package com.rezrobot.pages.web.hotel;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Image;
import com.rezrobot.pojo.Link;

public class Web_Hotel_searchPage extends PageObject{

	public boolean isPageAvailable() {
		return super.isPageAvailable("frame_homepageHotel_id");
	}

	public boolean doSearch() {
		try {
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean getLogoAvailability() throws Exception {
		try {
			return ((Image) getElement("img_company_logo")).isImageLoaded();
		} catch (Exception e) {
			return false;
		}
	}

	public String getLogoPath() throws Exception {
		return ((Image) getElement("img_company_logo")).getImagePath();
	}

	public boolean getCustomerLoginLinkAvailability() throws Exception {
		return getElementVisibility("link_customer_login");
	}

	public String getCustomerLoginLinkText() throws Exception {
		return ((Link) getElement("link_customer_login")).getText();
	}

	public String getCustomerLoginLinkPath() throws Exception {
		return ((Link) getElement("link_customer_login")).getUrl();
	}

	public boolean getBECAvailability() {
		try {
			switchToFrame("frame_homepageHotel_id");
			return getElementVisibility("button_hotel_xpath");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public ArrayList<String> getBecProducts() {
		ArrayList<String> AvailableProducts = new ArrayList<String>();

		if (getElementVisibility("button_MenuFlight_id"))
			AvailableProducts.add("Flight");
		if (getElementVisibility("button_hotel_xpath"))
			AvailableProducts.add("Hotel");
		if (getElementVisibility(""))
			AvailableProducts.add("Fixed Package");
		if (getElementVisibility(""))
			AvailableProducts.add("Holidays");
		if (getElementVisibility(""))
			AvailableProducts.add("Activity");
		if (getElementVisibility(""))
			AvailableProducts.add("Car");
		if (getElementVisibility(""))
			AvailableProducts.add("Transfer");

		return AvailableProducts;

	}

	public String getDefaultSelectedBEC() throws Exception {

		ArrayList<String> AvailableProducts = getBecProducts();

		if (AvailableProducts.contains("Flight")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Flight";
		else if (AvailableProducts.contains("Hotel")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Hotel";
		else if (AvailableProducts.contains("Activity")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Activity";
		else if (AvailableProducts.contains("Holidays")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Holidays";
		else if (AvailableProducts.contains("Transfer")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Transfer";
		else if (AvailableProducts.contains("Car")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Car";
		else if (AvailableProducts.contains("Fixed Package")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Fixed Package";
		else
			return "None";
	}
	
	public boolean isHotelBECLoaded() {
		try {
			switchToDefaultFrame();
			switchToFrame("frame_homepageHotel_id");
			getElementClick("button_hotel_xpath");
			return getElementVisibility("combobox_country_of_residence_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}


	public String monthDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_month"))
				.getDefaultSelectedOption();
	}

	public String dayDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_day")).getDefaultSelectedOption();
	}

	public String yearDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_year")).getDefaultSelectedOption();
	}

	public String distanceDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_distance"))
				.getDefaultSelectedOption();
	}

	public ArrayList<String> genderAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_gender")).getAvailableOptions();
	}

	public ArrayList<String> monthAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_month")).getAvailableOptions();
	}

	public ArrayList<String> dayAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_day")).getAvailableOptions();
	}

	public ArrayList<String> yearAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_year")).getAvailableOptions();
	}

	public HashMap<String, String> getPlaceHolders() {

		HashMap<String, String> placeholders = new HashMap<>();
		
	    try {
	    	placeholders.put("Country of Residence", ((ComboBox)getElement("combobox_ResidenceCountry_id")).getDefaultSelectedOption().trim());
	    	placeholders.put("ToCityInput", getPlaceHolder("inputfield_To_TxtBox_id"));
            
		} catch (Exception e) {
			e.printStackTrace();
		}

		return placeholders;
	}
	
	public HashMap<String, String> getHotelBECLabels() {

		HashMap<String, String> labels = new HashMap<>();
				
		try {
			labels.put("customerLogin", getLabelName("link_customer_login"));
			labels.put("countryOfResidence", getLabelName("label_country_of_residence_xpath"));
			labels.put("whereAreYouGoing", getLabelName("label_where_are_you_going_xpath"));
			labels.put("WhenAreYouGoing", getLabelName("label_when_are_you_going_xpath"));
			labels.put("howManyPeople", getLabelName("label_how_many_people_xpath"));
			labels.put("checkin", getLabelName("label_check_in_xpath"));
			labels.put("checkout", getLabelName("label_check_out_xpath"));
			labels.put("nights", getLabelName("label_nights_xpath"));
			labels.put("rooms", getLabelName("label_rooms_xpath"));
			labels.put("room", getLabelName("label_room_xpath"));
			labels.put("adults", getLabelName("label_adults_xpath"));
			labels.put("children", getLabelName("label_children_xpath"));
			labels.put("showAdditional", getLabelName("label_show_additional_search_options_xpath"));
			getElement("button_show_additional_search_options_xpath").click();
			labels.put("starRating", getLabelName("label_star_rating_xpath"));
			labels.put("hotelType", getLabelName("label_hotel_type_xpath"));
			labels.put("hotelName", getLabelName("label_hotel_name_xpath"));
			labels.put("preferredCurrency", getLabelName("lable_preferred_currency_xpath"));
			labels.put("promotionCode", getLabelName("label_promotion_code_xpath"));
			labels.put("hotelAvailability", getLabelName("label_hotel_availability_xpath"));
			labels.put("available", getLabelName("label_available_xpath"));
			labels.put("onRequest", getLabelName("label_on_request_xpath"));
			labels.put("priceRange", getLabelName("label_price_range_xpath"));
			labels.put("to", getLabelName("label_to_xpath"));
			labels.put("from", getLabelName("label_from_xpath"));
			labels.put("hideAdditional", getLabelName("label_hide_additional_search_options_xpath"));
			labels.put("searchForHotels", getLabelName("button_search_for_hotels_id"));
		} catch (Exception e) {
			e.printStackTrace();
		}


		return labels;
	}
	
	
	
	public HashMap<String, Boolean> getHotelAvailabilityOfInputFields() {

		HashMap<String, Boolean> fields = new HashMap<>();
		
		try {
				
			fields.put("whereAreYouGoing", getElementVisibility("inputfield_where_are_you_going_id"));
			//getElement("button_show_additional_search_options_xpath").click();
			fields.put("hotelName", getElementVisibility("inputfield|_hotel_name_id"));
			fields.put("promotionCode",getElementVisibility("inputfield_promotion_code_id"));
			fields.put("from",getElementVisibility("inputfield_from_id"));
			fields.put("to",getElementVisibility("inputfield_to_id"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fields;
	}
	
	public HashMap<String, Boolean> getHotelMandatoryList() {

		HashMap<String, Boolean> fields = new HashMap<>();

		fields.put("countryOfResidence", getMandatoryStatus("combobox_country_of_residence_id"));
		fields.put("whereAreYouGoing", getMandatoryStatus("inputfield_where_are_you_going_id"));
		return fields;
	}
	
	public HashMap<String, String> getHotelComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();

		fields.put("countryOfResidence", getComboBoxDataList("combobox_country_of_residence_id"));		
		fields.put("nights", getComboBoxDataList("combobox_nights_id"));
		fields.put("rooms", getComboBoxDataList("combobox_rooms_id"));
		fields.put("adults", getComboBoxDataList("combobox_adults_id"));
		fields.put("children",getComboBoxDataList("combobox_children_id"));
		((ComboBox)getElement("combobox_children_id")).selectOptionByText("1");
		System.out.println(getComboBoxDataList("combobox_children_age_id"));
		fields.put("age",getComboBoxDataList("combobox_children_age_id"));	
		fields.put("starRating", getComboBoxDataList("combobox_star_rating_id"));
		fields.put("hotelType", getComboBoxDataList("combobox_hotel_type_id"));
		fields.put("preferredCurrency",getComboBoxDataList("combobox_preferred_currency_id"));

		return fields;
	}

	public void searchForHotels() throws Exception {
		String destination = "1112|London||54||United Kingdom|";
		String checkin = "08/15/2016";
		String checkout = "08/16/2016";
		
		((ComboBox)getElement("combobox_country_of_residence_id")).selectOptionByText("Sri Lanka");
		getElement("inputfield_where_are_you_going_id").setText("London");
		getElement("inputfield_hidden_Destination_id").setJavaScript("$('#hid_H_Loc').val('"+destination+"');");
		getElement("inputfield_hidden_checkin_id").setJavaScript("$('#ho_departure').val('"+checkin+"');");
		getElement("inputfield_hidden_checlout_id").setJavaScript("$('#ho_arrival').val('"+checkout+"');");
		((ComboBox)getElement("combobox_adults_id")).selectOptionByText("2");
		((ComboBox)getElement("combobox_children_id")).selectOptionByText("1");	
		getElement("button_search_for_hotels_id").click();

	}
	
}
