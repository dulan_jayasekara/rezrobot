package com.rezrobot.pages.web.hotel;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelPaymentPageMoreDetails;
import com.rezrobot.dataobjects.HotelSearchObject;

public class WEB_Hotel_Paymentpage_occupancy_details extends PageObject {

	public HashMap<String, String> getHotelLabels() {

		HashMap<String, String> labels = new HashMap<>();
		try {
			labels.put("hotelRate", "label_hotel_rate_xpath");
			labels.put("billingInfoSubtotal", "label_billing_info_subtotal_id");
			labels.put("billingInfoTotaltax", "label_billing_info_totaltax_id");
			labels.put("billingInfoTotal", "label_billing_info_total_id");
			labels.put("billingInfoAmountprocessed", "label_billing_info_amountprocessed_id");
			labels.put("moreDetailsRoomtype", "label_more_details_roomtype_xpath");
			labels.put("moreDetailsBedtype", "label_more_details_bedtype_xpath");
			labels.put("moreDetailsRateplan", "label_more_details_rateplan_xpath");
			labels.put("moreDetailsRoomrate", "label_more_details_roomrate_xpath");
			labels.put("moreDetailsSubtotal", "label_more_details_subtotal_xpath");
			labels.put("moreDetailsTax", "label_more_details_tax_xpath");
			labels.put("moreDetailsHotelcost", "label_more_details_hotelcost_xpath");
			labels.put("moreDetailsAvailability", "label_more_details_avaialbilitystatus_xpath");
			labels.put("moreDetailsCancellationdeadline", "label_more_details_cancellationdeadline_xpath");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return labels;
	}
	
	public HotelPaymentPageMoreDetails getDetails(HotelSearchObject search) throws Exception
	{
		System.out.println("Payment page - Shopping cart - More details");
		String[] temp = null;
		HotelPaymentPageMoreDetails details = new HotelPaymentPageMoreDetails();
		try {
			details.setHotelPrice			(getElement("label_hotel_rate_xpath").getText());
			details.setSubtotal				(getElement("label_billing_info_subtotal_id").getText());
			details.setTaxes				(getElement("label_billing_info_totaltax_id").getText());
			details.setTotal				(getElement("label_billing_info_total_id").getText());
			details.setAmountProcessedNow	(getElement("label_billing_info_amountprocessed_id").getText());
			details.setCurrencyCode			(getElement("label_billing_info_currencyCode_id").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ArrayList<String>  				roomType 	= new ArrayList<String>();
		ArrayList<String>  				bedType 	= new ArrayList<String>();
		ArrayList<String>  				ratePlan 	= new ArrayList<String>();
		ArrayList<String>  				roomRate 	= new ArrayList<String>();
		
		getElement("button_more_details_xpath").click();
		
		for(int i = 1 ; i > 0 ; i++)
		{
			try {
				String checkAvailability = getElement("label_more_details_roomtype_xpath").changeRefAndGetElement(getElement("label_more_details_roomtype_xpath").getRef().replace("replace", String.valueOf(i))).getText();
				roomType.add(getElement("label_more_details_roomtype_xpath").changeRefAndGetElement(getElement("label_more_details_roomtype_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				bedType.add(getElement("label_more_details_bedtype_xpath").changeRefAndGetElement(getElement("label_more_details_bedtype_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				ratePlan.add(getElement("label_more_details_rateplan_xpath").changeRefAndGetElement(getElement("label_more_details_rateplan_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				String[] rate = getElement("label_more_details_roomrate_xpath").changeRefAndGetElement(getElement("label_more_details_roomrate_xpath").getRef().replace("replace", String.valueOf(i))).getText().split(" ");
				System.out.println(rate[1]);
				roomRate.add(rate[1]);
				if(checkAvailability.equals(null) || checkAvailability.isEmpty())
				{
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
		}
		details.setBedType				(bedType);
		details.setRoomType				(roomType);
		details.setRatePlan				(ratePlan);
		details.setRoomRate				(roomRate);
		
		try {
			System.out.println(getElement("label_more_details_avaialbilitystatus_xpath").getText().replace(": Status - ", "").trim());
			details.setStatus				(getElement("label_more_details_avaialbilitystatus_xpath").getText().replace(": Status - ", "").trim());
			temp = getElement("label_more_details_cancellationdeadline_xpath").getText().split("-");
			details.setCancellationDeadline	(temp[1].trim());
			System.out.println(getElement("label_more_details_hotelcost_xpath").getText());
			details.setMoreDetailsHotelcost	(getElement("label_more_details_hotelcost_xpath").getText());
			details.setMoreDetailsSubtotal	(getElement("label_more_details_subtotal_xpath").getText());
			details.setMoreDetailsTax		(getElement("label_more_details_tax_xpath").getText());
			getElement("button_more_details_close_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
			getElement("button_more_details_xpath").click();
		}
		
		
		return details;
		
		
	}
}
