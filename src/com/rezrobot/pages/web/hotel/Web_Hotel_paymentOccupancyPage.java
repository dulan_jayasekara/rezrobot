package com.rezrobot.pages.web.hotel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.utill.DriverFactory;

public class Web_Hotel_paymentOccupancyPage extends PageObject {

	public boolean isPaymentPageOccupancyDetailsAvailable(){
		System.out.println("Occupancy details availability");
		switchToDefaultFrame();
		//System.out.println(getElement("label_title_xpath").getRef());
		return super.isPageAvailable("label_hotel_name_xpath");
		
	}
	
	public void next() throws Exception
	{
		try {
			getElement("inputfield_child_last_name_classname").click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		getElement("button_next_step_classname").click();
	}
	
	public HashMap<String, Boolean> getOccupancyButtons() {

		HashMap<String, Boolean> buttons = new HashMap<>();
		
		try {
			buttons.put("roomOptions", getElementVisibility("button_room_options_xpath"));
			buttons.put("roomOptionsClose", getElementVisibility("button_room_options_close_xpath"));
			buttons.put("prevStep", getElementVisibility("button_prev_step_classname"));
			buttons.put("nextStep", getElementVisibility("button_next_step_classname"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return buttons;
	}
	
	public HashMap<String, String> getOccupancyLabels() {

		HashMap<String, String> labels = new HashMap<>();
		
		try {
		labels.put("hotelName", 	getLabelName("label_hotel_name_xpath"));
		labels.put("roomNumber", 	getLabelName("label_room_number_xpath"));
		labels.put("roomOptions", 	getLabelName("label_room_options_xpath"));
		labels.put("guestNumber", 	getLabelName("label_guest_number_xpath"));
		labels.put("childNumber", 	getLabelName("label_child_number_xpath"));
		labels.put("title", 		getLabelName("label_title_xpath"));
		labels.put("firstName", 	getElements("label_first_name_id").get(1).getText());
		labels.put("lastName", 		getElements("label_last_name_id").get(1).getText());
		getElement("button_room_options_xpath").click();
		labels.put("smoking", 		getLabelName("label_smoking_xpath"));
		labels.put("noSmoking", 	getLabelName("label_no_smoking_xpath"));
		labels.put("handicap",	 	getLabelName("label_handicap_xpath"));
		labels.put("wheelchair", 	getLabelName("label_wheelchair_xpath"));
		labels.put("estimatedArrivalTime", getLabelName("label_estimated_arrival_time_xpath"));} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getOccupancyComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();
		try {
			fields.put("title", getComboBoxDataList("combobox_title_classname"));	
			fields.put("estimatedArrivalTimeHours", getComboBoxDataList("combobox_estimated_arrival_time_hours_xpath"));	
			fields.put("estimatedArrivalTimeMins", getComboBoxDataList("combobox_estimated_arrival_time_mins_xpath"));	
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return fields;
	}
	
	public HashMap<String, Boolean> getOccupancyRadioButtons()
	{
		HashMap<String, Boolean> radioButton = new HashMap<>();
		try {
			radioButton.put("smoking", isRadioBoxSelected("radiobutton_smoking_xpath"));
			radioButton.put("noSmoking", isRadioBoxSelected("radiobutton_no_smoking_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return radioButton;
	}
	
	public HashMap<String, Boolean> getOccupancyCheckBox()
	{
		HashMap<String, Boolean> checkBox = new HashMap<>();
		
		try {
			checkBox.put("handicap", isCheckBoxSelected("checkbox_handicap_xpath"));
			checkBox.put("wheelchair", isCheckBoxSelected("checkbox_wheelchair_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return checkBox;
	}
	
	public HashMap<String, Boolean> getOccupancyinputfields() {

		HashMap<String, Boolean> fields = new HashMap<>();
		try {
			fields.put("adultFirstName", getElementVisibility("inputfield_adult_first_name_classname"));
			fields.put("adultLastName", getElementVisibility("inputfield_adult_last_name_classname"));
			fields.put("childFirstName", getElementVisibility("inputfield_child_first_name_classname"));
			fields.put("childLastName", getElementVisibility("inputfield_child_last_name_classname"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return fields;
	}
	
	public void enterHotelOccupancyDetails(ArrayList<HotelOccupancyObject> occupancyList) throws Exception
	{

		for(int i = 0 ; i < getElement("inputfield_adult_first_name_classname").getWebElements().size() ; i++)
		{
			try {
				getElement("inputfield_adult_first_name_classname").getWebElements().get(i).clear();
				getElement("inputfield_adult_last_name_classname").getWebElements().get(i).clear();
			} catch (Exception e) {
				// TODO: handle exception
			}	
			getElement("inputfield_adult_first_name_classname").getWebElements().get(i).sendKeys(occupancyList.get(i).getAdultFirstName());
			getElement("inputfield_adult_last_name_classname").getWebElements().get(i).sendKeys(occupancyList.get(i).getAdultLastName());
		}
		
		for(int i = 0 ; i < getElement("inputfield_child_first_name_classname").getWebElements().size() ; i++)
		{
			getElement("inputfield_child_first_name_classname").getWebElements().get(i).sendKeys(occupancyList.get(i).getChildFirstName());
			getElement("inputfield_child_last_name_classname").getWebElements().get(i).sendKeys(occupancyList.get(i).getChildLastName());
		}
	}
	
	public void enterOccupancyDetails() throws Exception
	{
		for(int i = 0 ; i < getElement("inputfield_adult_first_name_classname").getWebElements().size() ; i++)
		{
			try {
				getElement("inputfield_adult_first_name_classname").getWebElements().get(i).clear();
				getElement("inputfield_adult_last_name_classname").getWebElements().get(i).clear();
			} catch (Exception e) {
				// TODO: handle exception
			}	
			getElement("inputfield_adult_first_name_classname").getWebElements().get(i).sendKeys("aaa");
			getElement("inputfield_adult_last_name_classname").getWebElements().get(i).sendKeys("bbb");
		}
		
		for(int i = 0 ; i < getElement("inputfield_child_first_name_classname").getWebElements().size() ; i++)
		{
			getElement("inputfield_child_first_name_classname").getWebElements().get(i).sendKeys("ccc");
			getElement("inputfield_child_last_name_classname").getWebElements().get(i).sendKeys("ddd");
		}
		
		getElement("button_next_step_classname").click();
		
	}
	
}
