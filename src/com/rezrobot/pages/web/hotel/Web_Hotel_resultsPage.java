package com.rezrobot.pages.web.hotel;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.common.pojo.roomDetails;
import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelResultsPageMoreDetails;
import com.rezrobot.dataobjects.HotelRoomDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ReportTemplate;

	public class Web_Hotel_resultsPage extends PageObject{

	public boolean isHotelResultsPageAvailable() throws InterruptedException{
		switchToDefaultFrame();
		Thread.sleep(5000);
		return super.isPageAvailable("label_my_basket_xpath");
	}
	
	public boolean isSelectedHotelResultsAvailable() {
		try {
			switchToDefaultFrame();
			return getElementVisibility("label_results_hotel_name_id", 100);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
	
	
	public boolean isHotelResultsAvailable() {
		
		try {
			switchToDefaultFrame();
			return getElementVisibility("div_first_hotel_id", 100);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
	
	public HashMap<String, Boolean> getHotelAvailabilityOfButtons() {

		HashMap<String, Boolean> buttons = new HashMap<>();
		try {
			buttons.put("filterReset", getElementVisibility("button_reset_xpath"));
			buttons.put("moreDetails", getElementVisibility("button_more_details_xpath"));
			buttons.put("cancellationPolicy", getElementVisibility("button_cancellation_policy_xpath"));
			buttons.put("book", getElementVisibility("button_book_id"));
			buttons.put("addAndCon", getElementVisibility("button_add_and_continue_id"));
			buttons.put("showMoreRooms", getElementVisibility("button_show_more_rooms_id"));
			buttons.put("overview", getElementVisibility("button_overview_xpath"));
			buttons.put("location", getElementVisibility("button_location_on_map_xpath"));
			buttons.put("reviewAndRatings", getElementVisibility("button_reviews_and_ratings_xpath"));
			buttons.put("amenities", getElementVisibility("button_amenities_xpath"));
			buttons.put("additionalInfo", getElementVisibility("button_additional_info_xpath"));
			buttons.put("emailToFrnd", getElementVisibility("button_email_to_friend_xpath"));
			buttons.put("sendEmail", getElementVisibility("button_send_email_xpath"));
			buttons.put("emailIsSentOk", getElementVisibility("button_email_is_sent_ok_xpath"));
			buttons.put("resetFiltersLeft", getElementVisibility("button_reset_filters_left_side_xpath"));
			buttons.put("resetFiltersLeftBottom", getElementVisibility("button_reset_filters_left_bottom_xpath"));
			buttons.put("customerLogin", getElementVisibility("button_customer_login_xpath"));
			buttons.put("home", getElementVisibility("button_home_xpath"));
			buttons.put("upSellingHotels", getElementVisibility("button_up_selling_add_hotels_xpath"));
			buttons.put("upSellingFlights", getElementVisibility("button_up_selling_add_flights_xpath"));
			buttons.put("upSellingActivities", getElementVisibility("button_up_selling_add_activities_xpath"));
			buttons.put("upSellingCars", getElementVisibility("button_up_selling_add_cars_xpath"));
			buttons.put("upSellingTransfers", getElementVisibility("button_up_selling_add_transfers"));
			buttons.put("showAdditionalFilters", getElementVisibility("button_show_additional_filters_xpath"));
			buttons.put("addAndConActivities", getElementVisibility("button_add_and_continue_add_activities_xpath"));
			buttons.put("addConFlights", getElementVisibility("button_add_and_continue_add_flights_xpath"));
			buttons.put("addAndConCars", getElementVisibility("button_add_and_continue_add_cars_xpath"));
			buttons.put("addAndConTransfers", getElementVisibility("button_add_and_continue_add_transfers_xpath"));
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return buttons;
	}
	
	public HashMap<String, Boolean> getHotelAvailabilityOfInputFields() {

		HashMap<String, Boolean> fields = new HashMap<>();
		try {
			getElement("button_show_additional_filters_xpath").click();
			fields.put("hotelName", getElementVisibility("inputfield_hotel_name_id"));
			fields.put("promotionCode", getElementVisibility("inputfield_promotional_code_id"));
			fields.put("priceRangeFrom", getElementVisibility("inputfield_price_range_from_id"));
			fields.put("priceRangeTo", getElementVisibility("inputfield_price_range_to_id"));
			fields.put("hotelNameFilter", getElementVisibility("inputfield_filter_by_hotel_name_id"));
			fields.put("suburb", getElementVisibility("inputfield_suburb_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return fields;
	}
	
	public HashMap<String, String> getHotelComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();

		fields.put("starRating", getComboBoxDataList("combobox_star_rating_id"));		
		fields.put("hotelType", getComboBoxDataList("combocbox_hotel_type_id"));
		fields.put("preferredCurrency", getComboBoxDataList("combobox_preferred_currency_id"));
		fields.put("hotelAvailability", getComboBoxDataList("combobox_hotel_availability_id"));
		fields.put("nearByAttractions", getComboBoxDataList("combobox_near_by_attractions_xpath"));
		
		return fields;
	}
	
	public HashMap<String, String> getHotelLabels() {

		HashMap<String, String> labels = new HashMap<>();
				
		try {
			labels.put("showAdditional", getLabelName("label_show_additional_xpath"));
			labels.put("additionalStarRating", getLabelName("label_star_rating_xpath"));
			labels.put("additionalHotelType", getLabelName("label_hotel_type_xpath"));
			labels.put("additionalHotelName", getLabelName("label_hotel_name_xpath"));
			labels.put("additionalPreferredCurrency", getLabelName("label_preferred_currency_xpath"));
			labels.put("additionalPromotionCode", getLabelName("label_promotion_code_xpath"));
			labels.put("additionalPriceRangeFrom", getLabelName("label_price_range_from_xpath"));
			labels.put("additionalPriceRangeTo", getLabelName("label_price_range_to_xpath"));
			labels.put("additionalAvailable", getLabelName("label_available_xpath"));
			labels.put("additionalOnReq", getLabelName("label_on_request_xpath"));
			labels.put("location", getLabelName("label_location_id"));
			labels.put("hotelCount", getLabelName("label_number_of_hotels_avaialbe_xpath"));
			labels.put("sortPriceAscending", getLabelName("label_price_acsending_order_xpath"));
			labels.put("sortPrice", getLabelName("label_price_xpath"));
			labels.put("sortPriceDescending", getLabelName("label_price_descending_order_xpath"));
			labels.put("sortStarAscending", getLabelName("label_star_ascending_order_xpath"));
			labels.put("sortStar", getLabelName("label_star_xpath"));
			labels.put("sortStarDescending", getLabelName("label_star_descending_order_xpath"));
			labels.put("sortNameAscending", getLabelName("label_name_ascending_order_xpath"));
			labels.put("sortName", getLabelName("label_name_xpath"));
			labels.put("sortNameDescending", getLabelName("label_name_descending_xpath"));
			labels.put("hotelName", getLabelName("label_results_hotel_name_id"));
			labels.put("hotelRate", getLabelName("label_rate_id"));
			labels.put("hotelRoom", getLabelName("label_room_xpath"));
			labels.put("hotelRoomType", getLabelName("label_room_types_xpath"));
			labels.put("hotelRatePlan", getLabelName("label_rate_plan_xpath"));
			labels.put("hotelRoomPrice", getLabelName("label_price_xpath"));
			labels.put("hotelAddress", getLabelName("label_hotel_address_xpath"));
			labels.put("moreInfoOverview", getLabelName("label_overview_xpath"));
			labels.put("moreInfoAmenities", getLabelName("label_amenities_xpath"));
			labels.put("moreinfoAdditionalInfo", getLabelName("label_additional_info_xpath"));
			labels.put("emailHeader", getLabelName("label_email_header_xpath"));
			labels.put("emailSubject", getLabelName("label_email_subject_xpath"));
			labels.put("emailContent", getLabelName("label_email_content_xpath"));
			labels.put("emailIsSent", getLabelName("label_email_is_sent_xpath"));
			labels.put("myBasket", getLabelName("label_my_basket_xpath"));
			labels.put("noItemIsSelected", getLabelName("label_no_item_selected_xpath"));
			labels.put("priceRange", getLabelName("label_price_range_xpath"));
			labels.put("starRating", getLabelName("label_star_rating_xpath"));
			labels.put("starRange", getLabelName("label_star_range_xpath"));
			labels.put("suburb", getLabelName("label_suburb_xpath"));
			labels.put("hotelAvailability", getLabelName("label_hotel_availability_xpath"));
			labels.put("nearByAttractions", getLabelName("label_near_by_attractions_xpath"));
			labels.put("specialDeals", getLabelName("label_spacial_deals_xpath"));
			labels.put("propertyType", getLabelName("label_property_type_xpath"));
			labels.put("amenities", getLabelName("label_amenities_xpath"));
		}
		catch(Exception e)
		{
			
		}
		
		return labels;
		
	}
	
	
	public HotelDetails getSelectedHotelInfo(HotelSearchObject search, ArrayList<HotelDetails> hotelList, LabelReadProperties labelProperty, ReportTemplate printTemplate, HashMap<String, String> portalSpecificData) throws Exception
	{
		HotelDetails hotel = new HotelDetails();
		String tracerID = "";
		ArrayList<String> hotelName = new ArrayList<>();
		ArrayList<String> hotelID = new ArrayList<>();
		String starRating = "";
		int timeout = 10;
		int internal = 0;
		int hb = 0;
		int gta = 0;
		int tourico = 0;
		int rezlive = 0;
		int hotelspro = 0;
		try {
			ArrayList<HotelRoomDetails> roomList = new ArrayList<HotelRoomDetails>();
			for(int i = 0 ; i < hotelList.size() ; i++)
			{
				if(search.getSupplier().equals(hotelList.get(i).getSupplier()))
				{
					hotelName.add(hotelList.get(i).getTitle());
					hotelID.add(hotelList.get(i).getHotelID());
				}
			}
			
			for(int i = 0 ; i < hotelList.size() ; i++)
			{
				if(hotelList.get(i).getSupplier().equals("hotelbeds_v2"))
					hb++;
				else if(hotelList.get(i).getSupplier().equals("INV27"))
					gta++;
				else if(hotelList.get(i).getSupplier().equals("INV13"))
					tourico++;
				else if(hotelList.get(i).getSupplier().equals("rezlive"))
					rezlive++;
				else if(hotelList.get(i).getSupplier().equals("hotelspro"))
					hotelspro++;
				else
					internal++;
			}
			System.out.println("HB - " + hb);
			System.out.println("inv27 - " + gta);
			System.out.println("inv13 - " + tourico);
			System.out.println("rezlive - " + rezlive);
			System.out.println("Hotelspro - " + hotelspro);
			System.out.println("internal - " + internal);
//			printTemplate.setInfoTableHeading("Add to cart issues");
			for(int j = 0 ; j < hotelName.size() ; j++)
			{
				System.out.println(hotelName.get(j));
				if(getElement("button_show_additional_filters_xpath").getText().equals(labelProperty.getHotelLabelProperties().get("rpShowAdditional")))
				{
					getElement("button_show_additional_filters_xpath").click();
				}
				getElement("inputfield_hotel_name_id").clear();
				getElement("inputfield_hotel_name_id").setText(hotelName.get(j));
				String 		destinationHelp 	= search.getDestination().replace("|", "/");
				String[] 	destination 		= destinationHelp.split("/");
				getElement("inputfield_destination_id").clear();
				getElement("inputfield_destination_id").setText(destination[1]);
				getElement("button_search_again_classname").click();
				
				if(isSelectedHotelResultsAvailable())
				{
					hotel.setTitle(hotelName.get(j));
					String moreDetails[] = getElement("button_more_details_xpath").getAttribute("href").split("'");
					hotel.setHotelID(moreDetails[3]);
					hotel.setDestination(moreDetails[1]);
					hotel.setSupplier(moreDetails[7]);
					
					//get star rating
					for(int i = 0 ; i <= 7 ; i++)
					{
						if(getElement("image_hotel_star_rating_classname").changeRefAndGetElement(getElement("image_hotel_star_rating_classname").getRef().concat(String.valueOf(i))).isElementAvailable())
						{
							starRating = String.valueOf(i);
							break;
						}	
					}
					hotel.setStarRating(starRating);
					String rate[] = getElement("label_rate_id").changeRefAndGetElement(getElement("label_rate_id").getRef().concat(hotel.getHotelID()).concat("-1")).getText().split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
					hotel.setRate(rate[1]);
					hotel.setCurrency(rate[0]);
					hotel.setAddress(getElement("label_results_hotel_address_xpath").getText());
					

					//get room details
					int roomCount = 1;
					for(int i = 1 ; i  <= getElement("label_hotel_room_name_classname").getWebElements().size() ; i++)
					{
						HotelRoomDetails room = new HotelRoomDetails();
						if(getElement("label_room_types_id").changeRefAndGetElement(getElement("label_room_types_id").getRef().concat(String.valueOf(i))).isElementAvailable())
						{
							if(getElement("label_room_types_id").changeRefAndGetElement(getElement("label_room_types_id").getRef().concat(String.valueOf(i))).getText().equals(""))
							{
								
							}
							else
							{
								room.setRoom(getElement("label_room_id").changeRefAndGetElement(getElement("label_room_id").getRef().concat(String.valueOf(roomCount))).getText());
								room.setRoomType(getElement("label_room_types_id").changeRefAndGetElement(getElement("label_room_types_id").getRef().concat(String.valueOf(i))).getText());
								room.setRatePlan(getElement("label_rate_plan_id").changeRefAndGetElement(getElement("label_rate_plan_id").getRef().concat(String.valueOf(i))).getText());
								room.setRate(getElement("label_price_id").changeRefAndGetElement(getElement("label_price_id").getRef().concat(String.valueOf(i))).getText());
								roomCount++;
								roomList.add(room);
							}
						}	
					}
					hotel.setRoomList(roomList);
					
					// get more details
					getElement("button_more_details_classname").click();
					 
					// check more details availability
					HotelResultsPageMoreDetails moreinfo = new HotelResultsPageMoreDetails();
					if(getElement("button_overview_xpath").changeRefAndGetElement(getElement("button_overview_xpath").getRef().replace("replace", hotelID.get(j))).isElementVisible(timeout))
					{
						moreinfo.setMoreInfoAvailability(true);
						// overview
						try {
							moreinfo.setOverview(getElement("label_overview_xpath").changeRefAndGetElement(getElement("label_overview_xpath").getRef().replace("replace", hotelID.get(j))).getText());
							moreinfo.setImageAvailability(getElement("image_more_info_hotel_image_xpath").changeRefAndGetElement(getElement("image_more_info_hotel_image_xpath").getRef().replace("replace", hotelID.get(j))).isElementVisible(timeout));
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						
						// location on map
						try {
							getElement("button_location_on_map_xpath").changeRefAndGetElement(getElement("button_location_on_map_xpath").getRef().replace("replace", hotelID.get(j))).click();
							moreinfo.setMapAvailability(getElement("image_more_info_map_xpath").changeRefAndGetElement(getElement("image_more_info_map_xpath").getRef().replace("replace", hotelID.get(j))).isElementVisible(timeout));
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					
						// amenities
						try {
							getElement("button_amenities_xpath").changeRefAndGetElement(getElement("button_amenities_xpath").getRef().replace("replace", hotelID.get(j))).click();
							ArrayList<String> amenityList = new ArrayList<String>();
							if(getElement("label_amenities_xpath").changeRefAndGetElement(getElement("label_amenities_xpath").getRef().replace("replace1", hotelID.get(j)).replace("replace2", "1")).isElementVisible())
							{
								moreinfo.setAmanityAvailability(true);
								for(int i = 1 ; i <= getElement("label_more_info_amenities_classname").getWebElements().size() ; i++)
								{
									amenityList.add(getElement("label_amenities_xpath").changeRefAndGetElement(getElement("label_amenities_xpath").getRef().replace("replace1", hotelID.get(j)).replace("replace2", String.valueOf(i))).getText());
								}
								moreinfo.setAmenityList(amenityList);
							}
							else
							{
								moreinfo.setAmanityAvailability(false);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						//additional info
						try {
							getElement("button_additional_info_xpath").changeRefAndGetElement(getElement("button_additional_info_xpath").getRef().replace("replace", hotelID.get(j))).click();
							moreinfo.setAdditionalInfo(getElement("label_more_info_additional_info_xpath").changeRefAndGetElement(getElement("label_more_info_additional_info_xpath").getRef().replace("replace", hotelID.get(j))).getText());
						} catch (Exception e) {
							// TODO: handle exception
						}
						//send email
						getElement("button_email_to_friend_xpath").changeRefAndGetElement(getElement("button_email_to_friend_xpath").getRef().replace("replace", hotelID.get(j))).click();
						getElement("inputfield_email_address_id").setText("akila@colombo.rezgateway.com");//set dynamically
						hotel.setEmailSubject(getElement("inputfield_email_subject_id").getText());
						hotel.setEmailBody(getElement("inputfield_email_body_id").getText());
						getElement("button_send_email_xpath").click();
						hotel.setEmailStatus(getElement("label_email_is_sent_xpath").getText());
						getElement("button_email_sent_confirmation_xpath").click();
					}
					else
					{
						moreinfo.setMoreInfoAvailability(false);
					}
					hotel.setMoreInfo(moreinfo);
		// check whether tracer has to be splited			
					if(portalSpecificData.get("splitTracer").equals("yes"))
					{
						String[] temp = getElement("tracerID").changeRefAndGetElement(getElement("tracerID").getRef().concat(hotelID.get(j))).getAttribute("value").split("-");
						hotel.setTracerID(temp[1] + "-" + temp[2] + "-" + temp[3] + "-" + temp[4] + "-" + temp[5]);
					}
					else
					{
						hotel.setTracerID(getElement("tracerID").changeRefAndGetElement(getElement("tracerID").getRef().concat(hotelID.get(j))).getAttribute("value"));
					}
					
					System.out.println("Tracer ID - " + hotel.getTracerID());
					try {
						getElement("button_book_id").click();
						Thread.sleep(2000);
						if(getElementVisibility("label_cannot_add_this_combination_id", 30))
						{
//							printTemplate.addToInfoTabel(hotelName.get(j), getElement("label_cannot_add_this_combination_id").getText());
							getElement("button_cannot_add_this_combination_xpath").click();
							try {
								getElement("button_cannot_add_this_combination_xpath").click();
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
						else if(getElementVisibility("label_rate_got_changed_id", 10))
						{
//							printTemplate.addToInfoTabel(hotelName.get(j), getElement("label_rate_got_changed_id").getText());
							hotel.setIsRateChangeAvailable(true);
							getElement("button_accept_rate_change_xpath").click();
						}
						else
						{
//							printTemplate.addToInfoTabel(hotelName.get(j), "Add to cart was successful");
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("add to cart error - " + e);
					}
				}
				else{

				}
			}
//			printTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		
		return hotel;
		
	}
	
	public ArrayList<HotelDetails> getHotelDetails() throws Exception
	{
		
		String[] 	hotelCount 			= getElement("label_number_of_hotels_avaialbe_xpath").getText().split(" ");
		int 		pageCount 			= 0;
		
		
		if(Integer.parseInt(hotelCount[0])%10 == 0)
		{
			pageCount = Integer.parseInt(hotelCount[0])/10;
		}
		else
		{
			pageCount = Integer.parseInt(hotelCount[0])/10;
			pageCount = pageCount + 1;
		}
		System.out.println(pageCount);
		String 	moreDetails[];
		String 	tracerID = "";
		String 	city = "";
		String 	hotelID = "";
		String 	supplier = "";
		int 	count = 1;
		UIElement changeRef = new UIElement();
		ArrayList<HotelDetails> hotelList = new ArrayList<HotelDetails>();
		pageIterator : for(int pageIterator = 1 ; pageIterator <= pageCount ; pageIterator++)
		{
			try {
				executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_3'))");
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			// get all hotel details
			resultsIterator : for(int resultsIterator = 1 ; resultsIterator <= 10 ; resultsIterator++)
			{
				HotelDetails hotel = new HotelDetails();
				try {
					changeRef = getElement("button_more_details_xpath").changeRefAndGetElement(getElement("button_more_details_xpath").getRef().replace("_1", "_".concat(String.valueOf(count))));
					moreDetails = changeRef.getAttribute("href").replace("'", "").split(",");
					hotelID = moreDetails[1];
					supplier = moreDetails[3].replace(")", "");
					hotel.setHotelID(hotelID);
					changeRef = getElement("label_results_hotel_name_id").changeRefAndGetElement(getElement("label_results_hotel_name_id").getRef().replace("1", String.valueOf(count)));
					hotel.setTitle(changeRef.getText());
					changeRef = getElement("label_results_hotel_address_xpath").changeRefAndGetElement(getElement("label_results_hotel_address_xpath").getRef().replace("_1", "_".concat(String.valueOf(count))));
					hotel.setAddress(changeRef.getText());
					changeRef = getElement("label_results_rate_id").changeRefAndGetElement(getElement("label_results_rate_id").getRef().concat(hotelID).concat("-".concat(String.valueOf(count))));
					hotel.setRate(changeRef.getText());
					hotel.setSupplier(supplier);
					hotelList.add(hotel);
					
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				count++;
			}
		}
		
		return hotelList;
	}
}
