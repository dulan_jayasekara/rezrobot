package com.rezrobot.pages.web.hotel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelRoomDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.dataobjects.hotelOccupancy;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;


public class Web_Hotel_ConfirmationPage extends PageObject{
	
	public boolean isAvailable() {
		
		try {
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), 30);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
			Thread.sleep(2000);
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			try {
				switchToDefaultFrame();
			} catch (Exception e) {
				// TODO: handle exception
			}
			return getElementVisibility("label_reservation_number_id", 60);		
		} catch (Exception e) {
			return false;
		}		
	}
	
	public HotelConfirmationPageDetails getInfo(HotelSearchObject search, HotelDetails selectedHotel) throws Exception
	{
		HotelConfirmationPageDetails 	info 	= new HotelConfirmationPageDetails();
		String[] 						transID	= DriverFactory.getInstance().getDriver().getCurrentUrl().split("transectionId");
		info.setTransactionID					(transID[1].split("-")[1]);
		info.setBookingReference				(getElement("label_booking_reference_id").getText().replace(":", "").trim());
		info.setReservationNumber				(getElement("label_reservation_number_id").getText().replace(":", "").trim());
		System.out.println("reservation number - ".concat(info.getReservationNumber()) );
		info.setHotelName						(getElement("label_hotel_name_id").getText());
		info.setHotelAddress					(getElement("label_hotel_address_id").getText());
		info.setStatus							(getElement("label_hotel_booking_status_id").getText());
		info.setSupplierConfirmationNumber		(getElement("label_sup_confirmation_number_xpath").getText());
		info.setChekin							(getElement("label_hotel_chekin_date_id").getText());
		info.setCheckout						(getElement("label_hotel_checkout_date_id").getText());
		info.setNumberOfRooms					(getElement("label_hotel_number_of_rooms_id").getWebElements().get(1).getText());
		info.setNumberOfNights					(getElement("label_hotel_number_of_nights_id").getWebElements().get(2).getText());
		info.setEstimatedArrivalTime			(getElement("label_hotel_estimated_arrival_time_id").getText());
		
		//get occupancy details
		ArrayList<HotelRoomDetails> roomlist = new ArrayList<HotelRoomDetails>();
		for(int i = 1 ; i <= Integer.parseInt(search.getRooms()) ; i++)
		{
			HotelRoomDetails room = new HotelRoomDetails();
			room.setRoom						(getElement("label_hotel_room_id").changeRefAndGetElement(getElement("label_hotel_room_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setRoomType					(getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setBedType						(getElement("label_hotel_bed_type_id").changeRefAndGetElement(getElement("label_hotel_bed_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setRatePlan					(getElement("label_hotel_rate_plan_id").changeRefAndGetElement(getElement("label_hotel_rate_plan_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(0).getText());
			room.setRate						(getElement("label_hotel_rate_plan_id").changeRefAndGetElement(getElement("label_hotel_rate_plan_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(1).getText());
			roomlist.add(room);
		}
		info.setRoom(roomlist);
		
		//get rate details
		info.setCurrencyCode					(getElement("label_selling_currency_id").getText());
		info.setHotelSubtotal					(getElement("label_hotel_sub_total_id").getText().replace(",", ""));
		info.setHotelTax						(getElement("label_hotel_total_tax_and_service_charges_id").getText());
		info.setHotelTotal						(getElement("label_hotel_total_id").getText().replace(",", ""));
		info.setSubtotal						(getElement("label_sub_total_id").getText().replace(",", ""));
		info.setTax								(getElement("label_total_tax_and_other_charges_id").getText());
		info.setTotal							(getElement("label_total_id").getText().replace(",", ""));
		info.setAmountBeingProcessedNow			(getElement("label_amount_being_processed_now_id").getText().replace(",", ""));
		info.setAmountDueAtCheckin				(getElement("label_amount_due_at_checkin_id").getText().replace(",", ""));
		
		//get customer details
		info.setFirstName						(getElement("label_customer_first_name_id").getText().replace(":", "").trim());
		info.setLastName						(getElement("label_customer_last_name_id").getText().replace(":", "").trim());
		info.setAddress1						(getElement("label_customer_address_id").getText().replace(":", "").trim());
		info.setCity							(getElement("label_customer_city_id").getText().replace(":", "").trim());
		info.setCountry							(getElement("label_customer_country_id").getText().replace(":", "").trim());
		info.setState							(getElement("label_customer_state_id").getText().replace(":", "").trim());
		info.setPostalCode						(getElement("label_customer_postal_code_id").getText().replace(":", "").trim());
		info.setPhoneNumber						(getElement("label_customer_phone_number_id").getText().replace(":", "").trim());
		info.setEmergencyNumber					(getElement("label_customer_emegency_phone_number_id").getText().replace(":", "").trim());
		info.setEmail							(getElement("label_customer_email_id").getText().replace(":", "").trim());
		
		ArrayList<hotelOccupancy> adultOccupancyList = new ArrayList<hotelOccupancy>();
		ArrayList<hotelOccupancy> childOccupancyList = new ArrayList<hotelOccupancy>();
		String[] adultCountHelper = search.getAdults().split("/");
		String[] childCountHelper = search.getChildren().split("/");
		int adultCnt = 0;
		int childCnt = 0;
		for(int i = 1 ; i <= Integer.parseInt(search.getRooms()) ; i++)
		{
			int adultCountPerRoom = Integer.parseInt(adultCountHelper[(i-1)]);
			int childCountPerRoom = Integer.parseInt(childCountHelper[(i-1)]);
			for(int j = 0 ; j < adultCountPerRoom ; j++)
			{
				hotelOccupancy occupancy = new hotelOccupancy();
				try {
					occupancy.setTitle				(getElement("label_occupancy_adult_title_id").changeRefAndGetElement(getElement("label_occupancy_adult_title_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setFirstname			(getElement("label_occupancy_adult_fname_id").changeRefAndGetElement(getElement("label_occupancy_adult_fname_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setLastname			(getElement("label_occupancy_adult_lname_id").changeRefAndGetElement(getElement("label_occupancy_adult_lname_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setSmoking			(getElement("label_occupancy_adult_smoking_id").changeRefAndGetElement(getElement("label_occupancy_adult_smoking_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setHandicap			(getElement("label_occupancy_adult_handicap_id").changeRefAndGetElement(getElement("label_occupancy_adult_handicap_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setWheelchair			(getElement("label_occupancy_adult_wheelchair_id").changeRefAndGetElement(getElement("label_occupancy_adult_wheelchair_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(adultCnt).getText());
					adultCnt++;
					adultOccupancyList.add(occupancy);
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
			
			for(int j = 0 ; j < childCountPerRoom ; j++)
			{
				hotelOccupancy occupancy = new hotelOccupancy();
				try {
					occupancy.setTitle				(getElement("label_occupancy_child_title_id").changeRefAndGetElement(getElement("label_occupancy_child_title_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setFirstname			(getElement("label_occupancy_child_fname_id").changeRefAndGetElement(getElement("label_occupancy_child_fname_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setLastname			(getElement("label_occupancy_child_lname_id").changeRefAndGetElement(getElement("label_occupancy_child_lname_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setHandicap			(getElement("label_occupancy_child_handicap_id").changeRefAndGetElement(getElement("label_occupancy_child_handicap_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					occupancy.setWheelchair			(getElement("label_occupancy_child_wheelchair_id").changeRefAndGetElement(getElement("label_occupancy_child_wheelchair_id").getRef().replace("replace", String.valueOf(i))).getWebElements().get(j).getText());
					childCnt++;
					childOccupancyList.add(occupancy);
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
		}
		
		info.setAdultOccupancy						(adultOccupancyList);
		info.setChildOccupancy						(childOccupancyList);
		info.setMerchantTrackId						(getElement("label_merchent_track_id_id").getText());
		info.setAuthenticationReference				(getElement("label_authentication_reference_id").getWebElements().get(1).getText());
		info.setPaymentID							(getElement("label_payment_id_id").getWebElements().get(2).getText());
		info.setAmountInPGCurrency					(getElement("label_amount_in_pg_currency_id").getText());
		//dateFormat format = new dateFormat();
		String 	checkin = com.rezrobot.utill.Calender.getDate("MM/dd/yyyy", "yyyy-MMM-dd" , search.getCheckin());
		try {
			info.setCancellationDeadline				(getElement("label_cancellation_deadline_xpath").changeRefAndGetElement(getElement("label_cancellation_deadline_xpath").getRef().replace("hotelid", selectedHotel.getHotelID()).replace("checkin", checkin).replace("supplier", search.getSupplier())).getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return info;
	}

	public HashMap<String, String> getHotelLabels(HotelDetails selectedHotel, HotelSearchObject search) throws Exception {

		HashMap<String, String> labels = new HashMap<>();
		try {
			try {
				labels.put("confirmationStatus", 					getElement("label_confirmation_status_xpath").getText());
				labels.put("confirmationStatusBookingRefernce", 	getElement("label_booking_reference_label_xpath").getText());
				labels.put("confirmationStatusReservationNo", 		getElement("label_reservation_number_label_xpath").getText());
				labels.put("confirmationStatusPrint", 				getElement("label_please_print_this_page_for_your_records_label_xpath").getText());
				labels.put("confirmationStatusConfirmationEmail", 	getElement("label_confirmation_mail_has_been_sent_xpath").getText());
				labels.put("confirmationStatusDoNoRecieve", 		getElement("label_donot_recieve_mail_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				labels.put("bookingItenaryInfo", 					getElement("label_booking_itenery_info_xpath").getText());
				labels.put("bookingItenaryInfoStatus", 				getElement("label_status_hotel_label_xpath").getText());
				labels.put("bookingItenaryInfoCheckin", 			getElement("label_checkin_date_label_xpath").getText());
				labels.put("bookingItenaryInfocheckout", 			getElement("label_checkout_date_label_xpath").getText());
				labels.put("bookingItenaryInfoNumberOfRooms", 		getElement("label_number_of_rooms_label_xpath").getText());
				labels.put("bookingItenaryInfoNumberOfNights", 		getElement("label_number_of_nights_label_xpath").getText());
				labels.put("bookingItenaryInfoEstimatedTime", 		getElement("label_estimated_arrival_time_xpath").getText());
				labels.put("bookingItenaryInfoRoom", 				getElement("label_hotel_room_label_xpath").getText());
				labels.put("bookingItenaryInfoRoomType", 			getElement("label_hotel_room_type_label_xpath").getText());
				labels.put("bookingItenaryInfoBedType", 			getElement("label_hotel_bed_type_label_xpath").getText());
				labels.put("bookingItenaryInforatePlan", 			getElement("label_hotel_rate_plan_label_xpath").getText());
				labels.put("bookingItenaryInfoRoomRate", 			getElement("label_hotel_room_rate_label_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				for(int i = 2 ; i < 6 ; i++)
				{
					try {
						if("Sub Total".equals(getElement("label_hotel_sub_total_label_xpath").changeRefAndGetElement(getElement("label_hotel_sub_total_label_xpath").getRef().replace("replace", String.valueOf(i))).getText()))
						{
							labels.put("bookingItenaryInfoHotelSubTotal", 		getElement("label_hotel_sub_total_label_xpath").changeRefAndGetElement(getElement("label_hotel_sub_total_label_xpath").getRef().replace("replace", String.valueOf(i))).getText());
							labels.put("bookingItenaryInfoHotelTotalTax", 		getElement("label_hotel_total_tax_and_service_charges_label_xpath").changeRefAndGetElement(getElement("label_hotel_total_tax_and_service_charges_label_xpath").getRef().replace("replace", String.valueOf(i))).getText());
							labels.put("bookingItenaryInfoHotelTotal", 			getElement("label_hotel_label_xpath").changeRefAndGetElement(getElement("label_hotel_label_xpath").getRef().replace("replace", String.valueOf(i))).getText());
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				labels.put("bookingItenaryInfoSubTotal", 			getElement("label_sub_total_label_xapth").getText());
				labels.put("bookingItenaryInfoTotalTaxes", 			getElement("label_total_taxes_and_other_charges_label_xapth").getText());
				labels.put("bookingItenaryInfoTotal", 				getElement("label_total_label_xapth").getText());
				labels.put("bookingItenaryInfoAmountProcessedNow", 	getElement("label_amount_being_processed_now_label_xapth").getText());
				labels.put("bookingItenaryInfoDueAtCheckin", 		getElement("label_amount_due_at_checkin_label_xapth").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				labels.put("userInfo", 								getElement("label_user_information_label").getText());
				labels.put("userInfoFirstName", 					getElement("label_user_info_first_name_label_xpath").getText());
				labels.put("userInfoAddress1", 						getElement("label_user_info_address1_label_xpath").getText());
				labels.put("userInfoCountry", 						getElement("label_user_info_country_label_xpath").getText());
				labels.put("userInfoPostalCode", 					getElement("label_user_info_postal_code_label_xpath").getText());
				labels.put("userInfoEmergencynumber", 				getElement("label_user_info_emergency_number_label_xpath").getText());
				labels.put("userInfoLastName", 						getElement("label_user_info_last_name_label_xpath").getText());
				labels.put("userInfoCity", 							getElement("label_user_info_city_label_xpath").getText());
				labels.put("userInfoState", 						getElement("label_user_info_state_label_xpath").getText());
				labels.put("userInfoPoneNumber", 					getElement("label_user_info_phone_number_label_xpath").getText());
				labels.put("userInfoEmail", 						getElement("label_user_info_email_label_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				labels.put("roomsOccupancy", 						getElement("label_room_occupancy_details_label_xpath").getText());
				labels.put("roomsOccupancyTitle", 					getElement("label_room_occupancy_details_title_label_xpath").getText());
				labels.put("roomsOccupancyFirstName", 				getElement("label_room_occupancy_details_first_name_label_xpath").getText());
				labels.put("roomsOccupancyLastName", 				getElement("label_room_occupancy_details_last_name_label_xpath").getText());
				labels.put("roomsOccupancySpecialReq", 				getElement("label_room_occupancy_details_special_requests_label_xpath").getText());
				labels.put("roomsOccupancySmoking", 				getElement("label_room_occupancy_details_smorking_label_xpath").getText());
				labels.put("roomsOccupancyhandicap", 				getElement("label_room_occupancy_details_handicap_label_xpath").getText());
				labels.put("roomsOccupancyWheelChair", 				getElement("label_room_occupancy_details_wheelchair_label_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				System.out.println(getElement("label_user_information_label").getText());
				labels.put("billingInfo", 							getElement("label_biiling_info_title_xpath").getText());
				labels.put("billingInfoMerchant", 					getElement("label_billing_info_merchant_track_id_label_xpath").getText());
				labels.put("billingInfoAuthentication", 			getElement("label_billing_info_athentication_reference_label_xpath").getText());
				labels.put("billingInfoPaymentId", 					getElement("label_billing_info_payment_id_label_xpath").getText());
				String [] temp = getElement("label_billing_info_amount_label_xpath").getText().split(" ");
				labels.put("billingInfoAmount", 					temp[0]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				labels.put("amendements", 							getElement("label_amendments_xpath").getText());
				try {
					labels.put("amendementsLastDate", 				getElement("label_hotel_cancellation_deadline_id").getText());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				//.//*[@id='hotelcanpolicy_BAG_LON@@2016-Sep-06@@INV27']/div/div[3]/p
				//.//*[@id='hotelcanpolicy_BAG_LON@@2016-Oct-22@@INV27']/div/div[3]/p
				String today = Calender.getDate("MM/dd/yyyy", "yyyy-MMM-dd", search.getCheckin());
				labels.put("amendementsIndicatedPolicies", 			getElement("label_indicated_policy_xpath").changeRefAndGetElement(getElement("label_indicated_policy_xpath").getRef().replace("187490-1", selectedHotel.getHotelID()).replace("2016-Aug-18", today).replace("hotelbeds_v2", selectedHotel.getSupplier())).getText());
				labels.put("amendementsFullyRefundable", 			getElement("label_fully_refundable_date_xpath").getText());
				labels.put("amendementsGenaralTerms", 				getElement("label_general_terms_and_conditions_xpath").getText());
				labels.put("amendementsChargeAndCancellation", 		getElement("label_charge_and_cancellation_fees_xpath").getText());
				labels.put("amendementsAvailabilityAtTime", 		getElement("label_availability_at_time_xpath").getText());
				labels.put("amendementsIndividualConditions", 		getElement("label_the_individual_conditions_xpath").getText());
				labels.put("amendementsPortalConditions", 			getElement("label_portal_conditions_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return labels;
	}
}
