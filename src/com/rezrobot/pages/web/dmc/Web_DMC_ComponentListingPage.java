package com.rezrobot.pages.web.dmc;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.pages.web.common.Web_Com_PaymentsPage;
import com.rezrobot.utill.DriverFactory;

public class Web_DMC_ComponentListingPage extends PageObject {

	public boolean isDMCComponentListingpageavailable() {

		try {
			switchToDefaultFrame();
			return getElementVisibility("btn_booknow_id", 100);
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public Web_Com_PaymentsPage bookThepackage() throws Exception {

		getElement("btn_booknow_id").click();
		Web_Com_PaymentsPage PaymentsPage = new Web_Com_PaymentsPage();

		return PaymentsPage;

	}

	public HashMap<String, String> getselectedPackageflightdetails() {
		HashMap<String, String> values = new HashMap<>();

		try {

			// values.put("OutBound Flight Logo", getElement("img_departureFlightLogo_xpath").getAttribute("src"));
			values.put("OutBound Flight Number", getElement("txt_outboundflightno_id").getText());
			values.put("OutBound Flight Departure Date", getElement("txt_outbounddepartureDate_id").getText());
			values.put("OutBound Flight Departure Time", getElement("txt_outbounddepartureTime_id").getText());
			values.put("OutBound Flight Departure Airport Code", getElement("txt_outbounddepartureairportcode_id").getText());
			values.put("OutBound Flight Departure City", getElement("txt_outbounddepartureairport_id").getText());
			values.put("OutBound Flight Arrival Time", getElement("txt_outboundarrivalTime_id").getText());
			values.put("OutBound Flight Arrival Airport Code", getElement("txt_outboundarrivalairportCode_id").getText());
			values.put("OutBound Flight Arrival City", getElement("txt_outboundarrivalairport_id").getText());
			values.put("OutBound Flight Duration", getElement("txt_outboundduration_id").getText());
			values.put("OutBound Flight Numberof Stops", getElement("txt_outboundnumberofStops_id").getText());

			values.put("Outbound Flight depature Airline Name", getElement("text_outbounddepartureairlinename_xpath").getText());

			values.put("InBound Flight Logo", getElement("img_arrivalFlightLogo_xpath").getAttribute("src"));
			values.put("InBound Flight Number", getElement("txt_inboundflightno_id").getText());
			values.put("InBound Flight Arrival Date", getElement("txt_inboundarrivalDate_id").getText());
			values.put("InBound Flight Arrival Time", getElement("txt_inboundarrivaltime_id").getText());
			values.put("InBound Flight Arrival Airport Code", getElement("txt_inboundarrivalairportCode_id").getText());
			values.put("InBound Flight Arrival City", getElement("txt_inboundarrivalairport_id").getText());
			values.put("InBound Flight Departure Time", getElement("txt_inbounddepartureTime_id").getText());
			values.put("InBound Flight Departure Air Port Code", getElement("txt_inbounddepartureairportCode_id").getText());
			values.put("InBound Flight Departure City", getElement("txt_inbounddepartureairport_id").getText());
			values.put("InBound Flight Duration", getElement("txt_inboundduration_id").getText());
			values.put("InBound Flight Numberof Stops", getElement("txt_inboundnumberofStops_id").getText());
			values.put("InBound Flight depature Airline Name", getElement("text_InbounArrivalairlinename_xpath").getText());

			System.out.println(values);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("get selsected flight details error - " + e);
			// TODO: handle exception
		}
		try {
			String tracer = DriverFactory.getInstance().getDriver().findElement(By.id("airridetracer")).getAttribute("value");
			// String contents = (String)((JavascriptExecutor)DriverFactory.getInstance().getDriver()).executeScript("return arguments[0].innerHTML;", element);
			System.out.println("flight tracer id - " + tracer);
			values.put("flightTracer", tracer);

			// String abc = (String) ((JavascriptExecutor)DriverFactory.getInstance().getDriver()).executeScript("document.getElementById('flight-tracer-id').innerHTML;");
			// string descriptionTextXPath = "//div[contains(@class, 'description')]/h3";
			System.out.println(DriverFactory.getInstance().getDriver().findElement(By.id("vac_flight_WJ_1")).findElements(By.tagName("p")).size());
			System.out.println(DriverFactory.getInstance().getDriver().findElement(By.xpath("//div[contains(@id, 'vac_flight_WJ_1')]/span")).getText());
			System.out.println(DriverFactory.getInstance().getDriver().findElements(By.tagName("span")).size());
			for (int i = 0; i < DriverFactory.getInstance().getDriver().findElement(By.id("vac_flight_WJ_1")).findElements(By.tagName("span")).size(); i++) {
				System.out.println(DriverFactory.getInstance().getDriver().findElement(By.id("vac_flight_WJ_1")).findElements(By.tagName("span")).get(i).getText());
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		System.out.println(values.get("flightTracer"));
		return values;

	}

	public HashMap<String, String> getPackageCommonProperties() throws Exception {

		HashMap<String, String> values = new HashMap<>();

		values.put("PackageTotalPrice", getElement("txt_PackageTotalPrice_id").getText());

		System.out.println(values.get("PackageTotalPrice"));

		return values;

	}

}
