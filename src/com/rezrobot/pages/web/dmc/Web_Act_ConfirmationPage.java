package com.rezrobot.pages.web.dmc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.ActivityDetails;

public class Web_Act_ConfirmationPage extends PageObject{

	public boolean isConfirmationPageAvailable(){
		switchToDefaultFrame();
		getElementVisibility("div_ConfirmationPageLoaded_classname", 120);
		return super.isPageAvailable("div_ConfirmationPageLoaded_classname");
	}
	
	public HashMap<String, String> getConfirmationPageLabels() {

		HashMap<String, String> labels = new HashMap<>();
				
		try {
			labels.put("l_Confirmationstatus", getLabelName("label_Confirmationstatus_xpath"));
			labels.put("l_BookingReference", getLabelName("label_BookingReference_xpath"));
			labels.put("l_ReservationNo", getLabelName("label_ReservationNo_xpath"));
			labels.put("l_PrintRecords", getLabelName("label_PrintRecords_xpath"));
			labels.put("l_ConfirmationMailSendMail", getLabelName("label_ConfirmationMailSendMail_xpath"));
			labels.put("l_receiveMailText", getLabelName("label_receiveMailText_xpath"));
			labels.put("l_PortalTel", getLabelName("label_PortalTel_xpath"));
			labels.put("l_PortalFax", getLabelName("label_PortalFax_xpath"));
			
			labels.put("l_BookingItineraryInformation", getLabelName("label_BookingItineraryInformation_xpath"));
			labels.put("l_BookingStatus", getLabelName("label_BookingStatus_xpath"));
			labels.put("l_RateType", getLabelName("label_RateType_xpath"));
			labels.put("l_Rate", getLabelName("label_Rate_xpath"));
			labels.put("l_Qty", getLabelName("label_Qty_xpath"));
			labels.put("l_UsableOnTable", getLabelName("label_UsableOnTable_xpath"));
			labels.put("l_ActivityRate", getLabelName("label_ActivityRate_xpath"));
			labels.put("l_SubTotal", getLabelName("label_SubTotal_xpath"));
			labels.put("l_TaxCharges", getLabelName("label_TaxCharges_xpath"));
			labels.put("l_Total", getLabelName("label_Total_xpath"));
			labels.put("l_D_SubTotal", getLabelName("label_D_SubTotal_xpath"));
			labels.put("l_D_TotalTaxes", getLabelName("label_D_TotalTaxes_xpath"));
			labels.put("l_D_Total", getLabelName("label_D_Total_xpath"));
			labels.put("l_D_AmountBeingProcessedNow", getLabelName("label_D_AmountBeingProcessedNow_xpath"));
			labels.put("l_D_AmountDue", getLabelName("label_D_AmountDue_xpath"));
			
			labels.put("l_UserInfomation", getLabelName("label_UserInfomation_xpath"));
			labels.put("l_FirstName", getLabelName("label_FirstName_xpath"));
			labels.put("l_LastName", getLabelName("label_LastName_xpath"));
			labels.put("l_Address1", getLabelName("label_Address1_xpath"));
			labels.put("l_City", getLabelName("label_City_xpath"));
			labels.put("l_Country", getLabelName("label_Country_xpath"));
			labels.put("l_State", getLabelName("label_State_xpath"));
			labels.put("l_PostalCode", getLabelName("label_PostalCode_xpath"));
			labels.put("l_PhoneNumber", getLabelName("label_PhoneNumber_xpath"));
			labels.put("l_EmergencyNo", getLabelName("label_EmergencyNo_xpath"));
			labels.put("l_Email", getLabelName("label_Email_xpath"));
			labels.put("l_ActivityOccupancyDetails", getLabelName("label_ActivityOccupancyDetails_xpath"));
			labels.put("l_Occupancy_Title", getLabelName("label_Occupancy_Title_xpath"));
			labels.put("l_Occupancy_FName", getLabelName("label_Occupancy_FName_xpath"));
			labels.put("l_Occupancy_LName", getLabelName("label_Occupancy_LName_xpath"));
			labels.put("l_Occupancy_Adult1", getLabelName("label_Occupancy_Adult1_xpath"));		
		
			labels.put("l_BillingInformationCredit", getLabelName("label_BillingInformationCredit_xpath"));
			labels.put("l_MerchantTrackID", getLabelName("label_MerchantTrackID_xpath"));
			labels.put("l_AuthenticationReference", getLabelName("label_AuthenticationReference_xpath"));
			labels.put("l_PaymentID", getLabelName("label_PaymentID_xpath"));
			labels.put("l_Amount", getLabelName("label_Amount_xpath"));
			labels.put("l_AmendmentAndCancellation", getLabelName("label_AmendmentAndCancellation_xpath"));
			labels.put("l_TC_FullyreRundableifCancelled", getLabelName("label_TC_FullyreRundableifCancelled_xpath"));
			labels.put("l_TC_GeneralTermsAndCondition", getLabelName("label_TC_GeneralTermsAndCondition_xpath"));
			labels.put("l_TC_ChargeAndCancellationFeesApply", getLabelName("label_TC_ChargeAndCancellationFeesApply_xpath"));
			labels.put("l_TC_AvailabilityAtTimeOfMaking", getLabelName("label_TC_AvailabilityAtTimeOfMaking_xpath"));
			labels.put("l_TC_TheIndividualConditions", getLabelName("label_TC_TheIndividualConditions_xpath"));
			labels.put("l_TC_TermsConditionsApply", getLabelName("label_TC_TermsConditionsApply_xpath"));
						
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return labels;
	}
	
	public ActivityDetails getConfirmationInfoDetails(ActivityDetails activityDetails){
		
		try {
			
			activityDetails.setConfirmation_BookingRefference(getElement("text_confirmation_BookingRefference_xpath").getText().split(": ")[1]);
			activityDetails.setReservationNo(getElement("text_reservationNo_xpath").getText().split(": ")[1]);
			activityDetails.setConfirmationPageLoaded(true);
			activityDetails.setConfirmation_CusMailAddress(getElement("text_confirmation_CusMailAddress_xpath").getText());
			activityDetails.setConfirmation_PortalTel(getElement("text_confirmation_PortalTel_xpath").getText());
			activityDetails.setConfirmation_PortalFax(getElement("text_confirmation_PortalFax_xpath").getText());
			activityDetails.setConfirmation_PortalEmail(getElement("text_confirmation_PortalEmail_xpath").getText());
			
			activityDetails.setConfirmation_BI_ActivityName(getElement("text_confirmation_BI_ActivityName_xpath").getText());
			activityDetails.setConfirmation_BI_City(getElement("text_confirmation_BI_City_xpath").getText());
			activityDetails.setConfirmation_BI_ActType(getElement("text_confirmation_BI_ActType_xpath").getText());
			activityDetails.setConfirmation_BI_BookingStatus(getElement("text_confirmation_BI_BookingStatus_xpath").getText());		
			activityDetails.setConfirmation_BI_RateType(getElement("text_confirmation_BI_RateType_xpath").getText());
			activityDetails.setConfirmation_BI_DailyRate(getElement("text_confirmation_BI_DailyRate_xpath").getText());
			activityDetails.setConfirmation_BI_QTY(getElement("text_confirmation_BI_QTY_xpath").getText());
			activityDetails.setConfirmation_BI_UsableOnDate(getElement("text_confirmation_BI_UsableOnDate_xpath").getText());
			
			activityDetails.setConfirmation_BI_Currency1(getElement("text_confirmation_BI_Currency1_xpath").getText());		
			activityDetails.setConfirmation_BI_Currency2(getElement("text_confirmation_BI_Currency2_xpath").getText());
			activityDetails.setConfirmation_BI_Currency3(getElement("text_confirmation_BI_Currency3_xpath").getText());
			activityDetails.setConfirmation_BI_ActiivtyRate(getElement("text_confirmation_BI_ActiivtyRate_xpath").getText());		
			activityDetails.setConfirmation_BI_SubTotal_1(getElement("text_confirmation_BI_SubTotal_1_xpath").getText());
			activityDetails.setConfirmation_BI_Tax(getElement("text_confirmation_BI_Tax_xpath").getText());
			activityDetails.setConfirmation_BI_Total(getElement("text_confirmation_BI_Total_xpath").getText());		
			activityDetails.setConfirmation_BI_SubTotal_2(getElement("text_confirmation_BI_SubTotal_2_xpath").getText());
			activityDetails.setConfirmation_BI_TotalTaxWTTax(getElement("text_confirmation_BI_TotalTaxWTTax_xpath").getText());
			activityDetails.setConfirmation_BI_TotalBooking(getElement("text_confirmation_BI_TotalBooking_xpath").getText());		
			activityDetails.setConfirmation_BI_AmountNow(getElement("text_confirmation_BI_AmountNow_xpath").getText());
			activityDetails.setConfirmation_BI_AmountDue(getElement("text_confirmation_BI_AmountDue_xpath").getText());
			
			activityDetails.setConfirmation_FName(getElement("text_confirmation_FName").getText().split(": ")[1]);	
			activityDetails.setConfirmation_LName(getElement("text_confirmation_LName").getText().split(": ")[1]);
			activityDetails.setConfirmation_TP(getElement("text_confirmation_TP").getText().split(": ")[1]);
			activityDetails.setConfirmation_Email(getElement("text_confirmation_Email").getText().split(": ")[1]);		
			activityDetails.setConfirmation_address(getElement("text_confirmation_address").getText().split(": ")[1]);
			activityDetails.setConfirmation_country(getElement("text_confirmation_country").getText().split(": ")[1]);
			activityDetails.setConfirmation_city(getElement("text_confirmation_city").getText().split(": ")[1]);		
			activityDetails.setConfirmation_State(getElement("text_confirmation_State").getText().split(": ")[1]);
			activityDetails.setConfirmation_postalCode(getElement("text_confirmation_postalCode").getText().split(": ")[1]);
			
			activityDetails.setConfirmation_AODActivityName(getElement("text_confirmation_AODActivityName").getText());	
			activityDetails.setConfirmationPage_cusTitle(getElement("text_confirmationPage_cusTitle").getText());
			activityDetails.setConfirmationPage_cusFName(getElement("text_confirmationPage_cusFName").getText());
			activityDetails.setConfirmationPage_cusLName(getElement("text_confirmationPage_cusLName").getText());		
			activityDetails.setConfirmation_CardDetails_MerchantTrackID(getElement("text_confirmation_CardDetails_MerchantTrackID").getText());
			activityDetails.setConfirmation_CardDetails_AuthRef(getElement("text_confirmation_CardDetails_AuthRef").getText());
			activityDetails.setConfirmation_CardDetails_PayId(getElement("text_confirmation_CardDetails_PayId").getText());		
			activityDetails.setConfirmation_CardDetails_Amount(getElement("text_confirmation_CardDetails_Amount").getText());
			
			WebElement ele2 = new UIElement().getinnerWebElement(getElement("div_ActiivtyNameFromTerms_classname").getWebElement(), getElement("div_getActNameDIV"));
			String id = ele2.getAttribute("id").split("cxlpolicy_")[1];
			
			UIElement activityNameElement = getElement("text_confirmation_Cancel_ActName").changeRefAndGetElement(getElement("text_confirmation_Cancel_ActName").getRef().replace("XXX", id));
			activityDetails.setConfirmation_Cancel_ActName(activityNameElement.getText().split(" : ")[1]);
			
			UIElement cancelDeadlineElement = getElement("text_confirmation_Cancel_CancellationDead_1").changeRefAndGetElement(getElement("text_confirmation_Cancel_CancellationDead_1").getRef().replace("XXX", id));
			activityDetails.setConfirmation_Cancel_CancellationDead_1(cancelDeadlineElement.getText().split(" : ")[1]);
			
			activityDetails.setConfirmation_Cancel_CancellationDead_2(getElement("text_confirmation_Cancel_CancellationDead_2").getText());
			
			ArrayList<String> confirmationPageCancelPolicy = new ArrayList<String>();
			
			UIElement cancelElement = getElement("text_confirmation_Cancel_CancelPolicy").changeRefAndGetElement(getElement("text_confirmation_Cancel_CancelPolicy").getRef().replace("XXX", id));
			List<WebElement> cancellationList = new UIElement().getInnerWebElements(cancelElement.getWebElement(),  getElement("text_liTestFromUi"));
			
			for (int i = 0; i < cancellationList.size(); i++) {

				String cancelPolicies = cancellationList.get(i).getText();
				confirmationPageCancelPolicy.add(cancelPolicies);
			}
			
			activityDetails.setConfirmation_Cancel_CancelPolicy(confirmationPageCancelPolicy);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
	}
}
