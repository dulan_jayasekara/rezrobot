package com.rezrobot.pages.web.common;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.Common_verify_selected_products;
import com.rezrobot.processor.LabelReadProperties;

public class Web_Com_Verify_selected_products extends PageObject{

	public boolean isVerifyProductsAvailable()
	{
		try {
			//switchToDefaultFrame();
			return getelementAvailability("button_complete_xpath");		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}	
	}
	
	public ArrayList<Common_verify_selected_products> getDetails(LabelReadProperties labelProperty) throws Exception
	{
		HashMap<String, String> labelPropertyMap = labelProperty.getHotelLabelProperties();
		ArrayList<Common_verify_selected_products> productList = new ArrayList<Common_verify_selected_products>();
		int productcount = 0;
		System.out.println("verify selected products - get the product count");
		Thread.sleep(2000);
		for(int i = 1 ; i <=5 ; i++)
		{
			try {
				getElement("label_name_xpath").changeRefAndGetElement(getElement("label_name_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", "1")).getText();
				productcount++;
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}	
		}
		System.out.println("Product count - " + productcount);
		Thread.sleep(2000);
		for(int i = 1 ; i <= productcount ; i++)
		{
			for(int j = 2 ; j <= 2 ; j++)
			{
				System.out.println("verify selected products - Extracting" + (j-1));
				Common_verify_selected_products product = new Common_verify_selected_products();
				try {
					if(getElement("label_name_xpath").changeRefAndGetElement(getElement("label_name_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", "1")).getText().equals(labelPropertyMap.get("verifyProductsHotel")))
					{
						product.setProduct("Hotel");
					}
					else if(getElement("label_name_xpath").changeRefAndGetElement(getElement("label_name_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", "1")).getText().equals(labelPropertyMap.get("verifyProductsActivity")))
					{
						product.setProduct("Activity");
					}
					else if(getElement("label_name_xpath").changeRefAndGetElement(getElement("label_name_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", "1")).getText().equals(labelPropertyMap.get("verifyProductsFlight")))
					{
						product.setProduct("Flight");
					}
					
					product.setName(getElement("label_name_xpath").changeRefAndGetElement(getElement("label_name_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", String.valueOf(j))).getText());
					product.setStatus(getElement("label_status_xpath").changeRefAndGetElement(getElement("label_status_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", String.valueOf(j))).getText());
					product.setMessage(getElement("label_messge_xpath").changeRefAndGetElement(getElement("label_messge_xpath").getRef().replace("r1", String.valueOf(i)).replace("r2", String.valueOf(j))).getText());
					productList.add(product);
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
		}
		System.out.println("verify selected products - extracted product details");
		return productList;
	}
	
	public void close() throws Exception
	{
		
		getElement("button_close_xpath").click();
	}
	
	public void continueToPG() throws Exception
	{
		for(int i = 0 ; i < 10 ; i++)
		{
			Thread.sleep(10000);
			try {
				getElement("button_complete_xpath").click();
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		System.out.println("verify");
		
		/*try {
			executejavascript("javaScript:hideandSubmitThirdParty('false');");
		} catch (Exception e) {
			// TODO: handle exception
		}*/
	}
}
