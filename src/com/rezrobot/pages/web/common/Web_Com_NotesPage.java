package com.rezrobot.pages.web.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.ActivityDetails;

public class Web_Com_NotesPage extends PageObject{
	
	public boolean isSpecialNotesAvailable() throws InterruptedException{
		System.out.println("Notes avaialbility");
		switchToDefaultFrame();
		Thread.sleep(1000);
		return super.isPageAvailable("label_CustomerNoteLabel_xpath");
	}
	
	public HashMap<String, Boolean> getActivity_AvailabilityOfInputFields(){
		
		HashMap<String, Boolean> fieldsMap = new HashMap<>();
		
		try {
			fieldsMap.put("IN_customerNote", getElementVisibility("inputfield_customerNote_id"));				
			fieldsMap.put("IN_ActivityBillingInfo_Name_DropDown", getElementVisibility("combobox_ActivityBillingInfo_Name_DropDown_id"));
			fieldsMap.put("IN_ProgramNotes", getElementVisibility("inputfield_ActivityProgramNotes_id"));
			fieldsMap.put("IN_Notes_PrevStep", getElementVisibility("button_Notes_PrevStep_xpath"));
			fieldsMap.put("IN_Notes_ProceedToTerms", getElementVisibility("button_Notes_ProceedToTerms_xpath"));
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fieldsMap;
	}
	
	public HashMap<String, Boolean> getHotelFieldAvailability(){
		
		HashMap<String, Boolean> fieldsMap = new HashMap<>();
		try {
			fieldsMap.put("customerNotes", getElementVisibility("inputfield_customerNote_id"));
			fieldsMap.put("hotelList", getElementVisibility("combobox_hotel_list_xpath"));
			fieldsMap.put("hotelNotes", getElementVisibility("inputfield_hotel_notes_xpath"));
			fieldsMap.put("Navigate_to_previous_step", getElementVisibility("button_Notes_PrevStep_xpath"));
			fieldsMap.put("Proceed_to_next_step", getElementVisibility("button_Notes_ProceedToTerms_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return fieldsMap;
	}
	
	public HashMap<String, String> getNotesLabels(){
		
		HashMap<String, String> labelsMap = new HashMap<>();
		
		try {			
			labelsMap.put("L_CustomerNoteLabel", getLabelName("label_CustomerNoteLabel_xpath"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return labelsMap;
	}
	
	public void enterHotelNotes(HashMap<String, String> portalSpecificData) throws Exception
	{
		getElement("inputfield_customerNote_id").setText(portalSpecificData.get("customerNotes"));
		getElement("inputfield_hotel_notes_xpath").setText(portalSpecificData.get("hotelNotes"));
	}
	
	public HashMap<String, String> getHotelNotesLabels(){
		HashMap<String, String> labelsMap = new HashMap<>();
		try {
			labelsMap.put("customer_notes", getLabelName("label_CustomerNoteLabel_xpath"));
			labelsMap.put("hotel_notes", getLabelName("label_hotel_notes_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labelsMap;
	}
	
	public ActivityDetails enterActivityNotes(ActivityDetails activityDetails){
		
		try {
			String cusNotes = "I am wanting to generate a random string of 20 characters without " + "using apache classes. I don't really care about whether is "
					+ "alphanumeric or not. Also, I am going to convert it to an array of bytes later FYI";
			getElement("inputfield_customerNote_id").setText(cusNotes);
			activityDetails.setPaymentsPage_N_CusNotes(cusNotes);
			
			String actNotes = "Activity notes displaying here.";
			getElement("inputfield_ActivityProgramNotes_id").setText(actNotes);
			activityDetails.setPaymentsPage_N_ActivityNotes(actNotes);
						
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
	}
	
	public Web_Com_TermsPage loadTermsandConditions() throws Exception{
		getElements("button_Notes_ProceedToTerms_classname").get(2).click();
		Web_Com_TermsPage termsPage = new Web_Com_TermsPage();
		return termsPage;		
	}

}
