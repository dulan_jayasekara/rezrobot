package com.rezrobot.pages.web.common;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.Activity_WEB_PaymentDetails;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.pages.web.activity.Web_Act_QuotationConfirmationPage;
import com.rezrobot.pojo.ComboBox;

public class Web_Com_CustomerDetailsPage extends PageObject{
	
	public boolean isCustomerDetailsAvailable(){
		System.out.println("Customer details avaialbility");
		switchToDefaultFrame();
		return super.isPageAvailable("label_customertitle_id");
	}
	
	public HashMap<String, Boolean> getCustomerDetailsInputFields(){
		
		HashMap<String, Boolean> fieldsMap = new HashMap<>();
		
		try {		
			fieldsMap.put("IN_customertitle", getElementVisibility("combobox_customertitle_id"));
			fieldsMap.put("IN_customerFirst", getElementVisibility("inputfield_customer_first_name_id"));
			fieldsMap.put("IN_customer_last", getElementVisibility("inputfield_customer_last_name_id"));
			fieldsMap.put("IN_customer_address", getElementVisibility("inputfield_customer_address_id"));
			fieldsMap.put("IN_customer_city", getElementVisibility("inputfield_customer_city_id"));
			fieldsMap.put("IN_customer_country", getElementVisibility("combobox_customer_country_id"));
			fieldsMap.put("IN_customer_State", getElementVisibility("combobox_customer_State_id"));
			fieldsMap.put("IN_customer_PostalCode", getElementVisibility("inputfield_customer_PostalCode_id"));
			fieldsMap.put("IN_Phonenumber_Areacode", getElementVisibility("inputfield_customer_Phonenumber_Areacode_name_id"));
			fieldsMap.put("IN_Phone_number", getElementVisibility("inputfield_customer_Phone_number_name_id"));
			fieldsMap.put("IN_MobileNumber_Areacode", getElementVisibility("inputfield_customer_MobileNumber_Areacode_name_id"));
			fieldsMap.put("IN_customer_MobileNumber", getElementVisibility("inputfield_customer_MobileNumber_name_id"));			
			fieldsMap.put("IN_customer_Email", getElementVisibility("inputfield_customer_Email_id"));
			fieldsMap.put("IN_customer_VerifyEmail", getElementVisibility("inputfield_customer_VerifyEmail_id"));
			fieldsMap.put("IN_customer_phone_number", getElementVisibility("radiobutton_customer_phone_number_id"));
			fieldsMap.put("IN_customer_mobile_number", getElementVisibility("radiobutton_customer_mobile_number_id"));
			fieldsMap.put("IN_Savequotation", getElementVisibility("button_Savequotation_xpath"));
			fieldsMap.put("IN_ProceedToOccupancy", getElementVisibility("button_ProceedToOccupancy_xpath"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fieldsMap;
	}
	
	public HashMap<String, String> getCustomerDetailsPageLabels() {
		
		HashMap<String, String> labelMap = new HashMap<>();
		
		try {			
			labelMap.put("L_customertitle", getLabelName("label_customertitle_id"));
			labelMap.put("L_customerfirstname", getLabelName("label_customerfirstname_id"));
			labelMap.put("L_customerlastname", getLabelName("label_customerlastname_id"));
			labelMap.put("L_customeraddress", getLabelName("label_customeraddress_id"));
			labelMap.put("L_customercity", getLabelName("label_customercity_id"));
			labelMap.put("L_customercountry", getLabelName("label_customercountry_id"));			
			labelMap.put("L_customerState", getLabelName("label_customerState_id"));			
			labelMap.put("L_customerPostalCode", getLabelName("label_customerPostalCode_id"));			
			labelMap.put("L_customerPhone_number", getLabelName("label_customerPhone_number_id"));		
			labelMap.put("L_customerMobileNumber", getLabelName("label_customerMobileNumber_id"));		
			labelMap.put("L_customerfEmail", getLabelName("label_customerfEmail_id"));		
			labelMap.put("L_customerVerifyEmail", getLabelName("label_customerVerifyEmail_id"));		
			labelMap.put("L_SelectOneAsTheECM", getLabelName("label_SelectOneAsTheECM_id"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return labelMap;
	}
	
	public HashMap<String, Boolean> getCustomerDetailsMandatoryFields(){
		
		HashMap<String, Boolean> mandatoryMap = new HashMap<>();
		
		try {
			mandatoryMap.put("Man_customertitle", getLabelMandatoryStatus("label_customertitle_id"));
			mandatoryMap.put("Man_customerfirstname", getMandatoryStatusFromDataConstraints("inputfield_customer_first_name_id"));
			mandatoryMap.put("Man_customerlastname", getMandatoryStatusFromDataConstraints("inputfield_customer_last_name_id"));
			mandatoryMap.put("Man_customeraddress", getMandatoryStatusFromDataConstraints("inputfield_customer_address_id"));
			mandatoryMap.put("Man_customercity", getMandatoryStatusFromDataConstraints("inputfield_customer_city_id"));
			mandatoryMap.put("Man_customercountry", getComboBoxMandatoryStatusFromDataConstraints("combobox_customer_country_id"));
			mandatoryMap.put("Man_customerState", getComboBoxMandatoryStatusFromDataConstraints("combobox_customer_State_id"));
			mandatoryMap.put("Man_customerPostalCode", getMandatoryStatusFromDataConstraints("inputfield_customer_PostalCode_id"));
			mandatoryMap.put("Man_customerPhone_number", getMandatoryStatusFromDataConstraints("inputfield_customer_Phone_number_name_id"));
			mandatoryMap.put("Man_customerPhone_numberAreaCode", getMandatoryStatusFromDataConstraints("inputfield_customer_Phonenumber_Areacode_name_id"));
			mandatoryMap.put("Man_customerfEmail", getMandatoryStatusFromDataConstraints("inputfield_customer_Email_id"));
			mandatoryMap.put("Man_customerVerifyEmail", getMandatoryStatusFromDataConstraints("inputfield_customer_VerifyEmail_id"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mandatoryMap;
	}
	
	public void enterCustomerDetails(HashMap<String, String> portalSpecificData, String productType) throws Exception
	{
		// set inputfields
		getElement("inputfield_customer_first_name_id").setText(portalSpecificData.get("cd.firstname"));
		getElement("inputfield_customer_last_name_id").setText(portalSpecificData.get("cd.lastname"));
		getElement("inputfield_customer_address_id").setText(portalSpecificData.get("cd.address"));
		getElement("inputfield_customer_city_id").setText(portalSpecificData.get("cd.city"));
		getElement("inputfield_customer_PostalCode_id").setText(portalSpecificData.get("cd.postalcode"));
		getElement("inputfield_customer_Phonenumber_Areacode_name_id").setText(portalSpecificData.get("cd.phoneNumberCode"));
		getElement("inputfield_customer_Phone_number_name_id").setText(portalSpecificData.get("cd.phoneNumber"));
		getElement("inputfield_customer_MobileNumber_Areacode_name_id").setText(portalSpecificData.get("cd.mobileNumberCode"));
		getElement("inputfield_customer_MobileNumber_name_id").setText(portalSpecificData.get("cd.mobilenumber"));
		if(productType.equals("hotel"))
		{
			getElement("inputfield_customer_Email_id").setText(portalSpecificData.get("cd.hotelEmail"));
			getElement("inputfield_customer_VerifyEmail_id").setText(portalSpecificData.get("cd.hotelEmailVerify"));
		}
		else if(productType.equals("activity"))
		{
			getElement("inputfield_customer_Email_id").setText(portalSpecificData.get("cd.activityEmail"));
			getElement("inputfield_customer_VerifyEmail_id").setText(portalSpecificData.get("cd.activityEmailVerify"));
		}
		else if(productType.equals("flight"))
		{
			getElement("inputfield_customer_Email_id").setText(portalSpecificData.get("cd.flightEmail"));
			getElement("inputfield_customer_VerifyEmail_id").setText(portalSpecificData.get("cd.flightEmailVerify"));
		}
		else if(productType.equals("fp"))
		{
			getElement("inputfield_customer_Email_id").setText(portalSpecificData.get("cd.FPEmail"));
			getElement("inputfield_customer_VerifyEmail_id").setText(portalSpecificData.get("cd.FPEmailVerify"));
		}
		else if(productType.equals("vacation"))
		{
			getElement("inputfield_customer_Email_id").setText(portalSpecificData.get("cd.vacationEmail"));
			getElement("inputfield_customer_VerifyEmail_id").setText(portalSpecificData.get("cd.vacationEmailVerify"));
		}
		else
		{
			System.out.println("Invalid option");
		}
		
		((ComboBox)getElement("combobox_customertitle_id")).selectOptionByText(portalSpecificData.get("cd.title"));
		((ComboBox)getElement("combobox_customer_country_id")).selectOptionByText(portalSpecificData.get("cd.country"));
		if(getElementVisibility("combobox_customer_State_id"))
		{
			((ComboBox)getElement("combobox_customer_State_id")).selectOptionByText(portalSpecificData.get("cd.state"));
		}
		
	}
	
	public void enterCustomerDetailsWithout(String Field) throws Exception
	{
		// set inputfields
		getElement("inputfield_customer_first_name_id").setText("bruce");
		getElement("inputfield_customer_last_name_id").setText("wayne");
		getElement("inputfield_customer_address_id").setText("gotham");
		getElement("inputfield_customer_city_id").setText("gothamCity");
		getElement("inputfield_customer_PostalCode_id").setText("1234");
		getElement("inputfield_customer_Phonenumber_Areacode_name_id").setText("123");
		getElement("inputfield_customer_Phone_number_name_id").setText("777777777");
		getElement("inputfield_customer_MobileNumber_Areacode_name_id").setText("321");
		getElement("inputfield_customer_MobileNumber_name_id").setText("888888888");
		getElement("inputfield_customer_Email_id").setText("batman@batcave.com");
		getElement("inputfield_customer_VerifyEmail_id").setText("batman@batcave.com");
		
		((ComboBox)getElement("combobox_customertitle_id")).selectOptionByText("Mr");
		((ComboBox)getElement("combobox_customer_country_id")).selectOptionByText("Sri Lanka");
		if(getElementVisibility("combobox_customer_State_id"))
		{
			((ComboBox)getElement("combobox_customer_State_id")).selectOptionByText("");
		}
		
		getElement(Field).clear();
		
	}
	
	public void enterCustomerDetailsWithout() throws Exception
	{
		// set inputfields
		getElement("inputfield_customer_first_name_id").setText("bruce");
		getElement("inputfield_customer_last_name_id").setText("wayne");
		getElement("inputfield_customer_address_id").setText("gotham");
		getElement("inputfield_customer_city_id").setText("gothamCity");
		getElement("inputfield_customer_PostalCode_id").setText("1234");
		getElement("inputfield_customer_Phonenumber_Areacode_name_id").setText("123");
		getElement("inputfield_customer_Phone_number_name_id").setText("777777777");
		getElement("inputfield_customer_MobileNumber_Areacode_name_id").setText("321");
		getElement("inputfield_customer_MobileNumber_name_id").setText("888888888");
		getElement("inputfield_customer_Email_id").setText("batman@batcave.com");
		getElement("inputfield_customer_VerifyEmail_id").setText("batman@batcave.com");
		
		((ComboBox)getElement("combobox_customertitle_id")).selectOptionByText("Mr");
		((ComboBox)getElement("combobox_customer_country_id")).selectOptionByText("Sri Lanka");
		if(getElementVisibility("combobox_customer_State_id"))
		{
			((ComboBox)getElement("combobox_customer_State_id")).selectOptionByText("");
		}
		
/*		getElement(Field).clear();
*/		
	}
	
	public void enterActivityCustomerDetails(Activity_WEB_Search searchObject){
		
		try {
			
			ArrayList<Activity_WEB_PaymentDetails> paymentInfoList = searchObject.getPaymentDetailsInfo();
			
			for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
				
				((ComboBox)getElement("combobox_customertitle_id")).selectOptionByText(paymentDetails.getCustomerTitle());
				getElement("inputfield_customer_first_name_id").setText(paymentDetails.getCustomerName());
				getElement("inputfield_customer_last_name_id").setText(paymentDetails.getCustomerLastName());
				getElement("inputfield_customer_address_id").setText(paymentDetails.getAddress());
				getElement("inputfield_customer_city_id").setText(paymentDetails.getCity());
				((ComboBox)getElement("combobox_customer_country_id")).selectOptionByText(paymentDetails.getCountry());
				
				if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
					((ComboBox)getElement("combobox_customer_State_id")).selectOptionByText(paymentDetails.getState());				
				}
				getElement("inputfield_customer_PostalCode_id").setText(paymentDetails.getPostalCode());
				getElement("inputfield_customer_Phonenumber_Areacode_name_id").setText(paymentDetails.getTel().split("-")[0]);
				getElement("inputfield_customer_Phone_number_name_id").setText(paymentDetails.getTel().split("-")[1]);
				getElement("inputfield_customer_Email_id").setText(paymentDetails.getEmail());
				getElement("inputfield_customer_VerifyEmail_id").setText(paymentDetails.getEmail());
				
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public Web_Com_OccupancyDetailsPage proceedToOccupancy() throws Exception{
		try {
			getElement("button_ProceedToOccupancy_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
			getElement("button_proceedToOccupancy_classname").getWebElements().get(0).click();
		}
		
		Web_Com_OccupancyDetailsPage occupancyPage = new Web_Com_OccupancyDetailsPage();
		return occupancyPage;		
	}
	
	
	
	public Web_Act_QuotationConfirmationPage saveActivityQuotation() throws Exception{
		getElement("button_Savequotation_xpath").click();
		Web_Act_QuotationConfirmationPage quotationConfirmPage = new Web_Act_QuotationConfirmationPage();
		return quotationConfirmPage;		
	}
	
	
	
	

}
