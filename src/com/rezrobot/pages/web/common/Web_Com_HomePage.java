package com.rezrobot.pages.web.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.dataobjects.CarSearchObject;
import com.rezrobot.dataobjects.DMCSearchObject;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_SearchObject;
import com.rezrobot.pages.web.activity.Web_Act_ResultsPage;
import com.rezrobot.pages.web.fixed_packages.Web_FxP_ResultsPage;
import com.rezrobot.pages.web.flights.Web_Flight_ResultsPage;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Image;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.pojo.Link;
import com.rezrobot.utill.DriverFactory;

public class Web_Com_HomePage extends PageObject {

	public boolean isPageAvailable() {
		return super.isPageAvailable("frame_homepage_id");
	}

	public boolean doSearch() {
		try {
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean getLogoAvailability() throws Exception {
		try {
			return ((Image) getElement("img_company_logo")).isImageLoaded();
		} catch (Exception e) {
			return false;
		}
	}

	public String getLogoPath() throws Exception {
		return ((Image) getElement("img_company_logo")).getImagePath();
	}

	public boolean getCustomerLoginLinkAvailability() throws Exception {
		return getElementVisibility("link_customer_login");
	}

	public String getCustomerLoginLinkText() throws Exception {
		return ((Link) getElement("link_customer_login")).getText();
	}

	public String getCustomerLoginLinkPath() throws Exception {
		return ((Link) getElement("link_customer_login")).getUrl();
	}

	public boolean getBECAvailability() {
		try {
			switchToFrame("frame_homepage_id");
			return getElementVisibility("button_MenuFlight_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public ArrayList<String> getBecProducts() {
		ArrayList<String> AvailableProducts = new ArrayList<String>();

		if (getElementVisibility("button_MenuFlight_id"))
			AvailableProducts.add("Flight");
		if (getElementVisibility(""))
			AvailableProducts.add("Hotel");
		if (getElementVisibility(""))
			AvailableProducts.add("Fixed Package");
		if (getElementVisibility(""))
			AvailableProducts.add("Holidays");
		if (getElementVisibility(""))
			AvailableProducts.add("Activity");
		if (getElementVisibility(""))
			AvailableProducts.add("Car");
		if (getElementVisibility(""))
			AvailableProducts.add("Transfer");

		return AvailableProducts;

	}

	public String getDefaultSelectedBEC() throws Exception {

		ArrayList<String> AvailableProducts = getBecProducts();

		if (AvailableProducts.contains("Flight") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Flight";
		else if (AvailableProducts.contains("Hotel") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Hotel";
		else if (AvailableProducts.contains("Activity") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Activity";
		else if (AvailableProducts.contains("Holidays") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Holidays";
		else if (AvailableProducts.contains("Transfer") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Transfer";
		else if (AvailableProducts.contains("Car") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Car";
		else if (AvailableProducts.contains("Fixed Package") || getElement("").getAttribute("class").contains("ui-state-active"))
			return "Fixed Package";
		else
			return "None";
	}

	public boolean isFlightBECLoaded() {
		try {
			return getElementVisibility("inputfield_To_TxtBox_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public boolean isActivityBECLoaded() {
		try {
			switchToDefaultFrame();
			switchToFrame("frame_homepage_id");
			getElementClick("button_ActivityButtonXpath_xpath");
			return getElementVisibility("inputfield_destinationCity_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public boolean isFPBECAvailable() throws Exception {

		try {

			getElementClick("btn_FixedPackage_id");
			return getElementVisibility("input_OriginAir_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}

	}

	public String monthDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_month")).getDefaultSelectedOption();
	}

	public String dayDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_day")).getDefaultSelectedOption();
	}

	public String yearDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_year")).getDefaultSelectedOption();
	}

	public String distanceDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_distance")).getDefaultSelectedOption();
	}

	public ArrayList<String> genderAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_gender")).getAvailableOptions();
	}

	public ArrayList<String> monthAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_month")).getAvailableOptions();
	}

	public ArrayList<String> dayAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_day")).getAvailableOptions();
	}

	public ArrayList<String> yearAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_year")).getAvailableOptions();
	}

	public HashMap<String, String> getLabels() {

		HashMap<String, String> labels = new HashMap();
		labels.put("Where", getLabelName("label_whereGoingLabel_xpath"));

		return labels;
	}

	public HashMap<String, String> getPlaceHolders() {

		HashMap<String, String> placeholders = new HashMap();

		try {
			placeholders.put("Country of residance", ((ComboBox) getElement("combobox_ResidenceCountry_id")).getDefaultSelectedOption().trim());
			placeholders.put("ToCityInput", getPlaceHolder("inputfield_To_TxtBox_id"));

		} catch (Exception e) {
			// TODO: handle exception
		}

		return placeholders;
	}

	// FLIGHT
	public HashMap<String, Boolean> getFlightAvailabilityOfInputFields() {

		HashMap<String, Boolean> fileds = new HashMap();
		try {

			fileds.put("cor", getElementVisibility("combobox_ResidenceCountry_id"));
			fileds.put("roundtrip", getElementVisibility("radiobutton_RoundTrip_id"));
			fileds.put("onewaytrip", getElementVisibility("radiobutton_OnewayTrip_RadioBtn_id"));
			fileds.put("multidestination", getElementVisibility("radiobutton_MultiDestination_RadioBtn_id"));
			fileds.put("from", getElementVisibility("inputfield_FromTxtBox_id"));
			fileds.put("to", getElementVisibility("inputfield_To_TxtBox_id"));

			fileds.put("departuredate", getElementVisibility("inputfield_DepartureDateTemp_TxtBox_id"));
			try {
				objectMap.get("inputfield_DepartureDateTemp_TxtBox_id").click();
				fileds.put("departuredateselector", getElementVisibility("calendar_DepartureDate_id"));
				Thread.sleep(2000);
			} catch (Exception e) {
				fileds.put("departuredateselector", false);
			}
			fileds.put("departuretime", getElementVisibility("combobox_DepartureTime_DropDwn_id"));

			fileds.put("returndate", getElementVisibility("inputfield_ReturnDateTemp_TxtBox_id"));
			try {
				objectMap.get("inputfield_ReturnDateTemp_TxtBox_id").click();
				fileds.put("returndateselector", getElementVisibility("calendar_DepartureDate_id"));
				Thread.sleep(2000);
			} catch (Exception e) {
				fileds.put("returndateselector", false);
			}
			fileds.put("returntime", getElementVisibility("combobox_ReturnTime_DropDwn_id"));

			fileds.put("imflex", getElementVisibility("checkbox_Flexible_id"));
			fileds.put("adults", getElementVisibility("combobox_AdultCount_id"));
			fileds.put("children", getElementVisibility("combobox_ChildrenCount_DropDwn_id"));
			fileds.put("infant", getElementVisibility("combobox_InfantCount_DropDwn_id"));

			try {
				fileds.put("showadditional", getElementVisibility("label_AdditionalSearchOption_class"));
				objectMap.get("label_AdditionalSearchOption_class").click();
				Thread.sleep(2000);
			} catch (Exception e) {
				fileds.put("showadditional", false);
			}

			try {
				fileds.put("prefair", getElementVisibility("inputfield_PreferredAirline_id"));
			} catch (Exception e) {

			}
			fileds.put("cabinclass", getElementVisibility("combobox_CabinClass_id"));
			fileds.put("prefcurrency", getElementVisibility("combobox_PreferredCurrency_id"));
			fileds.put("promotioncode", getElementVisibility("inputfield_PromotionCode_TxtBox_id"));
			fileds.put("prefnonstop", getElementVisibility("checkbox_PreferNonStop_id"));
			fileds.put("searchbutton", getElementVisibility("button_SearchFlights_Btn_id"));

		} catch (Exception e) {
			// TODO: handle exception
		}

		return fileds;
	}

	public HashMap<String, String> getFlightBECLabels() {

		HashMap<String, String> actualLabels = new HashMap<>();
		try {
			actualLabels.put("Flightslabel", getLabelName("label_flight-icon_id"));
			actualLabels.put("Hotelslabel", getLabelName("label_hotel-icon_id"));
			actualLabels.put("Carslabel", getLabelName("label_car-icon_id"));
			actualLabels.put("Activitieslabel", getLabelName("label_activity-icon_id"));
			actualLabels.put("Trasferslabel", getLabelName("label_transfer-icon_id"));
			actualLabels.put("Holidayslabel", getLabelName("label_fh-icon_id"));
			actualLabels.put("Packageslabel", getLabelName("label_fp-icon_id"));
			actualLabels.put("CORlabel", getLabelName("label_air_country_lbl_div_id"));
			actualLabels.put("TripTypelabel", getLabelName("label_triptype_xpath"));
			actualLabels.put("RoundTriplabel", getLabelName("label_roundtrip_xpath"));
			actualLabels.put("OneWayTriplabel", getLabelName("label_onewaytrip_xpath"));
			actualLabels.put("MultiDestinationlabel", getLabelName("label_multidestination_xpath"));
			actualLabels.put("WhereYouGoinglabel", getLabelName("label_whereyougoing_xpath"));
			actualLabels.put("Fromlabel", getLabelName("label_from_xpath"));
			actualLabels.put("Tolabel", getLabelName("label_to_xpath"));
			actualLabels.put("DepartureDatelabel", getLabelName("label_departuredate_xpath"));
			actualLabels.put("DepartureTimelabel", getLabelName("label_departuretime_xpath"));
			actualLabels.put("ReturnDatelabel", getLabelName("label_returndate_xpath"));
			actualLabels.put("ReturnTimelabel", getLabelName("label_returntime_xpath"));
			actualLabels.put("ImFlexiblelabel", getLabelName("label_imflexible_xpath"));
			actualLabels.put("HowManyPeoplelabel", getLabelName("label_howmanypeople_xpath"));
			actualLabels.put("Adultslabel", getLabelName("label_adults_xpath"));
			actualLabels.put("Childrenlabel", getLabelName("label_children_xpath"));
			actualLabels.put("Infantlabel", getLabelName("label_infant_xpath"));
			try {
				actualLabels.put("AdditionalSearchlabel", getLabelName("label_AdditionalSearchOption_xpath"));
				objectMap.get("label_AdditionalSearchOption_class").click();
				Thread.sleep(2000);
			} catch (Exception e) {

			}
			actualLabels.put("PrefFlightlabel", getLabelName("label_prefflight_xpath"));
			actualLabels.put("Cabinlabel", getLabelName("label_cabin_xpath"));
			actualLabels.put("PreferredCurrlabel", getLabelName("label_preferredcurrency_xpath"));
			actualLabels.put("PromoCodelabel", getLabelName("label_promotioncode_xpath"));
			actualLabels.put("PrefNonStoplabel", getLabelName("label_prefernonstop_xpath"));
			((ComboBox) getElement("combobox_ChildrenCount_DropDwn_id")).selectOptionByText("1");
			actualLabels.put("AgeOfChildrenlabel", getLabelName("label_ageofchildren_xpath"));

		} catch (Exception e) {

		}

		return actualLabels;
	}

	public HashMap<String, String> getFlightComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();

		fields.put("countryOfResidence", getComboBoxDataList("combobox_ResidenceCountry_id"));
		fields.put("departureTime", getComboBoxDataList("combobox_DepartureTime_DropDwn_id"));
		fields.put("returnTime", getComboBoxDataList("combobox_ReturnTime_DropDwn_id"));
		fields.put("adults", getComboBoxDataList("combobox_AdultCount_id"));
		fields.put("children", getComboBoxDataList("combobox_ChildrenCount_DropDwn_id"));
		((ComboBox) getElement("combobox_ChildrenCount_DropDwn_id")).selectOptionByText("1");
		// String combobox_ChildrenAge_DropDwn_id_REF = ((ComboBox)getElement("combobox_ChildrenAge_DropDwn_id")).getRef().replace("REPLACE", "1");
		fields.put("childrenAge", getComboBoxDataList("combobox_ChildrenAge_DropDwn1_id"));
		fields.put("cabins", getComboBoxDataList("combobox_CabinClass_id"));
		fields.put("prefCurrency", getComboBoxDataList("combobox_PreferredCurrency_id"));

		return fields;
	}

	// FLIGHT ENDED

	public HashMap<String, Boolean> getMandatoryList() {

		HashMap<String, Boolean> fileds = new HashMap();

		fileds.put("cor", getMandatoryStatus("combobox_ResidenceCountry_id"));

		return fileds;
	}

	public Web_Flight_ResultsPage proceedToNext() throws Exception {
		getElement("button_SearchFlights_Btn_id").click();
		Web_Flight_ResultsPage fresultspage = new Web_Flight_ResultsPage();
		return fresultspage;
	}

	// ActivityList

	public void doActivitySearch(Activity_WEB_Search searchObject) {

		try {
			try {
				((ComboBox) getElement("combobox_CountryOfResidence_id")).selectOptionByText(searchObject.getCountry());
			} catch (Exception e) {
				e.printStackTrace();
			}

			String hiddenDestiantion = searchObject.getDestination();
			getElement("inputfield_destinationCity_id").setText(hiddenDestiantion.split("\\|")[1]);
			getElement("inputfield_HiddendestinationCity_id").setJavaScript("$('#hid_AC_Loc').val('" + hiddenDestiantion + "');");

			getElement("inputfield_HiddenADateFrom_id").setJavaScript("$('#ac_departure').val('" + searchObject.getDateFrom() + "');");
			getElement("inputfield_HiddenADateTo_id").setJavaScript("$('#ac_arrival').val('" + searchObject.getDateTo() + "');");
			((ComboBox) getElement("combobox_AAdultsDropDown_id")).selectOptionByText(searchObject.getAdults());
			((ComboBox) getElement("combobox_AChildsDropDown_id")).selectOptionByText(searchObject.getChildren());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Web_Act_ResultsPage proceedToActivityResultsPage() throws Exception {
		getElement("button_SearchActivityBtn_id").setJavaScript("search('A');");
		Web_Act_ResultsPage aResultsPage = new Web_Act_ResultsPage();
		return aResultsPage;
	}

	public HashMap<String, String> getActivityBECLabels() {

		HashMap<String, String> labels = new HashMap<>();

		try {
			labels.put("COR", getLabelName("label_COR_xpath"));
			labels.put("Where", getLabelName("label_DESTINATION_xpath"));
			labels.put("When", getLabelName("label_TIME_xpath"));
			labels.put("People", getLabelName("label_PEOPLE_xpath"));
			labels.put("ShowAdd", getLabelName("label_ShowAdd_xpath"));
			getElement("label_ShowAdd_xpath").click();
			labels.put("HideAdd", getLabelName("label_HideAdd_xpath"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return labels;
	}

	public HashMap<String, Boolean> getActivityAvailabilityOfInputFields() {

		HashMap<String, Boolean> fields = new HashMap<>();

		try {
			fields.put("CountryOfResidence", getElementVisibility("combobox_CountryOfResidence_id"));
			fields.put("DestinationCity", getElementVisibility("inputfield_destinationCity_id"));
			fields.put("DateFrom", getElementVisibility("inputfield_ADateFrom_id"));
			fields.put("DateTo", getElementVisibility("inputfield_ADateTo_id"));
			fields.put("Adults", getElementVisibility("combobox_AAdultsDropDown_id"));
			fields.put("Children", getElementVisibility("combobox_AChildsDropDown_id"));
			selectOptionByText("combobox_AChildsDropDown_id", "1");
			fields.put("ChildrenAge", getElementVisibility("combobox_AChildsAge_id"));
			getElement("label_ShowAdd_xpath").click();
			fields.put("ActivityType", getElementVisibility("inputfield_ActivityType_id"));
			fields.put("ProgramCategory", getElementVisibility("combobox_ProgramCategory_id"));
			fields.put("PreferCurrency", getElementVisibility("combobox_PreferCurrency_id"));
			fields.put("DiscountCoupon", getElementVisibility("inputfield_DiscountCoupon_id"));
			getElement("label_HideAdd_xpath").click();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fields;
	}

	public HashMap<String, Boolean> getActivityMandatoryList() {

		HashMap<String, Boolean> fields = new HashMap<>();

		fields.put("CountryOfResidence", getMandatoryStatus("combobox_CountryOfResidence_id"));
		fields.put("DestinationCity", getMandatoryStatus("inputfield_destinationCity_id"));

		return fields;
	}

	public HashMap<String, String> getActivityComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();

		fields.put("CountryListLoadedCheck", getComboBoxDataList("combobox_CountryOfResidenceDataLoad_xpath"));
		fields.put("ProgramCategoryCheck", getComboBoxDataList("combobox_ProgramCategoryLoadedCheck_xpath"));
		fields.put("prefercurencyLoadedCheck", getComboBoxDataList("combobox_PreferCurrencyLoadedCheck_xpath"));
		fields.put("ActivityProgramCatList", getElement("combobox_ProgramCategory_id").getText().replaceAll("\n", " "));

		return fields;
	}

	/**************
	 * FP HOME PAGE
	 * 
	 * @throws Exception
	 *******/

	public boolean isFPPRoductEnabled() {

		try {
			getElement("btn_FixedPackage_id").click();
			addobjects();
			return true;
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}

	}

	public HashMap<String, Boolean> getAvailabilityOfInputFields_FP() throws Exception {
		HashMap<String, Boolean> fileds = new HashMap<String, Boolean>();

		try {

			
			fileds.put("cor", getElementVisibility("combobox_ResidenceCountryFP_id"));
			fileds.put("includingflight", getElementVisibility("radiobtn_PackageTypeF_id"));
			fileds.put("excludingflight", getElementVisibility("radiobtn_PackageTypeL_id"));
			fileds.put("origin", getElementVisibility("input_OriginAir_id"));
			fileds.put("destination", getElementVisibility("input_DestinationAir_id"));
			getElement("radiobtn_PackageTypeL_id").click();
			fileds.put("destination_land", getElementVisibility("input_DestinationLand_id"));
			getElement("radiobtn_PackageTypeF_id").click();
			fileds.put("departuremonth", getElementVisibility("combobox_travelon_id"));
			fileds.put("duration", getElementVisibility("combobox_duration_id"));
			
			
			fileds.put("package_type", getElementVisibility("combobox_packagetype_id"));
			fileds.put("prefferedcurrency_FP", getElementVisibility("combobox_preferredCurrencyFP_id"));

		} catch (Exception e) {
			// TODO: handle exception
		}

		return fileds;

	}

	public HashMap<String, String> getLabelTexts_FP() throws Exception {
		HashMap<String, String> labels = new HashMap<String, String>();

		try {
			
			
			labels.put("CORlabel", getLabelName("lbl_countryofresidencelabel_id"));
			labels.put("Tourplanlabel", getLabelName("lbl_whereUgoinglabel_xpath"));
			labels.put("FlightInclusivelabel", getLabelName("lbl_includingflightlabel_xpath"));
			labels.put("Flightexclusivelabel", getLabelName("lbl_excludingflightlabel_xpath"));
			labels.put("departureairportlabel", getLabelName("lbl_leavingfromlabel_xpath"));
			labels.put("destinationairportlabel", getLabelName("lbl_goingtolabel_xpath"));
			labels.put("departurelabel", getLabelName("lbl_travellingwhenlabel_xpath"));
			getElementClick("lnk_Searchmoreoptions_xpath");
			labels.put("departuremonthlabel", getLabelName("lbl_departuremonthlabel_xpath"));
			labels.put("durationlabel", getLabelName("lbl_durationlabel_xpath"));
			labels.put("packagetypelabel", getLabelName("lbl_packagetypelabel_xpath"));
			labels.put("preferredcurrencylabel", getLabelName("lbl_preferredCurrencylabel_xpath"));
			
			
			getElement("radiobtn_PackageTypeL_id").click();
			
			Thread.sleep(3000);
			
			labels.put("destinationcitylabel", getLabelName("lbl_destinationlabel_xpath"));
			getElement("radiobtn_PackageTypeF_id").click();

			System.out.println();

		} catch (Exception e) {
			// TODO: handle exception
		}

		return labels;

	}

	public HashMap<String, String> getHotelBECLabels() {

		HashMap<String, String> labels = new HashMap<>();

		try {
			labels.put("customerLogin", getLabelName("link_customer_login"));
			labels.put("countryOfResidence", getLabelName("label_country_of_residence_xpath"));
			labels.put("whereAreYouGoing", getLabelName("label_where_are_you_going_xpath"));
			labels.put("WhenAreYouGoing", getLabelName("label_when_are_you_going_xpath"));
			labels.put("howManyPeople", getLabelName("label_how_many_people_xpath"));
			labels.put("checkin", getLabelName("label_check_in_xpath"));
			labels.put("checkout", getLabelName("label_check_out_xpath"));
			labels.put("nights", getLabelName("label_nights_xpath"));
			labels.put("rooms", getLabelName("label_rooms_xpath"));
			labels.put("room", getLabelName("label_room_xpath"));
			labels.put("adults", getLabelName("label_adults_xpath"));
			labels.put("children", getLabelName("label_children_xpath"));
			labels.put("showAdditional", getLabelName("label_show_additional_search_options_xpath"));
			getElement("button_show_additional_search_options_xpath").click();
			labels.put("starRating", getLabelName("label_star_rating_xpath"));
			labels.put("hotelType", getLabelName("label_hotel_type_xpath"));
			labels.put("hotelName", getLabelName("label_hotel_name_xpath"));
			labels.put("preferredCurrency", getLabelName("lable_preferred_currency_xpath"));
			labels.put("promotionCode", getLabelName("label_promotion_code_xpath"));
			labels.put("hotelAvailability", getLabelName("label_hotel_availability_xpath"));
			labels.put("available", getLabelName("label_available_xpath"));
			labels.put("onRequest", getLabelName("label_on_request_xpath"));
			labels.put("priceRange", getLabelName("label_price_range_xpath"));
			labels.put("to", getLabelName("label_to_xpath"));
			labels.put("from", getLabelName("label_from_xpath"));
			labels.put("hideAdditional", getLabelName("label_hide_additional_search_options_xpath"));
			labels.put("searchForHotels", getLabelName("button_search_for_hotels_id"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return labels;
	}

	public HashMap<String, Boolean> getHotelAvailabilityOfInputFields() {

		HashMap<String, Boolean> fields = new HashMap<>();

		try {

			fields.put("whereAreYouGoing", getElementVisibility("inputfield_where_are_you_going_id"));
			// getElement("button_show_additional_search_options_xpath").click();
			fields.put("hotelName", getElementVisibility("inputfield|_hotel_name_id"));
			fields.put("promotionCode", getElementVisibility("inputfield_promotion_code_id"));
			fields.put("from", getElementVisibility("inputfield_from_id"));
			fields.put("to", getElementVisibility("inputfield_to_id"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fields;
	}

	public HashMap<String, Boolean> getHotelMandatoryList() {

		HashMap<String, Boolean> fields = new HashMap<>();

		fields.put("countryOfResidence", getMandatoryStatus("combobox_country_of_residence_id"));
		fields.put("whereAreYouGoing", getMandatoryStatus("inputfield_where_are_you_going_id"));
		return fields;
	}

	public HashMap<String, String> getHotelComboBoxDataList(HotelSearchObject search) throws Exception {

		HashMap<String, String> fields = new HashMap<>();
		UIElement refChanger = new UIElement();

		fields.put("countryOfResidence", getComboBoxDataList("combobox_country_of_residence_id"));
		fields.put("nights", getComboBoxDataList("combobox_nights_id"));
		fields.put("rooms", getComboBoxDataList("combobox_rooms_id"));
		fields.put("adults", getComboBoxDataList("combobox_adults_id"));
		fields.put("children", getComboBoxDataList("combobox_children_id"));

		for (int i = 1; i <= Integer.parseInt(search.getRooms()); i++) {
			refChanger = getElement("combobox_adults_id").changeRefAndGetElement(getElement("combobox_adults_id").getRef().replace("REPLACE", String.valueOf(i)));

			System.out.println(refChanger.getRef());
		}

		((ComboBox) getElement("combobox_children_id")).selectOptionByText("1");
		Thread.sleep(1000);
		fields.put("age", getComboBoxDataList("combobox_children_age_id"));
		fields.put("starRating", getComboBoxDataList("combobox_star_rating_id"));
		fields.put("hotelType", getComboBoxDataList("combobox_hotel_type_id"));
		fields.put("preferredCurrency", getComboBoxDataList("combobox_preferred_currency_id"));

		return fields;
	}

	public HashMap<String, String> getFixDMCPackagesComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();

		fields.put("countryOfResidence", getComboBoxDataList("combobox_ResidenceCountryFP_id"));

		// getElement("lnk_Searchmoreoptions_xpath").click();

		fields.put("Departure month", getComboBoxDataList("combobox_travelon_id"));
		fields.put("Duration", getComboBoxDataList("combobox_duration_id"));
		fields.put("Package type", getComboBoxDataList("combobox_packagetype_id"));
		fields.put("Prefferred Currency", getComboBoxDataList("combobox_preferredCurrencyFP_id"));

		return fields;

	}

	public boolean isCarBECLoaded() {
		try {
			switchToDefaultFrame();
			switchToFrame("frame_homepageHotel_id");
			getElementClick("button_car_xpath");
			Thread.sleep(5000);
			return getElementVisibility("button_car_search_id", 10);
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public void searchForCar(CarSearchObject search) throws Exception {
		String[] temp = search.getPickupLocation().split("\\|");
		String pickup = temp[0];
		temp = search.getDropoffLocation().split("\\|");
		String dropoff = temp[0];
		getElement("inputfield_pickup_id").setText(pickup);
		getElement("inputfield_dropoff_id").setText(dropoff);
		getElement("inputfield_hidden_pickup_id").setJavaScript("$('#hid_car_Loc').val('" + search.getPickupLocation() + "');");
		getElement("inputfield_hidden_departure_id").setJavaScript("$('#hid_car_Loc1').val('" + search.getDropoffLocation() + "');");
		getElement("calander_pickup_date_id").setJavaScript("$('#car_departure').val('" + search.getPickupDate() + "');");
		getElement("calander_dropoff_date_id").setJavaScript("$('#car_arrival').val('" + search.getDropoffDate() + "');");
		((ComboBox) getElement("combobox_pickup_id")).selectOptionByText("9AM");
		getElement("button_car_search_id").click();

	}

	public boolean isHotelBECLoaded() {
		try {
			switchToDefaultFrame();
			switchToFrame("frame_homepageHotel_id");
			getElementClick("button_hotel_xpath");
			return getElementVisibility("combobox_country_of_residence_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public boolean isDMCBECLoaded() {
		try {
			switchToDefaultFrame();
			switchToFrame("frame_homepageHotel_id");
			getElementClick("button_DMC_xpath");
			return getElementVisibility("combobox_country_of_residence_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public void performTheSearch() throws Exception {
		try {
			getElement("button_search_for_hotels_id").getWebElements().get(1).click();
		} catch (Exception e) {
			getElement("button_search_for_hotels_id").getWebElements().get(0).click();
			System.out.println(e);
		}
	}

	public void searchForHotels(HotelSearchObject search) throws Exception {

		// switchToFrame("bec_container_frame");
		String destinationHelp = search.getDestination().replace("|", "/");
		String[] destination = destinationHelp.split("/");
		UIElement refChanger = new UIElement();

		try {
			((ComboBox) getElement("combobox_country_of_residence_id")).selectOptionByText(search.getCountry());
		} catch (Exception e) {
			// TODO: handle exception
		}

		// UIElement test = getElement("inputfield_where_are_you_going_id").changeRef(getElement("inputfield_where_are_you_going_id").getRef());
		// test.setText(destination[1]);
		getElement("inputfield_hidden_Destination_id").setJavaScript("$('#hid_H_Loc').val('" + search.getDestination() + "');");
		getElement("inputfield_hidden_checkin_id").setJavaScript("$('#ho_departure').val('" + search.getCheckin() + "');");

		// get checkout date
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		String dt = null;
		try {
			c.setTime(sdf.parse(search.getCheckin()));
			c.add(Calendar.DATE, Integer.parseInt(search.getNights()));
			dt = sdf.format(c.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		getElement("inputfield_hidden_checkin_id").setJavaScript("$('#ho_arrival').val('" + dt + "');");

		((ComboBox) getElement("combobox_nights_id")).selectOptionByText(search.getNights());
		((ComboBox) getElement("combobox_rooms_id")).selectOptionByText(search.getRooms());
		int childAge = 0;

		for (int i = 1; i <= Integer.parseInt(search.getRooms()); i++) {
			String[] adults = search.getAdults().split("/");
			String child[] = search.getChildren().split("/");
			String age[] = search.getAge().split("/");
			try {
				refChanger = ((ComboBox) getElement("combobox_adults_id")).changeRefAndGetElement(getElement("combobox_adults_id").getRef().replace("1", String.valueOf(i)));
				((ComboBox) refChanger).selectOptionByText(adults[i - 1]);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}

			try {
				refChanger = getElement("combobox_children_id").changeRefAndGetElement(getElement("combobox_children_id").getRef().replace("1", String.valueOf(i)));
				((ComboBox) refChanger).selectOptionByText(child[i - 1]);
			} catch (Exception e) {
				// TODO: handle exception
			}

			for (int j = 1; j <= Integer.parseInt(child[i - 1]); j++) {
				try {
					refChanger = getElement("combobox_children_age_id").changeRefAndGetElement(getElement("combobox_children_age_id").getRef().replace("R1", "R".concat(String.valueOf(i))).replace("_1", "_".concat(String.valueOf(j))));
					((ComboBox) refChanger).selectOptionByText(age[childAge]);
					childAge++;
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
				}
			}
		}
		getElement("inputfield_where_are_you_going_id").setText(destination[1]);

	}

	public void searchForVacation(Vacation_Search_object search) throws Exception {
		switchToDefaultFrame();
		switchToFrame("frame_bec_id");
		getElementVisibility("button_vacation_xpath", 30);
		Thread.sleep(2000);
		getElement("button_vacation_xpath").click();
		try {
			getElementVisibility("inputfield_from_id", 20);
		} catch (Exception e) {
			// TODO: handle exception
		}
		UIElement refChanger = new UIElement();
		String from = "";
		String to = "";
		String[] temp = null;
		temp = search.getDepartureAirport().split("\\|");
		from = temp[0];
		temp = search.getArrivalAirport().split("\\|");
		to = temp[0];

		getElement("inputfield_hidden_from_id").setJavaScript("$('#V_DepFromHid').val('" + search.getDepartureAirport() + "');");
		getElement("inputfield_hidden_to_id").setJavaScript("$('#V_RetLocHid').val('" + search.getArrivalAirport() + "');");
		getElement("calender_departure_id").setJavaScript("$('#vac_departure').val('" + search.getCheckin() + "');");
		getElement("inputfield_hidden_hotel_destination_id").setJavaScript("$('#hid_H_Loc_V').val('" + search.getHotelDestination() + "');");
		// get checkout date
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		String dt = null;
		try {
			c.setTime(sdf.parse(search.getCheckin()));
			c.add(Calendar.DATE, Integer.parseInt(search.getNights()));
			dt = sdf.format(c.getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		getElement("calender_arrival_id").setJavaScript("$('#vac_arrival').val('" + dt + "');");
		int childAge = 0;
		((ComboBox) getElement("combobox_num_of_rooms_id")).selectOptionByText(search.getRooms());
		for (int i = 1; i <= Integer.parseInt(search.getRooms()); i++) {
			String[] adults = search.getAdults().split("/");
			String child[] = search.getChildren().split("/");
			String age[] = search.getAge().split("/");
			String infant[] = search.getInfants().split("/");
			try {
				refChanger = ((ComboBox) getElement("combobox_adult_count_id")).changeRefAndGetElement(getElement("combobox_adult_count_id").getRef().replace("replace", String.valueOf(i)));
				((ComboBox) refChanger).selectOptionByText(adults[i - 1]);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}

			try {
				refChanger = getElement("combobox_children_count_id").changeRefAndGetElement(getElement("combobox_children_count_id").getRef().replace("replace", String.valueOf(i)));
				((ComboBox) refChanger).selectOptionByText(child[i - 1]);
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				refChanger = getElement("combobox_infant_count_id").changeRefAndGetElement(getElement("combobox_infant_count_id").getRef().replace("replace", String.valueOf(i)));
				((ComboBox) refChanger).selectOptionByText(infant[i - 1]);
			} catch (Exception e) {
				// TODO: handle exception
			}

			for (int j = 1; j <= Integer.parseInt(child[i - 1]); j++) {
				try {
					refChanger = getElement("combobox_age_id").changeRefAndGetElement(getElement("combobox_age_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j)));
					((ComboBox) refChanger).selectOptionByText(age[childAge]);
					childAge++;
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
				}
			}
		}
		try {
			((ComboBox) getElement("combobox_country_id")).selectOptionByText(search.getCountry());
		} catch (Exception e) {
			// TODO: handle exception
		}
		Thread.sleep(2000);
		getElement("inputfield_from_id").setText(from);
		getElement("inputfield_to_id").setText(to);
		System.out.println(getElements("button_search_id").size());
		Thread.sleep(2000);
		try {
			getElements("button_search_id").get(0).click();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("search button click error 1");
		}
		Thread.sleep(2000);
		try {
			getElements("button_search_id").get(1).click();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("search button click error 2");
		}
	}

	public void SearchForDMC(DMCSearchObject search) throws Exception {

		try {
			((ComboBox) getElement("combobox_ResidenceCountryFP_id")).selectOptionByText(search.getCountryOfResidence());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			System.out.println(e);
		}

		if (search.getFlightStatus().trim().equalsIgnoreCase("Including Flight")) {

			getElementClick("radiobtn_PackageTypeF_id");

			// getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('" + search.getDepartureValue() + "');");
			getElement("input_OriginAir_id").setText(search.getDepartureValue().split("\\|")[1]);
			Thread.sleep(6000);
			DriverFactory.getInstance().getDriver().findElement(By.id("package_Loc_ori")).sendKeys(Keys.ARROW_DOWN);
			DriverFactory.getInstance().getDriver().findElement(By.id("package_Loc_ori")).sendKeys(Keys.TAB);

			// getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('" + search.getArrivalValue() + "');");
			// getElement("input_DestinationAir_id").setText(search.getArrival());

			getElement("input_DestinationAir_id").setText(search.getArrivalValue().split("\\|")[1]);
			Thread.sleep(6000);
			DriverFactory.getInstance().getDriver().findElement(By.id("package_Loc_des")).sendKeys(Keys.ARROW_DOWN);
			DriverFactory.getInstance().getDriver().findElement(By.id("package_Loc_des")).sendKeys(Keys.TAB);

		}

		else if (search.getFlightStatus().trim().equalsIgnoreCase("Excluding Flight")) {

			getElementClick("radiobtn_PackageTypeL_id");

			getElement("input_DestinationLand_id").setText(search.getDeparture());
			Thread.sleep(6000);
			DriverFactory.getInstance().getDriver().findElement(By.id("package_Ex_des")).sendKeys(Keys.ARROW_DOWN);
			DriverFactory.getInstance().getDriver().findElement(By.id("package_Ex_des")).sendKeys(Keys.TAB);

		}

		else {
			System.out.println("Undefined package type ");
		}

	}

	public HashMap<String, Boolean> getMandatoryList_FP() {

		HashMap<String, Boolean> fileds = new HashMap();

		fileds.put("cor", getMandatoryStatus("combobox_ResidenceCountryFP_id"));
		fileds.put("origin", getElementVisibility("input_OriginAir_id"));
		fileds.put("destination", getElementVisibility("input_DestinationAir_id"));
		fileds.put("destination_land", getElementVisibility("input_DestinationLand_id"));

		return fileds;
	}

	public String departuremonthDefaultValue() throws Exception {
		return ((ComboBox) getElement("combobox_travelon_id")).getDefaultSelectedOption();
	}

	public String durationDefaultValue() throws Exception {
		return ((ComboBox) getElement("combobox_duration_id")).getDefaultSelectedOption();
	}

	public String packagetypeDefaultValue() throws Exception {
		return ((ComboBox) getElement("combobox_packagetype_id")).getDefaultSelectedOption();
	}

	public String preferredcurrencyDefaultValue() throws Exception {
		return ((ComboBox) getElement("combobox_preferredCurrencyFP_id")).getDefaultSelectedOption();
	}

	public ArrayList<String> getAvailablePackageTypeOptions() throws Exception {

		return ((ComboBox) getElement("combobox_packagetype_id")).getAvailableOptions();

	}

	public ArrayList<String> getAvailablepackagedurationOptions() throws Exception {

		return ((ComboBox) getElement("combobox_duration_id")).getAvailableOptions();

	}

	public ArrayList<String> getAvailabledeparturemonthOptions() throws Exception {

		return ((ComboBox) getElement("combobox_travelon_id")).getAvailableOptions();

	}

	public void doSearch_FP(FxP_SearchObject search) {

		try {
			((ComboBox) getElement("combobox_ResidenceCountryFP_id")).selectOptionByText(search.getCountryOfResidence());
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {

			if (search.getFlightStatus().contains("Including Flight ")) {

				getElementClick("radiobtn_PackageTypeF_id");

				((InputFiled) getElement("input_OriginAir_id")).setText(search.getDeparture());
				// getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('"+search.getDepartureValue()+"');");

				getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('" + "Dubai|DXB|4|Dubai International Airport|-|53||United Arab Emirates" + "');");

				Thread.sleep(4000);
				// ((InputFiled)getElement("input_DestinationAir_id")).setText(search.getArrival());

				((InputFiled) getElement("input_DestinationAir_id")).setText("Muscat");
				Thread.sleep(4000);

				// getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+search.getArrivalValue()+"');");

				getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('" + "Muscat|MCT|872|Muscat International Airport|-|231||Oman" + "');");
				/*
				 * getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");
				 * getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");
				 */
			} else {

				getElementClick("radiobtn_PackageTypeL_id");

				((InputFiled) getElement("input_DestinationLand_id")).setText(search.getArrival());
				getElement("input_DestinationLandvalue_id").setJavaScript("$('#hid_package_Ex_des').val('" + search.getArrivalValue() + "');");

			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public Web_FxP_ResultsPage proceedToNext_FP() throws Exception {
		getElement("btn_FPsearch_id").click();
		Web_FxP_ResultsPage fp_resultspage = new Web_FxP_ResultsPage();
		return fp_resultspage;
	}

	public Web_FxP_ResultsPage performDMCSearch() throws Exception {

		try {

			getElement("btn_FPsearch_id").click();

			Thread.sleep(5000);

		} catch (Exception e) {
			System.out.println(e);
		}

		Web_FxP_ResultsPage fp_resultspage = new Web_FxP_ResultsPage();
		return fp_resultspage;

		// TODO Auto-generated method stub

	}

}
