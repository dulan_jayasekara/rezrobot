package com.rezrobot.pages.web.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.CommonBillingInfoRates;

public class Web_Com_BillingInformationPage extends PageObject{
	
	public boolean isBillingInformationsAvailable(){
		System.out.println("Billing info availability");
		switchToDefaultFrame();
		return super.isPageAvailable("combobox_CreditCardList_id");
	}
	
	public HashMap<String, Boolean> getBillingInfoInputFields(){
		
		HashMap<String, Boolean> fieldsMap = new HashMap<>();
		
		try {			
			fieldsMap.put("IN_DiscountCouponNumber", getElementVisibility("inputfield_DiscountCouponNumber_id"));
			fieldsMap.put("IN_DiscountCouponValidate", getElementVisibility("button_DiscountCouponValidate_xpath"));					
			fieldsMap.put("IN_CreditCardList", getElementVisibility("combobox_CreditCardList_id"));
			fieldsMap.put("IN_credit_card_note", getElementVisibility("div_credit_card_note_xpath"));			
			fieldsMap.put("IN_BillingInfo_PrevStep", getElementVisibility("button_BillingInfo_PrevStep_xpath"));
			fieldsMap.put("IN_BillingInfo_ProceedToNotes", getElementVisibility("button_BillingInfo_ProceedToNotes_xpath"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fieldsMap;
	}
	
	public HashMap<String, String> getBillingInfoLabels(){
		
		HashMap<String, String> labelsMap = new HashMap<>();
		
		try {			
			labelsMap.put("L_EnterDiscountCoupon", getLabelName("label_EnterDiscountCoupon_xpath"));
			labelsMap.put("L_cardTypeLabel", getLabelName("label_cardTypeLabel_xpath"));
			labelsMap.put("L_BI_SubTotal", getLabelName("label_BI_SubTotal_xpath"));
			labelsMap.put("L_BI_TotalTaxandOthers", getLabelName("label_BI_TotalTaxandOthers_xpath"));
			labelsMap.put("L_BI_Total", getLabelName("label_BI_Total_xpath"));
			labelsMap.put("L_BI_AmountProcessedNow", getLabelName("label_BI_AmountProcessedNow_xpath"));
			labelsMap.put("L_BI_AmountDue", getLabelName("label_BI_AmountDue_xpath"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return labelsMap;
	}
	
	public ActivityDetails getActivityBillingInfoDetails(ActivityDetails activityDetails){
		
		try {
			
			activityDetails.setPaymentsPage_BI_CardCurrency(getElement("text_paymentsPage_BI_CardCurrency").getText());
			activityDetails.setPaymentsPage_BI_CardTotal(getElement("text_paymentsPage_BI_CardTotal").getText());
			activityDetails.setPaymentsPage_BI_Currency(getElement("text_paymentsPage_BI_Currency").getText());
			activityDetails.setPaymentsPage_BI_SubTotal(getElement("text_paymentsPage_BI_SubTotal").getText());
			activityDetails.setPaymentsPage_BI_TotalTax(getElement("text_paymentsPage_BI_TotalTax").getText());
			activityDetails.setPaymentsPage_BI_Total(getElement("text_paymentsPage_BI_Total").getText());
			activityDetails.setPaymentsPage_BI_AmountNow(getElement("text_paymentsPage_BI_AmountNow").getText());
			activityDetails.setPaymentsPage_BI_AmountDue(getElement("text_paymentsPage_BI_AmountDue").getText());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
		
	}
	
	public CommonBillingInfoRates getRates() throws Exception
	{
		CommonBillingInfoRates rates = new CommonBillingInfoRates();
		try {
			rates.setTotalPG(getElement("text_paymentsPage_BI_CardTotal").getText());
		} catch (Exception e) {
			// TODO: handle exception
			rates.setTotalPG(getElement("text_paymentsPage_BI_CardTotal2").getText());
			
		}
		
		rates.setCurrency(getElement("text_paymentsPage_BI_Currency").getText().replace(")", "").replace("(", ""));
		rates.setSubtotal(getElement("text_paymentsPage_BI_SubTotal").getText());
		rates.setTotalTaxAndFees(getElement("text_paymentsPage_BI_TotalTax").getText());
		rates.setTotal(getElement("text_paymentsPage_BI_Total").getText());
		rates.setAmountBeingProcessedNow(getElement("text_paymentsPage_BI_AmountNow").getText());
		return rates;
	}
	
	public Web_Com_NotesPage fillSpecialNotes() throws Exception{
		getElement("button_BillingInfo_ProceedToNotes_xpath").click();
		Web_Com_NotesPage notesPage = new Web_Com_NotesPage();
		return notesPage;
	}

	public void DMCfillSpecialNotes() throws Exception {

		try {
			getElementClick("DMC_button_BillingInfo_ProceedToNotes_xpath");

		} catch (Exception e) {
			System.out.println();
		}

		
	}
}
