package com.rezrobot.pages.web.common;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.Activity_WEB_PaymentDetails;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_DataReader;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_InputObject;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_OccupantObject;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_SearchObject;
import com.rezrobot.utill.DriverFactory;

public class Web_Com_OccupancyDetailsPage extends PageObject{
	
	public boolean isActivity_OccupancyDetailsAvailable(){
		switchToDefaultFrame();
		return super.isPageAvailable("label_occupancyActivity_Title_xpath");
	}
	
	public HashMap<String, Boolean> getActivity_OccupancyPageInputFields(){
		
		HashMap<String, Boolean> fieldsMap = new HashMap<>();
	
		try {
			fieldsMap.put("IN_occupancyActivity_title", getElementVisibility("combobox_occupancyActivity_title_classname"));
			fieldsMap.put("IN_occupancyActivity_first_name", getElementVisibility("inputfield_occupancyActivity_first_name_classname"));
			fieldsMap.put("IN_occupancyActivity_last_name", getElementVisibility("inputfield_occupancyActivity_last_name_classname"));				
			fieldsMap.put("IN_PickDetails_Station", getElementVisibility("radiobutton_PickDetails_Station_xpath"));
			fieldsMap.put("IN_PickDetails_Airport", getElementVisibility("radiobutton_PickDetails_Airport_xpath"));
			fieldsMap.put("IN_PickDetails_Hotel", getElementVisibility("radiobutton_PickDetails_Hotel_xpath"));			
			fieldsMap.put("IN_DropDownDetails_Station", getElementVisibility("radiobutton_DropDownDetails_Station_xpath"));
			fieldsMap.put("IN_DropDownDetails_Airport", getElementVisibility("radiobutton_DropDownDetails_Airport_xpath"));
			fieldsMap.put("IN_DropDownDetails_Hotel", getElementVisibility("radiobutton_DropDownDetails_Hotel_xpath"));		
			fieldsMap.put("IN_OccupancyDetails_PrevStep", getElementVisibility("button_OccupancyDetails_PrevStep_xpath"));
			fieldsMap.put("IN_OccupancyDetails_ProceedToBillingInfo", getElementVisibility("button_OccupancyDetails_ProceedToBillingInfo_xpath"));
					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fieldsMap; 		
	}
	
	public HashMap<String, String> getActivity_OccupancyPageLabels() {
		
		HashMap<String, String> labelMap = new HashMap<>();
		
		try {	
			labelMap.put("L_occupancyActivity_Title", getLabelName("label_occupancyActivity_Title_xpath"));
			labelMap.put("L_occupancyActivity_FName", getLabelName("label_occupancyActivity_FName_xpath"));
			labelMap.put("L_occupancyActivity_LName", getLabelName("label_occupancyActivity_LName_xpath"));
			labelMap.put("L_occupancyActivity_AdultOne", getLabelName("label_occupancyActivity_AdultOne_xpath"));			
			labelMap.put("L_occupancyActivityPickDropDetails", getLabelName("label_occupancyActivityPickDropDetails_xpath"));
			labelMap.put("L_PickDropDetailsEnter", getLabelName("label_PickDropDetailsEnter_xpath"));
			labelMap.put("L_PickDetails_Heading", getLabelName("label_PickDetails_Heading_xpath"));
			labelMap.put("L_PickDetails_Station", getLabelName("label_PickDetails_Station_xpath"));
			labelMap.put("L_PickDetails_Airport", getLabelName("label_PickDetails_Airport_xpath"));
			labelMap.put("L_PickDetails_Hotel", getLabelName("label_PickDetails_Hotel_xpath"));
			labelMap.put("L_DropDownDetails_Heading", getLabelName("label_DropDownDetails_Heading_xpath"));
			labelMap.put("L_DropDownDetails_Station", getLabelName("label_DropDownDetails_Station_xpath"));
			labelMap.put("L_DropDownDetails_Airpor", getLabelName("label_DropDownDetails_Airport_xpath"));
			labelMap.put("L_DropDownDetails_Hotel", getLabelName("label_DropDownDetails_Hotel_xpath"));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return labelMap;
	}
	
	public ActivityDetails setActivityPickUpDropOffDetails(ActivityDetails activityDetails, Activity_WEB_Search searchObject){
		
		try {
			
			ArrayList<Activity_WEB_PaymentDetails> paymentInfoList = searchObject.getPaymentDetailsInfo();
			
			for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
				
				UIElement firstNameElement = getElement("inputfield_occupancyActivity_first_name_classname");
				String firstName = firstNameElement.getAttribute("value");
				
				if (firstName.isEmpty()) {
					getElement("inputfield_occupancyActivity_first_name_classname").setText(paymentDetails.getCustomerName());
					activityDetails.setPaymentsPage_OD_FirstName(paymentDetails.getCustomerName());
				}else{
					activityDetails.setPaymentsPage_OD_FirstName(firstName);
				}
								
				UIElement lastNameElement = getElement("inputfield_occupancyActivity_last_name_classname");
				String lastName = lastNameElement.getAttribute("value");
				
				if (lastName.isEmpty()) {
					getElement("inputfield_occupancyActivity_last_name_classname").setText(paymentDetails.getCustomerLastName());
					activityDetails.setPaymentsPage_OD_LastName(paymentDetails.getCustomerLastName());
				}else{
					activityDetails.setPaymentsPage_OD_LastName(lastName);
				}				
			}
			
			UIElement pickUpDetailsElement = getElement("label_PickDetailsLabel_classname");
			String bidIdForPickUp = pickUpDetailsElement.getWebElements().get(0).getAttribute("name").split("Grp-")[1];
			
			UIElement pickuplocationStation_Element = getElement("inputfield_pickuplocationStationPortName_name").changeRefAndGetElement(getElement("inputfield_pickuplocationStationPortName_name").getRef().replace("XXXX", bidIdForPickUp));
			pickuplocationStation_Element.setText("Colombo");
			activityDetails.setPaymentsPage_OD_PickPort("Colombo");
			UIElement pickupaddionalinfo_Element = getElement("inputfield_pickupaddionalinfo_name").changeRefAndGetElement(getElement("inputfield_pickupaddionalinfo_name").getRef().replace("XXXX", bidIdForPickUp));
			pickupaddionalinfo_Element.setText("PickUp Additional Info");
			activityDetails.setPaymentsPage_OD_PickInfo("PickUp Additional Info");
			UIElement dropofflocationNameStation_Element = getElement("inputfield_dropofflocationNameStationPort_name").changeRefAndGetElement(getElement("inputfield_dropofflocationNameStationPort_name").getRef().replace("XXXX", bidIdForPickUp));
			dropofflocationNameStation_Element.setText("Hambanthota");
			activityDetails.setPaymentsPage_OD_DropPort("Hambanthota");
			UIElement dropoffaddionalinfo_Element = getElement("inputfield_dropoffaddionalinfo_name").changeRefAndGetElement(getElement("inputfield_dropoffaddionalinfo_name").getRef().replace("XXXX", bidIdForPickUp));
			dropoffaddionalinfo_Element.setText("DropOff Additional Info");	
			activityDetails.setPaymentsPage_OD_DropInfo("DropOff Additional Info");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
		
	}
	
	public void FxP_OccupantFill(FxP_InputObject input) {
		
		FxP_SearchObject search=input.getSearch();
		
		
		//Adult occupant details fill
		
		for (int i = 0; i < search.getAdultcount(); i++) {
			
		
		
		
		UIElement eleTitle=new UIElement();
		eleTitle.setElementProperties("combobx_title_id", "1", "adultTitle_Air["+i+"]");
		addTomap(eleTitle);
		UIElement eleFname=new UIElement();
		eleFname.setElementProperties("input_AdtFname_id", "1", "adultFNm_Air["+i+"]");
		addTomap(eleFname);
		UIElement eleLname=new UIElement();
		eleLname.setElementProperties("input_AdtLname_id", "1", "adultLNm_Air["+i+"]");
		addTomap(eleLname);
		UIElement elebday=new UIElement();
		elebday.setElementProperties("input_AdtBday_id", "1", "adultDateOfBirth_Air_"+i);
		addTomap(elebday);
		UIElement elegender=new UIElement();
		elegender.setElementProperties("combobx_AdtGender_id", "1", "adultGender["+i+"]");
		addTomap(elegender);
		
		
		}
		
		
		for (int i = 0; i <search.getChildCount(); i++) {
			
			UIElement eleTitle=new UIElement();
			eleTitle.setElementProperties("combobx_title_id", "1", "childFNm_Air[0]");
			addTomap(eleTitle);
			UIElement eleFname=new UIElement();
			eleFname.setElementProperties("combobx_title_id", "1", "childFNm_Air[0]");
			addTomap(eleFname);
			UIElement eleLname=new UIElement();
			eleLname.setElementProperties("combobx_title_id", "1", "childLNm_Air[0]");
			addTomap(eleLname);
			UIElement ele=new UIElement();
			ele.setElementProperties("combobx_title_id", "1", "childDateOfBirth_Air_"+i);
			addTomap(ele);
			
			
			
		}
		
		for (int i = 0; i < search.getInfantcount(); i++) {
			
			UIElement eleTitle=new UIElement();
			eleTitle.setElementProperties("combobx_title_id", "1", "adultTitle_Air["+i+"]");
			addTomap(eleTitle);
			UIElement eleFname=new UIElement();
			eleFname.setElementProperties("combobx_title_id", "1", "infantFNm_Air[0]");
			addTomap(eleFname);
			UIElement eleLname=new UIElement();
			eleLname.setElementProperties("combobx_title_id", "1", "adultLNm_Air["+i+"]");
			addTomap(eleLname);
			UIElement ele=new UIElement();
			ele.setElementProperties("combobx_title_id", "1", "adultDateOfBirth_Air_"+i);
			addTomap(ele);
			
		}
		
	}
	
	public Web_Com_BillingInformationPage loadBillingInformations() throws Exception{
		getElement("button_OccupancyDetails_ProceedToBillingInfo_xpath").click();
		Web_Com_BillingInformationPage billingInfoPage = new Web_Com_BillingInformationPage();
		return billingInfoPage;		
	}
	
	/*public static void main(String[] args) throws Exception {
		
	FxP_DataReader read=new FxP_DataReader();
	FxP_InputObject input=read.inputObjectConstruct().get(0);
	
	DriverFactory.getInstance().initializeDriver();
	DriverFactory.getInstance().getDriver().get("http://oman-stg.secure-reservation.com/omanairholidaysReservations/PaymentPage.do?method=addtocart&transectionId=omanairholidays-9d39cb1b13414f6b8f1588e9cb4494e3&payment=true");

	
	Web_Com_CustomerDetailsPage custPage=new Web_Com_CustomerDetailsPage();
	custPage.enterCustomerDetailsWithout();
	Web_Com_OccupancyDetailsPage occpage=custPage.proceedToOccupancy();
	occpage.FxP_OccupantFill(input);
	
	
	
	
		
	}*/

}
