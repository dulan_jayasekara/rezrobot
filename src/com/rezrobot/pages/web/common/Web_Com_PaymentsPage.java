package com.rezrobot.pages.web.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.pages.web.fixed_packages.Web_FxP_ComponentListingPage;
import com.rezrobot.utill.DriverFactory;

public class Web_Com_PaymentsPage extends PageObject{
	
	private HashMap<String, Boolean> activity_ButtonsMap;
	private HashMap<String, String> activity_LabelsMap;
	private HashMap<String, String> actualBasketValues;
	
	public boolean isPaymentsPageAvailable(){
		switchToDefaultFrame();
		getElementVisibility("div_PaymentsPageLoaded_classname", 200);
		return super.isPageAvailable("div_PaymentsPageLoaded_classname");
	}
	
	public void selectPaymentOption(String option)
	{
		if(option.contains("Online") || option.contains("online")) {
			try {
				getElementClick("radiobutton_PaymentOption_Online_id");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if(option.contains("Offline") || option.contains("offline")) {
			try {
				getElementClick("radiobutton_PaymentOption_Offline_id");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if(option.contains("Flocash") || option.contains("flocash")) {
			try {
				getElementClick("radiobutton_PaymentOption_Flocash_id");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isMyBasketLoaded(){
		switchToDefaultFrame();
		return getElementVisibility("div_MyBasketLoaded_classname");
	}
	
	public boolean isMyBasketBillingInfoSectionLoaded(){
		switchToDefaultFrame();
		return getElementVisibility("div_MyBasket_BillingInfoSectionLoaded_classname");
	}
	
	public HashMap<String, String> getPaymentsPageCommonLabels(){
		
		HashMap<String, String> commonLabelsMap = new HashMap<>();
		
		try {
			commonLabelsMap.put("L_AddHotels", getLabelName("label_AddHotels_xpath"));
			commonLabelsMap.put("L_AddFlights", getLabelName("label_AddFlights_xpath"));
			commonLabelsMap.put("L_AddActivities", getLabelName("label_AddActivities_xpath"));
			commonLabelsMap.put("L_AddCars", getLabelName("label_AddCars_xpath"));
			commonLabelsMap.put("L_AddTransfers", getLabelName("label_AddTransfers_xpath"));	
			
			commonLabelsMap.put("L_MyBasket", getLabelName("label_MyBasket_xpath"));
			commonLabelsMap.put("L_BillingInformation", getLabelName("label_BillingInformation_xpath"));
			commonLabelsMap.put("L_mybasketSubTotal_BillingInfo", getLabelName("label_mybasketSubTotal_BillingInformation_xpath"));
			commonLabelsMap.put("L_mybasketTotalTaxt", getLabelName("label_mybasketTotalTax_xpath"));
			commonLabelsMap.put("L_mybasketTotal", getLabelName("label_mybasketTotal_xpath"));
			commonLabelsMap.put("L_mybasketAmountBeingProcessed", getLabelName("label_mybasketAmountBeingProcessed_xpath"));
			commonLabelsMap.put("L_mybasketAmountDue", getLabelName("label_mybasketAmountDue_xpath"));
														
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return commonLabelsMap;
	}

	public boolean isMyBasketActivityMoreDetailsSectionLoaded(){
	
		try {
			getElementVisibility("button_my_basket_expand_xpath", 200);
			getElementClick("button_my_basket_expand_xpath");
			getElementVisibility("label_Mybasket_more_details_usable_on_xpath", 200);
			getElementClick("button_Mybasket_more_details_close_xpath");
			return true;		
		} catch (Exception e) {
			return false;
		}
	}
	
	public ActivityDetails getActivity_PaymentsPageValidations(ActivityDetails activityDetails){
		
		activity_ButtonsMap = new HashMap<>();
		activity_LabelsMap = new HashMap<>();
		
		try {			
			if (isMyBasketLoaded()) {
				
				activity_ButtonsMap.put("IN_button_my_basket", getElementVisibility("button_my_basket_expand_xpath"));				
				activityDetails.setMyBasket_Currency(getElement("text_myBasket_Currency").getText());
				activityDetails.setMyBasket_TotalActivityBookingValue(getElement("text_myBasket_TotalActivityBookingValue").getText());				
				activityDetails.setPaymentsPage_cart_TotalWTOCCfee(getElement("text_paymentsPage_cart_TotalWTOCCfee").getText());
				activityDetails.setPaymentsPageCartLoaded(true);
				activityDetails.setPaymentsPage_cart_SubTotal(getElement("text_paymentsPage_cart_SubTotal").getText());
				activityDetails.setPaymentsPage_cart_TotalTax(getElement("text_paymentsPage_cart_TotalTax").getText());
				activityDetails.setPaymentsPage_cart_Total(getElement("text_paymentsPage_cart_Total").getText());
				activityDetails.setPaymentsPage_cart_AmountNow(getElement("text_paymentsPage_cart_AmountNow").getText());
				activityDetails.setPaymentsPage_cart_AmountDue(getElement("text_paymentsPage_cart_AmountDue").getText());
							
				if (isMyBasketActivityMoreDetailsSectionLoaded()) {
				
					getElementClick("button_my_basket_expand_xpath");
					activityDetails.setPaymentsPage_AMD_ActiivtyName(getElement("text_paymentsPage_AMD_ActiivtyName").getText().split(" - ")[0]);
					activityDetails.setPaymentsPage_AMD_ActivityTypeName(getElement("text_paymentsPage_AMD_ActiivtyName").getText().split(" - ")[1]);					
					activityDetails.setPaymentsPage_AMD_UsableOnDate(getElement("text_paymentsPage_AMD_UsableOnDate").getText());
					activityDetails.setPaymentsPage_AMD_Period(getElement("text_paymentsPage_AMD_Period").getText());
					activityDetails.setPaymentsPage_AMD_RateType(getElement("text_paymentsPage_AMD_RateType").getText());
					activityDetails.setPaymentsPage_AMD_DailyRate(getElement("text_paymentsPage_AMD_DailyRate").getText());
					activityDetails.setPaymentsActivityMoreDetailsInfo(true);
					activityDetails.setPaymentsPage_AMD_QTY(getElement("text_paymentsPage_AMD_QTY").getText());
					activityDetails.setPaymentsPage_AMD_TotalWithCurrency(getElement("text_paymentsPage_AMD_TotalWithCurrency").getText());					
					activityDetails.setPaymentsPage_AMD_Status(getElement("text_paymentsPage_AMD_Status").getText().split(" - ")[1]);
					activityDetails.setPaymentsPage_AMD_SubTotal(getElement("text_paymentsPage_AMD_SubTotal").getText());
					activityDetails.setPaymentsPage_AMD_Tax(getElement("text_paymentsPage_AMD_Tax").getText());
					activityDetails.setPaymentsPage_AMD_Total(getElement("text_paymentsPage_AMD_Total").getText());
					activityDetails.setPaymentsPage_AMD_CancellationDealine(getElement("text_paymentsPage_AMD_CancellationDealine").getText().split(" : ")[1]);
					
					ArrayList<String> paymentsPage_AMD_CancelPolicy = new ArrayList<String>();
					
					UIElement cancelElement = getElement("text_paymentsPage_AMD_CancelPolicy");
					List<WebElement> cancellationList = new UIElement().getInnerWebElements(cancelElement.getWebElement(),  getElement("text_liTestFromUi"));
					
					for (int i = 0; i < cancellationList.size(); i++) {

						String cancelPolicies = cancellationList.get(i).getText();
						paymentsPage_AMD_CancelPolicy.add(cancelPolicies);
					}
					
					activityDetails.setPaymentsPage_AMD_CancelPolicy(paymentsPage_AMD_CancelPolicy);
					
					activity_LabelsMap.put("L_MD_activity_name", getLabelName("label_Mybasket_more_details_activity_name_xpath"));
					activity_LabelsMap.put("L_MD_usable_on", getLabelName("label_Mybasket_more_details_usable_on_xpath"));
					activity_LabelsMap.put("L_MD_period_session", getLabelName("label_Mybasket_more_details_period_session_xpath"));
					activity_LabelsMap.put("L_MD_rate_type", getLabelName("label_Mybasket_more_details_rate_type_xpath"));
					activity_LabelsMap.put("L_MD_rate", getLabelName("label_Mybasket_more_details_rate_xpath"));
					activity_LabelsMap.put("L_MD_qty", getLabelName("label_Mybasket_more_details_qty_xpath"));
					activity_LabelsMap.put("L_MD_total", getLabelName("label_Mybasket_more_details_total_table_xpath"));
					activity_LabelsMap.put("L_MD_subtotal", getLabelName("label_Mybasket_more_details_subtotal_xpath"));
					activity_LabelsMap.put("L_MD_tax_and_charges", getLabelName("label_Mybasket_more_details_tax_and_charges_xpath"));
					activity_LabelsMap.put("L_MD_total", getLabelName("label_Mybasket_more_details_total_xpath"));
					activity_LabelsMap.put("L_MD_status", getLabelName("label_Mybasket_more_details_status_xpath"));
					activity_LabelsMap.put("L_MD_cancellation_deadline", getLabelName("label_Mybasket_more_details_cancellation_deadline_xpath"));
					activity_LabelsMap.put("L_MD_cancellation_policy", getLabelName("label_Mybasket_more_details_cancellation_policy_Indicate_xpath"));			
					activity_ButtonsMap.put("IN_button_my_basket_Close", getElementVisibility("button_Mybasket_more_details_close_xpath"));
					getElementClick("button_Mybasket_more_details_close_xpath");			
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
	}
	
	public HashMap<String, Boolean> getActivity_AvailabileButtons(){
		
		try {
			if (!(activity_ButtonsMap.isEmpty())) {
				return activity_ButtonsMap;
			}else{
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	public HashMap<String, String> getActivity_MoreDetailsSetionLabels(){
		
		try {
			if (!(activity_LabelsMap.isEmpty())) {
				return activity_LabelsMap;
			}else{
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
		
	}
	
	public Web_Com_CustomerDetailsPage fillCustomerDetails() throws Exception{
		Web_Com_CustomerDetailsPage customerDetailsPage = new Web_Com_CustomerDetailsPage();
		return customerDetailsPage;		
	}
	
	public void FxPShoppingBasketRead() throws Exception {
		
		System.out.println(getElement("txt_shortDescription_id").getText());
		System.out.println(getElement("lbl_basketTitle_class").getText());
		System.out.println(getElement("img_packageImage_xpath").getAttribute("src"));
		
	}
	
	public Map<String, String> FxPRateRead() throws Exception {
		
		Map<String, String> ratemap=new HashMap();
		
		ratemap.put("Selling_currency", getElement("txt_FxPSellingCurrency_id").getText());
		ratemap.put("SubTotal",getElement("txt_FxPSubTotal_id").getText());
		ratemap.put("Tax",getElement("txt_FxPtaxAmount_id").getText());
		ratemap.put("PackageCost",getElement("txt_FxPtotalPackageCost_id").getText());
		ratemap.put("ChargedAmount",getElement("txt_FxPtotalChargedAmount_id").getText());
		
		return ratemap;
		
		
		
		
	}
	
	public void FxP_DetailedItinerary() throws Exception {
		getElement("btn_detailedItinerary_class").click();
		addobjects();
		getElement("div_closeDetailedItinerary_class").click();
		
	}
	
	public void Flight_readBasket()
	{
		
		//activityDetails.setMyBasket_Currency(getElement("text_myBasket_Currency").getText());
	}
	
	public void Flight_readBasketMoreDetails()
	{
		
	}
	
	/*public static void main(String[] args) throws Exception {
		
		
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get("http://dev3.rezg.net/omanairholidaysReservations/Search.do?method=loadWithResults&transectionId=omanairholidays-800b1eafd3d749fca2d12da8a8e09ea2&packageId=71&packDate=08-01-2016&numberOfAdults=1&numberOfChilds=0&numberOfInfants=0&numberOfRooms=1&occupancy=1|0|-|%25|%25|0|%25|-@&components=FHA&childAllowed=true&startDate=2016-06-30&endDate=2017-11-01&costedDates=&isDMCPackage=false&flightInclude=Y&cabinClass=Economy&cacheKey=Dubai|DXB|4|Dubai%20International%20Airport||53||United%20Arab%20Emirates@0@Muscat|MCT|872|Muscat%20International%20Airport||231||Oman@@9~OM@-@@0@DC@@@omanairholidays@");

		Web_FxP_ComponentListingPage page=new Web_FxP_ComponentListingPage();
		//page.clickonFlightDetails();
		//page.flightDetailReader();
		//page.returntoExistingPackages();
		page.getRideTraceID();
		System.out.println(page.airDisplayRead().get("InBound Flight Arrival Time"));
		//page.bookThepackage();
		page.readPackageRatelower();
		page.readPackageRaterateBasket();
		Web_Com_PaymentsPage paypage=page.bookThepackage();
		System.out.println(paypage.FxPRateRead().get("PackageCost"));
		paypage.FxPShoppingBasketRead();
		paypage.FxP_DetailedItinerary();
		Web_Com_CustomerDetailsPage custPage=paypage.fillCustomerDetails();
		//custPage.enterCustomerDetails(portalSpecificData)
		
		// Changed
		
	}*/
	
}
