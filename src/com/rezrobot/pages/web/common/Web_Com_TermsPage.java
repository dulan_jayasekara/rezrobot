package com.rezrobot.pages.web.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.pages.web.activity.Web_Act_ConfirmationPage;
import com.rezrobot.utill.ReportTemplate;

public class Web_Com_TermsPage extends PageObject{
	
	public boolean isTermsAndConditionsAvailable(){
		switchToDefaultFrame();
		try {
			getElementVisibility("checkbox_TC_CancelledCheckBox_id", 10);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return super.isPageAvailable("checkbox_TC_CancelledCheckBox_id");
	}
	
	public HashMap<String, Boolean> getTermsAndConditionInputFields(){
		
		HashMap<String, Boolean> fieldsMap = new HashMap<>();
		
		try {
			fieldsMap.put("IN_TC_CancelledCheckBox", getElementVisibility("checkbox_TC_CancelledCheckBox_xpath"));
			fieldsMap.put("IN_IConfirmedReadTheTermsAndConditions", getElementVisibility("checkbox_TC_IConfirmedReadTheTermsAndConditions_xpath"));
			fieldsMap.put("IN_WouldLikeToReceiv", getElementVisibility("checkbox_TC_WouldLikeToReceiv_xpath"));
			fieldsMap.put("IN_TC_PrevStep", getElementVisibility("button_TC_PrevStep_xpath"));
			fieldsMap.put("IN_TC_Make_payment", getElementVisibility("button_MAKEPAYMENT_xpath"));
			fieldsMap.put("IN_TC_create_login", getElementVisibility("checkbox_TC_Create_login_id"));
			
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fieldsMap;
	}
	
	public HashMap<String, String> getTermAndConditionPageLabels(){
		
		HashMap<String, String> labelsMap = new HashMap<>();
		
		try {	
			String[] temp = null;
			labelsMap.put("L_TC_FullyreRundableifCancelled", getLabelName("label_TC_FullyreRundableifCancelled_xpath"));
			labelsMap.put("L_TC_IConfirmReadPolicy", getLabelName("label_TC_IConfirmReadPolicy_xpath"));
			labelsMap.put("L_TC_GeneralTermsAndCondition", getLabelName("label_TC_GeneralTermsAndCondition_xpath"));
			labelsMap.put("L_TC_ChargeAndCancellationFeesApply", getLabelName("label_TC_ChargeAndCancellationFeesApply_xpath"));
			labelsMap.put("L_TC_AvailabilityAtTimeOfMaking", getLabelName("label_TC_AvailabilityAtTimeOfMaking_xpath"));
			labelsMap.put("L_TC_TheIndividualConditions", getLabelName("label_TC_TheIndividualConditions_xpath"));
			labelsMap.put("L_TC_OmanairholidaysTermsCondition", getLabelName("label_TC_OmanairholidaysTermsCondition_xpath"));
			labelsMap.put("L_TC_IfYouRequiretoAmendOrCancel", getLabelName("label_TC_IfYouRequiretoAmendOrCancel_xpath"));
			temp = getLabelName("label_TC_portalemail_xpath").split(":");
			labelsMap.put("L_TC_portalemail", temp[0]);
			temp = getLabelName("label_TC_portalphone_xpath").split(":");
			labelsMap.put("L_TC_portalphone", temp[0]);
			labelsMap.put("L_TC_IConfirmedReadTheTermsAndConditionsText", getLabelName("label_TC_IConfirmedReadTheTermsAndConditionsText_xpath"));
			temp = getLabelName("label_TC_WouldLikeToReceivText_xpath").split("www");
			labelsMap.put("L_WouldLikeToReceivText", temp[0].trim());
			labelsMap.put("L_PleaseCheckThatAllYourBooking", getLabelName("label_TC_PleaseCheckThatAllYourBooking_xpath"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return labelsMap;
	}
	
	public HashMap<String, Boolean> getTCMandatoryInputFields(){
		
		HashMap<String, Boolean> mandtFieldsMap = new HashMap<>();
		
		try {
			mandtFieldsMap.put("Man_TC_CancelledCheckBox", getCheckBoxMandatoryStatusFromDataConstraints("checkbox_TC_CancelledCheckBox_xpath"));
			mandtFieldsMap.put("Man_IConfirmedReadTheTermsAndConditions", getCheckBoxMandatoryStatusFromDataConstraints("checkbox_TC_IConfirmedReadTheTermsAndConditions_xpath"));
			getElementClick("checkbox_TC_CancelledCheckBox_xpath");
			getElementClick("checkbox_TC_IConfirmedReadTheTermsAndConditions_xpath");
			try {
				getElementClick("checkbox_TC_CancelledCheckBox_id");
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mandtFieldsMap;
	}
	
	public ActivityDetails getActivityTermsAndConditions(ActivityDetails activityDetails){
		
		try {
		
			WebElement ele2 = new UIElement().getinnerWebElement(getElement("div_ActiivtyNameFromTerms_classname").getWebElement(), getElement("div_getActNameDIV"));
			String id = ele2.getAttribute("id").split("cxlpolicy_")[1];
			
			UIElement activityNameElement = getElement("text_paymentsPage_TC_ActName").changeRefAndGetElement(getElement("text_paymentsPage_TC_ActName").getRef().replace("XXX", id));
			activityDetails.setPaymentsPage_TC_ActName(activityNameElement.getText().split(" : ")[1]);
			
			UIElement cancelDeadlineElement = getElement("text_paymentsPage_TC_CancellationDead_1").changeRefAndGetElement(getElement("text_paymentsPage_TC_CancellationDead_1").getRef().replace("XXX", id));
			activityDetails.setPaymentsPage_TC_CancellationDead_1(cancelDeadlineElement.getText().split(" : ")[1]);
			
			activityDetails.setPaymentsPage_TC_CancellationDead_2(getElement("text_paymentsPage_TC_CancellationDead_2").getText());
			activityDetails.setPaymentsPage_TC_portalName(getElement("text_paymentsPage_TC_portalName").getText().split(" : ")[1]);
			activityDetails.setPaymentsPage_TC_portalPhone(getElement("text_paymentsPage_TC_portalPhone").getText().split(" : ")[1]);
					
			ArrayList<String> paymentsPageCancelPolicy = new ArrayList<String>();
			
			UIElement cancelElement = getElement("text_paymentsPage_TC_CancelPolicy").changeRefAndGetElement(getElement("text_paymentsPage_TC_CancelPolicy").getRef().replace("XXX", id));
			List<WebElement> cancellationList = new UIElement().getInnerWebElements(cancelElement.getWebElement(),  getElement("text_liTestFromUi"));
			
			for (int i = 0; i < cancellationList.size(); i++) {

				String cancelPolicies = cancellationList.get(i).getText();
				paymentsPageCancelPolicy.add(cancelPolicies);
			}
			
			activityDetails.setPaymentsPage_TC_CancelPolicy(paymentsPageCancelPolicy);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
	}
	
	public Web_Com_PaymentGateway checkAvailability() throws Exception{
		try {
			getElement("button_MAKEPAYMENT_xpath").click();
			getElementVisibility("button_COMPLETETHEBOOKING_class", 120);
			//getElement("button_COMPLETETHEBOOKING_class").setJavaScript("hideandSubmitThirdParty('true');");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		Web_Com_PaymentGateway paymentGatewayPage = new Web_Com_PaymentGateway();
		return paymentGatewayPage;		
	}
	
	public void getError(ReportTemplate printTemplate)
	{
		try {
			System.out.println(getElement("label_error_id").getText());
			//printTemplate.setInfoTableHeading("Confirmation error");
			//printTemplate.addToInfoTabel("Error", getElement("label_error_id").getText());
			//printTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
