package com.rezrobot.pages.web.common;

import java.util.ArrayList;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.Common_upselling;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pojo.ComboBox;

public class Web_Com_upselling extends PageObject{

	public Common_upselling checkHotelUpSelling(HotelSearchObject search) throws Exception
	{
		
		Common_upselling up = new Common_upselling();
		try {
			getElement("link_up_selling_add_hotels_xpath").click();
			switchToFrame("frame_bec_id");
			up.setLocation(getElement("label_hotel_destination_id").getAttribute("value"));
			up.setCheckin(getElement("label_hotel_checkin_id").getAttribute("value"));
			up.setCheckout(getElement("label_hotel_checkout_id").getAttribute("value"));
			up.setNights(((ComboBox)getElement("combobox_hotel_nights_id")).getDefaultSelectedOption());
			up.setRooms(((ComboBox)getElement("combobox_hotel_rooms_id")).getDefaultSelectedOption());
			
			ArrayList<String> adults = new ArrayList<String>();
			ArrayList<String> children = new ArrayList<String>();
			ArrayList<String> age = new ArrayList<String>();
			for(int i = 1 ; i <= Integer.parseInt(search.getRooms()) ; i++)
			{
				adults.add(((ComboBox)getElement("combobox_hotel_nights_id").changeRefAndGetElement(getElement("combobox_hotel_nights_id").getRef().replace("1", String.valueOf(i)))).getDefaultSelectedOption());
				children.add(((ComboBox)getElement("combobox_hotel_children_id").changeRefAndGetElement(getElement("combobox_hotel_children_id").getRef().replace("1", String.valueOf(i)))).getDefaultSelectedOption());
				for(int j = 1 ; j > 0 ; j++)
				{
					try {
						age.add(((ComboBox)getElement("combobox_hotel_age_id").changeRefAndGetElement(getElement("combobox_hotel_age_id").getRef().replace("1", String.valueOf(i).replace("2", String.valueOf(j))))).getDefaultSelectedOption());
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					
				}
			}
			up.setHotelAdults(adults);
			up.setHotelChildren(children);
			up.setHotelAge(age);
			switchToDefaultFrame();
			getElement("button_Close_classname").click();	
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			try {
				switchToDefaultFrame();
			} catch (Exception e2) {
				// TODO: handle exception
			}	
		}
		
		return up;
	}
	
	public Common_upselling checkFlightUpSelling() throws Exception
	{
		try {
			getElement("link_up_selling_add_flights_xpath").click();
			switchToFrame("frame_bec_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Common_upselling up = new Common_upselling();
		try {
			up.setFrom(getElement("label_flight_from_id").getPlaceHolder());
			up.setTo(getElement("label_flight_to_id").getPlaceHolder());
			up.setDepartureDate(getElement("label_flight_departureDate_id").getText());
			up.setDepartureTime(((ComboBox)getElement("combobox_flight_departureTime_id")).getDefaultSelectedOption());
			up.setReturnDate(getElement("label_flight_returnDate_id").getText());
			up.setReturnTime(((ComboBox)getElement("comboboc_flight_returnTime_id")).getDefaultSelectedOption());
			up.setAdults(((ComboBox)getElement("combobox_flight_adults_id")).getDefaultSelectedOption());
			up.setChildren(((ComboBox)getElement("combobox_flight_children_id")).getDefaultSelectedOption());
			up.setInfants(((ComboBox)getElement("combobox_flight_infant_id")).getDefaultSelectedOption());
			switchToDefaultFrame();
			getElement("button_Close_classname").click();
		} catch (Exception e) {
			// TODO: handle exception
			try {
				switchToDefaultFrame();
			} catch (Exception e2) {
				// TODO: handle exception
			}
			System.out.println(e);
		}
		
		return up;
		
	}
	
	public Common_upselling checkActivityUpSelling() throws Exception
	{
		try {
			getElement("link_up_selling_add_activities_xpath").click();
			switchToFrame("frame_bec_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Common_upselling up = new Common_upselling();
		try {
			up.setLocation(getElement("label_activity_location_id").getPlaceHolder());
			up.setFrom(getElement("label_activity_from_id").getText());
			up.setTo(getElement("label_activity_to_id").getText());
			up.setAdults(((ComboBox)getElement("combobox_activity_adults_id")).getDefaultSelectedOption());
			up.setChildren(((ComboBox)getElement("combobox_activity_children_id")).getDefaultSelectedOption());
			switchToDefaultFrame();
			getElement("button_Close_classname").click();
		} catch (Exception e) {
			// TODO: handle exception
			try {
				switchToDefaultFrame();
			} catch (Exception e2) {
				// TODO: handle exception
			}
			
		}
		
		
		return up;
	}
}
