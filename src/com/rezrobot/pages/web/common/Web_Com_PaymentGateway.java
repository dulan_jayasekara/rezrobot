package com.rezrobot.pages.web.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.pages.web.activity.Web_Act_ConfirmationPage;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.processor.LabelReadProperties;

public class Web_Com_PaymentGateway extends PageObject{
	
	private String paymentGatewayName;
	
	
	public String getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(String paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}

	public boolean isPaymentGatewayLoaded(HashMap<String, String> configDetails){
		
		LabelReadProperties labelProperty = new LabelReadProperties();
		paymentGatewayName = labelProperty.getPortalSpecificData().get("PaymentGateway_CardType");
		System.out.println(configDetails.get("pgw.open.self.window"));
		try {
			if(configDetails.get("pgw.open.self.window").equals("N"))
			{
				switchToDefaultFrame();
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		
		if (paymentGatewayName.equalsIgnoreCase("rezPay")) {
			
			try {
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				
				getElementVisibility("inputfield_RezPaY_Card1_id", 50);				
				return true;		
			} catch (Exception e) {
				return false;
			}
			
		}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
			
			try {
				try {
					switchToFrame("frame_live_message_frame_id");
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				getElementVisibility("inputfield_wireCard_FirstName_id", 50);
				return true;
			} catch (Exception e) {
				return false;
			}
				
		}
		return false;
		
	}
	
	public boolean isPaymentGatewayLoaded(String paymentGatewayName)
	{
		this.paymentGatewayName = paymentGatewayName;
		//LabelReadProperties labelProperty = new LabelReadProperties();
		//paymentGatewayName = labelProperty.getPortalSpecificData().get("PaymentGateway_CardType");
				
		if (paymentGatewayName.equalsIgnoreCase("rezPay")) {
			
			try {
				switchToDefaultFrame();
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				
				getElementVisibility("inputfield_RezPaY_Card1_id", 50);				
				return true;		
			} catch (Exception e) {
				return false;
			}
			
		}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
			
			try {
				switchToDefaultFrame();		
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				getElementVisibility("inputfield_wireCard_FirstName_id", 50);
				return true;
			} catch (Exception e) {
				return false;
			}
				
		}else if(paymentGatewayName.equalsIgnoreCase("flocash")){
			
			try {
				switchToDefaultFrame();		
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				getElementVisibility("inputfield_flocash_FirstName_id", 50);
				return true;
			} catch (Exception e) {
				return false;
			}
				
		}
		return false;
	}
	
	public void enterCardDetails(HashMap<String, String> portalSpecificData){
		
		try {
			
			if (paymentGatewayName.equalsIgnoreCase("rezPay")) {
				
				getElement("inputfield_RezPaY_Card1_id").setJavaScript("$('#cardnumberpart1').val('"+ portalSpecificData.get("cc1") +"');");	
				getElement("inputfield_RezPaY_Card2_id").setJavaScript("$('#cardnumberpart2').val('"+ portalSpecificData.get("cc2") +"');");	
				getElement("inputfield_RezPaY_Card3_id").setJavaScript("$('#cardnumberpart3').val('"+ portalSpecificData.get("cc3") +"');");	
				getElement("inputfield_RezPaY_Card4_id").setJavaScript("$('#cardnumberpart4').val('"+ portalSpecificData.get("cc4") +"');");	
				((ComboBox)getElement("combobox_RezPaY_ExpMonth_id")).selectOptionByText(portalSpecificData.get("cc.month"));
				((ComboBox)getElement("combobox_RezPaY_ExpYear_id")).selectOptionByText(portalSpecificData.get("cc.year"));
				getElement("inputfield_RezPaY_CardHolderName_id").setText(portalSpecificData.get("cc.firstname").concat(" ").concat(portalSpecificData.get("cc.lastname")));
				getElement("inputfield_RezPaY_CVVNo_id").setText(portalSpecificData.get("cc.cvv"));
									
			}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
				
				getElement("inputfield_wireCard_FirstName_id").setText(portalSpecificData.get("cc.firstname"));
				getElement("inputfield_wireCard_LastName_id").setText(portalSpecificData.get("cc.lastname"));
				getElementClick("button_wireCard_CardTypeSelectButton_id");
				getElementClick("button_wireCard_VisaType_id");
				getElement("inputfield_wireCard_AccountNo_id").setText(portalSpecificData.get("cc1") + portalSpecificData.get("cc2") + portalSpecificData.get("cc3") + portalSpecificData.get("cc4"));
				getElement("inputfield_wireCard_CVVNo_id").setText(portalSpecificData.get("cc.cvv"));
				((ComboBox)getElement("combobox_wireCard_ExpMonth_id")).selectOptionByText(portalSpecificData.get("cc.month"));
				((ComboBox)getElement("combobox_wireCard_ExpYear_id")).selectOptionByText(portalSpecificData.get("cc.year"));	
				
			}
			else if(paymentGatewayName.equalsIgnoreCase("flocash")){
				
				//FLOCASH PAYMENTGATEWAY INPUTS
				getElement("inputfield_flocash_Country_id").setText("");
				getElement("inputfield_flocash_PayMethod_id").setText("");
				getElement("inputfield_flocash_FirstName_id").setText("Daniel");
				getElement("inputfield_flocash_LastName_id").setText("OBrien");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void confirm() throws Exception
	{
		LabelReadProperties 	labelProperty 			= new LabelReadProperties();
								paymentGatewayName 		= labelProperty.getPortalSpecificData().get("PaymentGateway_CardType");
		if(paymentGatewayName.equalsIgnoreCase("rezPay"))
		{
			getElement("button_RezPaY_ProceedButton_id").click();
		}
		else if(paymentGatewayName.equalsIgnoreCase("wireCard"))
		{
			getElement("button_wireCard_SubmitButton_id").click();
		}
	}
	
	public void confirm(String paymentGatewayName) throws Exception
	{
		if(paymentGatewayName.equalsIgnoreCase("rezPay"))
		{
			getElement("button_RezPaY_ProceedButton_id").click();
		}
		else if(paymentGatewayName.equalsIgnoreCase("wireCard"))
		{
			getElement("button_wireCard_SubmitButton_id").click();
		} 
		else if(paymentGatewayName.equalsIgnoreCase("flocash"))
		{
			getElement("button_flocash_SubmitButton_id").click();
		}
	}
	
	public Web_Act_ConfirmationPage loadActivityConfirmationPage() throws Exception{
		
		try {
			
			if (paymentGatewayName.equalsIgnoreCase("rezPay")) {				
				getElementClick("button_RezPaY_ProceedButton_id");
				Web_Act_ConfirmationPage aConfirmationPage = new Web_Act_ConfirmationPage();
				return aConfirmationPage;
				
			}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
				getElementClick("button_wireCard_SubmitButton_id");
				Web_Act_ConfirmationPage aConfirmationPage = new Web_Act_ConfirmationPage();
				return aConfirmationPage;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
