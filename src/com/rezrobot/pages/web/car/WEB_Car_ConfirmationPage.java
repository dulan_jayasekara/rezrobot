package com.rezrobot.pages.web.car;


import org.openqa.selenium.Alert;


import com.rezrobot.core.PageObject;
import com.rezrobot.utill.DriverFactory;

public class WEB_Car_ConfirmationPage extends PageObject{

	public boolean isAvailable()
	{
		try {
			Thread.sleep(20000);
			Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
			Thread.sleep(2000);
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			try {
				switchToDefaultFrame();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			boolean availabiltiy = getElementVisibility("label_error_id", 60);
			System.out.println(getElement("label_error_id").getText());
			getElement("button_error_xpath").click();
			return availabiltiy;	
			
		} catch (Exception e) {
			
			try {
				return getElementVisibility("label_confirmation_number_xpath", 60);		
			} catch (Exception e2) {
				// TODO: handle exception
				return false;
			}
		}		
	}
	
	public void getConfirmationNumber() throws Exception
	{
		System.out.println(getElement("label_confirmation_number_xpath").getText());
	}
}
