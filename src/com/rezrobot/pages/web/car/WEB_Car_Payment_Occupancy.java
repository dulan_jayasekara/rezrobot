package com.rezrobot.pages.web.car;

import com.rezrobot.core.PageObject;

public class WEB_Car_Payment_Occupancy extends PageObject{

	public boolean isAvailable()
	{
		try {
			return getElementVisibility("inputfield_first_name_id");
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public void next() throws Exception
	{
		getElement("button_next_classname").click();
	}
}
