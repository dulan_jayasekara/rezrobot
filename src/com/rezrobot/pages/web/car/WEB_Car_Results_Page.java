package com.rezrobot.pages.web.car;

import com.rezrobot.core.PageObject;

public class WEB_Car_Results_Page extends PageObject{
	
	public boolean checkResultsAvailability() throws Exception
	{
		switchToDefaultFrame();
		try {
			return getElementVisibility("label_number_or_results_id", 60);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return getElementVisibility("label_number_or_results_id", 60);
	}
	
	public void selectCar() throws Exception
	{
		getElements("button_book_classname").get(0).click();
		
	}
}
