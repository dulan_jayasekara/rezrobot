package com.rezrobot.pages.web.vacation;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.Vacation_Search_object;

public class WEB_Vacation_PaymentPage_MoreDetails extends PageObject{

	public HashMap<String, String> getFlightDetails()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			getElement("button_flight_more_details_xpath").click();
			values.put("adultCount", 	getElement("label_flight_adult_count_xpath").getText());
			try {
				values.put("childCount", 	getElement("label_flight_child_count_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				values.put("infantCount",	getElement("label_flight_infant_count_xpath").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			getElement("button_flight_more_details_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	public HashMap<String, String> getHotelDetails(Vacation_Search_object search)
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			getElement("button_hotel_more_details_xpath").click();
			values.put("checkin", 				getElement("label_hotel_checkin_id").getText());
			values.put("checkout", 				getElement("label_hotel_checkout_id").getText());
			String roomtype 	= "";
			String bedtype 		= "";
			String ratetype 	= "";
			String roomDetails 	= "";
			String cancellationpolicy = "";
			for(int i = 1 ; i <= Integer.parseInt(search.getRooms()) ; i++)
			{
				if(i == 1)
				{
					roomtype 	= getElement("label_hotel_room_type_xpath").changeRefAndGetElement(getElement("label_hotel_room_type_xpath").getRef().replace("replace", String.valueOf(i))).getText();
					bedtype 	= getElement("label_hotel_bed_type_xpath").changeRefAndGetElement(getElement("label_hotel_bed_type_xpath").getRef().replace("replace", String.valueOf(i))).getText();
					ratetype 	= getElement("label_hotel_rate_type_xpath").changeRefAndGetElement(getElement("label_hotel_rate_type_xpath").getRef().replace("replace", String.valueOf(i))).getText();
					roomDetails = getElement("label_hotel_room_details_xpath").changeRefAndGetElement(getElement("label_hotel_room_details_xpath").getRef().replace("replace", String.valueOf(i))).getText();
				}
				else
				{
					roomtype	= roomtype 		+ "," + getElement("label_hotel_room_type_xpath").changeRefAndGetElement(getElement("label_hotel_room_type_xpath").getRef().replace("replace", String.valueOf(i))).getText();
					bedtype		= bedtype 		+ "," + getElement("label_hotel_bed_type_xpath").changeRefAndGetElement(getElement("label_hotel_bed_type_xpath").getRef().replace("replace", String.valueOf(i))).getText();
					ratetype	= ratetype 		+ "," + getElement("label_hotel_rate_type_xpath").changeRefAndGetElement(getElement("label_hotel_rate_type_xpath").getRef().replace("replace", String.valueOf(i))).getText();
					roomDetails = roomDetails 	+ "," + getElement("label_hotel_room_details_xpath").changeRefAndGetElement(getElement("label_hotel_room_details_xpath").getRef().replace("replace", String.valueOf(i))).getText();
				}
			}
			values.put("roomType", 				roomtype);
			values.put("bedType", 				bedtype);
			values.put("rateType", 				ratetype);
			values.put("roomDetails",			roomDetails);
			values.put("cancellationDeadline", 	getElement("label_hotel_cancellation_deadline_id").getText());
			getElement("button_hotel_more_details_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	public HashMap<String, String> getCartDetails()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("sellingCurrency",  					getElement("label_cart_selling_currency_id").getText());
			values.put("subtotal", 							getElement("label_cart_subtotal_xpath").getText());
			values.put("taxAndOtherCharges", 				getElement("label_cart_total_tax_and_other_charges_id").getText());
			values.put("totalPackageCost", 					getElement("label_cart_total_package_cost_id").getText());
			values.put("amountBeingProcessedNow", 			getElement("label_cart_amount_being_processed_now_id").getText());
			values.put("amountBeingProcessedByAirline", 	getElement("label_cart_amount_being_processed_by_airline_id").getText());
			values.put("amountDueAtChecckin", 				getElement("label_cart_amount_due_at_checkin_id").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
}
