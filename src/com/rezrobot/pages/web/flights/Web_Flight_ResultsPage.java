package com.rezrobot.pages.web.flights;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.WebDriverWait;

//import system.classes.PopupMessage;















import com.rezrobot.flight_circuitry.*;
//import com.rezrobot.flightCircuitry.*;
import com.rezrobot.core.PageObject;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.pojo.Label;

/**
 * @author Dulan
 *
 */
public class Web_Flight_ResultsPage extends PageObject {

    
	String tracer							= "NULL";
	//String pageIteration					= "10";
	int Flight_Iterations					= 5;
	int Flight_PageIterations 				= 1;
	String numofflights						= "0";
	ArrayList<UIFlightItinerary> Resultlist	= new ArrayList<UIFlightItinerary>();
	String perPageFlights					= "";
	
	
	
	
	
	
    public int getFlight_Iterations() {
		return Flight_Iterations;
	}

	public void setFlight_Iterations(int flight_Iterations) {
		Flight_Iterations = flight_Iterations;
	}

	public int getFlight_PageIterations() {
		return Flight_PageIterations;
	}

	public void setFlight_PageIterations(int flight_PageIterations) {
		Flight_PageIterations = flight_PageIterations;
	}

	public String getNumofflights() {
		return numofflights;
	}

	public void setNumofflights(String numofflights) {
		this.numofflights = numofflights;
	}

	public ArrayList<UIFlightItinerary> getResultlist() {
		return Resultlist;
	}

	public void setResultlist(ArrayList<UIFlightItinerary> resultlist) {
		Resultlist = resultlist;
	}

	public String getPerPageFlights() {
		return perPageFlights;
	}

	public void setPerPageFlights(String perPageFlights) {
		this.perPageFlights = perPageFlights;
	}

	public void setTracer(String tracer) {
		this.tracer = tracer;
	}

	public String getTracer() {
		return tracer;
	}

	public void setTracer() {
		try {
			Thread.sleep(6000);
			this.tracer = this.objectMap.get("label_ResultPg_Tracer_id_1").getWebElement().getAttribute("value");
			
		} catch (Exception e) {
			this.tracer = "ERROR READ FAIL";
		}	
	}

	public boolean isPageAvailable() {
	   	
    	try {
	   	    return this.objectMap.get("inputfield_To_id").getWebElement().isDisplayed();
	   	} catch (Exception e) {
	   	    return false;
	   	}
	   	
    }
    
    public boolean getSearchAgainBECAvailability() {
		
		try {
			switchToDefaultFrame();
			return getElementVisibility("", 200);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
    
    public boolean isResultsAvailable() {
		
		try {
			switchToDefaultFrame();
			return getElementVisibility("div_FlightItinerary_article_class", 30);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
    
    public HashMap<String, Boolean> getAvailabilityFields()  {

		HashMap<String, Boolean> fields = new HashMap<>();
		
		try {
			fields.put("AddHotels", getElementVisibility("label_AddHotels_xpath"));
			fields.put("AddFlights", getElementVisibility("label_AddFlights_xpath"));
			fields.put("AddActivities", getElementVisibility("label_AddActivities_xpath"));
			fields.put("AddCars", getElementVisibility("label_AddCars_xpath"));
			fields.put("AddTransfers", getElementVisibility("label_AddTransfers_xpath"));
			
			fields.put("SearchAgainTripType", getElementVisibility("combobox_TripType_class"));
			fields.put("SearchAgainFrom", getElementVisibility("inputfield_From_id"));
			fields.put("SearchAgainTo", getElementVisibility("inputfield_To_id"));
			fields.put("SearchAgainDateRange", getElementVisibility("inputfield_DateRange_id"));
			fields.put("SearchAgainAdultChildCount", getElementVisibility("inputfield_Pax_class"));
			fields.put("ShowAdditionFilter", getElementVisibility("link_ShowAdditionFilter_linktext"));
			try {
				getElement("link_ShowAdditionFilter_linktext").click();
			} catch (Exception e) {
				e.printStackTrace();
			}
			fields.put("SearchAgainCabinClass", getElementVisibility("combobox_cabinclass_id"));
			fields.put("SearchAgainPrefAirline", getElementVisibility("inputfield_prefAirline_id"));
			fields.put("SearchAgainPrefCurrency", getElementVisibility("combobox_prefCurrency_id"));
			fields.put("SearchAgainDiscount", getElementVisibility("inputfield_discount_id"));
			fields.put("HideAdditionFilter", getElementVisibility("link_HideAdditionFilter_linktext"));
			fields.put("SearchAgainPrefNonStop", getElementVisibility("checkbox_prefNonStop_id"));
			fields.put("SearchAgainButton", getElementVisibility("button_SearchAgain_class"));
						
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fields;
	}
    
    public HashMap<String,String> getLabels(){
    	
    	HashMap<String, String> labels = new HashMap<>();
    	try {
			labels.put("AddHotels", getLabelName("label_AddHotels_xpath"));
			labels.put("AddFlights", getLabelName("label_AddFlights_xpath"));
			labels.put("AddActivities", getLabelName("label_AddActivities_xpath"));
			labels.put("AddCars", getLabelName("label_AddCars_xpath"));
			labels.put("AddTransfers", getLabelName("label_AddTransfers_xpath"));		
			
			labels.put("hideadditionalfilter", getLabelName("link_HideAdditionFilter_linktext"));
			getElement("link_HideAdditionFilter_linktext").click();
			labels.put("ShowAdditionFilter", getLabelName("link_ShowAdditionFilter_linktext"));
			getElement("link_ShowAdditionFilter_linktext").click();
			labels.put("cabinclass", getLabelName("label_cabinclass_xpath"));
			labels.put("prefairline", getLabelName("label_prefAirline_xpath"));
			labels.put("prefcurrency", getLabelName("label_prefCurrency_xpath"));			
			labels.put("discount", getLabelName("label_discount_xpath"));
			labels.put("prefnonstop", getLabelName("label_prefNonStop_xpath"));
			labels.put("resultsavailablefromto", getLabelName("label_resultsAvailableFromTo_id"));
			labels.put("resultsavailable", getLabelName("label_resultsAvailable_class"));			
			labels.put("sortprice", getLabelName("label_sortPrice_xpath"));
			labels.put("sortby", getLabelName("label_sortBy_xpath"));
			labels.put("mybasket", getLabelName("label_myBasket_xpath"));
			labels.put("noitemselected", getLabelName("label_noItemsSelected_xpath"));
			labels.put("resetfiltertop", getLabelName("label_resetFiltersTop_xpath"));
			labels.put("pricerange", getLabelName("label_priceRange_xpath"));
			labels.put("fromtopricefilter", getLabelName("label_fromToPrice_id"));
			labels.put("filterheadingairline", getLabelName("label_airlines_xpath"));
			labels.put("filterheadingduration", getLabelName("label_duration_xpath"));
			labels.put("filterheadinglayover", getLabelName("label_layover_xpath"));
			labels.put("filterheadingflightleg", getLabelName("label_flightleg_xpath"));
			labels.put("filterheadingflighttimes", getLabelName("label_flightTimes_xpath"));
			labels.put("seg1takeoff", getLabelName("label_Seg1takeOffFlight_id"));
			labels.put("seg1landing", getLabelName("label_Seg1landingFlight_id"));
			getElement("button_flightTimeSegment2_id").click();
			labels.put("seg2takeoff", getLabelName("label_Seg2takeOffFlight_id"));
			labels.put("seg2landing", getLabelName("label_Seg2landingFlight_id"));
			labels.put("resetfiltersbottom", getLabelName("label_resetFiltersBottom_xpath"));
			labels.put("showmatrix", getLabelName("label_showMatrix_id"));
			getElement("label_showMatrix_id").click();
			labels.put("hidematrix", getLabelName("label_hideMatrix_id"));
			labels.put("nonstop", getLabelName("label_nonstop_xpath"));
			labels.put("1stop", getLabelName("label_1stop_xpath"));
			labels.put("2stop", getLabelName("label_2+stop_xpath"));
			labels.put("showingpage", getLabelName("label_showingPage_xpath"));
			labels.put("flightdetailslink", getLabelName("label_flightdetails_xpath"));
						
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return labels;
    }
    
    public HashMap<String, String> getComboBoxDataList() throws Exception {

		HashMap<String, String> comboFields = new HashMap<>();
		
		comboFields.put("searchAgainTripType", getComboBoxDataList("combobox_TripType_class"));
		comboFields.put("searchAgainCabinClass", getComboBoxDataList("combobox_cabinclass_id"));
		comboFields.put("searchAgainPrefCurrency", getComboBoxDataList("combobox_prefCurrency_id"));
		
		return comboFields;
	}
    
    public HashMap<String, String> getAfterSearchSetValues() throws Exception {

		HashMap<String, String> placeHolders = new HashMap<>();
		placeHolders.put("searchAgainTripType", ((ComboBox)getElement("combobox_TripType_class")).getDefaultSelectedOption());
		placeHolders.put("searchAgainFrom", getElement("inputfield_From_id").getWebElement().getAttribute("value"));
		placeHolders.put("searchAgainTo", getElement("inputfield_To_id").getWebElement().getAttribute("value"));
		placeHolders.put("searchAgainDateRange", getElement("inputfield_DateRange_id").getWebElement().getAttribute("value"));
		placeHolders.put("searchAgainPax", getElement("inputfield_Pax_class").getWebElement().getText());
		placeHolders.put("searchAgainCabinClass", ((ComboBox)getElement("combobox_cabinclass_id")).getDefaultSelectedOption());
		placeHolders.put("searchAgainPrefAirline", getElement("inputfield_prefAirline_id").getWebElement().getAttribute("value"));
		placeHolders.put("searchAgainPrefCurrency", ((ComboBox)getElement("combobox_prefCurrency_id")).getDefaultSelectedOption());
		placeHolders.put("searchAgainDiscount", getElement("inputfield_discount_id").getWebElement().getAttribute("value"));
		
		return placeHolders;
	}
    
    public boolean addToCart(int n, int selector)
	{
    	boolean add = false;
    	
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//PopupMessage popup = new PopupMessage();
		//boolean done = false;
		int navcount1 = n/10;//Per page 10 find how many pages to navigate
		//System.out.println("pages :"+navcount1);
		int navcount2 = n%10;//Remaining flights
		//System.out.println("remainder :"+navcount2);
		int navigate = navcount1;
		if((navcount2>0)&&(navcount2<10))
		{
			navigate+=1;
		}
		int clickpattern = 2; 
		//System.out.println("NAVIGATE  :  "+navigate);
		int count = 0;
		ArrayList<WebElement> button = null;
		while(count<navigate)
		{
			try
			{	
				button = new ArrayList<WebElement>(getElement("button_ItineraryAddAndContinue_class").getWebElements());
				try {
					if(button.size()>=selector)
					{
						try {
							button.get(selector-1).click();
						} catch (Exception e) {
							//e.printStackTrace();
							System.out.println("addToCart - button.get(selector-1).click();");
						}
						
						Thread.sleep(3000);
						
						try {
							//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("upselling_dig_WJ_4")));
							try {
								Thread.sleep(3000);
								//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("upselling_dig_WJ_4"))));
							} catch (Exception e) {
								//e.printStackTrace();
								System.out.println("addToCart - driver.findElement(By.id(upselling_dig_WJ_4))");
							}
							getelementAvailability("div_AddProduct_id");
							getelementPresence(getElement("div_AddProduct_id"));
							if(getElementVisibility("div_AddProduct_id", 5000))
							{
								try {
									ArrayList<WebElement> ele = new ArrayList<WebElement> (getElements("div_AddProductElements_id"));
									ele.get(0).findElement(By.className("fa-times")).click();
								} catch (Exception e) {
									e.printStackTrace();
									try {
										ArrayList<WebElement> ele = new ArrayList<WebElement> (getElements("div_AddProductElements_id"));
										ele.get(0).findElement(By.className("fa-times")).click();
									} catch (Exception e2) {
										e2.printStackTrace();
									}
								}
								
							}/**/
							//popup = popupHandler(driver);
						} catch (Exception e) {
							System.out.println("addToCart");
						}
						
						break;
					}
				} catch (Exception e) {
					//e.printStackTrace();
					System.out.println("addToCart - if(button.size()>selector)");
				}
				
			}	
			catch(Exception e)
			{
				//e.printStackTrace();
				System.out.println("button = new ArrayList<WebElement>(driver.findElements(By.className(continue-shopping-btn)))");
			}

			executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			clickpattern++;		
			count++; 

		}
		
		clickpattern = 1;
		executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		
		
		
		if(getElementVisibility("div_AddProduct_id", 5000))
		{
			try {
				ArrayList<WebElement> ele = new ArrayList<WebElement> (getElements("div_AddProductElements_id"));
				ele.get(0).findElement(By.className("fa-times")).click();
			} catch (Exception e) {
				try {
					ArrayList<WebElement> ele = new ArrayList<WebElement> (getElements("div_AddProductElements_id"));
					ele.get(0).findElement(By.className("fa-times")).click();
				} catch (Exception e2) {
					
				}
			}
			
		}
		
		return add;
		
	}
    
    public void setResults(SearchObject searchObject, int pageIterations, int flightIterations){
		
    	this.Flight_Iterations = flightIterations;
    	this.Flight_PageIterations = pageIterations;
    	
    	boolean twoway = false;
    	
    	if(searchObject.getTriptype().equalsIgnoreCase("Round Trip")) {
    		twoway = true;
    	}
    	
		try {
			
			if(searchObject.getTriptype().equalsIgnoreCase("Round Trip")) {
				twoway = true;
			}
			
			try {
				
				int nflights = 0;
				
				numofflights = ((Label)getElement("label_FlightCount_class")).getText();
				
				nflights = Integer.parseInt(numofflights.trim());//Get no of flights
				int navcount1 = nflights/10;//Per page 10 find how many pages to navigate
				int navcount2 = nflights%10;//Remaining flights
				
				int navigate = navcount1;
				
				if((navcount2>0)&&(navcount2<10))
				{
					navigate+=1;
				}
				int clickpattern = 2; 
				int count = 0;
				
				if(Flight_PageIterations <= navigate)
					navigate	= Flight_PageIterations;
				
				while(count<navigate)
				{
					try
					{	
						ArrayList<WebElement> flights = new ArrayList<WebElement>(getElements("div_FlightItinerary_article_class"));
						perPageFlights = String.valueOf(flights.size());
						
						Iterator<WebElement> flightsIter = flights.iterator();
						int nt = 1;
						int index = 0;
						if( flights.size() <= Flight_Iterations )
						{
							this.Flight_Iterations = flights.size();
						}
						whileloop:while(flightsIter.hasNext())
						{
							if(nt > Flight_Iterations)
							break whileloop;
							
							UIFlightItinerary flightItinerary = new UIFlightItinerary();
							//Thread.sleep(5000);
							flightItinerary.setOutDuration_Label(flights.get(index).findElement(By.className(getElement("label_OutDurationLabel_class").getRef().trim())).getText());
							flightItinerary.setOutLayover_Label(flights.get(index).findElement(By.className(getElement("label_OutLayoverLabel_class").getRef().trim())).getText());
							flightItinerary.setOutStops_Label(flights.get(index).findElement(By.className(getElement("label_OutStopsLabel_class").getRef().trim())).getText());
							
							
							String OBDepFlightCode		= ""; 
							OBDepFlightCode				= getElement("label_OutDepFlightCode_id").changeRefAndGetElement(referenceChange("label_OutDepFlightCode_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBDepFlightCode(OBDepFlightCode);
						
							
							String OBDepLocationCode	= "";
							OBDepLocationCode			= getElement("label_OutDepLocCode_id").changeRefAndGetElement(referenceChange("label_OutDepLocCode_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBDepLocationCode(OBDepLocationCode);
							
							String OBDepDate			= "";
							OBDepDate					= getElement("label_OutDepDate_id").changeRefAndGetElement(referenceChange("label_OutDepDate_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBDepDate(OBDepDate);
							
							String OBDepTime			= "";
							OBDepTime					= getElement("label_OutDepTime_id").changeRefAndGetElement(referenceChange("label_OutDepTime_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBDepTime(OBDepTime);
							
							String OBArrFlightCode		= "";
							OBArrFlightCode				= getElement("label_OutArrFlightCode_id").changeRefAndGetElement(referenceChange("label_OutArrFlightCode_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBArrFlightCode(OBArrFlightCode);
							
							String OBArrLocationCode	= "";
							OBArrLocationCode			= getElement("label_OutArrLocCode_id").changeRefAndGetElement(referenceChange("label_OutArrLocCode_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBArrLocationCode(OBArrLocationCode);
							
							String OBArrDate			= "";
							OBArrDate					= getElement("label_OutArrDate_id").changeRefAndGetElement(referenceChange("label_OutArrDate_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBArrDate(OBArrDate);
							
							String OBArrTime			= "";
							OBArrTime					= getElement("label_OutArrTime_id").changeRefAndGetElement(referenceChange("label_OutArrTime_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBArrTime(OBArrTime);
							
							String OBDuration			= "";
							OBDuration					= getElement("label_OutDuration_id").changeRefAndGetElement(referenceChange("label_OutDuration_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBDuration(OBDuration);
							
							String OBStops				= "";
							OBStops						= getElement("label_OutStops_id").changeRefAndGetElement(referenceChange("label_OutStops_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBStops(OBStops);
							
							String OBLayover			= "";
							OBDuration					= getElement("label_OutLayover_id").changeRefAndGetElement(referenceChange("label_OutLayover_id", String.valueOf(index + 1))).getText();
							flightItinerary.setOBLayover(OBLayover);
							
							
							if(twoway)
							{
								//flights.get(index).findElement(By.className(getElement("label_InDurationLabel_class").getRef())).getText();
								//flights.get(index).findElement(By.className(getElement("label_InStopsLabel_class").getRef())).getText();
								//flights.get(index).findElement(By.className(getElement("label_InLayoverLabel_class").getRef())).getText();
								flightItinerary.setInDuration_Label(flights.get(index).findElement(By.className(getElement("label_InDurationLabel_class").getRef().trim())).getText());
								flightItinerary.setInLayover_Label(flights.get(index).findElement(By.className(getElement("label_InLayoverLabel_class").getRef().trim())).getText());
								flightItinerary.setInStops_Label(flights.get(index).findElement(By.className(getElement("label_InStopsLabel_class").getRef().trim())).getText());
								
								String IBDepFlightCode		= "";
								IBDepFlightCode				= getElement("label_InDepFlightCode_id").changeRefAndGetElement(referenceChange("label_InDepFlightCode_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBDepFlightCode(IBDepFlightCode);
								
								String IBDepLocationCode	= "";
								IBDepLocationCode			= getElement("label_InDepLocCode_id").changeRefAndGetElement(referenceChange("label_InDepLocCode_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBDepLocationCode(IBDepLocationCode);
								
								String IBDepDate			= "";
								IBDepDate					= getElement("label_InDepDate_id").changeRefAndGetElement(referenceChange("label_InDepDate_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBDepDate(IBDepDate);
								
								String IBDepTime			= "";
								IBDepTime					= getElement("label_InDepTime_id").changeRefAndGetElement(referenceChange("label_InDepTime_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBDepTime(IBDepTime);
								
								String IBArrFlightCode		= "";
								IBArrFlightCode				= getElement("label_InArrFlightCode_id").changeRefAndGetElement(referenceChange("label_InArrFlightCode_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBArrFlightCode(IBArrFlightCode);
								
								String IBArrLocationCode	= "";
								IBArrLocationCode			= getElement("label_InArrLocCode_id").changeRefAndGetElement(referenceChange("label_InArrLocCode_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBArrLocationCode(IBArrLocationCode);
								
								String IBArrDate			= "";
								IBArrDate					= getElement("label_InArrDate_id").changeRefAndGetElement(referenceChange("label_InArrDate_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBArrDate(IBArrDate);
								
								String IBArrTime			= "";
								IBArrTime					= getElement("label_InArrTime_id").changeRefAndGetElement(referenceChange("label_InArrTime_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBArrTime(IBArrTime);
								
								String IBDuration			= "";
								IBDuration					= getElement("label_InDuration_id").changeRefAndGetElement(referenceChange("label_InDuration_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBDuration(IBDuration);
								
								String IBStops				= "";
								IBStops						= getElement("label_InStops_id").changeRefAndGetElement(referenceChange("label_InStops_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBStops(IBStops);
								
								String IBLayover			= "";
								IBLayover					= getElement("label_InLayover_id").changeRefAndGetElement(referenceChange("label_InLayover_id", String.valueOf(index + 1))).getText();
								flightItinerary.setIBLayover(IBLayover);	
							}
							
							
							String currencyCode = "";
							String cost			= "0";
							try {
							
								currencyCode		= getElement("label_ItineraryCurrencyCode_xpath").changeRefAndGetElement(referenceChange("label_ItineraryCurrencyCode_xpath", String.valueOf(index + 1))).getText().trim();
								cost				= getElement("label_ItineraryPrice_xpath").changeRefAndGetElement(referenceChange("label_ItineraryPrice_xpath", String.valueOf(index + 1))).getText().trim();
							} catch (Exception e) {
								e.printStackTrace();
							}
							flightItinerary.setCurrencyCode(currencyCode);
							flightItinerary.setTotalCost(cost);
							
							getElement("label_MoreFlightDetails_xpath").changeRefAndGetElement(referenceChange("label_MoreFlightDetails_xpath", String.valueOf(index + 1))).click();
							
							FlightMoreDetails flightDetails = new FlightMoreDetails();
							setFlightMoreDetails(flightDetails, flights.get(index), twoway);
							flightItinerary.setMoreDetails(flightDetails);
							
							Resultlist.add(flightItinerary);
							nt++;
							index++;
						}	
					}	
					catch(Exception e)
					{
						e.printStackTrace();
						//Screenshot.takeScreenshot(scenarioCommonPath + "/Result page read fail.jpg", driver);
					}

					executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
					clickpattern++;		
					count++; 

				}
				
				clickpattern = 1;
				executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//return true;
		
			System.out.println(Resultlist.size());
			
		} catch (Exception e) {
			e.getMessage();
		}	
	}

    public HashMap<String,String> getCartDetails()
    {
    	HashMap<String, String> cartLabels = new HashMap<>();
    	try {
    		
    		String actualBasketPrice = getLabelName("label_BasketPrice_class").trim().split(" ")[1];
    		cartLabels.put("ActualBasketPrice", actualBasketPrice);
    		String actualBasketPriceCurrency = getLabelName("").trim().split(" ")[0];
    		cartLabels.put("ActualBasketPriceCurrency", actualBasketPriceCurrency);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return cartLabels;
    }
    
    public boolean searchAgain(SearchObject searchObject)
	{
		try {
			String trip = "";
			if(searchObject.getTriptype().equalsIgnoreCase("One Way Trip"))
			{
				trip = "oneway";
			}
			else if(searchObject.getTriptype().equalsIgnoreCase("Round Trip"))
			{
				trip = "roundtrip";
			}
			else if(searchObject.getTriptype().equalsIgnoreCase("Multi Destination"))
			{
				trip = "multidestination";
			}
			
			// Trip Type Search Again
			((ComboBox)getElement("combobox_TripType_class")).selectOptionByText(trip);
			((InputFiled)getElement("inputfield_From_id")).clearInput();
			((InputFiled)getElement("inputfield_From_id")).setText(searchObject.getFrom().trim().split("\\|")[0]);
			getElement("hiddenlabel_From_id").setJavaScript("$('#"+getElement("hiddenlabel_From_id").getRef().trim()+"').val('"+searchObject.getFrom()+"');");
			((InputFiled)getElement("inputfield_To_id")).clearInput();
			((InputFiled)getElement("inputfield_To_id")).setText(searchObject.getTo().trim().split("\\|")[0]);
			getElement("hiddenlabel_To_id").setJavaScript("$('#"+getElement("hiddenlabel_To_id").getRef().trim()+"').val('"+searchObject.getTo()+"');");
			
			getElement("button_SearchAgain_class").click();
			
		} catch (Exception e) {
			
		}
		
		return true;
	}
  
    public String referenceChange(String reference, String valueToBeReplaced) {
    	
    	String newReference = getElement(reference).getRef();
    	newReference = newReference.replace("REPLACE", valueToBeReplaced);
    	
    	return newReference;
    	
    }
    
    public void setFlightMoreDetails(FlightMoreDetails flightDetails, WebElement flights, boolean twoway) {
    	
    	try {
			flightDetails.setMoreDetailsTableHeader_Label(flights.findElement(By.className(getElement("label_MoreDetailsTableHeader_class").getRef())).getText());
			flightDetails.setMoreDetailsTableColumnHeaders_Label(flights.findElement(By.className(getElement("label_MoreDetailsTableColumnHeaders_class").getRef())).getText());
		} catch (Exception e2) {
			e2.printStackTrace();
		}
    	
    	
    	try {
			//System.out.println(getElement("label_MoreOutStopsLabel_id").getRef());
			//System.out.println(getElement("label_MoreInStopsLabel_id").getRef());
			flightDetails.setOutboundNonStop_Label(flights.findElement(By.id(getElement("label_MoreOutStopsLabel_id").getRef())).getText());
			flightDetails.setInboundNonStop_Label(flights.findElement(By.id(getElement("label_MoreInStopsLabel_id").getRef())).getText());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    	
		try {
			
			ArrayList<WebElement> outboundRows = new ArrayList<WebElement>(flights.findElements(By.className(getElement("div_MoreOutDetails_Row_class").getRef())));
			
			//System.out.println("Cabin class : "+outboundRows.get(0).findElement(By.className(getElement("label_MoreOutCabinClass_class").getRef())).getText());
			flightDetails.setOutboundCabinClass_Label(outboundRows.get(0).findElement(By.className(getElement("label_MoreOutCabinClass_class").getRef())).getText());
			
			outboundRows = new ArrayList<WebElement>(outboundRows.get(0).findElements(By.className(getElement("div_MoreOutDetails_RowRow_class").getRef()))); 
			
			Outbound outbound = new Outbound();
			
			ArrayList<Flight> outboundFlights = new ArrayList<Flight>(); 
			
			for(int out = 0; out<outboundRows.size(); out++) 
			{
				Flight flight = new Flight();
				
				String airLine = "";
				airLine = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutAirline_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText();
				flight.setMarketingAirline(airLine);
				
				String depDate = "";
				depDate = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutDepDatetime_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[0].trim();
				flight.setDepartureDate(depDate);
				String depTime = "";
				depTime = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutDepDatetime_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[1].replace("hrs", "").trim();
				flight.setDepartureTime(depTime);
				
				String depLocation = "";
				depLocation = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutDepLoc_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText();
				flight.setDeparture_port(depLocation);
				
				String depLocationCode = "";
				depLocationCode = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutDepLoc_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText();
				flight.setDepartureLocationCode(depLocationCode);
				
				String arrDate = "";
				arrDate = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutArrDateTime_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[0].trim();
				flight.setArrivalDate(arrDate);
				
				String arrTime = "";
				arrTime = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutArrDateTime_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[1].replace("hrs", "").trim();
				flight.setArrivalTime(arrTime);
				
				String arrLocation = "";
				arrLocation = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutArrLoc_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText();
				flight.setArrival_port(arrLocation);
				
				String arrLocationCode = "";
				arrLocationCode = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutArrLoc_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText();
				flight.setArrivalLocationCode(arrLocationCode);
				
				String flightNo = "";
				flightNo = outboundRows.get(out).findElement(By.id(getElement("label_MoreOutAirline_id").getRef().trim().replace("REPLACE", String.valueOf(out)))).getText();
				flight.setFlightNo(flightNo);
				
				outboundFlights.add(flight);
			}
			outbound.setOutBflightlist(outboundFlights);
			flightDetails.setOutbound(outbound);
		} catch (Exception e) {
			e.printStackTrace();
			/*Screenshot.takeScreenshot(scenarioCommonPath + "/More details "+nt+" read fail.jpg", driver);*/
		}
		
		if(twoway)
		{
			try {
				
				ArrayList<WebElement> inboundRows =  new ArrayList<WebElement>(flights.findElements(By.className(getElement("div_MoreInDetails_Row_class").getRef().trim())));
				
				//System.out.println("Cabin class : "+inboundRows.get(0).findElement(By.className(getElement("label_MoreOutCabinClass_class").getRef())).getText());
				flightDetails.setInboundCabinClass_Label(inboundRows.get(0).findElement(By.className(getElement("label_MoreInCabinClass_class").getRef())).getText());
				
				inboundRows =  new ArrayList<WebElement>(inboundRows.get(0).findElements(By.className(getElement("div_MoreInDetails_RowRow_class").getRef().trim())));
				
				Inbound inbound = new Inbound();
				
				ArrayList<Flight> inboundFlights = new ArrayList<Flight>(); 
				
				for(int in = 0; in<inboundRows.size(); in++) 
				{
					Flight flight = new Flight();
					
					String airLine = "";
					airLine = inboundRows.get(in).findElement(By.id(getElement("label_MoreInAirline_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText();
					flight.setMarketingAirline(airLine);
					
					String depDate = "";
					depDate = inboundRows.get(in).findElement(By.id(getElement("label_MoreInDepDatetime_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[0].trim();
					flight.setDepartureDate(depDate);
					String depTime = "";
					depTime = inboundRows.get(in).findElement(By.id(getElement("label_MoreInDepDatetime_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[1].replace("hrs", "").trim();
					flight.setDepartureTime(depTime);
					
					String depLocation = "";
					depLocation = inboundRows.get(in).findElement(By.id(getElement("label_MoreInDepLoc_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText();
					flight.setDeparture_port(depLocation);
					String depLocationCode = "";
					depLocationCode = inboundRows.get(in).findElement(By.id(getElement("label_MoreInDepLoc_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText();
					flight.setDepartureLocationCode(depLocationCode);
					
					String arrDate = "";
					arrDate = inboundRows.get(in).findElement(By.id(getElement("label_MoreInArrDateTime_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[0].trim();
					flight.setArrivalDate(arrDate);
					
					String arrTime = "";
					arrTime = inboundRows.get(in).findElement(By.id(getElement("label_MoreInArrDateTime_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[1].replace("hrs", "").trim();
					flight.setArrivalTime(arrTime);
					
					String arrLocation = "";
					arrLocation = inboundRows.get(in).findElement(By.id(getElement("label_MoreInArrLoc_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText();
					flight.setArrival_port(arrLocation);
					
					String arrLocationCode = "";
					arrLocationCode = inboundRows.get(in).findElement(By.id(getElement("label_MoreInArrLoc_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText();
					flight.setArrivalLocationCode(arrLocationCode);
					
					String flightNo = "";
					flightNo = inboundRows.get(in).findElement(By.id(getElement("label_MoreInAirline_id").getRef().trim().replace("REPLACE", String.valueOf(in)))).getText();
					flight.setFlightNo(flightNo);
					
					inboundFlights.add(flight);
				}
				inbound.setInBflightlist(inboundFlights);
				flightDetails.setInbound(inbound);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
    	
    }
    
    public void checkoutFlight()
    {
    	try {
    		try {
				if(getElement("div_AddProduct_id").isElementVisible())
				{
					this.closeAddProductScreen();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
    		
    		getElementVisibility("button_Checkout_class", 10000);
			getElement("button_Checkout_class").click();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void closeAddProductScreen()
    {
    	try {
    		Thread.sleep(5000);
			if(getElementVisibility("div_AddProduct_id", 10000))
			{
				try {
					ArrayList<WebElement> ele = new ArrayList<WebElement> (getElements("div_AddProductElements_id"));
					ele.get(0).findElement(By.className("fa-times")).click();
					getElementVisibility("button_Checkout_class", 10000);
				} catch (Exception e) {
					try {
						ArrayList<WebElement> ele = new ArrayList<WebElement> (getElements("div_AddProductElements_id"));
						ele.get(0).findElement(By.className("fa-times")).click();
						getElementVisibility("button_Checkout_class", 10000);
					} catch (Exception e2) {
						
					}
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}
    
    
