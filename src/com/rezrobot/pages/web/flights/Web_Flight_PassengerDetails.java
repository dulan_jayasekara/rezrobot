package com.rezrobot.pages.web.flights;

import com.rezrobot.core.PageObject;
import com.rezrobot.flight_circuitry.*;
import com.rezrobot.pojo.ComboBox;

public class Web_Flight_PassengerDetails extends PageObject  {

	
	public void setPassengerDetails(ReservationPassengerInfo passengerNames ){
		
		int adults		= passengerNames.getAdult().size();
		int children	= passengerNames.getChildren().size();
		int infants		= passengerNames.getInfant().size();
		
		try {
			
			String PayPg_OccupancyADTFname_TxtBox_id = objectMap.get("PayPg_OccupancyADTFname_TxtBox_id").getRef();
			String PayPg_OccupancyADTLname_TxtBox_id = objectMap.get("PayPg_OccupancyADTLname_TxtBox_id").getRef();
			String PayPg_OccupancyADTContact_TxtBox_id = objectMap.get("PayPg_OccupancyADTContact_TxtBox_id").getRef();
			String PayPg_OccupancyADTPassportNo_TxtBox_id = objectMap.get("PayPg_OccupancyADTPassportNo_TxtBox_id").getRef();
			String PayPg_OccupancyADTPassportExp_TxtBox_id = objectMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").getRef();
			String PayPg_OccupancyADTPassportIssue_TxtBox_id = objectMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").getRef();
			String PayPg_OccupancyADTBdate_TxtBox_id = objectMap.get("PayPg_OccupancyADTBdate_TxtBox_id").getRef();
			String PayPg_OccupancyADTNationality_DropDwn_id = objectMap.get("PayPg_OccupancyADTNationality_DropDwn_id").getRef();
			String PayPg_OccupancyADTPassIssuCounrty_DropDwn_id = objectMap.get("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").getRef();
			
			PayPg_OccupancyADTFname_TxtBox_id = PayPg_OccupancyADTFname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTLname_TxtBox_id = PayPg_OccupancyADTLname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTContact_TxtBox_id = PayPg_OccupancyADTContact_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassportNo_TxtBox_id = PayPg_OccupancyADTPassportNo_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassportExp_TxtBox_id = PayPg_OccupancyADTPassportExp_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassportIssue_TxtBox_id = PayPg_OccupancyADTPassportIssue_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTBdate_TxtBox_id = PayPg_OccupancyADTBdate_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTNationality_DropDwn_id = PayPg_OccupancyADTNationality_DropDwn_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassIssuCounrty_DropDwn_id = PayPg_OccupancyADTPassIssuCounrty_DropDwn_id.replace("REPLACE", "0");
			
			for(int a = 0; a<adults; a++)
			{
				if(a != 0)
				{
					String stra = String.valueOf(a-1);
					PayPg_OccupancyADTFname_TxtBox_id = PayPg_OccupancyADTFname_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTLname_TxtBox_id = PayPg_OccupancyADTLname_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTContact_TxtBox_id = PayPg_OccupancyADTContact_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassportNo_TxtBox_id = PayPg_OccupancyADTPassportNo_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassportExp_TxtBox_id = PayPg_OccupancyADTPassportExp_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassportIssue_TxtBox_id = PayPg_OccupancyADTPassportIssue_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTBdate_TxtBox_id = PayPg_OccupancyADTBdate_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTNationality_DropDwn_id = PayPg_OccupancyADTNationality_DropDwn_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassIssuCounrty_DropDwn_id = PayPg_OccupancyADTPassIssuCounrty_DropDwn_id.replace(stra, String.valueOf(a));
				}
				
				System.out.println("INFO -> adult : "+a+1);
				//new Select(getElement("PayPg_ProceedToOccupancy_Btn_xpath").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
				//System.out.println(propertyMap.get("PayPg_OccupancyADTFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)));
				//((ComboBox)getElement("combobox_ChildrenAge_DropDwn_id").changeRefAndGetElement(value)).selectOptionByText(searchObject.getChildrenAge().get((i - 1)).trim());
				
				getElement("PayPg_OccupancyADTFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTFname_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTFname_TxtBox_id).setText(passengerNames.getAdult().get(a).getGivenName());
				getElement("PayPg_OccupancyADTLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTLname_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTLname_TxtBox_id).setText(passengerNames.getAdult().get(a).getSurname());
				getElement("PayPg_OccupancyADTContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTContact_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTContact_TxtBox_id).setText(passengerNames.getAdult().get(a).getPhoneNumber());
				getElement("PayPg_OccupancyADTPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportNo_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportNo_TxtBox_id).setText(passengerNames.getAdult().get(a).getPassportNo());

				//executejavascript(script);
				//getElement("PayPg_OccupancyADTPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportExp_TxtBox_id).setJavaScript("$('#"+getElement("PayPg_OccupancyADTPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportExp_TxtBox_id)+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				getElement("PayPg_OccupancyADTPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportExp_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyADTPassportExp_TxtBox_id+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				
				getElement("PayPg_OccupancyADTPassportIssue_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportIssue_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyADTPassportIssue_TxtBox_id+"').val('"+passengerNames.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				
				getElement("PayPg_OccupancyADTBdate_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTBdate_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyADTBdate_TxtBox_id+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				
				((ComboBox) getElement("PayPg_OccupancyADTGender_DropDwn_class")).selectOptionByText(passengerNames.getAdult().get(a).getGender());
				//new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(passengerNames.getAdult().get(a).getGender());
	//			((ComboBox) getElement("PayPg_OccupancyADTNationality_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyADTNationality_DropDwn_id)).selectOptionByText(passengerNames.getAdult().get(a).getNationality());
				//new Select(getElement("PayPg_OccupancyADTNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getNationality());
	//			((ComboBox) getElement("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyADTPassIssuCounrty_DropDwn_id)).selectOptionByText(passengerNames.getAdult().get(a).getPassportIssuCountry());
				//new Select(getElement("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getPassportIssuCountry());
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	
	
		try {
			
			String PayPg_OccupancyCHDFname_TxtBox_id = objectMap.get("PayPg_OccupancyCHDFname_TxtBox_id").getRef();
			String PayPg_OccupancyCHDLname_TxtBox_id = objectMap.get("PayPg_OccupancyCHDLname_TxtBox_id").getRef();
			String PayPg_OccupancyCHDContact_TxtBox_id = objectMap.get("PayPg_OccupancyCHDContact_TxtBox_id").getRef();
			String PayPg_OccupancyCHDPassportNo_TxtBox_id = objectMap.get("PayPg_OccupancyCHDPassportNo_TxtBox_id").getRef();
			String PayPg_OccupancyCHDPassportExp_TxtBox_id = objectMap.get("PayPg_OccupancyCHDPassportExp_TxtBox_id").getRef();
			String PayPg_OccupancyCHDPassportIssue_TxtBox_id = objectMap.get("PayPg_OccupancyCHDPassportIssue_TxtBox_id").getRef();
			String PayPg_OccupancyCHDBdate_TxtBox_id = objectMap.get("PayPg_OccupancyCHDBdate_TxtBox_id").getRef();
			String PayPg_OccupancyCHDNationality_DropDwn_id = objectMap.get("PayPg_OccupancyCHDNationality_DropDwn_id").getRef();
			String PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id = objectMap.get("PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id").getRef();
			
			PayPg_OccupancyCHDFname_TxtBox_id = PayPg_OccupancyCHDFname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDLname_TxtBox_id = PayPg_OccupancyCHDLname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDContact_TxtBox_id = PayPg_OccupancyCHDContact_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassportNo_TxtBox_id = PayPg_OccupancyCHDPassportNo_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassportExp_TxtBox_id = PayPg_OccupancyCHDPassportExp_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassportIssue_TxtBox_id = PayPg_OccupancyCHDPassportIssue_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDBdate_TxtBox_id = PayPg_OccupancyCHDBdate_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDNationality_DropDwn_id = PayPg_OccupancyCHDNationality_DropDwn_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id = PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id.replace("REPLACE", "0");
			
			for(int c=0; c<children; c++)
			{
				
				if(c != 0)
				{
					String strc = String.valueOf(c-1);
					PayPg_OccupancyCHDFname_TxtBox_id = PayPg_OccupancyCHDFname_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDLname_TxtBox_id = PayPg_OccupancyCHDLname_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDContact_TxtBox_id = PayPg_OccupancyCHDContact_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassportNo_TxtBox_id = PayPg_OccupancyCHDPassportNo_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassportExp_TxtBox_id = PayPg_OccupancyCHDPassportExp_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassportIssue_TxtBox_id = PayPg_OccupancyCHDPassportIssue_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDBdate_TxtBox_id = PayPg_OccupancyCHDBdate_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDNationality_DropDwn_id = PayPg_OccupancyCHDNationality_DropDwn_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id = PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id.replace(strc, String.valueOf(c));
				}
				
			
				System.out.println("INFO -> child : "+c+1);
				//new Select(driver.findElement(By.name("childTitle_Air["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getNamePrefixTitle());
				getElement("PayPg_OccupancyCHDFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDFname_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDFname_TxtBox_id).setText(passengerNames.getChildren().get(c).getGivenName());
				getElement("PayPg_OccupancyCHDLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDLname_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDLname_TxtBox_id).setText(passengerNames.getChildren().get(c).getSurname());
				getElement("PayPg_OccupancyCHDContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDContact_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDContact_TxtBox_id).setText(passengerNames.getChildren().get(c).getPhoneNumber());
				getElement("PayPg_OccupancyCHDPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportNo_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportNo_TxtBox_id).setText(passengerNames.getChildren().get(c).getPassportNo());
				
				getElement("PayPg_OccupancyCHDPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportExp_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyCHDPassportExp_TxtBox_id+"').val('"+passengerNames.getChildren().get(c).getPassportExpDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				
				getElement("PayPg_OccupancyCHDPassportIssue_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportIssue_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyCHDPassportIssue_TxtBox_id+"').val('"+passengerNames.getChildren().get(c).getPassprtIssuDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				
				getElement("PayPg_OccupancyCHDBdate_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDBdate_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyCHDBdate_TxtBox_id+"').val('"+passengerNames.getChildren().get(c).getBirthDay().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				
				((ComboBox) getElement("PayPg_OccupancyCHDGender_DropDwn_class")).selectOptionByText(passengerNames.getChildren().get(c).getGender());
				//new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(passengerNames.getAdult().get(a).getGender());
//				((ComboBox) getElement("PayPg_OccupancyCHDNationality_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyCHDNationality_DropDwn_id)).selectOptionByText(passengerNames.getChildren().get(c).getNationality());
				//new Select(getElement("PayPg_OccupancyADTNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getNationality());
//				((ComboBox) getElement("PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id)).selectOptionByText(passengerNames.getChildren().get(c).getPassportIssuCountry());
				//new Select(getElement("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getPassportIssuCountry());
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	
		try {
			
			String PayPg_OccupancyINFFname_TxtBox_id = objectMap.get("PayPg_OccupancyINFFname_TxtBox_id").getRef();
			String PayPg_OccupancyINFLname_TxtBox_id = objectMap.get("PayPg_OccupancyINFLname_TxtBox_id").getRef();
			String PayPg_OccupancyINFBdate_TxtBox_id = objectMap.get("PayPg_OccupancyINFBdate_TxtBox_id").getRef();
			String PayPg_OccupancyINFNationality_DropDwn_id = objectMap.get("PayPg_OccupancyINFNationality_DropDwn_id").getRef();
			
			PayPg_OccupancyINFFname_TxtBox_id = PayPg_OccupancyINFFname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyINFLname_TxtBox_id = PayPg_OccupancyINFLname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyINFBdate_TxtBox_id = PayPg_OccupancyINFBdate_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyINFNationality_DropDwn_id = PayPg_OccupancyINFNationality_DropDwn_id.replace("REPLACE", "0");
			
			for(int i = 0; i<infants; i++)
			{
				if(i != 0)
				{
					String stri = String.valueOf(i);
					PayPg_OccupancyINFFname_TxtBox_id = PayPg_OccupancyINFFname_TxtBox_id.replace(stri, String.valueOf(i));
					PayPg_OccupancyINFLname_TxtBox_id = PayPg_OccupancyINFLname_TxtBox_id.replace(stri, String.valueOf(i));
					PayPg_OccupancyINFBdate_TxtBox_id = PayPg_OccupancyINFBdate_TxtBox_id.replace(stri, String.valueOf(i));
					PayPg_OccupancyINFNationality_DropDwn_id = PayPg_OccupancyINFNationality_DropDwn_id.replace(stri, String.valueOf(i));
				}
				
				System.out.println("INFO -> infant : "+i+1);
				//new Select(driver.findElement(By.name("infantTitle_Air["+i+"]"))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
				getElement("PayPg_OccupancyINFFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFFname_TxtBox_id).clear();
				getElement("PayPg_OccupancyINFFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFFname_TxtBox_id).setText(passengerNames.getInfant().get(i).getGivenName());
				getElement("PayPg_OccupancyINFLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFLname_TxtBox_id).clear();
				getElement("PayPg_OccupancyINFLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFLname_TxtBox_id).setText(passengerNames.getInfant().get(i).getSurname());
				
				getElement("PayPg_OccupancyINFBdate_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFBdate_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyINFBdate_TxtBox_id+"').val('"+passengerNames.getInfant().get(i).getBirthDay().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				
				((ComboBox) getElement("PayPg_OccupancyINFGender_DropDwn_class")).selectOptionByText(passengerNames.getInfant().get(i).getGender());
				//new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(passengerNames.getAdult().get(a).getGender());
				((ComboBox) getElement("PayPg_OccupancyINFNationality_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyINFNationality_DropDwn_id)).selectOptionByText(passengerNames.getInfant().get(i).getNationality());

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
	}
	
public void setDMCPassengerDetails(ReservationPassengerInfo passengerNames ){
		
		int adults		= passengerNames.getAdult().size();
		int children	= passengerNames.getChildren().size();
		int infants		= passengerNames.getInfant().size();
		
		try {
			
			String PayPg_OccupancyADTFname_TxtBox_id = objectMap.get("PayPg_OccupancyADTFname_TxtBox_id").getRef();
			String PayPg_OccupancyADTLname_TxtBox_id = objectMap.get("PayPg_OccupancyADTLname_TxtBox_id").getRef();
			String PayPg_OccupancyADTContact_TxtBox_id = objectMap.get("PayPg_OccupancyADTContact_TxtBox_id").getRef();
			String PayPg_OccupancyADTPassportNo_TxtBox_id = objectMap.get("PayPg_OccupancyADTPassportNo_TxtBox_id").getRef();
			String PayPg_OccupancyADTPassportExp_TxtBox_id = objectMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").getRef();
			String PayPg_OccupancyADTPassportIssue_TxtBox_id = objectMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").getRef();
			String PayPg_OccupancyADTBdate_TxtBox_id = objectMap.get("PayPg_OccupancyADTBdate_TxtBox_id").getRef();
			String PayPg_OccupancyADTNationality_DropDwn_id = objectMap.get("PayPg_OccupancyADTNationality_DropDwn_id").getRef();
			String PayPg_OccupancyADTPassIssuCounrty_DropDwn_id = objectMap.get("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").getRef();
			
			PayPg_OccupancyADTFname_TxtBox_id = PayPg_OccupancyADTFname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTLname_TxtBox_id = PayPg_OccupancyADTLname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTContact_TxtBox_id = PayPg_OccupancyADTContact_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassportNo_TxtBox_id = PayPg_OccupancyADTPassportNo_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassportExp_TxtBox_id = PayPg_OccupancyADTPassportExp_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassportIssue_TxtBox_id = PayPg_OccupancyADTPassportIssue_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTBdate_TxtBox_id = PayPg_OccupancyADTBdate_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyADTNationality_DropDwn_id = PayPg_OccupancyADTNationality_DropDwn_id.replace("REPLACE", "0");
			PayPg_OccupancyADTPassIssuCounrty_DropDwn_id = PayPg_OccupancyADTPassIssuCounrty_DropDwn_id.replace("REPLACE", "0");
			
			for(int a = 0; a<adults; a++)
			{
				if(a != 0)
				{
					String stra = String.valueOf(a-1);
					PayPg_OccupancyADTFname_TxtBox_id = PayPg_OccupancyADTFname_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTLname_TxtBox_id = PayPg_OccupancyADTLname_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTContact_TxtBox_id = PayPg_OccupancyADTContact_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassportNo_TxtBox_id = PayPg_OccupancyADTPassportNo_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassportExp_TxtBox_id = PayPg_OccupancyADTPassportExp_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassportIssue_TxtBox_id = PayPg_OccupancyADTPassportIssue_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTBdate_TxtBox_id = PayPg_OccupancyADTBdate_TxtBox_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTNationality_DropDwn_id = PayPg_OccupancyADTNationality_DropDwn_id.replace(stra, String.valueOf(a));
					PayPg_OccupancyADTPassIssuCounrty_DropDwn_id = PayPg_OccupancyADTPassIssuCounrty_DropDwn_id.replace(stra, String.valueOf(a));
				}
				
				System.out.println("INFO -> adult : "+a+1);
				//new Select(getElement("PayPg_ProceedToOccupancy_Btn_xpath").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
				//System.out.println(propertyMap.get("PayPg_OccupancyADTFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)));
				//((ComboBox)getElement("combobox_ChildrenAge_DropDwn_id").changeRefAndGetElement(value)).selectOptionByText(searchObject.getChildrenAge().get((i - 1)).trim());
				
				getElement("PayPg_OccupancyADTFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTFname_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTFname_TxtBox_id).setText(passengerNames.getAdult().get(a).getGivenName());
				getElement("PayPg_OccupancyADTLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTLname_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTLname_TxtBox_id).setText(passengerNames.getAdult().get(a).getSurname());
//				getElement("PayPg_OccupancyADTContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTContact_TxtBox_id).clear();
//				getElement("PayPg_OccupancyADTContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTContact_TxtBox_id).setText(passengerNames.getAdult().get(a).getPhoneNumber());
				getElement("PayPg_OccupancyADTPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportNo_TxtBox_id).clear();
				getElement("PayPg_OccupancyADTPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportNo_TxtBox_id).setText(passengerNames.getAdult().get(a).getPassportNo());

				//executejavascript(script);
				//getElement("PayPg_OccupancyADTPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportExp_TxtBox_id).setJavaScript("$('#"+getElement("PayPg_OccupancyADTPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportExp_TxtBox_id)+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				getElement("PayPg_OccupancyADTPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportExp_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyADTPassportExp_TxtBox_id+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				
				getElement("PayPg_OccupancyADTPassportIssue_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTPassportIssue_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyADTPassportIssue_TxtBox_id+"').val('"+passengerNames.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				
				getElement("PayPg_OccupancyADTBdate_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyADTBdate_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyADTBdate_TxtBox_id+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				
	//			((ComboBox) getElement("PayPg_OccupancyADTGender_DropDwn_class")).selectOptionByText(passengerNames.getAdult().get(a).getGender());
				//new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(passengerNames.getAdult().get(a).getGender());
	//			((ComboBox) getElement("PayPg_OccupancyADTNationality_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyADTNationality_DropDwn_id)).selectOptionByText(passengerNames.getAdult().get(a).getNationality());
				//new Select(getElement("PayPg_OccupancyADTNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getNationality());
	//			((ComboBox) getElement("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyADTPassIssuCounrty_DropDwn_id)).selectOptionByText(passengerNames.getAdult().get(a).getPassportIssuCountry());
				//new Select(getElement("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getPassportIssuCountry());
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	
	
		try {
			
			String PayPg_OccupancyCHDFname_TxtBox_id = objectMap.get("PayPg_OccupancyCHDFname_TxtBox_id").getRef();
			String PayPg_OccupancyCHDLname_TxtBox_id = objectMap.get("PayPg_OccupancyCHDLname_TxtBox_id").getRef();
	//		String PayPg_OccupancyCHDContact_TxtBox_id = objectMap.get("PayPg_OccupancyCHDContact_TxtBox_id").getRef();
			String PayPg_OccupancyCHDPassportNo_TxtBox_id = objectMap.get("PayPg_OccupancyCHDPassportNo_TxtBox_id").getRef();
			String PayPg_OccupancyCHDPassportExp_TxtBox_id = objectMap.get("PayPg_OccupancyCHDPassportExp_TxtBox_id").getRef();
			String PayPg_OccupancyCHDPassportIssue_TxtBox_id = objectMap.get("PayPg_OccupancyCHDPassportIssue_TxtBox_id").getRef();
			String PayPg_OccupancyCHDBdate_TxtBox_id = objectMap.get("PayPg_OccupancyCHDBdate_TxtBox_id").getRef();
			String PayPg_OccupancyCHDNationality_DropDwn_id = objectMap.get("PayPg_OccupancyCHDNationality_DropDwn_id").getRef();
			String PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id = objectMap.get("PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id").getRef();
			
			PayPg_OccupancyCHDFname_TxtBox_id = PayPg_OccupancyCHDFname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDLname_TxtBox_id = PayPg_OccupancyCHDLname_TxtBox_id.replace("REPLACE", "0");
//			PayPg_OccupancyCHDContact_TxtBox_id = PayPg_OccupancyCHDContact_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassportNo_TxtBox_id = PayPg_OccupancyCHDPassportNo_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassportExp_TxtBox_id = PayPg_OccupancyCHDPassportExp_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassportIssue_TxtBox_id = PayPg_OccupancyCHDPassportIssue_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDBdate_TxtBox_id = PayPg_OccupancyCHDBdate_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDNationality_DropDwn_id = PayPg_OccupancyCHDNationality_DropDwn_id.replace("REPLACE", "0");
			PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id = PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id.replace("REPLACE", "0");
			
			for(int c=0; c<children; c++)
			{
				
				if(c != 0)
				{
					String strc = String.valueOf(c-1);
					PayPg_OccupancyCHDFname_TxtBox_id = PayPg_OccupancyCHDFname_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDLname_TxtBox_id = PayPg_OccupancyCHDLname_TxtBox_id.replace(strc, String.valueOf(c));
//					PayPg_OccupancyCHDContact_TxtBox_id = PayPg_OccupancyCHDContact_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassportNo_TxtBox_id = PayPg_OccupancyCHDPassportNo_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassportExp_TxtBox_id = PayPg_OccupancyCHDPassportExp_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassportIssue_TxtBox_id = PayPg_OccupancyCHDPassportIssue_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDBdate_TxtBox_id = PayPg_OccupancyCHDBdate_TxtBox_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDNationality_DropDwn_id = PayPg_OccupancyCHDNationality_DropDwn_id.replace(strc, String.valueOf(c));
					PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id = PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id.replace(strc, String.valueOf(c));
				}
				
			
				System.out.println("INFO -> child : "+c+1);
				//new Select(driver.findElement(By.name("childTitle_Air["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getNamePrefixTitle());
				getElement("PayPg_OccupancyCHDFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDFname_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDFname_TxtBox_id).setText(passengerNames.getChildren().get(c).getGivenName());
				getElement("PayPg_OccupancyCHDLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDLname_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDLname_TxtBox_id).setText(passengerNames.getChildren().get(c).getSurname());
		//		getElement("PayPg_OccupancyCHDContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDContact_TxtBox_id).clear();
		//		getElement("PayPg_OccupancyCHDContact_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDContact_TxtBox_id).setText(passengerNames.getChildren().get(c).getPhoneNumber());
				getElement("PayPg_OccupancyCHDPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportNo_TxtBox_id).clear();
				getElement("PayPg_OccupancyCHDPassportNo_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportNo_TxtBox_id).setText(passengerNames.getChildren().get(c).getPassportNo());
				
				getElement("PayPg_OccupancyCHDPassportExp_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportExp_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyCHDPassportExp_TxtBox_id+"').val('"+passengerNames.getChildren().get(c).getPassportExpDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassportExpDate().trim()+"');");
				
				getElement("PayPg_OccupancyCHDPassportIssue_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDPassportIssue_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyCHDPassportIssue_TxtBox_id+"').val('"+passengerNames.getChildren().get(c).getPassprtIssuDate().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				
				getElement("PayPg_OccupancyCHDBdate_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyCHDBdate_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyCHDBdate_TxtBox_id+"').val('"+passengerNames.getChildren().get(c).getBirthDay().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				
//				((ComboBox) getElement("PayPg_OccupancyCHDGender_DropDwn_class")).selectOptionByText(passengerNames.getChildren().get(c).getGender());
				//new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(passengerNames.getAdult().get(a).getGender());
//				((ComboBox) getElement("PayPg_OccupancyCHDNationality_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyCHDNationality_DropDwn_id)).selectOptionByText(passengerNames.getChildren().get(c).getNationality());
				//new Select(getElement("PayPg_OccupancyADTNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getNationality());
//				((ComboBox) getElement("PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id)).selectOptionByText(passengerNames.getChildren().get(c).getPassportIssuCountry());
				//new Select(getElement("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(passengerNames.getAdult().get(a).getPassportIssuCountry());
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	
		try {
			
			String PayPg_OccupancyINFFname_TxtBox_id = objectMap.get("PayPg_OccupancyINFFname_TxtBox_id").getRef();
			String PayPg_OccupancyINFLname_TxtBox_id = objectMap.get("PayPg_OccupancyINFLname_TxtBox_id").getRef();
			String PayPg_OccupancyINFBdate_TxtBox_id = objectMap.get("PayPg_OccupancyINFBdate_TxtBox_id").getRef();
			String PayPg_OccupancyINFNationality_DropDwn_id = objectMap.get("PayPg_OccupancyINFNationality_DropDwn_id").getRef();
			
			PayPg_OccupancyINFFname_TxtBox_id = PayPg_OccupancyINFFname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyINFLname_TxtBox_id = PayPg_OccupancyINFLname_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyINFBdate_TxtBox_id = PayPg_OccupancyINFBdate_TxtBox_id.replace("REPLACE", "0");
			PayPg_OccupancyINFNationality_DropDwn_id = PayPg_OccupancyINFNationality_DropDwn_id.replace("REPLACE", "0");
			
			for(int i = 0; i<infants; i++)
			{
				if(i != 0)
				{
					String stri = String.valueOf(i);
					PayPg_OccupancyINFFname_TxtBox_id = PayPg_OccupancyINFFname_TxtBox_id.replace(stri, String.valueOf(i));
					PayPg_OccupancyINFLname_TxtBox_id = PayPg_OccupancyINFLname_TxtBox_id.replace(stri, String.valueOf(i));
					PayPg_OccupancyINFBdate_TxtBox_id = PayPg_OccupancyINFBdate_TxtBox_id.replace(stri, String.valueOf(i));
					PayPg_OccupancyINFNationality_DropDwn_id = PayPg_OccupancyINFNationality_DropDwn_id.replace(stri, String.valueOf(i));
				}
				
				System.out.println("INFO -> infant : "+i+1);
				//new Select(driver.findElement(By.name("infantTitle_Air["+i+"]"))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
				getElement("PayPg_OccupancyINFFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFFname_TxtBox_id).clear();
				getElement("PayPg_OccupancyINFFname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFFname_TxtBox_id).setText(passengerNames.getInfant().get(i).getGivenName());
				getElement("PayPg_OccupancyINFLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFLname_TxtBox_id).clear();
				getElement("PayPg_OccupancyINFLname_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFLname_TxtBox_id).setText(passengerNames.getInfant().get(i).getSurname());
				
				getElement("PayPg_OccupancyINFBdate_TxtBox_id").changeRefAndGetElement(PayPg_OccupancyINFBdate_TxtBox_id).setJavaScript("$('#"+PayPg_OccupancyINFBdate_TxtBox_id+"').val('"+passengerNames.getInfant().get(i).getBirthDay().trim()+"');");
				//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+passengerNames.getAdult().get(a).getBirthDay().trim()+"');");
				
				((ComboBox) getElement("PayPg_OccupancyINFGender_DropDwn_class")).selectOptionByText(passengerNames.getInfant().get(i).getGender());
				//new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(passengerNames.getAdult().get(a).getGender());
				((ComboBox) getElement("PayPg_OccupancyINFNationality_DropDwn_id").changeRefAndGetElement(PayPg_OccupancyINFNationality_DropDwn_id)).selectOptionByText(passengerNames.getInfant().get(i).getNationality());

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
	}
	
	
	public void proceedToBillingInfo()
	{
		try {
			getElement("button_PayPg_ProceedToBillingInfo_class").click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	
}


