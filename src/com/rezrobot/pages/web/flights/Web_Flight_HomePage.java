package com.rezrobot.pages.web.flights;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.flight_circuitry.SearchObject;
import com.rezrobot.pojo.CheckBox;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Image;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.pojo.Link;
import com.rezrobot.pojo.RadioButton;
import com.rezrobot.pojo.Button;

/**
 * @author Dulan
 *
 */

public class Web_Flight_HomePage extends PageObject {


	public void selectFlightBEC()
	{
		try {
			switchToFrame("frame_homepage_id");
			((Button)getElement("button_flight-icon_id")).click();
			switchToDefaultFrame();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isPageAvailable() {
		return super.isPageAvailable("frame_homepage_id");
	}

	public boolean doSearch() {
		try {
			
			
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean getLogoAvailability() throws Exception {
		try {
			return ((Image) getElement("img_company_logo")).isImageLoaded();
		} catch (Exception e) {
			return false;
		}
	}

	public String getLogoPath() throws Exception {
		return ((Image) getElement("img_company_logo")).getImagePath();
	}

	public boolean getCustomerLoginLinkAvailability() throws Exception {
		return getElementVisibility("link_customer_login");
	}

	public String getCustomerLoginLinkText() throws Exception {
		return ((Link) getElement("link_customer_login")).getText();
	}

	public String getCustomerLoginLinkPath() throws Exception {
		return ((Link) getElement("link_customer_login")).getUrl();
	}

	public boolean getBECAvailability() {
		try {
			switchToFrame("frame_homepage_id");
			return getElementVisibility("inputfield_FromTxtBox_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}

	public ArrayList<String> getBecProducts() {
		ArrayList<String> AvailableProducts = new ArrayList<String>();

		if (getElementVisibility("button_MenuFlight_id"))
			AvailableProducts.add("Flight");
		if (getElementVisibility(""))
			AvailableProducts.add("Hotel");
		if (getElementVisibility(""))
			AvailableProducts.add("Fixed Package");
		if (getElementVisibility(""))
			AvailableProducts.add("Holidays");
		if (getElementVisibility(""))
			AvailableProducts.add("Activity");
		if (getElementVisibility(""))
			AvailableProducts.add("Car");
		if (getElementVisibility(""))
			AvailableProducts.add("Transfer");

		return AvailableProducts;

	}

	public String getDefaultSelectedBEC() throws Exception {

		ArrayList<String> AvailableProducts = getBecProducts();

		if (AvailableProducts.contains("Flight")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Flight";
		else if (AvailableProducts.contains("Hotel")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Hotel";
		else if (AvailableProducts.contains("Activity")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Activity";
		else if (AvailableProducts.contains("Holidays")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Holidays";
		else if (AvailableProducts.contains("Transfer")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Transfer";
		else if (AvailableProducts.contains("Car")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Car";
		else if (AvailableProducts.contains("Fixed Package")
				|| getElement("").getAttribute("class").contains(
						"ui-state-active"))
			return "Fixed Package";
		else
			return "None";
	}

	public boolean isFlightBECLoaded() {
		try {
			return getElementVisibility("inputfield_To_TxtBox_id");
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
	}
	
	public String monthDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_month"))
				.getDefaultSelectedOption();
	}

	public String dayDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_day")).getDefaultSelectedOption();
	}

	public String yearDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_year")).getDefaultSelectedOption();
	}

	public String distanceDefaultValue() throws Exception {
		return ((ComboBox) getElement("combo_distance"))
				.getDefaultSelectedOption();
	}

	public ArrayList<String> genderAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_gender")).getAvailableOptions();
	}

	public ArrayList<String> monthAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_month")).getAvailableOptions();
	}

	public ArrayList<String> dayAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_day")).getAvailableOptions();
	}

	public ArrayList<String> yearAvailableOptions() throws Exception {
		return ((ComboBox) getElement("combo_year")).getAvailableOptions();
	}

	public HashMap<String, String> getPlaceHolders() {

		HashMap<String, String> placeholders = new HashMap<>();
		
	    try {
	    	placeholders.put("Country of residance", ((ComboBox)getElement("combobox_ResidenceCountry_id")).getDefaultSelectedOption().trim());
	    	placeholders.put("ToCityInput", getPlaceHolder("inputfield_To_TxtBox_id"));
            
		} catch (Exception e) {
			
		}

		return placeholders;
	}

	public HashMap<String, Boolean> getAvailabilityOfInputFields() {
		
		System.out.println("Started --> getAvailabilityOfInputFields()");
		
		try {
			switchToFrame("frame_homepage_id");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		HashMap<String, Boolean> fields = new HashMap<>();
		try {
			fields.put("cor", getElementVisibility("combobox_ResidenceCountry_id"));System.out.println("-> cor done :"+getElementVisibility("combobox_ResidenceCountry_id"));
			fields.put("roundtrip", getElementVisibility("radiobutton_RoundTrip_id"));System.out.println("-> roundtrip done :"+getElementVisibility("radiobutton_RoundTrip_id"));
			fields.put("onewaytrip", getElementVisibility("radiobutton_OnewayTrip_RadioBtn_id"));System.out.println("-> onewaytrip done :"+getElementVisibility("radiobutton_OnewayTrip_RadioBtn_id"));
			fields.put("multidestination",getElementVisibility("radiobutton_MultiDestination_RadioBtn_id"));System.out.println("-> multidestination done :"+getElementVisibility("radiobutton_MultiDestination_RadioBtn_id"));
			fields.put("from", getElementVisibility("inputfield_FromTxtBox_id"));System.out.println("-> from done :"+getElementVisibility("inputfield_FromTxtBox_id"));
			fields.put("to", getElementVisibility("inputfield_To_TxtBox_id"));System.out.println("-> to done :"+getElementVisibility("inputfield_To_TxtBox_id"));
			
			fields.put("departuredate",getElementVisibility("inputfield_DepartureDateTemp_TxtBox_id"));System.out.println("-> departuredate done :"+getElementVisibility("inputfield_DepartureDateTemp_TxtBox_id"));
			try {
				objectMap.get("inputfield_DepartureDateTemp_TxtBox_id").click();
				fields.put("departuredateselector",getElementVisibility("calendar_DepartureDate_id"));System.out.println("-> departuredateselector done :"+getElementVisibility("calendar_DepartureDate_id"));
				Thread.sleep(2000);
			} catch (Exception e) {
				fields.put("departuredateselector",false);System.out.println("-> departuredateselector done :"+false);
			}
			fields.put("departuretime",getElementVisibility("combobox_DepartureTime_DropDwn_id"));System.out.println("-> departuretime done :"+getElementVisibility("combobox_DepartureTime_DropDwn_id"));
			
			fields.put("returndate", getElementVisibility("inputfield_ReturnDateTemp_TxtBox_id"));System.out.println("-> returndate done :"+getElementVisibility("inputfield_ReturnDateTemp_TxtBox_id"));
			try {
				objectMap.get("inputfield_ReturnDateTemp_TxtBox_id").click();
				fields.put("returndateselector",getElementVisibility("calendar_DepartureDate_id"));System.out.println("-> returndateselector done :"+getElementVisibility("calendar_DepartureDate_id"));
				Thread.sleep(2000);
			} catch (Exception e) {
				fields.put("returndateselector",false);System.out.println("-> returndateselector done :"+false);
			}
			fields.put("returntime", getElementVisibility("combobox_ReturnTime_DropDwn_id"));System.out.println("-> returntime done :"+getElementVisibility("combobox_ReturnTime_DropDwn_id"));
			
			fields.put("imflex", getElementVisibility("checkbox_Flexible_id"));System.out.println("-> imflex done :"+getElementVisibility("checkbox_Flexible_id"));
			fields.put("adults", getElementVisibility("combobox_AdultCount_id"));System.out.println("-> adults done :"+getElementVisibility("combobox_AdultCount_id"));
			fields.put("children", getElementVisibility("combobox_ChildrenCount_DropDwn_id"));System.out.println("-> children done :"+getElementVisibility("combobox_ChildrenCount_DropDwn_id"));
			fields.put("infant", getElementVisibility("combobox_InfantCount_DropDwn_id"));System.out.println("-> infant done :"+getElementVisibility("combobox_InfantCount_DropDwn_id"));
			
			try {
				fields.put("showadditional", getElementVisibility("label_AdditionalSearchOption_class"));System.out.println("-> showadditional done :"+getElementVisibility("label_AdditionalSearchOption_class"));
				objectMap.get("label_AdditionalSearchOption_class").click();
				Thread.sleep(2000);
				fields.put("hideshowadditional", getElementVisibility("label_AdditionalSearchOption_class"));
			} catch (Exception e) {
				fields.put("showadditional", false);System.out.println("-> showadditional done :"+false);
			}
			
			try {
				fields.put("prefair", getElementVisibility("inputfield_PreferredAirline_id"));System.out.println("-> prefair done :"+getElementVisibility("inputfield_PreferredAirline_id"));
			} catch (Exception e) {
				
			}
			fields.put("cabinclass", getElementVisibility("combobox_CabinClass_id"));System.out.println("-> cabinclass done :"+getElementVisibility("combobox_CabinClass_id"));
			fields.put("prefcurrency", getElementVisibility("combobox_PreferredCurrency_id"));System.out.println("-> prefcurrency done :"+getElementVisibility("combobox_PreferredCurrency_id"));
			fields.put("promotioncode", getElementVisibility("inputfield_PromotionCode_TxtBox_id"));System.out.println("-> promotioncode done :"+getElementVisibility("inputfield_PromotionCode_TxtBox_id"));
			fields.put("prefnonstop", getElementVisibility("checkbox_PreferNonStop_name"));System.out.println("-> prefnonstop done :"+getElementVisibility("checkbox_PreferNonStop_name"));
			fields.put("searchbutton", getElementVisibility("button_SearchFlights_Btn_id"));System.out.println("-> searchbutton done :"+getElementVisibility("button_SearchFlights_Btn_id"));
		} catch (Exception e) {
			
		}
		switchToDefaultFrame();
		return fields;
	}

	public HashMap<String, Boolean> getMandatoryList() {

		HashMap<String, Boolean> fileds = new HashMap<>();

		try {
			switchToFrame("frame_homepage_id");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		fileds.put("cor", getComboBoxMandatoryStatus("combobox_ResidenceCountry_id"));
		fileds.put("roundtrip", getRadioButtonMandatoryStatus("radiobutton_RoundTrip_id"));
		fileds.put("onewaytrip", getRadioButtonMandatoryStatus("radiobutton_OnewayTrip_RadioBtn_id"));
		fileds.put("multidestination",getRadioButtonMandatoryStatus("radiobutton_MultiDestination_RadioBtn_id"));
		switchToDefaultFrame();
		return fileds;
	}

	public HashMap<String, String> getFlightBECLabels() {

		/*try {
			switchToFrame("frame_homepage_id");
		} catch (Exception e1) {
			e1.printStackTrace();
		}*/
		HashMap<String, String> actualLabels = new HashMap<>();
		try {
			actualLabels.put("Flightslabel", getLabelName("label_flight-icon_id"));
			actualLabels.put("Hotelslabel", getLabelName("label_hotel-icon_id"));
			actualLabels.put("Carslabel", getLabelName("label_car-icon_id"));
			actualLabels.put("Activitieslabel",getLabelName("label_activity-icon_id"));
			actualLabels.put("Trasferslabel",getLabelName("label_transfer-icon_id"));
			actualLabels.put("Holidayslabel", getLabelName("label_fh-icon_id"));
			actualLabels.put("Packageslabel", getLabelName("label_fp-icon_id"));
			actualLabels.put("CORlabel",getLabelName("label_air_country_lbl_div_id"));
			actualLabels.put("TripTypelabel",getLabelName("label_triptype_xpath"));
			actualLabels.put("RoundTriplabel", getLabelName("label_roundtrip_xpath"));
			actualLabels.put("OneWayTriplabel", getLabelName("label_onewaytrip_xpath"));
			actualLabels.put("MultiDestinationlabel", getLabelName("label_multidestination_xpath"));
			actualLabels.put("WhereYouGoinglabel", getLabelName("label_whereyougoing_xpath"));
			actualLabels.put("Fromlabel", getLabelName("label_from_xpath"));
			actualLabels.put("Tolabel", getLabelName("label_to_xpath"));
			actualLabels.put("DepartureDatelabel", getLabelName("label_departuredate_xpath"));
			actualLabels.put("DepartureTimelabel", getLabelName("label_departuretime_xpath"));
			actualLabels.put("ReturnDatelabel", getLabelName("label_returndate_xpath"));
			actualLabels.put("ReturnTimelabel", getLabelName("label_returntime_xpath"));
			actualLabels.put("ImFlexiblelabel", getLabelName("label_imflexible_xpath"));
			actualLabels.put("HowManyPeoplelabel", getLabelName("label_howmanypeople_xpath"));
			actualLabels.put("Adultslabel", getLabelName("label_adults_xpath"));
			actualLabels.put("Childrenlabel", getLabelName("label_children_xpath"));
			actualLabels.put("Infantlabel", getLabelName("label_infant_xpath"));
			try {
				actualLabels.put("AdditionalSearchlabel", getLabelName("label_AdditionalSearchOption_class"));
				getElement("label_AdditionalSearchOption_class").click();
				//objectMap.get("label_AdditionalSearchOption_class").click();
				Thread.sleep(2000);
				actualLabels.put("HideAdditionalSearchlabel", getLabelName("label_AdditionalSearchOption_class"));
			} catch (Exception e) {
				
			}
			actualLabels.put("PrefFlightlabel", getLabelName("label_prefflight_xpath"));
			actualLabels.put("Cabinlabel", getLabelName("label_cabin_xpath"));
			actualLabels.put("PreferredCurrlabel", getLabelName("label_preferredcurrency_xpath"));
			actualLabels.put("PromoCodelabel", getLabelName("label_promotioncode_xpath"));
			actualLabels.put("PrefNonStoplabel", getLabelName("label_prefernonstop_xpath"));
			((ComboBox)getElement("combobox_ChildrenCount_DropDwn_id")).selectOptionByText("1");
			actualLabels.put("AgeOfChildrenlabel", getLabelName("label_ageofchildren_xpath"));
			
			
		} catch (Exception e) {
			
		}

		switchToDefaultFrame();
		return actualLabels;
	}

	public HashMap<String, String> getFlightComboBoxDataList() throws Exception {

		HashMap<String, String> fields = new HashMap<>();

		switchToFrame("frame_homepage_id");
		fields.put("countryOfResidence", getComboBoxDataList("combobox_ResidenceCountry_id"));		
		fields.put("departureTime", getComboBoxDataList("combobox_DepartureTime_DropDwn_id"));
		fields.put("returnTime", getComboBoxDataList("combobox_ReturnTime_DropDwn_id"));
		fields.put("adults", getComboBoxDataList("combobox_AdultCount_id"));
		fields.put("children",getComboBoxDataList("combobox_ChildrenCount_DropDwn_id"));
		((ComboBox)getElement("combobox_ChildrenCount_DropDwn_id")).selectOptionByText("1");
		//String combobox_ChildrenAge_DropDwn_id_REF = ((ComboBox)getElement("combobox_ChildrenAge_DropDwn_id")).getRef().replace("REPLACE", "1");
		fields.put("childrenAge",getComboBoxDataList("combobox_ChildrenAge_DropDwn1_id"));
		fields.put("cabins",getComboBoxDataList("combobox_CabinClass_id"));
		fields.put("prefCurrency",getComboBoxDataList("combobox_PreferredCurrency_id"));
		switchToDefaultFrame();
		return fields;
	}
	
	public Web_Flight_ResultsPage doFlightSearch(SearchObject searchObject) throws Exception {
		
		try {
		//	switchToFrame("frame_homepage_id");
			System.out.println("INFO -> ENTER SEARCH DETAILS TO BEC");
			
			try {
				Thread.sleep(10000);
				getElementVisibility("combobox_ResidenceCountry_id", 10000);
				((ComboBox)getElement("combobox_ResidenceCountry_id")).selectOptionByText(searchObject.getCountry());
			} catch (Exception e) {
				
			}

			try {

				((InputFiled)getElement("inputfield_FromTxtBox_id")).setText(searchObject.getFrom().trim().split("\\|")[0]);
				getElement("label_From_Hidden_id").setJavaScript("$('#"+getElement("label_From_Hidden_id").getRef().trim()+"').val('"+searchObject.getFrom()+"');");
				((InputFiled)getElement("inputfield_To_TxtBox_id")).setText(searchObject.getTo().trim().split("\\|")[0]);
				getElement("label_To_Hidden_id").setJavaScript("$('#"+getElement("label_To_Hidden_id").getRef().trim()+"').val('"+searchObject.getTo()+"');");

			} catch (Exception e) {

			}
			
			if (searchObject.getTriptype().equals("Round Trip")) {//RoundTrip
				
				((RadioButton)getElement("radiobutton_RoundTrip_id")).click();
				getElement("inputfield_DepartureDate_TxtBox_id").setJavaScript("$('#"+getElement("inputfield_DepartureDate_TxtBox_id").getRef().trim()+"').val('"+searchObject.getDepartureDate()+"');");
				getElement("inputfield_RetrunDate_TxtBox_id").setJavaScript("$('#"+getElement("inputfield_RetrunDate_TxtBox_id").getRef().trim()+"').val('"+searchObject.getReturnDate()+"');");
				((ComboBox)getElement("combobox_DepartureTime_DropDwn_id")).selectOptionByText(searchObject.getDepartureTime());
				
			} else if (searchObject.getTriptype().equals("One Way Trip")) {//One Way

				((RadioButton)getElement("radiobutton_OnewayTrip_RadioBtn_id")).click();
				getElement("inputfield_DepartureDate_TxtBox_id").setJavaScript("$('#"+getElement("inputfield_DepartureDate_TxtBox_id").getRef().trim()+"').val('"+searchObject.getDepartureDate()+"');");
			}

			try {
				//DEPARTURE RETURN TIME 
				((ComboBox)getElement("combobox_DepartureTime_DropDwn_id")).selectOptionByText(searchObject.getDepartureTime());
				((ComboBox)getElement("combobox_ReturnTime_DropDwn_id")).selectOptionByText(searchObject.getDepartureTime());
			} catch (Exception e9) {
				e9.printStackTrace();
			}
			
			
			try {
				// FLEXIBLE FLIGHTS
				if (Boolean.valueOf(searchObject.isFlexible())) {
					
					((CheckBox) getElement("checkbox_Flexible_id")).click();
				}
			} catch (Exception e8) {
				e8.printStackTrace();
			}
			
			try {
				// ADULTS
				try {
					((ComboBox)getElement("combobox_AdultCount_id")).selectOptionByText(searchObject.getAdult());
				} catch (Exception e) {

				}
			} catch (Exception e7) {
				e7.printStackTrace();
			}

			try {
				// CHILDREN
				if (!searchObject.getChildren().trim().equals("0")) {

					((ComboBox)getElement("combobox_ChildrenCount_DropDwn_id")).selectOptionByText(searchObject.getChildren());
					
					String value = objectMap.get("combobox_ChildrenAge_DropDwn_id").getRef();
					try {
						for (int i = 1; i <= searchObject.getChildrenAge().size(); i++) {
							value = value.replace("REPLACE", String.valueOf(i));
							((ComboBox)getElement("combobox_ChildrenAge_DropDwn_id").changeRefAndGetElement(value)).selectOptionByText(searchObject.getChildrenAge().get((i - 1)).trim());
						}
					} catch (Exception e) {

					}
				}
			} catch (Exception e6) {
				e6.printStackTrace();
			}

			try {
				// INFANT
				if (!searchObject.getInfant().trim().equals("0")) {

					((ComboBox)getElement("combobox_InfantCount_DropDwn_id")).selectOptionByText(searchObject.getInfant());
				}
			} catch (Exception e5) {
				e5.printStackTrace();
			}
			
			try {
				// PREFERRED AIRLINE CODE
				if(searchObject.getPreferredAirline().trim().equals("APPLY"))
				{
					
				} 
				else if(!searchObject.getPreferredAirline().trim().equals("APPLY") && !searchObject.getPreferredAirline().trim().equals("NONE")){
				 
					 ((InputFiled)getElement("inputfield_PreferredAirline_id")).setText(searchObject.getPreferredAirline().trim());
				}
				
			} catch (Exception e4) {
				e4.printStackTrace();
			}

			try {
				// CABIN CLASS
				((ComboBox)getElement("combobox_CabinClass_id")).selectOptionByText(searchObject.getCabinClass());
			} catch (Exception e3) {
				e3.printStackTrace();
			}

			
			try {
				//PREFERRED CURRENCY
				if(!(searchObject.getPreferredCurrency().trim() == null)) {
				
					((ComboBox)getElement("combobox_PreferredCurrency_id")).selectOptionByText(searchObject.getPreferredCurrency());
					
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}

			try {
				if (searchObject.isApplyDiscount()) {
					try {
						((InputFiled)getElement("inputfield_PromotionCode_TxtBox_id")).setText(searchObject.getDiscountCouponNo());
					} catch (Exception e) {
						System.out.println("Discount No not entered");
					}

				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			try {
				// PREFER NONSTOP
				if (searchObject.isNonStop()) { 
					
					((CheckBox) getElement("checkbox_PreferNonStop_id")).click();
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
						
			
		} catch (Exception e) {
			
		}

		
		System.out.println("---> SEARCH ENDED SUCCESSFULLY");
		
		getElement("button_SearchFlights_Btn_id").click();
		Web_Flight_ResultsPage fresultspage = new Web_Flight_ResultsPage();
		switchToDefaultFrame();
		return fresultspage;
	}


}
