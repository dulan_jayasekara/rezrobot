package com.rezrobot.pages.web.flights;

import com.rezrobot.core.PageObject;
import com.rezrobot.pojo.Label;

public class Web_Flight_ConfirmationPage extends PageObject {
	
	String ReservationNo = "";
	String supplierConfirmationNo = "";

	public boolean isPageAvailable() {
	   	
    	try {
    		switchToDefaultFrame();
			return getElementVisibility("label_BookingReference_Lbl_id", 10);	
	   	    //return this.objectMap.get("label_BookingReference_Lbl_id").getWebElement().isDisplayed();
	   	} catch (Exception e) {
	   	    return false;
	   	}
	   	
    }
	
	public void setConfirmationDetails()
	{
		try {
			ReservationNo = ((Label)getElement("label_ReservationNo_Lbl_id")).getText();
			supplierConfirmationNo = ((Label)getElement("label_SupplierConfNo_Lbl_id")).getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getReservationNo() {
		return ReservationNo;
	}

	public void setReservationNo(String reservationNo) {
		ReservationNo = reservationNo;
	}

	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}

	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}
	
	
	
}
