package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;

public class CC_Com_OAH_SalesReport extends PageObject{

	public HashMap<String, String> getValues(String resNumber) throws Exception
	{
		switchToDefaultFrame();
		switchToFrame("frame_report_id");
		HashMap<String, String> details = new HashMap<>();
		String pageCountHelper = getElement("label_page_count_xpath").getText();
		String[] pageCount = pageCountHelper.split("of");
		System.out.println(pageCount[1]);
		if(getElement("label_booking_number_xpath").changeRefAndGetElement(getElement("label_booking_number_xpath").getRef().replace("replace", "1")).getText().isEmpty())
		{
			System.out.println("Profit and loss report - Record not found");
		}
		else
		{
			for(int j = 1 ; j <= Integer.parseInt(pageCount[1].trim()) ; j++)
			{
				int iterator = 9;
				executejavascript("javascript:getReportData('view','"+ j +"');");
				
				for(int i = 1 ; i <= 30 ; i++)
				{
					try {
						if(resNumber.equals(getElement("label_booking_number_xpath").changeRefAndGetElement(getElement("label_booking_number_xpath").getRef().replace("replace", String.valueOf(i))).getText().trim()))
						{
							System.out.println("Profit and loss report - Record found");
							details.put("reservationNumber", 		getElement("label_booking_number_xpath").changeRefAndGetElement(getElement("label_booking_number_xpath").getRef().replace("replace", String.valueOf(i))).getText().trim());
							details.put("bookingDate", 				getElements("label_booking_date_xpath").get(i-1).getText().trim());
							details.put("combination", 				getElement("label_combination_xpath").changeRefAndGetElement(getElement("label_combination_xpath").getRef().replace("replace", String.valueOf(i))).getText().trim());
							details.put("commissionTemplate", 		getElements("label_commission_template_xpath").get(i-1).getText().trim());
							details.put("contractType", 			getElements("label_contract_type_xpath").get(i-1).getText().trim());
							details.put("bookingType", 				getElements("label_booking_type_xpath").get(i-1).getText().trim());
							details.put("agentName", 				getElements("label_agent_name_xpath").get(i-1).getText().trim());
							details.put("bookingChannel", 			getElements("label_booking_channel_xpath").get(i-1).getText().trim());
							details.put("posLocation", 				getElements("label_pos_location_xpath").get(i-1).getText().trim());
							details.put("state", 					getElements("label_state_xpath").get(i-1).getText().trim());
							details.put("guestCount", 				getElements("label_guest_count_xpath").get(i-1).getText().trim());
							details.put("checkin/checkout", 		getElement("label_checkin_checkout_xpath").changeRefAndGetElement(getElement("label_checkin_checkout_xpath").getRef().replace("replace", String.valueOf(i))).getText().replace("\n", " ").trim());
							details.put("numberOfNights", 			getElements("label_number_of_nights_xpath").get(i-1).getText().trim());
							details.put("baseCurrency", 			getElements("label_base_currency_xpath").get(i-1).getText().trim());
							details.put("grossBookingValue", 		getElements("label_gross_booking_value_xpath").get(i-1).getText().replace(",", "").trim());	
							details.put("portalCommission", 		getElement("label_portal_commission_xpath").changeRefAndGetElement(getElement("label_portal_commission_xpath").getRef().replace("replace", String.valueOf(iterator))).getText().trim());
							details.put("agentCommission", 			getElements("label_agent_commission_xpath").get(i-1).getText().trim());
							details.put("netBookingValue", 			getElement("label_net_booking_value_xpath").changeRefAndGetElement(getElement("label_net_booking_value_xpath").getRef().replace("replace", String.valueOf(i))).getText().replace(",", "").trim());
							details.put("costOfSales", 				getElements("label_cost_of_sales_xpath").get(i-1).getText().replace(",", "").trim());
							details.put("profit", 					getElements("label_profit_xpath").get(i-1).getText().trim());
							details.put("profitPercentage", 		getElement("label_profit_percentage_xpath").changeRefAndGetElement(getElement("label_profit_percentage_xpath").getRef().replace("replace", String.valueOf(i))).getText().trim());
							details.put("invoiceTotal", 			getElements("label_invoice_total_xpath").get(i-1).getText().replace(",", "").trim());
							details.put("invoicePaid", 				getElements("label_invoice_paid_xpath").get(i-1).getText().replace(",", "").trim());
							details.put("balanceDue", 				getElement("label_balance_due_xpath").changeRefAndGetElement(getElement("label_balance_due_xpath").getRef().replace("replace", String.valueOf(i))).getText().trim());
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					iterator = iterator + 6;
				}
			}
		}
		
		return details;
	}
}
