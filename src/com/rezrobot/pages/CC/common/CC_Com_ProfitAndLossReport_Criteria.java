package com.rezrobot.pages.CC.common;



import com.rezrobot.core.PageObject;
import com.rezrobot.pojo.ComboBox;

public class CC_Com_ProfitAndLossReport_Criteria extends PageObject {

	public void enterCriteria(String date) throws Exception
	{
		switchToDefaultFrame();
		switchToFrame("frame_reportFrame_id");
		System.out.println(date);
		String[] reportDate = date.split("-");
		String day = reportDate[0];
		String month = reportDate[1];
		String year = reportDate[2];
		((ComboBox) getElement("combobox_from_day_id")).selectOptionByText(day);
		((ComboBox) getElement("combobox_from_month_id")).selectOptionByText(month);
		((ComboBox) getElement("combobox_from_year_id")).selectOptionByText(year);
		((ComboBox) getElement("combobox_from_day_id")).selectOptionByText(day);
		((ComboBox) getElement("combobox_from_month_id")).selectOptionByText(month);
		((ComboBox) getElement("combobox_from_year_id")).selectOptionByText(year);
		try {
			getElement("button_view_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
	}
}
