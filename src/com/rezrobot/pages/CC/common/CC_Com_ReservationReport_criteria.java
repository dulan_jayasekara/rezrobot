package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;

public class CC_Com_ReservationReport_criteria extends PageObject {

	public HashMap<String, Boolean> checkLabelAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> labels = new HashMap<>();
		
		try {
			labels.put("productType", 					getElementVisibility("label_product_type_id"));
			labels.put("productTypeAll", 				getElementVisibility("label_product_type_all_id"));
			labels.put("productTypeHotel", 				getElementVisibility("label_product_type_hotel_id"));
			labels.put("ProductTypeAir", 				getElementVisibility("label_product_type_air_id"));
			labels.put("productTypeCar", 				getElementVisibility("label_product_type_car_id"));
			labels.put("prodctTypeActivity", 			getElementVisibility("label_product_type_activity_id"));
			labels.put("productTypePackage", 			getElementVisibility("label_product_type_package_id"));
			labels.put("searchBy", 						getElementVisibility("label_search_by_id"));
			labels.put("searchByDate", 					getElementVisibility("label_search_by_dates_id"));
			labels.put("searchByReservationNumber", 	getElementVisibility("label_search_by_reservation_no_id"));
			labels.put("searchbyGuestName", 			getElementVisibility("label_search_by_guest_name_id"));
			labels.put("searchbyXmlPackageReference", 	getElementVisibility("label_search_by_xml_package_ref_id"));
			labels.put("dateFilter", 					getElementVisibility("label_date_filter_xpath"));
			labels.put("reportFrom", 					getElementVisibility("label_report_from_xpath"));
			labels.put("reportTo", 						getElementVisibility("label_report_to_xpath"));
			labels.put("datesBasedOn", 					getElementVisibility("label_dates_based_on_id"));
			labels.put("datesBasedonReservationDate", 	getElementVisibility("label_dates_based_on_reservation_date_id"));
			labels.put("datesBasedOnArrivalDate", 		getElementVisibility("label_dates_based_on_arrival_date_id"));
			labels.put("datesBasedOnDueDate", 			getElementVisibility("label_dates_based_on_due_date_id"));
			labels.put("paymentApproved", 				getElementVisibility("label_payment_approved_id"));
			labels.put("paymentApprovedAll", 			getElementVisibility("label_payment_approved_all_id"));
			labels.put("paymentApprovedYes", 			getElementVisibility("label_payment_approved_yes_id"));
			labels.put("paymentApprovedNo", 			getElementVisibility("label_payment_approved_no_id"));
			labels.put("invoiceIssued", 				getElementVisibility("label_invoice_issued_id"));
			labels.put("invoiceIssuedAll", 				getElementVisibility("label_invoice_issued_all_id"));
			labels.put("invoiceIssuedYes", 				getElementVisibility("label_invoice_issued_yes_id"));
			labels.put("invoiceIssuedNo", 				getElementVisibility("label_invoice_issued_no_id"));
			labels.put("voucherIssued", 				getElementVisibility("label_voucher_issued_id"));
			labels.put("voucherIssuedAll", 				getElementVisibility("label_voucher_issued_all_id"));
			labels.put("voucherIssuedYes", 				getElementVisibility("label_voucher_issued_yes_id"));
			labels.put("voucherIssuedNo", 				getElementVisibility("label_voucher_issued_no_id"));
			labels.put("lpoRequired", 					getElementVisibility("label_lpo_required_id"));
			labels.put("lpoRequiredYes", 				getElementVisibility("label_lpo_required_yes_id"));
			labels.put("lpoRequiredAll", 				getElementVisibility("label_lpo_required_all_id"));
			labels.put("lpoRequiredNo", 				getElementVisibility("label_lpo_required_no_id"));
			labels.put("bookingChannel", 				getElementVisibility("label_booking_channel_id"));
			labels.put("bookingChannelAll", 			getElementVisibility("label_booking_channel_all_id"));
			labels.put("bookingChannelCC", 				getElementVisibility("label_booking_channel_cc_id"));
			labels.put("bookingChannelWeb", 			getElementVisibility("label_booking_channel_web_id"));
			labels.put("partner", 						getElementVisibility("label_partner_id"));
			labels.put("partnerAll", 					getElementVisibility("label_partner_all_id"));
			labels.put("partnerDirectConsumer", 		getElementVisibility("label_partner_direct_consumer_id"));
			labels.put("partnerAffiliate", 				getElementVisibility("label_partner_affiliate_id"));
			labels.put("partnerB2bPartner", 			getElementVisibility("label_partner_b2bpartner_id"));
			labels.put("posLocation", 					getElementVisibility("label_pos_locaation_name_xpath"));
			labels.put("generatePDF", 					getElementVisibility("label_generate_pdf_id"));
			labels.put("generatePDFYes", 				getElementVisibility("label_generate_pdf_yes_id"));
			labels.put("generatePDFNo", 				getElementVisibility("label_generate_pdf_no_id"));
		}
		catch(Exception e)
		{
			
		}
		return labels;
	}
	
	public HashMap<String, Boolean> checkRadioButtonAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> buttons = new HashMap<>();
		try {
			buttons.put("productTypeAll", 				getElementVisibility("radiobutton_product_type_all_id"));
			buttons.put("productTypeHotel", 			getElementVisibility("radiobutton_product_type_hotel_id"));
			buttons.put("productTypeAir", 				getElementVisibility("radiobutton_product_type_air_id"));
			buttons.put("productTypeActivity", 			getElementVisibility("radiobutton_product_type_activity_id"));
			buttons.put("productTypeCar", 				getElementVisibility("radiobutton_product_type_car_id"));
			buttons.put("productTypePackage", 			getElementVisibility("radiobutton_product_type_packge_all_id"));
			buttons.put("searchByDates", 				getElementVisibility("radiobutton_search_by_dates_id"));
			buttons.put("searchByReservations", 		getElementVisibility("radiobutton_search_by_reservation_no_id"));
			buttons.put("searchByGuestName", 			getElementVisibility("radiobutton_search_by_guest_name_id"));
			buttons.put("SearchByXmlPackageRef", 		getElementVisibility("radiobutton_search_by_xml_package_ref_id"));
			buttons.put("datesBasedOnReservationDate", 	getElementVisibility("radiobutton_dates_based_on_reservation_date_id"));
			buttons.put("datesBasedOnArrivalDate", 		getElementVisibility("radiobutton_dates_based_on_arrival_date_id"));
			buttons.put("datesBasedOnDueDate", 			getElementVisibility("radiobutton_dates_based_on_due_date_id"));
			buttons.put("paymentApprovedAll", 			getElementVisibility("radiobutton_payment_approved_all_id"));
			buttons.put("paymentApprovedYes", 			getElementVisibility("radiobutton_payment_approved_yes_id"));
			buttons.put("paymentApprovedNo", 			getElementVisibility("radiobutton_payment_approved_no_id"));
			buttons.put("invoiceIssuedAll", 			getElementVisibility("radiobutton_invoice_issued_all_id"));
			buttons.put("invoiceIssuedYes", 			getElementVisibility("radiobutton_invoice_issued_yes_id"));
			buttons.put("invoiceIssuedNo", 				getElementVisibility("radiobutton_invoice_issued_no_id"));
			buttons.put("voucherIssuedAll", 			getElementVisibility("radiobutton_voucher_issued_all_id"));
			buttons.put("voucherIssuedYes", 			getElementVisibility("radiobutton_voucher_issued_yes_id"));
			buttons.put("voucherIssuedNo", 				getElementVisibility("radiobutton_voucher_issued_no_id"));
			buttons.put("lpoAll", 						getElementVisibility("radiobutton_lpo_required_all_id"));
			buttons.put("lpoYes", 						getElementVisibility("radiobutton_lpo_required_yes_id"));
			buttons.put("lpoNo", 						getElementVisibility("radiobutton_lpo_required_no_id"));
			buttons.put("bookingChannelAll", 			getElementVisibility("radiobutton_booking_channel_all_id"));
			buttons.put("bookingChannelWeb", 			getElementVisibility("radiobutton_booking_channel_web_id"));
			buttons.put("bookingChannelCC", 			getElementVisibility("radiobutton_booking_channel_cc_id"));
			buttons.put("partnerAll", 					getElementVisibility("radiobutton_partner_all_id"));
			buttons.put("partnerDirectConsumer", 		getElementVisibility("radiobutton_partner_direct_consumer_id"));
			buttons.put("partnerAffiliate", 			getElementVisibility("radiobutton_partner_affiliate_id"));
			buttons.put("partnerB2B", 					getElementVisibility("radiobutton_partner_b2b_id"));
			buttons.put("pdfNo", 						getElementVisibility("radiobutton_pdf_no_id"));
			buttons.put("pdfYes", 						getElementVisibility("radiobutton_pdf_yes_id"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return buttons;
	}
	
	public HashMap<String, Boolean> checkComboboxAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> combobox = new HashMap<>();
		try {
			combobox.put("dateFilter", 					getElementVisibility("combobox_date_filter_xpath"));
			combobox.put("reportFromDate",			 	getElementVisibility("combobox_report_from_date_id"));
			combobox.put("reportFromMonth", 			getElementVisibility("combobox_report_from_month_id"));
			combobox.put("reportFromYear", 				getElementVisibility("combobox_report_from_year_id"));
			combobox.put("reportToDate", 				getElementVisibility("combobox_report_to_date_id"));
			combobox.put("reportToMonth", 				getElementVisibility("combobox_report_to_month_id"));
			combobox.put("reportToYear", 				getElementVisibility("combobox_report_to_year_id"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return combobox;
	}
	
	public HashMap<String, Boolean> checkButtonxAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> button = new HashMap<>();
		try {
			button.put("posLocation", 		getElementVisibility("button_pos_location_name_id"));
			button.put("view", 				getElementVisibility("button_view_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return button;
	}
	
	public void enterCriteria(String resNumber, String product) throws Exception
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		getElement("radiobutton_search_by_reservation_no_id").click();
		Thread.sleep(3000);
		if(product.equals("hotel"))
		{
			getElement("radiobutton_product_type_hotel_id").click();
		}
		else if(product.equals("vacation"))
		{
			getElement("radiobutton_product_type_packge_all_id").click();
		}
		
		
		
		getElement("inputfield_reservationNumber_id").clear();
		getElement("inputfield_reservationNumber_id").setText(resNumber);
		/*getElement("button_reservationNumberLookup_id").click();
		switchToDefaultFrame();
		switchToFrame("frame_lookup_id");
		getElement("label_lookup_record_xpath").click();
		switchToDefaultFrame();
		switchToFrame("frame_reportFrame_id");*/
		getElement("button_view_xpath").click();
	}
	
	public HashMap<String, String> getLabels()
	{

		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, String> labels = new HashMap<>();
		
		try {
			labels.put("productType", 					getElement("label_product_type_id").getText());
			labels.put("productTypeAll", 				getElement("label_product_type_all_id").getText());
			labels.put("productTypeHotel", 				getElement("label_product_type_hotel_id").getText());
			labels.put("ProductTypeAir", 				getElement("label_product_type_air_id").getText());
			labels.put("productTypeCar", 				getElement("label_product_type_car_id").getText());
			labels.put("prodctTypeActivity", 			getElement("label_product_type_activity_id").getText());
			labels.put("productTypePackage", 			getElement("label_product_type_package_id").getText());
			labels.put("searchBy", 						getElement("label_search_by_id").getText());
			labels.put("searchByDate", 					getElement("label_search_by_dates_id").getText());
			labels.put("searchByReservationNumber", 	getElement("label_search_by_reservation_no_id").getText());
			labels.put("searchbyGuestName", 			getElement("label_search_by_guest_name_id").getText());
			labels.put("searchbyXmlPackageReference", 	getElement("label_search_by_xml_package_ref_id").getText());
			labels.put("dateFilter", 					getElement("label_date_filter_xpath").getText());
			labels.put("reportFrom", 					getElement("label_report_from_xpath").getText());
			labels.put("reportTo", 						getElement("label_report_to_xpath").getText());
			labels.put("datesBasedOn", 					getElement("label_dates_based_on_id").getText());
			labels.put("datesBasedonReservationDate", 	getElement("label_dates_based_on_reservation_date_id").getText());
			labels.put("datesBasedOnArrivalDate", 		getElement("label_dates_based_on_arrival_date_id").getText());
			labels.put("datesBasedOnDueDate", 			getElement("label_dates_based_on_due_date_id").getText());
			labels.put("paymentApproved", 				getElement("label_payment_approved_id").getText());
			labels.put("paymentApprovedAll", 			getElement("label_payment_approved_all_id").getText());
			labels.put("paymentApprovedYes", 			getElement("label_payment_approved_yes_id").getText());
			labels.put("paymentApprovedNo", 			getElement("label_payment_approved_no_id").getText());
			labels.put("invoiceIssued", 				getElement("label_invoice_issued_id").getText());
			labels.put("invoiceIssuedAll", 				getElement("label_invoice_issued_all_id").getText());
			labels.put("invoiceIssuedYes", 				getElement("label_invoice_issued_yes_id").getText());
			labels.put("invoiceIssuedNo", 				getElement("label_invoice_issued_no_id").getText());
			labels.put("voucherIssued", 				getElement("label_voucher_issued_id").getText());
			labels.put("voucherIssuedAll", 				getElement("label_voucher_issued_all_id").getText());
			labels.put("voucherIssuedYes", 				getElement("label_voucher_issued_yes_id").getText());
			labels.put("voucherIssuedNo", 				getElement("label_voucher_issued_no_id").getText());
			labels.put("lpoRequired", 					getElement("label_lpo_required_id").getText());
			labels.put("lpoRequiredYes", 				getElement("label_lpo_required_yes_id").getText());
			labels.put("lpoRequiredAll", 				getElement("label_lpo_required_all_id").getText());
			labels.put("lpoRequiredNo", 				getElement("label_lpo_required_no_id").getText());
			labels.put("bookingChannel", 				getElement("label_booking_channel_id").getText());
			labels.put("bookingChannelAll", 			getElement("label_booking_channel_all_id").getText());
			labels.put("bookingChannelCC", 				getElement("label_booking_channel_cc_id").getText());
			labels.put("bookingChannelWeb", 			getElement("label_booking_channel_web_id").getText());
			labels.put("partner", 						getElement("label_partner_id").getText());
			labels.put("partnerAll", 					getElement("label_partner_all_id").getText());
			labels.put("partnerDirectConsumer", 		getElement("label_partner_direct_consumer_id").getText());
			labels.put("partnerAffiliate", 				getElement("label_partner_affiliate_id").getText());
			labels.put("partnerB2bPartner", 			getElement("label_partner_b2bpartner_id").getText());
			labels.put("posLocation", 					getElement("label_pos_locaation_name_xpath").getText());
			labels.put("generatePDF", 					getElement("label_generate_pdf_id").getText());
			labels.put("generatePDFYes", 				getElement("label_generate_pdf_yes_id").getText());
			labels.put("generatePDFNo", 				getElement("label_generate_pdf_no_id").getText());
		}
		catch(Exception e)
		{
			
		}
		return labels;
	
	}
	
	
	
}
