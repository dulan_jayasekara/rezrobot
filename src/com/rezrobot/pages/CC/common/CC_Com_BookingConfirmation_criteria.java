package com.rezrobot.pages.CC.common;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;

public class CC_Com_BookingConfirmation_criteria extends PageObject{

	public void getHotelConfirmationMail(String resNumber) throws Exception
	{
		switchToDefaultFrame();
		switchToFrame("frame_reportFrame_id");
		getElement("radiobutton_reservation_number_id").click();
		getElement("radiobutton_hotel_id").click();
		getElement("radiobutton_reservation_number_id").click();
		Thread.sleep(2000);
		getElement("inputfield_res_number_id").clear();
		getElement("inputfield_res_number_id").setText(resNumber);
		getElement("button_view_xpath").click();
	}
}
