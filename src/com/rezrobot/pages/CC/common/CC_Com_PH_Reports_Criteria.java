package com.rezrobot.pages.CC.common;

import java.awt.print.Pageable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import sun.text.normalizer.CharTrie.FriendAgent;

import com.rezrobot.core.PageObject;
import com.rezrobot.pojo.Calendar;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ReportTemplate;
import com.sun.xml.internal.txw2.Document;

public class CC_Com_PH_Reports_Criteria extends PageObject{
	
	public void validate(HashMap<String, String> reportMap, String url, ReportTemplate printTemplate, HashMap<String, String> reportData) throws Exception
	{
		HashMap<String, String> defaultValues = new HashMap<>();
		HashMap<String, String> enteredValues = new HashMap<>();
		DriverFactory.getInstance().getDriver().get(url);
		switchToDefaultFrame();
		switchToFrame("frame_report_id");
		Iterator it = reportMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        
	        try {
	        	
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
	        
	        if(pair.getKey().toString().equals("reportFrom"))
	        {
	        	if(getElementVisibility("inputfield_report_from_id"))
	        	{
	        		getElements("button_calender_classname").get(0).click();
	        		defaultValues.put("reportFrom", getElements("label_calender_today_classname").get(0).getText() + " " + getElement("label_calender_month_classname").getText());
	        		getElement("inputfield_country_id").click();
	        	}
	        }
	        else if(pair.getKey().toString().equals("reportTo"))
	        {
	        	if(getElementVisibility("inputfield_report_to_id"))
	        	{
	        		getElements("button_calender_classname").get(1).click();
	        		defaultValues.put("reportTo", getElements("label_calender_today_classname").get(0).getText() + " " + getElement("label_calender_month_classname").getText());
	        		getElement("inputfield_country_id").click();
	        	}
	        }
	        else if(pair.getKey().toString().equals("country"))
	        {
	        	if(getElementVisibility("inputfield_country_id"))
	        	{
	        		defaultValues.put("country", getElement("inputfield_country_id").getAttribute("value"));
	        		getElement("inputfield_country_id").clearSetTextSelect(reportData.get("country"));
	        		enteredValues.put("country", getElement("inputfield_country_id").getAttribute("value"));
	        		/*try {
	        			System.out.println(((JavascriptExecutor) DriverFactory.getInstance().getDriver()).executeScript("document.getElementsByTagName('a')[0].textContent;"));
	        			System.out.println(((JavascriptExecutor) DriverFactory.getInstance().getDriver()).executeScript("document.getElementById('ui-id-2').textContent;"));
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
	        		
	        		defaultValues.put("country", getElement("inputfield_country_id").getText());
	        		
	        		WebElement abc = DriverFactory.getInstance().getDriver().findElement(By.tagName("ul"));
	        		System.out.println(abc.findElement(By.tagName("a")).getText());
	        		
	        		System.out.println(getElements("tag_ui_tagname").size());
	        		System.out.println(getElements("tag_ui_tagname").get(0).getText());
	        		
	        		for(int i = 2 ; i < 200 ; i++)
	        		{
	        			try {
	        				System.out.println(getElement("inputfield_country_value_id").changeRefAndGetElement(getElement("inputfield_country_value_id").getRef().replace("replace", String.valueOf(i))).getText());
							break;
						} catch (Exception e) {
							// TODO: handle exception
						}
	        		}
	        		System.out.println(getElement("inputfield_country_id").getText());
	        		System.out.println(getElement("inputfield_country_id").getPlaceHolder());
	        		System.out.println(getElement("inputfield_country_id").getAttribute("name"));*/
	        	}
	        }
	        else if(pair.getKey().toString().equals("city"))
	        {
	        	if(getElementVisibility("inputfield_city_id"))
	        	{
	        		defaultValues.put("city", getElement("inputfield_city_id").getAttribute("value"));
	        		getElement("inputfield_city_id").clearSetTextSelect(reportData.get("city"));
	        		enteredValues.put("city", getElement("inputfield_city_id").getAttribute("value"));
	        		try {
	        			getElement("inputfield_city_id").clearSetTextSelect(reportData.get("cityInvalid"));
		        		enteredValues.put("cityInvalid", "true");		        		
					} catch (Exception e) {
						// TODO: handle exception
						enteredValues.put("cityInvalid", "false");
					}
	        	}
	        }
	        else if(pair.getKey().toString().equals("hotelName"))
	        {
	        	if(getElementVisibility("inputfield_hotel_name_id"))
	        	{
	        		defaultValues.put("hotelName", getElement("inputfield_hotel_name_id").getAttribute("value"));
	        		getElement("inputfield_hotel_name_id").clearSetTextSelect(reportData.get("hotelName"));
	        		enteredValues.put("hotelName", getElement("inputfield_hotel_name_id").getAttribute("value"));
	        	}
	        }
	        else if(pair.getKey().toString().equals("programName"))
	        {
	        	if(getElementVisibility("inputfield_program_name_id"))
	        	{
	        		defaultValues.put("programName", getElement("inputfield_program_name_id").getAttribute("value"));
	        		getElement("inputfield_program_name_id").clearSetTextSelect(reportData.get("programName"));
	        		enteredValues.put("programName", getElement("inputfield_program_name_id").getAttribute("value"));
	        	}
	        }
	        else if(pair.getKey().toString().equals("supplierName"))
	        {
	        	if(getElementVisibility("inputfield_supplier_id"))
	        	{
	        		defaultValues.put("supplierName", getElement("inputfield_supplier_id").getAttribute("value"));
	        		getElement("inputfield_supplier_id").clearSetTextSelect(reportData.get("supplierName"));
	        		enteredValues.put("supplierName", getElement("inputfield_supplier_id").getAttribute("value"));
	        	}
	        }
	        else if(pair.getKey().toString().equals("searchBy"))
	        {
	        	if(getElementVisibility("radiobutton_search_by_name"))
	        	{
	        		defaultValues.put("searchBy", getElement("radiobutton_search_by_name").getAttribute("value"));
	        	} 	
	        }
	        else if(pair.getKey().toString().equals("contentOf"))
	        {
	        	if(getElementVisibility("radiobutton_report_content_of_name"))
	        	{
	        		defaultValues.put("contentOf", getElement("radiobutton_report_content_of_name").getAttribute("value"));
	        	}   	
	        }
	        else if(pair.getKey().toString().equals("searchByOption"))
	        {
	        	if(getElementVisibility("radiobutton_search_by_option_name"))
	        	{
	        		defaultValues.put("searchByOption", getElement("radiobutton_search_by_option_name").getAttribute("value"));
	        	}  	
	        }
	        else if(pair.getKey().toString().equals("viewReport"))
	        {
	        	if(getElementVisibility("radiobutton_view_report_name"))
	        	{
	        		defaultValues.put("viewReport", getElement("radiobutton_view_report_name").getAttribute("value"));
	        	}
	        }
	        else
	        {
	        	
	        }
	        
	       // System.out.println(pair.getKey() + " = " + pair.getValue());
	        it.remove(); 
	    } 
	    verify(printTemplate, defaultValues, enteredValues);
	}
	
	public void verify(ReportTemplate printTemplate, HashMap<String, String> defaultValues, HashMap<String, String> enteredValues)
	{
		printTemplate.verifyTrue("DatePeriod", 						defaultValues.get("searchBy"), 			"PH reports - Criteria validations - Radio buttons default value - Search By");
		printTemplate.verifyTrue("d", 								defaultValues.get("contentOf"), 		"PH reports - Criteria validations - Radio buttons default value - Content Of");
		printTemplate.verifyTrue("c", 								defaultValues.get("searchByOption"), 	"PH reports - Criteria validations - Radio buttons default value - Search By Option");
		printTemplate.verifyTrue("Detailed", 						defaultValues.get("viewReport"), 		"PH reports - Criteria validations - Radio buttons default value - View By");
		
		printTemplate.verifyTrue(Calender.getToday("dd-MMM-yyyy"), 	defaultValues.get("reportFrom"), 		"PH reports - Criteria validations - Inputfields Default value - Report From");
		printTemplate.verifyTrue(Calender.getToday("dd-MMM-yyyy"), 	defaultValues.get("reportTo"), 			"PH reports - Criteria validations - Inputfields Default value - Report To");
		printTemplate.verifyTrue("ALL", 							defaultValues.get("country"), 			"PH reports - Criteria validations - Inputfields Default value - Country");
		printTemplate.verifyTrue("ALL", 							defaultValues.get("city"), 				"PH reports - Criteria validations - Inputfields Default value - City");
		printTemplate.verifyTrue("ALL", 							defaultValues.get("hotelName"), 		"PH reports - Criteria validations - Inputfields Default value - Hotel Name");
		printTemplate.verifyTrue("ALL", 							defaultValues.get("programName"), 		"PH reports - Criteria validations - Inputfields Default value - Program Name");
		printTemplate.verifyTrue("ALL", 							defaultValues.get("supplierName"), 		"PH reports - Criteria validations - Inputfields Default value - Supplier Name");
	}
	
	public boolean checkReportAvailability(String url) throws Exception
	{
		DriverFactory.getInstance().getDriver().get("url");
		getElement("button_view_id").click();
		try {
			return getElementVisibility("label_table_Availability_id", 20);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
}
