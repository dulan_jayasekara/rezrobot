package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;


public class CC_Com_Reservation_Report extends PageObject{

	public HashMap<String, Boolean> hotelResReportLabelAvailability()
	{
		HashMap<String, Boolean> labels = new HashMap<>();
		try {
			labels.put("more", 							getElementVisibility("label_Hotel_more_label_xpath"));
			labels.put("info", 							getElementVisibility("label_Hotel_info_label_xpath"));
			labels.put("edit", 							getElementVisibility("label_Hotel_edit_label_xpath"));
			labels.put("bookingNumber", 				getElementVisibility("label_Hotel_booking_number_label_xpath"));
			labels.put("documentNumber", 				getElementVisibility("label_Hotel_document_number_label_xpath"));
			labels.put("bookingDate", 					getElementVisibility("label_Hotel_booking_date_label_xpath"));
			labels.put("notes", 						getElementVisibility("label_Hotel_notes_label_xpath"));
			labels.put("supplierName", 					getElementVisibility("label_Hotel_supplier_name_label_xpath"));
			labels.put("hotelName", 					getElementVisibility("label_Hotel_hotel_name_label_xpath"));
			labels.put("bookingType", 					getElementVisibility("label_Hotel_booking_type_label_xpath"));
			labels.put("firstName", 					getElementVisibility("label_Hotel_first_name_label_xpath"));
			labels.put("customer", 						getElementVisibility("label_Hotel_customer_label_xpath"));
			labels.put("consultantName", 				getElementVisibility("label_Hotel_consaltant_name_label_xpath"));
			labels.put("arrival", 						getElementVisibility("label_Hotel_arrival/departure_label_xpath"));
			labels.put("roomType", 						getElementVisibility("label_Hotel_room_type_label_xpath"));
			labels.put("bedType", 						getElementVisibility("label_Hotel_bed_type_label_xpath"));
			labels.put("supConfirmationNumber", 		getElementVisibility("label_Hotel_sup_confirmation_number_label_xpath"));
			labels.put("contactNumber", 				getElementVisibility("label_Hotel_contact_number_label_xpath"));
			labels.put("transactionId", 				getElementVisibility("label_Hotel_transaction_id_label_xpath"));
			labels.put("paymentType", 					getElementVisibility("label_Hotel_payment_type_label_xpath"));
			labels.put("hotelConfirmationNumber", 		getElementVisibility("label_Hotel_hotel_confirmation_number_label_xpath"));
			labels.put("orderId", 						getElementVisibility("label_Hotel_order_id_label"));
			labels.put("sellingCurrency", 				getElementVisibility("label_Hotel_selling_currency_label_xpath"));
			labels.put("totalCost", 					getElementVisibility("label_Hotel_total_cost_label_xpath"));
			labels.put("bookingFee", 					getElementVisibility("label_Hotel_hotel_booking_fee_label_xpath"));
			labels.put("orderValue", 					getElementVisibility("label_Hotel_order_value_xpath"));
			labels.put("rooms", 						getElementVisibility("label_Hotel_rooms_label_xpath"));
			labels.put("totalCostInPortalCurrency", 	getElementVisibility("label_Hotel_total_cost_in_portal_currency_label_xpath"));
			labels.put("orderValueInPortalCurrency", 	getElementVisibility("label_Hotel_order_value_in_portal_currency_label_xpath"));
			labels.put("pageTotal", 					getElementVisibility("label_Hotel_page_total_label_xpath"));	
			labels.put("grandTotal", 					getElementVisibility("label_Hotel_grand_total_label_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> hotelResReportGetLabels()
	{
		HashMap<String, String> labels = new HashMap<>();
		try {
			labels.put("more", 							getElement("label_Hotel_more_label_xpath").getText());
			labels.put("info", 							getElement("label_Hotel_info_label_xpath").getText());
			labels.put("edit", 							getElement("label_Hotel_edit_label_xpath").getText());
			labels.put("bookingNumber", 				getElement("label_Hotel_booking_number_label_xpath").getText());
			labels.put("documentNumber", 				getElement("label_Hotel_document_number_label_xpath").getText());
			labels.put("bookingDate", 					getElement("label_Hotel_booking_date_label_xpath").getText());
			labels.put("notes", 						getElement("label_Hotel_notes_label_xpath").getText());
			labels.put("supplierName", 					getElement("label_Hotel_supplier_name_label_xpath").getWebElements().get(1).getText());
			labels.put("hotelName", 					getElement("label_Hotel_hotel_name_label_xpath").getText());
			labels.put("bookingType", 					getElement("label_Hotel_booking_type_label_xpath").getText());
			labels.put("firstName", 					getElement("label_Hotel_first_name_label_xpath").getText());
			labels.put("customer", 						getElement("label_Hotel_customer_label_xpath").getText());
			labels.put("consultantName", 				getElement("label_Hotel_consaltant_name_label_xpath").getWebElements().get(1).getText());
			labels.put("arrival", 						getElement("label_Hotel_arrival/departure_label_xpath").getText());
			labels.put("roomType", 						getElement("label_Hotel_room_type_label_xpath").getText());
			labels.put("bedType", 						getElement("label_Hotel_bed_type_label_xpath").getText());
			labels.put("supConfirmationNumber", 		getElement("label_Hotel_sup_confirmation_number_label_xpath").getText());
			labels.put("contactNumber", 				getElement("label_Hotel_contact_number_label_xpath").getWebElements().get(1).getText());
			labels.put("transactionId", 				getElement("label_Hotel_transaction_id_label_xpath").getText());
			labels.put("paymentType", 					getElement("label_Hotel_payment_type_label_xpath").getText());
			labels.put("hotelConfirmationNumber", 		getElement("label_Hotel_hotel_confirmation_number_label_xpath").getText());
			labels.put("orderId", 						getElement("label_Hotel_order_id_label").getText());
			labels.put("sellingCurrency", 				getElement("label_Hotel_selling_currency_label_xpath").getText());
			labels.put("totalCost", 					getElement("label_Hotel_total_cost_label_xpath").getText());
			labels.put("bookingFee", 					getElement("label_Hotel_hotel_booking_fee_label_xpath").getText());
			labels.put("orderValue", 					getElement("label_Hotel_order_value_xpath").getText());
			labels.put("rooms", 						getElement("label_Hotel_rooms_label_xpath").getText());
			labels.put("totalCostInPortalCurrency", 	getElement("label_Hotel_total_cost_in_portal_currency_label_xpath").getText());
			labels.put("orderValueInPortalCurrency", 	getElement("label_Hotel_order_value_in_portal_currency_label_xpath").getText());
			labels.put("pageTotal", 					getElement("label_Hotel_page_total_label_xpath").getText());	
			labels.put("grandTotal", 					getElement("label_Hotel_grand_total_label_xpath").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, Boolean> hotelResReportGetValueAvailability()
	{
		HashMap<String, Boolean> values = new HashMap<>();
		try {
			values.put("bookingNumber", 				getElementVisibility("label_hotel_bookingNumber_xpath"));
			values.put("documentNumber", 				getElementVisibility("label_hotel_document_number_xpath"));
			values.put("booingDate", 					getElementVisibility("label_hotel_booking_date_xpath"));
			values.put("notes", 						getElementVisibility("label_hotel_notes_xpath"));
			values.put("supplierName", 					getElementVisibility("label_hotel_supplier_name_xpath"));
			values.put("hotelName", 					getElementVisibility("label_hotel_hotel_name_xpath"));
			values.put("bookingType", 					getElementVisibility("label_hotel_booking_type_xpath"));
			values.put("firstName", 					getElementVisibility("label_hotel_first_name_xpath"));
			values.put("customerType", 					getElementVisibility("label_hotel_customer_type_xpath"));
			values.put("consultantName", 				getElementVisibility("label_hotel_consultant_name_xpath"));
			values.put("arrival", 						getElementVisibility("label_hotel_arrival_xpath"));
			values.put("roomType", 						getElementVisibility("label_hotel_room_type_xpath"));
			values.put("bedType", 						getElementVisibility("label_hotel_bed_type_xpath"));
			values.put("supplierConfirmationNumber"	, 	getElementVisibility("label_hotel_supplier_confirmation_number_xpath"));
			values.put("contactNumber", 				getElementVisibility("label_hotel_contact_number_xpath"));
			values.put("transactionId", 				getElementVisibility("label_hotel_transaction_id_xpath"));
			values.put("paymentType", 					getElementVisibility("label_hotel_payment_type_xpath"));
			values.put("confirmationNumber", 			getElementVisibility("label_hotel_confirmation_number_xpath"));
			values.put("orderId", 						getElementVisibility("label_hotel_order_id_xpath"));
			values.put("sellingCurrency", 				getElementVisibility("label_hotel_selling_currency_xpath"));
			values.put("totalCost", 					getElementVisibility("label_hotel_total_cost_xpath"));
			values.put("bookingFee", 					getElementVisibility("label_hotel_hotel_booking_fee_xpath"));
			values.put("orderValue", 					getElementVisibility("label_hotel_order_value_xpath"));
			values.put("rooms", 						getElementVisibility("label_hotel_rooms_xpath"));
			values.put("totalCost", 					getElementVisibility("label_hotel_total_cost_in_portal"));
			values.put("orderValueInPortalCurrency"	, 	getElementVisibility("label_hotel_order_value_in_portal"));
			values.put("pageTotalTotalCost", 			getElementVisibility("label_hotel_page_total_total_cost_xpath"));
			values.put("pageTotalOrderValue", 			getElementVisibility("label_hotel_page_total_order_value_xpath"));
			values.put("grandTotalTotalCost", 			getElementVisibility("label_hotel_grand_total_total_cost_xpath"));
			values.put("geandTotalOrderValue", 			getElementVisibility("label_hotel_grand_total_order_value_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	public HashMap<String, String> getValuesHotel()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("bookingNumber", 				getElement("label_hotel_bookingNumber_xpath").getText());
			values.put("documentNumber", 				getElement("label_hotel_document_number_xpath").getText());
			values.put("booingDate", 					getElement("label_hotel_booking_date_xpath").getText());
			values.put("notes", 						getElement("label_hotel_notes_xpath").getText());
			values.put("supplierName", 					getElement("label_hotel_supplier_name_xpath").getText());
			values.put("hotelName", 					getElement("label_hotel_hotel_name_xpath").getText());
			values.put("bookingType", 					getElement("label_hotel_booking_type_xpath").getText());
			values.put("firstName", 					getElement("label_hotel_first_name_xpath").getText());
			values.put("customerType", 					getElement("label_hotel_customer_type_xpath").getText());
			values.put("consultantName", 				getElement("label_hotel_consultant_name_xpath").getText());
			values.put("arrival", 						getElement("label_hotel_arrival_xpath").getText());
			values.put("roomType", 						getElement("label_hotel_room_type_xpath").getText());
			values.put("bedType", 						getElement("label_hotel_bed_type_xpath").getText());
			values.put("supplierConfirmationNumber"	, 	getElement("label_hotel_supplier_confirmation_number_xpath").getText());
			values.put("contactNumber", 				getElement("label_hotel_contact_number_xpath").getText());
			values.put("transactionId", 				getElement("label_hotel_transaction_id_xpath").getText());
			values.put("paymentType", 					getElement("label_hotel_payment_type_xpath").getText());
			values.put("confirmationNumber", 			getElement("label_hotel_confirmation_number_xpath").getText());
			values.put("orderId", 						getElement("label_hotel_order_id_xpath").getText());
			values.put("sellingCurrency", 				getElement("label_hotel_selling_currency_xpath").getText());
			values.put("totalCostSelling", 				getElement("label_hotel_total_cost_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("bookingFee", 					getElement("label_hotel_hotel_booking_fee_xpath").getText().replace(",", "").replace(".0", ""));
			values.put("orderValue", 					getElement("label_hotel_order_value_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("rooms", 						getElement("label_hotel_rooms_xpath").getText());
			values.put("totalCostPortal", 				getElement("label_hotel_total_cost_in_portal").getText().replace(",", "").replace(".00", ""));
			values.put("orderValueInPortalCurrency"	, 	getElement("label_hotel_order_value_in_portal").getText().replace(",", "").replace(".00", ""));
			values.put("pageTotalTotalCost", 			getElement("label_hotel_page_total_total_cost_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("pageTotalOrderValue", 			getElement("label_hotel_page_total_order_value_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("grandTotalTotalCost", 			getElement("label_hotel_grand_total_total_cost_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("geandTotalOrderValue", 			getElement("label_hotel_grand_total_order_value_xpath").getText().replace(",", "").replace(".00", ""));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	public HashMap<String, String> getValuesVacation()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("bookingNumber", 			getElement("label_vacation_booking_number_xpath").getText());
			values.put("documentNumber", 			getElement("label_vacation_document_number_xpath").getText());
			values.put("bookingDate", 				getElement("label_vacation_booking_date_xpath").getText());
			values.put("customerType",		 		getElement("label_vacation_customer_type_xpath").getText());
			values.put("customer", 					getElement("label_vacation_customer_xpath").getText());
			values.put("productType", 				getElement("label_vacation_product_type_xpath").getText());
			values.put("consultantName", 			getElement("label_vacation_consultant_name_xpath").getText());
			values.put("bookingStatus", 			getElement("label_vacation_booking_status_xpath").getText());
			values.put("bookingChannel", 			getElement("label_vacation_booking_channel_xpath").getText());
			values.put("docsIssued", 				getElement("label_vacation_docs_issed_xpath").getText());
			values.put("supplierName", 				getElement("label_vacation_supplier_name_xpath").getText());
			values.put("supplierConNumber", 		getElement("label_vacation_supplier_con_number_xpath").getText());
			values.put("guestName", 				getElement("label_vacation_guest_name_xpath").getText());
			values.put("paymentTypeReference", 		getElement("label_vacation_payment_type_reference_xpath").getText());
			values.put("contactNumber", 			getElement("label_vacation_contact_number_xpath").getText());
			values.put("transactionID", 			getElement("label_vacation_trasaction_id_xpath").getText());
			values.put("orderID", 					getElement("label_vacation_order_id_xpath").getText());
			values.put("pgCurrency", 				getElement("label_vacation_pg_currency_xpath").getText());
			values.put("amountChargedByPG", 		getElement("label_vacation_amount_charged_by_pg").getText());
			values.put("sellingCurrency", 			getElement("label_vacation_selling_currency_xpath").getText());
			values.put("totalRate", 				getElement("label_vacation_total_rate_xpath").getText().replace(",", ""));
			values.put("ccfee", 					getElement("label_vacation_ccfee_xpath").getText().replace(",", ""));
			values.put("bookingFee", 				getElement("label_vacation_booking_fee_xpath").getText().replace(",", ""));
			values.put("amountPaid", 				getElement("label_vacation_amount_paid_xpath").getText().replace(",", ""));
			values.put("grossOrderValue", 			getElement("label_vacation_gross_order_value_xpath").getText().replace(",", ""));
			values.put("agentCommision", 			getElement("label_vacation_agent_commision_xpath").getText().replace(",", ""));
			values.put("netOrderValue", 			getElement("label_vacation_net_order_value_xpath").getText().replace(",", ""));
			values.put("orderValue", 				getElement("label_vacation_order_value_xpath").getText().replace(",", ""));
			values.put("grossoRderValue", 			getElement("label_vacation_gross_order_value_in_pc_xpath").getText().replace(",", ""));
			values.put("netOrderValueInPC", 		getElement("label_vacation_net_order_value_in_pc_xpath").getText().replace(",", ""));
			values.put("orderValueInPC", 			getElement("label_vacation_order_value_in_pc_xpath").getText().replace(",", ""));
			values.put("pageTotalNetOrderValue", 	getElement("label_vacation_page_total_net_order_value_xpath").getText().replace(",", ""));
			values.put("pageTotalorderValue", 		getElement("label_vacation_page_total_order_value_xpath").getText().replace(",", ""));
			values.put("grandTotalNetOrderValue", 	getElement("label_vacation_grand_total_net_order_value_xpath").getText().replace(",", ""));
			values.put("grandTotalOrderValue", 		getElement("label_vacation_grand_total_order_value_xpath").getText().replace(",", ""));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	public HashMap<String, Boolean> getLabelsAvailabiltiyVacation()
	{
		HashMap<String, Boolean> values = new HashMap<>();
		try {
			values.put("edit", 							getelementAvailability("label_vacation_edit_label_xpath"));
			values.put("bookingNumber", 				getelementAvailability("label_vacation_booking_number_label_xpath"));
			values.put("docNumber", 					getelementAvailability("label_vacation_document_number_label_xpath"));
			values.put("bookingDate", 					getelementAvailability("label_vacation_booking_date_label_xpath"));
			values.put("customerType", 					getelementAvailability("label_vacation_customer_type_label_xpath"));
			values.put("customerLabel", 				getelementAvailability("label_vacation_customer_label_xpath"));
			values.put("productType", 					getelementAvailability("label_vacation_product_type_label_xpath"));
			values.put("consultantName", 				getelementAvailability("label_vacation_consultant_name_label_xpath"));
			values.put("bookingStatus", 				getelementAvailability("label_vacation_booking_status_label_xpath"));
			values.put("bookingChannel", 				getelementAvailability("label_vacation_booking_channel_label_xpath"));
			values.put("docsIssued", 					getelementAvailability("label_vacation_docs_issued_label_xpath"));
			values.put("supplierName", 					getelementAvailability("label_vacation_supplier_name_label_xpath"));
			values.put("supplierConfirmationNumber", 	getelementAvailability("label_vacation_supplier_con_number_label_xpath"));
			values.put("guestName", 					getelementAvailability("label_vacation_guest_name_label_xpath"));
			values.put("paymentTypeReference", 			getelementAvailability("label_vacation_payment_type_reference_label_xpath"));
			values.put("contactNumber", 				getelementAvailability("label_vacation_contact_number_label_xpath"));
			values.put("transIDauthID", 				getelementAvailability("label_vacation_transid_authid_label_xpath"));
			values.put("vacationOrderID", 				getelementAvailability("label_vacation_orderid_label_xpath"));
			values.put("pgCurrency", 					getelementAvailability("label_vacation_pg_currency_label_xpath"));
			values.put("amountChargedByPG", 			getelementAvailability("label_vacation_amount_charged_by_pg_label_xpath"));
			values.put("sellingCurrency", 				getelementAvailability("label_vacation_selling_currency_label_xpath"));
			values.put("totalRate", 					getelementAvailability("label_vacation_total_rate_label_xpath"));
			values.put("ccFee", 						getelementAvailability("label_vacation_ccfee_label_xpath"));
			values.put("bookingFee", 					getelementAvailability("label_vacation_booking_fee_label_xpath"));
			values.put("amountPaid", 					getelementAvailability("label_vacation_amount_paid_label_xpath"));
			values.put("grossOrderValue", 				getelementAvailability("label_vacation_gross_order_value_label_xpath"));
			values.put("agentCommision", 				getelementAvailability("label_vacation_agent_commision_label_xpath"));
			values.put("netOrderValue", 				getelementAvailability("label_vacation_net_order_value_label_xpath"));
			values.put("orderValue", 					getelementAvailability("label_vacation_order_value_label_xpath"));
			values.put("grossOrderInPC", 				getelementAvailability("label_vacation_gross_order_in_pc_label_xpath"));
			values.put("netOrderValueInPC",		 		getelementAvailability("label_vacation_net_order_value_in_pc_label_xpath"));
			values.put("orderValueInPC", 				getelementAvailability("label_vacation_order_value_inpv_label_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	
}
