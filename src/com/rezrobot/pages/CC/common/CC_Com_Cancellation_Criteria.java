package com.rezrobot.pages.CC.common;

import com.rezrobot.core.PageObject;

public class CC_Com_Cancellation_Criteria extends PageObject{
	
	public void enterCriteria(String resNumber) throws Exception
	{
		switchToDefaultFrame();
		getElement("inputfield_res_number_id").setText(resNumber);
		getElement("button_lkup_id").click();
		switchToFrame("frame_lkup_id");
		getElement("button_first_record_xpath").click();
		switchToDefaultFrame();
		getElement("button_cancel_id").click();
	}
}
