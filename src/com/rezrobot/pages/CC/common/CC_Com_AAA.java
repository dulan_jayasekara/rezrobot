package com.rezrobot.pages.CC.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.openqa.selenium.By;

import com.rezrobot.core.PageObject;
import com.rezrobot.flight_circuitry.Initiator;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExcelReader;
import com.rezrobot.utill.ReportTemplate;

public class CC_Com_AAA extends PageObject{

	String officeid = "";
	String setAAA = "";
	String action = "";
	public String getOfficeid() {
		return officeid;
	}
	public void setOfficeid(String officeid) {
		this.officeid = officeid;
	}
	public String getSetAAA() {
		return setAAA;
	}
	public void setSetAAA(String setAAA) {
		this.setAAA = setAAA;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
	
	public void setup(String url, ReportTemplate PrintTemplate) throws Exception
	{
		DriverFactory.getInstance().getDriver().get(url);
		Initiator readExcel = new Initiator();
		ArrayList<CC_Com_AAA> aaaList 				= readExcel.getAAADetails();
		ArrayList<CC_Com_AAA> tableDetails			= getTableDetails();
	//	PrintTemplate.setInfoTableHeading("AAA Table details");
		for(int i = 0 ; i < tableDetails.size() ; i++)
		{
	//		PrintTemplate.addToInfoTabel("Office ID / Status ", tableDetails.get(i).getOfficeid().concat(" / ").concat(tableDetails.get(i).getSetAAA()));
		}
	//	PrintTemplate.markTableEnd();
		
	//	PrintTemplate.setTableHeading("AAA Verification");
		for(int i = 0 ; i < aaaList.size() ; i++)
		{
			if(aaaList.get(i).getAction().equals("add"))
			{
				getElement("inputfield_officeid_id").clear();
				getElement("inputfield_officeid_id").setText(aaaList.get(i).getOfficeid());
				getElement("button_officeid_lkup_id").click();
				try {
					switchToFrame("frame_lookup_id");
					getElement("label_first_record_xpath").click();
					switchToDefaultFrame();
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("Office id unavailable");
				}
				
				
				if(DriverFactory.getInstance().getDriver().findElements(By.name("status")).get(1).isSelected())
				{
					getElements("checkbox_aaa_status_name").get(1).click();
				}
				if(aaaList.get(i).getSetAAA().equals("yes"))
				{
					getElements("checkbox_aaa_status_name").get(1).click();
				}
				String aaaAvailability = "";
				for(int j = 0 ; j < tableDetails.size() ; j++)
				{
					if(aaaList.get(i).getOfficeid().equals(tableDetails.get(j).getOfficeid()))
					{
						aaaAvailability = aaaList.get(i).getOfficeid();
					}
				}
				if(aaaAvailability.equals(""))
				{
					getElement("button_add_officeid_id").click();
					if(getElementVisibility("label_confirmation_msg_id"))
					{
						PrintTemplate.verifyTrue("The Record Successfully Added.", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
						getElement("button_error_ok_xpath").click();
					}
					else
					{
						PrintTemplate.verifyTrue("The Record Successfully Added.", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
					}
				}
				else
				{
					getElement("button_add_officeid_id").click();
					if(getElementVisibility("label_confirmation_msg_id"))
					{
						PrintTemplate.verifyTrue("This record already Available.", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
						getElement("button_error_ok_xpath").click();
					}
					else
					{
						PrintTemplate.verifyTrue("This record already Available.", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
					}
				}
			}
			else if(aaaList.get(i).getAction().equals("remove"))
			{
				for(int j = 2 ; j > 0 ; j++)
				{
					try {
						if(aaaList.get(i).getOfficeid().equals(getElement("label_officeid_xpath").changeRefAndGetElement(getElement("label_officeid_xpath").getRef().replace("replace", String.valueOf(j))).getText()))
						{
							System.out.println(getElement("button_remove_xpath").changeRefAndGetElement(getElement("button_remove_xpath").getRef().replace("replace", String.valueOf(j))).getRef());
							getElement("button_remove_xpath").changeRefAndGetElement(getElement("button_remove_xpath").getRef().replace("replace", String.valueOf(j))).click();
							if(getElementVisibility("label_confirmation_msg_id"))
							{
								PrintTemplate.verifyTrue("Do you want to remove the record ?", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
								getElement("button_remove_confirm_xpath").click();
								PrintTemplate.verifyTrue("Succesfully Removed", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
								getElement("button_error_ok_xpath").click();
							}
							else
							{
								PrintTemplate.verifyTrue("Do you want to remove the record ?", getElement("label_confirmation_msg_id").getText(), aaaList.get(i).getOfficeid());
							}
							
						}
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					
				}
			}
		}
	//	PrintTemplate.markTableEnd();
		getElement("button_save").click();
	//	PrintTemplate.setInfoTableHeading("Confirmation details");
	//	PrintTemplate.addToInfoTabel("Saved status", getElement("label_confirmation_msg_id").getText());
	//	PrintTemplate.markTableEnd();
		getElement("button_error_ok_xpath").click();
		
	}
	
	public ArrayList<CC_Com_AAA> getTableDetails() throws Exception
	{
		ArrayList<CC_Com_AAA> aaaList = new ArrayList<CC_Com_AAA>();
		for(int i = 2 ; i > 0 ; i++)
		{
			CC_Com_AAA aaa = new CC_Com_AAA();
			try {	
				aaa.setOfficeid(getElement("label_officeid_xpath").changeRefAndGetElement(getElement("label_officeid_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				aaa.setSetAAA(getElement("label_status_xpath").changeRefAndGetElement(getElement("label_status_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				aaaList.add(aaa);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
		}
		return aaaList;
	}
}
