package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_SearchObject;
import com.rezrobot.pages.CC.fixed_packages.CC_FxP_ResultsPage;
import com.rezrobot.pages.web.fixed_packages.Web_FxP_ResultsPage;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.InputFiled;

public class CC_Com_SearchCriteria extends PageObject {
	
	public boolean isBECframeAvailable() {
		
		try {
			
			switchToDefaultFrame();
			switchToFrame("frame_mainFrame_id");
			
			System.out.println(getelementAvailability("frame_becFrame_id"));
			switchToFrame("frame_becFrame_id");
			addobjects();
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
	}
	
	
public boolean isFPPRoductEnabled() {
		
		try {
			getElement("lnk_FixedPackagelink_id").click();
			addobjects();
			
			return true;
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}
		
		
	}

public void bookingEngineFillFixedPackages(/*FxP_SearchObject search*/) throws Exception {
	
	String search="Including Flight ";
	
	try {
		//((ComboBox)getElement("combobox_ResidenceCountryFP_id")).selectOptionByText(search.getCountryOfResidence());
		((ComboBox)getElement("combobox_ResidenceCountryFP_id")).selectOptionByText("USA");

	} catch (Exception e) {
		// TODO: handle exception
	}
	
	try {
		
		
		
		if (search.contains("Including Flight ")/*search.getFlightStatus().contains("Including Flight ")*/) {
			
						
			getElementClick("radiobtn_PackageTypeF_id");
			
		//	((InputFiled)getElement("input_OriginAir_id")).setText(search.getDeparture());
			((InputFiled)getElement("input_OriginAir_id")).setText("Muscat");
			//getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('"+search.getDepartureValue()+"');");

			getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");

			//getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('"+"Dubai|DXB|4|Dubai International Airport|-|53||United Arab Emirates"+"');");

			Thread.sleep(4000);
			//((InputFiled)getElement("input_DestinationAir_id")).setText(search.getArrival());	
			
			((InputFiled)getElement("input_DestinationAir_id")).setText("Dubai");	
			Thread.sleep(4000);
			
			//getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+search.getArrivalValue()+"');");
			
			getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Dubai|DXB|4|Dubai International Airport|-|53||United Arab Emirates"+"');");
			/*getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");
			getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");
*/
		}else {
			
			getElementClick("radiobtn_PackageTypeL_id");
			
		/*	((InputFiled)getElement("input_DestinationLand_id")).setText(search.getArrival());
			getElement("input_DestinationLandvalue_id").setJavaScript("$('#hid_package_Ex_des').val('"+search.getArrivalValue()+"');");
*/
			
		}
		
	

		
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	
	
	
}

public String sameOriginDestination() throws Exception {
	
	String errormessage="Message not Generated";
	((ComboBox)getElement("combobox_ResidenceCountryFP_id")).selectOptionByText("USA");
	((InputFiled)getElement("input_OriginAir_id")).setText("Muscat");
	getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");
	Thread.sleep(8000);
	((InputFiled)getElement("input_DestinationAir_id")).setText("Muscat");	

	getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");


	
	return null;
	
	
}

public String  mandatoryfieldsValidation(String CheckedField) throws Exception {
	
	String Validationapplied="";
	UIElement targetelement=new UIElement();
	
	if (CheckedField=="CountryOfResidence") {
		targetelement=getElement("combobox_ResidenceCountryFP_id");
		
	}else if (CheckedField=="Origin") {
		targetelement=getElement("input_OriginAir_id");
	}else if (CheckedField=="Destination") {
		targetelement=getElement("input_DestinationAir_id");
	}
	
	if (CheckedField!="CountryOfResidence") {
		((ComboBox)getElement("combobox_ResidenceCountryFP_id")).selectOptionByText("USA");
	
	}
	if (CheckedField!="Origin") {
		((InputFiled)getElement("input_OriginAir_id")).setText("Muscat");
		getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('"+"Muscat|MCT|872|Muscat International Airport|-|231||Oman"+"');");
		
		
	}
	if (CheckedField!="Destination") {
		
		((InputFiled)getElement("input_DestinationAir_id")).setText("Dubai");	
		
		
		getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('"+"Dubai|DXB|4|Dubai International Airport|-|53||United Arab Emirates"+"');");
	
		
	}
	
	getElement("btn_FPsearch_id").click();
	Validationapplied=targetelement.getAttribute("require");
	System.out.println(Validationapplied);
	
	
	
	
	return Validationapplied;
	
	
	
}

public HashMap<String, Boolean> getAvailabilityOfInputFields_FP() throws Exception {
	HashMap<String, Boolean> fileds = new HashMap<String, Boolean>();
	
	try {
		
		fileds.put("cor", getElementVisibility("combobox_ResidenceCountryFP_id"));
		fileds.put("includingflight", getElementVisibility("radiobtn_PackageTypeF_id"));
		fileds.put("excludingflight", getElementVisibility("radiobtn_PackageTypeL_id"));
		fileds.put("origin", getElementVisibility("input_OriginAir_id"));
		fileds.put("destination", getElementVisibility("input_DestinationAir_id"));
		getElement("radiobtn_PackageTypeL_id").click();
		fileds.put("destination_land", getElementVisibility("input_DestinationLand_id"));
		getElement("radiobtn_PackageTypeF_id").click();
		fileds.put("departuremonth", getElementVisibility("combobox_travelon_id"));
		fileds.put("duration", getElementVisibility("combobox_duration_id"));
		getElement("lnk_Searchmoreoptions_xpath").click();
		fileds.put("package_type", getElementVisibility("combobox_packagetype_id"));
	//	fileds.put("prefferedcurrency_FP", getElementVisibility("combobox_preferredCurrencyFP_id"));

		
		
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	return fileds;
	
}

public HashMap<String, String> getLabelTexts_FP() throws Exception {
	HashMap<String, String> labels = new HashMap<String, String>();
	
	try {
		
		//labels.put("Tourplanlabel", getLabelName("lbl_whereUgoinglabel_xpath"));
		labels.put("FlightInclusivelabel", getLabelName("lbl_includingflightlabel_xpath"));
		labels.put("Flightexclusivelabel", getLabelName("lbl_excludingflightlabel_xpath"));
		labels.put("departureairportlabel", getLabelName("lbl_leavingfromlabel_xpath"));
		labels.put("destinationairportlabel", getLabelName("lbl_goingtolabel_xpath"));
		labels.put("destinationcitylabel", getLabelName("lbl_destinationlabel_xpath"));
		labels.put("departurelabel", getLabelName("lbl_travellingwhenlabel_xpath"));
		labels.put("departuremonthlabel", getLabelName("lbl_departuremonthlabel_xpath"));
		labels.put("durationlabel", getLabelName("lbl_durationlabel_xpath"));
		labels.put("packagetypelabel", getLabelName("lbl_packagetypelabel_xpath"));


		
		
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	return labels;
	
}

public void clearEnteredData() throws Exception {
	
	try {
		((ComboBox)getElement("combobox_ResidenceCountryFP_id")).selectOptionByText("Country of Residence");

	} catch (Exception e) {
		// TODO: handle exception
	}
	((InputFiled)getElement("input_OriginAir_id")).clear();
	getElement("input_originAirvalue_id").setJavaScript("$('#hid_package_Loc_ori').val('');");

	((InputFiled)getElement("input_DestinationAir_id")).clear();	
	
	
	getElement("input_DestinationAirvalue_id").setJavaScript("$('#hid_package_Loc_des').val('');");

	
}

public CC_FxP_ResultsPage proceedToNext_FP() throws Exception {
	
	/*try {*/
		//getElement("btn_FPsearch_id").click();
		executejavascript("search('P');");
		CC_FxP_ResultsPage resultsPage=new CC_FxP_ResultsPage();
	/*} catch (Exception e) {
		// TODO: handle exception
	}
	*/
	//getElement("btn_FPsearch_id").click();
	/*Web_FxP_ResultsPage fp_resultspage = new Web_FxP_ResultsPage();
	return fp_resultspage;*/
		return resultsPage;
}


}
