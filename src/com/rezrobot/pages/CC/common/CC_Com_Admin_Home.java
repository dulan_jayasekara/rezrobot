package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.rezrobot.utill.DriverFactory;


public class CC_Com_Admin_Home extends CC_Com_HomePage{
	
	public CC_Com_Admin_Home() throws Exception {
		getElement("link_configurations_id").checkElementVisisbilityWithException(120, "CC_Com_Admin_Home");
	}
	
	public CC_Com_Admin_ConfigurationPage getConfigurationScreen(String portalLink) throws Exception {
		
		
		WebElement mainMenu=getElement("link_configurations_id").getWebElement();
		WebElement subMenu=getElement("link_configurationssubmenu_id").getWebElement();
		
		Actions action=new Actions(DriverFactory.getInstance().getDriver());
		//action.moveToElement(mainMenu).click(subMenu).build().perform();
		DriverFactory.getInstance().getDriver().get(portalLink.concat("admin/configuration/ConfigurationPortalConfigPage.do?module=admin"));
		return new CC_Com_Admin_ConfigurationPage();
		
	}
	
}
