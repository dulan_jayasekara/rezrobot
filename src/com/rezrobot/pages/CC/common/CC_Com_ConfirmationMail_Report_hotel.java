package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;

public class CC_Com_ConfirmationMail_Report_hotel extends PageObject{

	public HashMap<String, String> readHotel() throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportframe_id");
			getElement("checkbox_first_record_id").click();
			getElement("radiobutton_confirmation_mail_id").click();
			switchToFrame("frame_mail_id");
			getElementVisibility("label_portal_detail_xpath", 60);
			values.put("portalInfo", 					getElement("label_portal_detail_xpath").getText());
			values.put("customerName", 					getElement("label_customer_name_xpath").getText());
			values.put("confirmationNumber", 			getElement("label_confirmation_number_xpath").getText());
			values.put("bookingStatus", 				getElement("label_booking_status_xpath").getText());
			values.put("hotelAddress", 					getElement("label_hotel_address1_xpath").getText());
			values.put("hotelAddress2", 				getElement("label_hotel_address2_xpath").getText());
			values.put("hotelTelephone", 				getElement("label_hotel_telephone_number_xpath").getText());
			values.put("hotelEmail",	 				getElement("label_hotel_email_xpath").getText());
			values.put("hotelCheckin", 					getElement("label_hotel_checkin_xpath").getText());
			values.put("hotelCheckout", 				getElement("label_hotel_checkout_xpath").getText());
			values.put("hotelNumOfNights", 				getElement("label_hotel_num_of_nights_xpath").getText());
			values.put("hotelNumOfRooms", 				getElement("label_hotel_num_of_rooms_xpath").getText());
			values.put("hotelBookingStatus", 			getElement("label_hotel_booking_status_xpath").getText());
			values.put("hotelSupConfirmation", 			getElement("label_hotel_supplier_confirmation_number_xpath").getText());
			values.put("hotelName", 					getElement("label_hotel_name_xpath").getText());
			values.put("hotelStarRating", 				getElement("label_hotel_star_rating_xpath").getText());
			int cnt = 1;
			for(int i = 3 ; i > 0 ; i++)
			{
				try {
					values.put("roomDetails".concat(String.valueOf(cnt)), 	getElement("label_room_details_xpath").changeRefAndGetElement(getElement("label_room_details_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("roomType".concat(String.valueOf(cnt)), 		getElement("label_room_type_xpath").changeRefAndGetElement(getElement("label_room_type_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("bedType".concat(String.valueOf(cnt)), 		getElement("label_bed_type_xpath").changeRefAndGetElement(getElement("label_bed_type_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("ratePlan".concat(String.valueOf(cnt)), 		getElement("label_rate_plan_xpath").changeRefAndGetElement(getElement("label_rate_plan_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("roomTotal".concat(String.valueOf(cnt)), 		getElement("label_room_total_xpath").changeRefAndGetElement(getElement("label_room_total_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				i = i+2;
				cnt++;
			}
			//hotel rates
			values.put("currencyCode", 						getElement("label_currency_code_xpath").getText());
			values.put("hotelSubtotal", 					getElement("label_hotel_sub_total_xpath").getText());
			values.put("hotelTotalTaxes", 					getElement("label_hotel_total_taxes_and_service_changes_xpath").getText());
			values.put("hotelAmountTotal", 					getElement("label_hotel_total_xpath").getText());
			values.put("hotelAmountPayableNow", 			getElement("label_hotel_amount_payable_now_xpath").getText());
			values.put("hotelPayAtCheckin", 				getElement("label_hotel_pay_at_checkin_xpath").getText());
			
			//all rates
			values.put("subtotal", 							getElement("label_sub_total_xpath").getText());
			values.put("totalTaxes", 						getElement("label_total_taxes_and_other_charges_xpath").getText());
			values.put("amountTotal", 						getElement("label_total_booking_value").getText());
			values.put("amountPayableNow", 					getElement("label_amount_being_processed_now_xpath").getText());
			values.put("payAtCheckin", 						getElement("label_amount_due_at_checkin_xpath").getText());
			
			//Customer details
			values.put("firstName", 						getElement("label_first_name_xpath").getText());
			values.put("lastName", 							getElement("label_last_name_xpath").getText());
			values.put("address1", 							getElement("label_address1_xpath").getText());
			values.put("address2", 							getElement("label_address2_xpath").getText());
			values.put("city", 								getElement("label_city_xpath").getText());
			values.put("postalCode", 						getElement("label_postal_code_xpath").getText());
			values.put("country", 							getElement("label_country_xpath").getText());
			values.put("phoneNumber", 						getElement("label_phone_number_xpath").getText());
			values.put("emergencyNumber", 					getElement("label_emergency_number_xpath").getText());
			values.put("email", 							getElement("label_email_xapth").getText());
			values.put("state", 							getElement("label_state_xpath").getText());
			
			//occypancy details
			cnt = 1;
			for(int i = 3 ; i > 0 ; i++)
			{
				try {
					values.put("occupancyTitle".concat(String.valueOf(cnt)), 		getElement("label_occupancy_title_xpath").changeRefAndGetElement(getElement("label_occupancy_title_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyFirstName".concat(String.valueOf(cnt)), 	getElement("label_occupancy_first_name_xpath").changeRefAndGetElement(getElement("label_occupancy_first_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyLastName".concat(String.valueOf(cnt)), 	getElement("label_oocupancy_last_name_xpath").changeRefAndGetElement(getElement("label_oocupancy_last_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyChildAge".concat(String.valueOf(cnt)), 	getElement("label_occupancy_child_age_xpath").changeRefAndGetElement(getElement("label_occupancy_child_age_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancySmoking".concat(String.valueOf(cnt)), 		getElement("label_occupancy_smoking_xpath").changeRefAndGetElement(getElement("label_occupancy_smoking_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyHandicap".concat(String.valueOf(cnt)), 	getElement("label_occupancy_handicap_xpath").changeRefAndGetElement(getElement("label_occupancy_handicap_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyWheelchair".concat(String.valueOf(cnt)), 	getElement("label_occupancy_wheelchair_xpath").changeRefAndGetElement(getElement("label_occupancy_wheelchair_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
					break;
				}
				cnt++;
			}
			
			values.put("paymentReference", 					getElement("label_payment_reference_number_xpath").getText());
			values.put("paymentAuth", 						getElement("label_payment_auth_code_xpath").getText());
			values.put("paymentID", 						getElement("label_payment_id_xpath").getText());
			values.put("PaymentAmount", 					getElement("label_payment_amount_xpath").getText());
			values.put("paymentCC",  						getElement("label_payment_cc_xpath").getText());
			
			values.put("customerNotes", 					getElement("label_customer_notes_xpath").getText());
			values.put("hotelNotes", 						getElement("label_hotel_notes_xpath").getText());
		} catch (Exception e) {	
			// TODO: handle exception
			System.out.println(e);
		}
		return values;
	}
}
