package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.rezrobot.core.PageObject;
import com.rezrobot.utill.DriverFactory;

public class CC_Com_BookingList_LiveBookingMoreInfo extends PageObject{

	public HashMap<String, Boolean> getHotelLabelAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_report_frame_id");
			switchToFrame("frame_dialog_window_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> labels = new HashMap<>();
		try {
			labels.put("country", 				getElementVisibility("label_hotel_country_label_xpath"));
			labels.put("city", 					getElementVisibility("label_hotel_city_name_label_xpath"));
			labels.put("supplier", 				getElementVisibility("label_hotel_supplier_label_xpath"));
			labels.put("hotelName", 			getElementVisibility("label_hotel_name_label_xpath"));
			labels.put("arrivalDeparture", 		getElementVisibility("label_hotel_arrival_departure_label_xpath"));
			labels.put("totalRooms", 			getElementVisibility("label_hotel_total_rooms_label_xpath"));
			labels.put("status", 				getElementVisibility("label_hotel_Status_label_xpath"));
			labels.put("sectorBookingValue", 	getElementVisibility("label_hotel_sector_booking_label_xpath"));
			labels.put("shoppingCartCode", 		getElementVisibility("label_hotel_shopping_cart_code_xpath"));
			labels.put("shoppingCart", 			getElementVisibility("label_hotel_shopping_cart_xpath"));
			labels.put("dynamicPackageCode", 	getElementVisibility("label_hotel_dynamic_package_code_xpath"));
			labels.put("dynamicPackage", 		getElementVisibility("label_hotel_dynamic_package_xpath"));
			labels.put("fpCode", 				getElementVisibility("label_hotel_fp_code_xpath"));
			labels.put("fp", 					getElementVisibility("label_hotel_fp_xpath"));
			labels.put("dmcCode", 				getElementVisibility("label_hotel_dmc_code_xpath"));
			labels.put("dmc", 					getElementVisibility("label_hotel_dmc_xpath"));
			labels.put("invoiceIssued", 		getElementVisibility("label_hotel_invoice_issued_xpath"));
			labels.put("paymentReceived", 		getElementVisibility("label_hotel_payment_recieved_xpath"));
			labels.put("voucherIssued", 		getElementVisibility("label_hotel_voucher_issued_xpath"));
			labels.put("ticketIssued", 			getElementVisibility("label_hotel_ticket_issued_xpath"));
			labels.put("emdIssued",	 			getElementVisibility("label_hotel_emd_issued_xpath"));
			labels.put("sectorFailed", 			getElementVisibility("label_hotel_sector_failed_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, Boolean> getVacationLabelAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_report_frame_id");
			switchToFrame("frame_dialog_window_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> availability = new HashMap<>();
		try {
			availability.put("hotelDetails", 			getelementAvailability("label_vacation_hotel_details_label_xpath"));
			availability.put("hotelCountry", 			getelementAvailability("label_vacation_hotel_country_label_xpath"));
			availability.put("hotelCity", 				getelementAvailability("label_vacation_hotel_city_label_xpath"));
			availability.put("hotelSupplier", 			getelementAvailability("label_vacation_hotel_supplier_label_xpath"));
			availability.put("hotelName", 				getelementAvailability("label_vacation_hotel_name_label_xpath"));
			availability.put("hotelCheckin", 			getelementAvailability("label_vacation_hotel_checkin_checkout_label_xpath"));
			availability.put("hotelTotal", 				getelementAvailability("label_vacation_hotel_total_rooms_label_xpath"));
			availability.put("hotelStatus", 			getelementAvailability("label_vacation_hotel_status_label_xpath"));
			availability.put("hotelCandeadline", 		getelementAvailability("label_vacation_hotel_candeadline_label_xpath"));
			availability.put("flightDetails", 			getelementAvailability("label_vacation_flight_flight_details_label_xpat"));
			availability.put("flightDate", 				getelementAvailability("label_vacation_flight_date_label_xpath"));
			availability.put("flightDepartureCountry", 	getelementAvailability("label_vacation_flight_departure_country_label_xpath"));
			availability.put("flightDepartureCityPort", getelementAvailability("label_vacation_flight_departure_city_port_label_xpath"));
			availability.put("flightDepartureTime", 	getelementAvailability("label_vacation_flight_departure_time_label_xpath"));
			availability.put("flightDepartureNumber", 	getelementAvailability("label_vacation_flight_departure_flight_number_label_xpath"));
			availability.put("flightDeparture", 		getelementAvailability("label_vacation_flight_departure_label_xpath"));
			availability.put("flightArrival", 			getelementAvailability("label_vacation_flight_arrival_label_xpath"));
			availability.put("flightArrivalCountry", 	getelementAvailability("label_vacation_flight_arrival_country_label_xpath"));
			availability.put("flightArrivalCityPort", 	getelementAvailability("label_vacation_flight_arivval_city_port_label_xpath"));
			availability.put("flightArrivalTime", 		getelementAvailability("label_vacation_flight_arrival_time_label_xpath"));
			availability.put("flightpax", 				getelementAvailability("label_vacation_flight_pax_label_xpath"));
			availability.put("flightStatus", 			getelementAvailability("label_vacation_flight_status_label_xpath"));
			availability.put("flightTicketIssued", 		getelementAvailability("label_vacation_flight_ticket_issued_xpath"));
			availability.put("flightTicketingDeadline", getelementAvailability("label_vacation_flight_ticketing_deadline_xpath"));
			availability.put("flightCanDeadline", 		getelementAvailability("label_vacation_flight_cancellation_deadline_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return availability;
	}
	
	public HashMap<String, String> getVacationLabels()
	{
		HashMap<String, String> labels = new HashMap<>();
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getVacationValues()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("hotelCountry", 					getElement("label_hotel_country_xpath").getText());
			values.put("hotelCity", 					getElement("label_hotel_city_xpath").getText());
			values.put("hotelSupplier", 				getElement("label_hotel_supplier_xpath").getText());
			values.put("hotelName", 					getElement("label_hotel_hotel_name_xpath").getText());
			values.put("hotelCheckinCheckout", 			getElement("label_hotel_arrival_departure_xpath").getText());
			values.put("hotelTotalRooms", 				getElement("label_hotel_total_rooms_xpath").getText());
			values.put("hotelStatus", 					getElement("label_hotel_status_xpath").getText());
			values.put("hotelCanDeadline", 				getElement("label_hotel_canDeadline_xpath").getText());
			values.put("flightDepartureDate", 			getElement("label_vacation_flight_departure_date_xpath").getText());
			values.put("flightDepartureCountry", 		getElement("label_vacation_flight_departure_country_xpath").getText());
			values.put("flightDepartureCityPort", 		getElement("label_vacation_flight_departure_city_port_xpath").getText());
			values.put("flightDepartureTime", 			getElement("label_vacation_flight_departure_time_xpath").getText());
			values.put("flightDepartureFlightNumber", 	getElement("label_vacation_flight_departure_flight_number_xpath").getText());
			values.put("flightArrivalCountry", 			getElement("label_vacation_flight_arrival_country_xpath").getText());
			values.put("flightArrivalCity", 			getElement("label_vacation_flight_arrival_city_port_xpath").getText());
			values.put("flightArrivalTime", 			getElement("label_vacation_flight_arrival_time_xpath").getText());
			values.put("flightPaxCount", 				getElement("label_vacation_flight_pax_count_xpath").getText());
			values.put("flightStatus", 					getElement("label_vacation_flight_status_xpath").getText());
			values.put("flightTicketIssued", 			getElement("label_vacation_flight_ticket_issued_xpath").getText());
			values.put("flightTicketingDeadline", 		getElement("label_vacation_flight_ticketing_daedline_xpath").getText());
			values.put("flightCancellationDeadline", 	getElement("label_vacation_flight_cancellation_deadline_xpath").getText());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	
	public HashMap<String, String> getHotelLabels()
	{
		HashMap<String, String> labels = new HashMap<>();
		try {
			try {
				System.out.println(labels.put("country", 				getElement("label_country_label_xpath").getText()));
			} catch (Exception e) {
				// TODO: handle exception
			}
			labels.put("country", 				getElement("label_hotel_country_label_xpath").getText().trim());
			labels.put("city", 					getElement("label_hotel_city_name_label_xpath").getText().trim());
			labels.put("supplier", 				getElement("label_hotel_supplier_label_xpath").getText().trim());
			labels.put("hotelName", 			getElement("label_hotel_name_label_xpath").getText().trim());
			labels.put("arrivalDeparture", 		getElement("label_hotel_arrival_departure_label_xpath").getText().trim());
			labels.put("totalRooms", 			getElement("label_hotel_total_rooms_label_xpath").getText().trim());
			labels.put("status", 				getElement("label_hotel_Status_label_xpath").getText().trim());
			labels.put("sectorBookingValue", 	getElement("label_hotel_sector_booking_label_xpath").getText().replace("\n", "").trim());
			labels.put("shoppingCartCode", 		getElement("label_hotel_shopping_cart_code_xpath").getText().trim());
			labels.put("shoppingCart", 			getElement("label_hotel_shopping_cart_xpath").getText().trim());
			labels.put("dynamicPackageCode", 	getElement("label_hotel_dynamic_package_code_xpath").getText().trim());
			labels.put("dynamicPackage", 		getElement("label_hotel_dynamic_package_xpath").getText().trim());
			labels.put("fpCode", 				getElement("label_hotel_fp_code_xpath").getText().trim());
			labels.put("fp", 					getElement("label_hotel_fp_xpath").getText().trim());
			labels.put("dmcCode", 				getElement("label_hotel_dmc_code_xpath").getText().trim());
			labels.put("dmc", 					getElement("label_hotel_dmc_xpath").getText().trim());
			labels.put("invoiceIssued", 		getElement("label_hotel_invoice_issued_xpath").getText().trim());
			labels.put("paymentReceived", 		getElement("label_hotel_payment_recieved_xpath").getText().trim());
			labels.put("voucherIssued", 		getElement("label_hotel_voucher_issued_xpath").getText().trim());
			labels.put("ticketIssued", 			getElement("label_hotel_ticket_issued_xpath").getText().trim());
			labels.put("emdIssued",	 			getElement("label_hotel_emd_issued_xpath").getText().trim());
			labels.put("sectorFailed", 			getElement("label_hotel_sector_failed_xpath").getText().trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getHotelValues()
	{
		HashMap<String, String> labels = new HashMap<>();
		try {
			labels.put("country", 				getElement("label_hotel_country_xpath").getText());
			labels.put("city", 					getElement("label_hotel_city_xpath").getText());
			labels.put("supplier", 				getElement("label_hotel_supplier_xpath").getText());
			labels.put("hotelName", 			getElement("label_hotel_hotel_name_xpath").getText());
			labels.put("ariivalDeparture", 		getElement("label_hotel_arrival_departure_xpath").getText());
			labels.put("totalRooms", 			getElement("label_hotel_total_rooms_xpath").getText());
			labels.put("status", 				getElement("label_hotel_status_xpath").getText());
			labels.put("bookingValue", 			getElement("label_hotel_sector_booking_value_xpath").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public void closeHotelMoreInfo() throws Exception
	{
		System.out.println(getElement("button_hotel_close_xpath").getRef());
		try {
			getElement("button_hotel_close_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
}
