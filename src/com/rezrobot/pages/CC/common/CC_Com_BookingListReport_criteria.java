package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;

public class CC_Com_BookingListReport_criteria extends PageObject{

	public HashMap<String, Boolean> getLabelAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> labels = new HashMap<>();
		try {
			
			labels.put("partner", 							getElementVisibility("label_partner_label_id"));
			labels.put("partner", 							getElementVisibility("label_partner_label_id"));
			labels.put("partnerAll", 						getElementVisibility("label_partner_all_label_id"));
			labels.put("partnerDc", 						getElementVisibility("label_partner_dc_label_id"));
			labels.put("partnerb2b", 						getElementVisibility("label_partner_b2b_label_id"));
			labels.put("partnerAffiliate", 					getElementVisibility("label_partner_affiliate_label_id"));
			labels.put("posLocation", 						getElementVisibility("label_pos_location_name_label_xpath"));
			labels.put("partnerBookingNo", 					getElementVisibility("label_booking_ref_no_label_xpath"));
			labels.put("consultantName", 					getElementVisibility("label_consultant_name_label_xpath"));
			labels.put("productType", 						getElementVisibility("label_product_type_label_id"));
			labels.put("productTypeAll", 					getElementVisibility("label_product_type_all_label_id"));
			labels.put("productTypeActivities", 			getElementVisibility("label_product_type_activities_label_id"));
			labels.put("productTypeHotels", 				getElementVisibility("label_product_type_hotels_label_id"));
			labels.put("productTypeAir", 					getElementVisibility("label_product_type_air_label_id"));
			labels.put("productTypeCar", 					getElementVisibility("label_product_type_car_label_id"));
			labels.put("productTypePackaging", 				getElementVisibility("label_product_type_packaging_label_id"));
			labels.put("firstAndLastName", 					getElementVisibility("label_first_and_last_name_label_xpath"));
			labels.put("bookingStatus", 					getElementVisibility("label_booking_status_label_id"));
			labels.put("bookingStatusAll", 					getElementVisibility("label_booking_status_all_label_id"));
			labels.put("bookingStatusConfirmed", 			getElementVisibility("label_booking_status_confirmed_label_id"));
			labels.put("bookingStatusOnReq", 				getElementVisibility("label_booking_status_onReq_label_id"));
			labels.put("bookingStatusCancelled", 			getElementVisibility("label_booking_status_cancelled_label_id"));
			labels.put("bookingStatusModified", 			getElementVisibility("label_booking_status_modified_label_id"));
			labels.put("bookingStatusWithFailedSector", 	getElementVisibility("label_booking_status_with_failed_sector_label_id"));
			labels.put("dateFilter", 						getElementVisibility("label_date_filter_label_xpath"));
			labels.put("reportFrom", 						getElementVisibility("label_report_from_label_xpath"));
			labels.put("reportTo", 							getElementVisibility("label_report_to_label_xpath"));
			labels.put("datesBasedOn", 						getElementVisibility("label_dates_based_on_label_xpath"));
			labels.put("datesBasedOnReservation", 			getElementVisibility("label_dates_based_on_reservation_label_xpath"));
			labels.put("datesBasedOnCancellation", 			getElementVisibility("label_dates_based_on_cancellation_label_xpath"));
			labels.put("datesBasedOnArrival", 				getElementVisibility("label_dates_based_on_arrival_label_xpath"));
			labels.put("datesBasedOnDeparture", 			getElementVisibility("label_dates_based_on_departure_label_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getLabels()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, String> labels = new HashMap<>();
		try {
			labels.put("partner", 							getElement("label_partner_label_id").getText());
			labels.put("partnerAll", 						getElement("label_partner_all_label_id").getText());
			labels.put("partnerDc", 						getElement("label_partner_dc_label_id").getText());
			labels.put("partnerb2b", 						getElement("label_partner_b2b_label_id").getText());
			labels.put("partnerAffiliate", 					getElement("label_partner_affiliate_label_id").getText());
			labels.put("posLocation", 						getElement("label_pos_location_name_label_xpath").getText());
			labels.put("partnerBookingNo", 					getElement("label_booking_ref_no_label_xpath").getText());
			labels.put("consultantName", 					getElement("label_consultant_name_label_xpath").getText());
			labels.put("productType", 						getElement("label_product_type_label_id").getText());
			labels.put("productTypeAll", 					getElement("label_product_type_all_label_id").getText());
			labels.put("productTypeActivities", 			getElement("label_product_type_activities_label_id").getText());
			labels.put("productTypeHotels", 				getElement("label_product_type_hotels_label_id").getText());
			labels.put("productTypeAir", 					getElement("label_product_type_air_label_id").getText());
			labels.put("productTypeCar", 					getElement("label_product_type_car_label_id").getText());
			labels.put("productTypePackaging", 				getElement("label_product_type_packaging_label_id").getText());
			labels.put("firstAndLastName", 					getElement("label_first_and_last_name_label_xpath").getText());
			labels.put("bookingStatus", 					getElement("label_booking_status_label_id").getText());
			labels.put("bookingStatusAll", 					getElement("label_booking_status_all_label_id").getText());
			labels.put("bookingStatusConfirmed", 			getElement("label_booking_status_confirmed_label_id").getText());
			labels.put("bookingStatusOnReq", 				getElement("label_booking_status_onReq_label_id").getText());
			labels.put("bookingStatusCancelled", 			getElement("label_booking_status_cancelled_label_id").getText());
			labels.put("bookingStatusModified", 			getElement("label_booking_status_modified_label_id").getText());
			labels.put("bookingStatusWithFailedSector", 	getElement("label_booking_status_with_failed_sector_label_id").getText());
			labels.put("dateFilter", 						getElement("label_date_filter_label_xpath").getText());
			labels.put("reportFrom", 						getElement("label_report_from_label_xpath").getText());
			labels.put("reportTo", 							getElement("label_report_to_label_xpath").getText());
			labels.put("datesBasedOn", 						getElement("label_dates_based_on_label_xpath").getText());
			labels.put("datesBasedOnReservation", 			getElement("label_dates_based_on_reservation_label_xpath").getText());
			labels.put("datesBasedOnCancellation", 			getElement("label_dates_based_on_cancellation_label_xpath").getText());
			labels.put("datesBasedOnArrival", 				getElement("label_dates_based_on_arrival_label_xpath").getText());
			labels.put("datesBasedOnDeparture", 			getElement("label_dates_based_on_departure_label_xpath").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public void enterCriteria(String resNumer) throws Exception
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		getElement("inputfield_booking_ref_number_id").clear();
		getElement("inputfield_booking_ref_number_id").setText(resNumer);
		getElement("button_view_xpath").click();
		
		
	}
	
}
