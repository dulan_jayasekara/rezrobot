package com.rezrobot.pages.CC.common;

import java.util.HashMap;
import java.util.Map;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;

public class CC_Com_ThirdPartySupplierPayable_Report extends PageObject{

	public HashMap<String, String> getValues(HotelConfirmationPageDetails confirmation) throws Exception
	{
		HashMap<String, String> values = new HashMap<String, String>();
		try {
			String[] pageCount = getElement("label_page_count_xpath").getText().split("of");
			int flag = 17;
			outerloop : for(int pageNumber = 0 ; pageNumber < Integer.parseInt(pageCount[1].trim()) ; pageNumber++)
			{
				for(int i = 1 ; i <= 20 ; i++)
				{
					if(confirmation.getReservationNumber().equals(getElement("label_booking_number_xpath").changeRefAndGetElement(getElement("label_booking_number_xpath").getRef().replace("replace", String.valueOf(i))).getText()))
					{
						values.put("bookingNumber", 				getElement("label_booking_number_xpath").changeRefAndGetElement(getElement("label_booking_number_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("documentNumber", 				getElement("label_document_number_xpath").changeRefAndGetElement(getElement("label_document_number_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("bookingDate",	 				getElement("label_booking_date_xpath").changeRefAndGetElement(getElement("label_booking_date_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("bookingChannel", 				getElement("label_booking_channel_xpath").changeRefAndGetElement(getElement("label_booking_channel_xpath").getRef().replace("replace", String.valueOf(flag))).getText());
						values.put("productType", 					getElement("label_product_type_xpath").changeRefAndGetElement(getElement("label_product_type_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("productName", 					getElement("label_product_name_xpath").changeRefAndGetElement(getElement("label_product_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("serviceElementDate", 			getElement("label_serviceElement_date_xpath").changeRefAndGetElement(getElement("label_serviceElement_date_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("supplierName", 					getElement("label_supplier_name_xpath").changeRefAndGetElement(getElement("label_supplier_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("supplierConfirmationNumber", 	getElement("label_sup_confirmation_number_xpath").changeRefAndGetElement(getElement("label_sup_confirmation_number_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("bookingStatus", 				getElement("label_booking_status_xpath").changeRefAndGetElement(getElement("label_booking_status_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("guestName", 					getElement("label_guest_name_xpath").changeRefAndGetElement(getElement("label_guest_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("portalCurrency", 				getElement("label_portal_currency_xpath").getText());
						values.put("portalPayableAmount", 			getElement("label_portal_payable_amount_xpath").changeRefAndGetElement(getElement("label_portal_payable_amount_xpath").getRef().replace("replace", String.valueOf(i))).getText().replace(",", ""));
						values.put("portalTotalPaid", 				getElement("label_portal_total_paid_xpath").changeRefAndGetElement(getElement("label_portal_total_paid_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("portalBalanceDue", 				getElement("label_portal_balance_due_xpath").changeRefAndGetElement(getElement("label_portal_balance_due_xpath").getRef().replace("replace", String.valueOf(i))).getText().replace(",", ""));
						values.put("supplierCurrency", 				getElement("label_supplier_currency_xpath").changeRefAndGetElement(getElement("label_supplier_currency_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("supplierPayableAmount", 		getElement("label_sup_payable_amount_xpath").changeRefAndGetElement(getElement("label_sup_payable_amount_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("supplierTotalPaid", 			getElement("label_sup_total_paid_xpath").changeRefAndGetElement(getElement("label_sup_total_paid_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						values.put("supplierBalanceDue", 			getElement("label_sup_balance_due_xpath").changeRefAndGetElement(getElement("label_sup_balance_due_xpath").getRef().replace("replace", String.valueOf(i))).getText());
						break outerloop;
					}
					flag = flag + 7;
				}
				getElement("button_next_page_xpath").click();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
}
