package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;

public class CC_Com_VoucherMail_Hotel extends PageObject{

	public HashMap<String, String> getValues() throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
			getElement("radiobutton_voucher_id").click();
			switchToFrame("frame_mailFrame_id");
			values.put("portalDetails", 				getElement("label_portal_details_xpath").getText());
			values.put("leadPassengerName", 			getElement("label_lead_passenger_xpath").getText());
			values.put("vouvherNumber", 				getElement("label_voucher_number_xpath").getText());
			values.put("reservationNumber", 			getElement("label_reservation_number_xpath").getText());
			values.put("leadPassengerAddress", 			getElement("label_lead_passenger_address_xpath").getText());
			values.put("leadPassengerCountry", 			getElement("label_lead_passenger_country_xpath").getText());
			values.put("hotelName", 					getElement("label_hotel_name_xpath").getText().replace(":", "").trim());
			values.put("city", 							getElement("label_city_xpath").getText().replace(":", "").trim());
			values.put("telephone", 					getElement("label_telephone_xpath").getText().replace(":", "").trim());
			values.put("chckinDate", 					getElement("label_checkin_xpath").getText().replace(":", "").trim());
			values.put("checkinTime", 					getElement("label_checkin_time_xpath").getText().replace(":", "").trim());
			values.put("estimatedArrivalTime",	 		getElement("label_estimated_arrival_time_xpath").getText().replace(":", "").trim());
			values.put("roomType", 						getElement("label_roomtype_xpath").getText().replace(":", "").trim());
			values.put("ratePlan", 						getElement("label_rateplan_xpath").getText().replace(":", "").trim().replace(":", "").trim());
			values.put("adultCount", 					getElement("label_adult_count_xpath").getText().replace(":", "").trim());
			values.put("starCategory", 					getElement("label_star_category_xpath").getText().replace(":", "").trim());
			values.put("address", 						getElement("label_address_xpath").getText().replace(":", "").trim());
			values.put("status", 						getElement("label_status_xpath").getText().replace(":", "").trim());
			values.put("checkoutDate", 					getElement("label_checkout_xpath").getText().replace(":", "").trim());
			values.put("checkoutTime", 					getElement("label_checkout_time_xpath").getText().replace(":", "").trim());
			values.put("bedType", 						getElement("label_bedtype_xpath").getText().replace(":", "").trim());
			values.put("nights", 						getElement("label_nights_xpath").getText().replace(":", "").trim());
			values.put("roomCount", 					getElement("label_room_count_xpath").getText().replace(":", "").trim());
			values.put("childrenCount", 				getElement("label_children_count_xpath").getText().replace(":", "").trim());
			int cnt = 1;
			int cnt2 = 0;
			for(int i = 24 ; i > 0 ; i++)
			{
				cnt2 = i;
				try {
					values.put("roomnumber".concat(String.valueOf(cnt)), 				getElement("label_room_number_xpath").changeRefAndGetElement(getElement("label_room_number_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("passengerName".concat(String.valueOf(cnt)), 			getElement("label_passenger_name_xpath").changeRefAndGetElement(getElement("label_passenger_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("passengerType".concat(String.valueOf(cnt)), 			getElement("label_passenger_type_xpath").changeRefAndGetElement(getElement("label_passenger_type_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("passengerAge".concat(String.valueOf(cnt)), 				getElement("label_passenger_age_xpath").changeRefAndGetElement(getElement("label_passenger_age_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("smoking".concat(String.valueOf(cnt)), 					getElement("label_smoking_xpath").changeRefAndGetElement(getElement("label_smoking_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("handicap".concat(String.valueOf(cnt)), 					getElement("label_handicap_xpath").changeRefAndGetElement(getElement("label_handicap_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("wheelchair".concat(String.valueOf(cnt)), 				getElement("label_wheelchair_xpath").changeRefAndGetElement(getElement("label_wheelchair_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				cnt++;
			}
			values.put("supplierNotes", 					getElement("label_supplier_notes_xpath").changeRefAndGetElement(getElement("label_supplier_notes_xpath").getRef().replace("replace", String.valueOf(cnt2 + 4))).getText());
			values.put("customerNotes", 					getElement("label_customer_notes_xpath").changeRefAndGetElement(getElement("label_customer_notes_xpath").getRef().replace("replace", String.valueOf(cnt2 + 6))).getText());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Voucher mail - Values extraction error - " + e);
		}
		
		
		return values;
	}
}
