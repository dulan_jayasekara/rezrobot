package com.rezrobot.pages.CC.common;

import java.util.HashMap;

import com.rezrobot.core.PageObject;

public class CC_Com_BookingList_Report extends PageObject{
	
	public HashMap<String, Boolean> getHotelLabelsAvailability()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_report_frame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		HashMap<String, Boolean> label = new HashMap<>();
		try {
			label.put("number", 				getElementVisibility("label_hotel_number_label_xpath"));
			label.put("reservations", 			getElementVisibility("label_hotel_reservation_label_xpath"));
			label.put("customer", 				getElementVisibility("label_hotel_customer_xapth"));
			label.put("leadGuest", 				getElementVisibility("label_hotel_lead_guest_label_xpath"));
			label.put("dateOf", 				getElementVisibility("label_hotel_date_of_label_xpath"));
			label.put("date", 					getElementVisibility("label_hotel_date_label_xpath"));
			label.put("time", 					getElementVisibility("label_hotel_time_label_xpath"));
			label.put("reservedBy", 			getElementVisibility("label_hotel_reserved_by_label_xpath"));
			label.put("paymentType", 			getElementVisibility("label_hotel_payment_type_label_xpath"));
			label.put("profitMarkUpType", 		getElementVisibility("label_hotel_profit_mark_up_type_label_xpath"));
			label.put("customerName", 			getElementVisibility("label_hotel_customer_name_label_xpath"));
			label.put("leadGuestName", 			getElementVisibility("label_hotel_lead_guest_name_label_xpath"));
			label.put("firstElement", 			getElementVisibility("label_hotel_first_element_label_xpath"));
			label.put("cancellationDeadline", 	getElementVisibility("label_hotel_cancellation_deadline_label_xpath"));
			label.put("cityCountryName", 		getElementVisibility("label_hotel_city_name_country_label_xpath"));
			label.put("product", 				getElementVisibility("label_hotel_product_label_xpath"));
			label.put("verified", 				getElementVisibility("label_hotel_verified_label_xpath"));
			label.put("status", 				getElementVisibility("label_hotel_status_label_xpath"));
			label.put("notifications", 			getElementVisibility("label_hotel_notifications_label_xpath"));
			label.put("edit", 					getElementVisibility("label_hotel_edit_label_xpath"));
			label.put("shoppingCartCode", 		getElementVisibility("label_hotel_shopping_cart_code_xpath"));
			label.put("shoppingCart", 			getElementVisibility("label_hotel_shopping_cart_xpath"));
			label.put("dynamicPackageCode", 	getElementVisibility("label_hotel_dynamic_package_code_xpath"));
			label.put("dynamicPackage", 		getElementVisibility("label_hotel_dynamic_package_xpath"));
			label.put("fixedPackageCode", 		getElementVisibility("label_hotel_fixed_packages_code_xpath"));
			label.put("fixedPackages", 			getElementVisibility("label_hotel_fixed_packages_xpath"));
			label.put("dmcCode", 				getElementVisibility("label_hotel_dmc_code_xpath"));
			label.put("dmc", 					getElementVisibility("label_hotel_dmc_xpath"));
			label.put("invoice", 				getElementVisibility("image_hotel_invoice_xpath"));
			label.put("invoiceIssued", 			getElementVisibility("label_hotel_invoice_issued_xpath"));
			label.put("payment", 				getElementVisibility("image_hotel_payment_xpath"));
			label.put("paymentReceived", 		getElementVisibility("label_hotel_payment_received_xpath"));
			label.put("voucher", 				getElementVisibility("image_hotel_voucher_xpath"));
			label.put("voucherIssued", 			getElementVisibility("label_hotel_vouvher_issued_xpath"));
			label.put("ticket", 				getElementVisibility("image_hotel_ticket_xpath"));
			label.put("ticketIssued", 			getElementVisibility("label_hotel_ticket_issued_xpath"));
			label.put("emd",	 				getElementVisibility("image_hotel_emd_xpath"));
			label.put("emdIssued", 				getElementVisibility("label_hotel_emd_issued_xpath"));
			label.put("failedSectorImage", 		getElementVisibility("image_hotel_failed_sector_xpath"));
			label.put("failedSector", 			getElementVisibility("label_hotel_failed_sector_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return label;
	}
	
	public HashMap<String, String> getHotelLabels() throws Exception
	{
		HashMap<String, String> label = new HashMap<>();
		try {
			label.put("number", 				getElement("label_hotel_number_label_xpath").getText());
			label.put("reservations", 			getElement("label_hotel_reservation_label_xpath").getText());
			label.put("customer", 				getElement("label_hotel_customer_xapth").getText());
			label.put("leadGuest", 				getElement("label_hotel_lead_guest_label_xpath").getText());
			label.put("dateOf", 				getElement("label_hotel_date_of_label_xpath").getText());
			label.put("date", 					getElement("label_hotel_date_label_xpath").getText());
			label.put("time", 					getElement("label_hotel_time_label_xpath").getText());
			label.put("reservedBy", 			getElement("label_hotel_reserved_by_label_xpath").getText());
			label.put("paymentType", 			getElement("label_hotel_payment_type_label_xpath").getText());
			label.put("profitMarkUpType", 		getElement("label_hotel_profit_mark_up_type_label_xpath").getText());
			label.put("customerName", 			getElement("label_hotel_customer_name_label_xpath").getText());
			label.put("leadGuestName", 			getElement("label_hotel_lead_guest_name_label_xpath").getText());
			label.put("firstElement", 			getElement("label_hotel_first_element_label_xpath").getText());
			label.put("cancellationDeadline", 	getElement("label_hotel_cancellation_deadline_label_xpath").getText());
			label.put("cityCountryName", 		getElement("label_hotel_city_name_country_label_xpath").getText());
			label.put("product", 				getElement("label_hotel_product_label_xpath").getText());
			label.put("verified", 				getElement("label_hotel_verified_label_xpath").getText());
			label.put("status", 				getElement("label_hotel_status_label_xpath").getText());
			label.put("notifications", 			getElement("label_hotel_notifications_label_xpath").getText());
			label.put("edit", 					getElement("label_hotel_edit_label_xpath").getText());
			label.put("shoppingCartCode", 		getElement("label_hotel_shopping_cart_code_xpath").getText());
			label.put("shoppingCart", 			getElement("label_hotel_shopping_cart_xpath").getText());
			label.put("dynamicPackageCode", 	getElement("label_hotel_dynamic_package_code_xpath").getText());
			label.put("dynamicPackage", 		getElement("label_hotel_dynamic_package_xpath").getText());
			label.put("fixedPackageCode", 		getElement("label_hotel_fixed_packages_code_xpath").getText());
			label.put("fixedPackages", 			getElement("label_hotel_fixed_packages_xpath").getText());
			label.put("dmcCode", 				getElement("label_hotel_dmc_code_xpath").getText());
			label.put("dmc", 					getElement("label_hotel_dmc_xpath").getText());
			//label.put("invoice", 				getElement("image_invoice_xpath").getText());
			label.put("invoiceIssued", 			getElement("label_hotel_invoice_issued_xpath").getText());
			//label.put("payment", 				getElement("image_payment_xpath").getText());
			label.put("paymentReceived", 		getElement("label_hotel_payment_received_xpath").getText());
			//label.put("voucher", 				getElement("image_voucher_xpath").getText());
			label.put("voucherIssued", 			getElement("label_hotel_vouvher_issued_xpath").getText());
			//label.put("ticket", 				getElement("image_ticket_xpath").getText());
			label.put("ticketIssued", 			getElement("label_hotel_ticket_issued_xpath").getText());
			//label.put("emd",	 				getElement("image_emd_xpath").getText());
			label.put("emdIssued", 				getElement("label_hotel_emd_issued_xpath").getText());
			//label.put("failedSectorImage", 		getElement("image_failed_sector_xpath").getText());
		//	System.out.println(getElement("label_hotel_failed_sector_xpath").getText());
			label.put("failedSector", 			getElement("label_hotel_failed_sector_xpath").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return label;
		
	}

	public HashMap<String, String> getHotelValues() throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("reservationNumber", 	getElement("label_hotel_reservation_number_xpath").getText());
			values.put("reservationDate", 		getElement("label_hotel_reservation_date_xpath").getText());
			values.put("reservationTime", 		getElement("label_hotel_reservation_time_xpath").getText());
			values.put("reservedBy", 			getElement("label_hotel_reserved_by_xpath").getText());
			values.put("paymentType", 			getElement("label_hotel_payment_type_xpath").getText());
			values.put("profitMarkupType", 		getElement("label_hotel_profit_markup_type_xpath").getText());
			values.put("customerName", 			getElement("label_hotel_customer_name_xpath").getText());
			values.put("leadGuestName", 		getElement("label_hotel_lead_guest_name_xpath").getText());
			values.put("firstElement", 			getElement("label_hotel_first_element_xpath").getText());
			values.put("cancellationDeadline", 	getElement("label_hotel_cancellation_deadline_xpath").getText());
			values.put("cityCountry", 			getElement("label_hotel_city_country_xpath").getText());
			values.put("verified", 				getElement("label_hotel_verified_xpath").getText());
			values.put("status", 				getElement("label_hotel_status_xpath").getText());
			
			if(getElementVisibility("image_hotel_invoice_yes_id"))
			{
				values.put("invoice", 				"Invoice issued");
			}
			else if(getElementVisibility("image_hotel_invoice_no_id"))
			{
				values.put("invoice", 				"Invoice not issued");
			}
			else
			{
				values.put("invoice", 				"Invoice status unavailable");
			}
			
			if(getElementVisibility("image_hotel_payment_yes_id"))
			{
				values.put("payment", 				"Payment done");
			}
			else if(getElementVisibility("image_hotel_payment_no_id"))
			{
				values.put("payment", 				"Payment not done");
			}
			else
			{
				values.put("payment", 				"Payment status unavailable");
			}
			
			if(getElementVisibility("image_hotel_voucher_yes_id"))
			{
				values.put("voucher", 				"Voucher issued");
			}
			else if(getElementVisibility("image_hotel_voucher_no_id"))
			{
				values.put("voucher", 				"Voucher not issued");
			}
			else
			{
				values.put("voucher", 				"Voucher status unavailable");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			getElement("button_hotel_view_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("booking list - live - button click error --- " +  e);
		}
		
		return values;
		
	}
	
	public HashMap<String, String> getActivityValues() throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		
		try {
			values.put("reservationNumber", 	getElement("label_hotel_reservation_number_xpath").getText());
			values.put("reservationDate", 		getElement("label_hotel_reservation_date_xpath").getText());
			values.put("reservationTime", 		getElement("label_hotel_reservation_time_xpath").getText());
			values.put("reservedBy", 			getElement("label_hotel_reserved_by_xpath").getText());
			values.put("paymentType", 			getElement("label_hotel_payment_type_xpath").getText());
			values.put("profitMarkupType", 		getElement("label_hotel_profit_markup_type_xpath").getText());
			values.put("customerName", 			getElement("label_hotel_customer_name_xpath").getText());
			values.put("leadGuestName", 		getElement("label_hotel_lead_guest_name_xpath").getText());
			values.put("firstElement", 			getElement("label_hotel_first_element_xpath").getText());
			values.put("cancellationDeadline", 	getElement("label_hotel_cancellation_deadline_xpath").getText());
			values.put("cityCountry", 			getElement("label_hotel_city_country_xpath").getText());
			values.put("verified", 				getElement("label_hotel_verified_xpath").getText());
			values.put("status", 				getElement("label_hotel_status_xpath").getText());
			
			if(getElementVisibility("image_hotel_invoice_yes_id"))
			{
				values.put("invoice", 				"Invoice issued");
			}
			else if(getElementVisibility("image_hotel_invoice_no_id"))
			{
				values.put("invoice", 				"Invoice not issued");
			}
			else
			{
				values.put("invoice", 				"Invoice status unavailable");
			}
			
			if(getElementVisibility("image_hotel_payment_yes_id"))
			{
				values.put("payment", 				"Payment done");
			}
			else if(getElementVisibility("image_hotel_payment_no_id"))
			{
				values.put("payment", 				"Payment not done");
			}
			else
			{
				values.put("payment", 				"Payment status unavailable");
			}
			
			if(getElementVisibility("image_hotel_voucher_yes_id"))
			{
				values.put("voucher", 				"Voucher issued");
			}
			else if(getElementVisibility("image_hotel_voucher_no_id"))
			{
				values.put("voucher", 				"Voucher not issued");
			}
			else
			{
				values.put("voucher", 				"Voucher status unavailable");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return values;
		
	}
	
	public void clickHotelReservationNumber() throws Exception
	{
		switchToDefaultFrame();
		switchToFrame("frame_report_frame_id");
		getElement("button_hotel_reservation_number_xpath").click();
	}

	public CC_Com_BookingListReport_BookingCard clickActivityReservationNumber() throws Exception{
		
		switchToDefaultFrame();
		switchToFrame("frame_report_frame_id");
		getElement("button_ActivityViewCard_xpath").click();
		CC_Com_BookingListReport_BookingCard bookingList_BookingCard = new CC_Com_BookingListReport_BookingCard();
		return bookingList_BookingCard;
	}

	
}
