package com.rezrobot.pages.CC.common;

import java.util.HashMap;
import org.openqa.selenium.By;
import com.rezrobot.core.PageObject;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.PropertySingleton;

public class CC_Com_Admin_ConfigurationPage extends PageObject {

	public CC_Com_Admin_ConfigurationPage() throws Exception {
		getElement("combobox_Module_id").checkElementVisisbilityWithException(
				120, "CC_Com_Admin_ConfigurationPage Not Loaded");
	}

	public HashMap<String, String> getConfigProperties() throws Exception {
		HashMap<String, String> ConfigMap = new HashMap<>();
		selectModule();
		getElement("label_config1_id").checkElementVisisbilityWithException(20,
				"Configurations are not available");

		DriverFactory.getInstance().manageTimeOut(1);
		for (int i = 1; i < 200; i++) {
			String Key = "";
			String value = "";

			try {
				Key = DriverFactory
						.getInstance()
						.getDriver()
						.findElement(
								By.id("user_interface_config_" + i
										+ "_label_text")).getText();
				value = DriverFactory.getInstance().getDriver()
						.findElement(By.id("user_interface_config_" + i))
						.getAttribute("value");
			} catch (Exception e) {
				// TODO: handle exception
			}
		//	System.out.println(Key + " - " + value);
			ConfigMap.put(Key, value);
		}
		DriverFactory.getInstance().manageTimeOut(
				Integer.parseInt(PropertySingleton.getInstance().getProperty(
						"Driver.Timeout")));
		return ConfigMap;
	}

	
	public void loadConfigProperties() throws Exception {
		
		PropertySingleton.getInstance().loadProperties(getConfigProperties());
	}


	public void selectModule() throws Exception {
		((ComboBox) getElement("combobox_Module_id")).selectOptionByText("ALL");
	}
}
