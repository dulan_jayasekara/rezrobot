package com.rezrobot.pages.CC.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.rezrobot.core.PageObject;
import com.rezrobot.flight_circuitry.Initiator;
import com.rezrobot.pojo.CheckBox;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExcelReader;

public class CC_Com_Nego_Code_Setup extends PageObject{

	String officeID = "";
	String all = "";
	String availability = "";
	String price = "";
	String reservation = "";
	String eticket = "";
	String pnrRead = "";
	String pnrUpdate = "";
	String fareRules = "";
	String cancellation = "";
	String add = "";
	String remove = "";
	String edit = "";
	public String getOfficeID() {
		return officeID;
	}
	public void setOfficeID(String officeID) {
		this.officeID = officeID;
	}
	public String getAll() {
		return all;
	}
	public void setAll(String all) {
		this.all = all;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getReservation() {
		return reservation;
	}
	public void setReservation(String reservation) {
		this.reservation = reservation;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public String getPnrRead() {
		return pnrRead;
	}
	public void setPnrRead(String pnrRead) {
		this.pnrRead = pnrRead;
	}
	public String getPnrUpdate() {
		return pnrUpdate;
	}
	public void setPnrUpdate(String pnrUpdate) {
		this.pnrUpdate = pnrUpdate;
	}
	public String getFareRules() {
		return fareRules;
	}
	public void setFareRules(String fareRules) {
		this.fareRules = fareRules;
	}
	public String getCancellation() {
		return cancellation;
	}
	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	public String getRemove() {
		return remove;
	}
	public void setRemove(String remove) {
		this.remove = remove;
	}
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	
	
	
	public void setup(String url) throws Exception
	{
		DriverFactory.getInstance().getDriver().get(url);
		Initiator readExcel = new Initiator();
		ArrayList<CC_Com_Nego_Code_Setup> negoList = readExcel.getNegoDetails();
		String negoStatus 	= getElement("label_nego_status_id").getText();
		String negoCode 	= getElement("label_nego_code_id").getText();
		
		for(int i = 0 ; i < negoList.size() ; i++)
		{
			getElement("inputfield_officeid_id").setText(negoList.get(i).getOfficeID());
			getElement("button_officeid_lkup_id").click();
			switchToDefaultFrame();
			switchToFrame("frame_lookup_id");
			getElement("label_first_record_xpath").click();
			switchToDefaultFrame();
			if(getElementVisibility("label_officeid_classname"))
			{
				if(negoList.get(i).getRemove().equals("yes") && negoList.get(i).getEdit().equals("yes"))
				{
					System.out.println(negoList.get(i).getOfficeID().concat(" Invalid action"));
				}
				else if(negoList.get(i).getRemove().equals("yes") && negoList.get(i).getEdit().equals("no"))
				{
					getElement("button_removeRecord_classname").click();
				}
				else if(negoList.get(i).getRemove().equals("no") && negoList.get(i).getEdit().equals("yes"))
				{
					getElement("button_editRecord_classname").click();
					if(isCheckBoxSelected("checkbox_all_id"))
					{
						if(negoList.get(i).getAll().equals("no"))
						{
							getElement("checkbox_all_id").click();
						}
					}
					else
					{
						if(isCheckBoxSelected("checkbox_availability_id"))
						{
							if(negoList.get(i).getAvailability().equals("no"))
							{
								getElement("checkbox_availability_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getAvailability().equals("yes"))
							{
								getElement("checkbox_availability_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_price_id"))
						{
							if(negoList.get(i).getPrice().equals("no"))
							{
								getElement("checkbox_price_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getPrice().equals("yes"))
							{
								getElement("checkbox_price_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_reservation_id"))
						{
							if(negoList.get(i).getPrice().equals("no"))
							{
								getElement("checkbox_reservation_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getPrice().equals("yes"))
							{
								getElement("checkbox_reservation_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_eticket_id"))
						{
							if(negoList.get(i).getEticket().equals("no"))
							{
								getElement("checkbox_eticket_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getEticket().equals("yes"))
							{
								getElement("checkbox_eticket_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_pnr_read_id"))
						{
							if(negoList.get(i).getPnrRead().equals("no"))
							{
								getElement("checkbox_pnr_read_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getPnrRead().equals("yes"))
							{
								getElement("checkbox_pnr_read_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_pnr_update_id"))
						{
							if(negoList.get(i).getPnrUpdate().equals("no"))
							{
								getElement("checkbox_pnr_update_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getPnrUpdate().equals("yes"))
							{
								getElement("checkbox_pnr_update_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_fare_rules_id"))
						{
							if(negoList.get(i).getFareRules().equals("no"))
							{
								getElement("checkbox_fare_rules_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getFareRules().equals("yes"))
							{
								getElement("checkbox_fare_rules_id").click();
							}
						}
						
						if(isCheckBoxSelected("checkbox_cancellation_id"))
						{
							if(negoList.get(i).getCancellation().equals("no"))
							{
								getElement("checkbox_cancellation_id").click();
							}
						}
						else
						{
							if(negoList.get(i).getCancellation().equals("yes"))
							{
								getElement("checkbox_cancellation_id").click();
							}
						}
					}
					getElement("button_add_id").click();
				}
			}
			else
			{
				if(negoList.get(i).getAdd().equals("yes"))
				{
					if(negoList.get(i).getAll().equals("yes"))
					{
						getElement("checkbox_all_id").click();
					}
					else
					{
						if(negoList.get(i).getAvailability().equals("yes"))
						{
							getElement("checkbox_availability_id").click();
						}
						if(negoList.get(i).getPrice().equals("yes"))
						{
							getElement("checkbox_price_id").click();
						}
						if(negoList.get(i).getReservation().equals("yes"))
						{
							getElement("checkbox_reservation_id").click();
						}
						if(negoList.get(i).getEticket().equals("yes"))
						{
							getElement("checkbox_eticket_id").click();
						}
						if(negoList.get(i).getPnrRead().equals("yes"))
						{
							getElement("checkbox_pnr_read_id").click();
						}
						if(negoList.get(i).getPnrUpdate().equals("yes"))
						{
							getElement("checkbox_pnr_update_id").click();
						}
						if(negoList.get(i).getFareRules().equals("yes"))
						{
							getElement("checkbox_fare_rules_id").click();
						}
						if(negoList.get(i).getCancellation().equals("yes"))
						{
							getElement("checkbox_cancellation_id").click();
						}
					}
					getElement("button_add_id").click();
				}
			}
		}
	}
	
	public ArrayList<CC_Com_Nego_Code_Setup> getTableDetails() throws Exception
	{
		ArrayList<CC_Com_Nego_Code_Setup> 		negoList 		= 	new ArrayList<CC_Com_Nego_Code_Setup>();
		for(int i = 0 ; i < getElements("label_officeid_classname").size() ; i++)
		{
			CC_Com_Nego_Code_Setup 				nego 			= new CC_Com_Nego_Code_Setup();
			nego.setOfficeID		(getElements("label_officeid_classname").get(i).getText());
			nego.setAvailability	(getElements("label_availability_status_classname").get(i).getText());
			nego.setPrice			(getElements("label_price_status_classname").get(i).getText());
			nego.setReservation		(getElements("label_reservation_status_classname").get(i).getText());
			nego.setEticket			(getElements("label_eticket_status_classname").get(i).getText());
			nego.setPnrRead			(getElements("label_pnrRead_status_classname").get(i).getText());
			nego.setPnrUpdate		(getElements("label_pnrUpdate_status_classname").get(i).getText());
			nego.setFareRules		(getElements("label_fareRules_status_classname").get(i).getText());
			nego.setCancellation	(getElements("label_cancellation_status_classname").get(i).getText());
			negoList.add(nego);
		}
		return negoList;
	}
	
}
