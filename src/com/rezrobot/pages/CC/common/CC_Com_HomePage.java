package com.rezrobot.pages.CC.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.utill.DriverFactory;

public class CC_Com_HomePage extends PageObject {
	
	
	

	public boolean isPageAvailable() {
		// TODO Auto-generated method stub
		return super.isPageAvailable("lnk_Operations_id");
	}

	public CC_Com_SearchCriteria navigateDropdown() throws Exception {
		
		
		WebElement mainMenu=getElement("lnk_ReservationsMenu_id").getWebElement();
		WebElement subMenu=getElement("lnk_CallcenterReservationsMenu_id").getWebElement();
		
		Actions action=new Actions(DriverFactory.getInstance().getDriver());
		action.moveToElement(mainMenu).click(subMenu).build().perform();
		
		CC_Com_SearchCriteria search=new CC_Com_SearchCriteria();
		return search;
		
	}
	
	 public CC_Com_OAH_SalesReport_Criteria getTourSalesReport() throws Exception {
			
			getElementClick("lnk_finance_id");
			WebElement mainMenu=getElement("lnk_reportsandalerts_id").getWebElement();
			WebElement subMenu=getElements("lnk_toursales_id").get(0);
			
			Actions action=new Actions(DriverFactory.getInstance().getDriver());
			action.moveToElement(mainMenu).click(subMenu).build().perform();

			return new CC_Com_OAH_SalesReport_Criteria() ;
			
		}
	
	public void enterHotelSearchCriteria(HotelSearchObject search) throws Exception
	{
		switchToDefaultFrame();
		switchToFrame("frame_live_message_frame_id");
		switchToFrame("frame_bec_container_id");
		getElement("radiobutton_bec_hotels_id").click();
		((ComboBox) getElement("combobox_bec_country_of_residence_id")).selectOptionByText(search.getCountry());
		String[] destination = search.getDestination().split("\\|");
		getElement("inputfield_hidden_destination_id").setJavaScript("$('#hid_H_Loc').val('"+search.getDestination()+"');");
		getElement("inputfieled_bec_checkin_id").setJavaScript("$('#ho_departure').val('"+search.getCheckin()+"');");
		SimpleDateFormat 		sdf 					= new SimpleDateFormat("MM/dd/yyyy");
		Calendar 				c 						= Calendar.getInstance();
		String 					checkout 				= null;
		try {
			c.setTime(sdf.parse(search.getCheckin()));
			//System.out.println(sdf.format(c.getTime()));
			c.add(Calendar.DATE, Integer.parseInt(search.getNights()));
			checkout 						= sdf.format(c.getTime());
			//System.out.println(dt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		getElement("inputfield_bec_checkout_id").setJavaScript("$('#ho_arrival').val('"+checkout+"');");
		((ComboBox) getElement("combobox_bec_nights_id")).selectOptionByText(search.getNights());
		((ComboBox)getElement("combobox_bec_rooms_id")).selectOptionByText(search.getRooms());
		String[] adults = search.getAdults().split("/");
		String[] children = search.getChildren().split("/");
		String[] age = search.getAge().split("/");
		int ageCnt = 0;
		for(int i = 1 ; i <= Integer.parseInt(search.getRooms()) ; i++)
		{
			((ComboBox)getElement("combobox_bec_adult_id").changeRefAndGetElement(getElement("combobox_bec_adult_id").getRef().replace("replace", String.valueOf(i)))).selectOptionByText(adults[i-1]);
			((ComboBox)getElement("combobox_bec_children_id").changeRefAndGetElement(getElement("combobox_bec_children_id").getRef().replace("replace", String.valueOf(i)))).selectOptionByText(children[i-1]);
			for(int j = 1 ; j <= Integer.parseInt(children[i-1]) ; j++)
			{
				((ComboBox)getElement("combobox_bec_age_id").changeRefAndGetElement(getElement("combobox_bec_age_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j)))).selectOptionByText(age[ageCnt]);
				ageCnt++;
			}
		}
		getElement("inputfieled_bec_destination_id").setText(destination[1]);
	}
	
	
	public void performHotelSarch() throws Exception
	{
		getElement("button_bec_search_for_hotels_id").click();
	}
	

}
