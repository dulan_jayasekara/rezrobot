package com.rezrobot.pages.CC.common;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.utill.DriverFactory;

public class CC_Com_Admin_WebSiteDetails extends PageObject{

	public void getWebSiteDetailsPage(String PortalLink)
	{
		DriverFactory.getInstance().getDriver().get(PortalLink.concat("admin/configuration/ConfigurationWebSiteDetailsSetupPage.do"));
	}
	
	public ArrayList<HashMap<String, String>> getDetails() throws Exception
	{
		ArrayList<HashMap<String, String>> valuesList = new ArrayList<HashMap<String, String>>();
		for(int i = 1 ; i > 0 ; i++)
		{
			try {
				HashMap<String, String> values = new HashMap<>();
				values.put("website", 			getElement("label_website_xpath").changeRefAndGetElement(getElement("label_website_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("websiteName", 		getElement("label_website_name_xpath").changeRefAndGetElement(getElement("label_website_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("websiteUrl", 		getElement("label_website_url_xpath").changeRefAndGetElement(getElement("label_website_url_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("email", 			getElement("label_email_xpath").changeRefAndGetElement(getElement("label_email_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("websiteAddress", 	getElement("label_website_address_xpath").changeRefAndGetElement(getElement("label_website_address_xpath").getRef().replace("replace", String.valueOf(i))).getText().replace("\n", ""));
				values.put("telephone", 		getElement("label_telephone_xpath").changeRefAndGetElement(getElement("label_telephone_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("fax", 				getElement("label_fax_xpath").changeRefAndGetElement(getElement("label_fax_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("tAndC", 			getElement("label_t_and_c_file_name_xpath").changeRefAndGetElement(getElement("label_t_and_c_file_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				values.put("mailLogo", 			getElement("label_mail_logo_name_xpath").changeRefAndGetElement(getElement("label_mail_logo_name_xpath").getRef().replace("replace", String.valueOf(i))).getText());
				valuesList.add(values);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
		}
		return valuesList;
	}
	
}
