package com.rezrobot.pages.CC.common;
import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.OAH_SalesAndRefund_Search;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.types.BookingChannelType;
import com.rezrobot.types.ReportBookingChannel;
import com.rezrobot.types.ReportBookingType;
import com.rezrobot.types.ReportCurrencyType;
import com.rezrobot.types.SummarizedBy;
import com.rezrobot.types.TourSalesReportType;
import com.rezrobot.types.UserType;

public class CC_Com_OAH_SalesReport_Criteria extends PageObject {

	public void enterCriteria(String date) throws Exception {
		switchToDefaultFrame();
		switchToFrame("frame_reportFrame_id");
		System.out.println(date);
		String[] reportDate = date.split("-");
		String day = reportDate[0];
		String month = reportDate[1];
		String year = reportDate[2];
		((ComboBox) getElement("combobox_from_day_id")).selectOptionByText(day);
		((ComboBox) getElement("combobox_from_month_id")).selectOptionByText(month);
		((ComboBox) getElement("combobox_from_year_id")).selectOptionByText(year);
		((ComboBox) getElement("combobox_from_day_id")).selectOptionByText(day);
		((ComboBox) getElement("combobox_from_month_id")).selectOptionByText(month);
		((ComboBox) getElement("combobox_from_year_id")).selectOptionByText(year);
		try {
			getElement("button_view_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

	}

	public String[] getDefaultSelectedDates() throws Exception {

		String FromDay = ((ComboBox) getElement("combobox_reportFrom_date_id")).getDefaultSelectedOption();
		String FromMonth = ((ComboBox) getElement("combobox_reportFrom_month_id")).getDefaultSelectedOption();
		String FromYear = ((ComboBox) getElement("combobox_reportFrom_year_id")).getDefaultSelectedOption();
		String TODay = ((ComboBox) getElement("combobox_reportTo_date_id")).getDefaultSelectedOption();
		String TOMonth = ((ComboBox) getElement("combobox_reportTo_month_id")).getDefaultSelectedOption();
		String ToYear = ((ComboBox) getElement("combobox_reportTo_year_id")).getDefaultSelectedOption();

		return new String[] { (FromDay + "-" + FromMonth + "-" + FromYear), (TODay + "-" + TOMonth + "-" + ToYear) };

	}

	public String getReportHeader() throws Exception {
		return getLabelName("label_reportheader_id");
	}

	public TourSalesReportType getDefaultSelectedReport() {
		if (isRadioBoxSelected("radiobutton_reporttype_sales_id"))
			return TourSalesReportType.SALES;
		if (isRadioBoxSelected("radiobutton_reporttype_refund_id"))
			return TourSalesReportType.REFUND;
		else
			return TourSalesReportType.NONE;

	}

	public ReportCurrencyType getDefaultSelectedCurrencyType() throws Exception {
		if (isRadioBoxSelected("radiobutton_currencytype_sell_id"))
			return ReportCurrencyType.SELLINGCURR;
		if (isRadioBoxSelected("radiobutton_currencytype_base_id"))
			return ReportCurrencyType.BASECURR;
		else
			throw new Exception("ReportCurrencyType option not available or nothing selected by default");
	}

	public String getDefaultCountry() {
		try {
			return ((InputFiled) getElement("inputfield_country_id")).getAttribute("value");
		} catch (Exception e) {
			return "N/A";
		}

	}

	public BookingChannelType getDefaultSelectedBookingChannel() throws Exception {
		if (isRadioBoxSelected("radiobutton_channel_all_id"))
			return BookingChannelType.ALL;
		if (isRadioBoxSelected("radiobutton_channel_cc_id"))
			return BookingChannelType.CALLCENTER;
		if (isRadioBoxSelected("radiobutton_channel_web_id"))
			return BookingChannelType.WEB;
		else
			throw new Exception("BookingChannelType option not available or nothing selected by default");
	}

	public String getDefaultCity() {
		try {
			return ((InputFiled) getElement("inputfield_city_id")).getAttribute("value");
		} catch (Exception e) {
			return "N/A";
		}

	}

	public String getDefaultPosLocation() {
		try {
			return ((InputFiled) getElement("inputfield_pos_location_id")).getAttribute("value");
		} catch (Exception e) {
			return "N/A";
		}

	}

	public ReportBookingType getDefaultSelectedBookingType() throws Exception {
		if (isRadioBoxSelected("radiobutton_booking_status_all_id"))
			return ReportBookingType.ALL;
		if (isRadioBoxSelected("radiobutton_booking_status_withflight_id"))
			return ReportBookingType.WITHF;
		if (isRadioBoxSelected("radiobutton_booking_status_withoutf_id"))
			return ReportBookingType.WITHOUTF;
		if (isRadioBoxSelected("radiobutton_booking_status_flightonly_id"))
			return ReportBookingType.FONLY;
		else
			throw new Exception("ReportBookingType option not available");
	}

	public SummarizedBy getDefaultSelectedSummerizeOption() throws Exception {
		if (isRadioBoxSelected("radiobutton_summerize_none_id"))
			return SummarizedBy.NONE;
		if (isRadioBoxSelected("radiobutton_summerize_booking_id"))
			return SummarizedBy.BOOKING;
		if (isRadioBoxSelected("radiobutton_summerize_pos_id"))
			return SummarizedBy.POS;
		if (isRadioBoxSelected("radiobutton_summerize_city_id"))
			return SummarizedBy.CITY;
		if (isRadioBoxSelected("radiobutton_summerize_country_id"))
			return SummarizedBy.COUNTRY;
		else
			throw new Exception("Summerized By option not available");
	}

	public void selectCrieteria(OAH_SalesAndRefund_Search ReportSearch) throws Exception {

		// Switch to report frame
		switchToDefaultFrame();
		switchToFrame("frame_reportFrame_id");

		// Selecting the report type
		if (ReportSearch.getReportType() == TourSalesReportType.SALES)
			getElementClick("radiobutton_reporttype_sales_id");
		else if (ReportSearch.getReportType() == TourSalesReportType.REFUND)
			getElementClick("radiobutton_reporttype_refund_id");

		// Selecting the from date
		selectOptionByText("combobox_reportFrom_date_id", ReportSearch.getReportFrom().split("-")[0].trim());
		selectOptionByText("combobox_reportFrom_month_id", ReportSearch.getReportFrom().split("-")[1].trim());
		selectOptionByText("combobox_reportFrom_year_id", ReportSearch.getReportFrom().split("-")[2].trim());

		// Selecting the TO date
		selectOptionByText("combobox_reportTo_date_id", ReportSearch.getReportFrom().split("-")[0].trim());
		selectOptionByText("combobox_reportTo_month_id", ReportSearch.getReportFrom().split("-")[1].trim());
		selectOptionByText("combobox_reportTo_year_id", ReportSearch.getReportFrom().split("-")[2].trim());

		// Checking the User type
		if (ReportSearch.getReportUserType() == UserType.INTERNAL) {

			// Selecting the currency
			if (ReportSearch.getCurrencyType() == ReportCurrencyType.SELLINGCURR)
				getElementClick("radiobutton_currencytype_sell_id");
			else if (ReportSearch.getCurrencyType() == ReportCurrencyType.BASECURR)
				getElementClick("radiobutton_currencytype_base_id");

			// Selecting the country
			((InputFiled) getElement("inputfield_country_id")).clear();
			((InputFiled) getElement("inputfield_country_id")).setText(ReportSearch.getCountry());
			getElementClick("button_country_lkup_id");
			waitForFrameAndSwitchToIt("frame_reportFrame_id", 100);
			getElementClick("label_countyraw_xpath");
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");

			// Selecting the Booking channel
			if (ReportSearch.getBookingChannel() == ReportBookingChannel.ALL)
				getElementClick("radiobutton_channel_all_id");
			else if (ReportSearch.getBookingChannel() == ReportBookingChannel.POS) {
				getElementClick("radiobutton_channel_cc_id");

				// Selecting the city
				((InputFiled) getElement("inputfield_city_id")).clear();
				((InputFiled) getElement("inputfield_city_id")).setText(ReportSearch.getCountry());
				getElementClick("button_city_lkup_id");
				waitForFrameAndSwitchToIt("frame_reportFrame_id", 100);
				getElementClick("label_cityraw_xpath");
				switchToDefaultFrame();
				switchToFrame("frame_reportFrame_id");

				// Selecting the pos location 

				((InputFiled) getElement("inputfield_pos_location_id")).clear();
				((InputFiled) getElement("inputfield_pos_location_id")).setText(ReportSearch.getCountry());
				getElementClick("button_pos_lkup_id");
				waitForFrameAndSwitchToIt("frame_reportFrame_id", 100);
				getElementClick("label_pos_lkup_first_record_xpath");
				switchToDefaultFrame();
				switchToFrame("frame_reportFrame_id");

			} else if (ReportSearch.getBookingChannel() == ReportBookingChannel.WEB)
				getElementClick("radiobutton_channel_web_id");
		}

		// Selecting Booking Type

		if (ReportSearch.getBookingType() == ReportBookingType.ALL)
			getElementClick("radiobutton_booking_status_all_id");
		else if (ReportSearch.getBookingType() == ReportBookingType.WITHF)
			getElementClick("radiobutton_booking_status_withflight_id");
		else if (ReportSearch.getBookingType() == ReportBookingType.WITHOUTF)
			getElementClick("radiobutton_booking_status_withoutf_id");
		else if (ReportSearch.getBookingType() == ReportBookingType.FONLY)
			getElementClick("radiobutton_booking_status_flightonly_id");

		// Selecting Summary Type

		if (ReportSearch.getSummerizedBy() == SummarizedBy.NONE)
			getElementClick("radiobutton_summerize_none_id");
		else if (ReportSearch.getSummerizedBy() == SummarizedBy.BOOKING)
			getElementClick("radiobutton_summerize_booking_id");
		else if (ReportSearch.getSummerizedBy() == SummarizedBy.POS)
			getElementClick("radiobutton_summerize_city_id");
		else if (ReportSearch.getSummerizedBy() == SummarizedBy.CITY)
			getElementClick("radiobutton_summerize_country_id");
		else if (ReportSearch.getSummerizedBy() == SummarizedBy.COUNTRY)
			getElementClick("radiobutton_reporttype_refund_id");

		// Generate Excel

		if (ReportSearch.isGenerateExcel())
			getElementClick("radiobutton_generate_pdf_yes_id");
		else
			getElementClick("radiobutton_generate_pdf_no_id");

	}

	public boolean getParametersAvailability() {
		return getElementVisibility("radiobutton_reporttype_sales_id");
	}

	public void switchToReportFrame() throws Exception {
		switchToDefaultFrame();
		switchToFrame("frame_reportFrame_id");
	
	}
	public boolean getExcelButtonAvailability() {
		return getElementVisibility("button_excel_xpath");
	}

}
