package com.rezrobot.pages.CC.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.UploadFile;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.ExchangeRateDetailsObject;
import com.rezrobot.utill.DriverFactory;

public class CC_Com_Currency_Page extends PageObject {

	public HashMap<String, String> getCCLabels() {

		HashMap<String, String> labels = new HashMap<>();
		
		try {
			labels.put("cc", getLabelName("label_currency_xpath"));
			labels.put("rate", getLabelName("label_rate_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getCCDetails()
	{
		HashMap<String, String> cc = new HashMap<>();
		int iterator = 2;
		for(int i = 1 ; 1 > 0 ; i++)
		{
			try {
				String currency 	= getElement("label_currency_xpath").changeRefAndGetElement(getElement("label_currency_xpath").getRef().replace("replace", String.valueOf(iterator))).getText();
				String rate 		= getElement("label_selling_rate_xpath").changeRefAndGetElement(getElement("label_selling_rate_xpath").getRef().replace("replace", String.valueOf(iterator))).getText();
				System.out.println(currency + " - " + rate);
				cc.put(currency, rate);
				iterator++;
				if(currency.equals("null") || currency.equals("") || currency.isEmpty())
				{
					break;
				}
			} catch (Exception e) {
				System.out.println("INFO ----> Number of Currency Rates : "+(i-1));
				//e.printStackTrace();
				break;
			}
			//iterator++;
		}
		return cc;
	}
	
	public String getDefaultCurrecny() throws Exception
	{
		String cc = getElement("label_default_xpath").getText();
		return cc;
	}
	
	public ArrayList<ExchangeRateDetailsObject> getAll()
	{
		int iterator = 2;
		ArrayList<ExchangeRateDetailsObject> rateList = new ArrayList<ExchangeRateDetailsObject>();
		for(int i = 1 ; 1 > 0 ; i++)
		{
			ExchangeRateDetailsObject rate = new ExchangeRateDetailsObject();
			try {
				rate.setDate			(getElement("label_date_xpath").changeRefAndGetElement				(getElement("label_date_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setDefaultCurrency	(getElement("label_default_value_xpath").changeRefAndGetElement		(getElement("label_default_value_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setCurrency		(getElement("label_currency_xpath").changeRefAndGetElement			(getElement("label_currency_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setBuyingRate		(getElement("label_buying_rate_xpath").changeRefAndGetElement		(getElement("label_buying_rate_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setSellingRate		(getElement("label_selling_rate_xpath").changeRefAndGetElement		(getElement("label_rate_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rateList.add(rate);
				iterator++;
				if(rate.getCurrency().equals("null") || rate.getCurrency().equals("") || rate.getCurrency().isEmpty())
				{
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
		}
		return rateList;
	}
	
	public ArrayList<ExchangeRateDetailsObject> getNewRates()
	{
		int iterator = 3;
		ArrayList<ExchangeRateDetailsObject> rateList = new ArrayList<ExchangeRateDetailsObject>();
		for(int i = 1 ; 1 > 0 ; i++)
		{
			ExchangeRateDetailsObject rate = new ExchangeRateDetailsObject();
			try {
				rate.setDate			(getElement("label_new_date_xpath").changeRefAndGetElement				(getElement("label_date_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setDefaultCurrency	(getElement("label_new_default_value_xpath").changeRefAndGetElement		(getElement("label_default_value_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setCurrency		(getElement("label_new_currency_xpath").changeRefAndGetElement			(getElement("label_currency_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setBuyingRate		(getElement("label_new_buying_rate_xpath").changeRefAndGetElement		(getElement("label_buying_rate_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setSellingRate		(getElement("label_new_selling_rate_xpath").changeRefAndGetElement		(getElement("label_rate_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rate.setValidity		(getElement("label_new_validity_xpath").changeRefAndGetElement			(getElement("label_rate_xpath").getRef().replace("replace", String.valueOf(iterator))).getText());
				rateList.add(rate);
				iterator++;
				if(rate.getCurrency().equals("null") || rate.getCurrency().equals("") || rate.getCurrency().isEmpty())
				{
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
		}
		return rateList;
	}
	
	public void clickConfirm() throws Exception
	{
		
		getElement("button_new_confirm_xpath").click();
	}
	
	public void uploadFile(String excepPath) throws Exception
	{
		uploadFile(excepPath, "button_new_import_xpath");
		getElement("button_new_import_xpath").getWebElement().sendKeys(excepPath);
	}
}
