package com.rezrobot.pages.CC.common;

import com.rezrobot.core.PageObject;

public class CC_Com_Cancellation_Report_criteria extends PageObject{

	public void enterCriteria(String resNumber) throws Exception
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_reportFrame_id");
			getElement("radiobutton_reservation_number_id").click();
			getElement("radiobutton_hotel_id").click();
			Thread.sleep(2000);
			getElement("inputfield_res_number_id").clear();
			getElement("inputfield_res_number_id").setText(resNumber);
			getElement("button_view_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
	}
}
