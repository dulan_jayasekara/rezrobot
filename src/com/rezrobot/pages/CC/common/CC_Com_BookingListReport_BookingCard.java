package com.rezrobot.pages.CC.common;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;

public class CC_Com_BookingListReport_BookingCard extends PageObject{

	public HashMap<String, Boolean> getHotelLabelAvailability() throws Exception
	{
		HashMap<String, Boolean> labels = new HashMap<>();
		switchToDefaultFrame();
		switchToFrame("frame_report_frame_id");
		switchToFrame("frame_dialog_window_id");
		try {
			getElement("button_hotel_hotel_details_xpath").click();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			labels.put("reservationSummary", 					getElementVisibility("label_hotel_reservation_summary_label_xapth"));
			labels.put("reservationNumber", 					getElementVisibility("label_hotel_reservation_number_label_xpath"));
			labels.put("originCityCountry", 					getElementVisibility("label_hotel_origin_city_country_label_xpath"));
			labels.put("destinationCityCountry", 				getElementVisibility("label_hotel_destination_city_country_label_xpath"));
			labels.put("productsBooked", 						getElementVisibility("label_hotel_products_booked_label_xpath"));
			labels.put("bookingStatus", 						getElementVisibility("label_hotel_booking_status_label_xpath"));
			labels.put("failedSectorIncluded", 					getElementVisibility("label_hotel_failed_sector_included_label_xpath"));
			labels.put("paymentLabel", 							getElementVisibility("label_hotel_payment_method_label_xpath"));
			labels.put("paymentReference", 						getElementVisibility("label_hotel_payment_reference_label_xpath"));
			labels.put("reservationDate", 						getElementVisibility("label_hotel_reservation_date_label_xpath"));
			labels.put("firstElement", 							getElementVisibility("label_hotel_date_of_fisrt_element_label_xpath"));
			labels.put("cancellationDeadline",	 				getElementVisibility("label_hotel_cancellation_deadline_label_xpath"));
			labels.put("amountChergedByPG", 					getElementVisibility("label_hotel_amount_charged_by_pg_label_xpath"));
			labels.put("subTotal", 								getElementVisibility("label_hotel_sub_total_label_xpath"));
			labels.put("bookingFee", 							getElementVisibility("label_hotel_booking_fee_label_xpath"));
			labels.put("taxAndOtherCharges", 					getElementVisibility("label_hotel_tax_and_other_charges_label_xpath"));
			labels.put("ccFee", 								getElementVisibility("label_hotel_cc_fee_label_xpath"));
			labels.put("totalBookingValue", 					getElementVisibility("label_hotel_total_booking_value_label_xpath"));
			labels.put("amountPayableUpfront", 					getElementVisibility("label_hotel_total_amount_payable_upfront_label_xpath"));
			labels.put("totalAmountPAyableAmountAtCheckin", 	getElementVisibility("label_hotel_total_amount_payable_at_checkin_label_xpath"));
			labels.put("amountPaid", 							getElementVisibility("label_hotel_amount_paid_label_xpath"));
			labels.put("customerLeadGuestDetails", 				getElementVisibility("label_hotel_customer_lead_guest_details_label_xpath"));
			labels.put("firstName", 							getElementVisibility("label_hotel_first_name_label_xpath"));
			labels.put("address", 								getElementVisibility("label_hotel_address_label_xpath"));
			labels.put("emailAddress", 							getElementVisibility("label_hotel_email_address_label_xpath"));
			labels.put("city", 									getElementVisibility("label_hotel_city_label_xpath"));
			labels.put("countryLabel", 							getElementVisibility("label_hotel_country_label_xpath"));
			labels.put("emergencyContactNumber", 				getElementVisibility("label_hotel_emergency_contact_number_label_xpath"));
			labels.put("phoneNumber", 							getElementVisibility("label_hotel_phone_number_label_xpath"));
			labels.put("hotelDetails", 							getElementVisibility("label_hotel_hotel_details_label_xpath"));
			labels.put("hotelName", 							getElementVisibility("label_hotel_hotel_name_label_xpath"));
			labels.put("hotelAddress", 							getElementVisibility("label_hotel_hotel_address_label_xpath"));
			labels.put("checkOut", 								getElementVisibility("label_hotel_check_out_label_xpath"));
			labels.put("supplierName", 							getElementVisibility("label_hotel_supplier_name_label_xpath"));
			labels.put("confirmationNumber", 					getElementVisibility("label_hotel_hotel_confirmation_number_label_xpath"));
			labels.put("starCategory", 							getElementVisibility("label_hotel_star_category_label_xpath"));
			labels.put("bookingStatus", 						getElementVisibility("label_hotel_hotel_booking_status_label_xpath"));
			labels.put("checkinDate", 							getElementVisibility("label_hotel_checkin_date_label_xpath"));
			labels.put("numberOfNights", 						getElementVisibility("label_hotel_number_of_nights_label_xpath"));
			labels.put("numberOfRooms", 						getElementVisibility("label_hotel_number_of_rooms_label_xpath"));
			labels.put("hotelContractType", 					getElementVisibility("label_hotel_hotel_contract_type_label_xpath"));
			labels.put("roomBedRate", 							getElementVisibility("label_hotel_room_bed_rate_label_xpath"));
			labels.put("occupantsNames", 						getElementVisibility("label_hotel_occupantcs_name_label_xpath"));
			labels.put("ageGroup", 								getElementVisibility("label_hotel_age_group_label_xpath"));
			labels.put("childAge", 								getElementVisibility("label_hotel_child_age_label_xpath"));
			labels.put("totalRate", 							getElementVisibility("label_hotel_total_rate_label_xpath"));
			labels.put("hotelSupplierNotes", 					getElementVisibility("label_hotel_hotel_supplier_notes_label_xpath"));
			labels.put("internalNotes", 						getElementVisibility("label_hotel_internal_notes_label_xpath"));
			labels.put("customerNotes", 						getElementVisibility("label_hotel_customer_notes_label_xpath"));
			labels.put("fSubtotal", 							getElementVisibility("label_hotel_f_sub_total_label_xpath"));
			labels.put("fTaxAndOtherCharges", 					getElementVisibility("label_hotel_f_tax_and_other_charges_label_xpath"));
			labels.put("fTotalBookingValue", 					getElementVisibility("label_hotel_f_total_booking_value_label_xpath"));
			labels.put("fAmountChargebleUpfront", 				getElementVisibility("label_hotel_f_amount_chargeble_upfront_label_xpath"));
			labels.put("amountDueAtChekin", 					getElementVisibility("label_hotel_amount_due_at_checkin_label_xpath"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getHotelLabels()
	{
		HashMap<String, String> labels = new HashMap<>();
		try {
			labels.put("reservationSummary", 					getElement("label_hotel_reservation_summary_label_xapth").getText());
			labels.put("reservationNumber", 					getElement("label_hotel_reservation_number_label_xpath").getText());
			labels.put("originCityCountry", 					getElement("label_hotel_origin_city_country_label_xpath").getText());
			labels.put("destinationCityCountry", 				getElement("label_hotel_destination_city_country_label_xpath").getText());
			labels.put("productsBooked", 						getElement("label_hotel_products_booked_label_xpath").getText());
			labels.put("bookingStatus", 						getElement("label_hotel_booking_status_label_xpath").getText());
			labels.put("failedSectorIncluded", 					getElement("label_hotel_failed_sector_included_label_xpath").getText());
			labels.put("paymentLabel", 							getElement("label_hotel_payment_method_label_xpath").getText());
			labels.put("paymentReference", 						getElement("label_hotel_payment_reference_label_xpath").getText());
			labels.put("reservationDate", 						getElement("label_hotel_reservation_date_label_xpath").getText());
			labels.put("firstElement", 							getElement("label_hotel_date_of_fisrt_element_label_xpath").getText());
			labels.put("cancellationDeadline",	 				getElement("label_hotel_cancellation_deadline_label_xpath").getText());
			labels.put("amountChergedByPG", 					getElement("label_hotel_amount_charged_by_pg_label_xpath").getText());
			labels.put("subTotal", 								getElement("label_hotel_sub_total_label_xpath").getText());
			labels.put("bookingFee", 							getElement("label_hotel_booking_fee_label_xpath").getText());
			labels.put("taxAndOtherCharges", 					getElement("label_hotel_tax_and_other_charges_label_xpath").getText());
			labels.put("ccFee", 								getElement("label_hotel_cc_fee_label_xpath").getText());
			labels.put("totalBookingValue", 					getElement("label_hotel_total_booking_value_label_xpath").getText());
			labels.put("amountPayableUpfront", 					getElement("label_hotel_total_amount_payable_upfront_label_xpath").getText());
			labels.put("totalAmountPAyableAmountAtCheckin", 	getElement("label_hotel_total_amount_payable_at_checkin_label_xpath").getText());
			labels.put("amountPaid", 							getElement("label_hotel_amount_paid_label_xpath").getText());
			labels.put("customerLeadGuestDetails", 				getElement("label_hotel_customer_lead_guest_details_label_xpath").getText());
			labels.put("firstName", 							getElement("label_hotel_first_name_label_xpath").getText());
			labels.put("address", 								getElement("label_hotel_address_label_xpath").getText());
			labels.put("emailAddress", 							getElement("label_hotel_email_address_label_xpath").getText());
			labels.put("city", 									getElement("label_hotel_city_label_xpath").getText());
			labels.put("countryLabel", 							getElement("label_hotel_country_label_xpath").getText());
			labels.put("emergencyContactNumber", 				getElement("label_hotel_emergency_contact_number_label_xpath").getText());
			labels.put("phoneNumber", 							getElement("label_hotel_phone_number_label_xpath").getText());
			labels.put("hotelDetails", 							getElement("label_hotel_hotel_details_label_xpath").getText());
			labels.put("hotelName", 							getElement("label_hotel_hotel_name_label_xpath").getText());
			labels.put("hotelAddress", 							getElement("label_hotel_hotel_address_label_xpath").getText());
			labels.put("checkOut", 								getElement("label_hotel_check_out_label_xpath").getText());
			labels.put("supplierName", 							getElement("label_hotel_supplier_name_label_xpath").getText());
			labels.put("confirmationNumber", 					getElement("label_hotel_hotel_confirmation_number_label_xpath").getText());
			labels.put("starCategory", 							getElement("label_hotel_star_category_label_xpath").getText());
			labels.put("bookingStatus", 						getElement("label_hotel_hotel_booking_status_label_xpath").getText());
			labels.put("checkinDate", 							getElement("label_hotel_checkin_date_label_xpath").getText());
			labels.put("numberOfNights", 						getElement("label_hotel_number_of_nights_label_xpath").getText());
			labels.put("numberOfRooms", 						getElement("label_hotel_number_of_rooms_label_xpath").getText());
			labels.put("hotelContractType", 					getElement("label_hotel_hotel_contract_type_label_xpath").getText());
			labels.put("roomBedRate", 							getElement("label_hotel_room_bed_rate_label_xpath").getText());
			labels.put("occupantsNames", 						getElement("label_hotel_occupantcs_name_label_xpath").getText());
			labels.put("ageGroup", 								getElement("label_hotel_age_group_label_xpath").getText());
			labels.put("childAge", 								getElement("label_hotel_child_age_label_xpath").getText());
			labels.put("totalRate", 							getElement("label_hotel_total_rate_label_xpath").getText());
			labels.put("hotelSupplierNotes", 					getElement("label_hotel_hotel_supplier_notes_label_xpath").getText());
			labels.put("internalNotes", 						getElement("label_hotel_internal_notes_label_xpath").getText());
			labels.put("customerNotes", 						getElement("label_hotel_customer_notes_label_xpath").getText());
			labels.put("fSubtotal", 							getElement("label_hotel_f_sub_total_label_xpath").getText());
			labels.put("fTaxAndOtherCharges", 					getElement("label_hotel_f_tax_and_other_charges_label_xpath").getText());
			labels.put("fTotalBookingValue", 					getElement("label_hotel_f_total_booking_value_label_xpath").getText());
			labels.put("fAmountChargebleUpfront", 				getElement("label_hotel_f_amount_chargeble_upfront_label_xpath").getText());
			labels.put("amountDueAtChekin", 					getElement("label_hotel_amount_due_at_checkin_label_xpath").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return labels;
	}
	
	public HashMap<String, String> getHotelValues()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("reservationNumber", 					getElement("label_hotel_reservation_number_xpath").getText());
			values.put("originCity", 							getElement("label_hotel_origin_city_country_xpath").getText());
			values.put("destinationCityCountry", 				getElement("label_hotel_destination_city_country_xpath").getText());
			values.put("productsBooked", 						getElement("label_hotel_products_bookied_xpath").getText());
			values.put("bookingStatus", 						getElement("label_hotel_booking_status_xpath").getText());
			values.put("failedSectorIncluded", 					getElement("label_hotel_failed_sector_included_xpath").getText());
			values.put("paymentMethod", 						getElement("label_hotel_payment_method_xpath").getText());
			values.put("paymentReference", 						getElement("label_hotel_payment_reference_xpath").getText());
			values.put("reservationDate", 						getElement("label_hotel_reservation_date_xpath").getText());
			values.put("firstElement", 							getElement("label_hotel_date_of_first_element_xpath").getText());
			values.put("cancellationDeadline", 					getElement("label_hotel_cancellation_deadline_xpath").getText());
			values.put("amountChargedByPG", 					getElement("label_hotel_amount_charged_by_pg_xpath").getText().replace(",", "").replace(".0", ""));
			values.put("reportCurrency", 						getElement("label_hotel_report_currency_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("subtotal", 								getElement("label_hotel_subtotal_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("bookingFee", 							getElement("label_hotel_booking)fee_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("taxAndOtherCharges", 					getElement("label_hotel_tax_and_other_charges_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("ccFee", 								getElement("label_hotel_cc_fee_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("bookingValue", 							getElement("label_hotel_total_booking_value_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("totalAmountPayable", 					getElement("label_hotel_totalamount_payable_ipfront_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("payableAtCheckin", 						getElement("label_hotel_payable_at_checkin_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("amountPaid", 							getElement("label_hotel_amount_paid_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("firstName", 							getElement("label_hotel_first_name_xpath").getText());
			values.put("address", 								getElement("label_hotel_address_xpath").getText());
			values.put("email", 								getElement("label_hotel_email_xpath").getText());
			values.put("city", 									getElement("label_hotel_city_xpath").getText());
			values.put("country", 								getElement("label_hotel_country_xpath").getText());
			values.put("emergencyContactNumber", 				getElement("label_hotel_emergency_contact_number_xpath").getText());
			values.put("phoneNumber",		 					getElement("label_hotel_phone_number_xpath").getText());
			values.put("hotelName", 							getElement("label_hotel_hotel_name_xpath").getText());
			values.put("hotelAddress", 							getElement("label_hotel_hotel_address_xpath").getText());
			values.put("checkoutDate", 							getElement("label_hotel_checkout_date_xpath").getText());
			values.put("supplier", 								getElement("label_hotel_supplier_xpath").getText());
			values.put("hotelConfirmation", 					getElement("label_hotel_hotel_confirmation_number_xpath").getText());
			values.put("starCategory", 							getElement("label_hotel_star_category_xpath").getText());
			values.put("bookingStatus", 						getElement("label_hotel_booking_status_xpath").getText());
			values.put("checkinDate", 							getElement("label_hotel_checkin_date_xpath").getText());
			values.put("numberOfNights", 						getElement("label_hotel_number_of_nights_xpath").getText());
			values.put("numberOfRooms", 						getElement("label_hotel_number_of_rooms_xpath").getText());
			values.put("hotelContractType", 					getElement("label_hotel_hotel_contract_type_xpath").getText());
			ArrayList<String> roomBedRate 	= new ArrayList<String>();
			ArrayList<String> occupantName 	= new ArrayList<String>();
			ArrayList<String> ageGroup 		= new ArrayList<String>();
			ArrayList<String> childAge 		= new ArrayList<String>();
			ArrayList<String> rate 			= new ArrayList<String>();
			for(int i = 2 ; i > 0 ; i++)
			{
				try {
					
					roomBedRate.add(getElement("label_hotel_room_bed_rate_xpath").changeRefAndGetElement(getElement("label_hotel_room_bed_rate_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					rate.add(getElement("label_hotel_rate_xpath").changeRefAndGetElement(getElement("label_hotel_rate_xpath").getRef().replace("replace", String.valueOf(i))).getText());
					for(int j = 1 ; j > 0 ; j++)
					{
						try {
							occupantName.add(getElement("label_hotel_occupant_name_xpath").changeRefAndGetElement(getElement("label_hotel_occupant_name_xpath").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).getText()); 	
							ageGroup.add(getElement("label_hotel_age_group_xpath").changeRefAndGetElement(getElement("label_hotel_age_group_xpath").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).getText());
							childAge.add(getElement("label_hotel_child_age_xpath").changeRefAndGetElement(getElement("label_hotel_child_age_xpath").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).getText());
						} catch (Exception e) {
							// TODO: handle exception
							break;
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
			String roomBedRateString = "";
			String occupantNameString = "";
			String ageGroupString = "";
			String childAgeString = "";
			String rateString = "";
			for(int i  = 0 ; i < roomBedRate.size() ; i++)
			{
				if(i == 0)
				{
					roomBedRateString = roomBedRate.get(i);
					rateString = rate.get(i);
				}
				else
				{
					roomBedRateString = roomBedRateString.concat(",").concat(roomBedRate.get(i));
					rateString = rateString.concat(",").concat(rate.get(i));
				}
			}
			for(int i = 0 ; i < occupantName.size() ; i++)
			{
				if(i == 0)
				{
					occupantNameString = occupantName.get(i);
					ageGroupString = ageGroup.get(i);
					childAgeString = childAge.get(i);
				}
				else
				{
					occupantNameString = occupantNameString.concat(",").concat(occupantName.get(i));
					ageGroupString = ageGroupString.concat(",").concat(ageGroup.get(i));
					childAgeString = childAgeString.concat(",").concat(childAge.get(i));
				}
			}
			System.out.println(occupantNameString);
			System.out.println(ageGroupString);
			System.out.println(childAgeString);
			values.put("roomBedRate", 							roomBedRateString);
			values.put("occupantName", 							occupantNameString);
			values.put("ageGroup",	 							ageGroupString);
			values.put("childAge", 								childAgeString);
			values.put("rate", 									rateString.replace(".00", ""));
			values.put("hotelNotes", 							getElement("label_hotel_hotel_notes_xpath").getText());
			values.put("internalNotes", 						getElement("label_hotel_internal_notes_xpath").getText());
			values.put("customerNotes", 						getElement("label_hotel_customer_notes_xpath").getText());
			values.put("fCC", 									getElement("label_hotel_f_cc_xpath").getText());
			values.put("fSubTotal", 							getElement("label_hotel_f_subtotal_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("fTaxAndOtherChrges", 					getElement("label_hotel_f_tax_and_other_charges_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("fTotalBookingValue", 					getElement("label_hotel_f_total_booking_value_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("fAmountChargebleUpFront", 				getElement("label_hotel_f_amount_chargeble_upfront_xpath").getText().replace(",", "").replace(".00", ""));
			values.put("fAmountDueAtCheckin", 					getElement("label_hotel_f_amount_due_at_checkin_xpath").getText().replace(",", "").replace(".00", ""));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
	
	public HashMap<String, Boolean> getActivityLabelAvailability() throws Exception{
		
		HashMap<String, Boolean> labels = new HashMap<>();
		switchToDefaultFrame();
		switchToFrame("frame_report_frame_id");
		switchToFrame("frame_dialog_window_id");
		
		try {
			labels.put("reservationSummary", 					getElementVisibility("label_hotel_reservation_summary_label_xapth"));
			labels.put("reservationNumber", 					getElementVisibility("label_hotel_reservation_number_label_xpath"));
			labels.put("originCityCountry", 					getElementVisibility("label_hotel_origin_city_country_label_xpath"));
			labels.put("destinationCityCountry", 				getElementVisibility("label_hotel_destination_city_country_label_xpath"));
			labels.put("productsBooked", 						getElementVisibility("label_hotel_products_booked_label_xpath"));
			labels.put("bookingStatus", 						getElementVisibility("label_hotel_booking_status_label_xpath"));
			labels.put("failedSectorIncluded", 					getElementVisibility("label_hotel_failed_sector_included_label_xpath"));
			labels.put("paymentLabel", 							getElementVisibility("label_hotel_payment_method_label_xpath"));
			labels.put("paymentReference", 						getElementVisibility("label_hotel_payment_reference_label_xpath"));
			labels.put("reservationDate", 						getElementVisibility("label_hotel_reservation_date_label_xpath"));
			labels.put("firstElement", 							getElementVisibility("label_hotel_date_of_fisrt_element_label_xpath"));
			labels.put("cancellationDeadline",	 				getElementVisibility("label_hotel_cancellation_deadline_label_xpath"));
			labels.put("amountChergedByPG", 					getElementVisibility("label_hotel_amount_charged_by_pg_label_xpath"));
			labels.put("subTotal", 								getElementVisibility("label_hotel_sub_total_label_xpath"));
			labels.put("bookingFee", 							getElementVisibility("label_hotel_booking_fee_label_xpath"));
			labels.put("taxAndOtherCharges", 					getElementVisibility("label_hotel_tax_and_other_charges_label_xpath"));
			labels.put("ccFee", 								getElementVisibility("label_hotel_cc_fee_label_xpath"));
			labels.put("totalBookingValue", 					getElementVisibility("label_hotel_total_booking_value_label_xpath"));
			labels.put("amountPayableUpfront", 					getElementVisibility("label_hotel_total_amount_payable_upfront_label_xpath"));
			labels.put("totalAmountPAyableAmountAtCheckin", 	getElementVisibility("label_hotel_total_amount_payable_at_checkin_label_xpath"));
			labels.put("amountPaid", 							getElementVisibility("label_hotel_amount_paid_label_xpath"));
			labels.put("customerLeadGuestDetails", 				getElementVisibility("label_hotel_customer_lead_guest_details_label_xpath"));
			labels.put("firstName", 							getElementVisibility("label_hotel_first_name_label_xpath"));
			labels.put("address", 								getElementVisibility("label_hotel_address_label_xpath"));
			labels.put("emailAddress", 							getElementVisibility("label_hotel_email_address_label_xpath"));
			labels.put("city", 									getElementVisibility("label_hotel_city_label_xpath"));
			labels.put("countryLabel", 							getElementVisibility("label_hotel_country_label_xpath"));
			labels.put("emergencyContactNumber", 				getElementVisibility("label_hotel_emergency_contact_number_label_xpath"));
			labels.put("phoneNumber", 							getElementVisibility("label_hotel_phone_number_label_xpath"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return labels;	
	}
	
	public HashMap<String, String> getActivityLabels(){
		
		HashMap<String, String> labels = new HashMap<>();
		try {
			
			labels.put("reservationSummary", 					getElement("label_hotel_reservation_summary_label_xapth").getText());
			labels.put("reservationNumber", 					getElement("label_hotel_reservation_number_label_xpath").getText());
			labels.put("originCityCountry", 					getElement("label_hotel_origin_city_country_label_xpath").getText());
			labels.put("destinationCityCountry", 				getElement("label_hotel_destination_city_country_label_xpath").getText());
			labels.put("productsBooked", 						getElement("label_hotel_products_booked_label_xpath").getText());
			labels.put("bookingStatus", 						getElement("label_hotel_booking_status_label_xpath").getText());
			labels.put("failedSectorIncluded", 					getElement("label_hotel_failed_sector_included_label_xpath").getText());
			labels.put("paymentLabel", 							getElement("label_hotel_payment_method_label_xpath").getText());
			labels.put("paymentReference", 						getElement("label_hotel_payment_reference_label_xpath").getText());
			labels.put("reservationDate", 						getElement("label_hotel_reservation_date_label_xpath").getText());
			labels.put("firstElement", 							getElement("label_hotel_date_of_fisrt_element_label_xpath").getText());
			labels.put("cancellationDeadline",	 				getElement("label_hotel_cancellation_deadline_label_xpath").getText());
			labels.put("amountChergedByPG", 					getElement("label_hotel_amount_charged_by_pg_label_xpath").getText());
			labels.put("subTotal", 								getElement("label_hotel_sub_total_label_xpath").getText());
			labels.put("bookingFee", 							getElement("label_hotel_booking_fee_label_xpath").getText());
			labels.put("taxAndOtherCharges", 					getElement("label_hotel_tax_and_other_charges_label_xpath").getText());
			labels.put("ccFee", 								getElement("label_hotel_cc_fee_label_xpath").getText());
			labels.put("totalBookingValue", 					getElement("label_hotel_total_booking_value_label_xpath").getText());
			labels.put("amountPayableUpfront", 					getElement("label_hotel_total_amount_payable_upfront_label_xpath").getText());
			labels.put("totalAmountPAyableAmountAtCheckin", 	getElement("label_hotel_total_amount_payable_at_checkin_label_xpath").getText());
			labels.put("amountPaid", 							getElement("label_hotel_amount_paid_label_xpath").getText());
			labels.put("customerLeadGuestDetails", 				getElement("label_hotel_customer_lead_guest_details_label_xpath").getText());
			labels.put("firstName", 							getElement("label_hotel_first_name_label_xpath").getText());
			labels.put("address", 								getElement("label_hotel_address_label_xpath").getText());
			labels.put("emailAddress", 							getElement("label_hotel_email_address_label_xpath").getText());
			labels.put("city", 									getElement("label_hotel_city_label_xpath").getText());
			labels.put("countryLabel", 							getElement("label_hotel_country_label_xpath").getText());
			labels.put("emergencyContactNumber", 				getElement("label_hotel_emergency_contact_number_label_xpath").getText());
			labels.put("phoneNumber", 							getElement("label_hotel_phone_number_label_xpath").getText());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return labels;
	}
	
	
	
}
