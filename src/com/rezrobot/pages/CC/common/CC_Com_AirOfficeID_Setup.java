package com.rezrobot.pages.CC.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.rezrobot.core.PageObject;
import com.rezrobot.flight_circuitry.Initiator;
import com.rezrobot.utill.ExcelReader;
import com.rezrobot.utill.ReportTemplate;

public class CC_Com_AirOfficeID_Setup extends PageObject{

	String country = "";
	String state = "";
	String airport = "";
	String officeid = "";
	String stationReq = "";
	String paymentFloType = "";
	String voucherType = "";
	String add = "";
	String edit = "";
	String remove = "";
	String stationid = "";
	String editStationReq = "";
	String editPaymentType = "";
	String editVoucherType = "";
	String editStationID = "";
	
	
	public String getEditStationReq() {
		return editStationReq;
	}
	public void setEditStationReq(String editStationReq) {
		this.editStationReq = editStationReq;
	}
	public String getEditPaymentType() {
		return editPaymentType;
	}
	public void setEditPaymentType(String editPaymentType) {
		this.editPaymentType = editPaymentType;
	}
	public String getEditVoucherType() {
		return editVoucherType;
	}
	public void setEditVoucherType(String editVoucherType) {
		this.editVoucherType = editVoucherType;
	}
	public String getEditStationID() {
		return editStationID;
	}
	public void setEditStationID(String editStationID) {
		this.editStationID = editStationID;
	}
	public String getStationid() {
		return stationid;
	}
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAirport() {
		return airport;
	}
	public void setAirport(String airport) {
		this.airport = airport;
	}
	public String getOfficeid() {
		return officeid;
	}
	public void setOfficeid(String officeid) {
		this.officeid = officeid;
	}
	public String getStationReq() {
		return stationReq;
	}
	public void setStationReq(String stationReq) {
		this.stationReq = stationReq;
	}
	public String getPaymentFloType() {
		return paymentFloType;
	}
	public void setPaymentFloType(String paymentFloType) {
		this.paymentFloType = paymentFloType;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	public String getRemove() {
		return remove;
	}
	public void setRemove(String remove) {
		this.remove = remove;
	}
	
	
	
	public void setup(String url, ReportTemplate PrintTemplate) throws Exception
	{
		Initiator readExcel = new Initiator();
		ArrayList<CC_Com_AirOfficeID_Setup> stationExcelList 		= readExcel.getStationDetails();
		ArrayList<CC_Com_AirOfficeID_Setup> stationTableList 	= getTabelDetails();
		for(int i = 0 ; i < stationExcelList.size() ; i++)
		{
			//set country name
			getElement("inputfield_countryname_id").setText(stationExcelList.get(i).getCountry());
			getElement("button_countryname_id").click();
			switchToFrame("frame_lookup_id");
			getElement("label_first_record_xpath").click();
			switchToDefaultFrame();
			//set state
			if(stationExcelList.get(i).getState().equals("n/a"))
			{
				
			}
			else
			{
				getElement("inputfield_state_id").setText(stationExcelList.get(i).getState());
				getElement("button_countryname_id").click();
				switchToFrame("frame_lookup_id");
				getElement("label_first_record_xpath").click();
				switchToDefaultFrame();
			}
			
			//set airport
			getElement("inputfield_airport_id").setText(stationExcelList.get(i).getAirport());
			getElement("button_countryname_id").click();
			switchToFrame("frame_lookup_id");
			getElement("label_first_record_xpath").click();
			switchToDefaultFrame();
			//set office id
			getElement("inputfield_officeid_id").setText(stationExcelList.get(i).getOfficeid());
			getElement("button_countryname_id").click();
			switchToFrame("frame_lookup_id");
			getElement("label_first_record_xpath").click();
			switchToDefaultFrame();
			
			//station required
			getElement("radiobutton_stationRequired_id").click();
			
			//select payment type
			if(stationExcelList.get(i).getPaymentFloType().equals("all"))
			{
				getElement("checkbox_pt_all_id").click();
			}
			else
			{
				if(stationExcelList.get(i).getPaymentFloType().equals("flocash"))
				{
					getElement("checkbox_pt_flow_cash_id").click();
				}
				else if(stationExcelList.get(i).getPaymentFloType().equals("credit"))
				{
					getElement("checkbox_pt_creditCard_id").click();
				}
				else if(stationExcelList.get(i).getPaymentFloType().equals("offline"))
				{
					getElement("checkbox_pt_offline_id").click();
				}
				else if(stationExcelList.get(i).getPaymentFloType().equals("flocash/credit"))
				{
					getElement("checkbox_pt_flow_cash_id").click();
					getElement("checkbox_pt_creditCard_id").click();
				}
				else if(stationExcelList.get(i).getPaymentFloType().equals("flocash/offline"))
				{
					getElement("checkbox_pt_flow_cash_id").click();
					getElement("checkbox_pt_offline_id").click();
				}
				else if(stationExcelList.get(i).getPaymentFloType().equals("credit/offline"))
				{
					getElement("checkbox_pt_creditCard_id").click();
					getElement("checkbox_pt_offline_id").click();
				}
				else if(stationExcelList.get(i).getPaymentFloType().equals("flocash/credit/offline"))
				{
					getElement("checkbox_pt_flow_cash_id").click();
					getElement("checkbox_pt_creditCard_id").click();
					getElement("checkbox_pt_offline_id").click();
				}
			}
			
			//select voucher type
			if(stationExcelList.get(i).getVoucherType().equals("emd"))
			{
				getElement("checkbox_vt_emd_id").click();
			}
			else if(stationExcelList.get(i).getVoucherType().equals("eticket"))
			{
				getElement("checkbox_vt_eticket_id").click();
			}
			else if(stationExcelList.get(i).getVoucherType().equals("both"))
			{
				getElement("checkbox_vt_emd_id").click();
				getElement("checkbox_vt_eticket_id").click();
			}
			
			//set station id
			getElement("inputfield_stationid_id").setText("000".concat(String.valueOf(i)));
			
			//view
			getElement("button_view_record_id").click();
		
			HashMap<String, String> tableData = new HashMap<>();
			if(getElementVisibility("label_office_id_classname"))
			{
				tableData.put("country", 		getElement("label_country_name_classname").getText());
				tableData.put("state", 			getElement("label_state_name_classname").getText());
				tableData.put("airport", 		getElement("label_airport_name_classname").getText());
				tableData.put("officeID", 		getElement("label_office_id_classname").getText());
				tableData.put("paymentType", 	getElement("label_payment_type_classname").getText());
				tableData.put("voucherType", 	getElement("label_voucher_type_classname").getText());
				tableData.put("stationID", 		getElement("label_station_id_classname").getText());
				verifyRecord(stationExcelList.get(i), tableData, PrintTemplate);
				if(stationExcelList.get(i).getEdit().equals("yes") && stationExcelList.get(i).getRemove().equals("yes"))
				{
					System.out.println("Invalid action");
				}
				//remove
				else if(stationExcelList.get(i).getEdit().equals("no") && stationExcelList.get(i).getRemove().equals("yes"))
				{
					getElement("button_remove_record_classname").click();
				}
				//edit
				else if(stationExcelList.get(i).getEdit().equals("yes") && stationExcelList.get(i).getRemove().equals("no"))
				{
					getElement("button_edit_record_classname").click();
					
					//reset fields before edit
					if(isCheckBoxSelected("checkbox_pt_flow_cash_id"))
					{
						getElement("checkbox_pt_flow_cash_id").click();
					}
					if(isCheckBoxSelected("checkbox_pt_creditCard_id"))
					{
						getElement("checkbox_pt_creditCard_id").click();
					}
					if(isCheckBoxSelected("checkbox_pt_offline_id"))
					{
						getElement("checkbox_pt_offline_id").click();
					}
					if(isCheckBoxSelected("checkbox_pt_all_id"))
					{
						getElement("checkbox_pt_all_id").click();
					}
					if(isCheckBoxSelected("checkbox_vt_emd_id"))
					{
						getElement("checkbox_vt_emd_id").click();
					}
					if(isCheckBoxSelected("checkbox_vt_eticket_id"))
					{
						getElement("checkbox_vt_eticket_id").click();
					}
					
					
					//station required
					getElement("radiobutton_stationRequired_id").click();
					
					//select payment type
					if(stationExcelList.get(i).getEditPaymentType().equals("all"))
					{
						getElement("checkbox_pt_all_id").click();
					}
					else
					{
						if(stationExcelList.get(i).getEditPaymentType().equals("flocash"))
						{
							getElement("checkbox_pt_flow_cash_id").click();
						}
						else if(stationExcelList.get(i).getEditPaymentType().equals("credit"))
						{
							getElement("checkbox_pt_creditCard_id").click();
						}
						else if(stationExcelList.get(i).getEditPaymentType().equals("offline"))
						{
							getElement("checkbox_pt_offline_id").click();
						}
						else if(stationExcelList.get(i).getEditPaymentType().equals("flocash/credit"))
						{
							getElement("checkbox_pt_flow_cash_id").click();
							getElement("checkbox_pt_creditCard_id").click();
						}
						else if(stationExcelList.get(i).getEditPaymentType().equals("flocash/offline"))
						{
							getElement("checkbox_pt_flow_cash_id").click();
							getElement("checkbox_pt_offline_id").click();
						}
						else if(stationExcelList.get(i).getEditPaymentType().equals("credit/offline"))
						{
							getElement("checkbox_pt_creditCard_id").click();
							getElement("checkbox_pt_offline_id").click();
						}
						else if(stationExcelList.get(i).getEditPaymentType().equals("flocash/credit/offline"))
						{
							getElement("checkbox_pt_flow_cash_id").click();
							getElement("checkbox_pt_creditCard_id").click();
							getElement("checkbox_pt_offline_id").click();
						}
					}
					
					//select voucher type
					if(stationExcelList.get(i).getEditVoucherType().equals("emd"))
					{
						getElement("checkbox_vt_emd_id").click();
					}
					else if(stationExcelList.get(i).getEditVoucherType().equals("eticket"))
					{
						getElement("checkbox_vt_eticket_id").click();
					}
					else if(stationExcelList.get(i).getEditVoucherType().equals("both"))
					{
						getElement("checkbox_vt_emd_id").click();
						getElement("checkbox_vt_eticket_id").click();
					}
					
					//set station id
					getElement("inputfield_stationid_id").setText("000".concat(String.valueOf(i)));
					
					getElement("button_add_record_id").click();
				}
			}
			else
			{
				if(stationExcelList.get(i).getAdd().equals("yes"))
				{
					getElement("button_add_record_id").click();
				}
			}
		}
	}	
	
	public ArrayList<CC_Com_AirOfficeID_Setup> getTabelDetails() throws Exception
	{
		ArrayList<CC_Com_AirOfficeID_Setup> stationList = new ArrayList<CC_Com_AirOfficeID_Setup>();
		for(int i = 0 ; i < getElements("label_country_name_classname").size() ; i++)
		{
			CC_Com_AirOfficeID_Setup station = new CC_Com_AirOfficeID_Setup();
			station.setCountry			(getElements("label_country_name_classname").get(i).getText());
			station.setState			(getElements("label_state_name_classname").get(i).getText());
			station.setAirport			(getElements("label_airport_name_classname").get(i).getText());
			station.setOfficeid			(getElements("label_office_id_classname").get(i).getText());
			station.setPaymentFloType	(getElements("label_payment_type_classname").get(i).getText());
			station.setVoucherType		(getElements("label_voucher_type_classname").get(i).getText());
			station.setStationid		(getElements("label_station_id_classname").get(i).getText());
			stationList.add(station);
		}
		return stationList;
	}
	
	public void verifyRecord(CC_Com_AirOfficeID_Setup excelData, HashMap<String, String> tableData, ReportTemplate printTemplate)
	{
		//printTemplate.setTableHeading("Verify record");
		printTemplate.verifyTrue(excelData.getCountry(), 			tableData.get("country"), 		"Country");
		printTemplate.verifyTrue(excelData.getState(), 				tableData.get("state"), 		"State");
		printTemplate.verifyTrue(excelData.getAirport(), 			tableData.get("airport"), 		"Airport");
		printTemplate.verifyTrue(excelData.getOfficeid(), 			tableData.get("officeID"), 		"Office ID");
		printTemplate.verifyTrue(excelData.getPaymentFloType(), 	tableData.get("paymentType"), 	"Payment Type");
		printTemplate.verifyTrue(excelData.getVoucherType(), 		tableData.get("voucherType"), 	"Voucher Type");
		printTemplate.verifyTrue(excelData.getStationid(), 			tableData.get("stationID"), 	"Station ID");
		//printTemplate.markTableEnd();
	}
}
