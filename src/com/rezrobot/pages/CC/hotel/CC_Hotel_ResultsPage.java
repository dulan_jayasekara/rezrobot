package com.rezrobot.pages.CC.hotel;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelResultsPageMoreDetails;
import com.rezrobot.dataobjects.HotelRoomDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.utill.DriverFactory;

public class CC_Hotel_ResultsPage extends PageObject{

	public boolean isAvailable() throws Exception
	{
		switchToDefaultFrame();
		switchToFrame("frame_live_message_frame_id");
		try {
			getElementVisibility("label_firts_hotel_id", 30);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return getelementAvailability("label_firts_hotel_id");
	}
	
	public ArrayList<HotelDetails> gethotelDetails() throws Exception
	{
		System.out.println("Get hotel list");
		String[] temp = getElement("label_number_of_hotel_xpath").getText().split(" ");
		int hotelCount = Integer.parseInt(temp[6].trim());
		int pagecnt = 0;
		if(hotelCount % 10 == 0)
		{
			pagecnt = hotelCount / 10;
		}
		else
		{
			pagecnt = hotelCount / 10;
			pagecnt++;
		}
		int hotelNumber = 1;
		String supplier = "";
		ArrayList<HotelDetails> hotelList = new ArrayList<HotelDetails>();
		for(int i = 1 ; i <= pagecnt ; i++)
		{
			if(i != 1)
			{
				try {
					executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+ i +"},'','WJ_2'))");
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
			for(int j = 0 ; j < 10 ; j++)
			{
				try {
					HotelDetails hotel = new HotelDetails();
					String hotelID = getElement("label_hotelID_id").changeRefAndGetElement(getElement("label_hotelID_id").getRef().replace("replace", String.valueOf(hotelNumber))).getAttribute("value");
					hotel.setTitle(getElement("label_hotel_name_id").changeRefAndGetElement(getElement("label_hotel_name_id").getRef().replace("replace", String.valueOf(hotelID))).getText());
					try {
						hotel.setAddress(getElement("label_hotel_address_id").changeRefAndGetElement(getElement("label_hotel_address_id").getRef().replace("replace", String.valueOf(hotelID))).getText());
					} catch (Exception e) {
						// TODO: handle exception
					}
					temp = getElement("label_moreinfo_id").changeRefAndGetElement(getElement("label_moreinfo_id").getRef().replace("replace", String.valueOf(hotelID))).getAttribute("href").split(",");
					hotel.setSupplier(temp[3].replace("'", "").replace(")", ""));
					hotel.setRate(getElement("label_rate_id").changeRefAndGetElement(getElement("label_rate_id").getRef().replace("replace1", String.valueOf(hotelID)).replace("replace2", String.valueOf(hotelNumber))).getText());
					hotelList.add(hotel);
					hotelNumber++;
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
			
		}
		return hotelList;
	}
	
	public HotelDetails selectHotel(ArrayList<HotelDetails> hotelList, HotelSearchObject search) throws Exception
	{
		System.out.println("Get selected hotel details");
		int hb = 0;
		int gta = 0;
		int tourico = 0;
		int rezlive = 0;
		int internal = 0;
		int hotelspro = 0;
		String selectedHotel = "";
		for(int i = 0 ; i < hotelList.size() ; i++)
		{
			if(selectedHotel.equals(""))
			{
				if(search.getSupplier().equals(hotelList.get(i).getSupplier()))
				{
					selectedHotel = hotelList.get(i).getTitle();
				}
			}
			if(hotelList.get(i).getSupplier().equals("hotelbeds_v2"))
				hb++;
			else if(hotelList.get(i).getSupplier().equals("INV27"))
				gta++;
			else if(hotelList.get(i).getSupplier().equals("INV13"))
				tourico++;
			else if(hotelList.get(i).getSupplier().equals("rezlive"))
				rezlive++;
			else if(hotelList.get(i).getSupplier().equals("hotelspro"))
				hotelspro++;
			else
				internal++;
		}
		System.out.println("hb - " + hb);
		System.out.println("gta - " + gta);
		System.out.println("tourico - " + tourico);
		System.out.println("rezlive - " + rezlive);
		System.out.println("internal - " + internal);
		System.out.println("hotelspro - " + hotelspro);
		
		String[] temp = getElement("label_number_of_hotel_xpath").getText().split(" ");
		int hotelCount = Integer.parseInt(temp[6].trim());
		int pagecnt = 0;
		if(hotelCount % 10 == 0)
		{
			pagecnt = hotelCount / 10;
		}
		else
		{
			pagecnt = hotelCount / 10;
			pagecnt++;
		}
		
		// get selected hotel details
			
		
		int hotelNumber = 1;
		String supplier = "";
		HotelDetails hotel = new HotelDetails();
		outerloop : for(int i = 1 ; i <= pagecnt ; i++)
		{
			try {
				executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+ i +"},'','WJ_2'))");
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			for(int j = 0 ; j < 10 ; j++)
			{
				try {
					String hotelID = getElement("label_hotelID_id").changeRefAndGetElement(getElement("label_hotelID_id").getRef().replace("replace", String.valueOf(hotelNumber))).getAttribute("value");
					if(getElement("label_hotel_name_id").changeRefAndGetElement(getElement("label_hotel_name_id").getRef().replace("replace", String.valueOf(hotelID))).getText().equals(selectedHotel))
					{
						try {
							hotel.setHotelID(hotelID);
							System.out.println("Selected hotel - " + selectedHotel);
							System.out.println("Selected hotel available - " + getElement("label_hotel_name_id").changeRefAndGetElement(getElement("label_hotel_name_id").getRef().replace("replace", String.valueOf(hotelID))).getText());
							hotel.setTitle(getElement("label_hotel_name_id").changeRefAndGetElement(getElement("label_hotel_name_id").getRef().replace("replace", String.valueOf(hotelID))).getText());
							try {
								hotel.setAddress(getElement("label_hotel_address_id").changeRefAndGetElement(getElement("label_hotel_address_id").getRef().replace("replace", String.valueOf(hotelID))).getText());
							} catch (Exception e) {
								// TODO: handle exception
							}
							hotel.setTracerID(getElement("label_tracerID_id").changeRefAndGetElement(getElement("label_tracerID_id").getRef().replace("replace", hotelID)).getAttribute("value"));
							temp = getElement("label_moreinfo_id").changeRefAndGetElement(getElement("label_moreinfo_id").getRef().replace("replace", String.valueOf(hotelID))).getAttribute("href").split(",");
							hotel.setSupplier(temp[3].replace("'", "").replace(")", ""));
							String[] rate = getElement("label_rate_id").changeRefAndGetElement(getElement("label_rate_id").getRef().replace("replace1", String.valueOf(hotelID)).replace("replace2", String.valueOf(hotelNumber))).getText().split(" ");
							hotel.setRate(rate[1]);
							hotel.setCurrency(rate[0]);
							
							try {
								Thread.sleep(2000);
								getElement("button_show_rooms_id").changeRefAndGetElement(getElement("button_show_rooms_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber))).click();
							} catch (Exception e) {
								// TODO: handle exception
							}	
		//					hotel.setStarRating(getElement("image_hotel_star_rating_xpath").changeRefAndGetElement(getElement("image_hotel_star_rating_xpath").getRef().replace("replace", hotelID)).getText());
							ArrayList<HotelRoomDetails> roomList = new ArrayList<HotelRoomDetails>();
							for(int k = 0 ; k > -1 ; k++)
							{
								HotelRoomDetails room = new HotelRoomDetails();
								try {
									room.setRoom(getElement("label_room_id").changeRefAndGetElement(getElement("label_room_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber)).replace("replace3", String.valueOf(k))).getText());
									room.setRoomType(getElement("label_room_type_id").changeRefAndGetElement(getElement("label_room_type_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber)).replace("replace3", String.valueOf(k))).getText());
									room.setBedType(getElement("label_bed_type_id").changeRefAndGetElement(getElement("label_bed_type_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber)).replace("replace3", String.valueOf(k))).getText());
									room.setRatePlan(getElement("label_rate_plan_id").changeRefAndGetElement(getElement("label_rate_plan_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber)).replace("replace3", String.valueOf(k))).getText());
									room.setRate(getElement("label_rate_id").changeRefAndGetElement(getElement("label_rate_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber)).replace("replace3", String.valueOf(k))).getText());
									roomList.add(room);
								} catch (Exception e) {
									// TODO: handle exception
									break;
								}
							}
							hotel.setRoomList(roomList);
							System.out.println("More info");
							getElement("link_hotel_info_id").changeRefAndGetElement(getElement("link_hotel_info_id").getRef().replace("replace", hotelID)).click();
							switchToFrame("frame_more_info_frame_id");
							HotelResultsPageMoreDetails moreinfo = new HotelResultsPageMoreDetails();
							if(getElementVisibility("label_more_details_hotel_name_xpath", 30))
							{
								System.out.println("Overview");
								moreinfo.setMoreInfoAvailability(true);
								try {
									getElementVisibility("label_more_details_overview_xpath", 20);
								} catch (Exception e) {
									// TODO: handle exception
								}
								moreinfo.setHotelNameAvailability(getElementVisibility("label_more_details_hotel_name_xpath"));
								moreinfo.setStarratingAvailability(getElementVisibility("image_more_details_star_rating_xpath"));
								moreinfo.setMoreInfoAvailability(getElementVisibility("label_more_details_overview_xpath"));
								System.out.println("location on map");
								getElement("button_more_details_location_on_map_xpath").click();
								moreinfo.setMapAvailability(getElementVisibility("map_more_details_loacation_on_map_xpath"));
								System.out.println("Reviews and ratings");
								getElement("button_more_details_reviews_and_rating_xpath").click();
								moreinfo.setReviewsAndRatingsAvailability(getElementVisibility("label_more_details_reviews_and_ratings_xpath"));
								System.out.println("Amenities");
								getElement("button_more_details_amenities_xpath").click();
								moreinfo.setAmanityAvailability(getElementVisibility("label_more_details_amenities_xpath"));
								ArrayList<String> amenityList = new ArrayList<String>();
								for(int amenity = 1 ; amenity > 0 ; amenity++)
								{
									try {
										amenityList.add(getElement("label_amenities_list_xpath").changeRefAndGetElement(getElement("label_amenities_list_xpath").getRef().replace("replace", String.valueOf(amenity))).getText());
									} catch (Exception e) {
										// TODO: handle exception
										break;
									}
								}
								moreinfo.setAmenityList(amenityList);
								System.out.println("Additional info");
								getElement("button_more_details_additional_info_xpath").click();
								moreinfo.setAdditionalInfo(getElement("label_more_details_additional_info_xpath").getText());
								System.out.println("Email to friend");
								getElement("link_more_details_email_to_friend_xpath").click();
								getElement("inputfield_email_address_id").setText("akila@colombo.rezgateway.com");
								moreinfo.setEmailSubject(getElement("label_more_details_email_to_friend_subject_xpath").getText());
								moreinfo.setEmailBody(getElement("label_more_details_email_to_friend_body_xpath").getText());
								getElement("button_send_email_classname").click();
								moreinfo.setEmailSentStatus(getElement("label_email_status_id").getText());
								getElement("button_email_confirm_xpath").click();
								switchToDefaultFrame();
								switchToFrame("frame_live_message_frame_id");
								getElement("button_more_info_close_xpath").click();
								Thread.sleep(2000);
								//select hotel click
								getElement("button_select_this_hotel_id").changeRefAndGetElement(getElement("button_select_this_hotel_id").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(hotelNumber))).click();
								
								break outerloop;
							}
							else
							{
								moreinfo.setMoreInfoAvailability(false);
							}
							hotel.setMoreInfo(moreinfo);
							break;
						
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("Selected hotel - Get details - Issue" + e);
							break;
						}
					}
					hotelNumber++;
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}	
			}
		}	
		//wait for the rate change
		try {
			System.out.println(getElement("label_rateChange_error_msg_id").getText());
			if(getElementVisibility("label_rateChange_error_msg_id", 20))
			{
				hotel.setRateChangeAvailable(true);
				hotel.setRateChangeError(getElement("label_rateChange_error_id").getText());
				hotel.setRateChangeErrorMsg(getElement("label_rateChange_error_msg_id").getText());
				getElement("button_rateChange_ok_xpath").click();
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		return hotel;
	}
	
	public HashMap<String, String> getCartValues() throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		if(getElementVisibility("label_cart_hotel_rate_id", 30))
		{
			values.put("hotelRate", 	getElement("label_cart_hotel_rate_id").getText());
			values.put("subtotal", 		getElement("label_cart_subtotal_id").getText());
			values.put("tax", 			getElement("label_cart_taxes_id").getText());
			values.put("totalPayable", 	getElement("label_cart_total_payable_id").getText());
			Thread.sleep(5000);
			getElement("button_checkout_id").click();
		}
		else
		{
			System.out.println("cart unavailable");
		}
		return values;
	}
}
