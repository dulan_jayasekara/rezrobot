package com.rezrobot.pages.CC.hotel;

import java.util.HashMap;

import com.rezrobot.core.PageObject;

public class CC_Hotel_Cancellation_Report extends PageObject{

	public HashMap<String, String> getValues()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("cancellationNumber", 						getElement("labelcanNumber_xpath").getText().trim());
			values.put("cancellationDate", 							getElement("label_cancellation_date_xpath").getText().trim());
			values.put("notes", 									getElement("label_notes_xpath").getText().trim());
			values.put("reservationNumber", 						getElement("label_resNumber_xpath").getText().trim());
			values.put("documentNumber", 							getElement("label_document_number_xpath").getText().trim());
			values.put("cancelledBy", 								getElement("label_cancelled_by_xpath").getText().trim());
			values.put("cancellationReason", 						getElement("label_cancellation_reason_xpath").getText().trim());
			values.put("hotelName", 								getElement("label_hotel_name_xpath").getText().trim());
			values.put("supplierName", 								getElement("label_supplier_name_xpath").getText().trim());
			values.put("supplierConfirmationNumber", 				getElement("label_supplier_confirmation_number_xpath").getText().trim());
			values.put("numberOfRooms", 							getElement("label_number_of_rooms_xpath").getText().trim());
			values.put("numberOfNights", 							getElement("label_number_of_nights_xpath").getText().trim());
			values.put("customerName", 								getElement("label_customer_name_xpath").getText().trim());
			values.put("checkin/checkout", 							getElement("label_checkin_checkout_xpath").getText().trim());
			values.put("bookingDate", 								getElement("label_booking_date_xpath").getText().trim());
			values.put("roomType", 									getElement("label_room_type_xpath").getText().trim());
			values.put("bedType", 									getElement("label_bed_type_xpath").getText().trim());
			values.put("ratePlan", 									getElement("label_rateplan_xpath").getText().trim());
			values.put("sellingCurrecny", 							getElement("label_selling_currency_xpath").getText().trim());
			values.put("orderValue", 								getElement("label_order_value_xpath").getText().trim());
			values.put("totalCancellationCharge", 					getElement("label_total_cancellation_charge_xpath").getText().trim());
			values.put("additionalCharge", 							getElement("label_additional_charge_xpath").getText().trim());
			values.put("orderValueInPortalCurrecny", 				getElement("label_order_value_in_portal_currency_xpath").getText().trim());
			values.put("totalCancellationFeeInPortalCurrency", 		getElement("label_total_cancellation_charge_in_portal_currency_xpath").getText().trim());
			values.put("orderValuePageTotal", 						getElement("label_order_value_page_total_xpath").getText().trim());
			values.put("totalCancellationFeePageTotal", 			getElement("label_total_cancellation_fee_page_total_xpath").getText().trim());
			values.put("orderValueGrandTotal", 						getElement("label_order_value_grand_total_xpath").getText().trim());
			values.put("totalCancellationFeeGrandTotal",	 		getElement("label_total_cancellation_fee_grand_total_xpath").getText().trim());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
	}
}
