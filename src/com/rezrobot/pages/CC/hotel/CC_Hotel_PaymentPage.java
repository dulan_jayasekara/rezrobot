package com.rezrobot.pages.CC.hotel;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.util.HSSFColor.SEA_GREEN;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.utill.Calender;

public class CC_Hotel_PaymentPage extends PageObject{
	
	public boolean isAvailabile()
	{
		try {
			return getElementVisibility("inputfield_activate_manager_override_user_id_id", 30);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public HashMap<String, String> getValues(HotelSearchObject search) throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		if(search.getOnlineOrOffline().equals("offline"))
		{
			getElement("radiobutton_payment_pay_offline_id").click();
			Thread.sleep(5000);
		}
		values.put("hotelName", 									getElement("label_hotel_name_id").getText());
		values.put("address", 										getElement("label_hotel_address_id").getText());
		values.put("checkin", 										getElement("label_hotel_checkin_id").getText());
		values.put("checkout", 										getElement("label_hotel_checkout_id").getText());
		values.put("nights", 										getElement("label_hotel_nights_id").getText());
		values.put("rooms", 										getElement("label_hotel_rooms_id").getText());
		values.put("bookingStatus", 								getElement("label_hotel_status_id").getText());
		String[] canDeadline = getElement("label_hotel_cancellation_deadline_id").getText().split(":");
		
		values.put("cancellationDeadline", 							canDeadline[1].trim());
		for(int i = 1 ; i <= Integer.parseInt(search.getRooms()) ; i++)
		{
			values.put("room" 		+ String.valueOf(i), 			getElement("label_hotel_room_id").changeRefAndGetElement(getElement("label_hotel_room_id").getRef().replace("replace", String.valueOf(i))).getText());
		//	System.out.println(getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			values.put("roomType" 	+ String.valueOf(i), 			getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			values.put("bedType" 	+ String.valueOf(i), 			getElement("label_hotel_bed_type_id").changeRefAndGetElement(getElement("label_hotel_bed_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			values.put("ratePlan" 	+ String.valueOf(i), 			getElement("label_hotel_rate_plan_id").changeRefAndGetElement(getElement("label_hotel_rate_plan_id").getRef().replace("replace", String.valueOf(i))).getText());
			values.put("totalRate" 	+ String.valueOf(i), 			getElement("label_hotel_total_rate_id").changeRefAndGetElement(getElement("label_hotel_total_rate_id").getRef().replace("replace", String.valueOf(i))).getText());
		}
		values.put("hotelSubTotal", 								getElement("label_hotel_subtotal_id").getText());
		values.put("hotelTax", 										getElement("label_hotel_total_tax_and_other_charges_id").getText());
		values.put("hotelTotal", 									getElement("label_hotel_total_hotel_booking_value").getText());
		String[] temp = getElement("label_currency_code_xpath").getText().split("\\(");
		temp = temp[1].split("\\)");
		
		values.put("currencyCode", 									temp[0]);
		values.put("totalGrossPackageBookingValue", 				getElement("label_total_gross_package_booking_value_id").getText());
		values.put("totalTaxesAndOtherCharges", 					getElement("label_total_taxes_and_other_charges_id").getText());
		values.put("totalBookingValue", 							getElement("label_total_booking_value_id").getText());
		values.put("amountBeingProcessedNow", 						getElement("label_amount_being_processed_now").getText());
		values.put("amountDueAtCheckin", 							getElement("label_amount_due_at_checkin_xpath").getText());
		temp = getElement("label_payment_pg_currency_id").getText().split("\\(");
		temp = temp[1].split("\\)");
		values.put("pgCurrency", 									temp[0]);
		values.put("pgTotalGrossPackageBookingValue", 				getElement("label_payment_total_gross_package_booking_value_id").getText());
		values.put("pgTotalTaxes", 									getElement("label_payment_total_taxes_and_fees_id").getText());
		values.put("pgTotalPackageBookingValue", 					getElement("label_payment_total_package_booking_value_id").getText());
		values.put("pgAmountBeingProcessedNow", 					getElement("label_payment_amount_being_processed_now_id").getText());
		values.put("pgAmountDueAtCheckin", 							getElement("label_payment_amount_due_at_checkin_id").getText());
		values.put("hotelCancellationDeadline", 					getElement("label_hotel_cancellation_deadline_id").getText());
		return values;
		
	}
	
	public void enterDetails(HashMap<String, String> portalSpecificData, ArrayList<HotelOccupancyObject> occupancyList, HotelSearchObject search, HotelDetails selectedHotel) throws Exception
	{
		System.out.println("Fill payment page");
		getElement("inputfield_customer_details_first_name_id").setText(portalSpecificData.get("cd.firstname"));
		getElement("inputfield_customer_details_tel_area_code_id").setText(portalSpecificData.get("cd.phoneNumberCode"));
		getElement("inputfield_customer_details_tel_number_id").setText(portalSpecificData.get("cd.phoneNumber"));
		getElement("inputfield_customer_details_email_id").setText(portalSpecificData.get("cd.hotelEmail"));
		getElement("inputfield_customer_details_address_id").setText(portalSpecificData.get("cd.address"));
		getElement("inputfield_customer_details_emergency_contact_number_id").setText(portalSpecificData.get("cd.mobileNumberCode").concat(portalSpecificData.get("cd.mobilenumber")));
		getElement("inputfield_customer_details_last_name_id").setText(portalSpecificData.get("cd.lastname"));
		((ComboBox) getElement("combobox_customer_details_country_id")).selectOptionByText(portalSpecificData.get("cd.country"));
		getElement("inputfield_customer_details_city_id").setText(portalSpecificData.get("cd.city"));
		getElement("image_customer_details_city_lookup_id").click();
		try {
			getElementVisibility("link_customer_details_city_lookup_first_record_xpath", 10);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Thread.sleep(5000);
		getElement("link_customer_details_city_lookup_first_record_xpath").click();
		
		String[] adultCnt = search.getAdults().split("/");
		String[] childCnt = search.getChildren().split("/");
		int adult = 0 ;
		int child = 0;
		for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
		{
			try {
				for(int j = 0 ; j < Integer.parseInt(adultCnt[i]) ; j++ )
				{
					System.out.println(adultCnt[j]);
					if(i == 0 && j == 0)
					{
						getElement("inputfield_adult_occupancy_hotel_first_name_id").changeRefAndGetElement(getElement("inputfield_adult_occupancy_hotel_first_name_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).clear();
						getElement("inputfield_adult_occupancy_hotel_last_name_id").changeRefAndGetElement(getElement("inputfield_adult_occupancy_hotel_last_name_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).clear();
					}
					getElement("inputfield_adult_occupancy_hotel_first_name_id").changeRefAndGetElement(getElement("inputfield_adult_occupancy_hotel_first_name_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).setText(occupancyList.get(adult).getAdultFirstName());
					getElement("inputfield_adult_occupancy_hotel_last_name_id").changeRefAndGetElement(getElement("inputfield_adult_occupancy_hotel_last_name_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).setText(occupancyList.get(adult).getAdultLastName());
					if(occupancyList.get(adult).getAdultSmoking().equals("y"))
					{
						getElement("radiobutton_adult_occupancy_hotel_smoking_yes_id").changeRefAndGetElement(getElement("radiobutton_adult_occupancy_hotel_smoking_yes_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).click();
					}
					else
					{
						getElement("radiobutton_adult_occupancy_hotel_smoking_no_id").changeRefAndGetElement(getElement("radiobutton_adult_occupancy_hotel_smoking_no_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).click();
					}
					if(occupancyList.get(adult).getAdultWheelchari().equals("y"))
					{
						getElement("checkbox_adult_occupancy_hotel_wheelchair_id").changeRefAndGetElement(getElement("checkbox_adult_occupancy_hotel_wheelchair_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).click();
					}
					if(occupancyList.get(adult).getAdultHandicap().equals("y"))
					{
						getElement("checkbox_adult_occupancy_hotel_handicap_id").changeRefAndGetElement(getElement("checkbox_adult_occupancy_hotel_handicap_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).click();
					}
					adult++;
					System.out.println(j);
				}
				
				for(int j = 0 ; j < Integer.parseInt(childCnt[i]) ; j++ )
				{
					getElement("inputfield_child_occupancy_hotel_first_name_id").changeRefAndGetElement(getElement("inputfield_child_occupancy_hotel_first_name_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).setText(occupancyList.get(child).getChildFirstName());
					getElement("inputfield_child_ccupancy_hotel_last_name_id").changeRefAndGetElement(getElement("inputfield_child_ccupancy_hotel_last_name_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).setText(occupancyList.get(child).getChildLastName());
					if(occupancyList.get(child).getAdultWheelchari().equals("y"))
					{
						getElement("checkbox_child_occupancy_hotel_wheelchair_id").changeRefAndGetElement(getElement("checkbox_child_occupancy_hotel_wheelchair_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).click();
					}
					if(occupancyList.get(child).getAdultHandicap().equals("y"))
					{
						getElement("checkbox_child_occupancy_hotel_handicap_id").changeRefAndGetElement(getElement("checkbox_child_occupancy_hotel_handicap_id").getRef().replace("replace1", String.valueOf(i)).replace("replace2", String.valueOf(j))).click();
					}
					child++;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		getElement("inputfield_notes_customer_notes_id").setText(portalSpecificData.get("customerNotes"));
		getElement("inputfield_notes_internal_notes_id").setText(portalSpecificData.get("internalNotes"));
		getElement("inputfield_notes_customer_notes_id").changeRefAndGetElement(getElement("inputfield_notes_customer_notes_id").getRef().replace("hotelID", selectedHotel.getHotelID()).replace("checkin", Calender.getDate("MM/dd/yyyy", "yyyy-MMM-dd", search.getCheckin())).replace("supplier", search.getSupplier())).setText(portalSpecificData.get("hotelNotes"));
		getElement("checkbox_read_cancellation_policy_confirm_id").click();
		getElement("button_continue_id").click();
		
		try {
			getElementVisibility("button_proceed_classname", 30);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			Thread.sleep(5000);
			getElement("button_proceed_classname").click();
		} catch (Exception e) {
			// TODO: handle exception
			try {
				Thread.sleep(5000);
				getElement("button_proceed_xpath").click();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		
	}
}
