package com.rezrobot.pages.CC.hotel;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.rezrobot.core.PageObject;
import com.rezrobot.utill.DriverFactory;

public class CC_Hotel_CancellationPage extends PageObject{

	public HashMap<String, String> getValues() throws Exception
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			switchToDefaultFrame();
			values.put("customerName", 								getElement("inputfield_customer_name_id").getText().replace(":", "").trim());
			values.put("resNumber", 								getElement("label_reservation_number_id").getText().replace(":", "").trim());
			values.put("bookingDate", 								getElement("label_booking_date_id").getText().replace(":", "").trim());
			values.put("toName", 									getElement("label_b2b_name_id").getText().replace(":", "").trim());
			values.put("bookingChannel", 							getElement("label_booking_channel_id").getText().replace(":", "").trim());
			values.put("subTotal", 									getElement("label_sub_total_id").getText().replace(":", "").trim());
			values.put("totalPayable", 								getElement("label_total_payable_id").getText().replace(":", "").trim());
			values.put("hotelName", 								getElement("label_hotel_name_id").getText().replace(":", "").trim());
			values.put("address", 									getElement("label_address_id").getText().replace(":", "").trim());
			values.put("arrivalDate", 								getElement("label_arrival_date_id").getText().replace(":", "").trim());
			values.put("departureDate", 							getElement("label_departure_date_id").getText().replace(":", "").trim());
			values.put("adultCount", 								getElement("label_adult_count_id").getText().replace(":", "").trim());
			values.put("supplierName", 								getElements("label_supplier_name").get(1).getText().replace(":", "").trim());
			values.put("location", 									getElement("label_location_id").getText().replace(":", "").trim());
			values.put("nightsCount", 								getElement("label_nights_count_id").getText().replace(":", "").trim());
			values.put("roomCount", 								getElement("label_room_count_id").getText().replace(":", "").trim());
			values.put("supConfirmationNumber", 					getElements("label_sup_confirmation_id_").get(1).getText().replace(":", "").trim());
			int cnt = 1;
			for(int i = 1 ; i <= 4 ; i++)
			{
				for(int j = 0 ; j < getElement("label_occupancy_room_number_id").changeRefAndGetElement(getElement("label_occupancy_room_number_id").getRef().replace("replace", String.valueOf(i))).getWebElements().size() ; j++)
				{
					values.put("occupancyRoomNumber"	+ cnt, 		getElement("label_occupancy_room_number_id").changeRefAndGetElement(getElement("label_occupancy_room_number_id").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyTitleID"		+ cnt, 		getElement("label_occupancy_title_id").changeRefAndGetElement(getElement("label_occupancy_title_id").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyLastName"		+ cnt, 		getElement("label_occupancy_lastname_id").changeRefAndGetElement(getElement("label_occupancy_lastname_id").getRef().replace("replace", String.valueOf(i))).getText());
					values.put("occupancyFirstName"		+ cnt, 		getElement("label_occupancy_firstname_id").changeRefAndGetElement(getElement("label_occupancy_firstname_id").getRef().replace("replace", String.valueOf(i))).getText());
					cnt++;
				}
				
			}
			cnt = 1;
			for(int i = 0 ; i < getElement("label_room_type_id").getWebElements().size() ; i++)
			{
				values.put("roomType" 		+ cnt, 					getElement("label_room_type_id").getWebElements().get(i).getText().trim());
				values.put("bedAndBoard" 	+ cnt, 					getElement("label_bed_and_board_id").getWebElements().get(i).getText().trim());
				values.put("adultCount" 	+ cnt, 					getElement("label_adults_count_id").getWebElements().get(i).getText().trim());
				values.put("childrenCount" 	+ cnt, 					getElement("label_children_count_id").getWebElements().get(i).getText().trim());
				values.put("roomRate" 		+ cnt, 					getElement("label_room_rate_id").getWebElements().get(i).getText().trim());
				values.put("supplimentary" 	+ cnt, 					getElement("label_supplimentary_id").getWebElements().get(i).getText().trim());
				values.put("roomTotal" 		+ cnt, 					getElement("label_room_total_id").getWebElements().get(i).getText().trim());
				cnt++;
			}
			values.put("supplierCancellationCharge", 				getElement("inputfield_supplier_cancellation_charge_id").getAttribute("value"));
			values.put("customerCancellationCharge", 				getElement("inputfield_customer_cancellation_charge_id").getAttribute("value"));
			values.put("totalSupplierCancellationCharge", 			getElement("inputfield_total-supplier_cancellation_charge_id").getAttribute("value"));
			values.put("totalCustomerCancellationCharge", 			getElement("inputfield_total-customer_cancellation_charge_id").getAttribute("value"));
			values.put("additionalCancellationCharge", 				getElement("inputfield_additional_cancellation_charge_id").getAttribute("value"));
			values.put("gstCharge", 								getElement("inputfield_gst_charge_id").getAttribute("value"));
			values.put("totalCancellationCharge", 					getElement("inputfield_total_charge_id").getAttribute("value"));
			try {
				System.out.println(getElements("label_currency_code_classname").size());
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			
			values.put("currencyCode",   							getElement("label_currency_code_classname").getText());
			getElement("button_save_id").click();
			
			values.put("cancellationStatus", 						getElement("label_cancellation_status_id").getText());
			getElement("button_cancellation_status_xpath").click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return values;
		
	}
}
