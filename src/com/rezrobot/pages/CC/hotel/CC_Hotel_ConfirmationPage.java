package com.rezrobot.pages.CC.hotel;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelRoomDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.utill.Calender;

public class CC_Hotel_ConfirmationPage extends PageObject{

	public boolean isAvailable()
	{
		try {
			switchToDefaultFrame();
			switchToFrame("frame_live_msg_frame_id");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			return getElementVisibility("label_reservation_number_id", 60);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public HotelConfirmationPageDetails getValues(HotelSearchObject search, HotelDetails selectedHotel) throws Exception
	{
		
		HashMap<String, String> 		values 	= new HashMap<>();
		HotelConfirmationPageDetails 	info 	= new HotelConfirmationPageDetails();
		String[] 						temp 	= getElement("label_reservation_number_id").getText().split(":");
		info.setReservationNumber	(temp[1].trim());
		info.setStatus				(getElement("label_hotel_booking_status_id").getText());
		String[] hotelName = getElement("label_hotel_hotel_name_id").getText().split("View");
		info.setHotelName(hotelName[0].trim());
		info.setHotelAddress(getElement("label_hotel_address_id").getText());
		info.setChekin(getElement("label_hotel_checkin_id").getText());
		info.setCheckout(getElement("label_hotel_checkout_id").getText());
		info.setNumberOfNights(getElement("label_hotel_nights_id").getText());
		info.setNumberOfRooms(getElement("label_hotel_number_of_rooms_id").getText());
		//values.put("bookingStatus", 					getElement("label_hotel_status_id").getText());
		info.setSupplierConfirmationNumber(getElement("label_sup_confirmation_number_label").getText());
		try {
			info.setCancellationDeadline(getElement("label_hotel_can_deadline_label").changeRefAndGetElement(getElement("label_hotel_can_deadline_label").getRef().replace("hotelID", selectedHotel.getHotelID()).replace("checkin", Calender.getDate("MM/dd/yyyy", "yyyy-MMM-dd", search.getCheckin())).replace("supplier", search.getSupplier())).getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		ArrayList<HotelRoomDetails> roomList = new ArrayList<HotelRoomDetails>();
		for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
		{
			HotelRoomDetails room = new HotelRoomDetails();
			room.setRoom(getElement("label_hotel_rooms_id").changeRefAndGetElement(getElement("label_hotel_rooms_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setRoomType(getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setBedType(getElement("label_hotel_bed_type_id").changeRefAndGetElement(getElement("label_hotel_bed_type_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setRate(getElement("label_hotel_rate_plan_id").changeRefAndGetElement(getElement("label_hotel_rate_plan_id").getRef().replace("replace", String.valueOf(i))).getText());
			room.setRate(getElement("label_hotel_total_rate_id").changeRefAndGetElement(getElement("label_hotel_total_rate_id").getRef().replace("replace", String.valueOf(i))).getText());
			roomList.add(room);
		}
		info.setRoom(roomList);
		info.setHotelTax(getElement("label_hotel_total_tax_and_service_charges_id").getText());
		info.setHotelSubtotal(getElement("label_hotel_subtotal_id").getText());
		info.setHotelTotal(getElement("label_hotel_total_id").getText());
		info.setSubtotal(getElement("label_total_gross_booking_value_id").getText());
		info.setTax(getElement("label_total_tax_and_other_charges_id").getText());
		info.setTotal(getElement("label_the_total_package_booking_value_id").getText());
		System.out.println(getElement("label_amount_being_processed_now_xpath").getRef());
		info.setAmountBeingProcessedNow(getElement("label_amount_being_processed_now_xpath").getText());
		info.setAmountDueAtCheckin(getElement("label_amount_due_at_checkin_xpath").getText());
		info.setFirstName(getElement("label_customer_details_first_name_id").getText());
		info.setLastName(getElement("label_customer_details_last_name_id").getText());
		info.setPhoneNumber(getElement("label_customer_details_phone_number_id").getText());
		info.setEmail(getElement("label_customer_details_email1_id").getText());
		info.setAddress1(getElement("label_customer_details_address1_id").getText());
		info.setCountry(getElement("label_customer_details_country_id").getText());
		info.setCity(getElement("label_customer_details_city_id").getText());
		info.setEmergencyNumber(getElement("label_customer_details_emergency_number_id").getText());
		//values.put("fax", 								getElement("label_customer_details_fax_id").getText());
		//values.put("email2", 							getElement("label_customer_details_email2_id").getText());
		//values.put("address2", 							getElement("label_customer_details_address2_id").getText());
		info.setMerchantTrackId(getElement("label_online_payment_details_merchant_track_id_id").getText());
		info.setPaymentID(getElement("label_online_payment_details_payment_idid").getText());
		info.setAuthenticationReference(getElement("label_online_payment_details_aithentication_reference_id").getText());
		info.setAmountInPGCurrency(getElement("label_online_payment_details_amount_id").getText());
		
		return info;
	}
}
