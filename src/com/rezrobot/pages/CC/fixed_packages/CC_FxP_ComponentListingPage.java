package com.rezrobot.pages.CC.fixed_packages;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;

public class CC_FxP_ComponentListingPage extends PageObject{
	
	
	public HashMap<String, String> RateReaders() throws Exception {
		
		HashMap<String, String> Ratemap=new HashMap<>();
		
		System.out.println(getElement("txt_packageSummery_id").getText());
		
		Ratemap.put("PackageSummery", getElement("txt_packageSummery_id").getText());
		Ratemap.put("Subtotal", getElement("txt_subtotal_id").getText());
		Ratemap.put("tax", getElement("txt_tax_id").getText());
		Ratemap.put("total", getElement("txt_total_id").getText());

		
		return Ratemap;
		
		
	}
	
	public CC_FxP_PaymentPage ProceedTonext() throws Exception {
		
		getElement("btn_Booknw_id").click();
		CC_FxP_PaymentPage PaymentPage=new CC_FxP_PaymentPage();
		return PaymentPage;
		
		
		
	}
	
	public void AirReader() {
		
	}
	public void HotelReader() {
		
	}
	public void ActivityReader() throws Exception {
		
		//switchToFrame("frame_mainframe_id");
		
		System.out.println("ACT************"+getElementVisibility("btn_activityResults_xpath"));
		System.out.println(getElement("btn_activityResults_xpath").getRef());
		
		getElement("btn_hotelResults_xpath").click();

		getElement("btn_activityResults_xpath").click();
		
		
		
		try {
			UIElement Activityx= new UIElement();
			Activityx.setElementProperties("Activityx", "3", ".//*[@id='pkg_dv_WJ_1']/div[1]/div[5]/div/table/tbody/tr[2]/td[1]");
			addTomap(Activityx);
			
			System.out.println(getElement("Activityx").getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
	}

}
