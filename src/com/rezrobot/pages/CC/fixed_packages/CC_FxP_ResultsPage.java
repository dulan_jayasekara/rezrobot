package com.rezrobot.pages.CC.fixed_packages;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;

public class CC_FxP_ResultsPage extends PageObject {


	int totalPackageCount;
	int totalPageCount;
	int packagePage;
	int packageIndex;
	String PackageID;

	public void pacakgenavigate(String PackageName) throws Exception {

		switchToDefaultFrame();
		switchToFrame("frame_mainframe_id");

		addobjects();


		totalPackageCount=Integer.parseInt(getElement("txt_paginationText_xpath").getText().split("of")[1].trim());
		totalPageCount=(totalPackageCount/10)+1;
		int t=1;
		for (int j = 1; j < (totalPageCount+1); j++) {


			for (int i = 1; i <= 10; i++) {

				try {
					UIElement packageElement=new UIElement();
					packageElement.setElementProperties("div_packageblock"+t+"_xpath", "3", ".//*[@id='pkg_dv_WJ_1']/table["+i+"]");
					objectMap.put(packageElement.getKey(), packageElement);
					UIElement packagetitle=new UIElement();
					packagetitle.setElementProperties("txt_pkg"+(t)+"Title_xpath", "3", "/html/body/div[5]/div[3]/div[1]/div/div/div[2]/div[2]/table["+i+"]/tbody/tr[1]/th/p");
					objectMap.put(packagetitle.getKey(), packagetitle);
					System.out.println(packagetitle.getText());
					if (packagetitle.getText().contains(PackageName)) {

						packagePage=j;
						packageIndex=i;



					




					}


				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		}


	}
	
	public CC_FxP_ComponentListingPage readPackage() throws Exception {
		
		
		executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+packagePage+"},'','WJ_2'))");
		
		UIElement fromdate=new UIElement();
		fromdate.setElementProperties("txt_fromdate_xpath", "3", "html/body/div[5]/div[3]/div[1]/div/div/div[2]/div[2]/table["+packageIndex+"]/tbody/tr[3]/td/table/tbody/tr/td[1]/div[1]/i");

		addTomap(fromdate);
		UIElement todate=new UIElement();
		todate.setElementProperties("txt_todate_xpath", "3", "html/body/div[5]/div[3]/div[1]/div/div/div[2]/div[2]/table["+packageIndex+"]/tbody/tr[3]/td/table/tbody/tr/td[1]/div[2]/i");
		addTomap(todate);
		readmoreinfo(packageIndex);		//	moreinfoDetailRead();
		
CC_FxP_ComponentListingPage CLPage=availabilitySearch();

		
		
		return CLPage;
		
		
	}

	public void readmoreinfo(int i) throws Exception {

		UIElement moreinfo=new UIElement();
		moreinfo.setElementProperties("lnk_moreinfo_xpath", "3", "html/body/div[5]/div[3]/div[1]/div/div/div[2]/div[2]/table["+i+"]/tbody/tr[3]/td/table/tbody/tr/td[2]/a");
		objectMap.put(moreinfo.getKey(), moreinfo);
		PackageID=moreinfo.getAttribute("id").replace("pkg_info_", "");
		System.out.println(PackageID);




	}

	public void moreinfoDetailRead() {

		executejavascript("result_wjt_WJ_1.moreInfo("+PackageID+",false)");

	}

	public CC_FxP_ComponentListingPage availabilitySearch() throws Exception {

		executejavascript("result_wjt_WJ_1.toggleDiv('"+PackageID+"')");
		UIElement packagedate=new UIElement();
		String packageDateref="packagedate"+PackageID;
		System.out.println(packageDateref);
		packagedate.setElementProperties("input_PckageDate_id", "1", packageDateref);
		addTomap(packagedate);
	System.out.println(getElement("input_PckageDate_id").getAttribute("value"));
		setPackageDate("08-25-2016");
		
		UIElement searchbtn=new UIElement();
		String searchbtnref="searchpkg_"+PackageID;
		System.out.println("searchbtnref"+searchbtnref);
		searchbtn.setElementProperties("btn_SearchBtn_id", "1", searchbtnref);
		addTomap(searchbtn);
		getElement("btn_SearchBtn_id").click();
		
		CC_FxP_ComponentListingPage CLPage=new CC_FxP_ComponentListingPage();
		
		switchToDefaultFrame();
		switchToFrame("frame_mainframe_id");
		addobjects();
		/*try {
			switchToDefaultFrame();
			switchToFrame("frame_mainframe_id");
			addobjects();
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		
		
		return CLPage;



	}
	public String getPackageDateset() {

		UIElement packageDate=new UIElement();
		packageDate.setElementProperties("input_packageDate_", "3", ".//*[@id='pkg_dv_WJ_1']/table["+packageIndex+"]/tbody/tr[4]/td/table/tbody/tr/td/div[1]/div[1]");
		return PackageID;

	}

	public void AvailabilityCheckLabelReaders() {
		UIElement packageDatelabel=new UIElement();
		packageDatelabel.setElementProperties("lbl_packageDate_xpath", "3", ".//*[@id='pkg_dv_WJ_1']/table["+packageIndex+"]/tbody/tr[4]/td/table/tbody/tr/td/div[1]/div[1]");
		addTomap(packageDatelabel)	;	
		UIElement cabinclasslabel=new UIElement();
		cabinclasslabel.setElementProperties("lbl_cabinclass_xpath", "3", ".//*[@id='pkg_dv_WJ_1']/table["+packageIndex+"]/tbody/tr[4]/td/table/tbody/tr/td/div[1]/div[2]/div[2]/div[1]");
		addTomap(cabinclasslabel);
		UIElement roomslabel=new UIElement();
		roomslabel.setElementProperties("lbl_roomlabel_xpath", "3", "html/body/div[5]/div[3]/div[1]/div/div/div[2]/div[2]/table["+packageIndex+"]/tbody/tr[4]/td/table/tbody/tr/td/div[1]/div[3]/div[1]/div[1]/label");
		addTomap(roomslabel);

		for (int i = 1; i < 5; i++) {

			UIElement roomlabel_i=new UIElement();
			roomlabel_i.setElementProperties("lbl_Room"+i+"_xpath", "3", ".//*[@id='rm"+i+PackageID+"']/div[1]");
			addTomap(roomlabel_i);

		}





	}

	public void setPackageDate(String SearchDate) {
		System.out.println("$('#packagedate"+PackageID+"').val('"+SearchDate+"');");
		try {
			executejavascript("$('#packagedate"+PackageID+"').val('"+SearchDate+"');");

		} catch (Exception e) {
			// TODO: handle exception
		}


	}




	
	

}
