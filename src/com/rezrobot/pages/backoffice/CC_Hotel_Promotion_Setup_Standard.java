package com.rezrobot.pages.backoffice;

import org.openqa.selenium.By;

import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.PromotionSetupdetails;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;

public class CC_Hotel_Promotion_Setup_Standard extends PageObject {

	public void setuppromo(PromotionSetupdetails details) throws Exception {

		if (details.getSelectIOption().equalsIgnoreCase("create")) {

			getElement("radiobutton_Createpromo_xpath").click();

			// getElement("inputfield_hotel_name_id").setText(details.getHotelName());

			getElement("inputfield_promotion_code_id").setText(details.getPromotionCode());
			getElement("inputfield_promotion_name_id").setText(details.getPromotionName());
			
			if(details.getAddDescription().equalsIgnoreCase("yes")){
				
				getElementClick("button_Add_discription_id");
				getElement("inputfield_discription_id").setText(details.getDescriptionText());
				
			}
			
			if(details.getAddMultilingual().equalsIgnoreCase("yes")){
				
//				getElementClick("button_Add_multilingual_id");
//				selectOptionByText("comboBox_Language_id", details.getLanguage());
//				getElement("inputfield_mul_promoName_id").setText(details.getMulPromoName());
//				getElement("inputfield_mul_description_id").setText(details.getMulDescrepscrition());
				
				
				
				
			}
			
			if(details.getAddDescription().equalsIgnoreCase("yes")||details.getAddMultilingual().equalsIgnoreCase("yes")){
				
				getElementClick("button_Add_mul_id");
			}
			
			
			

			if (details.getPromotionType().equalsIgnoreCase("kidsfree")) {

				getElement("checkbox_promotion_Type_ChildFreeES_id").click();

			}

			else if (details.getPromotionType().equalsIgnoreCase("discount")) {

				getElement("checkbox_promotion_Type_DiscountES_id").click();

			}

			else if (details.getPromotionType().equalsIgnoreCase("freenights") || details.getPromotionType().equalsIgnoreCase("freenight")) {

				getElement("checkbox_promotion_Type_FreeNightsES_id").click();

			}

			else if (details.getPromotionType().equalsIgnoreCase("specialrates")) {

				getElement("checkbox_promotion_Type_SpecialRateES_id").click();

			} else if (details.getPromotionType().equalsIgnoreCase("Valueadded")) {

				getElement("checkbox_promotion_Type_ValueaddedES_id").click();

			}

			if (details.getPromotionStatus().equalsIgnoreCase("Active")) {

				getElement("radiobutton_Prmostatus_Active_id").click();

			}

			else {
				getElement("radiobutton_Prmostatus_inactive_id").click();

			}
			
			
			
			
			
			
			
			
			
			getElementClick("button_Promotion Condition_section_xpath");
			
			
			
			
			

		}

		else if (details.getSelectIOption().equalsIgnoreCase("modify")) {

			getElement("radiobutton_Modifypromo_xpath").click();

		}

		else if (details.getSelectIOption().equalsIgnoreCase("delete")) {

			getElement("radiobutton_Deletepromo_xpath").click();

		}

	}

}
