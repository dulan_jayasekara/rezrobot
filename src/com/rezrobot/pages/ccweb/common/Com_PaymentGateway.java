package com.rezrobot.pages.ccweb.common;

import com.rezrobot.core.PageObject;
import com.rezrobot.pages.web.activity.Web_Act_ConfirmationPage;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.processor.LabelReadProperties;

public class Com_PaymentGateway extends PageObject{
	
	private String paymentGatewayName;
	
	public boolean isPaymentGatewayLoaded(){
		
		LabelReadProperties labelProperty = new LabelReadProperties();
		paymentGatewayName = labelProperty.getPortalSpecificData().get("PaymentGateway_CardType");
				
		if (paymentGatewayName.equalsIgnoreCase("rezPay")) {
			
			try {
				switchToDefaultFrame();
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				
				getElementVisibility("inputfield_RezPaY_Card1_id", 300);				
				return true;		
			} catch (Exception e) {
				return false;
			}
			
		}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
			
			try {
				switchToDefaultFrame();		
				try {
					if(getelementAvailability("frame_PaymentGateWayFrame_id"))
					{
						switchToFrame("frame_PaymentGateWayFrame_id");
					}
				} catch (Exception e) {
					
				}
				getElementVisibility("inputfield_wireCard_FirstName_id", 300);
				return true;
			} catch (Exception e) {
				return false;
			}
				
		}
		return false;
		
	}
	
	public void enterCardDetails(){
		
		try {
			
			if (paymentGatewayName.equalsIgnoreCase("rezPay")) {
				
				getElement("inputfield_RezPaY_Card1_id").setJavaScript("$('#cardnumberpart1').val('4000');");	
				getElement("inputfield_RezPaY_Card2_id").setJavaScript("$('#cardnumberpart2').val('0000');");	
				getElement("inputfield_RezPaY_Card3_id").setJavaScript("$('#cardnumberpart3').val('0000');");	
				getElement("inputfield_RezPaY_Card4_id").setJavaScript("$('#cardnumberpart4').val('0002');");	
				((ComboBox)getElement("combobox_RezPaY_ExpMonth_id")).selectOptionByText("02");
				((ComboBox)getElement("combobox_RezPaY_ExpYear_id")).selectOptionByText("2017");
				getElement("inputfield_RezPaY_CardHolderName_id").setText("Daniel");
				getElement("inputfield_RezPaY_CVVNo_id").setText("123");
									
			}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
				
				getElement("inputfield_wireCard_FirstName_id").setText("Daniel");
				getElement("inputfield_wireCard_LastName_id").setText("OBrien");
				getElementClick("button_wireCard_CardTypeSelectButton_id");
				getElementClick("button_wireCard_VisaType_id");
				getElement("inputfield_wireCard_AccountNo_id").setText("4000000000000002");
				getElement("inputfield_wireCard_CVVNo_id").setText("111");
				((ComboBox)getElement("combobox_wireCard_ExpMonth_id")).selectOptionByText("02");
				((ComboBox)getElement("combobox_wireCard_ExpYear_id")).selectOptionByText("2017");	
				
			}
			else if(paymentGatewayName.equalsIgnoreCase("flocash")){
				
				//FLOCASH PAYMENTGATEWAY INPUTS
				getElement("inputfield_flocash_FirstName_id").setText("Daniel");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void confirm() throws Exception
	{
		LabelReadProperties 	labelProperty 			= new LabelReadProperties();
								paymentGatewayName 		= labelProperty.getPortalSpecificData().get("PaymentGateway_CardType");
		if(paymentGatewayName.equalsIgnoreCase("rezPay"))
		{
			getElement("button_RezPaY_ProceedButton_id").click();
		}
		else if(paymentGatewayName.equalsIgnoreCase("wireCard"))
		{
			getElement("button_wireCard_SubmitButton_id").click();
		}
	}
	
	public Web_Act_ConfirmationPage loadActivityConfirmationPage() throws Exception{
		
		try {
			
			if (paymentGatewayName.equalsIgnoreCase("rezPay")) {				
				getElementClick("button_RezPaY_ProceedButton_id");
				Web_Act_ConfirmationPage aConfirmationPage = new Web_Act_ConfirmationPage();
				return aConfirmationPage;
				
			}else if(paymentGatewayName.equalsIgnoreCase("wireCard")){
				getElementClick("button_wireCard_SubmitButton_id");
				Web_Act_ConfirmationPage aConfirmationPage = new Web_Act_ConfirmationPage();
				return aConfirmationPage;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
