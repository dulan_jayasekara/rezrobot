package com.rezrobot.processor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.rezrobot.utill.ReadProperties;

public class LabelReadProperties {
	
	private static  String RELATIVE_URL;
	private static FileReader reader;
	private static HashMap<String, String>  	labelsMap 			= new HashMap<String, String>();

	public static void main(String[] args) {
		LabelReadProperties prop=new LabelReadProperties();
		System.out.println(prop.becLabelFPConstructor().get("Tourplanlabel"));
		System.out.println(prop.getPortalSpecificData().get("Portal.Email"));
	}

	public static HashMap<String, String> getProperty() {
		Properties prop = new Properties();

		try {
			reader = new FileReader(new File(RELATIVE_URL));
			prop.load(reader);
			for (String key : prop.stringPropertyNames()) 
			{
				String value 		= prop.getProperty(key);
				labelsMap.put(key, value);
				//System.out.println(key);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return labelsMap;
	}
	
	public HashMap<String, String> getPortalSpecificData()
	{
		RELATIVE_URL = "../Rezrobot_Details/Common/portalSpecificData/portalInfo.properties";
		HashMap<String, String> labels = new HashMap<String, String>();
		labels = getProperty();
		return labels;
	}
	
	public HashMap<String, String> getHotelLabelProperties()
	{
		RELATIVE_URL = "../Rezrobot_Details/Common/LabelProperties/hotelLabels.properties";
		HashMap<String, String> labels = new HashMap<String, String>();
		labels = getProperty();
		return labels;
	}
	
	public HashMap<String, String> getDMCLabelProperties()
	{
		RELATIVE_URL = "../Rezrobot_Details/Common/LabelProperties/DMCPackageLabels.properties";
		HashMap<String, String> labels = new HashMap<String, String>();
		labels = getProperty();
		return labels;
	}
	
	public HashMap<String, String> getConfigProperties()
	{
		RELATIVE_URL = "../Rezrobot_Details/Common/Config.properties";
		HashMap<String, String> labels = new HashMap<String, String>();
		labels = getProperty();
		return labels;
	}
	
	
	
	public HashMap<String, String> getActivityLabelProperties()
	{
		RELATIVE_URL = "../Rezrobot_Details/Common/LabelProperties/ActivityLabels.properties";
		HashMap<String, String> activityLabels = new HashMap<String, String>();
		activityLabels = getProperty();
		return activityLabels;
	}
		
	public HashMap<String, String> becLabelFPConstructor() {
		
		HashMap<String, String> labels = new HashMap<String, String>();
		
		RELATIVE_URL="Resources\\LabelProperties\\fixedPackageLabels.properties";
		
		labels=getProperty();
		
	
		return labels;
		
	}
	public HashMap<String, String> getCommonLabels()
	{
		RELATIVE_URL = "../Rezrobot_Details/Common/LabelProperties/commonLabels.properties";
		HashMap<String, String> labels = new HashMap<String, String>();
		labels = getProperty();
		System.out.println(labels.get("resReportCriteriaInvoiceIssuedNo"));
		return labels;
	}
	
	public HashMap<String, String> becLabelFlightConstructor()
	{
		HashMap<String, String> expectedLabels = new HashMap<String, String>();
		RELATIVE_URL="Resources\\LabelProperties\\flightLabels.properties";
		expectedLabels = getProperty();
		return expectedLabels;
	}
	
	public HashMap<String, String> changePortalSpecificDetails(HashMap<String, String> configDetails, HashMap<String, String> portalSpecificData)
	{
		portalSpecificData.put("portal.pgCurrency", configDetails.get("currency.code"));
		return portalSpecificData;
	}
	
	
	public HashMap<String, String> addCurrencyList(HashMap<String, String> CurrencyMap, HashMap<String, String> portalSpecificData)
	{
		Iterator<Map.Entry<String,String>>   itr = CurrencyMap.entrySet().iterator();
		String currList = "Select Currency";
		while(itr.hasNext()){
			currList.concat(" "+itr.next().getKey());
		}
		portalSpecificData.put("preferredCurrency",currList);
		return portalSpecificData;
	}

}
