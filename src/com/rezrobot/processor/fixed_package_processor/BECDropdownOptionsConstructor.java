package com.rezrobot.processor.fixed_package_processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class BECDropdownOptionsConstructor {

	public ArrayList<String> packageDurationconstructor() {
		
		ArrayList<String> optionmap=new ArrayList<>();
		
		optionmap.add(0, "All");
		optionmap.add(1, "1 3 Days");
		optionmap.add(2, "4 7 Days");
		optionmap.add(3, "8 12 Days");
		optionmap.add(4, "12 Days and Above");
		
		
		return optionmap;
		
	}
	
public ArrayList<String> packageTypeconstructor() {
		
	ArrayList<String> optionmap=new ArrayList<>();
		
		optionmap.add(0, "All Packages");
		optionmap.add(1, "Adventure Package");
		optionmap.add(2, "Beach Holiday Package");
		optionmap.add(3, "Honeymoon Package");
		optionmap.add(4, "Other Package");
		optionmap.add(5, "Pilgrimage Package");
		optionmap.add(6, "Standard Package");
		optionmap.add(7, "Tour Package");
		
		
		
		
		return optionmap;
		
	}

public ArrayList<String> departuremonthconstructor() {
	
	
	ArrayList<String> optionmap=new ArrayList<>();
	
	 Calendar cal = Calendar.getInstance();
	 Date currentdate = cal.getTime();
	 cal.setTime(currentdate);
	
	DateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
	
	optionmap.add(0, "Any");
	optionmap.add(1, dateFormat.format(currentdate));
	for (int i = 1; i < 12; i++) {
		
	
		
		cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+1));
		
		
		
		optionmap.add((i+1), dateFormat.format(cal.getTime()));
		
		
		
	}
	
	
	
	
	
	return optionmap;
	
}



	
	
	/*public static void main(String[] args) {
		BECDropdownOptionsConstructor bec=new BECDropdownOptionsConstructor();
		bec.departuremonthconstructor();

	}*/

}
