package com.rezrobot.processor.fixed_package_processor;

import java.io.IOException;
import java.util.HashMap;

import com.rezrobot.utill.ReadProperties;

public class LabelMapConstructor {
	
	
	public HashMap<String, String> becLabelConstructor() {
		
		HashMap<String, String> labels = new HashMap<String, String>();
		
		
		
		
	try {
		
		labels.put("CORlabel", getproperty("fpBEC.countryofresidencelabel"));
		labels.put("Tourplanlabel", getproperty("fpBEC.packageplanlabel"));
		labels.put("FlightInclusivelabel", getproperty("fpBEC.flightlabel"));
		labels.put("Flightexclusivelabel", getproperty("fpBEC.landlabel"));
		labels.put("departureairportlabel", getproperty("fpBEC.originlabel"));
		labels.put("destinationairportlabel", getproperty("fpBEC.destinationairportlabel"));
		labels.put("destinationcitylabel", getproperty("fpBEC.destinationcitylabel"));
		labels.put("departurelabel", getproperty("fpBEC.departuretimelabel"));
		labels.put("departuremonthlabel", getproperty("fpBEC.departuremonthlabel"));
		labels.put("durationlabel", getproperty("fpBEC.durationlabel"));
		labels.put("packagetypelabel", getproperty("fpBEC.packagetypelabel"));
		labels.put("preferredcurrencylabel", getproperty("fpBEC.currencylabel"));
		labels.put("showsearchoptionslabel", getproperty("fpBEC.searchoptionslabel"));


		
		
		
	
		
	} catch (Exception e) {
		// TODO: handle exception
	}
		
		return labels;
		
	}

	@SuppressWarnings("static-access")
	public  String getproperty(String key) throws IOException {
		
		String value="";
		
		ReadProperties propertyReader=new ReadProperties();
		value=propertyReader.readpropeties("Resources\\LabelProperties\\fixedPackageLabels.properties").get(key);
		
		return value;
		
	}
	/*public static void main(String[] args) {
		LabelMapConstructor map=new LabelMapConstructor();
		
		System.out.println(map.becLabelConstructor().get("showsearchoptionslabel"));
	}*/

}
