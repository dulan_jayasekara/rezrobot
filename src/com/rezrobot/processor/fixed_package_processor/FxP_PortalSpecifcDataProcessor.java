package com.rezrobot.processor.fixed_package_processor;

import java.util.HashMap;

import com.rezrobot.processor.LabelReadProperties;

public class FxP_PortalSpecifcDataProcessor {
public HashMap<String, String> ConfirmationStatusLoader() {
	

	
	HashMap<String, String> confstatusMap=new HashMap<>();
	LabelReadProperties prop=new LabelReadProperties();
	confstatusMap.put("BookingReference", "");
	confstatusMap.put("ResvationNo", "");
	confstatusMap.put("Customeremail", "");
	confstatusMap.put("portalWeb", prop.getPortalSpecificData().get("Portal.Website"));
	confstatusMap.put("portalTel", prop.getPortalSpecificData().get("Portal.Tel"));
	confstatusMap.put("portalfax", prop.getPortalSpecificData().get("Portal.Fax"));
	
	
	return confstatusMap;
	
}
	

}
