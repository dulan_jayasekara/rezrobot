package com.rezrobot.processor.fixed_package_processor;

import java.util.ArrayList;

import com.rezrobot.dataobjects.fixed_package_objects.FxP_DataReader;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_HotelAssignment;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_PackageObject;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_RoomOccupancy;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_SearchObject;

public class FxP_RateConstructor {
	
	public double roomRateConstructor(FxP_HotelAssignment room, FxP_RoomOccupancy occupancy) {
		
		
		double RoomRate=0;
		
		int RoomaddAdultCount=0;
		int noOfdays=room.getNumberOfDays();
		int stdCount=room.getAssignedHotel().getRoomSelected().getStdAdultCount();
		double stdRate=room.getAssignedHotel().getRoomSelected().getStdRate();
		double addRate=room.getAssignedHotel().getRoomSelected().getAddAdultRate();
		double chdRate=room.getAssignedHotel().getRoomSelected().getChildRate();
		
		if (occupancy.getAdultCount()>stdCount) {
			RoomaddAdultCount=occupancy.getAdultCount()-stdCount;
		}
		
		
		if (room.getAssignedHotel().getRateType().equals("Per Person")) {
			
			RoomRate=((stdCount*stdRate)+(RoomaddAdultCount*addRate)+(occupancy.getChildcount()*chdRate));
			
			
				
		}else {
			RoomRate=(stdCount)+(RoomaddAdultCount*addRate)+(occupancy.getChildcount()*chdRate);
		}
		RoomRate=RoomRate*noOfdays;
		return RoomRate;
		
		
		
		
		
		
		
		
		
		
	}
	
	public void hotelrateConstructor(FxP_PackageObject packageobj, FxP_SearchObject search) {
		
		ArrayList<FxP_HotelAssignment> hotellist=packageobj.getHotelList();
		ArrayList<FxP_RoomOccupancy> roomlist=search.getRoomOccupancy();
		for (int i = 0; i < roomlist.size(); i++) {
			
			for (int j = 0; j < hotellist.size(); j++) {
				
				if ((hotellist.get(j).getHotelchange().equals("default"))&& (hotellist.get(j).getRoomchange().equals("default"))) {
					
					System.out.println("Room "+(i+1));
					System.out.println("Group "+hotellist.get(j).getGroupID() );
					
					System.out.println(roomRateConstructor(hotellist.get(j), roomlist.get(i)));
					
					
					
				}
				
			}
			
			
			
			
			
			
			
		}
		
		
		
	}
	
	public double HotelBookingFeeCalc(FxP_PackageObject packageobj, double totalHotelRate) {
		
		
		return totalHotelRate;
		
	}
	
	
	/*public static void main(String[] args) {
		
		FxP_DataReader dataReader=new FxP_DataReader();


		System.out.println(dataReader.packageObjectConstruct("8").getHotelList().get(0).getHotelchange());

System.out.println(dataReader.packageObjectConstruct("8").getHotelList().get(0).getRoomchange());
		
		FxP_RateConstructor rate=new FxP_RateConstructor();
		
rate.hotelrateConstructor(dataReader.packageObjectConstruct("8"), dataReader.searchObjectReader("test1"));
		
	}*/

}
