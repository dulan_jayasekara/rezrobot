package com.rezrobot.pojo;

import org.openqa.selenium.interactions.Actions;

import com.rezrobot.core.UIElement;
import com.rezrobot.utill.DriverFactory;

public class InputFiled extends UIElement {
	
	public void clearInput() throws Exception {
		
		this.getWebElement().clear();
		
	}
	
    public String getPlaceHolder(String key){
   	try {
      	  return  this.getAttribute("placeholder");
      	} catch (Exception e) {
      	    return null;
      	}
   }
    
    public boolean getMandatoryStatus(){
 	try {
    	  return this.getAttribute("required").equalsIgnoreCase("required") ? true : false;
    	    
    	} catch (Exception e) {
    	    return false;
    	}
    }
    
    public boolean getMandatoryStatusFromDataConstraints(){
     	try {
        	  return this.getAttribute("data-constraints").contains("required") ? true : false;
        	    
        	} catch (Exception e) {
        	    return false;
        	}
    }
    
    
	public void setActionText(String Text) throws Exception {
		try {
			Actions action = new Actions(DriverFactory.getInstance().getDriver());
		    action.sendKeys(this.getWebElement(), new CharSequence[] { Text }).build().perform();
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}


}
