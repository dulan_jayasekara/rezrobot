package com.rezrobot.xml_readers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.rezrobot.common.pojo.cancellationDetails;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.common.pojo.occupancyDetails;
import com.rezrobot.common.pojo.roomDetails;
import com.rezrobot.pojo.Calendar;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.SetProxyPort;


public class GtaReader {

	public hotelDetails availabilityReq(String url) throws IOException
	{

		System.out.println(url);
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		hotelDetails			hotelDetails		= new hotelDetails();
		hotelDetails.setUser		(doc.getElementsByTag("RequestorID").first().attr("Client"));
		hotelDetails.setEmail		(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
		hotelDetails.setLocation	(doc.getElementsByTag("ItemDestination").first().attr("DestinationType"));
		hotelDetails.setCheckin		(doc.getElementsByTag("CheckInDate").first().text());
		
		Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
		ArrayList<roomDetails> roomList = new ArrayList<roomDetails>();
		
		while(roomIterator.hasNext())
		{
			Element	currentElement							= roomIterator.next();
			roomDetails room = new roomDetails();
			room.setRoomID			(currentElement.getElementsByTag("Room").first().attr("Code"));
			room.setRoomCount		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
			room.setNumberOfCots	(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		
		
		return hotelDetails;
	}
	
	public ArrayList<hotelDetails> availabilityRes(String url) throws IOException
	{

		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		ArrayList<hotelDetails>	hotelList		= new ArrayList<hotelDetails>(); 
		Iterator<Element> 		hotelIterator 	= doc.getElementsByTag("Hotel").iterator();
		while(hotelIterator.hasNext())
		{
			Element currentElement = hotelIterator.next();
			hotelDetails	hDetails			= new hotelDetails(); 
			hDetails.setLocation			(currentElement.getElementsByTag("City").first().text());
			hDetails.setHotel				(currentElement.getElementsByTag("Item").first().text());
			hDetails.setHotelCode			(currentElement.getElementsByTag("Item").first().attr("Code") + "_" + currentElement.getElementsByTag("City").first().attr("Code"));
			hDetails.setStarRatingCode		(currentElement.getElementsByTag("StarRating").first().text());
			
			Iterator<Element> categoryIterator = currentElement.getElementsByTag("RoomCategory").iterator();
			ArrayList<roomDetails> rcList = new ArrayList<roomDetails>();
			while(categoryIterator.hasNext())
			{
				Element categoryElement = categoryIterator.next();
				roomDetails rc = new roomDetails();
				rc.setRoomType				(categoryElement.getElementsByTag("Description").first().text());
				rc.setPrice					(categoryElement.getElementsByTag("ItemPrice").first().text());
				rc.setCurrency				(categoryElement.getElementsByTag("ItemPrice").first().attr("Currency"));
				rc.setAvailability			(categoryElement.getElementsByTag("Confirmation").first().text());
				rc.setMealBasis				(categoryElement.getElementsByTag("Basis").first().text());
				try {
					rc.setMealBreakfast			(categoryElement.getElementsByTag("Breakfast").first().text());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				Iterator<Element> roomIterator = categoryElement.getElementsByTag("HotelRoom").iterator();
				ArrayList<roomDetails> rhrList = new ArrayList<roomDetails>();
				while(roomIterator.hasNext())
				{
					Element 		roomElement = roomIterator.next();
					roomDetails 	rhr 		= new roomDetails();
					rhr.setRoomCode			(roomElement.getElementsByTag("HotelRoom").first().attr("Code"));
					rhr.setPrice			(roomElement.getElementsByTag("RoomPrice ").first().attr("Gross"));
					rhr.setCheckin			(roomElement.getElementsByTag("FromDate").first().text());
					rhr.setCheckout			(roomElement.getElementsByTag("ToDate").first().text());
					rhr.setNights			(roomElement.getElementsByTag("Price ").first().attr("Nights"));
					rhrList.add(rhr);
				}
				rc.setRoomDetails(rhrList);
				rcList.add(rc);
			}
			hDetails.setRoomDetails(rcList);
			hotelList.add(hDetails);
		}
		return hotelList;
	}
	
	public hotelDetails roomAvailabilityReq(String url) throws IOException
	{
		hotelDetails 				hotel 		= new hotelDetails();
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		try {
			hotel.setUser			(doc.getElementsByTag("RequestorID").first().attr("Client"));
			hotel.setEmail			(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
			try {
				hotel.setHotel		(doc.getElementsByTag("ItemName").first().text());
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotel.setHotelCode		(doc.getElementsByTag("ItemCode").first().text());
			hotel.setCheckin		(doc.getElementsByTag("CheckInDate").first().text());
			hotel.setNights			(doc.getElementsByTag("Duration").first().text());
			
			Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
			ArrayList<roomDetails> roomList = new ArrayList<roomDetails>();
			while(roomIterator.hasNext())
			{
				Element currentElement = roomIterator.next();
				roomDetails room = new roomDetails();
				room.setRoomName		(currentElement.getElementsByTag("Room").first().attr("Code"));
				room.setRoomID			(currentElement.getElementsByTag("Room").first().attr("id"));
				room.setRoomCount		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
				room.setNumberOfCots	(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
				
				roomList.add(room);
			}
			hotel.setRoomDetails(roomList);
		} catch (Exception e) {
			// TODO: handle exception
		}	
		return hotel;
	}
	
	public hotelDetails roomAvailabilityRes(String url) throws IOException
	{

		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		hotelDetails			hotel			= new hotelDetails(); 
		
		try {
			hotel.setLocation						(doc.getElementsByTag("City").first().text());
			hotel.setLocationCode					(doc.getElementsByTag("City").first().attr("Code"));
		} catch (Exception e) {
			// TODO: handle exception
			hotel.setLocation						("City tag unavailable");
			hotel.setLocationCode					("Attribute code unavailable");
		}
		
		hotel.setHotelCode						(doc.getElementsByTag("Item").first().attr("Code"));
		hotel.setHotel							(doc.getElementsByTag("Item").first().text());
		Element roomDetails = doc.getElementsByTag("HotelRooms").first();
		hotel.setRoomCount						(String.valueOf(roomDetails.getElementsByTag("HotelRoom ").size()));
		Iterator<Element> 			inv27Iterator 	= roomDetails.getElementsByTag("HotelRoom").iterator();
		
//-------------------------------------------------------------------------------------------------------------------------------		
		
		/*ArrayList<inv27HotelRoom>     	inv27RoomList 	= new ArrayList<inv27HotelRoom>();
		while(inv27Iterator.hasNext())
		{
			Element currenteElement = inv27Iterator.next();
			inv27HotelRoom room = new inv27HotelRoom();
			room.setRoomCode					(currenteElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setRoomCount					(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			try {
				room.setExtraBed				(currenteElement.getElementsByTag("HotelRoom").first().attr("ExtraBed"));
				room.setExtraBedCount			(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfExtraBeds"));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			inv27RoomList.add(room);
		}
		hotel.setRoomDetails(inv27RoomList);*/
		
//-------------------------------------------------------------------------------------------------------------------------------		
		hotel.setStarRating						(doc.getElementsByTag("StarRating").first().text());
		hotel.setDescription					(doc.getElementsByTag("Description").first().text());
		//System.out.println(hotel.getDesdription());
		hotel.setPrice							(doc.getElementsByTag("ItemPrice ").first().text());
		hotel.setCurrencyCode					(doc.getElementsByTag("ItemPrice").first().attr("Currency"));
		hotel.setAvailabilityStatus				(doc.getElementsByTag("Confirmation").first().text());
		
		Element hotelRoomPrices = doc.getElementsByTag("HotelRoomPrices").first();
		Iterator<Element> roomIterator = hotelRoomPrices.getElementsByTag("HotelRoom ").iterator();
		ArrayList<roomDetails> roomList = new ArrayList<roomDetails>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			roomDetails room = new roomDetails();
			room.setRoomName				(doc.getElementsByTag("Description").first().text());
			room.setRoomID					(currentElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setPrice					(currentElement.getElementsByTag("RoomPrice").first().attr("Gross"));
			room.setCheckin					(currentElement.getElementsByTag("FromDate").first().text());
			room.setCheckout				(currentElement.getElementsByTag("ToDate").first().text());
			try {
				room.setBoard					(doc.getElementsByTag("Breakfast").first().text() + " " + doc.getElementsByTag("Basis").first().text());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			String numOfRooms = "";
			try {
				numOfRooms = currentElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms");
			} catch (Exception e) {
				// TODO: handle exception
				numOfRooms = "1";
			}
			room.setRoomCount(numOfRooms);
			room.setNights						(currentElement.getElementsByTag("Price").first().attr("Nights"));
			roomList.add(room);
		}
		hotel.setRoomDetails(roomList);
		ArrayList<cancellationDetails> 	cancellationlist 	= new ArrayList<cancellationDetails>();
		Iterator<Element> cancelationIterator = doc.getElementsByTag("ChargeCondition").iterator();
		while(cancelationIterator.hasNext())
		{
			Element 						currentElement 		= cancelationIterator.next();
			Iterator<Element> 				policyIterator 		= currentElement.getElementsByTag("Condition").iterator();
			
			while(policyIterator.hasNext())
			{
				Element currentElement2 = policyIterator.next();
				cancellationDetails can = new cancellationDetails();
				try {
					can.setCharge						(currentElement.getElementsByTag("ChargeCondition").first().attr("Type"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				if(can.getCharge().equals("cancellation"))
				{
					try {
						can.setChargeAmount					(currentElement2.getElementsByTag("Condition").first().attr("ChargeAmount"));
						can.setCurrency						(currentElement2.getElementsByTag("Condition").first().attr("Currency"));
						can.setCharge						(currentElement2.getElementsByTag("Condition").first().attr("Charge"));
					} catch (Exception e) {
						// TODO: handle exception
					}
					try {
						can.setDeadline						(currentElement2.getElementsByTag("Condition").first().attr("FromDate"));	
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				cancellationlist.add(can);
			}
		}
		hotel.setCancellation(cancellationlist);
		hotel.setNights							(doc.getElementsByTag("Price").attr("Nights"));
		return hotel;
	
	}
	
	public hotelDetails preReservationReq(String url) throws IOException
	{
		hotelDetails 			hotel 			= new hotelDetails();
		try {
			SetProxyPort 			proxyAndPort	= new SetProxyPort();
			Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
			
			hotel.setUser			(doc.getElementsByTag("RequestorID").first().attr("Client"));
			hotel.setEmail			(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
			try {
				hotel.setHotel		(doc.getElementsByTag("ItemName").first().text());
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotel.setHotelCode		(doc.getElementsByTag("ItemCode").first().text());
			hotel.setCheckin		(doc.getElementsByTag("CheckInDate").first().text());
			hotel.setNights			(doc.getElementsByTag("Duration").first().text());
			
			Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
			ArrayList<roomDetails> roomList = new ArrayList<roomDetails>();
			while(roomIterator.hasNext())
			{
				Element currentElement = roomIterator.next();
				roomDetails room = new roomDetails();
				room.setRoomType		(currentElement.getElementsByTag("Room").first().attr("Code"));
				room.setRoomID			(currentElement.getElementsByTag("Room").first().attr("id"));
				room.setRoomCount		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
				room.setNumberOfCots	(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
				
				roomList.add(room);
			}
			hotel.setRoomDetails(roomList);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Pre reservation request error - " + e);
		}
		
		
		return hotel;
	}
	
	public hotelDetails preReservationResponse(String url) throws IOException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		hotelDetails			hotel	= new hotelDetails(); 
		
		hotel.setLocation				(doc.getElementsByTag("City").first().text());
		hotel.setLocationCode			(doc.getElementsByTag("City").first().attr("Code"));
		hotel.setHotel					(doc.getElementsByTag("Item ").first().text());
		hotel.setHotelCode				(doc.getElementsByTag("Item ").first().attr("Code"));
		
		Element roomDetails = doc.getElementsByTag("HotelRooms").first();
		Iterator<Element> 			inv27Iterator 	= roomDetails.getElementsByTag("HotelRoom").iterator();
		
		ArrayList<roomDetails>     	inv27RoomList 	= new ArrayList<roomDetails>();
		while(inv27Iterator.hasNext())
		{
			Element currenteElement = inv27Iterator.next();
			roomDetails room = new roomDetails();
			room.setRoomCode					(currenteElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setRoomCount					(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			roomDetails inv27Room = new roomDetails();
			try {
				inv27Room.setExtraBed			(currenteElement.getElementsByTag("HotelRoom").first().attr("ExtraBed"));
				inv27Room.setExtraBedCount		(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfExtraBeds"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			inv27RoomList.add(room);
		}
		hotel.setRoomDetails(inv27RoomList);
		hotel.setDescription					(doc.getElementsByTag("Description").first().text());
		hotel.setPrice							(doc.getElementsByTag("ItemPrice ").first().text());
		hotel.setCurrencyCode					(doc.getElementsByTag("ItemPrice").first().attr("Currency"));
		hotel.setAvailabilityStatus				(doc.getElementsByTag("Confirmation").first().text());
		
		Element hotelRoomPrices = doc.getElementsByTag("HotelRoomPrices").first();
		Iterator<Element> roomIterator = hotelRoomPrices.getElementsByTag("HotelRoom ").iterator();
		ArrayList<roomDetails> roomList = new ArrayList<roomDetails>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			roomDetails room = new roomDetails();
			room.setRoomID						(currentElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setPrice						(currentElement.getElementsByTag("RoomPrice").first().attr("Gross"));
			room.setCheckin						(currentElement.getElementsByTag("FromDate").first().text());
			room.setCheckout					(currentElement.getElementsByTag("ToDate").first().text());
			try {
				room.setRoomCount					(currentElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			room.setNights						(currentElement.getElementsByTag("Price").first().attr("Nights"));
			roomList.add(room);
		}
		hotel.setRoomDetails(roomList);
		
		Iterator<Element> cancelationIterator = doc.getElementsByTag("ChargeCondition").iterator();
		ArrayList<cancellationDetails> cancellationlist = new ArrayList<cancellationDetails>();
		while(cancelationIterator.hasNext())
		{
			Element currentElement = cancelationIterator.next();
			Iterator<Element> policyIterator = currentElement.getElementsByTag("Condition").iterator();	
			while(policyIterator.hasNext())
			{
				Element currentElement2 = policyIterator.next();
				cancellationDetails can = new cancellationDetails();
				try {
					can.setCharge							(currentElement.getElementsByTag("ChargeCondition").first().attr("Type"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					can.setChargeAmount					(currentElement2.getElementsByTag("Condition").first().attr("ChargeAmount"));
					can.setCurrency						(currentElement2.getElementsByTag("Condition").first().attr("Currency"));
					can.setCharge						(currentElement2.getElementsByTag("Condition").first().attr("Charge"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					can.setDeadline						(currentElement2.getElementsByTag("Condition").first().attr("FromDate"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				cancellationlist.add(can);
			}
		}
		hotel.setCancellation(cancellationlist);
		return hotel;	
	}
	
	public hotelDetails reservationReq(String url) throws IOException
	{

		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		hotelDetails 			req 			= new hotelDetails();
		Element 				pax	 			= doc.getElementsByTag("PaxNames").first();
		Iterator<Element>  		paxList 		= pax.getElementsByTag("PaxName").iterator();
		ArrayList<occupancyDetails> 	guestList 	= new ArrayList<occupancyDetails>();
		while(paxList.hasNext())
		{
			Element 			currenteElement 	= paxList.next();
			occupancyDetails 	guest 				= new occupancyDetails();
			guest.setId			(currenteElement.getElementsByTag("PaxName").attr("PaxId"));
			guest.setFirstName	(currenteElement.getElementsByTag("PaxName ").first().text());
			try {
				guest.setType		(currenteElement.getElementsByTag("PaxName ").attr("PaxType"));
				guest.setAge		(currenteElement.getElementsByTag("PaxName ").attr("ChildAge"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			guestList.add(guest);
		}
		req.setOccupancy(guestList);
		req.setHotelType		(doc.getElementsByTag("BookingItem").attr("ItemType"));
		req.setHotelReference	(doc.getElementsByTag("ItemReference").first().text());
		req.setCity				(doc.getElementsByTag("ItemCity ").attr("Code"));
		req.setHotelCode		(doc.getElementsByTag("Item").attr("Code"));
		req.setCheckin			(doc.getElementsByTag("CheckInDate").first().text());
		req.setCheckout			(doc.getElementsByTag("CheckOutDate").first().text());
		Element 					rooms 				= doc.getElementsByTag("HotelRooms").first();
		Iterator<Element>  			roomIterator 		= pax.getElementsByTag("HotelRoom").iterator();
		ArrayList<roomDetails> 		roomList 			= new ArrayList<roomDetails>();
		while(roomIterator.hasNext())
		{
			Element 				currenteElement 	= roomIterator.next();
			roomDetails 			room 				= new roomDetails();
			room.setRoomCode		(currenteElement.getElementsByTag("HotelRoom ").attr("Code"));
			room.setRoomCode		(currenteElement.getElementsByTag("HotelRoom ").attr("id"));
			try {
				room.setNumberOfRooms	(currenteElement.getElementsByTag("HotelRoom").attr("NumberOfCots"));
				room.setExtraBedCount	(currenteElement.getElementsByTag("HotelRoom").attr("ExtraBed"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			roomList.add(room);
		}
		req.setRoomDetails(roomList);
		return req;
	}
	
	public hotelDetails reservationRes(String url) throws IOException, ParseException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(url));
		hotelDetails					res					= 	new hotelDetails();
		Element 						pax 				= 	doc.getElementsByTag("PaxNames").first();
		Iterator<Element>  				paxList 			= 	pax.getElementsByTag("PaxName").iterator();
		ArrayList<occupancyDetails> 	guestList 			= 	new ArrayList<occupancyDetails>();
		while(paxList.hasNext())
		{
			Element 					currenteElement 	= 	paxList.next();
			occupancyDetails 			guest 				= 	new occupancyDetails();
			guest.setId					(currenteElement.getElementsByTag("PaxName").attr("PaxId"));
			guest.setFirstName			(currenteElement.getElementsByTag("PaxName ").first().text());
			try {
				guest.setType			(currenteElement.getElementsByTag("PaxName ").attr("PaxType"));
				guest.setAge			(currenteElement.getElementsByTag("PaxName ").attr("ChildAge"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			guestList.add(guest);
		}
		res.setOccupancy(guestList);
		
		res.setSupConfirmationNumber		(doc.getElementsByTag("BookingReference").get(1).text());
		res.setCity							(doc.getElementsByTag("ItemCity").first().text());
		res.setCityCode						(doc.getElementsByTag("ItemCity").attr("Code"));
		res.setBookingDate					(doc.getElementsByTag("BookingCreationDate").first().text());
		res.setReferenceTime				(doc.getElementsByTag("BookingDepartureDate").first().text());
		res.setPrice						(doc.getElementsByTag("BookingPrice").attr("Gross"));
		res.setCurrencyCode					(doc.getElementsByTag("BookingPrice ").attr("Currency"));
		res.setAvailabilityStatus			(doc.getElementsByTag("BookingStatus").first().text());
		res.setHotel						(doc.getElementsByTag("Item").first().text());
		res.setHotelCode					(doc.getElementsByTag("Item").first().attr("Code"));
		res.setCheckin						(doc.getElementsByTag("CheckInDate").first().text());
		res.setCheckout						(doc.getElementsByTag("CheckOutDate").first().text());
		try {
			res.setMealBasis					(doc.getElementsByTag("Basis").first().text());
			res.setMealBreakFast				(doc.getElementsByTag("Breakfast").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Calender cal = new Calender();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date day1 = sdf.parse(res.getCheckin());
		Date day2 = sdf.parse(res.getCheckout());
		res.setNights(cal.differenceBetweenDays(day2, day1));
		
		Iterator<Element>  					roomIterator 	= doc.getElementsByTag("HotelRoom").iterator();
		ArrayList<roomDetails> 				roomList 		= new ArrayList<roomDetails>();
		ArrayList<ArrayList<String>> 		paxxList		= new ArrayList<ArrayList<String>>();
		while(roomIterator.hasNext())
		{
			Element 		currenteElement 	= roomIterator.next();
			roomDetails 		room 			= new roomDetails();
			
			room.setRoomCode		(currenteElement.getElementsByTag("HotelRoom").attr("Code"));
			room.setRoomID			(currenteElement.getElementsByTag("HotelRoom").attr("id"));
			room.setDescription		(currenteElement.getElementsByTag("Description").first().text());
			try {
				room.setBoard			(doc.getElementsByTag("Breakfast").first().text() + " " + doc.getElementsByTag("Basis").first().text());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				room.setNumberOfRooms		(currenteElement.getElementsByTag("HotelRoom").attr("NumberOfCots"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				room.setExtraBedCount	(currenteElement.getElementsByTag("HotelRoom").attr("ExtraBed"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				room.setExtraBedCount	(currenteElement.getElementsByTag("HotelRoom").attr("SharingBedding"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			Iterator<Element>  		paxIterator 	= currenteElement.getElementsByTag("PaxId").iterator();
			
//-------------------------------------------------------------------------------------------------------------------------			
			/*ArrayList<String> 		paxx 			= new ArrayList<String>();
			while(paxIterator.hasNext())
			{
				Element 		currenteElement2 	= paxIterator.next();
				paxx.add(currenteElement2.getElementsByTag("PaxId").first().text());
			}
			paxxList.add(paxx);*/
//-------------------------------------------------------------------------------------------------------------------------	
			roomList.add(room);
		}
		//res.setPaxList(paxxList);
		res.setRoomCount(String.valueOf(roomList.size()));
		res.setRoomDetails(roomList);
		ArrayList<cancellationDetails> cancellationlist = new ArrayList<cancellationDetails>();
		Iterator<Element> cancelationIterator = doc.getElementsByTag("ChargeCondition").iterator();
		while(cancelationIterator.hasNext())
		{
			Element currentElement = cancelationIterator.next();
			Iterator<Element> policyIterator = currentElement.getElementsByTag("Condition").iterator();
			while(policyIterator.hasNext())
			{
				Element currentElement2 = policyIterator.next();
				cancellationDetails can = new cancellationDetails();
				try {
					can.setCharge						(currentElement.getElementsByTag("ChargeCondition").first().attr("Type"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					can.setChargeAmount					(currentElement2.getElementsByTag("Condition").first().attr("ChargeAmount"));
					can.setCurrency						(currentElement2.getElementsByTag("Condition").first().attr("Currency"));
					can.setCharge						(currentElement2.getElementsByTag("Condition").first().attr("Charge"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					can.setDeadline						(currentElement2.getElementsByTag("Condition").first().attr("FromDate"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				cancellationlist.add(can);
			}
		}
		res.setCancellation(cancellationlist);
		
		return res;
	
	}
}
