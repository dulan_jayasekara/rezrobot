package com.rezrobot.xml_readers;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import com.rezrobot.common.pojo.cancellationDetails;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.common.pojo.occupancyDetails;
import com.rezrobot.common.pojo.roomDetails;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.SetProxyPort;

public class RezLiveReader {

	public hotelDetails availabilityReq(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails			hotelDetails		= new hotelDetails();
		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser				(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword			(doc.getElementsByTag("Password").first().text());
		hotelDetails.setCheckin				(doc.getElementsByTag("ArrivalDate").first().text());
		hotelDetails.setCheckout			(doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCountryCode			(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode			(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry		(doc.getElementsByTag("GuestNationality").first().text());
		Elements starRating = doc.getElementsByTag("HotelRating");
		String star = "";
		for(int i = 0 ; i < starRating.size() ; i++)
		{
			if(i == 0)
			{
				star = starRating.get(i).getElementsByTag("HotelRating").text();
			}
			else
			{
				star = star.concat("/").concat(starRating.get(i).getElementsByTag("HotelRating").text());
			}
		}
		hotelDetails.setStarRating			(star);
		ArrayList<roomDetails> roomlist = new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("Room");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			room.setRoomType				(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount				(roomListElement.get(i).getElementsByTag("NoOfAdults").first().text());
			room.setChildCount				(roomListElement.get(i).getElementsByTag("NoOfChilds").first().text());
			try {
				ArrayList<occupancyDetails> 		occupancyList				= new ArrayList<occupancyDetails>();
				Elements childAgeElement = roomListElement.get(i).getElementsByTag("ChildAge");
				for(int j = 0 ; j < childAgeElement.size() ; j++)
				{
					occupancyDetails occupancy = new occupancyDetails();
					occupancy.setAge		(childAgeElement.get(j).getElementsByTag("ChildAge").first().text());
					occupancyList.add(occupancy);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			roomlist.add(room);
		}
		hotelDetails.setRoomDetails(roomlist);
		return hotelDetails;
	}
	
	public ArrayList<hotelDetails> availabilityRes(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		Elements 				hotelListElement	= doc.getElementsByTag("Hotel");
		
		ArrayList<hotelDetails> hotelList 			= new ArrayList<hotelDetails>();
		for(int i = 0 ; i < hotelListElement.size() ; i++)
		{
			hotelDetails			hotelDetails		= new hotelDetails();
			try {
				hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotelDetails.setCheckin						(doc.getElementsByTag("ArrivalDate").first().text());
			hotelDetails.setCheckout					(doc.getElementsByTag("DepartureDate").first().text());
			hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
			hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
			hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
			hotelDetails.setHotelCode					(hotelListElement.get(i).getElementsByTag("Id").first().text());
			hotelDetails.setHotel						(hotelListElement.get(i).getElementsByTag("Name").first().text());
			hotelDetails.setStarRating					(hotelListElement.get(i).getElementsByTag("Rating").first().text());
			hotelDetails.setPrice						(hotelListElement.get(i).getElementsByTag("Price").first().text());
			Elements roomListElement 					= hotelListElement.get(i).getElementsByTag("RoomDetail");
			ArrayList<roomDetails> roomList 			= new ArrayList<roomDetails>();
			for(int j = 0 ; j < roomListElement.size() ; j++)
			{
				roomDetails room = new roomDetails();
				room.setRoomType						(roomListElement.get(j).getElementsByTag("Type").first().text());
				room.setAdultCount						(roomListElement.get(j).getElementsByTag("Adults").first().text());
				room.setChildCount						(roomListElement.get(j).getElementsByTag("Children").first().text());
				ArrayList<occupancyDetails> 			occupancyList				= new ArrayList<occupancyDetails>();
				occupancyDetails						occupancy					= new occupancyDetails();
				occupancy.setAge						(roomListElement.get(j).getElementsByTag("ChildrenAges").first().text());
				occupancyList.add(occupancy);
				room.setOccupancyDetails(occupancyList);
				room.setRoomCount						(roomListElement.get(j).getElementsByTag("TotalRooms").first().text());
				room.setPrice							(roomListElement.get(j).getElementsByTag("TotalRate").first().text());
				room.setBoard							(roomListElement.get(j).getElementsByTag("RoomDescription").first().text());
				roomList.add(room);
			}
			hotelDetails.setRoomDetails(roomList);
			hotelList.add(hotelDetails);
		}
		
		return hotelList;
	}
	
	public hotelDetails roomAvailabilityReq(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails 			hotelDetails 		= new hotelDetails();
		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser						(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword					(doc.getElementsByTag("Password").first().text());
		hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
		hotelDetails.setCheckin						(doc.getElementsByTag("ArrivalDate").first().text());
		hotelDetails.setCheckout					(doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCountryCode					(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode					(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
		hotelDetails.setHotelCode					(doc.getElementsByTag("HotelId").first().text());
		hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
		ArrayList<roomDetails>  	roomList		= new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("RoomDetail");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			room.setRoomName						(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount						(roomListElement.get(i).getElementsByTag("Adults").first().text());
			room.setChildCount						(roomListElement.get(i).getElementsByTag("Children").first().text());
			ArrayList<occupancyDetails> occupancyList 	= new ArrayList<occupancyDetails>();
			occupancyDetails 			occupancy 		= new occupancyDetails();
			occupancy.setAge						(roomListElement.get(i).getElementsByTag("ChildrenAges").first().text());
			occupancyList.add(occupancy);
			room.setOccupancyDetails(occupancyList);
			room.setRoomCount						(roomListElement.get(i).getElementsByTag("TotalRooms").first().text());
			room.setPrice							(roomListElement.get(i).getElementsByTag("TotalRate").first().text());
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		return hotelDetails;
	}
	
	public hotelDetails roomAvailabilityRes(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails 			hotelDetails 		= new hotelDetails();
		
		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser						(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword					(doc.getElementsByTag("Password").first().text());
		hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
		hotelDetails.setCheckin						(doc.getElementsByTag("ArrivalDate").first().text());
		hotelDetails.setCheckout					(doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCountryCode					(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode					(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
		hotelDetails.setHotelCode					(doc.getElementsByTag("HotelId").first().text());
		hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
		hotelDetails.setAvailabilityStatus			(doc.getElementsByTag("Status").first().text());
		hotelDetails.setPrice						(doc.getElementsByTag("BookingBeforePrice").first().text());
		hotelDetails.setAgentBalance				(doc.getElementsByTag("AgentBalance").first().text());
		ArrayList<roomDetails>  	roomList		= new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("RoomDetail");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			room.setRoomName						(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount						(roomListElement.get(i).getElementsByTag("Adults").first().text());
			room.setChildCount						(roomListElement.get(i).getElementsByTag("Children").first().text());
			ArrayList<occupancyDetails> occupancyList 	= new ArrayList<occupancyDetails>();
			occupancyDetails 			occupancy 		= new occupancyDetails();
			occupancy.setAge						(roomListElement.get(i).getElementsByTag("ChildrenAges").first().text());
			occupancyList.add(occupancy);
			room.setOccupancyDetails(occupancyList);
			room.setRoomCount						(roomListElement.get(i).getElementsByTag("TotalRooms").first().text());
			room.setPrice							(roomListElement.get(i).getElementsByTag("TotalRate").first().text());
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		ArrayList<cancellationDetails>  	cancellationList			= new ArrayList<cancellationDetails>();
		Elements 							cancellationListElement 	= doc.getElementsByTag("CancellationInformation");
		for(int i = 0 ; i < cancellationListElement.size() ; i++)
		{
			cancellationDetails	cancellation = new cancellationDetails();
			cancellation.setStartDate				(cancellationListElement.get(i).getElementsByTag("StartDate").first().text());
			cancellation.setEndDate					(cancellationListElement.get(i).getElementsByTag("EndDate").first().text());
			cancellation.setCharge					(cancellationListElement.get(i).getElementsByTag("ChargeType").first().text());
			cancellation.setChargeAmount			(cancellationListElement.get(i).getElementsByTag("ChargeAmount").first().text());
			cancellation.setCurrency				(cancellationListElement.get(i).getElementsByTag("Currency").first().text());
			cancellationList.add(cancellation);
		}
		hotelDetails.setCancellation(cancellationList);
		return hotelDetails;
	}
	
	public hotelDetails preReservationReq(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails 			hotelDetails 		= new hotelDetails();
		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser						(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword					(doc.getElementsByTag("Password").first().text());
		hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
		hotelDetails.setCheckin						(doc.getElementsByTag("ArrivalDate").first().text());
		hotelDetails.setCheckout					(doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCountryCode					(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode					(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
		hotelDetails.setHotelCode					(doc.getElementsByTag("HotelId").first().text());
		hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
		ArrayList<roomDetails>  	roomList		= new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("RoomDetail");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			room.setRoomName						(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount						(roomListElement.get(i).getElementsByTag("Adults").first().text());
			room.setChildCount						(roomListElement.get(i).getElementsByTag("Children").first().text());
			ArrayList<occupancyDetails> occupancyList 	= new ArrayList<occupancyDetails>();
			occupancyDetails 			occupancy 		= new occupancyDetails();
			occupancy.setAge						(roomListElement.get(i).getElementsByTag("ChildrenAges").first().text());
			occupancyList.add(occupancy);
			room.setOccupancyDetails(occupancyList);
			room.setRoomCount						(roomListElement.get(i).getElementsByTag("TotalRooms").first().text());
			room.setPrice							(roomListElement.get(i).getElementsByTag("TotalRate").first().text());
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		return hotelDetails;
	}
	
	public hotelDetails preReservationRes(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails 			hotelDetails 		= new hotelDetails();

		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser						(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword					(doc.getElementsByTag("Password").first().text());
		hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
		hotelDetails.setCheckin						(doc.getElementsByTag("ArrivalDate").first().text());
		hotelDetails.setCheckout					(doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCountryCode					(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode					(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
		hotelDetails.setHotelCode					(doc.getElementsByTag("HotelId").first().text());
		hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
		hotelDetails.setAvailabilityStatus			(doc.getElementsByTag("Status").first().text());
		hotelDetails.setPrice						(doc.getElementsByTag("BookingBeforePrice").first().text());
		hotelDetails.setAgentBalance				(doc.getElementsByTag("AgentBalance").first().text());
		ArrayList<roomDetails>  	roomList		= new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("RoomDetail");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			room.setRoomName						(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount						(roomListElement.get(i).getElementsByTag("Adults").first().text());
			room.setChildCount						(roomListElement.get(i).getElementsByTag("Children").first().text());
			ArrayList<occupancyDetails> occupancyList 	= new ArrayList<occupancyDetails>();
			occupancyDetails 			occupancy 		= new occupancyDetails();
			occupancy.setAge						(roomListElement.get(i).getElementsByTag("ChildrenAges").first().text());
			occupancyList.add(occupancy);
			room.setOccupancyDetails(occupancyList);
			room.setRoomCount						(roomListElement.get(i).getElementsByTag("TotalRooms").first().text());
			room.setPrice							(roomListElement.get(i).getElementsByTag("TotalRate").first().text());
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		ArrayList<cancellationDetails>  	cancellationList			= new ArrayList<cancellationDetails>();
		Elements 							cancellationListElement 	= doc.getElementsByTag("CancellationInformation");
		for(int i = 0 ; i < cancellationListElement.size() ; i++)
		{
			cancellationDetails	cancellation = new cancellationDetails();
			cancellation.setStartDate				(cancellationListElement.get(i).getElementsByTag("StartDate").first().text());
			cancellation.setEndDate					(cancellationListElement.get(i).getElementsByTag("EndDate").first().text());
			cancellation.setCharge					(cancellationListElement.get(i).getElementsByTag("ChargeType").first().text());
			cancellation.setChargeAmount			(cancellationListElement.get(i).getElementsByTag("ChargeAmount").first().text());
			cancellation.setCurrency				(cancellationListElement.get(i).getElementsByTag("Currency").first().text());
			cancellationList.add(cancellation);
		}
		hotelDetails.setCancellation(cancellationList);
	
		
		return hotelDetails;
	}
	
	public hotelDetails reservationReq(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails 			hotelDetails 		= new hotelDetails();
		
		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser						(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword					(doc.getElementsByTag("Password").first().text());
		hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
		hotelDetails.setCheckin						(doc.getElementsByTag("ArrivalDate").first().text());
		hotelDetails.setCheckout					(doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCountryCode					(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode					(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
		hotelDetails.setHotelCode					(doc.getElementsByTag("HotelId").first().text());
		hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
		ArrayList<roomDetails>  	roomList		= new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("RoomDetail");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			room.setRoomName						(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount						(roomListElement.get(i).getElementsByTag("Adults").first().text());
			room.setChildCount						(roomListElement.get(i).getElementsByTag("Children").first().text());
			room.setRoomCount						(roomListElement.get(i).getElementsByTag("TotalRooms").first().text());
			room.setPrice							(roomListElement.get(i).getElementsByTag("TotalRate").first().text());
			
			ArrayList<occupancyDetails> occupancyList = new ArrayList<occupancyDetails>();
			Elements occupancyListElement = roomListElement.get(i).getElementsByTag("Guest");
			for(int j = 0 ; j < occupancyListElement.size() ; j++)
			{
				occupancyDetails occupancy = new occupancyDetails();
				occupancy.setTitle					(occupancyListElement.get(j).getElementsByTag("Salutation").first().text());
				occupancy.setFirstName				(occupancyListElement.get(j).getElementsByTag("FirstName").first().text());
				occupancy.setLastName				(occupancyListElement.get(j).getElementsByTag("LastName").first().text());
				try {
					if(occupancyListElement.get(j).getElementsByTag("IsChild").size() > 0)
					{
						occupancy.setType			("Child");
						occupancy.setAge			(occupancyListElement.get(j).getElementsByTag("Age").first().text());
					}
					else
					{
						occupancy.setType			("Adult");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				occupancyList.add(occupancy);
			}
			room.setOccupancyDetails(occupancyList);
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		
		return hotelDetails;
	}
	
	public hotelDetails reservationRes(String pageSource) throws IOException, ParseException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails 			hotelDetails 		= new hotelDetails();
		hotelDetails.setAgentCode					(doc.getElementsByTag("AgentCode").first().text());
		hotelDetails.setUser						(doc.getElementsByTag("UserName").first().text());
		hotelDetails.setPassword					(doc.getElementsByTag("Password").first().text());
		hotelDetails.setSessionID					(doc.getElementsByTag("SearchSessionId").first().text());
		String checkin = Calender.getDate("dd/MM/yyyy", "MMM dd yyyy", doc.getElementsByTag("ArrivalDate").first().text());
		String checkout = Calender.getDate("dd/MM/yyyy", "MMM dd yyyy", doc.getElementsByTag("DepartureDate").first().text());
		hotelDetails.setCheckin						(checkin);
		hotelDetails.setCheckout					(checkout);
		hotelDetails.setCountryCode					(doc.getElementsByTag("CountryCode").first().text());
		hotelDetails.setCityCode					(doc.getElementsByTag("City").first().text());
		hotelDetails.setGuestCountry				(doc.getElementsByTag("GuestNationality").first().text());
		hotelDetails.setHotelCode					(doc.getElementsByTag("HotelId").first().text());
		hotelDetails.setHotel						(doc.getElementsByTag("Name").first().text());	
		hotelDetails.setCurrencyCode				(doc.getElementsByTag("Currency").first().text());
		hotelDetails.setAvailabilityStatus			(doc.getElementsByTag("BookingStatus").first().text());
		try {
			hotelDetails.setPrice						(doc.getElementsByTag("BookingBeforePrice").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			hotelDetails.setAgentBalance				(doc.getElementsByTag("AgentBalance").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ArrayList<roomDetails>  	roomList		= new ArrayList<roomDetails>();
		Elements roomListElement = doc.getElementsByTag("RoomDetail");
		for(int i = 0 ; i < roomListElement.size() ; i++)
		{
			roomDetails room = new roomDetails();
			String[] roomType = null;
			if(roomListElement.get(i).getElementsByTag("Type").first().text().contains("|"))
			{
				roomType = roomListElement.get(i).getElementsByTag("Type").first().text().split("\\|");
			}
			//room.setRoomName						(roomType[0]);
			room.setRoomName						(roomListElement.get(i).getElementsByTag("Type").first().text());
			room.setAdultCount						(roomListElement.get(i).getElementsByTag("Adults").first().text());
			room.setChildCount						(roomListElement.get(i).getElementsByTag("Children").first().text());
			room.setRoomCount						(roomListElement.get(i).getElementsByTag("TotalRooms").first().text());
			room.setPrice							(roomListElement.get(i).getElementsByTag("TotalRate").first().text());
			ArrayList<occupancyDetails> occupancyList = new ArrayList<occupancyDetails>();
			Elements occupancyListElement = roomListElement.get(i).getElementsByTag("Guest");
			for(int j = 0 ; j < occupancyListElement.size() ; j++)
			{
				occupancyDetails occupancy = new occupancyDetails();
				occupancy.setTitle					(occupancyListElement.get(j).getElementsByTag("Salutation").first().text());
				occupancy.setFirstName				(occupancyListElement.get(j).getElementsByTag("FirstName").first().text());
				occupancy.setLastName				(occupancyListElement.get(j).getElementsByTag("LastName").first().text());
				try {
					if(occupancyListElement.get(j).getElementsByTag("IsChild").size() > 0)
					{
						occupancy.setType			("Child");
						occupancy.setAge			(occupancyListElement.get(j).getElementsByTag("Age").first().text());
					}
					else
					{
						occupancy.setType			("Adult");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				occupancyList.add(occupancy);
			}
			room.setOccupancyDetails(occupancyList);
			roomList.add(room);
		}
		hotelDetails.setRoomDetails(roomList);
		ArrayList<cancellationDetails>  	cancellationList			= new ArrayList<cancellationDetails>();
		Elements 							cancellationListElement 	= doc.getElementsByTag("CancellationInformation");
		for(int i = 0 ; i < cancellationListElement.size() ; i++)
		{
			cancellationDetails	cancellation = new cancellationDetails();
			cancellation.setStartDate				(cancellationListElement.get(i).getElementsByTag("StartDate").first().text());
			cancellation.setEndDate					(cancellationListElement.get(i).getElementsByTag("EndDate").first().text());
			cancellation.setCharge					(cancellationListElement.get(i).getElementsByTag("ChargeType").first().text());
			cancellation.setChargeAmount			(cancellationListElement.get(i).getElementsByTag("ChargeAmount").first().text());
			cancellation.setCurrency				(cancellationListElement.get(i).getElementsByTag("Currency").first().text());
			cancellationList.add(cancellation);
		}
		hotelDetails.setCancellation(cancellationList);
		hotelDetails.setSupConfirmationNumber			(doc.getElementsByTag("BookingId").first().text());
		hotelDetails.setPrice							(doc.getElementsByTag("BookingPrice").first().text());
		hotelDetails.setCurrencyCode					(doc.getElementsByTag("BookingCurrency").first().text());
		hotelDetails.setRoomCount						(doc.getElementsByTag("TotalRooms").first().text());
		return hotelDetails;
	}
}
