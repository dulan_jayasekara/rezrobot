package com.rezrobot.common.pojo;

public class HotelRates {
	String rate = "0";
	String pm = "0";
	String bf = "0";
	String ccFee = "0";
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getPm() {
		return pm;
	}
	public void setPm(String pm) {
		this.pm = pm;
	}
	public String getBf() {
		return bf;
	}
	public void setBf(String bf) {
		this.bf = bf;
	}
	public String getCcFee() {
		return ccFee;
	}
	public void setCcFee(String ccFee) {
		this.ccFee = ccFee;
	}
	
	
}
