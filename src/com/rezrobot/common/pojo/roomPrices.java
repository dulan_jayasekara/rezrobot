package com.rezrobot.common.pojo;

import java.io.Serializable;

public class roomPrices implements Serializable{

	String roomCode = ""; 
	String numberOfRooms = ""; 
	String price = ""; 
	String checkin = ""; 
	String checkout = ""; 
	String nights = ""; 
	String priceDross = ""; 
	String childAge = "";
	
	
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getNumberOfRooms() {
		return numberOfRooms;
	}
	public void setNumberOfRooms(String numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getPriceDross() {
		return priceDross;
	}
	public void setPriceDross(String priceDross) {
		this.priceDross = priceDross;
	}
	public String getChildAge() {
		return childAge;
	}
	public void setChildAge(String childAge) {
		this.childAge = childAge;
	}
	
	
	
}
