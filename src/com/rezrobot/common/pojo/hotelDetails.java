package com.rezrobot.common.pojo;

import java.io.Serializable;
import java.util.ArrayList;

public class hotelDetails implements Serializable{

	String user = "";
	String password = "";
	String city = "";
	String cityCode = ""; 
	String country = "";
	String countryCode = "";
	String hotel = ""; 
	String hotelCode = ""; 
	String hotelType = "";
	String location = ""; 
	String locationCode = ""; 
	String starRating = ""; 
	String hasMap = ""; 
	String hasExtraInfo = ""; 
	String hasPictures = ""; 
	String incomingOfficeCode = "";
	String classifiaction = ""; 
	String classificationCode = ""; 
	String checkin = ""; 
	String checkout = ""; 
	String currency = ""; 
	String currencyCode = ""; 
	String starRatingCode = ""; 
	String zone = ""; 
	String zoneType = ""; 
	String zoneCode = ""; 
	String childAgeFrom = ""; 
	String childAgeTo = ""; 
	String latitude = ""; 
	String longitude = ""; 
	String provider = ""; 
	String address = ""; 
	String category = ""; 
	String bestValue = ""; 
	String thumb = ""; 
	String minAvgPrice = ""; 
	String description = ""; 
	String descriptionType = "";
	String brandId = ""; 
	String brandName = "";
	String roomCount = "";
	String error = "";
	String availabilityStatus = "";
	String price = "";
	String firstName = "";
	String lastName = "";
	String supConfirmationNumber = "";
	String nights = "";
	String hotelReference = "";
	String email = "";
	String bookingDate = "";
	String referenceTime = "";
	String guestCountry = "";
	String sessionID = "";
	String agentCode = "";
	String agentBalance = "";
	String mealBasis = "";
	String mealBreakFast = "";
	
	ArrayList<gtaSecureExtraInfoList> 	gtaSecureExtraInfoList 	= new ArrayList<gtaSecureExtraInfoList>();
	ArrayList<roomDetails> 				roomDetails 			= new ArrayList<roomDetails>();
	ArrayList<hbAdditionalCost>   		hbAdditionalCost		= new ArrayList<hbAdditionalCost>();
	ArrayList<discount> 				discount				= new ArrayList<discount>();
	ArrayList<occupancyDetails> 		occupancy				= new ArrayList<occupancyDetails>();
	ArrayList<errorList> 				errorList				= new ArrayList<errorList>();
	ArrayList<cancellationDetails>  	cancellation			= new ArrayList<cancellationDetails>();
	
	
	
	public String getMealBreakFast() {
		return mealBreakFast;
	}
	public void setMealBreakFast(String mealBreakFast) {
		this.mealBreakFast = mealBreakFast;
	}
	public String getMealBasis() {
		return mealBasis;
	}
	public void setMealBasis(String mealBasis) {
		this.mealBasis = mealBasis;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getAgentBalance() {
		return agentBalance;
	}
	public void setAgentBalance(String agentBalance) {
		this.agentBalance = agentBalance;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getGuestCountry() {
		return guestCountry;
	}
	public void setGuestCountry(String guestCountry) {
		this.guestCountry = guestCountry;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getReferenceTime() {
		return referenceTime;
	}
	public void setReferenceTime(String referenceTime) {
		this.referenceTime = referenceTime;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getHotelReference() {
		return hotelReference;
	}
	public void setHotelReference(String hotelReference) {
		this.hotelReference = hotelReference;
	}
	public ArrayList<cancellationDetails> getCancellation() {
		return cancellation;
	}
	public void setCancellation(ArrayList<cancellationDetails> cancellation) {
		this.cancellation = cancellation;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getSupConfirmationNumber() {
		return supConfirmationNumber;
	}
	public void setSupConfirmationNumber(String supConfirmationNumber) {
		this.supConfirmationNumber = supConfirmationNumber;
	}
	public ArrayList<errorList> getErrorList() {
		return errorList;
	}
	public void setErrorList(ArrayList<errorList> errorList) {
		this.errorList = errorList;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public ArrayList<occupancyDetails> getOccupancy() {
		return occupancy;
	}
	public void setOccupancy(ArrayList<occupancyDetails> occupancy) {
		this.occupancy = occupancy;
	}
	public ArrayList<discount> getDiscount() {
		return discount;
	}
	public void setDiscount(ArrayList<discount> discount) {
		this.discount = discount;
	}
	public ArrayList<hbAdditionalCost> getHbAdditionalCost() {
		return hbAdditionalCost;
	}
	public void setHbAdditionalCost(ArrayList<hbAdditionalCost> hbAdditionalCost) {
		this.hbAdditionalCost = hbAdditionalCost;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDescriptionType() {
		return descriptionType;
	}
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}
	public String getAvailabilityStatus() {
		return availabilityStatus;
	}
	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public ArrayList<roomDetails> getRoomDetails() {
		return roomDetails;
	}
	public void setRoomDetails(ArrayList<roomDetails> roomDetails) {
		this.roomDetails = roomDetails;
	}
	public String getRoomCount() {
		return roomCount;
	}
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}
	public ArrayList<gtaSecureExtraInfoList> getGtaSecureExtraInfoList() {
		return gtaSecureExtraInfoList;
	}
	public void setGtaSecureExtraInfoList(
			ArrayList<gtaSecureExtraInfoList> gtaSecureExtraInfoList) {
		this.gtaSecureExtraInfoList = gtaSecureExtraInfoList;
	}
	
	
	public String getHotelType() {
		return hotelType;
	}
	public void setHotelType(String hotelType) {
		this.hotelType = hotelType;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getHotel() {
		return hotel;
	}
	public void setHotel(String hotel) {
		this.hotel = hotel;
	}
	public String getHotelCode() {
		return hotelCode;
	}
	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getStarRating() {
		return starRating;
	}
	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}
	public String getHasMap() {
		return hasMap;
	}
	public void setHasMap(String hasMap) {
		this.hasMap = hasMap;
	}
	public String getHasExtraInfo() {
		return hasExtraInfo;
	}
	public void setHasExtraInfo(String hasExtraInfo) {
		this.hasExtraInfo = hasExtraInfo;
	}
	public String getHasPictures() {
		return hasPictures;
	}
	public void setHasPictures(String hasPictures) {
		this.hasPictures = hasPictures;
	}
	public String getIncomingOfficeCode() {
		return incomingOfficeCode;
	}
	public void setIncomingOfficeCode(String incomingOfficeCode) {
		this.incomingOfficeCode = incomingOfficeCode;
	}
	public String getClassifiaction() {
		return classifiaction;
	}
	public void setClassifiaction(String classifiaction) {
		this.classifiaction = classifiaction;
	}
	public String getClassificationCode() {
		return classificationCode;
	}
	public void setClassificationCode(String classificationCode) {
		this.classificationCode = classificationCode;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getStarRatingCode() {
		return starRatingCode;
	}
	public void setStarRatingCode(String starRatingCode) {
		this.starRatingCode = starRatingCode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getZoneType() {
		return zoneType;
	}
	public void setZoneType(String zoneType) {
		this.zoneType = zoneType;
	}
	public String getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getChildAgeFrom() {
		return childAgeFrom;
	}
	public void setChildAgeFrom(String childAgeFrom) {
		this.childAgeFrom = childAgeFrom;
	}
	public String getChildAgeTo() {
		return childAgeTo;
	}
	public void setChildAgeTo(String childAgeTo) {
		this.childAgeTo = childAgeTo;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getBestValue() {
		return bestValue;
	}
	public void setBestValue(String bestValue) {
		this.bestValue = bestValue;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	public String getMinAvgPrice() {
		return minAvgPrice;
	}
	public void setMinAvgPrice(String minAvgPrice) {
		this.minAvgPrice = minAvgPrice;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	} 
	
	
	
}
