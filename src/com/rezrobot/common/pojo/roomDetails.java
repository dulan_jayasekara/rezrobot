package com.rezrobot.common.pojo;

import java.io.Serializable;
import java.util.ArrayList;

public class roomDetails implements Serializable{

	String roomID = "";
	String roomCode = "";
	String roomName = ""; 
	String price = ""; 
	String comPercentage = ""; 
	String currency = ""; 
	String availability = ""; 
	String availabilityCode = ""; 
	String sharingBedding = ""; 
	String board = ""; 
	String boardCode = ""; 
	String roomCount = ""; 
	String adultCount = ""; 
	String childCount = ""; 
	String roomAvailableCount = ""; 
	String boardType = ""; 
	String roomType = ""; 
	String roomCharacteristics = ""; 
	String nights = ""; 
	String checkin = ""; 
	String checkout = "";
	String hotelRoomTypeId = ""; 
	String occupancyId = ""; 
	String maxAdult = ""; 
	String maxChild = ""; 
	String tax = ""; 
	String doubleBed = ""; 
	String avgNightPrice = ""; 
	String boardPrice = ""; 
	String roomSequenceNumber = "";
	String cancellationAmount = "";
	String cancellationFrom = "";
	String canellationTo = "";
	String status = "";
	String bedType = "-";
	String numberOfCots = "";
	String mealBasis  = "";
	String mealBreakfast = "";
	String numberOfRooms = "";
	String childAge = "";
	String extraBed = "";
	String extraBedCount = "";
	String description = "";
	String cancellationTime = "";
	
	
	ArrayList<roomDetails> 				roomDetails 			= new ArrayList<roomDetails>();
	ArrayList<touricoAvailableOffset> 	touricoAvailableOffset 	= new ArrayList<touricoAvailableOffset>();
	ArrayList<touricoPriceOffset> 		touricoPriceOffset 		= new ArrayList<touricoPriceOffset>();
	ArrayList<occupancyDetails> 		occupancyDetails		= new ArrayList<occupancyDetails>();
	
	
	
	public String getCancellationTime() {
		return cancellationTime;
	}
	public void setCancellationTime(String cancellationTime) {
		this.cancellationTime = cancellationTime;
	}
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getNumberOfRooms() {
		return numberOfRooms;
	}
	public void setNumberOfRooms(String numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}
	public String getChildAge() {
		return childAge;
	}
	public void setChildAge(String childAge) {
		this.childAge = childAge;
	}
	public String getExtraBed() {
		return extraBed;
	}
	public void setExtraBed(String extraBed) {
		this.extraBed = extraBed;
	}
	public String getExtraBedCount() {
		return extraBedCount;
	}
	public void setExtraBedCount(String extraBedCount) {
		this.extraBedCount = extraBedCount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getMealBasis() {
		return mealBasis;
	}
	public void setMealBasis(String mealBasis) {
		this.mealBasis = mealBasis;
	}
	public String getMealBreakfast() {
		return mealBreakfast;
	}
	public void setMealBreakfast(String mealBreakfast) {
		this.mealBreakfast = mealBreakfast;
	}
	public String getNumberOfCots() {
		return numberOfCots;
	}
	public void setNumberOfCots(String numberOfCots) {
		this.numberOfCots = numberOfCots;
	}
	public String getBedType() {
		return bedType;
	}
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCanellationTo() {
		return canellationTo;
	}
	public void setCanellationTo(String canellationTo) {
		this.canellationTo = canellationTo;
	}
	public String getCancellationAmount() {
		return cancellationAmount;
	}
	public void setCancellationAmount(String cancellationAmount) {
		this.cancellationAmount = cancellationAmount;
	}
	public String getCancellationFrom() {
		return cancellationFrom;
	}
	public void setCancellationFrom(String cancellationFrom) {
		this.cancellationFrom = cancellationFrom;
	}
	public String getCacnellationTo() {
		return canellationTo;
	}
	public void setCacnellationTo(String cacnellationTo) {
		this.canellationTo = cacnellationTo;
	}
	public ArrayList<occupancyDetails> getOccupancyDetails() {
		return occupancyDetails;
	}
	public void setOccupancyDetails(ArrayList<occupancyDetails> occupancyDetails) {
		this.occupancyDetails = occupancyDetails;
	}
	public ArrayList<roomDetails> getRoomDetails() {
		return roomDetails;
	}
	public void setRoomDetails(ArrayList<roomDetails> roomDetails) {
		this.roomDetails = roomDetails;
	}
	public ArrayList<touricoAvailableOffset> getTouricoAvailableOffset() {
		return touricoAvailableOffset;
	}
	public void setTouricoAvailableOffset(
			ArrayList<touricoAvailableOffset> touricoAvailableOffset) {
		this.touricoAvailableOffset = touricoAvailableOffset;
	}
	public ArrayList<touricoPriceOffset> getTouricoPriceOffset() {
		return touricoPriceOffset;
	}
	public void setTouricoPriceOffset(
			ArrayList<touricoPriceOffset> touricoPriceOffset) {
		this.touricoPriceOffset = touricoPriceOffset;
	}
	public String getRoomID() {
		return roomID;
	}
	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getComPercentage() {
		return comPercentage;
	}
	public void setComPercentage(String comPercentage) {
		this.comPercentage = comPercentage;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getAvailabilityCode() {
		return availabilityCode;
	}
	public void setAvailabilityCode(String availabilityCode) {
		this.availabilityCode = availabilityCode;
	}
	public String getSharingBedding() {
		return sharingBedding;
	}
	public void setSharingBedding(String sharingBedding) {
		this.sharingBedding = sharingBedding;
	}
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getBoardCode() {
		return boardCode;
	}
	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}
	public String getRoomCount() {
		return roomCount;
	}
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}
	public String getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	public String getChildCount() {
		return childCount;
	}
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	public String getRoomAvailableCount() {
		return roomAvailableCount;
	}
	public void setRoomAvailableCount(String roomAvailableCount) {
		this.roomAvailableCount = roomAvailableCount;
	}
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRoomCharacteristics() {
		return roomCharacteristics;
	}
	public void setRoomCharacteristics(String roomCharacteristics) {
		this.roomCharacteristics = roomCharacteristics;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getHotelRoomTypeId() {
		return hotelRoomTypeId;
	}
	public void setHotelRoomTypeId(String hotelRoomTypeId) {
		this.hotelRoomTypeId = hotelRoomTypeId;
	}
	public String getOccupancyId() {
		return occupancyId;
	}
	public void setOccupancyId(String occupancyId) {
		this.occupancyId = occupancyId;
	}
	public String getMaxAdult() {
		return maxAdult;
	}
	public void setMaxAdult(String maxAdult) {
		this.maxAdult = maxAdult;
	}
	public String getMaxChild() {
		return maxChild;
	}
	public void setMaxChild(String maxChild) {
		this.maxChild = maxChild;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getDoubleBed() {
		return doubleBed;
	}
	public void setDoubleBed(String doubleBed) {
		this.doubleBed = doubleBed;
	}
	public String getAvgNightPrice() {
		return avgNightPrice;
	}
	public void setAvgNightPrice(String avgNightPrice) {
		this.avgNightPrice = avgNightPrice;
	}
	public String getBoardPrice() {
		return boardPrice;
	}
	public void setBoardPrice(String boardPrice) {
		this.boardPrice = boardPrice;
	}
	public String getRoomSequenceNumber() {
		return roomSequenceNumber;
	}
	public void setRoomSequenceNumber(String roomSequenceNumber) {
		this.roomSequenceNumber = roomSequenceNumber;
	}
	
	
}
