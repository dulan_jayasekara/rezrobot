package com.rezrobot.common.pojo;

import java.io.Serializable;

public class hbAdditionalCost implements Serializable{

	String costType = "";
	String price = "";
	
	public String getCostType() {
		return costType;
	}
	public void setCostType(String costType) {
		this.costType = costType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
