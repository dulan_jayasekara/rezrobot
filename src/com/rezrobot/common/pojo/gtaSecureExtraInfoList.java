package com.rezrobot.common.pojo;

import java.io.Serializable;

public class gtaSecureExtraInfoList implements Serializable{

	String extendedDataType = "";
	String name = "";
	String value = "";
	public String getExtendedDataType() {
		return extendedDataType;
	}
	public void setExtendedDataType(String extendedDataType) {
		this.extendedDataType = extendedDataType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
