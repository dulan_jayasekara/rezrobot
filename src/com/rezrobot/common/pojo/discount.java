package com.rezrobot.common.pojo;

import java.io.Serializable;

public class discount implements Serializable{

	String unitCount = "";
	String paxCount = "";
	String amount = "";
	String checkin = "";
	String checkout = "";
	String description = "";
	public String getUnitCount() {
		return unitCount;
	}
	public void setUnitCount(String unitCount) {
		this.unitCount = unitCount;
	}
	public String getPaxCount() {
		return paxCount;
	}
	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
