package com.rezrobot.common.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelSearchObject;

public class HotelObject implements Serializable{

	HotelSearchObject 					search 				= new HotelSearchObject();
	HotelDetails 						selectedHotel 		= new HotelDetails();
	HashMap<String, String> 			portalSpecificData 	= new HashMap<>();
	HashMap<String, String> 			hotelLabels 		= new HashMap<>();
	HashMap<String, String> 			commonLabels 		= new HashMap<>();
	hotelDetails 						reservationRes 		= new hotelDetails();
	HashMap<String, String> 			paymentDetails      = new HashMap<>();
	HashMap<String, String> 			configDetails		= new HashMap<>();
	ArrayList<HashMap<String, String>> 	websiteDetails 		= new ArrayList<HashMap<String, String>>();
	String canDeadline = "";
	String defaultCurrency = "";
	HashMap<String, String> 			currencyMap  		= new HashMap<>();
	HotelConfirmationPageDetails 		confirmation 		= new HotelConfirmationPageDetails();
	ArrayList<HotelOccupancyObject> 	occupancyList 		= new ArrayList<HotelOccupancyObject>();
	hotelDetails						roomAvailablityRes 	= new hotelDetails();
	ArrayList<hotelDetails> 			hotelAvailabilityRes= new ArrayList<hotelDetails>();
	boolean								isConfirmed			= false;
	String								reportDate			= "";
	
	
	
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public HotelSearchObject getSearch() {
		return search;
	}
	public void setSearch(HotelSearchObject search) {
		this.search = search;
	}
	public HotelDetails getSelectedHotel() {
		return selectedHotel;
	}
	public void setSelectedHotel(HotelDetails selectedHotel) {
		this.selectedHotel = selectedHotel;
	}
	public HashMap<String, String> getPortalSpecificData() {
		return portalSpecificData;
	}
	public void setPortalSpecificData(HashMap<String, String> portalSpecificData) {
		this.portalSpecificData = portalSpecificData;
	}
	
	public HashMap<String, String> getHotelLabels() {
		return hotelLabels;
	}
	public void setHotelLabels(HashMap<String, String> hotelLabels) {
		this.hotelLabels = hotelLabels;
	}
	public HashMap<String, String> getCommonLabels() {
		return commonLabels;
	}
	public void setCommonLabels(HashMap<String, String> commonLabels) {
		this.commonLabels = commonLabels;
	}
	public hotelDetails getReservationRes() {
		return reservationRes;
	}
	public void setReservationRes(hotelDetails reservationRes) {
		this.reservationRes = reservationRes;
	}
	public HashMap<String, String> getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(HashMap<String, String> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public HashMap<String, String> getConfigDetails() {
		return configDetails;
	}
	public void setConfigDetails(HashMap<String, String> configDetails) {
		this.configDetails = configDetails;
	}
	public ArrayList<HashMap<String, String>> getWebsiteDetails() {
		return websiteDetails;
	}
	public void setWebsiteDetails(ArrayList<HashMap<String, String>> websiteDetails) {
		this.websiteDetails = websiteDetails;
	}
	public String getCanDeadline() {
		return canDeadline;
	}
	public void setCanDeadline(String canDeadline) {
		this.canDeadline = canDeadline;
	}
	public String getDefaultCurrency() {
		return defaultCurrency;
	}
	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}
	public HashMap<String, String> getCurrencyMap() {
		return currencyMap;
	}
	public void setCurrencyMap(HashMap<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}
	public HotelConfirmationPageDetails getConfirmation() {
		return confirmation;
	}
	public void setConfirmation(HotelConfirmationPageDetails confirmation) {
		this.confirmation = confirmation;
	}
	public ArrayList<HotelOccupancyObject> getOccupancyList() {
		return occupancyList;
	}
	public void setOccupancyList(ArrayList<HotelOccupancyObject> occupancyList) {
		this.occupancyList = occupancyList;
	}
	public hotelDetails getRoomAvailablityRes() {
		return roomAvailablityRes;
	}
	public void setRoomAvailablityRes(hotelDetails roomAvailablityRes) {
		this.roomAvailablityRes = roomAvailablityRes;
	}
	public ArrayList<hotelDetails> getHotelAvailabilityRes() {
		return hotelAvailabilityRes;
	}
	public void setHotelAvailabilityRes(ArrayList<hotelDetails> hotelAvailabilityRes) {
		this.hotelAvailabilityRes = hotelAvailabilityRes;
	}
	public boolean isConfirmed() {
		return isConfirmed;
	}
	public void setConfirmed(boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}
	
	
}
