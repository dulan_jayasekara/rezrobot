package com.rezrobot.common.objects;

import java.io.Serializable;

import com.rezrobot.flight_circuitry.FlightReservation;


public class Reservation implements Serializable{

	String product = "";
	FlightReservation 	flightReservation 	= new FlightReservation();
	HotelObject 		hotelReservation 	= new HotelObject();
	ActivityReservation activityReservation = new ActivityReservation();
	VacationObject		vacationReservation	= new VacationObject();
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public FlightReservation getFlightReservation() {
		return flightReservation;
	}
	public void setFlightReservation(FlightReservation flightReservation) {
		this.flightReservation = flightReservation;
	}
	public HotelObject getHotelReservation() {
		return hotelReservation;
	}
	public void setHotelReservation(HotelObject hotelReservation) {
		this.hotelReservation = hotelReservation;
	}
	public ActivityReservation getActivityReservation() {
		return activityReservation;
	}
	public void setActivityReservation(ActivityReservation activityReservation) {
		this.activityReservation = activityReservation;
	}
	public VacationObject getVacationReservation() {
		return vacationReservation;
	}
	public void setVacationReservation(VacationObject vacationReservation) {
		this.vacationReservation = vacationReservation;
	}
	
	
	
}
