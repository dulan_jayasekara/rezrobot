package com.rezrobot.enumtypes;

public enum DaysOftheWeek {

	Sunday, Monday, Tuesday, Wednesday, Thurday, Friday, Saturday, ALL, None;

	public static DaysOftheWeek getDaysOftheWeek(String Action) {
		if (Action.equalsIgnoreCase("su"))
			return Sunday;

		else if (Action.equalsIgnoreCase("mo"))
			return Monday;
		else if (Action.equalsIgnoreCase("tu"))
			return Tuesday;

		else if (Action.equalsIgnoreCase("we"))
			return Wednesday;

		else if (Action.equalsIgnoreCase("th"))
			return Thurday;

		else if (Action.equalsIgnoreCase("fr"))
			return Friday;

		else if (Action.equalsIgnoreCase("sa"))
			return Saturday;

		else if (Action.equalsIgnoreCase("all"))
			return ALL;

		else

			return None;

	}

}
