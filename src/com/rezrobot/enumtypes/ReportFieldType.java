package com.rezrobot.enumtypes;

public enum ReportFieldType {
	
	reportFrom, reportTo, country, city, hotelName, programName, supplierName, searchBy, contentOf, searchByOption, viewReport;
}
