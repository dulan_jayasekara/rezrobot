package com.rezrobot.verifications;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;

import com.rezrobot.dataobjects.DMCSearchObject;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;
import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.CurrencyConverter;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.TaxCalculator;

public class DMC_Verify {

	private CurrencyConverter converter = new CurrencyConverter();
	private TaxCalculator tax = new TaxCalculator();

	public void webHomePage(ExtentReportTemplate extentReportTemplate, Web_Com_HomePage home, LabelReadProperties labelProperty, DMCSearchObject search) throws Exception {

		extentReportTemplate.verifyTrue(true, home.isPageAvailable(), "General Home Page Validations - Home Page Availability");
		extentReportTemplate.verifyTrue("Rezgateway", home.getPageTitel().trim(), "General Home Page Validations - Home Page Titel Validation");
		extentReportTemplate.verifyTrue(true, home.getBECAvailability(), "General Home Page Validations - Booking Engine Availability");

		Boolean isBecLoaded = home.isDMCBECLoaded();

		HashMap<String, String> DMCBecLabels = home.getLabelTexts_FP();
		HashMap<String, Boolean> DMCBecInputFilelds = home.getAvailabilityOfInputFields_FP();

		LabelReadProperties labelReader = new LabelReadProperties();
		labelReader.getDMCLabelProperties();

		HashMap<String, String> PackageBecComboBox = home.getFixDMCPackagesComboBoxDataList();

		// DMC bec input fields availability

		try {

			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("cor"), "General Home Page Validations - country of residence");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("includingflight"), "General Home Page Validations DMC Package - including flight option");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("excludingflight"), "General Home Page Validations DMC Package - Excluding flight option");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("origin"), "General Home Page Validations DMC Package - origin(leaving from)");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("destination"), "General Home Page Validations DMC Package - Destination");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("departuremonth"), "General Home Page Validations DMC Package - DepatureMonthinputfield");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("duration"), "General Home Page Validations DMC Package - Duration Inputfield");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("package_type"), "General Home Page Validations DMC Package - package type Inputfield");
			extentReportTemplate.verifyTrue(true, DMCBecInputFilelds.get("prefferedcurrency_FP"), "General Home Page Validations DMC Package - prefferedcurrency FP input field");

		} catch (Exception e) {
			// TODO: handle exception
		}

		// check bec label availability

		try {

			HashMap<String, String> labelPropertyMap = labelProperty.getDMCLabelProperties();

			extentReportTemplate.verifyTrue(labelPropertyMap.get("CORlabel"), DMCBecLabels.get("CORlabel"), "Country of Residence Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("Tourplanlabel"), DMCBecLabels.get("Tourplanlabel"), "Tour Plan Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("FlightInclusivelabel"), DMCBecLabels.get("FlightInclusivelabel"), "Flight inclusive option Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("Flightexclusivelabel"), DMCBecLabels.get("Flightexclusivelabel"), "flight exclusive option Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("departureairportlabel"), DMCBecLabels.get("departureairportlabel"), "departure airport Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("destinationairportlabel"), DMCBecLabels.get("destinationairportlabel"), "Destination Airport Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("destinationcitylabel"), DMCBecLabels.get("destinationcitylabel"), "Destination city Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("departurelabel"), DMCBecLabels.get("departurelabel"), "Tour plan-Time Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("departuremonthlabel"), DMCBecLabels.get("departuremonthlabel"), "Departure month Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("durationlabel"), DMCBecLabels.get("durationlabel"), "Package Duration Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("packagetypelabel"), DMCBecLabels.get("packagetypelabel"), "Package Type Label Validation");
			extentReportTemplate.verifyTrue(labelPropertyMap.get("preferredcurrencylabel"), DMCBecLabels.get("preferredcurrencylabel"), "PreferredCurrency Label Validation");
			// extentReportTemplate.verifyTrue(labelPropertyMap.get("showsearchoptionslabel"), DMCBecLabels.get("showsearchoptionslabel"), "Search more options Label Validation");

		} catch (Exception e) {
			// TODO: handle exception
		}

		// check for DMC Bec combobox availability
		try {
			HashMap<String, String> portalSpecificData = labelProperty.getPortalSpecificData();

			// PrintTemplate.setTableHeading("Combo Box Attributes Validation");
			extentReportTemplate.verifyTrue(portalSpecificData.get("countryList"), PackageBecComboBox.get("countryOfResidence").replace("\n", " "), "Combo Box Attributes Validation - Country List verification");
			extentReportTemplate.verifyTrue(new SupportFunctions().getFPDepartureMonths(), PackageBecComboBox.get("Departure month").replace("\n", " "), "Combo Box Attributes Validation - Depature month List verification");

			extentReportTemplate.verifyTrue(portalSpecificData.get("DMCPackageDurations"), PackageBecComboBox.get("Duration").replace("\n", " "), "Combo Box Attributes Validation - package Duration List verification");
			extentReportTemplate.verifyTrue(portalSpecificData.get("PackageTypes"), PackageBecComboBox.get("Package type").replace("\n", " "), "Combo Box Attributes Validation - package type List verification");

			extentReportTemplate.verifyTrue(portalSpecificData.get("preferredCurrency"), PackageBecComboBox.get("Prefferred Currency").replace("\n", " "), "Combo Box Attributes Validation - Preferred currency verification");

		}

		catch (Exception e) {

		}

	}

	public void resultsPageVerification(ExtentReportTemplate extentReportTemplate, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificDetails, HashMap<String, String> selectedFlightDetails,
			FareResponse fareResponse, DMCSearchObject search, PriceResponse priceResponse) throws ParseException {
		// Flight details verification dmc flight segment

		try {
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getOperatingAirlineCode() + fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getFlightNo(),
					selectedFlightDetails.get("OutBound Flight Number"), "OutBound Flight Number Verification");

			extentReportTemplate.verifyTrue(Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureDate()), selectedFlightDetails.get("OutBound Flight Departure Date"),
					"Flight origing Depature Date Verification");

			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getOperatingAirline(), selectedFlightDetails.get("Outbound Flight depature Airline Name"),
					"Flight Origin Depature airlinename Verification");
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureTime().split(":")[0] + ":"
					+ fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureTime().split(":")[1], selectedFlightDetails.get("OutBound Flight Departure Time"), "OutBound Flight Departure Time Verification");
			
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureLocationCode(), selectedFlightDetails.get("OutBound Flight Departure Airport Code"),
					"OutBound Flight Depature Airport Code Verification");
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDeparture_port(), selectedFlightDetails.get("OutBound Flight Departure City"), "OutBound Flight Depature City Verification");
			
			
			
			
			
			
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getArrival_port(), selectedFlightDetails.get("OutBound Flight Arrival City"), "OutBound Flight Arrival City Verification");

			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getArrivalLocationCode(), selectedFlightDetails.get("OutBound Flight Arrival Airport Code"),
					"OutBound Flight Arrival Airport Code Verification");

			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getArrivalTime().split(":")[0] + ":"+ fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getArrivalTime().split(":")[1], selectedFlightDetails.get("OutBound Flight Arrival Time"), "OutBound Flight Arrival Time Verification");

			
			
			
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDepartureTime().split(":")[0]+":"+fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDepartureTime().split(":")[1], selectedFlightDetails.get("InBound Flight Departure Time"), "InBound Flight Departure Time Verification");
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDepartureLocationCode(), selectedFlightDetails.get("InBound Flight Departure Air Port Code"), "Inbound Flight Depature Airport Code Verification");
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDeparture_port(), selectedFlightDetails.get("InBound Flight Departure City"), "InBound Flight Departure City Verification");
			
			
			
			
			
			
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getArrivalTime().split(":")[0]+":"+fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getArrivalTime().split(":")[1], selectedFlightDetails.get("InBound Flight Arrival Time"), "InBound Flight Arrival Time Verification");
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getArrivalLocationCode(), selectedFlightDetails.get("InBound Flight Arrival Airport Code"), "InBound Flight Arrival Airport Code Verification");

			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getArrival_port(), selectedFlightDetails.get("InBound Flight Arrival City"), "InBound Flight Arrival City Verification");
			
			

			
			
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getOperatingAirlineCode()+ fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getFlightNo(),
					selectedFlightDetails.get("InBound Flight Number"), "InBound Flight Number Verification");
			
			
			extentReportTemplate.verifyTrue(Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getArrivalDate()), selectedFlightDetails.get("InBound Flight Arrival Date"),
					"InBound Fligh arrival Date Verification");
			
			
			extentReportTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getOperatingAirline(), selectedFlightDetails.get("InBound Flight depature Airline Name"),
					"InBound Flight depature Airline Name Verification");
			
			
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
