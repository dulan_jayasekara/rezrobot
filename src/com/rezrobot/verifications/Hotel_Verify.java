package com.rezrobot.verifications;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.remote.server.handler.mobile.GetNetworkConnection;

import com.rezrobot.common.pojo.cancellationDetails;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.CommonBillingInfoRates;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelPaymentPageMoreDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pages.web.common.Web_Com_BillingInformationPage;
import com.rezrobot.pages.web.common.Web_Com_CustomerDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.pages.web.common.Web_Com_NotesPage;
import com.rezrobot.pages.web.common.Web_Com_TermsPage;
import com.rezrobot.pages.web.hotel.Web_Hotel_paymentOccupancyPage;
import com.rezrobot.pages.web.hotel.Web_Hotel_resultsPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.TaxCalculator;
import com.rezrobot.utill.CurrencyConverter;


public class Hotel_Verify {

	private CurrencyConverter 	converter 	= new CurrencyConverter();
	private TaxCalculator 		tax 		= new TaxCalculator();
	
	public void webHomePage(ReportTemplate PrintTemplate, Web_Com_HomePage home, LabelReadProperties labelProperty, HotelSearchObject search) throws Exception
	{
//		PrintTemplate.setTableHeading	("General Home Page Validations");		
		PrintTemplate.verifyTrue		(true, home.isPageAvailable(),"General Home Page Validations - Home Page Availability");
		PrintTemplate.verifyTrue		("Rezgateway", home.getPageTitel().trim(), "General Home Page Validations - Home Page Titel Validation");
//		PrintTemplate.markTableEnd();
//		PrintTemplate.setTableHeading	("Booking Engine Validations");
		PrintTemplate.verifyTrue		(true, home.getBECAvailability(), "General Home Page Validations - Booking Engine Availability");
//		PrintTemplate.markTableEnd();
		
		Boolean 					isBecLoaded 			= home.isHotelBECLoaded();
		HashMap<String, String> 	hotelBecLabels 			= home.getHotelBECLabels();
		HashMap<String, Boolean> 	hotelBecInputFilelds 	= home.getHotelAvailabilityOfInputFields();
		LabelReadProperties 		labelReader 			= new LabelReadProperties();
		labelReader.getHotelLabelProperties();
		HashMap<String, String> 	hotelBecComboBox  		= home.getHotelComboBoxDataList(search);
		//home.getHotelMandatoryList();
		
		//Check bec hotel inputfield availability
		try {
//			PrintTemplate.setTableHeading("Hotel Booking Engine inputfiled Availability Check");
			PrintTemplate.verifyTrue(true, hotelBecInputFilelds.get("whereAreYouGoing"), 	"General Home Page Validations - Destination");
			PrintTemplate.verifyTrue(true, hotelBecInputFilelds.get("hotelName"), 			"General Home Page Validations - Hotel Name");
			PrintTemplate.verifyTrue(true, hotelBecInputFilelds.get("promotionCode"), 		"General Home Page Validations - Prmotion Code");
			PrintTemplate.verifyTrue(true, hotelBecInputFilelds.get("from"), 				"General Home Page Validations - Price range from");
			PrintTemplate.verifyTrue(true, hotelBecInputFilelds.get("to"), 					"General Home Page Validations - Price range to");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check bec label availability
		try {
			HashMap<String, String> labelPropertyMap = labelProperty.getHotelLabelProperties();
//			PrintTemplate.setTableHeading("Booking Engine - Label Text Validations");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becCountryOfResidence"), 	hotelBecLabels.get("countryOfResidence") , 	"Booking Engine - Label Text Validations - Country of Residence label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becWhereAreYouGoing"), 		hotelBecLabels.get("whereAreYouGoing") , 	"Booking Engine - Label Text Validations - Destination label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becWhenAreYouGoing"), 		hotelBecLabels.get("WhenAreYouGoing") , 	"Booking Engine - Label Text Validations - Date label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becCheckin"), 				hotelBecLabels.get("checkin") , 			"Booking Engine - Label Text Validations - Checkin label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becNights"), 				hotelBecLabels.get("nights") , 				"Booking Engine - Label Text Validations - Nights label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becCheckout"), 				hotelBecLabels.get("checkout") , 			"Booking Engine - Label Text Validations - Checkout label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becRooms"), 					hotelBecLabels.get("rooms") , 				"Booking Engine - Label Text Validations - Rooms label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becHowManyPeople"), 			hotelBecLabels.get("howManyPeople") , 		"Booking Engine - Label Text Validations - Pax count label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becRoom"), 					hotelBecLabels.get("room") , 				"Booking Engine - Label Text Validations - Room Number label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becAdults"), 				hotelBecLabels.get("adults") , 				"Booking Engine - Label Text Validations - Adult count label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becChildren"), 				hotelBecLabels.get("children") , 			"Booking Engine - Label Text Validations - Children count label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becShowAdditional"), 		hotelBecLabels.get("showAdditional") , 		"Booking Engine - Label Text Validations - Show additional option label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becStarRating"), 			hotelBecLabels.get("starRating") , 			"Booking Engine - Label Text Validations - Star rating label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becHotelType"), 				hotelBecLabels.get("hotelType") , 			"Booking Engine - Label Text Validations - Hotel type label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becHotelName"), 				hotelBecLabels.get("hotelName") , 			"Booking Engine - Label Text Validations - Hotel name label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becPreferredCurrency"), 		hotelBecLabels.get("preferredCurrency") , 	"Booking Engine - Label Text Validations - Preferred currency label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becPromotionCode"), 			hotelBecLabels.get("promotionCode") , 		"Booking Engine - Label Text Validations - Promotion code label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becHotelAvailability"), 		hotelBecLabels.get("hotelAvailability") , 	"Booking Engine - Label Text Validations - Hotel availability label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becAvailable"), 				hotelBecLabels.get("available") , 			"Booking Engine - Label Text Validations - Available label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becOnReq"), 					hotelBecLabels.get("onRequest") , 			"Booking Engine - Label Text Validations - On request label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becPriceRange"), 			hotelBecLabels.get("priceRange") , 			"Booking Engine - Label Text Validations - Price range label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becFrom"), 					hotelBecLabels.get("from") , 				"Booking Engine - Label Text Validations - Price range from label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becTo"), 					hotelBecLabels.get("to") , 					"Booking Engine - Label Text Validations - Price range to label verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("becHideAdditional"), 		hotelBecLabels.get("hideAdditional") , 		"Booking Engine - Label Text Validations - Hide additional option label verification");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// check for combobox availability
		try {
			HashMap<String, String> portalSpecificData = labelProperty.getPortalSpecificData();
//			PrintTemplate.setTableHeading("Combo Box Attributes Validation");
			PrintTemplate.verifyTrue(portalSpecificData.get("countryList"), 		hotelBecComboBox.get("countryOfResidence").replace("\n", " ") , 	"Combo Box Attributes Validation - Country List verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("numberOfNights"), 		hotelBecComboBox.get("nights").replace("\n", " ") , 				"Combo Box Attributes Validation - Number of nights verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("numberOfRooms"), 		hotelBecComboBox.get("rooms").replace("\n", " ") , 					"Combo Box Attributes Validation - Number of rooms verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("numberOfAdults"), 		hotelBecComboBox.get("adults").replace("\n", " ") , 				"Combo Box Attributes Validation - Number of adults verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("numberOfChildren"), 	hotelBecComboBox.get("children").replace("\n", " ") , 				"Combo Box Attributes Validation - Number of children verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("childAge"), 			hotelBecComboBox.get("age").replace("\n", " ") , 					"Combo Box Attributes Validation - Children age verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("starRating"), 			hotelBecComboBox.get("starRating").replace("\n", " ") ,	 			"Combo Box Attributes Validation - Star rating veriifcation");
			PrintTemplate.verifyTrue(portalSpecificData.get("hotelType"), 			hotelBecComboBox.get("hotelType").replace("\n", " ") , 				"Combo Box Attributes Validation - Hotel type verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("preferredCurrency"), 	hotelBecComboBox.get("preferredCurrency").replace("\n", " ") , 		"Combo Box Attributes Validation - Preferred currency verification");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		

		
		//check mandatory field availability
		/*HashMap<String, Boolean> MandatoryMap = home.getHotelMandatoryList();
		PrintTemplate.setTableHeading("Mandatory Status validation");
		PrintTemplate.verifyTrue(true, MandatoryMap.get("countryOfResidence"), "Mandatory validation Of to filed");
		PrintTemplate.markTableEnd(); */
	}
	
	public void webResultsPage(ReportTemplate PrintTemplate, Web_Hotel_resultsPage resultsPage, LabelReadProperties labelProperty) throws Exception
	{
		HashMap<String, String> labelPropertyMap = labelProperty.getHotelLabelProperties();
		
		HashMap<String, String> portalSpecificData = labelProperty.getPortalSpecificData();
		
		//check inputfield availability
		try {
//			PrintTemplate.setTableHeading("Results Page inputfields verification");
			HashMap<String, Boolean> hotelResultsPageInputFilelds = resultsPage.getHotelAvailabilityOfInputFields();
			PrintTemplate.verifyTrue(true, hotelResultsPageInputFilelds.get("hotelName"), 		"Results Page inputfields verification - Hotel name additional filters");
			PrintTemplate.verifyTrue(true, hotelResultsPageInputFilelds.get("promotionCode"), 	"Results Page inputfields verification - Promotion code additional filters");
			PrintTemplate.verifyTrue(true, hotelResultsPageInputFilelds.get("priceRangeFrom"), 	"Results Page inputfields verification - Price range from additional filters");
			PrintTemplate.verifyTrue(true, hotelResultsPageInputFilelds.get("priceRangeTo"), 	"Results Page inputfields verification - Price range to additional filters");
			PrintTemplate.verifyTrue(true, hotelResultsPageInputFilelds.get("hotelNameFilter"), "Results Page inputfields verification - Hotel name filter");
			PrintTemplate.verifyTrue(true, hotelResultsPageInputFilelds.get("suburb"), 			"Results Page inputfields verification - Suburb");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// check button availability
		try {
//			PrintTemplate.setTableHeading("Results Page button verification");
			HashMap<String, Boolean> hotelResultsPageButtons = resultsPage.getHotelAvailabilityOfButtons();
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("filterReset"), 			"Results Page button verification - Reset filters");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("moreDetails"), 			"Results Page button verification - Hotel more details");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("cancellationPolicy"), 	"Results Page button verification - Hotel cancellation policy (Internal)");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("book"), 				"Results Page button verification - Hotel book button");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("addAndCon"), 			"Results Page button verification - Hotel add and continue button");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("showMoreRooms"), 		"Results Page button verification - Show more rooms");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("overview"), 			"Results Page button verification - Additional info overview");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("location"), 			"Results Page button verification - Additional info location");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("reviewAndRatings"), 	"Results Page button verification - Additional info review and ratings");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("amenities"), 			"Results Page button verification - Additional info amenities");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("additionalInfo"), 		"Results Page button verification - Additional info additional info");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("emailToFrnd"), 			"Results Page button verification - Email to friend");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("sendEmail"), 			"Results Page button verification - Email to friend send mail");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("emailIsSentOk"), 		"Results Page button verification - Email to friend confirmation");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("resetFiltersLeft"), 	"Results Page button verification - Reset filter left side");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("resetFiltersLeftBottom"),	"Results Page button verification - Reset filters left side bottom");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("customerLogin"), 		"Results Page button verification - Customer login");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("home"), 				"Results Page button verification - Home");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("upSellingHotels"), 		"Results Page button verification - Up Selling links - Hotel");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("upSellingFlights"), 	"Results Page button verification - Up selling links - Flights");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("upSellingActivities"), 	"Results Page button verification - Up selling links - Activities");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("upSellingCars"), 		"Results Page button verification - Up selling links - Cars");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("upSellingTransfers"), 	"Results Page button verification - Up selling links - Transfers");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("showAdditionalFilters"), 	"Results Page button verification - Show additional filters");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("addAndConActivities"), 	"Results Page button verification - Add and continue - Activities");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("addConFlights"), 		"Results Page button verification - Add and continue - Flights");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("addAndConCars"), 		"Results Page button verification - Add and continue - Cars");
			PrintTemplate.verifyTrue(true, hotelResultsPageButtons.get("addAndConTransfers"), 	"Results Page button verification - Add and continue - Transfers");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		// check results page combo boxes
		try {
			HashMap<String, String> hotelResultsPageCombobox = resultsPage.getHotelComboBoxDataList();
//			PrintTemplate.setTableHeading("Results Page combobox verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("starRating"), 				hotelResultsPageCombobox.get("starRating").replace("\n", " ") , 		"Results Page combobox verification - Star rating");
			PrintTemplate.verifyTrue(portalSpecificData.get("hotelType"), 				hotelResultsPageCombobox.get("hotelType").replace("\n", " ") , 			"Results Page combobox verification - Hotel type");
			PrintTemplate.verifyTrue(portalSpecificData.get("preferredCurrency"), 		hotelResultsPageCombobox.get("preferredCurrency").replace("\n", " ") , 	"Results Page combobox verification - Preferred currency");
			PrintTemplate.verifyTrue(portalSpecificData.get("hotelAvailabilityTypes"), 	hotelResultsPageCombobox.get("hotelAvailability").replace("\n", " ") , 	"Results Page combobox verification - Hotel availability");
			PrintTemplate.verifyTrue(portalSpecificData.get("hotelNearByAttractions"), 	hotelResultsPageCombobox.get("nearByAttractions").replace("\n", " ") , 	"Results Page combobox verification - Near by attractions");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// check results page label availability
		try {
			HashMap<String, String> hotelResultsPageLabels = resultsPage.getHotelLabels();
//			PrintTemplate.setTableHeading("Results Page labels verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpShowAdditional"), 				hotelResultsPageLabels.get("showAdditional") , 				"Results Page labels verification - Show additional filters");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpStarRating"), 					hotelResultsPageLabels.get("additionalStarRating") , 		"Results Page labels verification - Additional filters - Star rating");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelType"), 					hotelResultsPageLabels.get("additionalHotelType") , 		"Results Page labels verification - Additional filters - Hotel type");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelName"),	 				hotelResultsPageLabels.get("additionalHotelName") , 		"Results Page labels verification - Additional filters - Hotel name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPreferredCurency"), 			hotelResultsPageLabels.get("additionalPreferredCurrency") , "Results Page labels verification - Additional filters - Preferred currency");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPromotionCode"), 				hotelResultsPageLabels.get("additionalPromotionCode") , 	"Results Page labels verification - Additional filters - Prmotion code");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPriceRangeFrom"), 				hotelResultsPageLabels.get("additionalPriceRangeFrom") , 	"Results Page labels verification - Additional filters - Price range from");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPriceRangeTo"), 				hotelResultsPageLabels.get("additionalPriceRangeTo") , 		"Results Page labels verification - Additional filters - Price range to");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpAvailable"), 					hotelResultsPageLabels.get("additionalAvailable") , 		"Results Page labels verification - Additional filters - Availability - Available");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpOnReq"), 						hotelResultsPageLabels.get("additionalOnReq") , 			"Results Page labels verification - Additional filters - Availability - On request");
			PrintTemplate.verifyTrue("London", 												hotelResultsPageLabels.get("location") , 					"Results Page labels verification - Hotel location"); //take from the excel
			PrintTemplate.verifyTrue("01", 													hotelResultsPageLabels.get("hotelCount") , 					"Results Page labels verification - Number of hotels"); // set dynamically
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHideAdditional"), 				hotelResultsPageLabels.get("showAdditional") , 				"Results Page labels verification - Hide additional filters");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPriceFilter"), 				hotelResultsPageLabels.get("sortPrice") , 					"Results Page labels verification - Sort according to price");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpStarFilter"), 					hotelResultsPageLabels.get("sortStar") , 					"Results Page labels verification - Sort according to star rating");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelNameFilter"), 			hotelResultsPageLabels.get("sortName") , 					"Results Page labels verification - Sort according to the hotel name");
			PrintTemplate.verifyTrue("", 													hotelResultsPageLabels.get("hotelName") , 					"Results Page labels verification - Selected hotel"); //set dynamically
			PrintTemplate.verifyTrue("", 													hotelResultsPageLabels.get("hotelRate") , 					"Results Page labels verification - Hotel rate"); //set dynamically
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelRoom"), 					hotelResultsPageLabels.get("hotelRoom") , 					"Results Page labels verification - Hotel room");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelRoomType"), 				hotelResultsPageLabels.get("hotelRoomType") , 				"Results Page labels verification - Hotel room type");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelRatePlan"), 				hotelResultsPageLabels.get("hotelRatePlan") , 				"Results Page labels verification - Hotel rate plan");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelRoomPrice"), 				hotelResultsPageLabels.get("hotelRoomPrice") , 				"Results Page labels verification - Hotel room price");
			PrintTemplate.verifyTrue(labelPropertyMap.get(""), 								hotelResultsPageLabels.get("hotelAddress") , 				"Results Page labels verification - Hotel address"); // set dynamically
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpMoreDetailsOverview"), 		hotelResultsPageLabels.get("moreInfoOverview") , 			"Results Page labels verification - More details - Overview");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpMoreDetailsAmenities"), 		hotelResultsPageLabels.get("moreInfoAmenities") , 			"Results Page labels verification - More details - Amenities");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpMoreDetailsAdditionalInfo"), 	hotelResultsPageLabels.get("moreinfoAdditionalInfo") , 		"Results Page labels verification - More details - Additional info");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpEmailHeader"), 				hotelResultsPageLabels.get("emailHeader") , 				"Results Page labels verification - Email header");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpEmailSubject"), 				hotelResultsPageLabels.get("emailSubject") , 				"Results Page labels verification - Email subject");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpEmailBody"), 					hotelResultsPageLabels.get("emailContent") , 				"Results Page labels verification - Email body");
			PrintTemplate.verifyTrue("", 													hotelResultsPageLabels.get("emailIsSent") , 				"Results Page labels verification - Email sent status"); //check after execute
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpMyBasket"), 					hotelResultsPageLabels.get("myBasket") , 					"Results Page labels verification - My basket");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpNoItemIsSelected"), 			hotelResultsPageLabels.get("noItemIsSelected") , 			"Results Page labels verification - No item is selected");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPriceRangeLeft"), 				hotelResultsPageLabels.get("priceRange") , 					"Results Page labels verification - Price range");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpStarRatingLeft"), 				hotelResultsPageLabels.get("starRating") , 					"Results Page labels verification - Star rating");
			PrintTemplate.verifyTrue("", 													hotelResultsPageLabels.get("starRange") , 					"Results Page labels verification - Star range"); // set dynamically
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpSuburbLeft"), 					hotelResultsPageLabels.get("suburb") , 						"Results Page labels verification - Suburb");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpHotelAvailabilityLeft"), 		hotelResultsPageLabels.get("hotelAvailability") , 			"Results Page labels verification - Hotel availability");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpNearByAttractionsLeft"), 		hotelResultsPageLabels.get("nearByAttractions") , 			"Results Page labels verification - Near by attractions");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpSpecialDealsLeft"), 			hotelResultsPageLabels.get("specialDeals") , 				"Results Page labels verification - Special deals");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpPropertyTypeLeft"), 			hotelResultsPageLabels.get("propertyType") , 				"Results Page labels verification - Property type");
			PrintTemplate.verifyTrue(labelPropertyMap.get("rpAmenitiesLeft"), 				hotelResultsPageLabels.get("amenities") , 					"Results Page labels verification - Amenities");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void confirmationPagelabels(HashMap<String, String> confirmationPageLabels, HashMap<String, String> labelPropertyMap, ReportTemplate PrintTemplate, HashMap<String, String> portalSpecificData, String canDeadline)
	{
		try {
//			PrintTemplate.setTableHeading("Verify confirmation page labels");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpConfirmationStatus"), 						confirmationPageLabels.get("confirmationStatus"), 									"Verify confirmation page labels - Confirmation status");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpConfirmationStatusBookingRefernce"), 		confirmationPageLabels.get("confirmationStatusBookingRefernce"), 					"Verify confirmation page labels - Booking reference");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpConfirmationStatusReservationNo"), 		confirmationPageLabels.get("confirmationStatusReservationNo"), 						"Verify confirmation page labels - Reservation number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpConfirmationStatusPrint"), 				confirmationPageLabels.get("confirmationStatusPrint"), 								"Verify confirmation page labels - Print this page");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpConfirmationStatusConfirmationEmail") + " " + portalSpecificData.get("cd.hotelEmail"), 	confirmationPageLabels.get("confirmationStatusConfirmationEmail"), 					"Verify confirmation page labels - A confirmation email has been sent to");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpConfirmationStatusDoNoRecieve"), 			confirmationPageLabels.get("confirmationStatusDoNoRecieve"), 						"Verify confirmation page labels - If you do not receive this email");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfo"), 						confirmationPageLabels.get("bookingItenaryInfo"), 									"Verify confirmation page labels - Booking itinerary information");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoStatus"), 				confirmationPageLabels.get("bookingItenaryInfoStatus"), 							"Verify confirmation page labels - Status");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoCheckin"), 				confirmationPageLabels.get("bookingItenaryInfoCheckin").replace(":", ""), 			"Verify confirmation page labels - CheckIn Date");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfocheckout"), 				confirmationPageLabels.get("bookingItenaryInfocheckout").replace(":", ""), 			"Verify confirmation page labels - CheckOut Date");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoNumberOfRooms"), 		confirmationPageLabels.get("bookingItenaryInfoNumberOfRooms").replace(":", ""), 	"Verify confirmation page labels - Number of Room(s):");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoNumberOfNights"), 		confirmationPageLabels.get("bookingItenaryInfoNumberOfNights").replace(":", ""), 	"Verify confirmation page labels - Number of Night(s)");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoEstimatedTime"), 		confirmationPageLabels.get("bookingItenaryInfoEstimatedTime").replace(":", ""), 	"Verify confirmation page labels - Estimated Arrival Time");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoRoom"), 					confirmationPageLabels.get("bookingItenaryInfoRoom"), 								"Verify confirmation page labels - Room");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoRoomType"), 				confirmationPageLabels.get("bookingItenaryInfoRoomType"), 							"Verify confirmation page labels - Room type");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoBedType"), 				confirmationPageLabels.get("bookingItenaryInfoBedType"), 							"Verify confirmation page labels - Bed type");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInforatePlan"), 				confirmationPageLabels.get("bookingItenaryInforatePlan"), 							"Verify confirmation page labels - Rate plan");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoRoomRate"), 				confirmationPageLabels.get("bookingItenaryInfoRoomRate"), 							"Verify confirmation page labels - Room rate");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoHotelSubTotal"), 		confirmationPageLabels.get("bookingItenaryInfoHotelSubTotal"), 						"Verify confirmation page labels - Hotel sub total");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoHotelTotalTax"), 		confirmationPageLabels.get("bookingItenaryInfoHotelTotalTax"), 						"Verify confirmation page labels - Hotel total tax");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoHotelTotal"), 			confirmationPageLabels.get("bookingItenaryInfoHotelTotal"), 						"Verify confirmation page labels - Hotel total");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoSubTotal"), 				confirmationPageLabels.get("bookingItenaryInfoSubTotal"), 							"Verify confirmation page labels - Sub total");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoTotalTaxes"), 			confirmationPageLabels.get("bookingItenaryInfoTotalTaxes"), 						"Verify confirmation page labels - Total taxes");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoTotal"), 				confirmationPageLabels.get("bookingItenaryInfoTotal"), 								"Verify confirmation page labels - Total");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoAmountProcessedNow"), 	confirmationPageLabels.get("bookingItenaryInfoAmountProcessedNow"), 				"Verify confirmation page labels - Amount been processed now");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBookingItenaryInfoDueAtCheckin"), 			confirmationPageLabels.get("bookingItenaryInfoDueAtCheckin"), 						"Verify confirmation page labels - Amount due at checkin");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfo"), 								confirmationPageLabels.get("userInfo"), 											"Verify confirmation page labels - User information heading");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoFirstName"), 						confirmationPageLabels.get("userInfoFirstName"), 									"Verify confirmation page labels - Customer first name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoAddress1"), 						confirmationPageLabels.get("userInfoAddress1"), 									"Verify confirmation page labels - Customer address");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoCountry"), 						confirmationPageLabels.get("userInfoCountry"), 										"Verify confirmation page labels - Customer country");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoPostalCode"), 						confirmationPageLabels.get("userInfoPostalCode"), 									"Verify confirmation page labels - Customer postal code");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoEmergencynumber"), 				confirmationPageLabels.get("userInfoEmergencynumber"), 								"Verify confirmation page labels - Customer emergency number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoLastName"), 						confirmationPageLabels.get("userInfoLastName"), 									"Verify confirmation page labels - Customer last name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoCity"), 							confirmationPageLabels.get("userInfoCity"), 										"Verify confirmation page labels - Customer city");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoState"), 							confirmationPageLabels.get("userInfoState"), 										"Verify confirmation page labels - Customer state");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoPoneNumber"), 						confirmationPageLabels.get("userInfoPoneNumber"), 									"Verify confirmation page labels - Customer phone number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpUserInfoEmail"), 							confirmationPageLabels.get("userInfoEmail"), 										"Verify confirmation page labels - Customer email");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancy"), 							confirmationPageLabels.get("roomsOccupancy"), 										"Verify confirmation page labels - Room occupancy details heading");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancyTitle"), 					confirmationPageLabels.get("roomsOccupancyTitle"), 									"Verify confirmation page labels - Title");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancyFirstName"), 				confirmationPageLabels.get("roomsOccupancyFirstName"), 								"Verify confirmation page labels - First name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancyLastName"), 					confirmationPageLabels.get("roomsOccupancyLastName"), 								"Verify confirmation page labels - Last name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancySpecialReq"), 				confirmationPageLabels.get("roomsOccupancySpecialReq"), 							"Verify confirmation page labels - Special requests");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancySmoking"), 					confirmationPageLabels.get("roomsOccupancySmoking"), 								"Verify confirmation page labels - Smoking");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancyhandicap"), 					confirmationPageLabels.get("roomsOccupancyhandicap"), 								"Verify confirmation page labels - Handicap");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpRoomsOccupancyWheelChair"), 				confirmationPageLabels.get("roomsOccupancyWheelChair"), 							"Verify confirmation page labels - Wheelchair");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBillingInfo"), 							confirmationPageLabels.get("billingInfo"), 											"Verify confirmation page labels - Billing info heading");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBillingInfoMerchant"), 					confirmationPageLabels.get("billingInfoMerchant"), 									"Verify confirmation page labels - Merchant track id");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBillingInfoAuthentication"), 				confirmationPageLabels.get("billingInfoAuthentication"), 							"Verify confirmation page labels - Authentication reference");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBillingInfoPaymentId"), 					confirmationPageLabels.get("billingInfoPaymentId"),	 								"Verify confirmation page labels - Payment id");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpBillingInfoAmount"), 						confirmationPageLabels.get("billingInfoAmount"), 									"Verify confirmation page labels - Amount");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendements"), 							confirmationPageLabels.get("amendements"), 											"Verify confirmation page labels - Amendments heading");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsLastDate")+ " " + Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", canDeadline), 	confirmationPageLabels.get("amendementsLastDate"), 									"Verify confirmation page labels - Cancellation deadline");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsIndicatedPolicies").trim(), 	confirmationPageLabels.get("amendementsIndicatedPolicies").trim(), 					"Verify confirmation page labels - Indicated policies");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsFullyRefundable") + " " + Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", canDeadline), 				confirmationPageLabels.get("amendementsFullyRefundable"), 							"Verify confirmation page labels - Fully refundable before");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsGenaralTerms"), 				confirmationPageLabels.get("amendementsGenaralTerms"), 								"Verify confirmation page labels - Genereal terms and conditions heading");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsChargeAndCancellation"), 		confirmationPageLabels.get("amendementsChargeAndCancellation"), 					"Verify confirmation page labels - Charge and cancellation fees apply and vary depending on,");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsAvailabilityAtTime"), 			confirmationPageLabels.get("amendementsAvailabilityAtTime"), 						"Verify confirmation page labels - Availability at time of making the change or cancellation.");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsIndividualConditions"), 		confirmationPageLabels.get("amendementsIndividualConditions"), 						"Verify confirmation page labels - The individual conditions that will be presented at time of booking.");
			PrintTemplate.verifyTrue(labelPropertyMap.get("cpAmendementsPortalConditions"), 			confirmationPageLabels.get("amendementsPortalConditions"), 							"Verify confirmation page labels - rezproduction  Terms & Conditions apply.");
			
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void availabilityReqAgainstEnstredDate(ReportTemplate PrintTemplate, hotelDetails hotelAvailabilityReq, HotelSearchObject search)
	{
		//availability request verification
//		PrintTemplate.setTableHeading("Verify availability request against entered data");
		try {
			if(search.getSupplier().equals("INV27"))
			{
				PrintTemplate.verifyTrue(search.getCheckin(), 			Calender.getDate("yyyy-MM-dd", "MM/dd/yyyy", hotelAvailabilityReq.getCheckin()), 		 	"Verify availability request against entered data - Verify checkin date");
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				PrintTemplate.verifyTrue(search.getCheckin(), 			Calender.getDate("yyyyMMdd", "MM/dd/yyyy", hotelAvailabilityReq.getCheckin()), 	 			"Verify availability request against entered data - Verify checkin date");
				SimpleDateFormat 		sdf 					= new SimpleDateFormat("MM/dd/yyyy");
				Calendar 				c 						= Calendar.getInstance();
				String 					checkout 				= null;
				try {
					c.setTime(sdf.parse(search.getCheckin()));
					c.add(Calendar.DATE, Integer.parseInt(search.getNights()));
					checkout 						= sdf.format(c.getTime());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				PrintTemplate.verifyTrue(checkout,			 			Calender.getDate("yyyyMMdd", "MM/dd/yyyy", hotelAvailabilityReq.getCheckout()), 		 	"Verify availability request against entered data - Verify checkout date");
				String[] destination = search.getDestination().split("\\|");
				PrintTemplate.verifyTrue(destination[1], 		hotelAvailabilityReq.getCityCode(), 													 	"Verify availability request against entered data - Verify destination");
				String adult = "";
				String child = "";
				String age = "";
				for(int i = 0 ; i < hotelAvailabilityReq.getRoomDetails().size() ; i++)
				{
					if(i == 0)
					{
						adult = hotelAvailabilityReq.getRoomDetails().get(i).getAdultCount();
						child = hotelAvailabilityReq.getRoomDetails().get(i).getChildCount();
					}
					else
					{
						adult = adult + "," + hotelAvailabilityReq.getRoomDetails().get(i).getAdultCount();
						child = child + "," + hotelAvailabilityReq.getRoomDetails().get(i).getChildCount();
					}
					for(int j = 0 ; j < hotelAvailabilityReq.getRoomDetails().get(i).getOccupancyDetails().size() ; j++)
					{
						if(i == 0 && j == 0)
						{
							age = hotelAvailabilityReq.getRoomDetails().get(i).getOccupancyDetails().get(j).getAge();
						}
						else
						{
							age = age + "," + hotelAvailabilityReq.getRoomDetails().get(i).getOccupancyDetails().get(j).getAge();
						}
					}
				}
				String adultE 	= "";
				String childE 	= "";
				String ageE 	= "";
				String[] adultOccupancy = search.getAdults().split("/");
				String[] childOccupancy = search.getChildren().split("/");
				String[] ageOccupancy = search.getAge().split("/");
				int childAgeCnt = 0;
				for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
				{
					for(int j = 0 ; j < Integer.parseInt(ageOccupancy[i]) ; j++)
					{
						if(i == 0 && j ==0)
						{
							ageE 	= ageOccupancy[childAgeCnt];
						}
						else
						{
							ageE 	= ageE + "," + ageOccupancy[childAgeCnt];
						}
						childAgeCnt++;
					}
					if(i == 0)
					{
						adultE 	= adultOccupancy[i];
						childE 	= childOccupancy[i];
						
					}
					else
					{
						adultE 	= adultE + "," + adultOccupancy[i];
						childE 	= childE + "," + childOccupancy[i];
					}
				}
				PrintTemplate.verifyTrue(adultE, 			adult, 								"Verify availability request against entered data - Verify adult count");
				PrintTemplate.verifyTrue(childE,		child, 							 	"Verify availability request against entered data - Verify child count");
				PrintTemplate.verifyTrue(ageE,				age,						 	 	"Verify availability request against entered data - Verify age");	
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
//		PrintTemplate.markTableEnd();
	}
	
	public void webCustomerDetails(ReportTemplate PrintTemplate, Web_Com_CustomerDetailsPage cusDetails, HashMap<String, String> labelPropertyMap, HotelSearchObject search)
	{
		// check inputfield availability
		try {
			HashMap<String, Boolean> hotelCusDetailsInputFields = cusDetails.getCustomerDetailsInputFields();
//			PrintTemplate.setTableHeading("Customer details inputfield verification");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customertitle") , 			"Customer details inputfield verification - Customer titel");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customerFirst") , 			"Customer details inputfield verification - Customer first name");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_last") , 			"Customer details inputfield verification - Customer last name");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_address") , 			"Customer details inputfield verification - Customer address");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_city") , 			"Customer details inputfield verification - Customer city");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_country") , 			"Customer details inputfield verification - Customer country");
			PrintTemplate.verifyTrue(false, hotelCusDetailsInputFields.get("IN_customer_State") , 			"Customer details inputfield verification - Customer state");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_PostalCode") , 		"Customer details inputfield verification - Customer postal code");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_Phonenumber_Areacode") , 		"Customer details inputfield verification - Customer area code");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_Phone_number") , 				"Customer details inputfield verification - Phone number");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_MobileNumber_Areacode") , 	"Customer details inputfield verification - Mobile number area code");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_MobileNumber") , 	"Customer details inputfield verification - Mobile number");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_Email") , 			"Customer details inputfield verification - Customer email address");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_VerifyEmail") , 		"Customer details inputfield verification - Verify customer email address");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_phone_number") , 	"Customer details inputfield verification - Customer phone number");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_customer_mobile_number") , 	"Customer details inputfield verification - Customer mobile number radio button");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_Savequotation") , 			"Customer details inputfield verification - Save quotation button");
			PrintTemplate.verifyTrue(true, hotelCusDetailsInputFields.get("IN_ProceedToOccupancy") , 		"Customer details inputfield verification - Proceed to occupancy button");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check label availability
		try {
			HashMap<String, String> hotelCustomerDetailsLabels = cusDetails.getCustomerDetailsPageLabels();
//			PrintTemplate.setTableHeading("Customer details labels verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsTitle"), 			hotelCustomerDetailsLabels.get("L_customertitle") , 		"Customer details labels verification - Customer title");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsFirstName"), 		hotelCustomerDetailsLabels.get("L_customerfirstname") , 	"Customer details labels verification - Customer first name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsLastname"), 		hotelCustomerDetailsLabels.get("L_customerlastname") , 		"Customer details labels verification - Customer last name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsAddress"), 			hotelCustomerDetailsLabels.get("L_customeraddress") , 		"Customer details labels verification - Customer address");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsCity"), 			hotelCustomerDetailsLabels.get("L_customercity") , 			"Customer details labels verification - Customer city");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsCountry"), 			hotelCustomerDetailsLabels.get("L_customercountry") , 		"Customer details labels verification - Customer country");
			if(search.getCountry().equals("USA") || search.getCountry().equals("United Kingdom") || search.getCountry().equals("Australis"))
			{
				PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsState"), 			hotelCustomerDetailsLabels.get("L_customerState") , 		"Customer details labels verification - Customer state");
			}
			else
			{
				PrintTemplate.verifyTrue("", 														hotelCustomerDetailsLabels.get("L_customerState") , 		"Customer details labels verification - Customer state");
			}
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsPostalCode"), 		hotelCustomerDetailsLabels.get("L_customerPostalCode") , 	"Customer details labels verification - Customer postal code");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsPhonenumber"), 		hotelCustomerDetailsLabels.get("L_customerPhone_number") , 	"Customer details labels verification - Customer phone number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsMobileNumber"), 	hotelCustomerDetailsLabels.get("L_customerMobileNumber") , 	"Customer details labels verification - Customer mobile number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsEmail"),	 		hotelCustomerDetailsLabels.get("L_customerfEmail") , 		"Customer details labels verification - Customer email address");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsVerifyEmail"), 		hotelCustomerDetailsLabels.get("L_customerVerifyEmail") , 	"Customer details labels verification - Customer email address verification");
			System.out.println(labelPropertyMap.get("ppCustomerDetailsSelectEmergeny").trim());
			System.out.println(hotelCustomerDetailsLabels.get("L_SelectOneAsTheECM").trim());
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppCustomerDetailsSelectEmergeny").trim(), 	hotelCustomerDetailsLabels.get("L_SelectOneAsTheECM").trim() , 	"Customer details labels verification - Select the emergency phone number");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
		// check mandotory fields
		try {
			HashMap<String, Boolean> hotelCustomerDetailsmandotory = cusDetails.getCustomerDetailsMandatoryFields();
//			PrintTemplate.setTableHeading("Customer details mandotory fields verification");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customertitle") , 		"Customer details mandotory fields verification - Customer title");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customerfirstname") , 	"Customer details mandotory fields verification - Customer first name");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customerlastname") , 		"Customer details mandotory fields verification - Customer last name");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customeraddress") , 		"Customer details mandotory fields verification - Customer address");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customercity") , 			"Customer details mandotory fields verification - Customer city");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customercountry") , 		"Customer details mandotory fields verification - Customer country");
			if(search.getCountry().equals("USA") || search.getCountry().equals("United Kingdom") || search.getCountry().equals("Australis"))
			{
				PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customerState") , 		"Customer details mandotory fields verification - Customer state");
			}
			else
			{
				PrintTemplate.verifyTrue(false, hotelCustomerDetailsmandotory.get("Man_customerState") , 		"Customer details mandotory fields verification - Customer state");
			}
			
			PrintTemplate.verifyTrue(false, hotelCustomerDetailsmandotory.get("Man_customerPostalCode") , 	"Customer details mandotory fields verification - Customer postal code");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customerPhone_number") , 	"Customer details mandotory fields verification - Customer phone number");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customerfEmail") , 		"Customer details mandotory fields verification - Customer email address");
			PrintTemplate.verifyTrue(true, hotelCustomerDetailsmandotory.get("Man_customerVerifyEmail") , 	"Customer details mandotory fields verification - Customer email address verification");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void webOccupancyDetails(ReportTemplate PrintTemplate, Web_Hotel_paymentOccupancyPage occupancyDetails, HashMap<String, String> labelPropertyMap, HashMap<String, String> portalSpecificData, hotelDetails roomAvailablityRes) throws Exception
	{
		// check buttons
		try {
//			PrintTemplate.setTableHeading("Occupancy details buttons verification");
			HashMap<String, Boolean> occupancyButtons = occupancyDetails.getOccupancyButtons();
			PrintTemplate.verifyTrue(true, occupancyButtons.get("roomOptions"), 		"Occupancy details buttons verification - Room options");
			PrintTemplate.verifyTrue(true, occupancyButtons.get("prevStep"), 			"Occupancy details buttons verification - Navigate to previous step - Customer details");
			PrintTemplate.verifyTrue(true, occupancyButtons.get("nextStep"), 			"Occupancy details buttons verification - navigate to next step - Billing info");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check labels
		try {
//			PrintTemplate.setTableHeading("Occupancy details labels verification");
			HashMap<String, String> occupancyLabels = occupancyDetails.getOccupancyLabels();
			PrintTemplate.verifyTrue("Hotel : " + roomAvailablityRes.getHotel(), 				occupancyLabels.get("hotelName"), 				"Occupancy details labels verification - Hotel name"); //set dynamically
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsRoom"), 			occupancyLabels.get("roomNumber"), 				"Occupancy details labels verification - Room number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsRoomOptions"), 	occupancyLabels.get("roomOptions"), 			"Occupancy details labels verification - Room options");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsGuest"),	 		occupancyLabels.get("guestNumber"), 			"Occupancy details labels verification - Adutlt number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsChild"), 			occupancyLabels.get("childNumber"), 			"Occupancy details labels verification - Child number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsTitle"), 			occupancyLabels.get("title"), 					"Occupancy details labels verification - Title");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsFirstname"), 		occupancyLabels.get("firstName"), 				"Occupancy details labels verification - First name");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsLastname"), 		occupancyLabels.get("lastName"), 				"Occupancy details labels verification - Last name");
		
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsSmoking"), 		occupancyLabels.get("smoking"), 				"Occupancy details labels verification - Smoking");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsNoSmoking"), 		occupancyLabels.get("noSmoking"), 				"Occupancy details labels verification - Not smoking");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsHandicap"), 		occupancyLabels.get("handicap"), 				"Occupancy details labels verification - handicap");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsWheelchair"), 		occupancyLabels.get("wheelchair"), 				"Occupancy details labels verification - Wheelchair");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppOccupancyDetailsEstArrivalTime"), 	occupancyLabels.get("estimatedArrivalTime"), 	"Occupancy details labels verification - Estimated arrival time");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check  combox
		try {
			HashMap<String, String> occupancyCombobox = occupancyDetails.getOccupancyComboBoxDataList();
//			PrintTemplate.setTableHeading("Occupancy details combobox data verification");
			PrintTemplate.verifyTrue(portalSpecificData.get("titles"), occupancyCombobox.get("title").replace("\n", " "), 					"Occupancy details combobox data verification - Title");
			PrintTemplate.verifyTrue(portalSpecificData.get("hours"), occupancyCombobox.get("estimatedArrivalTimeHours"), 					"Occupancy details combobox data verification - Estimated arrival time - Hours");
			PrintTemplate.verifyTrue(portalSpecificData.get("mins"), occupancyCombobox.get("estimatedArrivalTimeMins").replace("\n", " "), 	"Occupancy details combobox data verification - Estimated arrival time - mins");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check radio buttons
		try {
			HashMap<String, Boolean> occupancyRadio = occupancyDetails.getOccupancyRadioButtons();
//			PrintTemplate.setTableHeading("Occupancy details radio buttons verification");
			PrintTemplate.verifyTrue(false, occupancyRadio.get("smoking"), 			"Occupancy details radio buttons verification- Smoking");
			PrintTemplate.verifyTrue(false, occupancyRadio.get("noSmoking"), 		"Occupancy details radio buttons verification - Not Smoking");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check check box
		try {
			HashMap<String, Boolean> occupancyCheckBox = occupancyDetails.getOccupancyCheckBox();
//			PrintTemplate.setTableHeading("Occupancy details check box verification");
			PrintTemplate.verifyTrue(false, occupancyCheckBox.get("handicap"), 		"Occupancy details check box verification - Handicap");
			PrintTemplate.verifyTrue(false, occupancyCheckBox.get("wheelchair"), 	"Occupancy details check box verification - Wheelchair");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check input fields
		try {
			HashMap<String, Boolean> occupancyInput = occupancyDetails.getOccupancyinputfields();
//			PrintTemplate.setTableHeading("Occupancy details input fields verification");
			PrintTemplate.verifyTrue(true, occupancyInput.get("adultFirstName"), 	"Occupancy details input fields verification - Adult first name");
			PrintTemplate.verifyTrue(true, occupancyInput.get("adultLastName"), 	"Occupancy details input fields verification - Adult last name");
			PrintTemplate.verifyTrue(true, occupancyInput.get("childFirstName"), 	"Occupancy details input fields verification - Child first name");
			PrintTemplate.verifyTrue(true, occupancyInput.get("childLastName"), 	"Occupancy details input fields verification - Child last name");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void webBillingInfo(ReportTemplate PrintTemplate, Web_Com_BillingInformationPage billinginfo, HashMap<String, String> labelPropertyMap, CommonBillingInfoRates rates, hotelDetails res, HashMap<String, String> portalSpecificData, String defaultCC, HashMap<String, String> currencyMap)
	{
		//check input fields availability
		try {
			HashMap<String, Boolean> billingInfoInputFields = billinginfo.getBillingInfoInputFields();
//			PrintTemplate.setTableHeading("Billing info input fields verification");
			PrintTemplate.verifyTrue(true, billingInfoInputFields.get("IN_DiscountCouponNumber"), 		"Billing info input fields verification - Discount coupon number");
			PrintTemplate.verifyTrue(true, billingInfoInputFields.get("IN_DiscountCouponValidate"), 	"Billing info input fields verification - Discount coupon validate button");
			PrintTemplate.verifyTrue(true, billingInfoInputFields.get("IN_CreditCardList"), 			"Billing info input fields verification - Credit card list");
			PrintTemplate.verifyTrue(true, billingInfoInputFields.get("IN_credit_card_note"), 			"Billing info input fields verification - Credit card note");
			PrintTemplate.verifyTrue(true, billingInfoInputFields.get("IN_BillingInfo_PrevStep"), 		"Billing info input fields verification - Navigate to previous step button");
			PrintTemplate.verifyTrue(true, billingInfoInputFields.get("IN_BillingInfo_ProceedToNotes"), "Billing info input fields verification - Navigate to next step button");
//			PrintTemplate.markTableEnd();	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// check label availability
		try {
			HashMap<String, String> billingInfoLabels = billinginfo.getBillingInfoLabels();
//			PrintTemplate.setTableHeading("Billing info labels verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoEnterDiscount"), 			billingInfoLabels.get("L_EnterDiscountCoupon"), 	"Billing info labels verification - Enter discount coupon");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoCardType"), 				billingInfoLabels.get("L_cardTypeLabel"), 			"Billing info labels verification - Credit card type");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoSubTotal"), 				billingInfoLabels.get("L_BI_SubTotal"), 			"Billing info labels verification - Sub total");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoTotalTaxAndFees"), 			billingInfoLabels.get("L_BI_TotalTaxandOthers"), 	"Billing info labels verification - Total tax and other charges");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoTotalValue"), 				billingInfoLabels.get("L_BI_Total"), 				"Billing info labels verification - Total");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoAmountBeingProcessedNow"), 	billingInfoLabels.get("L_BI_AmountProcessedNow"), 	"Billing info labels verification - Amount processed now");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppBillingInfoAmountDueAtCheckin"), 		billingInfoLabels.get("L_BI_AmountDue"), 			"Billing info labels verification - Amount due");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// check rates
		try {
			String value 	= converter.convert(defaultCC, res.getPrice(), rates.getCurrency(), currencyMap, res.getCurrencyCode());
			String pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, rates.getCurrency(), currencyMap);
			String bf 		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, rates.getCurrency(), currencyMap);
			String ccfee 	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, rates.getCurrency(), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			
//			PrintTemplate.setTableHeading("Billing info rates verification");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), rates.getSubtotal(), 																	"Billing info rates verification - Sub total");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccfee)), rates.getTotalTaxAndFees(), 															"Billing info rates verification - Total tax and fees");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccfee) + Integer.parseInt(value) + Integer.parseInt(pm)), rates.getTotal(), 					"Billing info rates verification - Total Value");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccfee) + Integer.parseInt(value) + Integer.parseInt(pm)), rates.getAmountBeingProcessedNow(), 	"Billing info rates verification - Amount being Processed Now");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccfee) + Integer.parseInt(value) + Integer.parseInt(pm)), rates.getTotalPG(), 					"Billing info rates verification - Amount at payment gateway");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void webNotes(ReportTemplate PrintTemplate, Web_Com_NotesPage notes, HashMap<String, String> labelPropertyMap)
	{
		// check availability of fields
		try {
			HashMap<String, Boolean> notesFieldsAvailability = notes.getHotelFieldAvailability();
//			PrintTemplate.setTableHeading("Notes info fields verification");
			PrintTemplate.verifyTrue(true, notesFieldsAvailability.get("customerNotes"), 				"Notes info fields verification - Customer notes");
			PrintTemplate.verifyTrue(true, notesFieldsAvailability.get("hotelList"), 					"Notes info fields verification - Hotel list");
			PrintTemplate.verifyTrue(true, notesFieldsAvailability.get("hotelNotes"), 					"Notes info fields verification - Hotel notes");
			PrintTemplate.verifyTrue(true, notesFieldsAvailability.get("Navigate_to_previous_step"), 	"Notes info fields verification - Navigate to the previous step");
			PrintTemplate.verifyTrue(true, notesFieldsAvailability.get("Proceed_to_next_step"), 		"Notes info fields verification - Proceed to the next step");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// check label availability
		try {
			HashMap<String, String> notesLabels = notes.getHotelNotesLabels();
//			PrintTemplate.setTableHeading("Notes info labels verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppNotesCustomerNotes"), 	notesLabels.get("customer_notes"), 	"Notes info labels verification - Customer notes");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppHotelNotes"), 			notesLabels.get("hotel_notes"), 	"Notes info labels verification - Hotel notes");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void webTermsAndConditions(ReportTemplate PrintTemplate, Web_Com_TermsPage terms, HashMap<String, String> labelPropertyMap, String canDeadline, HashMap<String, String> portalSpecificData)
	{
		//check inputfields
		try {
			HashMap<String, Boolean> termsInput = terms.getTermsAndConditionInputFields();
//			PrintTemplate.setTableHeading("Terms and conditions input fields verification");
			PrintTemplate.verifyTrue(true, termsInput.get("IN_TC_CancelledCheckBox"), 					"Terms and conditions input fields verification - Read cancellation policy");
			PrintTemplate.verifyTrue(true, termsInput.get("IN_IConfirmedReadTheTermsAndConditions"), 	"Terms and conditions input fields verification - Read terms and conditions");
			PrintTemplate.verifyTrue(true, termsInput.get("IN_WouldLikeToReceiv"), 						"Terms and conditions input fields verification - Recieve special offers");
			PrintTemplate.verifyTrue(true, termsInput.get("IN_TC_create_login"), 						"Terms and conditions input fields verification - Create own login");
			PrintTemplate.verifyTrue(true, termsInput.get("IN_TC_PrevStep"), 							"Terms and conditions input fields verification - Navigate to previous step");
			PrintTemplate.verifyTrue(true, termsInput.get("IN_TC_Make_payment"), 						"Terms and conditions input fields verification - Make payment");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//check labels
		try {
			HashMap<String, String> termsLabels = terms.getTermAndConditionPageLabels();
//			PrintTemplate.setTableHeading("Terms and conditions labels verification");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsFullyreRundableifCancelled") + " " + Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", canDeadline), termsLabels.get("L_TC_FullyreRundableifCancelled"), 				"Terms and conditions labels verification - Fully refundable before the given date");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsIConfirmReadPolicy"), 																			termsLabels.get("L_TC_IConfirmReadPolicy"), 						"Terms and conditions labels verification - Confirm read policy");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsGeneralTermsAndCondition"), 																		termsLabels.get("L_TC_GeneralTermsAndCondition"), 					"Terms and conditions labels verification - General terms and conditions");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsChargeAndCancellationFeesApply"), 																termsLabels.get("L_TC_ChargeAndCancellationFeesApply"), 			"Terms and conditions labels verification - Charge and cancellation fee apply");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsAvailabilityAtTimeOfMaking"), 																	termsLabels.get("L_TC_AvailabilityAtTimeOfMaking"), 				"Terms and conditions labels verification - Availability at time of biiking");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsTheIndividualConditions"), 																		termsLabels.get("L_TC_TheIndividualConditions"), 					"Terms and conditions labels verification - Individual conditions");
			PrintTemplate.verifyTrue(portalSpecificData.get("PortalName") + "  " + labelPropertyMap.get("ppTermsOmanairholidaysTermsCondition"), 					termsLabels.get("L_TC_OmanairholidaysTermsCondition"), 				"Terms and conditions labels verification - Terms and conditions");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsIfYouRequiretoAmendOrCancel"), 																	termsLabels.get("L_TC_IfYouRequiretoAmendOrCancel"), 				"Terms and conditions labels verification - Amendement or cancel");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsportalemail"), 																					termsLabels.get("L_TC_portalemail"), 								"Terms and conditions labels verification - Portal mail");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsportalphone"), 																					termsLabels.get("L_TC_portalphone"), 								"Terms and conditions labels verification - Portal phone number");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsIConfirmedReadTheTermsAndConditionsText").trim(), 												termsLabels.get("L_TC_IConfirmedReadTheTermsAndConditionsText").trim(), 	"Terms and conditions labels verification - Confirmed read the terms");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsWouldLikeToReceivText"), 																			termsLabels.get("L_WouldLikeToReceivText"), 						"Terms and conditions labels verification - Would like to recieve");
			PrintTemplate.verifyTrue(labelPropertyMap.get("ppTermsPleaseCheckThatAllYourBooking"), 																	termsLabels.get("L_PleaseCheckThatAllYourBooking"), 				"Terms and conditions labels verification - Please check that all your bookings");
//			PrintTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void availabilityResAgainstResults(ReportTemplate PrintTemplate, ArrayList<HotelDetails> hotelList, ArrayList<hotelDetails> hotelAvailabilityRes, HotelSearchObject search)
	{
		ArrayList<HotelDetails> hotels = new ArrayList<HotelDetails>();
		for(int i = 0 ; i < hotelList.size() ; i++)
		{
			if(search.getSupplier().equals(hotelList.get(i).getSupplier()))
			{
				hotels.add(hotelList.get(i));
			}
		}
//		PrintTemplate.setTableHeading("Verify availability response against results");
		for(int i = 0 ; i < hotels.size() ; i++)
		{
			for(int j = 0 ; j < hotelAvailabilityRes.size() ; j++)
			{
				if(hotels.get(i).getTitle().equals(hotelAvailabilityRes.get(j).getHotel()))
				{
					PrintTemplate.verifyTrue(hotelAvailabilityRes.get(j).getHotel(), hotels.get(i).getTitle(), "Verify availability response against results - Verify hotel name");
					
				}
			}
		}
//		PrintTemplate.markTableEnd();
	}
	
	public void availabilityResAgainstSelectedHotel(ReportTemplate PrintTemplate, ArrayList<hotelDetails> hotelAvailabilityRes, HotelDetails selectedHotel, HashMap<String, String> currencyMap, String defaultCC, HashMap<String, String> portalSpecificData)
	{
		try {
			hotelDetails xmlHotel = new hotelDetails();
			for(int i = 0 ; i < hotelAvailabilityRes.size() ; i++)
			{
				if(selectedHotel.getTitle().equals(hotelAvailabilityRes.get(i).getHotel()))
				{
					xmlHotel = hotelAvailabilityRes.get(i);
					break;
				}
			}
//			PrintTemplate.setTableHeading("Verify availability response against the selected hotel");
			PrintTemplate.verifyTrue(xmlHotel.getHotel(), selectedHotel.getTitle(), 			"Verify availability response against the selected hotel - Hotel name");
			PrintTemplate.verifyTrue(xmlHotel.getStarRating(), selectedHotel.getStarRating(), 	"Verify availability response against the selected hotel - Star category");
			ArrayList<String> xmlHotelRoomRates 			= new ArrayList<String>();
			ArrayList<String> xmlHotelRoomType 				= new ArrayList<String>();
			ArrayList<String> xmlHotelRatePlan 				= new ArrayList<String>();
			ArrayList<String> selectedHotelRoomRates 		= new ArrayList<String>();
			ArrayList<String> selectedHotelRoomType 		= new ArrayList<String>();
			ArrayList<String> selectedHotelRatePlan 		= new ArrayList<String>();
			
			for(int i = 0 ; i < selectedHotel.getRoomList().size() ; i++)
			{
				for(int j = 0 ; j < xmlHotel.getRoomDetails().size() ; j++)
				{
					if(selectedHotel.getRoomList().get(i).getRoomType().equalsIgnoreCase(xmlHotel.getRoomDetails().get(j).getRoomType()) && selectedHotel.getRoomList().get(i).getRatePlan().equalsIgnoreCase(xmlHotel.getRoomDetails().get(j).getBoard()))
					{
						String conValue  		= 	converter.convertWithoutRoundingUp(defaultCC, xmlHotel.getRoomDetails().get(j).getPrice(), selectedHotel.getCurrency(), currencyMap, xmlHotel.getCurrencyCode());
						String pm				=	tax.getTaxWithoutRoundUp(conValue, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
						xmlHotelRoomRates.add		(String.valueOf(Float.parseFloat(conValue) + Float.parseFloat(pm)));
						xmlHotelRoomType.add		(xmlHotel.getRoomDetails().get(j).getRoomType());
						xmlHotelRatePlan.add		(xmlHotel.getRoomDetails().get(j).getBoard());
						selectedHotelRoomRates.add	(selectedHotel.getRoomList().get(i).getRate());
						selectedHotelRoomType.add	(selectedHotel.getRoomList().get(i).getRoomType());
						selectedHotelRatePlan.add	(selectedHotel.getRoomList().get(i).getRatePlan());					
					}
				}
			}
			try {
				PrintTemplate.verifyArrayList(xmlHotelRoomType, 	selectedHotelRoomType, 									"Verify availability response against the selected hotel - Verify room type");
				PrintTemplate.verifyArrayList(xmlHotelRatePlan, 	selectedHotelRatePlan, 									"Verify availability response against the selected hotel - Verify rate plan");
				PrintTemplate.verifyArrayList(xmlHotelRoomRates, 	selectedHotelRoomRates, 								"Verify availability response against the selected hotel - Verify room room rate");
				PrintTemplate.verifyTrue(true, 						selectedHotel.getMoreInfo().isAmanityAvailability(), 	"Verify availability response against the selected hotel - Check amenity availability");
				PrintTemplate.verifyTrue(true, 						selectedHotel.getMoreInfo().isImageAvailability(), 		"Verify availability response against the selected hotel - Check images availability");
				PrintTemplate.verifyTrue(true, 						selectedHotel.getMoreInfo().isMapAvailability(), 		"Verify availability response against the selected hotel - Check map availability");
				PrintTemplate.verifyTrue(true, 						selectedHotel.getMoreInfo().isMoreInfoAvailability(), 	"Verify availability response against the selected hotel - Check overview availability");
//				PrintTemplate.markTableEnd();
			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
//		PrintTemplate.markTableEnd();
	}
	
	public void selectedHotelAgainstAvailabliltyAndRoomAvailabilityResponses(HotelDetails selectedHotel, ArrayList<hotelDetails> hotelAvailabilityRes, hotelDetails res, ReportTemplate printTemplate, HotelSearchObject search, HashMap<String, String> portalSpecificData, HashMap<String, String> currencyMap, String defaultCC)
	{
		try {
			printTemplate.verifyTrue(res.getHotel(), selectedHotel.getTitle(), "Verify selected hotel - Hotel name");
			printTemplate.verifyTrue(search.getSupplier(), selectedHotel.getSupplier(), "Verify selected hotel - Hotel supplier");
			String value 	= converter.convert(defaultCC, res.getPrice(), selectedHotel.getCurrency(), currencyMap, res.getCurrencyCode());
			String pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), 		defaultCC, selectedHotel.getCurrency(), currencyMap);
			String bf		= tax.getTax(value, portalSpecificData.get("hotelbf"), 		defaultCC, selectedHotel.getCurrency(), currencyMap);
			String ccFee	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, selectedHotel.getCurrency(), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			System.out.println(value);
			System.out.println(pm);
			System.out.println(selectedHotel.getRate());
			printTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), selectedHotel.getRate(), "Verify selected hotel - Hotel rate");
			
			String 	webRoomType 	= "";
			String 	xmlRoomType 	= "";
			String 	webBedType 		= "";
			String 	xmlBedType 		= "";
			String 	webRatePlan 	= "";
			String 	xmlRatePlan 	= "";
			String 	webRate 		= "";
			String 	xmlRate	 		= "";
			
			String[] roomTypeHelper = null;
			String[] rezliveRatePlan = null;
			if(search.getSupplier().equals("rezlive"))
			{
				for(int i = 0 ; i < hotelAvailabilityRes.size() ; i++)
				{
					if(hotelAvailabilityRes.get(i).getHotel().equals(res.getHotel()))
					{
						roomTypeHelper = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getRoomType().split("\\|");
						rezliveRatePlan = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getBoard().split("\\|");
						break;
					}
				}
			}
			
			for(int i = 0 ; i < res.getRoomDetails().size() ; i++)
			{
				if(i == 0)
				{
					webRoomType = selectedHotel.getRoomList().get(i).getRoomType();
					xmlRoomType = res.getRoomDetails().get(i).getRoomName();
					
					webBedType 	= selectedHotel.getRoomList().get(i).getBedType();
					webRatePlan = selectedHotel.getRoomList().get(i).getRatePlan();
					if(search.getSupplier().equals("rezlive"))
					{
						String[] roomType = roomTypeHelper[0].split("-");
						xmlBedType = roomType[0];
						xmlRatePlan = rezliveRatePlan[0];
					}
					else
					{
						xmlBedType = res.getRoomDetails().get(i).getBedType();
						xmlRatePlan = res.getRoomDetails().get(i).getBoard();
					}
					
					
					value = "";
					pm = "";
					if(search.getSupplier().equals("rezlive"))
					{
						String priceHelper = res.getRoomDetails().get(0).getPrice().replace("//|", "/");
						String[] price = res.getRoomDetails().get(0).getPrice().split("\\|");
						for(int j = 0 ; j < price.length ; j++)
						{
							value 			= converter.convertWithoutRoundingUp(defaultCC, price[j], selectedHotel.getCurrency(), currencyMap, res.getCurrencyCode());
							pm 				= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
							webRate = selectedHotel.getRoomList().get(j).getRate();
							xmlRate = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
						}
					}
					else
					{
						value 			= converter.convertWithoutRoundingUp(defaultCC, res.getRoomDetails().get(i).getPrice(), selectedHotel.getCurrency(), currencyMap, res.getCurrencyCode());
						pm 				= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
						webRate = selectedHotel.getRoomList().get(i).getRate();
						xmlRate = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					}
				}
				else
				{
					webRoomType = webRoomType + "," + selectedHotel.getRoomList().get(i).getRoomType();
					xmlRoomType = xmlRoomType + "," + res.getRoomDetails().get(i).getRoomName();
					
					webBedType 	= webBedType + "," + selectedHotel.getRoomList().get(i).getBedType();
					webRatePlan = webRatePlan + "," + selectedHotel.getRoomList().get(i).getRatePlan();
					if(search.getSupplier().equals("rezlive"))
					{
						String[] roomType = roomTypeHelper[0].split("-");
						xmlBedType = xmlBedType + "," + roomType[0];
						xmlRatePlan = xmlRatePlan + "," + rezliveRatePlan[0];
					}
					else
					{
						xmlBedType 	= xmlBedType + "," + res.getRoomDetails().get(i).getBedType();
						xmlRatePlan = xmlRatePlan + "," + res.getRoomDetails().get(i).getBoard();
					}
					
					
					value = "";
					pm = "";
					if(search.getSupplier().equals("rezlive"))
					{
						String priceHelper = res.getRoomDetails().get(0).getPrice().replace("//|", "/");
						String[] price = res.getRoomDetails().get(0).getPrice().split("\\|");
						for(int j = 0 ; j < price.length ; j++)
						{
							value 			= converter.convertWithoutRoundingUp(defaultCC, price[j], selectedHotel.getCurrency(), currencyMap, res.getCurrencyCode());
							pm 				= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
							webRate = webRate + "," + selectedHotel.getRoomList().get(j).getRate();
							xmlRate = xmlRate + "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
						}
					}
					else
					{
						value 			= converter.convertWithoutRoundingUp(defaultCC, res.getRoomDetails().get(i).getPrice(), selectedHotel.getCurrency(), currencyMap, res.getCurrencyCode());
						pm 				= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
						webRate = webRate + "," + selectedHotel.getRoomList().get(i).getRate();
						xmlRate = xmlRate + "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					}
				}
			}
			//printTemplate.verifyArrayList(Expected, Actual, "Verify selected hotel - Room");
			printTemplate.verifyTrue(xmlRoomType, 	webRoomType, 														"Verify selected hotel - Room type");
			printTemplate.verifyTrue(xmlBedType, 	webBedType, 														"Verify selected hotel - Bed type");
			printTemplate.verifyTrue(xmlRatePlan, 	webRatePlan, 														"Verify selected hotel - Rate plan");
			printTemplate.verifyTrue(xmlRate, 		webRate, 															"Verify selected hotel - Room rate");
			printTemplate.verifyTrue(true, 			selectedHotel.getMoreInfo().isMoreInfoAvailability(), 				"Verify selected hotel - More info Availability");
			if(search.getChannel().equals("cc"))
			{
				printTemplate.verifyTrue(true, 			selectedHotel.getMoreInfo().isHotelNameAvailability(),	 			"Verify selected hotel - More info - Hotel name availability");
				printTemplate.verifyTrue(true, 			selectedHotel.getMoreInfo().isStarratingAvailability(), 			"Verify selected hotel - More info - Star rating availability");	
			}
			printTemplate.verifyTrue(true, 			selectedHotel.getMoreInfo().isAmanityAvailability(), 				"Verify selected hotel - More info - Amenity availability");
			printTemplate.verifyTrue(true, 			selectedHotel.getMoreInfo().isMapAvailability(), 					"Verify selected hotel - More info - Map availability");
			printTemplate.verifyTrue(true, 			selectedHotel.getMoreInfo().isReviewsAndRatingsAvailability(), 		"Verify selected hotel - More info - Reviews and Ratings availability");
		
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void shoppingCartVerificationCC(ReportTemplate printTemplate, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificData, hotelDetails roomAvailablityRes, HashMap<String, String> shoppingCartValues, HotelDetails selectedHotel)
	{
		String value 	= converter.convert(defaultCC, roomAvailablityRes.getPrice(), selectedHotel.getCurrency(), currencyMap, roomAvailablityRes.getCurrencyCode());
		String pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
		String bf		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, selectedHotel.getCurrency(), currencyMap);
		String ccFee	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, selectedHotel.getCurrency(), currencyMap, portalSpecificData.get("portal.pgCurrency"));
		
		printTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), shoppingCartValues.get("hotelRate"), "Verify shopping cart - Hotel rate");
		printTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), shoppingCartValues.get("subtotal"), "Verify shopping cart - Sub total");
		printTemplate.verifyTrue(bf, shoppingCartValues.get("tax"), "Verify shopping cart - Tax");
		printTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf)), shoppingCartValues.get("totalPayable"), "Verify shopping cart - Total payable");
	}
	
	public void paymentPageVerificationCC(HashMap<String, String> paymentPageValues, HashMap<String, String> portalSpecificData, HashMap<String, String> currencyMap, String defaultCC, HotelSearchObject search, ReportTemplate printTemplate, String canDeadline, hotelDetails res, ArrayList<hotelDetails> hotelAvailabilityRes)
	{//19 Jan 2017    20170119
		try {
			printTemplate.verifyTrue(res.getHotel(), 													paymentPageValues.get("hotelName"), 						"Verify payment page values - Hotel name");
			printTemplate.verifyTrue(res.getAddress(), 													paymentPageValues.get("address"), 							"Verify payment page values - Hotel address");
			if(search.getChannel().equals("cc"))
			{
				printTemplate.verifyTrue(Calender.getDate("yyyyMMdd", "dd MMM yyyy", res.getCheckin()), 						paymentPageValues.get("checkin"), 							"Verify payment page values - Checkin");
				printTemplate.verifyTrue(Calender.getDate("yyyyMMdd", "dd MMM yyyy", res.getCheckout()), 												paymentPageValues.get("checkout"), 							"Verify payment page values - Checkout");
			}
			else
			{
				printTemplate.verifyTrue(res.getCheckin(), 													paymentPageValues.get("checkin"), 							"Verify payment page values - Checkin");
				printTemplate.verifyTrue(res.getCheckout(), 												paymentPageValues.get("checkout"), 							"Verify payment page values - Checkout");
			}
			
			printTemplate.verifyTrue(res.getNights(), 														paymentPageValues.get("nights"), 							"Verify payment page values - Number of nights");
			printTemplate.verifyTrue(res.getRoomCount(), 													paymentPageValues.get("rooms"), 							"Verify payment page values - Number of rooms");
			String availabilityStatus = "";
			if(search.getSupplier().equals("hotelbeds_v2"))	
			{
				if(res.getAvailabilityStatus().equals("NEW"))
				{
					availabilityStatus = "Available to book";
				}
			}
			else
			{
				availabilityStatus = res.getAvailabilityStatus();
			}
			printTemplate.verifyTrue(availabilityStatus, 												paymentPageValues.get("bookingStatus"), 					"Verify payment page values - Booking status");
			printTemplate.verifyTrue(canDeadline, 														paymentPageValues.get("cancellationDeadline"), 				"Verify payment page values - Cancellation deadline");

			String 	webRoomType 	= "";
			String 	xmlRoomType 	= "";
			String 	webBedType 		= "";
			String 	xmlBedType 		= "";
			String 	webRatePlan 	= "";
			String 	xmlRatePlan 	= "";
			String 	webRate 		= "";
			String 	xmlRate	 		= "";
			
			String value 	= ""; 	
			String pm  		= "";
			String bf 		= "";
			String ccFee 	= "";
			
			String[] roomTypeHelper = null;
			String[] rezliveRatePlan = null;
			if(search.getSupplier().equals("rezlive"))
			{
				for(int i = 0 ; i < hotelAvailabilityRes.size() ; i++)
				{
					if(hotelAvailabilityRes.get(i).getHotel().equals(res.getHotel()))
					{
						roomTypeHelper = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getRoomType().split("\\|");
						rezliveRatePlan = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getBoard().split("\\|");
						break;
					}
				}
			}
			
			for(int i = 1 ; i <= res.getRoomDetails().size() ; i++)
			{
				if(i == 1)
				{
					webRoomType = paymentPageValues.get("roomType" + String.valueOf(i));
					xmlRoomType = res.getRoomDetails().get(i).getRoomName();
					
					webBedType 	= paymentPageValues.get("bedType" + String.valueOf(i));
					webRatePlan = paymentPageValues.get("ratePlan" + String.valueOf(i));
					if(search.getSupplier().equals("rezlive"))
					{
						String[] roomType = roomTypeHelper[0].split("-");
						xmlBedType = roomType[0];
						xmlRatePlan = rezliveRatePlan[0];
					}
					else
					{
						xmlBedType = res.getRoomDetails().get(i).getBedType();
						xmlRatePlan = res.getRoomDetails().get(i).getBoard();
					}
					
					
					value = "";
					pm = "";
					if(search.getSupplier().equals("rezlive"))
					{
						String priceHelper = res.getRoomDetails().get(0).getPrice().replace("//|", "/");
						String[] price = res.getRoomDetails().get(0).getPrice().split("\\|");
						for(int j = 0 ; j < price.length ; j++)
						{
							value 			= converter.convert(defaultCC, price[j], paymentPageValues.get("currencyCode"), currencyMap, res.getCurrencyCode());
							pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap);
							webRate 		= paymentPageValues.get("totalRate" + String.valueOf(i));
							xmlRate 		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
						}
					}
					else
					{
						value 			= converter.convert(defaultCC, res.getRoomDetails().get(i).getPrice(), paymentPageValues.get("currencyCode"), currencyMap, res.getCurrencyCode());
						pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap);
						webRate 		= paymentPageValues.get("totalRate" + String.valueOf(i));
						xmlRate 		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					}
				}
				else
				{
					webRoomType = webRoomType + "," + paymentPageValues.get("roomType" + String.valueOf(i));
					xmlRoomType = xmlRoomType + "," + res.getRoomDetails().get(i).getRoomName();
					
					webBedType 	= webBedType + "," + paymentPageValues.get("bedType" + String.valueOf(i));
					webRatePlan = webRatePlan + "," + paymentPageValues.get("ratePlan" + String.valueOf(i));
					if(search.getSupplier().equals("rezlive"))
					{
						String[] roomType = roomTypeHelper[0].split("-");
						xmlBedType = xmlBedType + "," + roomType[0];
						xmlRatePlan = xmlRatePlan + "," + rezliveRatePlan[0];
					}
					else
					{
						xmlBedType 	= xmlBedType + "," + res.getRoomDetails().get(i).getBedType();
						xmlRatePlan = xmlRatePlan + "," + res.getRoomDetails().get(i).getBoard();
					}
					
					
					value = "";
					pm = "";
					if(search.getSupplier().equals("rezlive"))
					{
						String priceHelper = res.getRoomDetails().get(0).getPrice().replace("//|", "/");
						String[] price = res.getRoomDetails().get(0).getPrice().split("\\|");
						for(int j = 0 ; j < price.length ; j++)
						{
							value 			= converter.convert(defaultCC, price[j], paymentPageValues.get("currencyCode"), currencyMap, res.getCurrencyCode());
							pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap);
							webRate 		= webRate + "," + paymentPageValues.get("totalRate" + String.valueOf(i));
							xmlRate 		= xmlRate + "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
						}
					}
					else
					{
						value 			= converter.convert(defaultCC, res.getRoomDetails().get(i).getPrice(), paymentPageValues.get("currencyCode"), currencyMap, res.getCurrencyCode());
						pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap);
						webRate 		= webRate + "," + paymentPageValues.get("totalRate" + String.valueOf(i));
						xmlRate 		= xmlRate + "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					}
				}
			}
			
			//printTemplate.verifyTrue(res.getRoomDetails().get(0).getRoomName(), paymentPageValues.get("room"), "Verify payment page values - Room");
			printTemplate.verifyTrue(xmlRoomType, 		webRoomType, 	"Verify payment page values - Room type");
			printTemplate.verifyTrue(xmlBedType, 		webBedType, 	"Verify payment page values - Bed type");
			printTemplate.verifyTrue(xmlRatePlan, 		webRatePlan, 	"Verify payment page values - Rate plan");
			printTemplate.verifyTrue(xmlRate, 			webRate, 		"Verify payment page values - Total rate");
		
			value 			= converter.convert(defaultCC, res.getPrice(), paymentPageValues.get("currencyCode"), currencyMap, res.getCurrencyCode());
			pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap);
			bf				= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap);
			ccFee			= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, paymentPageValues.get("currencyCode"), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String subTotal = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
			String total	= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee));
			
			printTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), 	paymentPageValues.get("hotelSubTotal"), 					"Verify payment page values - Hotel sub total");
			printTemplate.verifyTrue(bf, 																paymentPageValues.get("hotelTax"), 							"Verify payment page values - Hotel tax");
			printTemplate.verifyTrue(String.valueOf(Integer.parseInt(subTotal) + Integer.parseInt(bf)), paymentPageValues.get("hotelTotal"), 						"Verify payment page values - Hotel total");
			printTemplate.verifyTrue(subTotal, 															paymentPageValues.get("totalGrossPackageBookingValue"),	 	"Verify payment page values - Total gross booking value");
			printTemplate.verifyTrue(String.valueOf(Integer.parseInt(ccFee) + Integer.parseInt(bf)), 	paymentPageValues.get("totalTaxesAndOtherCharges"), 		"Verify payment page values - Total tax and other charges");
			printTemplate.verifyTrue(total, 															paymentPageValues.get("totalBookingValue"), 				"Verify payment page values - Total booking value");
			printTemplate.verifyTrue(total, 															paymentPageValues.get("amountBeingProcessedNow"), 			"Verify payment page values - Amount being processed now");
			printTemplate.verifyTrue(total, 															paymentPageValues.get("amountDueAtCheckin"), 				"Verify payment page values - Amount due at checkin");
			
			System.out.println(paymentPageValues.get("pgCurrency"));
			value 	= converter.convert(defaultCC, res.getPrice(), paymentPageValues.get("pgCurrency"), currencyMap, res.getCurrencyCode());
			pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentPageValues.get("pgCurrency"), currencyMap);
			bf		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, paymentPageValues.get("pgCurrency"), currencyMap);
			ccFee	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, paymentPageValues.get("pgCurrency"), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			subTotal = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
			total	= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee));
			
			printTemplate.verifyTrue(subTotal, 															paymentPageValues.get("pgTotalGrossPackageBookingValue"), 	"Verify payment page values - PG - Total gross package booking value");
			printTemplate.verifyTrue(String.valueOf(Integer.parseInt(ccFee) + Integer.parseInt(bf)), 	paymentPageValues.get("pgTotalTaxes"), 						"Verify payment page values - PG - Total taxes");
			printTemplate.verifyTrue(total, 															paymentPageValues.get("pgTotalPackageBookingValue"), 		"Verify payment page values - PG - Total package booking value");
			printTemplate.verifyTrue(total, 															paymentPageValues.get("pgAmountBeingProcessedNow"), 		"Verify payment page values - PG - Amount being processed now");
			printTemplate.verifyTrue(total, 															paymentPageValues.get("pgAmountDueAtCheckin"), 				"Verify payment page values - PG - Amount due at checkin");
			printTemplate.verifyTrue(canDeadline, 														paymentPageValues.get("hotelCancellationDeadline"), 		"Verify payment page values - Hotel cacnellation deadline");
		
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Verify payment page values");
		}
	}
	
	public void paymentPageMoreDetailsVerification(HotelPaymentPageMoreDetails ppMoreDetails, hotelDetails roomAvailablityRes, ReportTemplate PrintTemplate, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificData, String canDeadline, HotelSearchObject search)
	{
		System.out.println("Payment page - Shopping cart - More information - verification");
		try {
			String value 	= converter.convert(defaultCC, roomAvailablityRes.getPrice(), ppMoreDetails.getCurrencyCode(), currencyMap, roomAvailablityRes.getCurrencyCode());
			String pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, ppMoreDetails.getCurrencyCode(), currencyMap);
			String bf		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, ppMoreDetails.getCurrencyCode(), currencyMap);
			String ccFee	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, ppMoreDetails.getCurrencyCode(), currencyMap, portalSpecificData.get("portal.pgCurrency"));
//			PrintTemplate.setTableHeading("Verify payment page shopping cart and more details against room availability response");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf)), 							ppMoreDetails.getHotelPrice(), 					"Verify payment page shopping cart and more details against room availability response - Hotel price with profit markup (without CCFEE)");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), 													ppMoreDetails.getSubtotal(), 					"Verify payment page shopping cart and more details against room availability response - Shopping cart sub total");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccFee)), 													ppMoreDetails.getTaxes(), 						"Verify payment page shopping cart and more details against room availability response - Shopping cart taxes"); //-----
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(bf) + Integer.parseInt(pm) + Integer.parseInt(ccFee)), 	ppMoreDetails.getTotal(), 						"Verify payment page shopping cart and more details against room availability response - Shopping cart total");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(bf) + Integer.parseInt(pm) + Integer.parseInt(ccFee)), 	ppMoreDetails.getAmountProcessedNow(), 			"Verify payment page shopping cart and more details against room availability response - Shopping cart amount being processed now");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), 													ppMoreDetails.getMoreDetailsSubtotal(), 		"Verify payment page shopping cart and more details against room availability response - More details sub total");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(bf)), 																				ppMoreDetails.getMoreDetailsTax(), 				"Verify payment page shopping cart and more details against room availability response - More details taxes"); //----
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(bf) + Integer.parseInt(pm)), 							ppMoreDetails.getMoreDetailsHotelcost(), 		"Verify payment page shopping cart and more details against room availability response - More details hotel total cost");
			String status = "";
			String cancellationDeadline = "";
			if(search.getSupplier().equals("INV27"))
			{
				cancellationDeadline = Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", canDeadline);
				if(roomAvailablityRes.getAvailabilityStatus().equals("AVAILABLE"))
				{
					status = "Available to book";
				}
				else
				{
					status = roomAvailablityRes.getAvailabilityStatus();
				}
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				if(roomAvailablityRes.getAvailabilityStatus().equals("NEW"))
				{
					status = "Available to book";
				}
				else
				{
					status = roomAvailablityRes.getAvailabilityStatus();
				}
			}
			else
			{
				cancellationDeadline = canDeadline;
				status = roomAvailablityRes.getAvailabilityStatus();
			}
			PrintTemplate.verifyTrue(status, ppMoreDetails.getStatus().replace("Status - ", ""), 																									"Verify payment page shopping cart and more details against room availability response - More details hotel availability status");
			PrintTemplate.verifyTrue(Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", cancellationDeadline), ppMoreDetails.getCancellationDeadline(), 												"Verify payment page shopping cart and more details against room availability response - More details cancellation deadline");
			ArrayList<String> roomtype1 = new ArrayList<String>();
			ArrayList<String> rateplan1 = new ArrayList<String>();
			ArrayList<String> roomrate1 = new ArrayList<String>();
			for(int i = 0 ; i < roomAvailablityRes.getRoomDetails().size() ; i++)
			{
				roomtype1.add(roomAvailablityRes.getRoomDetails().get(i).getRoomName());
				if(roomAvailablityRes.getRoomDetails().get(i).getBoard().equals(null) || roomAvailablityRes.getRoomDetails().get(i).getBoard().isEmpty())
				{
					rateplan1.add("RoomOnly");
				}
				else
				{
					rateplan1.add(roomAvailablityRes.getRoomDetails().get(i).getBoard());
				}
				
				value 	= converter.convert(defaultCC, roomAvailablityRes.getRoomDetails().get(i).getPrice(), ppMoreDetails.getCurrencyCode(), currencyMap, roomAvailablityRes.getCurrencyCode());
				pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, ppMoreDetails.getCurrencyCode(), currencyMap);
				roomrate1.add(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)));
			}
			PrintTemplate.verifyArrayList(roomtype1, ppMoreDetails.getRoomType(), 																												"Verify payment page shopping cart and more details against room availability response - Room type");
			PrintTemplate.verifyArrayList(rateplan1, ppMoreDetails.getRatePlan(), 																												"Verify payment page shopping cart and more details against room availability response - Rate plan");
			PrintTemplate.verifyArrayList(roomrate1, ppMoreDetails.getRoomRate(), 																												"Verify payment page shopping cart and more details against room availability response - Room rate");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
//			PrintTemplate.markTableEnd();
		}
//		PrintTemplate.markTableEnd();
	}

	
	public void confirmationPageAgainsrReservationResponse(ReportTemplate PrintTemplate, HotelConfirmationPageDetails confirmation, hotelDetails res, HashMap<String, String> portalSpecificData, HashMap<String, String> currencyMap, String defaultCC, ArrayList<HotelOccupancyObject> occupancyList, HotelSearchObject search, String canDeadline, HashMap<String, String> paymentDetails, ArrayList<hotelDetails> hotelAvailabilityRes) throws ParseException
	{
//		PrintTemplate.setTableHeading("Verify confirmation page against reservation response");
		try {
			String[] roomTypeHelper = null;
			String[] rezliveRatePlan = null;
			if(search.getSupplier().equals("rezlive"))
			{
				for(int i = 0 ; i < hotelAvailabilityRes.size() ; i++)
				{
					if(hotelAvailabilityRes.get(i).getHotel().equals(res.getHotel()))
					{
						roomTypeHelper = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getRoomType().split("\\|");
						rezliveRatePlan = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getBoard().split("\\|");
						break;
					}
				}
			}
			
			String bookingReference = "";
			String chekin = "";
			String checkout = "";
			String nights = "";
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				if(search.getChannel().equals("cc"))
				{
					bookingReference = "Mr " + res.getFirstName().concat(" ").concat(res.getLastName());
					chekin = Calender.getDate("yyyyMMdd", "dd MMM yyyy", res.getCheckin());
					checkout = Calender.getDate("yyyyMMdd", "dd MMM yyyy", res.getCheckout());
					nights = res.getNights().replace("-", "");
				}
				else
				{
					bookingReference = "Mr " + res.getFirstName().concat(" ").concat(res.getLastName());
					chekin = Calender.getDate("yyyyMMdd", "MMM dd yyyy", res.getCheckin());
					checkout = Calender.getDate("yyyyMMdd", "MMM dd yyyy", res.getCheckout());
					nights = res.getNights().replace("-", "");	
				}
			}
			else if(search.getSupplier().equals("INV27"))
			{
				bookingReference = "Mr ".concat(portalSpecificData.get("cd.firstname")).concat(" ").concat(portalSpecificData.get("cd.lastname"));
				chekin = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", res.getCheckin());
				checkout = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", res.getCheckout());
				nights = res.getNights();
			}
			else if(search.getSupplier().equals("INV13"))
			{
				bookingReference = "Mr ".concat(portalSpecificData.get("cd.firstname")).concat(" ").concat(portalSpecificData.get("cd.lastname"));
				chekin = res.getCheckin();
				checkout = res.getCheckout();
				nights = res.getNights();
			}
			else if(search.getSupplier().equals("rezlive"))
			{
				bookingReference = "Mr ".concat(portalSpecificData.get("cd.firstname")).concat(" ").concat(portalSpecificData.get("cd.lastname"));
				chekin = res.getCheckin();
				checkout = res.getCheckout();
				nights = res.getNights();
			}
			else
			{
				bookingReference = "Mr ".concat(portalSpecificData.get("cd.firstname")).concat(" ").concat(portalSpecificData.get("cd.lastname"));
				chekin = res.getCheckin();
				checkout = res.getCheckout();
				nights = search.getNights();
			}
			if(search.getChannel().equals("web"))
			{
				PrintTemplate.verifyTrue(bookingReference, 																	confirmation.getBookingReference(), 			"Verify confirmation page against reservation response - Booking reference");
			}
			
			PrintTemplate.verifyTrue(res.getHotel(), 																		confirmation.getHotelName(), 					"Verify confirmation page against reservation response - Hotel name");
			String avaialbilityStatus = "";
			if(search.getChannel().equals("cc") && res.getAvailabilityStatus().equals("CONFIRMED") || search.getChannel().equals("cc") && res.getAvailabilityStatus().equals("Confirmed"))
			{
				avaialbilityStatus = "Hotel Bookings Saved Successfully";
			}
			else if(search.getChannel().equals("web") && res.getAvailabilityStatus().equals("CONFIRMED") || search.getChannel().equals("web") && res.getAvailabilityStatus().equals("Confirmed"))
			{
				avaialbilityStatus = "Confirmed";
			}
			else
			{
				avaialbilityStatus = "Failed";
			}
			System.out.println(avaialbilityStatus);
			System.out.println(confirmation.getStatus());
			PrintTemplate.verifyTrue(avaialbilityStatus, 										confirmation.getStatus(), 						"Verify confirmation page against reservation response - Status");
			PrintTemplate.verifyTrue(res.getSupConfirmationNumber(), 							confirmation.getSupplierConfirmationNumber(), 	"Verify confirmation page against reservation response - Supplier confirmation number");
			PrintTemplate.verifyTrue(chekin,		 											confirmation.getChekin(), 						"Verify confirmation page against reservation response - Checkin date");
			PrintTemplate.verifyTrue(checkout, 													confirmation.getCheckout(), 					"Verify confirmation page against reservation response - Checkout date");
			PrintTemplate.verifyTrue(res.getRoomCount(), 										confirmation.getNumberOfRooms(), 				"Verify confirmation page against reservation response - Number of rooms");
			PrintTemplate.verifyTrue(nights,			 										confirmation.getNumberOfNights(), 				"Verify confirmation page against reservation response - Number of nights");
			
			ArrayList<String> 	webRoomType 	= new ArrayList<String>();
			ArrayList<String> 	xmlRoomType 	= new ArrayList<String>();
			ArrayList<String> 	webBedType 		= new ArrayList<String>();
			ArrayList<String> 	xmlBedType 		= new ArrayList<String>();
			ArrayList<String> 	webRatePlan 	= new ArrayList<String>();
			ArrayList<String> 	xmlRatePlan 	= new ArrayList<String>();
			ArrayList<String> 	webRate 		= new ArrayList<String>();
			ArrayList<String> 	xmlRate	 		= new ArrayList<String>();
			
			for(int i = 0 ; i < res.getRoomDetails().size() ; i++)
			{
				webRoomType.add	(confirmation.getRoom().get(i).getRoomType());
				xmlRoomType.add	(res.getRoomDetails().get(i).getRoomName());
				
				webBedType.add	(confirmation.getRoom().get(i).getBedType());
				webRatePlan.add	(confirmation.getRoom().get(i).getRatePlan());
				if(search.getSupplier().equals("rezlive"))
				{
					String[] roomType = roomTypeHelper[0].split("-");
					xmlBedType.add	(roomType[0]);
					xmlRatePlan.add(rezliveRatePlan[0]);
				}
				else
				{
					xmlBedType.add	(res.getRoomDetails().get(i).getBedType());
					xmlRatePlan.add	(res.getRoomDetails().get(i).getBoard());
				}
				
				
				String value = "";
				String pm = "";
				if(search.getSupplier().equals("rezlive"))
				{
					String priceHelper = res.getRoomDetails().get(0).getPrice().replace("//|", "/");
					String[] price = res.getRoomDetails().get(0).getPrice().split("\\|");
					for(int j = 0 ; j < price.length ; j++)
					{
						value 			= converter.convert(defaultCC, price[j], confirmation.getCurrencyCode(), currencyMap, res.getCurrencyCode());
						pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, confirmation.getCurrencyCode(), currencyMap);
						webRate.add		(confirmation.getRoom().get(j).getRate());
						xmlRate.add		(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)));
					}
				}
				else
				{
					value 			= converter.convert(defaultCC, res.getRoomDetails().get(i).getPrice(), confirmation.getCurrencyCode(), currencyMap, res.getCurrencyCode());
					pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, confirmation.getCurrencyCode(), currencyMap);
					webRate.add		(confirmation.getRoom().get(i).getRate());
					xmlRate.add		(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)));
				}
			}
			
			PrintTemplate.verifyArrayList(xmlRoomType, 	webRoomType, 	"Verify confirmation page against reservation response - Room type");
			PrintTemplate.verifyArrayList(xmlBedType, 	webBedType, 	"Verify confirmation page against reservation response - Bed type");
			PrintTemplate.verifyArrayList(xmlRatePlan, 	webRatePlan, 	"Verify confirmation page against reservation response - Rate plan");
			PrintTemplate.verifyArrayList(xmlRate, 		webRate, 		"Verify confirmation page against reservation response - Room rate");
			
			String value 	= converter.convert(defaultCC, res.getPrice(), confirmation.getCurrencyCode(), 	currencyMap, 	res.getCurrencyCode());
			String pm		= tax.getTax(value, 	portalSpecificData.get("hotelpm"), 					defaultCC, 		confirmation.getCurrencyCode(), currencyMap);
			String bf		= tax.getTax(value,	 	portalSpecificData.get("hotelbf"), 					defaultCC, 		confirmation.getCurrencyCode(), currencyMap);
			String ccFee	= tax.getTax(value, 	portalSpecificData.get("portal.ccfee"), 			defaultCC, 		confirmation.getCurrencyCode(), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), 													confirmation.getHotelSubtotal(), 	"Verify confirmation page against reservation response - Hotel sub total");
			PrintTemplate.verifyTrue(bf, 																												confirmation.getHotelTax(), 		"Verify confirmation page against reservation response - Hotel taxes");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf)), 							confirmation.getHotelTotal(), 		"Verify confirmation page against reservation response - Hotel total");
			
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm)), 													confirmation.getSubtotal(), 				"Verify confirmation page against reservation response - Sub total");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(ccFee) + Integer.parseInt(bf)), 													confirmation.getTax(), 						"Verify confirmation page against reservation response - Total tax and other charges");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee)), 	confirmation.getTotal(), 					"Verify confirmation page against reservation response - Total");
			PrintTemplate.verifyTrue(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee)), 	confirmation.getAmountBeingProcessedNow(), 	"Verify confirmation page against reservation response - Amount being processed now");
			
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.firstname"),													 	confirmation.getFirstName(), 						"Verify confirmation page against reservation response - First name");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.lastname"), 														confirmation.getLastName(), 						"Verify confirmation page against reservation response - Last name");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.address"), 															confirmation.getAddress1(), 						"Verify confirmation page against reservation response - Address 1");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.city"), 															confirmation.getCity(), 							"Verify confirmation page against reservation response - City");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.country"), 															confirmation.getCountry(), 							"Verify confirmation page against reservation response - Country");
			String state = "";
			if(portalSpecificData.get("cd.country").equals("USA") ||portalSpecificData.get("cd.country").equals("Australia") ||portalSpecificData.get("cd.country").equals("Canada"))
			{
				state = portalSpecificData.get("cd.state");
			}
			else
			{
				state = "-";
			}
			PrintTemplate.verifyTrue(state, 															confirmation.getState(), 							"Verify confirmation page against reservation response - State");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.postalcode"), 														confirmation.getPostalCode(), 						"Verify confirmation page against reservation response - Postal code");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.phoneNumberCode").concat("-").concat(portalSpecificData.get("cd.phoneNumber")), confirmation.getPhoneNumber(), 			"Verify confirmation page against reservation response - Emergency number");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.phoneNumberCode").concat("-").concat(portalSpecificData.get("cd.phoneNumber")), confirmation.getEmergencyNumber(), 		"Verify confirmation page against reservation response - Phone number");
			PrintTemplate.verifyTrue(portalSpecificData.get("cd.hotelEmail"), 														confirmation.getEmail(), 							"Verify confirmation page against reservation response - Email");
			
			String[] adultCountHelper = search.getAdults().split("/");
			String[] childCountHelper = search.getChildren().split("/");
			int adultCnt = 0 ;
			int childCnt = 0 ;
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				adultCnt = adultCnt + Integer.parseInt(adultCountHelper[i]);
				childCnt = childCnt + Integer.parseInt(childCountHelper[i]);
			}
			
			ArrayList<String> webAddults 	= new ArrayList<String>();
			ArrayList<String> excelAddults 	= new ArrayList<String>();
			ArrayList<String> webChildren 	= new ArrayList<String>();
			ArrayList<String> excelChildren = new ArrayList<String>();
			
			for(int i = 0 ; i < adultCnt ; i++)
			{
				webAddults.add		(confirmation.getAdultOccupancy().get(i).getFirstname().concat(" ").concat(confirmation.getAdultOccupancy().get(i).getLastname()));
				excelAddults.add	(occupancyList.get(i).getAdultFirstName().concat(" ").concat(occupancyList.get(i).getAdultLastName()));
				webChildren.add		(confirmation.getChildOccupancy().get(i).getFirstname().concat(" ").concat(confirmation.getChildOccupancy().get(i).getLastname()));
				excelChildren.add	(occupancyList.get(i).getChildFirstName().concat(" ").concat(occupancyList.get(i).getChildLastName()));
			}
			PrintTemplate.verifyArrayList(excelAddults, webAddults, 	"Verify confirmation page against reservation response - Adult names");
			PrintTemplate.verifyArrayList(excelChildren, webChildren, 	"Verify confirmation page against reservation response - Children names");
			//Calender cal = new Calender();
			SimpleDateFormat 	sdf1 		= new SimpleDateFormat("yyyy-MMM-dd");
			SimpleDateFormat 	sdf2 		= new SimpleDateFormat("dd-MMM-yyyy");
	    	Date 				canDead 	= sdf2.parse(canDeadline);
	    	Date 				today 		= sdf1.parse(Calender.getToday("yyyy-MMM-dd"));
	    	if(canDead.after(today))
	    	{
	    		PrintTemplate.verifyTrue(Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", canDeadline), confirmation.getCancellationDeadline(), "Cancellation deadline");
	    	}
	    	else
	    	{
	    		PrintTemplate.verifyTrue(portalSpecificData.get("withinCancellation"), confirmation.getCancellationDeadline(), "Cancellation deadline");
	    	}
	    	PrintTemplate.verifyTrue(paymentDetails.get("transactionID"), 		confirmation.getMerchantTrackId(), 				"Verify confirmation page against reservation response - Transaction ID");
	    	PrintTemplate.verifyTrue(paymentDetails.get("authId"), 				confirmation.getAuthenticationReference(), 		"Verify confirmation page against reservation response - Authentication reference");
	    	PrintTemplate.verifyTrue(paymentDetails.get("paymentReference"), 	confirmation.getPaymentID(), 					"Verify confirmation page against reservation response - Payment ID");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
//		PrintTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaLabelsAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaLabels)
	{
//		printTemplate.setTableHeading("Reservation report criteria labels availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productType"), 					"Reservation report criteria labels availability - Product Type");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypeAll"), 				"Reservation report criteria labels availability - Product Type - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypeHotel"), 			"Reservation report criteria labels availability - Product Type - Hotel");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("ProductTypeAir"), 				"Reservation report criteria labels availability - Product Type - Air");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypeCar"), 				"Reservation report criteria labels availability - Product Type - Car");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("prodctTypeActivity"), 			"Reservation report criteria labels availability - Product Type - Activity");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypePackage"), 			"Reservation report criteria labels availability - Product Type - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchBy"), 					"Reservation report criteria labels availability - Search By");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchByDate"), 				"Reservation report criteria labels availability - Search By - Date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchByReservationNumber"), 	"Reservation report criteria labels availability - Search By - Reservation number");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchbyGuestName"), 			"Reservation report criteria labels availability - Search By - Guest name");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchbyXmlPackageReference"), 	"Reservation report criteria labels availability - Search By - Xml package reference");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("dateFilter"), 					"Reservation report criteria labels availability - Date filter");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("reportFrom"), 					"Reservation report criteria labels availability - Report from date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("reportTo"), 					"Reservation report criteria labels availability - Report to date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedOn"), 				"Reservation report criteria labels availability - Dates based on");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedonReservationDate"), 	"Reservation report criteria labels availability - Dates based on - Reservation date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedOnArrivalDate"), 		"Reservation report criteria labels availability - Dates based on - Arrival date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedOnDueDate"), 			"Reservation report criteria labels availability - Dates based on - Due date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApproved"), 				"Reservation report criteria labels availability - Payment approved");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApprovedAll"), 			"Reservation report criteria labels availability - Payment approved - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApprovedYes"), 			"Reservation report criteria labels availability - Payment approved - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApprovedNo"), 			"Reservation report criteria labels availability - Payment approved - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssued"), 				"Reservation report criteria labels availability - Invoice issued");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssuedAll"), 			"Reservation report criteria labels availability - Invoice issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssuedYes"), 			"Reservation report criteria labels availability - Invoice issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssuedNo"), 				"Reservation report criteria labels availability - Invoice issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssued"), 				"Reservation report criteria labels availability - Voucher issued");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssuedAll"), 			"Reservation report criteria labels availability - Voucher issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssuedYes"), 			"Reservation report criteria labels availability - Voucher issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssuedNo"), 				"Reservation report criteria labels availability - Voucher issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequired"), 					"Reservation report criteria labels availability - Lpo required");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequiredYes"), 				"Reservation report criteria labels availability - Lpo required - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequiredAll"), 				"Reservation report criteria labels availability - Lpo required - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequiredNo"), 				"Reservation report criteria labels availability - Lpo required - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannel"), 				"Reservation report criteria labels availability - Booking channel");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannelAll"), 			"Reservation report criteria labels availability - Booking channel - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannelCC"), 			"Reservation report criteria labels availability - Booking channel - CC");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannelWeb"), 			"Reservation report criteria labels availability - Booking channel - Web");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partner"), 						"Reservation report criteria labels availability - Partner");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerAll"), 					"Reservation report criteria labels availability - Partner - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerDirectConsumer"), 		"Reservation report criteria labels availability - Partner - Direct Consumer");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerAffiliate"), 			"Reservation report criteria labels availability - Partner - Affiliate");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerB2bPartner"), 			"Reservation report criteria labels availability - Partner - B2B partner");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("posLocation"), 					"Reservation report criteria labels availability - Pos location");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("generatePDF"), 					"Reservation report criteria labels availability - Generate PDF");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("generatePDFYes"), 				"Reservation report criteria labels availability - Generate PDF - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("generatePDFNo"), 				"Reservation report criteria labels availability - Generate PDF - No");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaButtonsAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaButtons)
	{
//		printTemplate.setTableHeading("Reservation report criteria buttons availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaButtons.get("posLocation"), 	"Reservation report criteria buttons availability - Pos Location");
			printTemplate.verifyTrue(true, resReportCriteriaButtons.get("view"), 			"Reservation report criteria buttons availability - View");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaComboboxAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaCombobox)
	{
//		printTemplate.setTableHeading("Reservation report criteria combobox availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("dateFilter"), 		"Reservation report criteria combobox availability - Date filter");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportFromDate"), 	"Reservation report criteria combobox availability - Report from date");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportFromMonth"), 	"Reservation report criteria combobox availability - Report from month");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportFromYear"), 	"Reservation report criteria combobox availability - Report from year");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportToDate"), 		"Reservation report criteria combobox availability - Report to date");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportToMonth"), 		"Reservation report criteria combobox availability - Report to month");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportToYear"), 		"Reservation report criteria combobox availability - Report to year");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaRadioButtonsAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaRadioButtons)
	{
//		printTemplate.setTableHeading("Reservation report criteria Radio buttons availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeAll"), 				"Reservation report criteria Radio buttons availability - Product type - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeHotel"), 				"Reservation report criteria Radio buttons availability - Product type - Hotel");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeAir"), 				"Reservation report criteria Radio buttons availability - Product type - Air");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeActivity"), 			"Reservation report criteria Radio buttons availability - Product type - Activity");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeCar"), 				"Reservation report criteria Radio buttons availability - Product type - Car");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypePackage"), 			"Reservation report criteria Radio buttons availability - Product type - Package");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("searchByDates"), 					"Reservation report criteria Radio buttons availability - Search by - Date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("searchByReservations"), 			"Reservation report criteria Radio buttons availability - Search by - reservation number");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("searchByGuestName"), 				"Reservation report criteria Radio buttons availability - Search by - guest name");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("SearchByXmlPackageRef"), 			"Reservation report criteria Radio buttons availability - Search by - xml package reference");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("datesBasedOnReservationDate"), 	"Reservation report criteria Radio buttons availability - Dates based on - Reservation date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("datesBasedOnArrivalDate"), 		"Reservation report criteria Radio buttons availability - Dates based on - Arrival date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("datesBasedOnDueDate"), 			"Reservation report criteria Radio buttons availability - Dates based on - Due date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("paymentApprovedAll"), 			"Reservation report criteria Radio buttons availability - Payment approved - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("paymentApprovedYes"), 			"Reservation report criteria Radio buttons availability - Payment approved - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("paymentApprovedNo"), 				"Reservation report criteria Radio buttons availability - Payment approved - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("invoiceIssuedAll"), 				"Reservation report criteria Radio buttons availability - Invoice issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("invoiceIssuedYes"), 				"Reservation report criteria Radio buttons availability - Invoice issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("invoiceIssuedNo"), 				"Reservation report criteria Radio buttons availability - Invoice issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("voucherIssuedAll"), 				"Reservation report criteria Radio buttons availability - Voucher issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("voucherIssuedYes"), 				"Reservation report criteria Radio buttons availability - Voucher issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("voucherIssuedNo"), 				"Reservation report criteria Radio buttons availability - Voucher issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("lpoAll"), 						"Reservation report criteria Radio buttons availability - Lpo required - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("lpoYes"),		 					"Reservation report criteria Radio buttons availability - Lpo required - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("lpoNo"), 							"Reservation report criteria Radio buttons availability - Lpo required - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("bookingChannelAll"), 				"Reservation report criteria Radio buttons availability - Booking channel - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("bookingChannelWeb"), 				"Reservation report criteria Radio buttons availability - Booking channel - Web");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("bookingChannelCC"), 				"Reservation report criteria Radio buttons availability - Booking channel - CC");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerAll"), 					"Reservation report criteria Radio buttons availability - Partner - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerDirectConsumer"), 			"Reservation report criteria Radio buttons availability - Partner - Direct customer");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerAffiliate"), 				"Reservation report criteria Radio buttons availability - Partner - Affiliate");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerB2B"), 					"Reservation report criteria Radio buttons availability - Partner - B2B partner");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("pdfNo"), 							"Reservation report criteria Radio buttons availability - Generate PDF - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("pdfYes"), 						"Reservation report criteria Radio buttons availability - Generate PDF - Yes");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaLabelsVerification(ReportTemplate printTemplate, HashMap<String, String> resReportCriteriaLabels, HashMap<String, String> labels)
	{
//		printTemplate.setTableHeading("Reservation report criteria labels verification");
		try {
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductType"), 					resReportCriteriaLabels.get("productType"), 				"Reservation report criteria labels verification - Product type");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeAll"), 				resReportCriteriaLabels.get("productTypeAll"), 				"Reservation report criteria labels verification - Product type - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeHotels"), 				resReportCriteriaLabels.get("productTypeHotel"), 			"Reservation report criteria labels verification - Product type - Hotel");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeAir"), 				resReportCriteriaLabels.get("ProductTypeAir"), 				"Reservation report criteria labels verification - Product type - Air");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeCar"), 				resReportCriteriaLabels.get("productTypeCar"), 				"Reservation report criteria labels verification - Product type - Car");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeActivities"), 			resReportCriteriaLabels.get("prodctTypeActivity"), 			"Reservation report criteria labels verification - Product type - Activity");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypePackage"), 			resReportCriteriaLabels.get("productTypePackage"), 			"Reservation report criteria labels verification - Product type - Package");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchBy"), 						resReportCriteriaLabels.get("searchBy"), 					"Reservation report criteria labels verification - Search by");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByDates"), 					resReportCriteriaLabels.get("searchByDate"), 				"Reservation report criteria labels verification - Search by - Date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByResNumber"), 				resReportCriteriaLabels.get("searchByReservationNumber"), 	"Reservation report criteria labels verification - Search by - Reservation number");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByGuestname"), 				resReportCriteriaLabels.get("searchbyGuestName"), 			"Reservation report criteria labels verification - Search by - Guest name");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByXmlPckRef"), 				resReportCriteriaLabels.get("searchbyXmlPackageReference"), "Reservation report criteria labels verification - Search by - Xml package reference");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDateFilter"), 					resReportCriteriaLabels.get("dateFilter"), 					"Reservation report criteria labels verification - Date filter");
			printTemplate.verifyTrue(labels.get("resReportCriteriaReportFrom"), 					resReportCriteriaLabels.get("reportFrom"), 					"Reservation report criteria labels verification - Report from");
			printTemplate.verifyTrue(labels.get("resReportCriteriaReportTo"), 						resReportCriteriaLabels.get("reportTo"), 					"Reservation report criteria labels verification - Report to");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOn"), 					resReportCriteriaLabels.get("datesBasedOn"), 				"Reservation report criteria labels verification - Dates based on");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOnReservationDate"), 	resReportCriteriaLabels.get("datesBasedonReservationDate"), "Reservation report criteria labels verification - Dates based on - reservation date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOnArrivalDate"), 		resReportCriteriaLabels.get("datesBasedOnArrivalDate"), 	"Reservation report criteria labels verification - Dates based on - Arrival date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOnDueDate"), 			resReportCriteriaLabels.get("datesBasedOnDueDate"), 		"Reservation report criteria labels verification - Dates based on - Due date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApproved"), 				resReportCriteriaLabels.get("paymentApproved"), 			"Reservation report criteria labels verification - Payment approved");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApprovedAll"), 			resReportCriteriaLabels.get("paymentApprovedAll"), 			"Reservation report criteria labels verification - Payment approved - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApprovedYes"), 			resReportCriteriaLabels.get("paymentApprovedYes"), 			"Reservation report criteria labels verification - Payment approved - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApprovedNo"), 				resReportCriteriaLabels.get("paymentApprovedNo"), 			"Reservation report criteria labels verification - Payment approved - No");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssued"), 					resReportCriteriaLabels.get("invoiceIssued"), 				"Reservation report criteria labels verification - Invoice issued");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssuedAll"), 				resReportCriteriaLabels.get("invoiceIssuedAll"), 			"Reservation report criteria labels verification - Invoice issued - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssuedYes"), 				resReportCriteriaLabels.get("invoiceIssuedYes"), 			"Reservation report criteria labels verification - Invoice issued - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssuedNo"), 				resReportCriteriaLabels.get("invoiceIssuedNo"), 			"Reservation report criteria labels verification - Invoice issued - No");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssued"), 					resReportCriteriaLabels.get("voucherIssued"), 				"Reservation report criteria labels verification - Voucher issued");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssuedAll"), 				resReportCriteriaLabels.get("voucherIssuedAll"), 			"Reservation report criteria labels verification - Voucher issued - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssuedYes"), 				resReportCriteriaLabels.get("voucherIssuedYes"), 			"Reservation report criteria labels verification - Voucher issued - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssuedNo"), 				resReportCriteriaLabels.get("voucherIssuedNo"), 			"Reservation report criteria labels verification - Voucher issued - No");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequired"), 					resReportCriteriaLabels.get("lpoRequired"), 				"Reservation report criteria labels verification - Lpo required");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequiredAll"), 				resReportCriteriaLabels.get("lpoRequiredAll"), 				"Reservation report criteria labels verification - Lpo required - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequiredYes"), 				resReportCriteriaLabels.get("lpoRequiredYes"), 				"Reservation report criteria labels verification - Lpo required - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequiredNo"), 					resReportCriteriaLabels.get("lpoRequiredNo"), 				"Reservation report criteria labels verification - Lpo required - No,");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannel"), 				resReportCriteriaLabels.get("bookingChannel"), 				"Reservation report criteria labels verification - Booking channel");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannelAll"), 				resReportCriteriaLabels.get("bookingChannelAll"), 			"Reservation report criteria labels verification - Booking channel - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannelWeb"), 				resReportCriteriaLabels.get("bookingChannelWeb"), 			"Reservation report criteria labels verification - Booking channel - Web");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannelCC"), 				resReportCriteriaLabels.get("bookingChannelCC"), 			"Reservation report criteria labels verification - Booking channel - CC");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartner"), 						resReportCriteriaLabels.get("partner"), 					"Reservation report criteria labels verification - Partner");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerAll"), 					resReportCriteriaLabels.get("partnerAll"), 					"Reservation report criteria labels verification - Partner - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerDirectCustomer"), 			resReportCriteriaLabels.get("partnerDirectConsumer"), 		"Reservation report criteria labels verification - Partner - Direct consumer");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerAffiliate"), 				resReportCriteriaLabels.get("partnerAffiliate"), 			"Reservation report criteria labels verification - Partner - Affiliate");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerB2B"), 					resReportCriteriaLabels.get("partnerB2bPartner"), 			"Reservation report criteria labels verification - Partner - B2B");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPosLocationName"), 				resReportCriteriaLabels.get("posLocation"), 				"Reservation report criteria labels verification - Pos location");
			printTemplate.verifyTrue(labels.get("resReportCriteriaGeneratePdf"), 					resReportCriteriaLabels.get("generatePDF"), 				"Reservation report criteria labels verification - Generate PDF");
			printTemplate.verifyTrue(labels.get("resReportCriteriaGeneratePdfYes"), 				resReportCriteriaLabels.get("generatePDFYes"), 				"Reservation report criteria labels verification - Generate PDF - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaGeneratePdfNo"), 					resReportCriteriaLabels.get("generatePDFNo"), 				"Reservation report criteria labels verification - Generate PDF - No");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void resReportLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportLabelAvailability)
	{
//		printTemplate.setTableHeading("Reservation report labels availability");
		try {
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("more"), 						"Reservation report labels availability - More");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("info"), 						"Reservation report labels availability - Info");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("edit"), 						"Reservation report labels availability - Edit");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("bookingNumber"), 			"Reservation report labels availability - Booking number");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("documentNumber"), 			"Reservation report labels availability - Document number");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("bookingDate"), 				"Reservation report labels availability - Booking date");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("notes"), 					"Reservation report labels availability - Notes");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("supplierName"), 				"Reservation report labels availability - Supplier name");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("hotelName"),	 				"Reservation report labels availability - Hotel name");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("bookingType"), 				"Reservation report labels availability - Booking type");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("firstName"), 				"Reservation report labels availability - Customer name");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("customer"), 					"Reservation report labels availability - Customer");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("consultantName"), 			"Reservation report labels availability - Consultant name");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("arrival"), 					"Reservation report labels availability - Arrival");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("roomType"), 					"Reservation report labels availability - Room type");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("bedType"), 					"Reservation report labels availability - Bed type");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("supConfirmationNumber"), 	"Reservation report labels availability - Supplier confirmation Number");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("contactNumber"), 			"Reservation report labels availability - Contact number");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("transactionId"), 			"Reservation report labels availability - Transaction id");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("paymentType"), 				"Reservation report labels availability - Payment id");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("hotelConfirmationNumber"), 	"Reservation report labels availability - Hotel confirmation number");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("orderId"), 					"Reservation report labels availability - Order id");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("sellingCurrency"), 			"Reservation report labels availability - Selling currency");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("totalCost"), 				"Reservation report labels availability - Total cost");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("bookingFee"), 				"Reservation report labels availability - Booking fee");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("orderValue"), 				"Reservation report labels availability - Order value");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("rooms"), 					"Reservation report labels availability - Rooms");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("totalCostInPortalCurrency"), "Reservation report labels availability - Total cost in portal currency");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("orderValueInPortalCurrency"), "Reservation report labels availability - Order valu in portal currency");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("pageTotal"), 				"Reservation report labels availability - Page total");
			printTemplate.verifyTrue(true, resReportLabelAvailability.get("grandTotal"), 				"Reservation report labels availability - Grand total");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void resReportLabelVerification(ReportTemplate printTemplate, HashMap<String, String> resReportLabels, HashMap<String, String> labels, HashMap<String, String> portalSpecificData)
	{
//		printTemplate.setTableHeading("Reservation report labels verification");
		try {

			printTemplate.verifyTrue(labels.get("resReportMore"), 						resReportLabels.get("more"), 						"Reservation report labels verification - More");
			printTemplate.verifyTrue(labels.get("resReportInfo"), 						resReportLabels.get("info"), 						"Reservation report labels verification - Info");
			printTemplate.verifyTrue(labels.get("ResReportEdit"), 						resReportLabels.get("edit"), 						"Reservation report labels verification - Edit");
			printTemplate.verifyTrue(labels.get("resReportBookingNumber"), 				resReportLabels.get("bookingNumber"), 				"Reservation report labels verification - Booking number");
			printTemplate.verifyTrue(labels.get("resReportDocumentNumber"), 			resReportLabels.get("documentNumber"), 				"Reservation report labels verification - Document number");
			printTemplate.verifyTrue(labels.get("resReportBookingDate"), 				resReportLabels.get("bookingDate"), 				"Reservation report labels verification - Booking date");
			printTemplate.verifyTrue(labels.get("resReportNotes"), 						resReportLabels.get("notes"), 						"Reservation report labels verification - Notes");
			printTemplate.verifyTrue(labels.get("resReportSupplierName"), 				resReportLabels.get("supplierName"), 				"Reservation report labels verification - Supplier name");
			printTemplate.verifyTrue(labels.get("resReportHotelName"), 					resReportLabels.get("hotelName"),	 				"Reservation report labels verification - Hotel name");
			printTemplate.verifyTrue(labels.get("resReportBookingType"), 				resReportLabels.get("bookingType"), 				"Reservation report labels verification - Booking type");
			printTemplate.verifyTrue(labels.get("resReportName"), 						resReportLabels.get("firstName"), 					"Reservation report labels verification - Customer name");
			printTemplate.verifyTrue(labels.get("resReportCustomerType"), 				resReportLabels.get("customer"), 					"Reservation report labels verification - Customer");
			printTemplate.verifyTrue(labels.get("resReportConsultantName"), 			resReportLabels.get("consultantName"), 				"Reservation report labels verification - Consultant name");
			printTemplate.verifyTrue(labels.get("resReportArrival"), 					resReportLabels.get("arrival"), 					"Reservation report labels verification - Arrival");
			printTemplate.verifyTrue(labels.get("resReportRoomType"), 					resReportLabels.get("roomType"), 					"Reservation report labels verification - Room type");
			printTemplate.verifyTrue(labels.get("resReportBedType"), 					resReportLabels.get("bedType"), 					"Reservation report labels verification - Bed type");
			printTemplate.verifyTrue(labels.get("resReportSupplierConfirm"), 			resReportLabels.get("supConfirmationNumber"), 		"Reservation report labels verification - Supplier confirmation Number");
			printTemplate.verifyTrue(labels.get("resReportContactNumber"), 				resReportLabels.get("contactNumber"), 				"Reservation report labels verification - Contact number");
			printTemplate.verifyTrue(labels.get("resReportTransactionId"), 				resReportLabels.get("transactionId"), 				"Reservation report labels verification - Transaction id");
			printTemplate.verifyTrue(labels.get("resReportpaymentType"), 				resReportLabels.get("paymentType"), 				"Reservation report labels verification - Payment id");
			printTemplate.verifyTrue(labels.get("resReportHotelConfirmation"), 			resReportLabels.get("hotelConfirmationNumber"), 	"Reservation report labels verification - Hotel confirmation number");
			printTemplate.verifyTrue(labels.get("resReportOrderId"), 					resReportLabels.get("orderId"), 					"Reservation report labels verification - Order id");
			printTemplate.verifyTrue(labels.get("resReportSellCurrency"), 				resReportLabels.get("sellingCurrency"), 			"Reservation report labels verification - Selling currency");
			printTemplate.verifyTrue(labels.get("resReportTotalCost"), 					resReportLabels.get("totalCost"), 					"Reservation report labels verification - Total cost");
			printTemplate.verifyTrue(labels.get("resReportHotelBookingFee"), 			resReportLabels.get("bookingFee"), 					"Reservation report labels verification - Booking fee");
			printTemplate.verifyTrue(labels.get("resReportOrderValue"), 				resReportLabels.get("orderValue"), 					"Reservation report labels verification - Order value");
			printTemplate.verifyTrue(labels.get("resReportRooms"), 						resReportLabels.get("rooms"), 						"Reservation report labels verification - Rooms");
			printTemplate.verifyTrue(labels.get("resReportTotalCostPortalCurrency").replace("USD", portalSpecificData.get("portal.Currency")), 	resReportLabels.get("totalCostInPortalCurrency"), 	"Reservation report labels verification - Total cost in portal currency");
			printTemplate.verifyTrue(labels.get("resReportOrderValuePortalCurrency").replace("USD", portalSpecificData.get("portal.Currency")), 	resReportLabels.get("orderValueInPortalCurrency"), 	"Reservation report labels verification - Order valu in portal currency");
			printTemplate.verifyTrue(labels.get("resReportPageTotal"), 					resReportLabels.get("pageTotal"), 					"Reservation report labels verification - Page total");
			printTemplate.verifyTrue(labels.get("resReportGrandTotal"), 				resReportLabels.get("grandTotal"), 					"Reservation report labels verification - Grand total");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void resReportValueAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportValueAvailability)
	{
//		printTemplate.setTableHeading("Reservation report value availability");
		try {
			printTemplate.verifyTrue(true, resReportValueAvailability.get("bookingNumber"), 				"Reservation report value availability - Booking number");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("documentNumber"), 				"Reservation report value availability - Document number");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("booingDate"), 					"Reservation report value availability - Booking date");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("notes"), 						"Reservation report value availability - Hotel notes");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("supplierName"), 					"Reservation report value availability - Supplier Name");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("hotelName"), 					"Reservation report value availability - Hotel name");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("bookingType"), 					"Reservation report value availability - Booking type / Channel");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("firstName"), 					"Reservation report value availability - First Name / Last name");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("customerType"), 					"Reservation report value availability - Customer type");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("consultantName"), 				"Reservation report value availability - Consultant name");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("arrival"), 						"Reservation report value availability - Arrival and Departure dates");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("roomType"), 						"Reservation report value availability - Room type");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("bedType"), 						"Reservation report value availability - Bed type / Rate plan");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("supplierConfirmationNumber"), 	"Reservation report value availability - Supplier confirmation number");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("contactNumber"), 				"Reservation report value availability - Contact number");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("transactionId"), 				"Reservation report value availability - Transaction id / Auth code");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("paymentType"), 					"Reservation report value availability - Payment type / Payment reference number");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("confirmationNumber"), 			"Reservation report value availability - Hotel confirmation number");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("orderId"), 						"Reservation report value availability - Order id");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("sellingCurrency"), 				"Reservation report value availability - Selling currency");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("totalCost"), 					"Reservation report value availability - Total cost");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("bookingFee"), 					"Reservation report value availability - Booking fee");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("orderValue"), 					"Reservation report value availability - Order value");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("rooms"), 						"Reservation report value availability - Rooms / Nights");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("totalCost"), 					"Reservation report value availability - Total cost");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("orderValueInPortalCurrency"), 	"Reservation report value availability - Order value in portal currency");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("pageTotalTotalCost"), 			"Reservation report value availability - Page total - Total cost");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("pageTotalOrderValue"), 			"Reservation report value availability - Page total - Order value");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("grandTotalTotalCost"), 			"Reservation report value availability - Grand total - Total cost");
			printTemplate.verifyTrue(true, resReportValueAvailability.get("geandTotalOrderValue"), 			"Reservation report value availability - Grand total - Order value");	
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportVerify(ReportTemplate printTemplate, HashMap<String, String> values, hotelDetails res, HashMap<String, String> paymentDetails, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificData, ArrayList<HotelOccupancyObject> occupancyList, HotelConfirmationPageDetails confirmation, HotelSearchObject search, String defaultCC, String reportDate)
	{
		Calender cal = new Calender();
//		printTemplate.setTableHeading("Reservation report values verification");
		try {
			String channel = "";
			if(search.getChannel().equals("cc"))
			{
				channel = "Call Center";
			}
			else
			{
				channel = "web";
			}
			
			String bookingType 		= res.getAvailabilityStatus().concat(" / ").concat(channel.toUpperCase());
			String customerName 	= occupancyList.get(0).getAdultFirstName().concat(" ").concat(occupancyList.get(0).getAdultLastName());
			String customerType 	= "";
			if(search.getChannel().equals("web"))
			{
				customerType 		= "DC";
			}
			else if(search.getChannel().equals("cc"))
			{
				customerType 		= "DC";
			}
			String checkin 			= "";
			String checkout 		= "";
			String arrivalDeparture = "";
			String bedType			= "";
			String ratePlan 		= "";
			String bedRate 			= "";
			String supNumber 		= "";
			String SupName 			= "";
			String docNumber		= confirmation.getReservationNumber().replaceAll("\\D+","");
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				checkin 			= Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", res.getCheckin());
				checkout			= cal.getDate("yyyyMMdd", "dd-MMM-yyyy", res.getCheckout());
				bedType 			= "";
				ratePlan 			= res.getRoomDetails().get(0).getBoard();
				supNumber 			= res.getSupConfirmationNumber();
				SupName 			= "HotelBed_Hotels";
			}
			else if(search.getSupplier().equals("INV27"))
			{
				checkin 			= Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", res.getCheckin());
				checkout			= cal.getDate("yyyy-MM-dd", "dd-MMM-yyyy", res.getCheckout());
				if(res.getRoomDetails().get(0).getRoomCode().equals("DB"))
				{
					bedType 			= "Double";
				}
				else
				{
					bedType				= res.getRoomDetails().get(0).getRoomCode();
				}
				
				ratePlan 			= res.getRoomDetails().get(0).getDescription();
				supNumber 			= res.getSupConfirmationNumber();
				SupName 			= "GTA_Hotels";
			}
			else if(search.getSupplier().equals("INV13"))
			{
				//TODO
			}
			else if(search.getSupplier().equals("rezlive"))
			{
				checkin 			= Calender.getDate("MMM dd yyyy", "dd-MMM-yyyy", res.getCheckin());
				checkout			= cal.getDate("MMM dd yyyy", "dd-MMM-yyyy", res.getCheckout());
				bedType 			= "";
				ratePlan 			= res.getRoomDetails().get(0).getBoard();
				supNumber 			= res.getSupConfirmationNumber();
				SupName 			= "rezlive";
			}
			
			arrivalDeparture = checkin.concat(" / ").concat(checkout);
			if(bedType.isEmpty())
			{
				bedRate = ratePlan;
			}
			else
			{
				bedRate = bedType.concat(" / ").concat(ratePlan);
			}
			
			String contactNumber 		= portalSpecificData.get("cd.phoneNumberCode").concat("-").concat(portalSpecificData.get("cd.phoneNumber"));
			String transAuth 			= "";
			String paymentType 			= "";
			String paymentReference 	= paymentDetails.get("paymentReference");
			try {
				transAuth 				= paymentDetails.get("paymentID").concat("/");
				transAuth				= transAuth.concat(paymentDetails.get("authId"));
				paymentType 			= "";
				paymentReference 		= paymentDetails.get("paymentReference");
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			if(search.getOnlineOrOffline().equals("online"))
			{
				paymentType = "Credit Card";
			}
			else
			{
				//TODO
			}
			String paymentTypeReference = paymentType.concat(" / ").concat(paymentReference);
			String sellingCurrency = "";
			if(search.getCountry().equals("Sri Lanka"))
			{
				if(search.getChannel().equals("cc"))
				{
					sellingCurrency = "USD";
				}
				else
				{
					sellingCurrency = "LKR";
				}		
			}
			
			//in selling currency
			String valueSelling = "";
			valueSelling 		= converter.convert(defaultCC, res.getPrice(), values.get("sellingCurrency"), currencyMap, res.getCurrencyCode());
			
			String pmSelling 			= tax.getTax(valueSelling, portalSpecificData.get("hotelpm"), defaultCC, values.get("sellingCurrency"), currencyMap);
			String bfSelling 			= tax.getTax(valueSelling, portalSpecificData.get("hotelbf"), defaultCC, values.get("sellingCurrency"), currencyMap);
			String ccfeeSelling 		= tax.getTax(valueSelling, portalSpecificData.get("portal.ccfee"), defaultCC, values.get("sellingCurrency"), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String totalCostSelling 	= String.valueOf(Integer.parseInt(valueSelling) + Integer.parseInt(ccfeeSelling));
			String orderValueSelling 	= String.valueOf(Integer.parseInt(totalCostSelling) + Integer.parseInt(bfSelling) + Integer.parseInt(pmSelling));
			String roomsNights 			= search.getRooms().concat(" / ").concat(search.getNights());
			//in portal currency
			String valuePortal 			= converter.convert(defaultCC, res.getPrice(), defaultCC, currencyMap, res.getCurrencyCode());
			String pmPortal 			= tax.getTax(valuePortal, portalSpecificData.get("hotelpm"), defaultCC, defaultCC, currencyMap);
			String bfPortal 			= tax.getTax(valuePortal, portalSpecificData.get("hotelbf"), defaultCC, defaultCC, currencyMap);
			String ccfeePortal 			= tax.getTax(valuePortal, portalSpecificData.get("portal.ccfee"), defaultCC, defaultCC, currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String totalCostPortal 		= String.valueOf(Integer.parseInt(valuePortal) + Integer.parseInt(ccfeePortal));
			String orderValuePortal 	= String.valueOf(Integer.parseInt(totalCostPortal) + Integer.parseInt(bfPortal) + Integer.parseInt(pmPortal));
			
			printTemplate.verifyTrue(confirmation.getReservationNumber(), 		values.get("bookingNumber"), 				"Reservation report values verification - Booking number");
			//printTemplate.verifyTrue("", 										values.get("documentNumber"), 				"Reservation report values verification - Document number");
			printTemplate.verifyTrue(reportDate, 								values.get("booingDate"), 					"Reservation report values verification - Booking date");
			printTemplate.verifyTrue(portalSpecificData.get("internalNotes"), 	values.get("notes"), 						"Reservation report values verification - Hotel notes");
			printTemplate.verifyTrue(SupName,			 						values.get("supplierName"), 				"Reservation report values verification - Supplier Name");
			printTemplate.verifyTrue(res.getHotel(), 							values.get("hotelName"), 					"Reservation report values verification - Hotel Name");
			printTemplate.verifyTrue(bookingType, 								values.get("bookingType"), 					"Reservation report values verification - Booking type / Channel");
			printTemplate.verifyTrue(customerName, 								values.get("firstName"), 					"Reservation report values verification - First Name / Last name");
			printTemplate.verifyTrue(customerType, 								values.get("customerType"), 				"Reservation report values verification - Customer type");
			String consultant = "";
			if(search.getChannel().equals("web"))
			{
				consultant = "rezgadmin(rezadmin/rezadmin)";
			}
			else
			{
				consultant = portalSpecificData.get("hotelUsername") + "(" + occupancyList.get(0).getAdultFirstName() + "/" + occupancyList.get(0).getAdultLastName() + ")";
			}
			printTemplate.verifyTrue(consultant, 								values.get("consultantName"), 				"Reservation report values verification - Consultant name");
			printTemplate.verifyTrue(arrivalDeparture, 							values.get("arrival"), 						"Reservation report values verification - Arrival and Departure dates");
			String roomType = "";
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				if(search.getSupplier().equals("INV27"))
				{
					if(i == 0)
					{
						roomType = res.getRoomDetails().get(0).getDescription();
					}
					else
					{
						if(!roomType.contains(res.getRoomDetails().get(i).getDescription()))
						{
							roomType =  roomType + " / " + res.getRoomDetails().get(i).getDescription();
						}
					}
				}
				else
				{
					if(i == 0)
					{
						roomType = res.getRoomDetails().get(0).getRoomName();
					}
					else
					{
						if(!roomType.contains(res.getRoomDetails().get(i).getRoomName()))
						{
							roomType =  roomType + " / " + res.getRoomDetails().get(i).getRoomName();
						}
					}
				}
			}
			printTemplate.verifyTrue(roomType, 									values.get("roomType"), 					"Reservation report values verification - Room type");
			printTemplate.verifyTrue(bedRate, 									values.get("bedType"), 						"Reservation report values verification - Bed type / Rate plan");
			printTemplate.verifyTrue(supNumber, 								values.get("supplierConfirmationNumber"), 	"Reservation report values verification - Supplier confirmation number");
			printTemplate.verifyTrue(contactNumber, 							values.get("contactNumber"), 				"Reservation report values verification - Contact number");
			printTemplate.verifyTrue(transAuth, 								values.get("transactionId"), 				"Reservation report values verification - Transaction id / Auth code");
			printTemplate.verifyTrue(paymentTypeReference, 						values.get("paymentType"), 					"Reservation report values verification - Payment type / Payment reference number");
			printTemplate.verifyTrue(confirmation.getReservationNumber(), 		values.get("bookingNumber"), 				"Reservation report values verification - Hotel confirmation number");
			//printTemplate.verifyTrue("",										values.get("orderId"), 						"Reservation report values verification - Order id");
			printTemplate.verifyTrue(sellingCurrency, 							values.get("sellingCurrency"), 				"Reservation report values verification - Selling currency");
			printTemplate.verifyTrue(totalCostSelling, 							values.get("totalCostSelling"), 			"Reservation report values verification - Total cost");
			printTemplate.verifyTrue(bfSelling, 								values.get("bookingFee"), 					"Reservation report values verification - Booking fee");
			printTemplate.verifyTrue(orderValueSelling, 						values.get("orderValue"), 					"Reservation report values verification - Order value");
			printTemplate.verifyTrue(roomsNights, 								values.get("rooms"), 						"Reservation report values verification - Rooms / Nights");
			printTemplate.verifyTrue(totalCostPortal, 							values.get("totalCostPortal"), 				"Reservation report values verification - Total cost");
			printTemplate.verifyTrue(orderValuePortal, 							values.get("orderValueInPortalCurrency"), 	"Reservation report values verification - Order value in portal currency");
			printTemplate.verifyTrue(totalCostPortal, 							values.get("pageTotalTotalCost"), 			"Reservation report values verification - Page total - Total cost");
			printTemplate.verifyTrue(orderValuePortal, 							values.get("pageTotalOrderValue"), 			"Reservation report values verification - Page total - Order value");
			printTemplate.verifyTrue(totalCostPortal, 							values.get("grandTotalTotalCost"), 			"Reservation report values verification - Grand total - Total cost");
			printTemplate.verifyTrue(orderValuePortal, 							values.get("geandTotalOrderValue"), 		"Reservation report values verification - Grand total - Order value");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("", String.valueOf(e), "Reservation report values verification error - ");
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListReportCriteriaLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> blAvailabiltiy)
	{
//		printTemplate.setTableHeading("Booking list report - Search criteria label availability");
		{
			try {
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partner"), 						"Booking list report - Search criteria label availability - Partner");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerAll"), 					"Booking list report - Search criteria label availability - Partner - All");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerDc"), 					"Booking list report - Search criteria label availability - Partner - DC");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerb2b"), 					"Booking list report - Search criteria label availability - Partner - B2B");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerAffiliate"), 				"Booking list report - Search criteria label availability - Partner - Affiliate");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("posLocation"), 					"Booking list report - Search criteria label availability - Pos location");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerBookingNo"), 				"Booking list report - Search criteria label availability - Partner booking number");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("consultantName"), 				"Booking list report - Search criteria label availability - Consultant name");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productType"), 					"Booking list report - Search criteria label availability - Product type");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeAll"), 				"Booking list report - Search criteria label availability - Product type - All");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeActivities"), 		"Booking list report - Search criteria label availability - Product type - Activities");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeHotels"), 			"Booking list report - Search criteria label availability - Product type - Hotels");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeAir"), 				"Booking list report - Search criteria label availability - Product type - Air");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeCar"), 				"Booking list report - Search criteria label availability - Product type - Car");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypePackaging"), 			"Booking list report - Search criteria label availability - Product type - Packaging");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("firstAndLastName"), 				"Booking list report - Search criteria label availability - First and last name");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatus"), 				"Booking list report - Search criteria label availability - Booking status");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusAll"), 				"Booking list report - Search criteria label availability - Booking status - All");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusConfirmed"), 		"Booking list report - Search criteria label availability - Booking status - Confirmed");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusOnReq"), 			"Booking list report - Search criteria label availability - Booking status - On request");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusCancelled"), 		"Booking list report - Search criteria label availability - Booking status - Cancelled");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusModified"), 		"Booking list report - Search criteria label availability - Booking status - Modified");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusWithFailedSector"), "Booking list report - Search criteria label availability - Booking status - With fail sector");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("dateFilter"), 					"Booking list report - Search criteria label availability - Date filter");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("reportFrom"), 					"Booking list report - Search criteria label availability - Report from");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("reportTo"), 						"Booking list report - Search criteria label availability - Report to");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOn"), 					"Booking list report - Search criteria label availability - Dates based on");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnReservation"), 		"Booking list report - Search criteria label availability - Dates based on - Reservation");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnCancellation"), 		"Booking list report - Search criteria label availability - Dates based on - Cancellation");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnArrival"), 			"Booking list report - Search criteria label availability - Dates based on - Arrival");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnDeparture"), 		"Booking list report - Search criteria label availability - Dates based on - Departure");
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListReportCriteriaLabelVerification(ReportTemplate printTemplate, HashMap<String, String> report, HashMap<String, String> labelProperty)
	{
//		printTemplate.setTableHeading("Booking list report - Search criteria label verification");
		try {
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriapartner"), 							report.get("partner"), 						"Booking list report - Search criteria label verification - Partner");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaPartnerAll"), 						report.get("partnerAll"), 					"Booking list report - Search criteria label verification - Partner - All");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaPartnerDC"), 						report.get("partnerDc"), 					"Booking list report - Search criteria label verification - Partner - DC");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriapartnerB2B"), 						report.get("partnerb2b"), 					"Booking list report - Search criteria label verification - Partner - B2B");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriapartnerAffiliate"), 					report.get("partnerAffiliate"), 			"Booking list report - Search criteria label verification - Partner - Affiliate");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaPosLocationName"), 					report.get("posLocation"), 					"Booking list report - Search criteria label verification - Pos location");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingNo"), 						report.get("partnerBookingNo"), 			"Booking list report - Search criteria label verification - Partner booking number");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaConsultantName"), 					report.get("consultantName"), 				"Booking list report - Search criteria label verification - Consultant name");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductType"), 						report.get("productType"), 					"Booking list report - Search criteria label verification - Product type");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeAll"), 					report.get("productTypeAll"), 				"Booking list report - Search criteria label verification - Product type - All");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeActivities"), 			report.get("productTypeActivities"), 		"Booking list report - Search criteria label verification - Product type - Activities");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeHotels"), 				report.get("productTypeHotels"), 			"Booking list report - Search criteria label verification - Product type - Hotels");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeAir"), 					report.get("productTypeAir"), 				"Booking list report - Search criteria label verification - Product type - Air");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeCar"), 					report.get("productTypeCar"), 				"Booking list report - Search criteria label verification - Product type - Car");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypePackaging"), 				report.get("productTypePackaging"), 		"Booking list report - Search criteria label verification - Product type - Packaging");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaFirstAndLastName"),				 	report.get("firstAndLastName"), 			"Booking list report - Search criteria label verification - First and lact name");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatus"), 					report.get("bookingStatus"), 				"Booking list report - Search criteria label verification - Booking status");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusAll"), 					report.get("bookingStatusAll"), 			"Booking list report - Search criteria label verification - Booking status - All");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusConfirmed"), 			report.get("bookingStatusConfirmed"), 		"Booking list report - Search criteria label verification - Booking status - Confirmed");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusOnReq"), 				report.get("bookingStatusOnReq"), 			"Booking list report - Search criteria label verification - Booking status - On request");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusCancelled"), 			report.get("bookingStatusCancelled"), 		"Booking list report - Search criteria label verification - Booking status - Cancelled");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusModified"), 			report.get("bookingStatusModified"), 		"Booking list report - Search criteria label verification - Booking status - Modified");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusWithFailedSector"), 	report.get("bookingStatusWithFailedSector"),"Booking list report - Search criteria label verification - Booking status - With failed sector");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDateFilter"), 						report.get("dateFilter"), 					"Booking list report - Search criteria label verification - Date filter");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaReportFrom"), 						report.get("reportFrom"), 					"Booking list report - Search criteria label verification - Report from");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaReportTo"), 							report.get("reportTo"), 					"Booking list report - Search criteria label verification - Report to");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOn"), 						report.get("datesBasedOn"), 				"Booking list report - Search criteria label verification - Dates based on");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnReservation"), 			report.get("datesBasedOnReservation"), 		"Booking list report - Search criteria label verification - Dates based on - Reservation");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnCancellation"), 			report.get("datesBasedOnCancellation"), 	"Booking list report - Search criteria label verification - Dates based on - candellatiom");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnArrival"), 				report.get("datesBasedOnArrival"), 			"Booking list report - Search criteria label verification - Dates based on - Arrival");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnDeparture"), 			report.get("datesBasedOnDeparture"), 		"Booking list report - Search criteria label verification - Dates based on - Depature");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> labels)
	{
//		printTemplate.setTableHeading("Booking list report - Label availability");
		try {
			printTemplate.verifyTrue(true, labels.get("number"), 				"Booking list report - Label availability - Number");
			printTemplate.verifyTrue(true, labels.get("reservations"), 			"Booking list report - Label availability - Reservations");
			printTemplate.verifyTrue(true, labels.get("customer"), 				"Booking list report - Label availability - Customer");
			printTemplate.verifyTrue(true, labels.get("leadGuest"), 			"Booking list report - Label availability - Lead guest");
			printTemplate.verifyTrue(true, labels.get("dateOf"), 				"Booking list report - Label availability - Date of");
			printTemplate.verifyTrue(true, labels.get("date"), 					"Booking list report - Label availability - Date");
			printTemplate.verifyTrue(true, labels.get("time"), 					"Booking list report - Label availability - Time");
			printTemplate.verifyTrue(true, labels.get("reservedBy"), 			"Booking list report - Label availability - Reserved by");
			printTemplate.verifyTrue(true, labels.get("paymentType"), 			"Booking list report - Label availability - Payment type");
			printTemplate.verifyTrue(true, labels.get("profitMarkUpType"), 		"Booking list report - Label availability - Profit mark up type");
			printTemplate.verifyTrue(true, labels.get("customerName"), 			"Booking list report - Label availability - Customer name");
			printTemplate.verifyTrue(true, labels.get("leadGuestName"), 		"Booking list report - Label availability - Lead guest name");
			printTemplate.verifyTrue(true, labels.get("firstElement"), 			"Booking list report - Label availability - First element");
			printTemplate.verifyTrue(true, labels.get("cancellationDeadline"), 	"Booking list report - Label availability - Cancellation deadline");
			printTemplate.verifyTrue(true, labels.get("cityCountryName"), 		"Booking list report - Label availability - City country name");
			printTemplate.verifyTrue(true, labels.get("product"), 				"Booking list report - Label availability - Product");
			printTemplate.verifyTrue(true, labels.get("verified"), 				"Booking list report - Label availability - Verified");
			printTemplate.verifyTrue(true, labels.get("status"), 				"Booking list report - Label availability - Status");
			printTemplate.verifyTrue(true, labels.get("notifications"), 		"Booking list report - Label availability - Notifications");
			printTemplate.verifyTrue(true, labels.get("edit"), 					"Booking list report - Label availability - Edit");
			printTemplate.verifyTrue(true, labels.get("shoppingCartCode"), 		"Booking list report - Label availability - Shpping cart code");
			printTemplate.verifyTrue(true, labels.get("shoppingCart"), 			"Booking list report - Label availability - Shopping cart");
			printTemplate.verifyTrue(true, labels.get("dynamicPackageCode"), 	"Booking list report - Label availability - Dynamic package code");
			printTemplate.verifyTrue(true, labels.get("dynamicPackage"), 		"Booking list report - Label availability - Dynamic package");
			printTemplate.verifyTrue(true, labels.get("fixedPackageCode"), 		"Booking list report - Label availability - Fixed package code");
			printTemplate.verifyTrue(true, labels.get("fixedPackages"), 		"Booking list report - Label availability - Fixed package");
			printTemplate.verifyTrue(true, labels.get("dmcCode"), 				"Booking list report - Label availability - Dmc code");
			printTemplate.verifyTrue(true, labels.get("dmc"), 					"Booking list report - Label availability - DMc");
			printTemplate.verifyTrue(true, labels.get("invoice"), 				"Booking list report - Label availability - Invoice");
			printTemplate.verifyTrue(true, labels.get("invoiceIssued"), 		"Booking list report - Label availability - Invoice issued");
			printTemplate.verifyTrue(true, labels.get("payment"), 				"Booking list report - Label availability - Payment");
			printTemplate.verifyTrue(true, labels.get("paymentReceived"), 		"Booking list report - Label availability - Payment received");
			printTemplate.verifyTrue(true, labels.get("voucher"), 				"Booking list report - Label availability - Voucher");
			printTemplate.verifyTrue(true, labels.get("voucherIssued"), 		"Booking list report - Label availability - Voucher issued");
			printTemplate.verifyTrue(true, labels.get("ticket"), 				"Booking list report - Label availability - Ticket");
			printTemplate.verifyTrue(true, labels.get("ticketIssued"), 			"Booking list report - Label availability - Ticket issued");
			printTemplate.verifyTrue(true, labels.get("emd"), 					"Booking list report - Label availability - EMD");
			printTemplate.verifyTrue(true, labels.get("emdIssued"), 			"Booking list report - Label availability - EMD issued");
			printTemplate.verifyTrue(true, labels.get("failedSectorImage"), 	"Booking list report - Label availability - Failed sector Image");
			printTemplate.verifyTrue(true, labels.get("failedSector"), 			"Booking list report - Label availability - Failed sector");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListLabelVerification(ReportTemplate printTemplate, HashMap<String, String> labels, HashMap<String, String> labelProperty)
	{
//		printTemplate.setTableHeading("Booking list report - Label verification");
		try {
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservationNumber"), 		labels.get("number"), 					"Booking list report - Label verification - Reservation number");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservationDate"), 		labels.get("date"), 					"Booking list report - Label verification - Reservation date");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservationTime"), 		labels.get("time"), 					"Booking list report - Label verification - Reservation time");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservedBy"), 				labels.get("reservedBy"), 				"Booking list report - Label verification - Reserved by");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportPaymentType"), 			labels.get("paymentType"), 				"Booking list report - Label verification - Payment type");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportPMType"), 					labels.get("profitMarkUpType"), 		"Booking list report - Label verification - Profit mark up type");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCustomerName"), 			labels.get("customerName"), 			"Booking list report - Label verification - Customer name");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportLeadGuestName"), 			labels.get("leadGuestName"), 			"Booking list report - Label verification - Lead guest name");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportFirstElement"), 			labels.get("firstElement"), 			"Booking list report - Label verification - First element");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCancellationDeadline"), 	labels.get("cancellationDeadline"), 	"Booking list report - Label verification - Cancellation deadline");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCityCountry"), 			labels.get("cityCountryName"), 			"Booking list report - Label verification - City/Country name");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportProduct"), 				labels.get("product"), 					"Booking list report - Label verification - Product");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportVerified"), 				labels.get("verified"), 				"Booking list report - Label verification - Verified");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportStatus"), 					labels.get("status"), 					"Booking list report - Label verification - Status");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportNotifications"), 			labels.get("notifications"), 			"Booking list report - Label verification - Notifications");
//			printTemplate.verifyTrue(labelProperty.get("bookingListReportview"), 					labels.get(""), 						"Booking list report - Label verification - ");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportEdit"), 					labels.get("edit"), 					"Booking list report - Label verification - Edit");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservation"), 			labels.get("reservations"), 			"Booking list report - Label verification - Reservations");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCustomer"), 				labels.get("customer"), 				"Booking list report - Label verification - Customer");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportLeadGuest"), 				labels.get("leadGuest"), 				"Booking list report - Label verification - Lead guest");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDateOf"), 					labels.get("dateOf"), 					"Booking list report - Label verification - Date of");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportSC"), 						labels.get("shoppingCartCode"), 		"Booking list report - Label verification - Shopping cart code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportShoppingCart"), 			labels.get("shoppingCart"), 			"Booking list report - Label verification - Shopping cart");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDP"), 						labels.get("dynamicPackageCode"), 		"Booking list report - Label verification - Dynamic package code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDynamicPackage"), 			labels.get("dynamicPackage"), 			"Booking list report - Label verification - Dynamic package");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportFP"), 						labels.get("fixedPackageCode"), 		"Booking list report - Label verification - Fixed package code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportFixedPackage"), 			labels.get("fixedPackages"), 			"Booking list report - Label verification - Fixed packages");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDMCP"), 					labels.get("dmcCode"), 					"Booking list report - Label verification - DMC code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDMCPackage"), 				labels.get("dmc"), 						"Booking list report - Label verification - DMC");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportInvoiceIssued"), 			labels.get("invoiceIssued"), 			"Booking list report - Label verification - Invoice issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportPaymentRecieved"), 		labels.get("paymentReceived"), 			"Booking list report - Label verification - Payment received");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportVoucherIssued"), 			labels.get("voucherIssued"), 			"Booking list report - Label verification - Voucher issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportTicketIssued"), 			labels.get("ticketIssued"), 			"Booking list report - Label verification - Ticket issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportEMDIssued"), 				labels.get("emdIssued"), 				"Booking list report - Label verification - EMD issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportSectorFailed"), 			labels.get("failedSector"), 			"Booking list report - Label verification - Failed sector");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListValueVarification()
	{
		
	}
	
	public void bookingListLiveLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> availability)
	{
//		printTemplate.setTableHeading("Booking list report - Live booking more information - label availability");
		try {
			printTemplate.verifyTrue(true, availability.get("country"),	 			"Booking list report - Live booking more information - label availability - Country");
			printTemplate.verifyTrue(true, availability.get("city"), 				"Booking list report - Live booking more information - label availability - City");
			printTemplate.verifyTrue(true, availability.get("supplier"), 			"Booking list report - Live booking more information - label availability - Supplier");
			printTemplate.verifyTrue(true, availability.get("hotelName"), 			"Booking list report - Live booking more information - label availability - Hotel name");
			printTemplate.verifyTrue(true, availability.get("arrivalDeparture"), 	"Booking list report - Live booking more information - label availability - Arrival/Departure dates");
			printTemplate.verifyTrue(true, availability.get("totalRooms"), 			"Booking list report - Live booking more information - label availability - Total rooms");
			printTemplate.verifyTrue(true, availability.get("status"), 				"Booking list report - Live booking more information - label availability - Status");
			printTemplate.verifyTrue(true, availability.get("sectorBookingValue"), 	"Booking list report - Live booking more information - label availability - Sector booking value");
			printTemplate.verifyTrue(true, availability.get("shoppingCartCode"), 	"Booking list report - Live booking more information - label availability - Shopping cart code");
			printTemplate.verifyTrue(true, availability.get("shoppingCart"), 		"Booking list report - Live booking more information - label availability - Shopping cart");
			printTemplate.verifyTrue(true, availability.get("dynamicPackageCode"), 	"Booking list report - Live booking more information - label availability - Dynamic package code");
			printTemplate.verifyTrue(true, availability.get("dynamicPackage"), 		"Booking list report - Live booking more information - label availability - Dynamic package");
			printTemplate.verifyTrue(true, availability.get("fpCode"), 				"Booking list report - Live booking more information - label availability - Fixed package code");
			printTemplate.verifyTrue(true, availability.get("fp"), 					"Booking list report - Live booking more information - label availability - Fixed package");
			printTemplate.verifyTrue(true, availability.get("dmcCode"), 			"Booking list report - Live booking more information - label availability - DMC code");
			printTemplate.verifyTrue(true, availability.get("dmc"), 				"Booking list report - Live booking more information - label availability - DMc");
			printTemplate.verifyTrue(true, availability.get("invoiceIssued"), 		"Booking list report - Live booking more information - label availability - Invoice issued");
			printTemplate.verifyTrue(true, availability.get("paymentReceived"), 	"Booking list report - Live booking more information - label availability - Payment received");
			printTemplate.verifyTrue(true, availability.get("voucherIssued"), 		"Booking list report - Live booking more information - label availability - Voucher issued");
			printTemplate.verifyTrue(true, availability.get("ticketIssued"), 		"Booking list report - Live booking more information - label availability - Ticket issued");
			printTemplate.verifyTrue(true, availability.get("emdIssued"), 			"Booking list report - Live booking more information - label availability - EMD issued");
			printTemplate.verifyTrue(true, availability.get("sectorFailed"), 		"Booking list report - Live booking more information - label availability - Sector failed");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue(true, false, "Booking list report values verification error - " + e);
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListLiveLabelVerification(ReportTemplate printTemplate, HashMap<String, String> values, HashMap<String, String> labelMap, HotelSearchObject search)
	{
//		printTemplate.setTableHeading("Booking list report - Live booking more information - label verification");
		try {
			printTemplate.verifyTrue(labelMap.get("resReportLiveCountry"), 				values.get("country"), 				"Booking list report - Live booking more information - label verification - Country");
			printTemplate.verifyTrue(labelMap.get("resReportLiveCity"), 				values.get("city"), 				"Booking list report - Live booking more information - label verification - City");
			printTemplate.verifyTrue(labelMap.get("resReportLiveSupplier"), 			values.get("supplier"), 			"Booking list report - Live booking more information - label verification - Supplier");
			printTemplate.verifyTrue(labelMap.get("resReportLiveHotelName"), 			values.get("hotelName"), 			"Booking list report - Live booking more information - label verification - Hotel name");
			printTemplate.verifyTrue(labelMap.get("resReportLiveArrivalDeparture"), 	values.get("arrivalDeparture"), 	"Booking list report - Live booking more information - label verification - Arrival/Departure date");
			printTemplate.verifyTrue(labelMap.get("resReportLiveTotalRooms"), 			values.get("totalRooms"), 			"Booking list report - Live booking more information - label verification - Total rooms");
			printTemplate.verifyTrue(labelMap.get("resReportLiveStatus"), 				values.get("status"), 				"Booking list report - Live booking more information - label verification - Status");
			String currency = "";
			if(search.getChannel().equals("web"))
			{
				currency = "LKR";
			}
			else
			{
				currency = "USD";
			}
			String sectorBookingValue = labelMap.get("resReportLiveSectorBookingValue") + "(" + currency + ")";
			printTemplate.verifyTrue(sectorBookingValue, 								values.get("sectorBookingValue"), 	"Booking list report - Live booking more information - label verification - Sector booking value");
			printTemplate.verifyTrue(labelMap.get("resReportLiveShoppingCartCode"), 	values.get("shoppingCartCode"), 	"Booking list report - Live booking more information - label verification - Shopping cart code");
			printTemplate.verifyTrue(labelMap.get("resReportLiveShoppingCart"), 		values.get("shoppingCart"), 		"Booking list report - Live booking more information - label verification - Shopping cart");
			printTemplate.verifyTrue(labelMap.get("resReportLiveDynamicPackageCode"), 	values.get("dynamicPackageCode"), 	"Booking list report - Live booking more information - label verification - Dynamic package code");
			//everything else
			printTemplate.verifyTrue(labelMap.get("resReportLiveDynamicPackage"), 		values.get("dynamicPackage"), 		"Booking list report - Live booking more information - label verification - Dynamic package");
			printTemplate.verifyTrue(labelMap.get("resReportLiveFixedPackageCode"), 	values.get("fpCode"), 				"Booking list report - Live booking more information - label verification - FP code");
			printTemplate.verifyTrue(labelMap.get("resReportLiveFixedPackage"), 		values.get("fp"), 					"Booking list report - Live booking more information - label verification - FP");
			printTemplate.verifyTrue(labelMap.get("resReportLiveDMCCode"), 				values.get("dmcCode"), 				"Booking list report - Live booking more information - label verification - DMC code");
			printTemplate.verifyTrue(labelMap.get("resReportLiveDMC"), 					values.get("dmc"), 					"Booking list report - Live booking more information - label verification - DMC");
			printTemplate.verifyTrue(labelMap.get("resReportLiveInvoiceIssued"), 		values.get("invoiceIssued"), 		"Booking list report - Live booking more information - label verification - Invoice issued");
			printTemplate.verifyTrue(labelMap.get("resReportLivePaymentReceived"), 		values.get("paymentReceived"), 		"Booking list report - Live booking more information - label verification - Payment recieved");
			printTemplate.verifyTrue(labelMap.get("resReportLiveVoucherIssued"), 		values.get("voucherIssued"), 		"Booking list report - Live booking more information - label verification - Voucher issued");
			printTemplate.verifyTrue(labelMap.get("resReportLiveTicketIssued"), 		values.get("ticketIssued"), 		"Booking list report - Live booking more information - label verification - Ticket issued");
			printTemplate.verifyTrue(labelMap.get("resReportLiveEMDIssued"), 			values.get("emdIssued"), 			"Booking list report - Live booking more information - label verification - EMD issued");
			printTemplate.verifyTrue(labelMap.get("resReportLiveSectorFailed"), 		values.get("sectorFailed"), 		"Booking list report - Live booking more information - label verification - Sector failed");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListLiveValueVerification(ReportTemplate printTemplate, HashMap<String, String> reportValues, hotelDetails res, HotelSearchObject search, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> labels, HashMap<String, String> portalSpecificData)
	{
//		printTemplate.setTableHeading("Booking list report - Live booking more information - value verification");
		try {
			String countryHelper = search.getDestination().replace("|", "/");
			String[] country = countryHelper.split("/");
			printTemplate.verifyTrue(country[5],									reportValues.get("country"), 			"Booking list report - Live booking more information - value verification - Country");
			printTemplate.verifyTrue(res.getCity(), 								reportValues.get("city"), 				"Booking list report - Live booking more information - value verification - City");
			String supplier = "";
			String checkin = "";
			String checkout = "";
			if(search.getSupplier().equals("INV27"))
			{
				checkin = Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", res.getCheckin());
				checkout = Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", res.getCheckout());
				supplier = "GTA_Hotels";
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				checkin = Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", res.getCheckin());
				checkout = Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", res.getCheckout());
				supplier = "HotelBed_Hotels";
			}
			else
			{
				checkout = res.getCheckout();
				checkin = res.getCheckin();
			}
			printTemplate.verifyTrue(supplier, 										reportValues.get("supplier"), 			"Booking list report - Live booking more information - value verification - Supplier");
			printTemplate.verifyTrue(res.getHotel(), 								reportValues.get("hotelName"), 			"Booking list report - Live booking more information - value verification - Hotel name");
			printTemplate.verifyTrue(checkin + "/ " + checkout , 					reportValues.get("ariivalDeparture"), 	"Booking list report - Live booking more information - value verification - Arrival and departure date");
			printTemplate.verifyTrue(search.getRooms(), 							reportValues.get("totalRooms"), 		"Booking list report - Live booking more information - value verification - Total rooms");
			String status = "";
			if(res.getAvailabilityStatus().equals("CONFIRMED") || res.getAvailabilityStatus().equals("Confirmed"))
			{
				status = "CON";
			}
			else
			{
				status = res.getAvailabilityStatus();
			}
			printTemplate.verifyTrue(status, 					reportValues.get("status"), 			"Booking list report - Live booking more information - value verification - Status");
			String[]			reportCCHelper	= labels.get("sectorBookingValue").split(" ");
			String 				reportCC 		= reportCCHelper[2].replace("(", "").replace(")", "").replace("Value", "").trim();
			String 				supValue 		= converter.convert(defaultCC, res.getPrice(), reportCC, currencyMap, res.getCurrencyCode());
			String   			pm				= tax.getTax(supValue, portalSpecificData.get("hotelpm"), defaultCC, reportCC, currencyMap);
			String   			bf				= tax.getTax(supValue, portalSpecificData.get("hotelbf"), defaultCC, reportCC, currencyMap);
			String   			ccFee			= tax.getTax(supValue, portalSpecificData.get("portal.ccfee"), defaultCC, reportCC, currencyMap, portalSpecificData.get("portal.pgCurrency"));
			
		
			printTemplate.verifyTrue(String.valueOf(Integer.parseInt(supValue) + Integer.parseInt(pm)), 	reportValues.get("bookingValue"), 		"Booking list report - Live booking more information - value verification - Booking value");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("Error", String.valueOf(e), "Booking list report - Live booking more info - values verification error - ");
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListBookingCardLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> availability)
	{
//		printTemplate.setTableHeading("booking list - Booking card - Label availability");
		try {
			printTemplate.verifyTrue(true, availability.get("reservationSummary"), 					"booking list - Booking card - Label availability - Reservation summary");
			printTemplate.verifyTrue(true, availability.get("reservationNumber"), 					"booking list - Booking card - Label availability - Reservation number");
			printTemplate.verifyTrue(true, availability.get("originCityCountry"), 					"booking list - Booking card - Label availability - Origin city / country");
			printTemplate.verifyTrue(true, availability.get("destinationCityCountry"), 				"booking list - Booking card - Label availability - Destination city / country");
			printTemplate.verifyTrue(true, availability.get("productsBooked"), 						"booking list - Booking card - Label availability - Products booked");
			printTemplate.verifyTrue(true, availability.get("bookingStatus"), 						"booking list - Booking card - Label availability - Booking status");
			printTemplate.verifyTrue(true, availability.get("failedSectorIncluded"), 				"booking list - Booking card - Label availability - Failed sector included");
			printTemplate.verifyTrue(true, availability.get("paymentLabel"), 						"booking list - Booking card - Label availability - Payment label");
			printTemplate.verifyTrue(true, availability.get("paymentReference"), 					"booking list - Booking card - Label availability - Payment reference");
			printTemplate.verifyTrue(true, availability.get("reservationDate"), 					"booking list - Booking card - Label availability - Reservation date");
			printTemplate.verifyTrue(true, availability.get("firstElement"), 						"booking list - Booking card - Label availability - First element");
			printTemplate.verifyTrue(true, availability.get("cancellationDeadline"), 				"booking list - Booking card - Label availability - Cancellation deadline");
			printTemplate.verifyTrue(true, availability.get("amountChergedByPG"), 					"booking list - Booking card - Label availability - Amount charged by payment gateway");
			printTemplate.verifyTrue(true, availability.get("subTotal"), 							"booking list - Booking card - Label availability - Sub total");
			printTemplate.verifyTrue(true, availability.get("bookingFee"), 							"booking list - Booking card - Label availability - Booking fee");
			printTemplate.verifyTrue(true, availability.get("taxAndOtherCharges"), 					"booking list - Booking card - Label availability - Tax and other charges");
			printTemplate.verifyTrue(true, availability.get("ccFee"), 								"booking list - Booking card - Label availability - Credit card fee");
			printTemplate.verifyTrue(true, availability.get("totalBookingValue"), 					"booking list - Booking card - Label availability - Total booking value");
			printTemplate.verifyTrue(true, availability.get("amountPayableUpfront"), 				"booking list - Booking card - Label availability - Amount payable upfront");
			printTemplate.verifyTrue(true, availability.get("totalAmountPAyableAmountAtCheckin"), 	"booking list - Booking card - Label availability - Total amount payable at chekin");
			printTemplate.verifyTrue(true, availability.get("amountPaid"), 							"booking list - Booking card - Label availability - Amount paid");
			printTemplate.verifyTrue(true, availability.get("customerLeadGuestDetails"), 			"booking list - Booking card - Label availability - Customer lead guest details");
			printTemplate.verifyTrue(true, availability.get("firstName"), 							"booking list - Booking card - Label availability - First name");
			printTemplate.verifyTrue(true, availability.get("address"), 							"booking list - Booking card - Label availability - Address");
			printTemplate.verifyTrue(true, availability.get("emailAddress"), 						"booking list - Booking card - Label availability - Email");
			printTemplate.verifyTrue(true, availability.get("city"), 								"booking list - Booking card - Label availability - City");
			printTemplate.verifyTrue(true, availability.get("countryLabel"), 						"booking list - Booking card - Label availability - Country");
			printTemplate.verifyTrue(true, availability.get("emergencyContactNumber"), 				"booking list - Booking card - Label availability - Emergency contact number");
			printTemplate.verifyTrue(true, availability.get("phoneNumber"), 						"booking list - Booking card - Label availability - Phone number");
			printTemplate.verifyTrue(true, availability.get("hotelDetails"), 						"booking list - Booking card - Label availability - Hotel details");
			printTemplate.verifyTrue(true, availability.get("hotelName"), 							"booking list - Booking card - Label availability - Hotel name");
			printTemplate.verifyTrue(true, availability.get("hotelAddress"), 						"booking list - Booking card - Label availability - Hotel address");
			printTemplate.verifyTrue(true, availability.get("checkOut"), 							"booking list - Booking card - Label availability - Check out");
			printTemplate.verifyTrue(true, availability.get("supplierName"), 						"booking list - Booking card - Label availability - Supplier name");
			printTemplate.verifyTrue(true, availability.get("confirmationNumber"), 					"booking list - Booking card - Label availability - Confirmation number");
			printTemplate.verifyTrue(true, availability.get("starCategory"), 						"booking list - Booking card - Label availability - Star category");
			printTemplate.verifyTrue(true, availability.get("bookingStatus"), 						"booking list - Booking card - Label availability - Booking status");
			printTemplate.verifyTrue(true, availability.get("checkinDate"), 						"booking list - Booking card - Label availability - Checkin date");
			printTemplate.verifyTrue(true, availability.get("numberOfNights"), 						"booking list - Booking card - Label availability - Number of nights");
			printTemplate.verifyTrue(true, availability.get("numberOfRooms"), 						"booking list - Booking card - Label availability - Number of rooms");
			printTemplate.verifyTrue(true, availability.get("hotelContractType"), 					"booking list - Booking card - Label availability - Hotel contract type");
			printTemplate.verifyTrue(true, availability.get("roomBedRate"), 						"booking list - Booking card - Label availability - Room / Bed / Rate");
			printTemplate.verifyTrue(true, availability.get("occupantsNames"), 						"booking list - Booking card - Label availability - Occupants name");
			printTemplate.verifyTrue(true, availability.get("ageGroup"), 							"booking list - Booking card - Label availability - Age group");
			printTemplate.verifyTrue(true, availability.get("childAge"), 							"booking list - Booking card - Label availability - Child age");
			printTemplate.verifyTrue(true, availability.get("totalRate"), 							"booking list - Booking card - Label availability - Total rate");
			printTemplate.verifyTrue(true, availability.get("hotelSupplierNotes"), 					"booking list - Booking card - Label availability - Hotel supplier notes");
			printTemplate.verifyTrue(true, availability.get("internalNotes"), 						"booking list - Booking card - Label availability - Internal notes");
			printTemplate.verifyTrue(true, availability.get("customerNotes"), 						"booking list - Booking card - Label availability - Customer notes");
			printTemplate.verifyTrue(true, availability.get("fSubtotal"), 							"booking list - Booking card - Label availability - Hotel sub total");
			printTemplate.verifyTrue(true, availability.get("fTaxAndOtherCharges"), 				"booking list - Booking card - Label availability - Hotel tax and other charges");
			printTemplate.verifyTrue(true, availability.get("fTotalBookingValue"), 					"booking list - Booking card - Label availability - Hotel total booking value");
			printTemplate.verifyTrue(true, availability.get("fAmountChargebleUpfront"), 			"booking list - Booking card - Label availability - Hotel amount chargeble upfront");
			printTemplate.verifyTrue(true, availability.get("amountDueAtChekin"), 					"booking list - Booking card - Label availability - Hotel amount due at checkin");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListBookingCardLabelVarify(ReportTemplate printTemplate, HashMap<String, String> property, HashMap<String, String> labels, HotelSearchObject search)
	{
//		printTemplate.setTableHeading("booking list - Booking card - Label verification");
		try {
			printTemplate.verifyTrue(property.get("reservationSummary"), 			labels.get("reservationSummary"), 					"booking list - Booking card - Label verification - Reservation summary");
			printTemplate.verifyTrue(property.get("resNumber"), 					labels.get("reservationNumber"), 					"booking list - Booking card - Label verification - Reservation number");
			printTemplate.verifyTrue(property.get("originCity"), 					labels.get("originCityCountry"), 					"booking list - Booking card - Label verification - Origin city / country");
			printTemplate.verifyTrue(property.get("destinationCity"), 				labels.get("destinationCityCountry"), 				"booking list - Booking card - Label verification - Destination city / country");
			printTemplate.verifyTrue(property.get("productsBooked"), 				labels.get("productsBooked"), 						"booking list - Booking card - Label verification - Products booked");
			printTemplate.verifyTrue(property.get("bookingStatus"), 				labels.get("bookingStatus"), 						"booking list - Booking card - Label verification - Booking status");
			printTemplate.verifyTrue(property.get("FailedSectorIncluded"), 			labels.get("failedSectorIncluded"), 				"booking list - Booking card - Label verification - Failed sector included");
			printTemplate.verifyTrue(property.get("paymentMethod"), 				labels.get("paymentLabel"), 						"booking list - Booking card - Label verification - Payment label");
			printTemplate.verifyTrue(property.get("paymentReference"), 				labels.get("paymentReference"), 					"booking list - Booking card - Label verification - Payment reference");
			printTemplate.verifyTrue(property.get("reservationDate"), 				labels.get("reservationDate"), 						"booking list - Booking card - Label verification - Reservation date");
			printTemplate.verifyTrue(property.get("firstElement"), 					labels.get("firstElement"), 						"booking list - Booking card - Label verification - First element");
			printTemplate.verifyTrue(property.get("cancellationDeadline"), 			labels.get("cancellationDeadline"), 				"booking list - Booking card - Label verification - Cancellation deadline");
			printTemplate.verifyTrue(property.get("amountChargedByPG"), 			labels.get("amountChergedByPG"), 					"booking list - Booking card - Label verification - Amount charged by payment gateway");
			printTemplate.verifyTrue(property.get("subtotal"), 						labels.get("subTotal"), 							"booking list - Booking card - Label verification - Sub total");
			printTemplate.verifyTrue(property.get("bookingFee"), 					labels.get("bookingFee"), 							"booking list - Booking card - Label verification - Booking fee");
			printTemplate.verifyTrue(property.get("taxAndOtherCharges"), 			labels.get("taxAndOtherCharges"), 					"booking list - Booking card - Label verification - Tax and other charges");
			printTemplate.verifyTrue(property.get("ccFee"), 						labels.get("ccFee"), 								"booking list - Booking card - Label verification - Credit card fee");
			printTemplate.verifyTrue(property.get("totalBookingValue"), 			labels.get("totalBookingValue"), 					"booking list - Booking card - Label verification - Total booking value");
			printTemplate.verifyTrue(property.get("totalAmountPayableUpfront"), 	labels.get("amountPayableUpfront"), 				"booking list - Booking card - Label verification - Amount payable upfront");
			printTemplate.verifyTrue(property.get("totalPayableAtCheckin"), 		labels.get("totalAmountPAyableAmountAtCheckin"), 	"booking list - Booking card - Label verification - Total amount payable at chekin");
			printTemplate.verifyTrue(property.get("amountPaid"), 					labels.get("amountPaid"), 							"booking list - Booking card - Label verification - Amount paid");
			printTemplate.verifyTrue(property.get("customerDetails"), 				labels.get("customerLeadGuestDetails"), 			"booking list - Booking card - Label verification - Customer lead guest details");
			printTemplate.verifyTrue(property.get("firstName"), 					labels.get("firstName"), 							"booking list - Booking card - Label verification - First name");
			printTemplate.verifyTrue(property.get("address"), 						labels.get("address"), 								"booking list - Booking card - Label verification - Address");
			printTemplate.verifyTrue(property.get("email"), 						labels.get("emailAddress"), 						"booking list - Booking card - Label verification - Email");
			printTemplate.verifyTrue(property.get("city"), 							labels.get("city"), 								"booking list - Booking card - Label verification - City");
			printTemplate.verifyTrue(property.get("country"), 						labels.get("countryLabel"), 						"booking list - Booking card - Label verification - Country");
			printTemplate.verifyTrue(property.get("emergencyContact"), 				labels.get("emergencyContactNumber"), 				"booking list - Booking card - Label verification - Emergency contact number");
			printTemplate.verifyTrue(property.get("phoneNumber"), 					labels.get("phoneNumber"), 							"booking list - Booking card - Label verification - Phone number");
			printTemplate.verifyTrue(property.get("hotelDetails"), 					labels.get("hotelDetails"), 						"booking list - Booking card - Label verification - Hotel details");
			printTemplate.verifyTrue(property.get("hotelName"), 					labels.get("hotelName"), 							"booking list - Booking card - Label verification - Hotel name");
			printTemplate.verifyTrue(property.get("hotelAddress"), 					labels.get("hotelAddress"), 						"booking list - Booking card - Label verification - Hotel address");
			printTemplate.verifyTrue(property.get("checkOutDate"), 					labels.get("checkOut"), 							"booking list - Booking card - Label verification - Check out");
			printTemplate.verifyTrue(property.get("supplierName"), 					labels.get("supplierName"), 						"booking list - Booking card - Label verification - Supplier name");
			printTemplate.verifyTrue(property.get("hotelConNumber"), 				labels.get("confirmationNumber"), 					"booking list - Booking card - Label verification - Confirmation number");
			printTemplate.verifyTrue(property.get("starCategory"), 					labels.get("starCategory"), 						"booking list - Booking card - Label verification - Star category");
			printTemplate.verifyTrue(property.get("hotelBookingStatus"), 			labels.get("bookingStatus"), 						"booking list - Booking card - Label verification - Booking status");
			printTemplate.verifyTrue(property.get("checkinDate"), 					labels.get("checkinDate"), 							"booking list - Booking card - Label verification - Checkin date");
			printTemplate.verifyTrue(property.get("numberOfNights"), 				labels.get("numberOfNights"), 						"booking list - Booking card - Label verification - Number of nights");
			printTemplate.verifyTrue(property.get("numberOfRooms"), 				labels.get("numberOfRooms"), 						"booking list - Booking card - Label verification - Number of rooms");
			printTemplate.verifyTrue(property.get("hotelContractType"), 			labels.get("hotelContractType"), 					"booking list - Booking card - Label verification - Hotel contract type");
			printTemplate.verifyTrue(property.get("roomBedRate"), 					labels.get("roomBedRate"), 							"booking list - Booking card - Label verification - Room / Bed / Rate");
			printTemplate.verifyTrue(property.get("occupantsName"), 				labels.get("occupantsNames"), 						"booking list - Booking card - Label verification - Occupants name");
			printTemplate.verifyTrue(property.get("ageGroup"), 						labels.get("ageGroup"), 							"booking list - Booking card - Label verification - Age group");
			printTemplate.verifyTrue(property.get("childAge"), 						labels.get("childAge"), 							"booking list - Booking card - Label verification - Child age");
			String currency = "";
			if(search.getChannel().equals("web"))
			{
				currency = "LKR";
			}
			else
			{
				currency = "USD";
			}
			printTemplate.verifyTrue(property.get("totalRate") + " (" + currency + ")", labels.get("totalRate"), 							"booking list - Booking card - Label verification - Total rate");
			printTemplate.verifyTrue(property.get("hotelNotes"), 						labels.get("hotelSupplierNotes"), 					"booking list - Booking card - Label verification - Hotel supplier notes");
			printTemplate.verifyTrue(property.get("internalNotes"), 					labels.get("internalNotes"), 						"booking list - Booking card - Label verification - Internal notes");
			printTemplate.verifyTrue(property.get("customerNotes"), 					labels.get("customerNotes"), 						"booking list - Booking card - Label verification - Customer notes");
			printTemplate.verifyTrue(property.get("hotelSubTotal"), 					labels.get("fSubtotal"), 							"booking list - Booking card - Label verification - Hotel sub total");
			printTemplate.verifyTrue(property.get("hotelTaxes"), 						labels.get("fTaxAndOtherCharges"), 					"booking list - Booking card - Label verification - Hotel tax and other charges");
			printTemplate.verifyTrue(property.get("hotelTotalBookingValue"), 			labels.get("fTotalBookingValue"), 					"booking list - Booking card - Label verification - Hotel total booking value");
			printTemplate.verifyTrue(property.get("amountChargebleUpfront"), 			labels.get("fAmountChargebleUpfront"), 				"booking list - Booking card - Label verification - Hotel amount chargeble upfront");
			printTemplate.verifyTrue(property.get("amountDueAtCheckin"), 				labels.get("amountDueAtChekin"), 					"booking list - Booking card - Label verification - Hotel amount due at checkin");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListBookingCardValueVerify(ReportTemplate printTemplate, HashMap<String, String> bookingcard, HotelSearchObject search, hotelDetails res, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificData, HotelConfirmationPageDetails confirmationPage, HashMap<String, String> paymentDetails, String canDeadline, hotelDetails roomRes, ArrayList<HotelOccupancyObject> occupancyList, String reportDate)
	{
		Calender cal = new Calender();
//		printTemplate.setTableHeading("booking list - Booking card - values verification");
		try {
			printTemplate.verifyTrue(confirmationPage.getReservationNumber(), 		bookingcard.get("reservationNumber"), 			"booking list - Booking card - values verification - Reservation number");
			printTemplate.verifyTrue("N/A", 										bookingcard.get("originCity"), 					"booking list - Booking card - values verification - Origin city");
			printTemplate.verifyTrue("N/A", 										bookingcard.get("destinationCityCountry"), 		"booking list - Booking card - values verification - Destination city and country");
			printTemplate.verifyTrue("H", 											bookingcard.get("productsBooked"), 				"booking list - Booking card - values verification - Products booked");
			String status = "";
			if(res.getAvailabilityStatus().equals("Confirmed") || res.getAvailabilityStatus().equals("CONFIRMED"))
			{
				status = "CON";
			}
			else
			{
				status = res.getAvailabilityStatus();
			}
			printTemplate.verifyTrue(status, 					bookingcard.get("bookingStatus"), 				"booking list - Booking card - values verification - Booking status");
			printTemplate.verifyTrue("No", 											bookingcard.get("failedSectorIncluded"), 		"booking list - Booking card - values verification - Failed sector included");
			String paymentMethod = "";
			if(search.getOnlineOrOffline().equals("online"))
			{
				paymentMethod = "Online (Credit Card)";
			}
			else
			{
				paymentMethod = "offline";
			}
			printTemplate.verifyTrue(paymentMethod, 															bookingcard.get("paymentMethod"), 				"booking list - Booking card - values verification - Payment method");
			printTemplate.verifyTrue(paymentDetails.get("transactionID"), 										bookingcard.get("paymentReference"), 			"booking list - Booking card - values verification - Payment reference");
			printTemplate.verifyTrue(Calender.getDate("dd-MMM-yyyy", "dd/MM/YYYY", reportDate), 				bookingcard.get("reservationDate"),		 "booking list - Booking card - values verification - Reservation date");
			String date = "";
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				date = Calender.getDate("yyyyMMdd", "dd-MM-yyyy", res.getCheckin());
			}
			else if(search.getSupplier().equals("INV27"))
			{
				date = Calender.getDate("yyyy-MM-dd", "dd-MM-yyyy", res.getCheckin());
			}
			printTemplate.verifyTrue(date.replace("-", "/"),			 								bookingcard.get("firstElement"), 				"First element");
			date = Calender.getDate("dd-MMM-yyyy", "dd-MM-yyyy", canDeadline);
			printTemplate.verifyTrue(date.replace("-", "/"), 											bookingcard.get("cancellationDeadline"), 		"Cancellation deadline");
			
			
			String[] currency = bookingcard.get("amountChargedByPG").split(" ");
			String value 	= converter.convert(defaultCC, res.getPrice(), currency[0], currencyMap, res.getCurrencyCode());
			String pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, currency[0], currencyMap);
			String bf 		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, currency[0], currencyMap);
			String ccFee 	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, currency[0], currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String amountChargedAtPG = currency[0].concat(" ").concat(String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee)));
			
			printTemplate.verifyTrue(amountChargedAtPG, 							bookingcard.get("amountChargedByPG"), 			"booking list - Booking card - values verification - Amount charged by payment gateway");
			String reportCurrency = bookingcard.get("reportCurrency").replace("(", "").replace(")", "");
			
			value 	= converter.convert(defaultCC, res.getPrice(), reportCurrency, currencyMap, res.getCurrencyCode());
			pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, currency[0], currencyMap);
			bf 		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, currency[0], currencyMap);
			ccFee 	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, reportCurrency, currencyMap, portalSpecificData.get("portal.pgCurrency"));
			
			
			String total = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf));
			String totalWithCCfee = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee));
			String subtotal = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
			String taxAndOtherCharges = String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccFee));
			String hoteltaxAndOtherCharges = String.valueOf(Integer.parseInt(bf));
			printTemplate.verifyTrue(subtotal, 										bookingcard.get("subtotal"), 					"booking list - Booking card - values verification - Sub total");
			printTemplate.verifyTrue(bf, 											bookingcard.get("bookingFee"), 					"booking list - Booking card - values verification - Booking fee");
			printTemplate.verifyTrue("0", 											bookingcard.get("taxAndOtherCharges"), 			"booking list - Booking card - values verification - Tax and other charges");
			printTemplate.verifyTrue(ccFee,											bookingcard.get("ccFee"), 						"booking list - Booking card - values verification - CC Fee");
			printTemplate.verifyTrue(totalWithCCfee, 								bookingcard.get("bookingValue"), 				"booking list - Booking card - values verification - Booking value");
			printTemplate.verifyTrue(totalWithCCfee, 								bookingcard.get("totalAmountPayable"), 			"booking list - Booking card - values verification - Total amount payable");
			printTemplate.verifyTrue("0",											bookingcard.get("payableAtCheckin"), 			"booking list - Booking card - values verification - Payable at checkin");
			printTemplate.verifyTrue(totalWithCCfee, 								bookingcard.get("amountPaid"), 					"booking list - Booking card - values verification - Amount paid");
			printTemplate.verifyTrue(portalSpecificData.get("cd.firstname"), 		bookingcard.get("firstName"), 					"booking list - Booking card - values verification - Customer first name");
			printTemplate.verifyTrue(portalSpecificData.get("cd.address"), 			bookingcard.get("address"), 					"booking list - Booking card - values verification - Address");
			printTemplate.verifyTrue(portalSpecificData.get("cd.hotelEmail"), 		bookingcard.get("email"), 						"booking list - Booking card - values verification - Email");
			printTemplate.verifyTrue(portalSpecificData.get("cd.city"), 			bookingcard.get("city"), 						"booking list - Booking card - values verification - City");
			printTemplate.verifyTrue(portalSpecificData.get("cd.country"), 			bookingcard.get("country"), 					"booking list - Booking card - values verification - Country");
			String phonenumber = portalSpecificData.get("cd.phoneNumberCode") + "-" + portalSpecificData.get("cd.phoneNumber");
			printTemplate.verifyTrue(phonenumber , 									bookingcard.get("emergencyContactNumber"), 		"booking list - Booking card - values verification - Emergency contact number");
			printTemplate.verifyTrue(phonenumber, 									bookingcard.get("phoneNumber"), 				"booking list - Booking card - values verification - Phone number");
			printTemplate.verifyTrue(res.getHotel(), 								bookingcard.get("hotelName"), 					"booking list - Booking card - values verification - Hotel name");
			printTemplate.verifyTrue(res.getAddress(), 								bookingcard.get("hotelAddress"), 				"booking list - Booking card - values verification - Hotel address");
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				date = Calender.getDate("yyyyMMdd", "dd/MM/yyyy", res.getCheckout());
			}
			else if(search.getSupplier().equals("INV27"))
			{
				date = Calender.getDate("yyyy-MM-dd", "dd/MM/yyyy", res.getCheckout());
			}
			printTemplate.verifyTrue(date, 											bookingcard.get("checkoutDate"), 								"booking list - Booking card - values verification - Checkout date");
			printTemplate.verifyTrue(search.getSupplier(), 							bookingcard.get("supplier"), 					"booking list - Booking card - values verification - Supplier");
			printTemplate.verifyTrue(res.getSupConfirmationNumber(), 				bookingcard.get("hotelConfirmation"), 			"booking list - Booking card - values verification - Supplier confirmation number");
			String starRating = "";
			if(search.getSupplier().equals("INV27"))
			{
				starRating = roomRes.getStarRating() + " Star";
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				String temp[] = roomRes.getCategory().split(" ");
				starRating = temp[0] + " Star";
			}
			else
			{
				starRating = roomRes.getStarRating();
			}
			printTemplate.verifyTrue(starRating, 					bookingcard.get("starCategory"), 				"booking list - Booking card - values verification - Star category");
			status = "";
			if(res.getAvailabilityStatus().equals("Confirmed") || res.getAvailabilityStatus().equals("CONFIRMED"))
			{
				status = "CON";
			}
			else
			{
				status = res.getAvailabilityStatus();
			}
			printTemplate.verifyTrue(status, 					bookingcard.get("bookingStatus"), 				"booking list - Booking card - values verification - Booking status");
			
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				date = Calender.getDate("yyyyMMdd", "dd-MM-yyyy", res.getCheckin());
			}
			else if(search.getSupplier().equals("INV27"))
			{
				date = Calender.getDate("yyyy-MM-dd", "dd-MM-yyyy", res.getCheckin());
			}
			printTemplate.verifyTrue(date.replace("-", "/"), 						bookingcard.get("checkinDate"), 				"booking list - Booking card - values verification - Checkin date");
			printTemplate.verifyTrue(search.getNights(), 							bookingcard.get("numberOfNights"), 				"booking list - Booking card - values verification - Number of nights");
			printTemplate.verifyTrue(search.getRooms(), 							bookingcard.get("numberOfRooms"), 				"booking list - Booking card - values verification - Number of rooms");
			printTemplate.verifyTrue("Net", 										bookingcard.get("hotelContractType"), 			"booking list - Booking card - values verification - Hotel contract type");
			
			String roomBedRate = "";
			
				for(int i = 0 ; i < res.getRoomDetails().size() ; i++)
				{
					String room = "";
					String bed = "";
					String rate = "";
					if(search.getSupplier().equals("INV27"))
					{
						if(res.getRoomDetails().get(i).getDescription().equals(""))
						{
							room = "-";
						}
						else
						{
							room = res.getRoomDetails().get(i).getDescription();
						}
						if(res.getRoomDetails().get(i).getRoomCode().equals(""))
						{
							bed = "-";
						}
						else
						{
							if(res.getRoomDetails().get(i).getRoomCode().equals("DB"))
							{
								bed = "Double";
							}
							else
							{
								bed = res.getRoomDetails().get(i).getRoomCode();
							}
						}
						System.out.println(res.getRoomDetails().get(i).getBoard());
						if(res.getRoomDetails().get(i).getBoard().equals(""))
						{
							rate = "-";
						}
						else
						{
							rate = res.getRoomDetails().get(i).getBoard();
						}
						if(i == 0 )
						{
							roomBedRate = room + "/" + bed + "/" + rate;
						}
						else
						{
							roomBedRate = roomBedRate + "," + room + "/" + bed + "/" + rate;
						}
					}
					else
					{
						if(res.getRoomDetails().get(i).getRoomName().equals(""))
						{
							room = "-";
						}
						else
						{
							room = res.getRoomDetails().get(i).getRoomName();
						}
						if(res.getRoomDetails().get(i).getBedType().equals(""))
						{
							bed = "-";
						}
						else
						{
							bed = res.getRoomDetails().get(i).getBedType();
						}
						if(res.getRoomDetails().get(i).getBoard().equals(""))
						{
							rate = "-";
						}
						else
						{
							rate = res.getRoomDetails().get(i).getBoard();
						}
						if(i == 0 )
						{
							roomBedRate = room + "/" + bed + "/" + rate;
						}
						else
						{
							roomBedRate = roomBedRate + "," + room + "/" + bed + "/" + rate;
						}
					}	
				}

					
				

			printTemplate.verifyTrue(roomBedRate, 									bookingcard.get("roomBedRate"), 				"booking list - Booking card - values verification - Room / Bed / Rate");
			String passengernameE = "";
			String passengerAgeE = "";
			String ageGrp = "";
			int cnt1 = 0;
			int cnt2 = 0;
			String[] adultCnt = search.getAdults().split("/");
			String[] childCnt = search.getChildren().split("/");
			String[] age = search.getAge().split("/");
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				for(int j = 0 ; j < Integer.parseInt(adultCnt[i]) ; j++)
				{
					if(j == 0 && i == 0)
					{
						passengernameE 	= "Mr. " + occupancyList.get(cnt1).getAdultFirstName().concat(" ").concat(occupancyList.get(cnt1).getAdultLastName());
						passengerAgeE 	= " ";
						ageGrp = " ";
					}
					else
					{
						passengernameE 	= passengernameE.concat(",").concat("Mr. ").concat(occupancyList.get(cnt1).getAdultFirstName().concat(" ").concat(occupancyList.get(cnt1).getAdultLastName()));
						passengerAgeE 	= passengerAgeE.concat(",").concat(" ");
						ageGrp = ageGrp.concat(",").concat(" ");
					}
					cnt1++;
				}
				
				for(int j = 0 ; j < Integer.parseInt(childCnt[i]) ; j++)
				{
					passengernameE 		= passengernameE.concat(",").concat("Mst. ").concat(occupancyList.get(cnt2).getChildFirstName().concat(" ").concat(occupancyList.get(cnt2).getChildLastName()));
					passengerAgeE 		= passengerAgeE.concat(", ").concat(age[cnt2]);
					ageGrp = ageGrp.concat(",").concat(" ");
					cnt2++;
				}
			}
			printTemplate.verifyTrue(passengernameE, 								bookingcard.get("occupantName"), 				"booking list - Booking card - values verification - Occupant name");
			printTemplate.verifyTrue(ageGrp, 										bookingcard.get("ageGroup"), 					"booking list - Booking card - values verification - Age group");
			printTemplate.verifyTrue(passengerAgeE, 								bookingcard.get("childAge"), 					"booking list - Booking card - values verification - Child age");
			String roomRate = "";
			for(int i = 0 ; i < res.getRoomDetails().size() ; i++)
			{
				value 	= converter.convert(defaultCC, res.getRoomDetails().get(i).getPrice(), reportCurrency, currencyMap, res.getCurrencyCode());
				pm 		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, reportCurrency, currencyMap);
				if(i == 0)
				{
					roomRate = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
				else
				{
					roomRate = roomRate + "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
			}
			printTemplate.verifyTrue(roomRate, 										bookingcard.get("rate"), 						"booking list - Booking card - values verification - Rate");
			printTemplate.verifyTrue("Hotel/Supplier Notes", 						bookingcard.get("hotelNotes"), 					"booking list - Booking card - values verification - Hotel notes");
			//printTemplate.verifyTrue("", 											bookingcard.get("internalNotes"), 				"Internal notes");
			printTemplate.verifyTrue("Customer Notes", 								bookingcard.get("customerNotes"), 				"booking list - Booking card - values verification - Customer notes");
			printTemplate.verifyTrue(subtotal, 										bookingcard.get("fSubTotal"), 					"booking list - Booking card - values verification - Hotel sub total");
			printTemplate.verifyTrue(hoteltaxAndOtherCharges,						bookingcard.get("fTaxAndOtherChrges"), 			"booking list - Booking card - values verification - Hotel tax and other charges");
			printTemplate.verifyTrue(total, 										bookingcard.get("fTotalBookingValue"), 			"booking list - Booking card - values verification - Hotel total booking value");
			printTemplate.verifyTrue(total, 										bookingcard.get("fAmountChargebleUpFront"), 	"booking list - Booking card - values verification - Hotel amount chargeble upfront");
			printTemplate.verifyTrue("0", 											bookingcard.get("fAmountDueAtCheckin"), 		"booking list - Booking card - values verification - Hotel amount due at checkin");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("", String.valueOf(e), "Booking list report - Booking card - values verification error - ");
			
		}
//		printTemplate.markTableEnd();
	}
	
	public void profitAndLossReportValuesVerification(ReportTemplate printTemplate, HashMap<String, String> values, HotelSearchObject search, HashMap<String, String> portalSpecificData, String defaultCC, HashMap<String, String> currencyMap, hotelDetails reservationRes, HotelConfirmationPageDetails confirmation, String reportDate, ArrayList<HotelOccupancyObject> occupancyList)
	{
//		printTemplate.setTableHeading("Profit and loss report - values verification");
		try {
			printTemplate.verifyTrue(confirmation.getReservationNumber(), 			values.get("reservationNumber"), 				"Profit and loss report - values verification - Reservation number");
			printTemplate.verifyTrue(reportDate, 									values.get("bookingDate"), 						"Profit and loss report - values verification - Booking date");
			printTemplate.verifyTrue("Hotel", 										values.get("combination"), 						"Profit and loss report - values verification - Combination");
			printTemplate.verifyTrue("-", 											values.get("commissionTemplate"), 				"Profit and loss report - values verification - Commission template");
			printTemplate.verifyTrue("Net Rate", 									values.get("contractType"), 					"Profit and loss report - values verification - Contract type");
			printTemplate.verifyTrue("DC", 											values.get("bookingType"), 						"Profit and loss report - values verification - Booking type");
			String agentName = "";
			if(search.getChannel().equals("web"))
			{
				agentName = occupancyList.get(0).getAdultFirstName() + " " + occupancyList.get(0).getAdultLastName();
			}
			else
			{
				agentName = portalSpecificData.get("hotelUsername");
			}
			printTemplate.verifyTrue(agentName, 		values.get("agentName"), 						"Profit and loss report - values verification - Agent name");
			String bookingChannel = "";
			if(search.getChannel().equals("web"))
			{
				bookingChannel = "Website";
			}
			else if(search.getChannel().equals("cc"))
			{
				bookingChannel = "Call Center";
			}
			printTemplate.verifyTrue(bookingChannel, 								values.get("bookingChannel"), 					"Profit and loss report - values verification - Booking channel");
			printTemplate.verifyTrue("-", 											values.get("posLocation"), 						"Profit and loss report - values verification - Pos location");
			printTemplate.verifyTrue("-", 											values.get("state"), 							"Profit and loss report - values verification - State");
			String [] adultcnt = search.getAdults().split("/");
			String [] childcnt = search.getChildren().split("/");
			int paxCount = 0;
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				paxCount = paxCount + Integer.parseInt(adultcnt[i]) + Integer.parseInt(childcnt[i]);
			}
			printTemplate.verifyTrue(String.valueOf(paxCount), values.get("guestCount"), 				"Profit and loss report - values verification - Guest count");
			String checinCheckout = "";
			if(search.getSupplier().equals("INV27"))
			{
				checinCheckout = reservationRes.getCheckin().concat("/ ").concat(reservationRes.getCheckout());
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				checinCheckout = Calender.getDate("yyyyMMdd", "yyyy-MM-dd", reservationRes.getCheckin()).concat("/ ").concat(Calender.getDate("yyyyMMdd", "yyyy-MM-dd", reservationRes.getCheckout()));
			}
			printTemplate.verifyTrue(checinCheckout, 								values.get("checkin/checkout"), 				"Profit and loss report - values verification - Checkin / Checkout");
			printTemplate.verifyTrue(search.getNights(), 							values.get("numberOfNights"), 					"Profit and loss report - values verification - Number of nights");
			printTemplate.verifyTrue(defaultCC, 									values.get("baseCurrency"), 					"Profit and loss report - values verification - Base currency");
			String value 	= converter.convertWithoutRoundingUp(defaultCC, reservationRes.getPrice(), values.get("baseCurrency"), currencyMap, reservationRes.getCurrencyCode());
			String pm 		= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("hotelpm"), defaultCC, values.get("baseCurrency"), currencyMap);
			String bf 		= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("hotelbf"), defaultCC, values.get("baseCurrency"), currencyMap);
			String ccFee	= tax.getTaxWithoutRoundUp(value, portalSpecificData.get("portal.ccfee"), defaultCC, values.get("baseCurrency"), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String grossBookingValue = String.valueOf(Float.parseFloat(value) + Float.parseFloat(pm) + Float.parseFloat(bf) + Float.parseFloat(ccFee));
			printTemplate.verifyTrue(grossBookingValue, 							values.get("grossBookingValue"), 				"Profit and loss report - values verification - Gross booking value");
			String portalCommission = String.valueOf(Double.parseDouble(pm) + Double.parseDouble(bf));
			printTemplate.verifyTrue(portalCommission, 								values.get("portalCommission"), 				"Profit and loss report - values verification - Portal commission");
			printTemplate.verifyTrue("N/A", 										values.get("agentCommission"), 					"Profit and loss report - values verification - Agent commission");
			printTemplate.verifyTrue(grossBookingValue, 							values.get("netBookingValue"), 					"Profit and loss report - values verification - Net booking value");
			String costOfSales = String.valueOf(Float.parseFloat(value) + Float.parseFloat(ccFee));
			printTemplate.verifyTrue(costOfSales,	 								values.get("costOfSales"), 						"Profit and loss report - values verification - Cost of sales");
			portalCommission = String.valueOf(Float.parseFloat(pm) + Float.parseFloat(bf));
			printTemplate.verifyTrue(portalCommission, 								values.get("profit"), 							"Profit and loss report - values verification - Profit");
			String profitPercentage = String.valueOf((Float.parseFloat(portalCommission)/Float.parseFloat(grossBookingValue))*100);
			BigDecimal bd = new BigDecimal(Float.parseFloat(profitPercentage));
		    bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		    profitPercentage = String.valueOf(bd);
			printTemplate.verifyTrue(profitPercentage.concat(" %"), 				values.get("profitPercentage"), 				"Profit and loss report - values verification - Profit percentage");
			printTemplate.verifyTrue(grossBookingValue, 							values.get("invoiceTotal"), 					"Profit and loss report - values verification - Invoice total");
			printTemplate.verifyTrue(grossBookingValue, 							values.get("invoicePaid"), 						"Profit and loss report - values verification - invoice paid");
			printTemplate.verifyTrue("0.00", 										values.get("balanceDue"), 						"Profit and loss report - values verification - Balance due");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("", String.valueOf(e), "Profit and loss report values verification error - ");
		}
//		printTemplate.markTableEnd();
	}
	
	public void thirdPartySupplierPayableValuesVerification(ReportTemplate printTemplate, hotelDetails res, HashMap<String, String> values, HotelSearchObject search, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificData, HotelConfirmationPageDetails confirmation, ArrayList<HotelOccupancyObject> occupancyList, String reportDate)
	{
//		printTemplate.setTableHeading("Third party supplier payable report - Values verification");
		try {
			printTemplate.verifyTrue(confirmation.getReservationNumber(),	values.get("bookingNumber"), 		"Third party supplier payable report - Values verification - Booking status");
			//printTemplate.verifyTrue("", 									values.get("documentNumber"), 		"Third party supplier payable report - Values verification - Document number");
			printTemplate.verifyTrue(reportDate, 							values.get("bookingDate"), 			"Third party supplier payable report - Values verification - Booking date");
			printTemplate.verifyTrue(search.getChannel(), 					values.get("bookingChannel"), 		"Third party supplier payable report - Values verification - Booking channel");
			printTemplate.verifyTrue("Hotels", 								values.get("productType"), 			"Third party supplier payable report - Values verification - Product type");
			printTemplate.verifyTrue(res.getHotel(), 						values.get("productName"), 			"Third party supplier payable report - Values verification - Product name");
			String checkin = "";
			if(search.getSupplier().equals("rezlive"))
			{
				checkin = Calender.getDate("MMM dd yyyy", "dd-MMM-yyyy", res.getCheckin());
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				checkin = Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", res.getCheckin());
			}
			else if(search.getSupplier().equals("INV27"))
			{
				checkin = Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", res.getCheckin());
			}
			else
			{
				checkin = res.getCheckin();
			}
			printTemplate.verifyTrue(checkin, 								values.get("serviceElementDate"), 			"Third party supplier payable report - Values verification - Service element date");
			String supplier = "";
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				supplier = "HotelBed_Hotels";
			}
			else if(search.getSupplier().equals("INV27"))
			{
				supplier = "GTA_Hotels";
			}
			else
			{
				supplier = search.getSupplier();
			}
			printTemplate.verifyTrue(supplier, 					values.get("supplierName"), 				"Third party supplier payable report - Values verification - Supplier name");
			printTemplate.verifyTrue(res.getSupConfirmationNumber(), 		values.get("supplierConfirmationNumber"), 	"Third party supplier payable report - Values verification - Supplier confirmation number");
			String status = "";
			if(res.getAvailabilityStatus().equalsIgnoreCase("CONFIRMED"))
			{
				status = "Normal";
			}
			else
			{
				status = res.getAvailabilityStatus();
			}
			printTemplate.verifyTrue(status, 			values.get("bookingStatus"), 				"Third party supplier payable report - Values verification - Booking number");
			String customerName 	= occupancyList.get(0).getAdultFirstName().concat(" ").concat(occupancyList.get(0).getAdultLastName());
			printTemplate.verifyTrue(customerName, 							values.get("guestName"), 					"Third party supplier payable report - Values verification - Guest name");
			
			String[] portalCurrencyHelper = values.get("portalCurrency").split(" ");
			String portalCurrency = portalCurrencyHelper[4].replace("(", "").replace(")", "");
			printTemplate.verifyTrue(defaultCC, portalCurrency, 			"Third party supplier payable report - Values verification - Portal currency");
			
			String portalValue 			= converter.convertWithoutRoundingUp(defaultCC, res.getPrice(), portalCurrency, currencyMap, res.getCurrencyCode());
			String supplierValue 		= converter.convertWithoutRoundingUp(defaultCC, res.getPrice(), values.get("supplierCurrency"), currencyMap, res.getCurrencyCode());
			
			printTemplate.verifyTrue(portalValue, 							values.get("portalPayableAmount"), 		"Third party supplier payable report - Values verification - Payable amount in portal currency");
			printTemplate.verifyTrue("0.00", 								values.get("portalTotalPaid"), 			"Third party supplier payable report - Values verification - Total paid in portal currency");
			printTemplate.verifyTrue(portalValue,		 					values.get("portalBalanceDue"), 		"Third party supplier payable report - Values verification - Balance due in portal currency");
			printTemplate.verifyTrue(res.getCurrencyCode(), 				values.get("supplierCurrency"), 		"Third party supplier payable report - Values verification - Supplier currency");
			printTemplate.verifyTrue(supplierValue, 						values.get("supplierPayableAmount"), 	"Third party supplier payable report - Values verification - Payable amount in supplier currency");
			printTemplate.verifyTrue("0.00", 								values.get("supplierTotalPaid"), 		"Third party supplier payable report - Values verification - Total paid in supplier currency");
			printTemplate.verifyTrue(supplierValue, 						values.get("supplierBalanceDue"), 		"Third party supplier payable report - Values verification - Balance due in supplier currency");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("", String.valueOf(e), "3rd party supplier payable report values verification error - ");
		}
//		printTemplate.markTableEnd();
	}
	
	public void confirmationMail(ReportTemplate printTemplate, HashMap<String, String> values, HashMap<String, String> portalSpecificData, hotelDetails reservationRes, ArrayList<HashMap<String, String>> websiteDetails, HotelConfirmationPageDetails confirmation, HashMap<String, String> currencyMap, String defaultCC, ArrayList<hotelDetails> hotelAvailabilityRes, HashMap<String, String> paymentDetails, ArrayList<HotelOccupancyObject> occupancyList, HotelSearchObject search, hotelDetails roomAvailablityRes)
	{
//		printTemplate.setTableHeading("Confirmation mail - Values verification");
		try {
			String[] 		splitter 		= values.get("portalInfo").split("Tel");
			String 			address 		= splitter[0].replace("Address : ", "").trim();
							splitter 		= splitter[1].split("Fax");
			String 			tel 			= splitter[0].replace(":", "");
							splitter 		= splitter[1].split("Email");
			String 			fax 			= splitter[0].replace(":", "");
							splitter 		= splitter[1].split("Web Site");
			String 			email 			= splitter[0].replace("", ":").trim();
			String 			website 		= splitter[1].replace(":", "").trim();
			
			printTemplate.verifyTrue(websiteDetails.get(0).get("websiteAddress").trim(), 	address.trim(), 	"Confirmation mail - Values verification - Portal address 1");
			printTemplate.verifyTrue(websiteDetails.get(0).get("telephone"), 		tel, 		"Confirmation mail - Values verification - Portal contact number");
			printTemplate.verifyTrue(websiteDetails.get(0).get("fax"), 				fax, 		"Confirmation mail - Values verification - Portal fax number");
			printTemplate.verifyTrue(websiteDetails.get(0).get("email"), 			email, 		"Confirmation mail - Values verification - Portal email");
			printTemplate.verifyTrue(websiteDetails.get(0).get("websiteUrl").replace(":", ""), 		website, 	"Confirmation mail - Values verification - Portal website");
			String customerName = portalSpecificData.get("cd.firstname").concat("  ").concat(portalSpecificData.get("cd.lastname"));
			printTemplate.verifyTrue("Mr   " + customerName, 							values.get("customerName").replace("Name : ", "").trim(), 						"Confirmation mail - Values verification - Cusomer name");
			printTemplate.verifyTrue(confirmation.getReservationNumber(), 			values.get("confirmationNumber").replace("Reference No : ", ""), 		"Confirmation mail - Values verification - Reservation number");
			printTemplate.verifyTrue(reservationRes.getAvailabilityStatus(), 		values.get("bookingStatus").replace("Booking Status : ", ""), 			"Confirmation mail - Values verification - Booking status");
			printTemplate.verifyTrue("", 											values.get("hotelAddress").replace(":", "").trim(), 					"Confirmation mail - Values verification - Hotel address 1");
			String[] address2 = search.getDestination().split("\\|");			
			printTemplate.verifyTrue(address2[1], 									values.get("hotelAddress2").replace(":", "").trim(), 					"Confirmation mail - Values verification - Hotel address 2");
			printTemplate.verifyTrue("", 											values.get("hotelTelephone").replace(":", "").trim(), 					"Confirmation mail - Values verification - Hotel contact number");
			printTemplate.verifyTrue("", 											values.get("hotelEmail").replace(":", "").trim(), 						"Confirmation mail - Values verification - Hotel email");
			String checkin = "";
			String checkout = "";
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				checkin = Calender.getDate("yyyyMMdd", "MMM-dd-yyyy", reservationRes.getCheckin());
				checkout = Calender.getDate("yyyyMMdd", "MMM-dd-yyyy", reservationRes.getCheckout());
			}
			else if(search.getSupplier().equals("INV27"))
			{
				checkin = Calender.getDate("yyyy-MM-dd", "MMM-dd-yyyy", reservationRes.getCheckin());
				checkout = Calender.getDate("yyyy-MM-dd", "MMM-dd-yyyy", reservationRes.getCheckout());
			}
			else
			{
				checkin = reservationRes.getCheckin();
				checkout = reservationRes.getCheckout();
			}
			printTemplate.verifyTrue(checkin,					 					values.get("hotelCheckin").replace(":", "").trim(), 					"Confirmation mail - Values verification - Checkin date");
			printTemplate.verifyTrue(checkout,					 					values.get("hotelCheckout").replace(":", "").trim(), 					"Confirmation mail - Values verification - Checkout date");
			printTemplate.verifyTrue(reservationRes.getNights().replace("-", ""), 	values.get("hotelNumOfNights").replace(":", "").trim(), 				"Confirmation mail - Values verification - Number of nights");
			printTemplate.verifyTrue(search.getRooms(),				 				values.get("hotelNumOfRooms").replace(":", "").trim(), 					"Confirmation mail - Values verification - Number of rooms");
			printTemplate.verifyTrue(reservationRes.getAvailabilityStatus(), 		values.get("hotelBookingStatus").replace(":", "").trim(), 				"Confirmation mail - Values verification - Hotel booking status");
			printTemplate.verifyTrue(reservationRes.getSupConfirmationNumber(), 	values.get("hotelSupConfirmation").replace(":", "").trim(), 			"Confirmation mail - Values verification - Supplier confirmation number");
			printTemplate.verifyTrue(reservationRes.getHotel(), 					values.get("hotelName"), 												"Confirmation mail - Values verification - Hotel name");
			String starRating = "";
			if(search.getSupplier().equals("INV27"))
			{
				starRating = roomAvailablityRes.getStarRating() + " Star";
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				String temp[] = reservationRes.getStarRating().split(" ");
				starRating = temp[0] + " Star";
			}
			else
			{
				starRating = reservationRes.getStarRating();
			}
			printTemplate.verifyTrue(starRating, 				values.get("hotelStarRating"), 											"Confirmation mail - Values verification - Star rating");
			String 		roomdetails1 	= "";
			String 		roomtype1 		= "";
			String 		bedtype1 		= "";
			String 		rateplan1 		= "";
			String 		roomrate1 		= "";
			String[] 	adultcnt 		= reservationRes.getRoomDetails().get(0).getAdultCount().split("\\|");
			String[] 	childCount 		= reservationRes.getRoomDetails().get(0).getChildCount().split("\\|");
			String 		roomdetails2 	= "";
			String[] 	roomType 		= reservationRes.getRoomDetails().get(0).getRoomName().split("\\|");
			String 		roomtype2 		= "";
			String 		bedtype2 		= "";
			String[] 	rateplan 		= null;
			String 		rateplan2 		= "";
			String[] 	roomrate 		= reservationRes.getRoomDetails().get(0).getPrice().split("\\|");
			String 		roomrate2		= "";
			for(int i = 0 ; i < hotelAvailabilityRes.size() ; i++)
			{
				if(values.get("hotelName").equals(hotelAvailabilityRes.get(i).getHotel()))
				{
					rateplan = hotelAvailabilityRes.get(i).getRoomDetails().get(0).getBoard().split("\\|");
					break;
				}
			}
			
			for(int i = 1 ; i > 0 ; i++)
			{
				try {
					String value 	= 	converter.convert(defaultCC, roomrate[(i-1)], values.get("currencyCode"), currencyMap, reservationRes.getCurrencyCode());
					String pm		=  	tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, values.get("currencyCode"), currencyMap);
					String	rate	= 	String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					if(i==1)
					{
						roomdetails1 	= values.get("roomDetails".concat(String.valueOf(i)));
						roomtype1 		= values.get("roomType".concat(String.valueOf(i)));
						bedtype1 		= values.get("bedType".concat(String.valueOf(i)));
						rateplan1 		= values.get("ratePlan".concat(String.valueOf(i)));
						roomrate1 		= values.get("roomTotal".concat(String.valueOf(i)));
						roomdetails2 	= "Room".concat(String.valueOf(i)).concat(" (Adult(s)").concat(adultcnt[(i-1)]).concat(", Children").concat(childCount[(i-1)]).concat(")");
						roomtype2 		= roomType[0];
						bedtype2 		= "-";
						rateplan2		= rateplan[0];
						roomrate2		= rate;
					}
					else
					{
						roomdetails1 	= roomdetails1.concat(",").concat(values.get("roomDetails".concat(String.valueOf(i))));
						roomtype1 		= roomtype1.concat(",").concat(values.get("roomType".concat(String.valueOf(i))));
						bedtype1 		= bedtype1.concat(",").concat(values.get("bedType".concat(String.valueOf(i))));
						rateplan1 		= rateplan1.concat(",").concat(values.get("ratePlan".concat(String.valueOf(i))));
						roomrate1 		= roomrate1.concat(",").concat(values.get("roomTotal".concat(String.valueOf(i))));
						roomdetails2 	= roomdetails2.concat(",").concat("Room".concat(String.valueOf(i)).concat(" (Adult(s)").concat(adultcnt[(i-1)]).concat(", Children").concat(childCount[(i-1)]).concat(")"));
						roomtype2 		= roomtype2.concat(",").concat(roomType[(i-1)]);
						bedtype2		= bedtype2.concat(",").concat("-");
						rateplan2		= rateplan2.concat(",").concat(rateplan[(i-1)]);
						roomrate2		= roomrate2.concat(",").concat(rate);
					}
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
			printTemplate.verifyTrue(roomdetails2, 								roomdetails1, 															"Confirmation mail - Values verification - Room occupancy details");
			printTemplate.verifyTrue(roomtype2, 								roomtype2, 																"Confirmation mail - Values verification - Room type");
			printTemplate.verifyTrue(bedtype2, 									bedtype2, 																"Confirmation mail - Values verification - Bed type");
			printTemplate.verifyTrue(rateplan2, 								rateplan2, 																"Confirmation mail - Values verification - Rate plan");
			printTemplate.verifyTrue(roomrate2, 								roomrate2, 																"Confirmation mail - Values verification - Room price");
			String value 	= 	converter.convert(defaultCC, reservationRes.getPrice(), values.get("currencyCode"), currencyMap, reservationRes.getCurrencyCode());
			String pm		=	tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, values.get("currencyCode"), currencyMap);
			String bf		= 	tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, values.get("currencyCode"), currencyMap);
			String ccfee 	=	tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, values.get("currencyCode"), currencyMap, portalSpecificData.get("portal.pgCurrency"));
		//	printTemplate.verifyTrue("", 										values.get("currencyCode"), 												"Confirmation mail - Values verification - Currency code");
			String subtotal = 	String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
			String	taxes 	= 	String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccfee));
			String hotelTotal = String.valueOf(Integer.parseInt(subtotal) + Integer.parseInt(bf));
			String total	=	String.valueOf(Integer.parseInt(subtotal) + Integer.parseInt(taxes));
			printTemplate.verifyTrue(subtotal, 									values.get("hotelSubtotal").replace(",", "").replace(".00", ""), 			"Confirmation mail - Values verification - Hotel sub total");
			printTemplate.verifyTrue(bf, 										values.get("hotelTotalTaxes").replace(",", "").replace(".00", ""), 			"Confirmation mail - Values verification - Hotel total taxes");
			printTemplate.verifyTrue(hotelTotal, 								values.get("hotelAmountTotal").replace(",", "").replace(".00", ""), 		"Confirmation mail - Values verification - Hotel total");
			printTemplate.verifyTrue(hotelTotal, 								values.get("hotelAmountPayableNow").replace(",", "").replace(".00", ""), 	"Confirmation mail - Values verification - Hotel amount payable now");
			printTemplate.verifyTrue("0", 										values.get("hotelPayAtCheckin").replace(",", "").replace(".00", ""), 		"Confirmation mail - Values verification - Hotel amount payable at checkin");
			printTemplate.verifyTrue(subtotal, 									values.get("subtotal").replace(",", "").replace(".00", ""), 				"Confirmation mail - Values verification - Sub total");
			printTemplate.verifyTrue(taxes, 									values.get("totalTaxes").replace(",", "").replace(".00", ""), 				"Confirmation mail - Values verification - Total taxes");
			printTemplate.verifyTrue(total, 									values.get("amountTotal").replace(",", "").replace(".00", ""), 				"Confirmation mail - Values verification - Total");
			printTemplate.verifyTrue(total, 									values.get("amountPayableNow").replace(",", "").replace(".00", ""), 		"Confirmation mail - Values verification - Amount payable now");
			printTemplate.verifyTrue("0",	 									values.get("payAtCheckin").replace(",", "").replace(".00", ""), 			"Confirmation mail - Values verification - Pay at ching");
			printTemplate.verifyTrue(portalSpecificData.get("cd.firstname"), 	values.get("firstName").replace(":", "").trim(), 							"Confirmation mail - Values verification - Customer first name");
			printTemplate.verifyTrue(portalSpecificData.get("cd.lastname"), 	values.get("lastName").replace(":", "").trim(), 							"Confirmation mail - Values verification - Customer last name");
			printTemplate.verifyTrue(portalSpecificData.get("cd.address"), 		values.get("address1").replace(":", "").trim(), 							"Confirmation mail - Values verification - Customer address 1");
			printTemplate.verifyTrue("", 										values.get("address2").replace(":", "").trim(), 							"Confirmation mail - Values verification - Customer address 2");
			printTemplate.verifyTrue(portalSpecificData.get("cd.city"), 		values.get("city").replace(":", "").trim(), 								"Confirmation mail - Values verification - Customer city");
			printTemplate.verifyTrue(portalSpecificData.get("cd.postalcode"), 	values.get("postalCode").replace(":", "").trim(), 							"Confirmation mail - Values verification - Customer postal code");
			printTemplate.verifyTrue(portalSpecificData.get("cd.country"), 		values.get("country").replace(":", "").trim(), 								"Confirmation mail - Values verification - Customer country");
			String phoneNumber = portalSpecificData.get("cd.phoneNumberCode").concat("-").concat(portalSpecificData.get("cd.phoneNumber"));
			printTemplate.verifyTrue(phoneNumber, 								values.get("phoneNumber").replace(":", "").trim(), 							"Confirmation mail - Values verification - Customer phone number");
			printTemplate.verifyTrue(phoneNumber, 								values.get("emergencyNumber").replace(":", "").trim(), 						"Confirmation mail - Values verification - Customer emergency phone number");
			printTemplate.verifyTrue(portalSpecificData.get("cd.hotelEmail"), 	values.get("email").replace(":", "").trim(), 								"Confirmation mail - Values verification - Customer email address");
			printTemplate.verifyTrue("-",								 		values.get("state").replace(":", "").trim(), 								"Confirmation mail - Values verification - Customer state");
			String titleA 		= "";
			String firstnameA 	= "";
			String lastnameA 	= "";
			String childageA 	= "";
			String smokingA 	= "";
			String handicapA 	= "";
			String wheelchairA 	= "";
			String firstnameE 	= "";
			String lastnameE 	= "";
			String childageE 	= "";
			String smokingE 	= "";
			String handicapE 	= "";
			String wheelchairE 	= "";
			for(int i = 1 ; i > 0 ; i++)
			{
				try {
					if(i == 1)
					{
						titleA 		= values.get("occupancyTitle".concat(String.valueOf(i)));
						firstnameA 	= values.get("occupancyFirstName".concat(String.valueOf(i)));
						lastnameA 	= values.get("occupancyLastName".concat(String.valueOf(i)));
						childageA 	= values.get("occupancyChildAge".concat(String.valueOf(i)));
						smokingA 	= values.get("occupancySmoking".concat(String.valueOf(i)));
						handicapA 	= values.get("occupancyHandicap".concat(String.valueOf(i)));
						wheelchairA = values.get("occupancyWheelchair".concat(String.valueOf(i)));
					}
					else
					{
						titleA 		= titleA.concat(",").concat(values.get("occupancyTitle".concat(String.valueOf(i))));
						firstnameA 	= firstnameA.concat(",").concat(values.get("occupancyFirstName".concat(String.valueOf(i))));
						lastnameA 	= lastnameA.concat(",").concat(values.get("occupancyLastName".concat(String.valueOf(i))));
						childageA 	= childageA.concat(",").concat(values.get("occupancyChildAge".concat(String.valueOf(i))));
						smokingA 	= smokingA.concat(",").concat(values.get("occupancySmoking".concat(String.valueOf(i))));
						handicapA 	= handicapA.concat(",").concat(values.get("occupancyHandicap".concat(String.valueOf(i))));
						wheelchairA = wheelchairA.concat(",").concat(values.get("occupancyWheelchair".concat(String.valueOf(i))));
					}
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
			}
			
			String[] adultCnt = search.getAdults().split("/");
			String[] childCnt = search.getChildren().split("/");
			String[] age = search.getAge().split("/");
			int cnt1 = 0;
			int cnt2 = 0;
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				for(int j = 0 ; j < Integer.parseInt(adultCnt[i]) ; j++)
				{
					if(j == 0 && i == 0)
					{
						firstnameE 	= occupancyList.get(cnt1).getAdultFirstName();
						lastnameE	= occupancyList.get(cnt1).getAdultLastName();
						childageE 	= "-";
						smokingE 		= occupancyList.get(cnt1).getAdultSmoking();
						handicapE 		= occupancyList.get(cnt1).getAdultHandicap();
						wheelchairE 	= occupancyList.get(cnt1).getAdultWheelchari();
					}
					else
					{
						firstnameE 	= firstnameE.concat(",").concat(occupancyList.get(cnt1).getAdultFirstName());
						lastnameE	= lastnameE.concat(",").concat(occupancyList.get(cnt1).getAdultLastName());
						childageE 		= childageE.concat(",").concat("-");
						smokingE 		= smokingE.concat(",").concat(occupancyList.get(cnt1).getAdultSmoking());
						handicapE 		= handicapE.concat(",").concat(occupancyList.get(cnt1).getAdultHandicap());
						wheelchairE 	= wheelchairE.concat(",").concat(occupancyList.get(cnt1).getAdultWheelchari());
					}
					cnt1++;
				}
				
				for(int j = 0 ; j < Integer.parseInt(childCnt[i]) ; j++)
				{
					firstnameE 	= firstnameE.concat(",").concat(occupancyList.get(cnt1).getChildFirstName());
					lastnameE	= lastnameE.concat(",").concat(occupancyList.get(cnt1).getChildLastName());
					childageE 			= childageE.concat(",").concat(age[cnt2]);
					String smoke 		= "";
					String handicap 	= "";
					String wheelchair 	= "";
					smokingE 			= smokingE.concat(",").concat("");
					handicapE 			= handicapE.concat(",").concat(occupancyList.get(cnt2).getChildHandicap());
					wheelchairE 		= wheelchairE.concat(",").concat(occupancyList.get(cnt2).getChildWheelchair());
					cnt2++;
				}
			}
			
			try {
				for(int i = 0 ; i < reservationRes.getRoomDetails().size() ; i++)
				{
					for(int j = 0 ; j < reservationRes.getRoomDetails().get(i).getOccupancyDetails().size() ; j++)
					{
						System.out.println(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getFirstName().replace("Mr", "").trim());
						if(j == 0 && i ==0)
						{
							firstnameE = reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getFirstName().replace("Mr", "").trim();
							lastnameE = reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getLastName();
							System.out.println(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getType());
							if(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getType().equals("AD"))
							{
								childageE = "-";
							}
							else
							{
								childageE = reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(i).getAge();
							}
							
						}
						else
						{
							firstnameE 	= firstnameE.concat(",").concat(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getFirstName());
							lastnameE 	= lastnameE.concat(",").concat(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getLastName());
							if(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getType().equals("Adult"))
							{
								childageE = childageE.concat(",").concat("-");
							}
							else
							{
								childageE = childageE.concat(",").concat(reservationRes.getRoomDetails().get(i).getOccupancyDetails().get(j).getAge());
							}
						}	
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			System.out.println(firstnameE);
			printTemplate.verifyTrue(firstnameE, 								firstnameA.replace(", ,", ","), 								"Confirmation mail - Values verification - First name");
			printTemplate.verifyTrue(lastnameE, 								lastnameA.replace(", ,", ","), 									"Confirmation mail - Values verification - Last name");
			printTemplate.verifyTrue(childageE, 								childageA.replace(", ,", ","), 									"Confirmation mail - Values verification - Child age");
			printTemplate.verifyTrue(smokingE, 									smokingA.replace(", ,", ","), 									"Confirmation mail - Values verification - Smoking");
			printTemplate.verifyTrue(handicapE, 								handicapA.replace(", ,", ","), 									"Confirmation mail - Values verification - Handicap");
			printTemplate.verifyTrue(wheelchairE, 								wheelchairA.replace(", ,", ","), 								"Confirmation mail - Values verification - Wheelchair");
			printTemplate.verifyTrue(paymentDetails.get("paymentReference"), 	values.get("paymentID").replace(":", "").trim(), 		"Confirmation mail - Values verification - Payment reference");
			printTemplate.verifyTrue(paymentDetails.get("authId"), 				values.get("paymentAuth").replace(":", "").trim(), 				"Confirmation mail - Values verification - Auth code");
			printTemplate.verifyTrue(paymentDetails.get("transactionID"), 			values.get("paymentReference").replace(":", "").trim(), 				"Confirmation mail - Values verification - Payment ID");
			String[] paymentCC = values.get("paymentCC").split("\\(");
			paymentCC = paymentCC[1].split("\\)");
			value 	= 	converter.convert(defaultCC, reservationRes.getPrice(), paymentCC[0], currencyMap, reservationRes.getCurrencyCode());
			pm		=	tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, paymentCC[0], currencyMap);
			bf		= 	tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, paymentCC[0], currencyMap);
			ccfee 	=	tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, paymentCC[0], currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String paymentTotal = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccfee)); //full amount to be paid
			System.out.println(paymentTotal);
			System.out.println(values.get("PaymentAmount").replace(":", "").trim().replace(",", "").replace(".00", ""));
			printTemplate.verifyTrue(paymentTotal, 								values.get("PaymentAmount").replace(":", "").trim().replace(",", "").replace(".00", ""), 	"Confirmation mail - Values verification - Payment amoount");
			printTemplate.verifyTrue(portalSpecificData.get("customerNotes"), 	values.get("customerNotes"), 																"Confirmation mail - Values verification - Customer notes");
			printTemplate.verifyTrue(portalSpecificData.get("hotelNotes"), 		values.get("hotelNotes"), 																	"Confirmation mail - Values verification - Hotel notes");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("", String.valueOf(e), "Confirmation mail values verification error - ");
		}
//		printTemplate.markTableEnd();
	}
	
	public void voucherMail(ReportTemplate printTemplate, HashMap<String, String> values, HotelConfirmationPageDetails confirmation, ArrayList<HotelOccupancyObject> occupancyList, ArrayList<HashMap<String, String>> websiteDetails, HashMap<String, String> portalSpecificData, hotelDetails reservationRes, HotelSearchObject search, hotelDetails roomRes)
	{
		try {
			String[] temp = values.get("portalDetails").split(":");
			String address = temp[1].replace("Tel", "").trim();
			String tel = temp[2].replace("Fax", "").trim();
			String fax = temp[3].replace("Email", "").trim();
			String email = temp[4].replace("Web Site", "").trim();
			String website = temp[5].trim();
			System.out.println(portalSpecificData.get("cd.firstname"));
			System.out.println(portalSpecificData.get("cd.lastname"));
			String customerName = portalSpecificData.get("cd.firstname").concat(" ").concat(portalSpecificData.get("cd.lastname")); 
			printTemplate.verifyTrue(websiteDetails.get(0).get("websiteAddress").trim(), 	address.trim(), 							"Voucher mail values verification - Portal Details - Address");
			printTemplate.verifyTrue(websiteDetails.get(0).get("telephone"), 				tel, 										"Voucher mail values verification - Portal Details - Tel");
			printTemplate.verifyTrue(websiteDetails.get(0).get("fax"), 						fax, 										"Voucher mail values verification - Portal Details - Fax");
			printTemplate.verifyTrue(websiteDetails.get(0).get("email"), 					email, 										"Voucher mail values verification - Portal Details - Email");
			printTemplate.verifyTrue(websiteDetails.get(0).get("websiteUrl"), 				website, 									"Voucher mail values verification - Portal Details - Web site");
			printTemplate.verifyTrue("Mr " + customerName, 									values.get("leadPassengerName").replace("   ", " "), 			"Voucher mail values verification - Lead passenger name");
	//		printTemplate.verifyTrue("", 													values.get("vouvherNumber"), 				"Voucher mail values verification - Voucher number");
			printTemplate.verifyTrue(confirmation.getReservationNumber(), 					values.get("reservationNumber"), 			"Voucher mail values verification - Reservation number");
			String customerAddress = portalSpecificData.get("cd.address").concat(", ").concat(portalSpecificData.get("cd.city"));
			printTemplate.verifyTrue(customerAddress.trim(), 								values.get("leadPassengerAddress").trim(), 	"Voucher mail values verification - Lead passenger address");
			printTemplate.verifyTrue(portalSpecificData.get("cd.country"), 					values.get("leadPassengerCountry"), 		"Voucher mail values verification - Lead passenger country");
			printTemplate.verifyTrue(reservationRes.getHotel(), 							values.get("hotelName"), 					"Voucher mail values verification - Hotel name");
			printTemplate.verifyTrue(reservationRes.getCity(), 								values.get("city"), 						"Voucher mail values verification - City");
			printTemplate.verifyTrue("-", 													values.get("telephone"), 					"Voucher mail values verification - Telephone");

			String checkin = "";
			String checkout = "";
			if(search.getSupplier().equals("INV27"))
			{
				checkin = Calender.getDate("yyyy-MM-dd", "MMM-dd-yyyy", reservationRes.getCheckin());
				checkout = Calender.getDate("yyyy-MM-dd", "MMM-dd-yyyy", reservationRes.getCheckout());
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				checkin = Calender.getDate("yyyyMMdd", "MMM-dd-yyyy", reservationRes.getCheckin());
				checkout = Calender.getDate("yyyyMMdd", "MMM-dd-yyyy", reservationRes.getCheckout());
			}
			else
			{
				checkin = reservationRes.getCheckin();
				checkout = reservationRes.getCheckout();
			}
			
			printTemplate.verifyTrue(checkin, 	values.get("chckinDate"), 				"Voucher mail values verification - Checkin date");
			printTemplate.verifyTrue("-", 		values.get("checkinTime"), 				"Voucher mail values verification - Checkin time");
			printTemplate.verifyTrue("0000", 	values.get("estimatedArrivalTime"), 	"Voucher mail values verification - Estimated arrival time");
			
			String roomType = "";
			String rateplan = "";
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				if(i == 0)
				{
					if(search.getSupplier().equals("INV27"))
					{
						roomType = reservationRes.getRoomDetails().get(i).getDescription();
						rateplan = "RoomOnly";
					}
					else
					{
						roomType = reservationRes.getRoomDetails().get(i).getRoomName();
						rateplan = reservationRes.getRoomDetails().get(i).getBoard();
					}
				}
				else
				{
					if(search.getSupplier().equals("INV27"))
					{
						roomType = roomType + "," + reservationRes.getRoomDetails().get(i).getDescription();
						rateplan = rateplan + "," + "RoomOnly";
					}
					else
					{
						roomType = roomType + "," + reservationRes.getRoomDetails().get(i).getRoomName();
						rateplan = rateplan + "," + reservationRes.getRoomDetails().get(i).getBoard();
					}
				}
			}
			printTemplate.verifyTrue(roomType, values.get("roomType"), "Voucher mail values verification - Room type");
			printTemplate.verifyTrue(rateplan, values.get("ratePlan"), "Voucher mail values verification - Rate plan");
			temp = search.getAdults().split("/");
			int adultsCount = 0;
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				adultsCount = adultsCount + Integer.parseInt(temp[i]);
			}
			printTemplate.verifyTrue(String.valueOf(adultsCount), values.get("adultCount"), "Voucher mail values verification - Adult count");
			String starRating = "";
			if(search.getSupplier().equals("INV27"))
			{
				starRating = roomRes.getStarRating() + " Star";
			}
			else if(search.getSupplier().equals("hotelbeds_v2"))
			{
				String temp1[] = reservationRes.getStarRating().split(" ");
				starRating = temp1[0] + " Star";
			}
			else
			{
				starRating = reservationRes.getStarRating();
			}
			printTemplate.verifyTrue(starRating, 								values.get("starCategory"), 	"Voucher mail values verification - Star category");
			printTemplate.verifyTrue(reservationRes.getAddress(), 				values.get("address"), 			"Voucher mail values verification - Address");
			printTemplate.verifyTrue(reservationRes.getAvailabilityStatus(),	values.get("status"), 			"Voucher mail values verification - Status");
			printTemplate.verifyTrue(checkout, 									values.get("checkoutDate"), 	"Voucher mail values verification - Checkout date");
			printTemplate.verifyTrue("-", 										values.get("checkoutTime"), 	"Voucher mail values verification - Checkout time");
			String bedType = "";
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				if(i == 0)
				{
					if(search.getSupplier().equals("INV27"))
					{
						if(reservationRes.getRoomDetails().get(i).getRoomCode().equals("DB"))
						{
							bedType = "Double";
						}
						else
						{
							bedType = reservationRes.getRoomDetails().get(i).getRoomCode();
						}
					}
					else
					{
						bedType = reservationRes.getRoomDetails().get(i).getBedType();
					}
				}
				else
				{
					if(search.getSupplier().equals("INV27"))
					{
						if(reservationRes.getRoomDetails().get(i).getRoomCode().equals("DB"))
						{
							bedType = bedType + "," + "Double";
						}
						else
						{
							bedType = bedType + "," + reservationRes.getRoomDetails().get(i).getRoomCode();
						}
						
					}
					else
					{
						bedType = bedType.concat(",").concat(reservationRes.getRoomDetails().get(i).getBedType());
					}
				}
				
			}
			
			printTemplate.verifyTrue(bedType, 						values.get("bedType"), 		"Voucher mail values verification - Bed type");
			printTemplate.verifyTrue(search.getNights(), 			values.get("nights"), 		"Voucher mail values verification - Nights");
			printTemplate.verifyTrue(search.getRooms(), 			values.get("roomCount"), 	"Voucher mail values verification - Room count");
			temp = search.getChildren().split("/");
			int childrenCount = 0;
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				childrenCount = childrenCount + Integer.parseInt(temp[i]);
			}
			printTemplate.verifyTrue(String.valueOf(childrenCount), values.get("childrenCount"), "Voucher mail values verification - Child count");
			String passengernameA 	= ""; 
			String passengerTypeA 	= ""; 
			String passengerAgeA 	= "";
			String smokingA 		= "";
			String handicapA 		= "";
			String wheelchairA 		= "";
			String passengernameE 	= ""; 
			String passengerTypeE 	= ""; 
			String passengerAgeE 	= "";
			String smokingE 		= "";
			String handicapE 		= "";
			String wheelchairE 		= "";
			
			for(int i = 1 ; i > 0 ; i++)
			{
				try {
					
					if(values.get("passengerName" + String.valueOf(i)).equals(" ") || values.get("passengerName" + String.valueOf(i)).equals(""))
					{
						break;
					}
					if(i == 1)
					{
						passengernameA 	= values.get("passengerName" + String.valueOf(i));
						passengerTypeA 	= values.get("passengerType" + String.valueOf(i));	
						passengerAgeA 	= values.get("passengerAge" + String.valueOf(i));
						smokingA 		= values.get("smoking" + String.valueOf(i));
						handicapA 		= values.get("handicap" + String.valueOf(i));
						wheelchairA 	= values.get("wheelchair" + String.valueOf(i));
					}
					else
					{
						passengernameA 	= passengernameA.concat(",").concat(values.get("passengerName" + String.valueOf(i)));
						passengerTypeA 	= passengerTypeA.concat(",").concat(values.get("passengerType" + String.valueOf(i)));
						passengerAgeA 	= passengerAgeA.concat(",").concat(values.get("passengerAge" + String.valueOf(i)));
						smokingA 		= smokingA.concat(",").concat(values.get("smoking" + String.valueOf(i)));
						handicapA 		= handicapA.concat(",").concat(values.get("handicap" + String.valueOf(i)));
						wheelchairA 	= wheelchairA.concat(",").concat(values.get("wheelchair" + String.valueOf(i)));
					}
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
			String[] adultCnt = search.getAdults().split("/");
			String[] childCnt = search.getChildren().split("/");
			String[] age = search.getAge().split("/");
			int cnt1 = 0;
			int cnt2 = 0;
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				for(int j = 0 ; j < Integer.parseInt(adultCnt[i]) ; j++)
				{
					String smoking = "";
					if(occupancyList.get(cnt1).getAdultSmoking().equals("n"))
					{
						smoking = "N";
					}
					if(j == 0 && i == 0)
					{
						passengernameE 	= occupancyList.get(cnt1).getAdultLastName().concat(" / ").concat(occupancyList.get(cnt1).getAdultFirstName());
						passengerTypeE 	= "Adult";
						passengerAgeE 	= "-";
						smokingE 		= smoking;
						handicapE 		= occupancyList.get(cnt1).getAdultHandicap();
						wheelchairE 	= occupancyList.get(cnt1).getAdultWheelchari();
					}
					else
					{
						passengernameE 	= passengernameE.concat(",").concat(occupancyList.get(cnt1).getAdultLastName().concat(" / ").concat(occupancyList.get(cnt1).getAdultFirstName()));
						passengerTypeE 	= passengerTypeE.concat(",").concat("Adult");
						passengerAgeE 	= passengerAgeE.concat(",").concat("-");
						smokingE 		= smokingE.concat(",").concat(smoking);
						handicapE 		= handicapE.concat(",").concat(occupancyList.get(cnt1).getAdultHandicap());
						wheelchairE 	= wheelchairE.concat(",").concat(occupancyList.get(cnt1).getAdultWheelchari());
					}
					cnt1++;
				}
				
				for(int j = 0 ; j < Integer.parseInt(childCnt[i]) ; j++)
				{
					passengernameE 		= passengernameE.concat(",").concat(occupancyList.get(cnt2).getChildLastName().concat(" / ").concat(occupancyList.get(cnt2).getChildFirstName()));
					passengerTypeE 		= passengerTypeE.concat(",").concat("Child");
					passengerAgeE 		= passengerAgeE.concat(",").concat(age[cnt2]);
					String smoke 		= "";
					String handicap 	= "";
					String wheelchair 	= "";
					smokingE 			= smokingE.concat(",").concat("");
					handicapE 			= handicapE.concat(",").concat(occupancyList.get(cnt2).getChildHandicap());
					wheelchairE 		= wheelchairE.concat(",").concat(occupancyList.get(cnt2).getChildWheelchair());
					cnt2++;
				}
			}
			printTemplate.verifyTrue(passengernameE, 	passengernameA, 				"Voucher mail values verification - Passenger name");
			printTemplate.verifyTrue(passengerTypeE, 	passengerTypeA, 				"Voucher mail values verification - Passenger type");
			printTemplate.verifyTrue(passengerAgeE, 	passengerAgeA, 					"Voucher mail values verification - Passenger age");
			printTemplate.verifyTrue(smokingE, 			smokingA, 						"Voucher mail values verification - Smoking");
			printTemplate.verifyTrue(handicapE, 		handicapA, 						"Voucher mail values verification - Handicap");
			printTemplate.verifyTrue(wheelchairE, 		wheelchairA, 					"Voucher mail values verification - Wheelchair");
			printTemplate.verifyTrue("", 				values.get("supplierNotes"), 	"Voucher mail values verification - Supplier notes");
			printTemplate.verifyTrue("Customer Notes", 	values.get("customerNotes"), 	"Voucher mail values verification - Customer notes");
		} catch (Exception e) {
			// TODO: handle exception
			printTemplate.verifyTrue("", String.valueOf(e), "Voucher mail values verification error - ");
		}
	}
	
	public void cancellationValuesVerification(HashMap<String, String> values, HotelConfirmationPageDetails confirmation, ArrayList<HotelOccupancyObject> occupancyList, HashMap<String, String> portalSpecificData, hotelDetails reservationRes, ReportTemplate printTemplate, String reportDate, HotelSearchObject search, HashMap<String, String> currencyMap, String defaultCC, String reportCurrency, String canDeadline, hotelDetails roomAvailablityRes) throws ParseException
	{
		printTemplate.verifyTrue(portalSpecificData.get("cd.lastname"), values.get("customerName"), "Cancellation page values verification - Customer name");
		printTemplate.verifyTrue(confirmation.getReservationNumber(), values.get("resNumber"), "Cancellation page values verification - Reservation number");
		printTemplate.verifyTrue(reportDate, values.get("bookingDate"), "Cancellation page values verification - Booking date");
		printTemplate.verifyTrue("-", values.get("toName"), "Cancellation page values verification - TO name");
		String bookingChannel = "";
		if(search.getChannel().equals("web"))
		{
			bookingChannel = "Web Reservation";
		}
		else
		{
			bookingChannel = search.getChannel();
		}
		printTemplate.verifyTrue(bookingChannel, values.get("bookingChannel"), "Cancellation page values verification - Booking channel");
		String value 	= converter.convert(defaultCC, reservationRes.getPrice(), reportCurrency, currencyMap, reservationRes.getCurrencyCode());
		String pm		= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, reportCurrency, currencyMap);
		String bf		= tax.getTax(value, portalSpecificData.get("hotelbf"), defaultCC, reportCurrency, currencyMap);
		String ccFee	= tax.getTax(value, portalSpecificData.get("portal.ccfee"), defaultCC, reportCurrency, currencyMap, portalSpecificData.get("portal.pgCurrency"));
		
		String subTotal = String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
		String total	= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm) + Integer.parseInt(bf) + Integer.parseInt(ccFee));
		
		printTemplate.verifyTrue(subTotal, 					values.get("subTotal"), "Cancellation page values verification - Sub total");
		printTemplate.verifyTrue(total, 					values.get("totalPayable"), "Cancellation page values verification - Total payable");
		printTemplate.verifyTrue(reservationRes.getHotel(), values.get("hotelName"), "Cancellation page values verification - Hotel name");
		printTemplate.verifyTrue("", 						values.get("address"), "Cancellation page values verification - Address");
		String checkin 	= "";
		String checkout = "";
		if(search.getSupplier().equals("INV27"))
		{
			checkin = Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", reservationRes.getCheckin());
			checkout = Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", reservationRes.getCheckout());
		}
		else if(search.getSupplier().equals("hotelbeds_v2"))
		{
			checkin = Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", reservationRes.getCheckin());
			checkout = Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", reservationRes.getCheckout());
		}
		else
		{
			checkin = reservationRes.getCheckin();
			checkout = reservationRes.getCheckout();
		}
		printTemplate.verifyTrue(checkin, 					values.get("arrivalDate"), 		"Cancellation page values verification - Arrival date");
		printTemplate.verifyTrue(checkout, 					values.get("departureDate"), 	"Cancellation page values verification - Departure date");
		String[] adultcnt = search.getAdults().split("/");
		String[] childcnt = search.getChildren().split("/");
		int adults = 0 ;
		int children = 0 ;
		for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
		{
			adults = adults + Integer.parseInt(adultcnt[i]);
			children = children + Integer.parseInt(childcnt[i]);
		}
		printTemplate.verifyTrue(String.valueOf(adults), values.get("adultCount"), "Cancellation page values verification - Adult count");
		String supplier = "";
		String nights = "";
		String roomCount = "";
		if(search.getSupplier().equals("hotelbeds_v2"))
		{
			nights = reservationRes.getNights().replace("-", "").trim();
			supplier = "HOTELBEDS";
			roomCount = String.valueOf(reservationRes.getRoomDetails().size());
		}
		else if(search.getSupplier().equals("INV27"))
		{
			nights = reservationRes.getNights();
			supplier = "INV27";
			roomCount = String.valueOf(reservationRes.getRoomDetails().size());
		}
		else
		{
			nights = reservationRes.getNights();
			supplier = search.getSupplier();
			roomCount = String.valueOf(reservationRes.getRoomDetails().size());
		}
		printTemplate.verifyTrue(supplier, 										values.get("supplierName"), 				"Cancellation page values verification - Supplier Name");
		String[] destination = search.getDestination().split("\\|");
		String location = destination[1] + "/-/" + portalSpecificData.get("cd.postalcode") + "/" + destination[5];
		printTemplate.verifyTrue(location, 										values.get("location"), 				"Cancellation page values verification - Location");
		printTemplate.verifyTrue(nights, 										values.get("nightsCount"), 				"Cancellation page values verification - Night count");
		printTemplate.verifyTrue(roomCount, 									values.get("roomCount"), 				"Cancellation page values verification - Room count");
		printTemplate.verifyTrue(reservationRes.getSupConfirmationNumber(), 	values.get("supConfirmationNumber"), 	"Cancellation page values verification - Suplier confirmation number");
		//printTemplate.verifyTrue("", values.get("occupancyTitleID"), "Cancellation page values verification - Occupancy - Title");
		//printTemplate.verifyTrue("", values.get("occupancyLastName"), "Cancellation page values verification - Occupancy - Last name");
		//printTemplate.verifyTrue("", values.get("occupancyFirstName"), "Cancellation page values verification - Occuancy - First name");
		String roomTypeA = "";
		String bedAndBoardA = "";
		String adultCountA = "";
		String childCountA = "";
		String roomRateA = "";
		String suppliemntaryA = "";
		String roomTotalA = "";
		String roomTypeE = "";
		String bedAndBoardE = "";
		String adultCountE = "";
		String childCountE = "";
		String roomRateE = "";
		String suppliemntaryE = "";
		String roomTotalE = "";
		
		for(int i = 1 ; i <= Integer.parseInt(values.get("roomCount")) ; i++)
		{
			if(i == 1)
			{
				roomTypeA 		= values.get("roomType" + i);
				bedAndBoardA 	= values.get("bedAndBoard" + i);
				adultCountA 	= values.get("adultCount" + i);
				childCountA 	= values.get("childrenCount" + i);
				roomRateA 		= values.get("roomRate" + i);
				suppliemntaryA 	= values.get("supplimentary" + i);
				roomTotalA 		= values.get("roomTotal" + i).replace(".00", "").trim();
			}
			else
			{
				roomTypeA 		= roomTypeA 		+ "," + values.get("roomType" + i);
				bedAndBoardA 	= bedAndBoardA 		+ "," + values.get("bedAndBoard" + i);
				adultCountA 	= adultCountA 		+ "," + values.get("adultCount" + i);
				childCountA 	= childCountA 		+ "," + values.get("childrenCount" + i);
				roomRateA 		= roomRateA 		+ "," + values.get("roomRate" + i);
				suppliemntaryA 	= suppliemntaryA 	+ "," + values.get("supplimentary" + i);
				roomTotalA 		= roomTotalA 		+ "," + values.get("roomTotal" + i).replace(".00", "").trim();
			}
		}
		
		if(search.getSupplier().equals("hotelbeds_v2"))
		{
			for(int i = 0 ; i < reservationRes.getRoomDetails().size() ; i++)
			{
				value 			= converter.convert(defaultCC, reservationRes.getRoomDetails().get(i).getPrice(), reportCurrency, currencyMap, reservationRes.getCurrencyCode());
				pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, reportCurrency, currencyMap);
				if(i == 0 )
				{
					roomTypeE 		= reservationRes.getRoomDetails().get(i).getRoomName();
					bedAndBoardE 	= reservationRes.getRoomDetails().get(i).getBedType() + "/" + reservationRes.getRoomDetails().get(i).getBoard();
					childCountE 	= reservationRes.getRoomDetails().get(i).getChildCount();
					adultCountE 	= reservationRes.getRoomDetails().get(i).getAdultCount();
					roomRateE 		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					suppliemntaryE	= "0";
					roomTotalE		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
				else
				{
					roomTypeE 		= roomTypeE 		+ "," + reservationRes.getRoomDetails().get(i).getRoomName();
					bedAndBoardE 	= bedAndBoardE 		+ "," + reservationRes.getRoomDetails().get(i).getBedType() + "/" + reservationRes.getRoomDetails().get(i).getBoard();
					childCountE 	= childCountE 		+ "," + reservationRes.getRoomDetails().get(i).getChildCount();
					adultCountE 	= adultCountE 		+ "," + reservationRes.getRoomDetails().get(i).getAdultCount();
					roomRateE 		= roomRateE 		+ "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					suppliemntaryE	= suppliemntaryE 	+ "," + "0";
					roomTotalE		= roomTotalE 		+ "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
			}
		}
		else if(search.getSupplier().equals("INV27"))
		{
			String bedType = "";
			String ratePlan = "";
			String[] adultCount = search.getAdults().split("/");
			String[] childCount = search.getChildren().split("/");
			for(int i = 0 ; i < reservationRes.getRoomDetails().size() ; i++)
			{
				System.out.println(defaultCC);
				System.out.println(roomAvailablityRes.getRoomDetails().get(i).getPrice());
				System.out.println(reportCurrency);
				System.out.println(reservationRes.getCurrencyCode());
				if(search.getSupplier().equals("INV27"))
				{
					value 			= converter.convert(defaultCC, roomAvailablityRes.getRoomDetails().get(i).getPrice(), reportCurrency, currencyMap, reservationRes.getCurrencyCode());
				}
				else
				{
					value 			= converter.convert(defaultCC, reservationRes.getRoomDetails().get(i).getPrice(), reportCurrency, currencyMap, reservationRes.getCurrencyCode());
				}
				
				pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, reportCurrency, currencyMap);
				if(reservationRes.getRoomDetails().get(i).getBedType().equals("DB"))
				{
					bedType = "Double";
				}
				else
				{
					bedType = reservationRes.getRoomDetails().get(i).getBedType();
				}
				System.out.println(reservationRes.getRoomDetails().get(i).getBoard());
				if(reservationRes.getRoomDetails().get(i).getBoard().equals(""))
				{
					ratePlan = "RoomOnly";
				}
				else
				{
					ratePlan = reservationRes.getRoomDetails().get(i).getBoard();
				}
				if(i == 0 )
				{
					roomTypeE 		= reservationRes.getRoomDetails().get(i).getDescription();
					bedAndBoardE 	= bedType + "/" + ratePlan;
					childCountE 	= childCount[i];
					adultCountE 	= adultCount[i];
					roomRateE 		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					suppliemntaryE	= "0";
					roomTotalE		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
				else
				{
					roomTypeE 		= roomTypeE 		+ "," + reservationRes.getRoomDetails().get(i).getDescription();
					bedAndBoardE 	= bedAndBoardE 		+ "," + bedType + "/" + ratePlan;
					childCountE 	= childCountE 		+ "," + childCount[i];;
					adultCountE 	= adultCountE 		+ "," + adultCount[i];
					roomRateE 		= roomRateE 		+ "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					suppliemntaryE	= suppliemntaryE 	+ "," + "0";
					roomTotalE		= roomTotalE 		+ "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
			}
		}
		else
		{
			for(int i = 0 ; i < reservationRes.getRoomDetails().size() ; i++)
			{
				value 			= converter.convert(defaultCC, reservationRes.getRoomDetails().get(i).getPrice(), reportCurrency, currencyMap, reservationRes.getCurrencyCode());
				pm 				= tax.getTax(value, portalSpecificData.get("hotelpm"), defaultCC, reportCurrency, currencyMap);
				System.out.println(value);
				System.out.println(pm);
				if(i == 0 )
				{
					roomTypeE 		= reservationRes.getRoomDetails().get(i).getRoomName();
					bedAndBoardE 	= reservationRes.getRoomDetails().get(i).getBedType() + "/" + reservationRes.getRoomDetails().get(i).getBoard();
					childCountE 	= reservationRes.getRoomDetails().get(i).getChildCount();
					adultCountE 	= reservationRes.getRoomDetails().get(i).getAdultCount();
					roomRateE 		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					suppliemntaryE	= "0";
					roomTotalE		= String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
				else
				{
					roomTypeE 		= roomTypeE 		+ "," + reservationRes.getRoomDetails().get(i).getRoomName();
					bedAndBoardE 	= bedAndBoardE 		+ "," + reservationRes.getRoomDetails().get(i).getBedType() + "/" + reservationRes.getRoomDetails().get(i).getBoard();
					childCountE 	= childCountE 		+ "," + reservationRes.getRoomDetails().get(i).getChildCount();
					adultCountE 	= adultCountE 		+ "," + reservationRes.getRoomDetails().get(i).getAdultCount();
					roomRateE 		= roomRateE 		+ "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
					suppliemntaryE	= suppliemntaryE 	+ "," + "0";
					roomTotalE		= roomTotalE 		+ "," + String.valueOf(Integer.parseInt(value) + Integer.parseInt(pm));
				}
			}
		}
		System.out.println(childCountE);
		System.out.println(childCountA);
		printTemplate.verifyTrue(roomTypeE, 		roomTypeA, 		"Cancellation page values verification - Room type");
		printTemplate.verifyTrue(bedAndBoardE, 		bedAndBoardA, 	"Cancellation page values verification - Bed and rate plan");
		printTemplate.verifyTrue(adultCountE, 		adultCountA, 	"Cancellation page values verification - Adult count");
		printTemplate.verifyTrue(childCountE, 		childCountA, 	"Cancellation page values verification - Children count");
		System.out.println(roomRateE);
		System.out.println(roomRateA);
		printTemplate.verifyTrue(roomRateE, 		roomRateA, 		"Cancellation page values verification - Room rate");
		printTemplate.verifyTrue(suppliemntaryE, 	suppliemntaryA, "Cancellation page values verification - Supplimentary");
		printTemplate.verifyTrue(roomTotalE, 		roomTotalA, 	"Cancellation page values verification - Room total");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date deadline 	= sdf.parse(canDeadline);
		Date today		= sdf.parse(Calender.getToday("dd-MMM-yyyy"));
		String supCancellationCharge = "";
		String cusCancellationCharge = "";
		String addCancellationCharge = "";
		if(deadline.after(today))
		{
			supCancellationCharge = "0.00";
			cusCancellationCharge = "0.00";
		}
		else
		{
			supCancellationCharge = value;
			cusCancellationCharge = pm;
			
			
		}
		addCancellationCharge = String.valueOf(Integer.parseInt(bf) + Integer.parseInt(ccFee));
		String totalCancellationCharge = String.valueOf(Double.parseDouble(supCancellationCharge) + Double.parseDouble(cusCancellationCharge) + Double.parseDouble(addCancellationCharge));
		printTemplate.verifyTrue(supCancellationCharge, 	values.get("supplierCancellationCharge"), 		"Cancellation page values verification - Supplier cancellation charge");
		printTemplate.verifyTrue(cusCancellationCharge, 	values.get("customerCancellationCharge"), 		"Cancellation page values verification - Customer cancellation charge");
		printTemplate.verifyTrue(supCancellationCharge, 	values.get("totalSupplierCancellationCharge"), 	"Cancellation page values verification - Total supplier cancellation charge");
		printTemplate.verifyTrue(cusCancellationCharge, 	values.get("totalCustomerCancellationCharge"), 	"Cancellation page values verification - Total customer cancellation charge");
		printTemplate.verifyTrue(addCancellationCharge, 	values.get("additionalCancellationCharge"), 	"Cancellation page values verification - Additional cancellation charge");
		printTemplate.verifyTrue("0.00", 					values.get("gstCharge"), 						"Cancellation page values verification - GST charge");
		printTemplate.verifyTrue(totalCancellationCharge, 	values.get("totalCancellationCharge"), 			"Cancellation page values verification - Total cancellation cahrge");
		printTemplate.verifyTrue("Reservation # "+ confirmation.getReservationNumber() +" has been Successfully cancelled !", 	values.get("cancellationStatus"), 				"Cancellation page values verification - Cancellation status");
	}
	
	public void cancellationReportValues(HashMap<String, String> values, HashMap<String, String> portalSpecificData, ReportTemplate printTemplate, hotelDetails reservationRes, String canDeadline, HashMap<String, String> currencyMap, String defaultCC, HotelConfirmationPageDetails confirmation, HotelSearchObject search, ArrayList<HotelOccupancyObject> occupancyList)
	{
		try {
			printTemplate.verifyTrue(confirmation.getReservationNumber() + "C", values.get("cancellationNumber"), "Cancellation report values verification - Cancellation number");
			printTemplate.verifyTrue(Calender.getToday("dd-MMM-yy"), values.get("cancellationDate"), "Cancellation report values verification - Cancellation date");
			printTemplate.verifyTrue("-", values.get("notes"), "Cancellation report values verification - Notes");
			printTemplate.verifyTrue(confirmation.getReservationNumber(), values.get("reservationNumber"), "Cancellation report values verification - Reservation number");
			printTemplate.verifyTrue(portalSpecificData.get("hotelUsername"), values.get("cancelledBy"), "Cancellation report values verification - Cancelled by");
			printTemplate.verifyTrue("", values.get("cancellationReason"), "Cancellation report values verification - Cancellation reason");
			printTemplate.verifyTrue(reservationRes.getHotel(), values.get("hotelName"), "Cancellation report values verification - Hotel name");
			
			String supName = "";
			String chekinCheckout = "";
			if(search.getSupplier().equals("hotelbeds_v2"))
			{
				supName 			= "HotelBed_Hotels";
				chekinCheckout = Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", reservationRes.getCheckin()) + " / " + Calender.getDate("yyyyMMdd", "dd-MMM-yyyy", reservationRes.getCheckout());
			}
			else if(search.getSupplier().equals("INV27"))
			{
				supName 			= "GTA_Hotels";
				chekinCheckout = Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", reservationRes.getCheckin()) + " / " + Calender.getDate("yyyy-MM-dd", "dd-MMM-yyyy", reservationRes.getCheckout());
			}
			else if(search.getSupplier().equals("INV13"))
			{
				supName = "Tourico";
				chekinCheckout = reservationRes.getCheckin() + "/" + reservationRes.getCheckout();
			}
			else if(search.getSupplier().equals("rezlive"))
			{
				supName 			= "rezlive";
				chekinCheckout = reservationRes.getCheckin() + "/" + reservationRes.getCheckout();
			}
			printTemplate.verifyTrue(supName, values.get("supplierName"), "Cancellation report values verification - Supplier name");
			printTemplate.verifyTrue(reservationRes.getSupConfirmationNumber(), values.get("supplierConfirmationNumber"), "Cancellation report values verification - Supplier confirmation number");
			printTemplate.verifyTrue(String.valueOf(reservationRes.getRoomDetails().size()), values.get("numberOfRooms"), "Cancellation report values verification - Number of rooms");
			printTemplate.verifyTrue(reservationRes.getNights(), values.get("numberOfNights"), "Cancellation report values verification - Number of nights");
			String customerName = occupancyList.get(0).getAdultFirstName() + " " + occupancyList.get(0).getAdultLastName();
			printTemplate.verifyTrue(customerName, values.get("customerName"), "Cancellation report values verification - Customer name");
			printTemplate.verifyTrue(chekinCheckout, values.get("checkin/checkout"), "Cancellation report values verification - Checkin/Checkout");
			printTemplate.verifyTrue(Calender.getToday("dd-MMM-yyyy"), values.get("bookingDate"), "Cancellation report values verification - Booking date");
			String roomType = "";
			String bedType = "";
			String ratePlan = "";
			for(int i = 0 ; i < reservationRes.getRoomDetails().size() ; i++)
			{
				if(i == 0)
				{
					if(search.getSupplier().equals("INV27"))
					{
						roomType = reservationRes.getRoomDetails().get(i).getDescription();
						if(reservationRes.getRoomDetails().get(i).getRoomCode().equals("DB"))
						{
							bedType = "Double";
						}
						else
						{
							bedType = reservationRes.getRoomDetails().get(i).getRoomCode();
						}
						
						if(reservationRes.getRoomDetails().get(i).getBoard().equals(""))
						{
							ratePlan = "RoomOnly";
						}
						else
						{
							ratePlan = reservationRes.getRoomDetails().get(i).getBoard();
						}
						
					}
					else
					{
						roomType = reservationRes.getRoomDetails().get(i).getRoomName();
						bedType = reservationRes.getRoomDetails().get(i).getBedType();
						ratePlan = reservationRes.getRoomDetails().get(i).getBoard();
					}		
				}
				else
				{
					if(search.getSupplier().equals("INV27"))
					{
						roomType = roomType + "," + reservationRes.getRoomDetails().get(i).getDescription();
						if(reservationRes.getRoomDetails().get(i).getRoomCode().equals("DB"))
						{
							if(!bedType.contains("Double"))
							{
								bedType = bedType + "," + "Double";
							}
						}
						else
						{
							if(!bedType.contains(reservationRes.getRoomDetails().get(i).getRoomCode()))
							{
								bedType = bedType + "," + reservationRes.getRoomDetails().get(i).getRoomCode();
							}
						}
						
						if(reservationRes.getRoomDetails().get(i).getBoard().equals(""))
						{
							if(!ratePlan.contains("RoomOnly"))
							{
								ratePlan = ratePlan + "," +"RoomOnly";
							}
							
						}
						else
						{
							if(!ratePlan.contains(reservationRes.getRoomDetails().get(i).getBoard()))
							{
								ratePlan = ratePlan + "," +reservationRes.getRoomDetails().get(i).getBoard();	
							}
						}
						
						
					}
					else
					{
						if(!roomType.contains(reservationRes.getRoomDetails().get(i).getRoomName()))
						{
							roomType = roomType + "," + reservationRes.getRoomDetails().get(i).getRoomName();
						}
						if(!bedType.contains(reservationRes.getRoomDetails().get(i).getBedType()))
						{
							bedType = bedType + "," + reservationRes.getRoomDetails().get(i).getBedType();
						}
						if(!ratePlan.contains(reservationRes.getRoomDetails().get(i).getBoard()))
						{
							ratePlan = ratePlan + "," +reservationRes.getRoomDetails().get(i).getBoard();
						}	
					}
				}
			}
			printTemplate.verifyTrue(roomType, values.get("roomType"), "Cancellation report values verification - Room type");
			printTemplate.verifyTrue(bedType, values.get("bedType"), "Cancellation report values verification - Bed type");
			printTemplate.verifyTrue(ratePlan, values.get("ratePlan"), "Cancellation report values verification - Rate plan");
			String sellingCurrency = "";
			if(search.getChannel().equals("c"))
			{
				sellingCurrency = "USD";
			}
			else
			{
				if(search.getCountry().equals("Sri Lanka"))
				{
					sellingCurrency = "LKR";
				}
				else
				{
					sellingCurrency = "USD";
				}
			}
			printTemplate.verifyTrue(sellingCurrency, values.get("sellingCurrecny"), "Cancellation report values verification - Selling currency");
			
			CurrencyConverter 	cc 		= new CurrencyConverter();
			TaxCalculator 		addTax 	= new TaxCalculator();
			String netRate 	= cc.convertWithoutRoundingUp(defaultCC, reservationRes.getPrice(), values.get("sellingCurrecny"), currencyMap, reservationRes.getCurrencyCode());
			String pm		= addTax.getTaxWithoutRoundUp(netRate, portalSpecificData.get("hotelpm"), defaultCC, values.get("sellingCurrecny"), currencyMap);
			String bf		= addTax.getTaxWithoutRoundUp(netRate, portalSpecificData.get("hotelbf"), defaultCC, values.get("sellingCurrecny"), currencyMap);
			String ccFee	= addTax.getTaxWithoutRoundUp(netRate, portalSpecificData.get("portal.ccfee"), defaultCC, values.get("sellingCurrecny"), currencyMap, portalSpecificData.get("portal.pgCurrency"));
			String orderValue = String.valueOf(Float.parseFloat(netRate) + Float.parseFloat(pm) + Float.parseFloat(bf) + Float.parseFloat(ccFee));
			String cancellationCharge = "";
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			Date today = dateFormat.parse(Calender.getToday("dd-MMM-yyyy"));
			Date canDay = dateFormat.parse(canDeadline);
			if(today.before(canDay)) //today < after
			{
				cancellationCharge = String.valueOf(Float.parseFloat(ccFee) + Float.parseFloat(bf));
			}
			else
			{
				cancellationCharge = orderValue;
			}
			
			String additionalCharge = String.valueOf(Float.parseFloat(ccFee) + Float.parseFloat(bf));
			printTemplate.verifyTrue(orderValue, values.get("orderValue"), "Cancellation report values verification - Order value");
			printTemplate.verifyTrue(cancellationCharge, values.get("totalCancellationCharge"), "Cancellation report values verification - Total cancellation charge");
			printTemplate.verifyTrue(additionalCharge, values.get("additionalCharge"), "Cancellation report values verification - Additional charge");
			
			netRate 			= cc.convertWithoutRoundingUp(defaultCC, reservationRes.getPrice(), defaultCC, currencyMap, reservationRes.getCurrencyCode());
			pm					= addTax.getTaxWithoutRoundUp(netRate, portalSpecificData.get("hotelpm"), defaultCC, defaultCC, currencyMap);
			bf					= addTax.getTaxWithoutRoundUp(netRate, portalSpecificData.get("hotelbf"), defaultCC, defaultCC, currencyMap);
			ccFee				= addTax.getTaxWithoutRoundUp(netRate, portalSpecificData.get("portal.ccfee"), defaultCC, defaultCC, currencyMap, portalSpecificData.get("portal.pgCurrency"));
			orderValue = String.valueOf(Float.parseFloat(netRate) + Float.parseFloat(pm) + Float.parseFloat(bf) + Float.parseFloat(ccFee));
			if(today.before(canDay)) //today < after
			{
				cancellationCharge = String.valueOf(Float.parseFloat(ccFee) + Float.parseFloat(bf));
			}
			else
			{
				cancellationCharge = orderValue;
			}
			printTemplate.verifyTrue(orderValue, values.get("orderValueInPortalCurrecny"), "Cancellation report values verification - Order value in portal currency");
			printTemplate.verifyTrue(cancellationCharge, values.get("totalCancellationFeeInPortalCurrency"), "Cancellation report values verification - Total cancellation fee in portal currency");
			printTemplate.verifyTrue(orderValue, values.get("orderValuePageTotal"), "Cancellation report values verification - Order value page total");
			printTemplate.verifyTrue(cancellationCharge, values.get("totalCancellationFeePageTotal"), "Cancellation report values verification - Total cancellation fee page total");
			printTemplate.verifyTrue(orderValue, values.get("orderValueGrandTotal"), "Cancellation report values verification - Order value grand total");
			printTemplate.verifyTrue(cancellationCharge, values.get("totalCancellationFeeGrandTotal"), "Cancellation report values verification - Total cancellation fee grand total");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Cancellation report values verification error - " + e);
		}
	}
}
