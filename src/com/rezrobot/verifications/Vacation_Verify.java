package com.rezrobot.verifications;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.CommonBillingInfoRates;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.dataobjects.Vacation_Rate_Details;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;
import com.rezrobot.flight_circuitry.xml_objects.ReservationResponse;
import com.rezrobot.utill.CurrencyConverter;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.TaxCalculator;


public class Vacation_Verify {

	
	
	public void resultsPageVerification(ReportTemplate printTemplate, String defaultCC, HashMap<String, String> currencyMap, HashMap<String, String> portalSpecificDetails, ArrayList<hotelDetails> hotelAvailabilityRes, HashMap<String, String> selectedFlightDetails, HotelDetails selectedHotel, FareResponse fareResponse, Vacation_Search_object search, HashMap<String, String> hotelLabels, PriceResponse priceResponse) throws ParseException
	{
		//flight details verification
		printTemplate.verifyTrue(portalSpecificDetails.get("flightPreferredAirline") + "-" + fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getFlightNo(), 				selectedFlightDetails.get("outflightdepflightcode"), 	"Results page - Flight details - Out flight departure flight code");
		printTemplate.verifyTrue(Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureDate()), 						selectedFlightDetails.get("outflightdepdate"), 			"Results page - Flight details - Out flight departure date");
		printTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureLocationCode(), 																selectedFlightDetails.get("outflightdeplocation"), 		"Results page - Flight details - Out flight departure location");
		String[] time = fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(0).getDepartureTime().split(":");
		printTemplate.verifyTrue(time[0] + ":" + time[1], 																																			selectedFlightDetails.get("outflightdeptime"), 			"Results page - Flight details - Out flight departure time");
		int temp = fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().size();
		printTemplate.verifyTrue(portalSpecificDetails.get("flightPreferredAirline") + "-" + fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(temp-1).getFlightNo(), 			selectedFlightDetails.get("outflightarrflightcode"), 	"Results page - Flight details - Out flight arrival flight code");
		time = fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(temp-1).getArrivalTime().split(":");
		printTemplate.verifyTrue(time[0] + ":" + time[1], 																																			selectedFlightDetails.get("outflightarrtime"), 			"Results page - Flight details - Out flight arrival time");
		printTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(temp-1).getArrivalLocationCode(),															selectedFlightDetails.get("outflightarrlocation"), 		"Results page - Flight details - Out flight arrival location");
		printTemplate.verifyTrue(Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", fareResponse.getList().get(0).getOrigins().get(0).getFlightlist().get(temp-1).getArrivalDate()), 					selectedFlightDetails.get("outflightarrdate"), 			"Results page - Flight details - Out flight arrival date");
		printTemplate.verifyTrue(portalSpecificDetails.get("flightPreferredAirline") + "-" + fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getFlightNo(), 				selectedFlightDetails.get("inbounddepfligthcode"), 		"Results page - Flight details - Inbound flight departure flight code");
		printTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDepartureLocationCode(), 																selectedFlightDetails.get("inbounddeploaction"), 		"Results page - Flight details - Inbound flight departure location");
		printTemplate.verifyTrue(Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDepartureDate()), 						selectedFlightDetails.get("inbounddepdate"), 			"Results page - Flight details - Inbound flight departure date");
		time = fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(0).getDepartureTime().split(":");
		printTemplate.verifyTrue(time[0] + ":" + time[1], 																																			selectedFlightDetails.get("inbounddeptime"), 			"Results page - Flight details - Inbound flight departure time");
		printTemplate.verifyTrue(portalSpecificDetails.get("flightPreferredAirline") + "-" + fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(temp-1).getFlightNo(), 			selectedFlightDetails.get("inboundarrflightcode"), 		"Results page - Flight details - Inbound flight arrival fligh code");
		printTemplate.verifyTrue(fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(temp-1).getArrivalLocationCode(), 															selectedFlightDetails.get("inboundarrlocation"), 		"Results page - Flight details - Inbound flight arrival location");
		printTemplate.verifyTrue(Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(temp-1).getArrivalDate()), 					selectedFlightDetails.get("inboundarrdate"), 			"Results page - Flight details - Inbound flight arrival date");
		time = fareResponse.getList().get(0).getOrigins().get(1).getFlightlist().get(temp-1).getArrivalTime().split(":");
		printTemplate.verifyTrue(time[0] + ":" + time[1], 																																			selectedFlightDetails.get("inboundarrtime"), 			"Results page - Flight details - Inbound flight arrival time");
		
		
		CurrencyConverter 	cc 		= new CurrencyConverter();
		TaxCalculator 		taxcal 	= new TaxCalculator();
		
		//get flight value
		String flightValue = cc.convert(defaultCC, priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareAmount(), selectedHotel.getCurrency(), currencyMap, priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareCurrencyCode());
		String flightpm = taxcal.getTax(flightValue, portalSpecificDetails.get("flightpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
		String flighttax = cc.convert(defaultCC, priceResponse.getPriceinfo().getInSupplierCurrency().getTaxAmount(), selectedHotel.getCurrency(), currencyMap, priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareCurrencyCode());
		
		
		//hotel details verification
		String hotelTitle = "";
		String hotelID = "";
		String hotelDestination = "";
		String starRating = "";
		String hotelValue = "";
		String hotelpm = "";
		ArrayList<String> roomPrices = new ArrayList<>();
		
		
		for(int i = 0 ; i < hotelAvailabilityRes.size() ; i++)
		{
			if(hotelAvailabilityRes.get(i).getHotel().equals(selectedHotel.getTitle()))
			{
				//get room rates
				for(int k = 0 ; k < selectedHotel.getRoomList().size() ; k++)
				{
					for(int j = 0 ; j < hotelAvailabilityRes.get(i).getRoomDetails().size() ; j++)
					{
						if(selectedHotel.getRoomList().get(k).getRoomType().equalsIgnoreCase(hotelAvailabilityRes.get(i).getRoomDetails().get(j).getRoomType()) && selectedHotel.getRoomList().get(k).getRatePlan().equalsIgnoreCase(hotelAvailabilityRes.get(i).getRoomDetails().get(j).getBoard()))
						{
							roomPrices.add(hotelAvailabilityRes.get(i).getRoomDetails().get(j).getPrice());
							break;
						}
					}
				}
				
				
				hotelTitle = hotelAvailabilityRes.get(i).getHotel();
				hotelID = hotelAvailabilityRes.get(i).getHotelCode();
				if(search.getHotelSupplier().equals("INV27"))
				{
					hotelDestination = hotelAvailabilityRes.get(i).getLocation();
				}
				else
				{
					hotelDestination = hotelAvailabilityRes.get(i).getLocationCode();
				}
			
				starRating 	= hotelAvailabilityRes.get(i).getStarRatingCode();
				break;
			}
		}
		String price = "0";
		for(int i = 0 ; i < roomPrices.size() ; i++)
		{
			price = String.valueOf(Float.parseFloat(price) + Float.parseFloat(roomPrices.get(i)));
		}
		hotelValue 	= cc.convert(defaultCC, price, selectedHotel.getCurrency(), currencyMap, hotelAvailabilityRes.get(0).getCurrencyCode());
		hotelpm 	= taxcal.getTax(hotelValue, portalSpecificDetails.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
		
		printTemplate.verifyTrue(hotelTitle, 									selectedHotel.getTitle(), 																			"Results page - Hotel details - Hotel title");
		printTemplate.verifyTrue(hotelID, 										selectedHotel.getHotelID().split("-")[0], 															"Results page - Hotel details - Hotel ID");
		printTemplate.verifyTrue(hotelDestination, 								selectedHotel.getDestination(), 																	"Results page - Hotel details - Destination");
		printTemplate.verifyTrue(search.getHotelSupplier(), 					selectedHotel.getSupplier(), 																		"Results page - Hotel details - Supplier");
		printTemplate.verifyTrue(starRating, 									selectedHotel.getStarRating(), 																		"Results page - Hotel details - Star rating");
		printTemplate.verifyTrue(hotelLabels.get("rpEmailSubject"), 			selectedHotel.getEmailSubject(), 																	"Results page - Hotel details - Email subject");
		printTemplate.verifyTrue(hotelLabels.get("rpEmailBody"), 				selectedHotel.getEmailBody().replace("rezproduction", portalSpecificDetails.get("Portal.Name")), 	"Results page - Hotel details - Email body");
		printTemplate.verifyTrue(hotelLabels.get("rpEmailSentConfirmation"), 	selectedHotel.getEmailStatus(), 																	"Results page - Hotel details - Email status");
		printTemplate.verifyTrue(true, 											selectedHotel.getMoreInfo().isAmanityAvailability(), 												"Results page - Hotel more details availability - Check amenity availability");
		printTemplate.verifyTrue(true, 											selectedHotel.getMoreInfo().isImageAvailability(), 													"Results page - Hotel more details availability - Check images availability");
		printTemplate.verifyTrue(true, 											selectedHotel.getMoreInfo().isMapAvailability(), 													"Results page - Hotel more details availability - Check map availability");
		printTemplate.verifyTrue(true, 											selectedHotel.getMoreInfo().isMoreInfoAvailability(), 												"Results page - Hotel more details availability - Check overview availability");
		String total = String.valueOf(Integer.parseInt(flightValue) + Integer.parseInt(flightpm) + Integer.parseInt(flighttax) + Integer.parseInt(hotelValue) + Integer.parseInt(hotelpm));
		printTemplate.verifyTrue(total,	 										selectedHotel.getRate(), 																			"Results page - Vacation rate"); 
	}
	
	public void paymentPageCartAndMoreDetails(ReportTemplate printTemplate, hotelDetails roomAvailablityRes, PriceResponse priceResponse, Vacation_Search_object search, HashMap<String, String> currencyMap, String defaultCC, HashMap<String, String> portalSpecificDetails, HashMap<String, String> paymentPageCartDetails, HashMap<String, String> paymentPageFlightMoreDetails, HashMap<String, String> paymentPageHotelMoreDetails, String canDeadline) throws ParseException
	{
		String adultCount 		= "0";
		String childCount 		= "0";
		String infantCount 		= "0";
		String roomDetails 		= "";
		String checkin 			= "";
		String checkout 		= "";
		String roomType 		= "";
		String bedType 			= "";
		String ratePlan 		= "";
		CurrencyConverter cc 	= new CurrencyConverter();
		TaxCalculator tax 		= new TaxCalculator();
		
		for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
		{
			if(i == 0)
			{
				roomDetails 	= "Room " + (i+1) + " ( Adults " + search.getAdults().split("/")[i] + " , Child " + search.getChildren().split("/")[i] + " )";
				roomType 		= roomAvailablityRes.getRoomDetails().get(i).getRoomName();
				bedType 		= roomAvailablityRes.getRoomDetails().get(i).getBedType();
				ratePlan 		= roomAvailablityRes.getRoomDetails().get(i).getBoard();
			}
			else
			{
				roomDetails 	= roomDetails 	+ "," + "Room " + (i+1) + " ( Adults " + search.getAdults().split("/")[i] + " , Child " + search.getChildren().split("/")[i] + " )";
				roomType 		= roomType 		+ "," + roomAvailablityRes.getRoomDetails().get(i).getRoomName();
				bedType 		= bedType 		+ "," + roomAvailablityRes.getRoomDetails().get(i).getBedType();
				ratePlan 		= ratePlan 		+ "," + roomAvailablityRes.getRoomDetails().get(i).getBoard();
			}
			adultCount 			= String.valueOf(Integer.parseInt(adultCount) + Integer.parseInt(search.getAdults().split("/")[i]));
			childCount 			= String.valueOf(Integer.parseInt(childCount) + Integer.parseInt(search.getChildren().split("/")[i]));
			infantCount 		= String.valueOf(Integer.parseInt(infantCount) + Integer.parseInt(search.getInfants().split("/")[i]));
			
		}
		printTemplate.verifyTrue(adultCount, 						paymentPageFlightMoreDetails.get("adultCount"), 				"Payment page - Cart and more details - Flight - Adult count");
		printTemplate.verifyTrue(childCount, 						paymentPageFlightMoreDetails.get("childCount"), 				"Payment page - Cart and more details - Flight - Child count");
		if(!search.getInfants().split("/")[0].equals("0"))
		{
			printTemplate.verifyTrue(infantCount, 						paymentPageFlightMoreDetails.get("infantCount"), 				"Payment page - Cart and more details - Flight - Infant count");
		}
		printTemplate.verifyTrue(roomDetails, 						paymentPageHotelMoreDetails.get("roomDetails"), 				"Payment page - Cart and more details - Hotel - Room details");
		if(search.getHotelSupplier().equals("hotelbeds_v2"))
		{
			checkin 	= Calender.getDate("yyyyMMdd", "MMM dd yyyy", roomAvailablityRes.getCheckin());
			checkout 	= Calender.getDate("yyyyMMdd", "MMM dd yyyy", roomAvailablityRes.getCheckout());
		}
		else
		{
			checkin 	= roomAvailablityRes.getCheckin();
			checkout 	= roomAvailablityRes.getCheckout();
		}
		printTemplate.verifyTrue(checkin, 							paymentPageHotelMoreDetails.get("checkin"), 					"Payment page - Cart and more details - Hotel - Checkin");
		printTemplate.verifyTrue(checkout, 							paymentPageHotelMoreDetails.get("checkout"), 					"Payment page - Cart and more details - Hotel - Checkout");
		printTemplate.verifyTrue(roomType, 							paymentPageHotelMoreDetails.get("roomType"), 					"Payment page - Cart and more details - Hotel - Room type");
		printTemplate.verifyTrue(bedType, 							paymentPageHotelMoreDetails.get("bedType"), 					"Payment page - Cart and more details - Hotel - Bed type");
		printTemplate.verifyTrue(ratePlan, 							paymentPageHotelMoreDetails.get("rateType"), 					"Payment page - Cart and more details - Hotel - Rate type");
		printTemplate.verifyTrue(Calender.getDate("dd-MMM-yyy", "MMM dd yyyy", canDeadline), 						paymentPageHotelMoreDetails.get("cancellationDeadline"), 		"Payment page - Cart and more details - Hotel - Cancellation policy");
		
		String hotelValue 											= cc.convert(defaultCC,		roomAvailablityRes.getPrice(), 			paymentPageCartDetails.get("sellingCurrency"), 				currencyMap, roomAvailablityRes.getCurrencyCode());
		String hotelPm												= tax.getTax(hotelValue, 	portalSpecificDetails.get("hotelpm"), 	defaultCC, paymentPageCartDetails.get("sellingCurrency"), 	currencyMap);
 		String hotelBf												= tax.getTax(hotelValue, 	portalSpecificDetails.get("hotelbf"), 	defaultCC, paymentPageCartDetails.get("sellingCurrency"), 	currencyMap);
		String flightValue											= cc.convert(defaultCC, 	priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareAmount(), paymentPageCartDetails.get("sellingCurrency"), currencyMap, priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareCurrencyCode());
 		String flightPm												= tax.getTax(flightValue, 	portalSpecificDetails.get("flightpm"), 	defaultCC, paymentPageCartDetails.get("sellingCurrency"), 	currencyMap);
 		String flightBf												= tax.getTax(flightValue, 	portalSpecificDetails.get("flightbf"), 	defaultCC, paymentPageCartDetails.get("sellingCurrency"), 	currencyMap);
 		String flightTax											= cc.convert(defaultCC, 	priceResponse.getPriceinfo().getInSupplierCurrency().getTaxAmount(), paymentPageCartDetails.get("sellingCurrency"), currencyMap, priceResponse.getPriceinfo().getInSupplierCurrency().getTaxCurrencyCode());

		String subtotal 											= String.valueOf(Integer.parseInt(hotelValue) + Integer.parseInt(flightValue) + Integer.parseInt(hotelPm) + Integer.parseInt(flightPm));
		String taxAndOtherCharges 									= String.valueOf(Integer.parseInt(flightTax) + Integer.parseInt(flightBf) + Integer.parseInt(hotelBf));
		String totalPackageCost 									= String.valueOf(Integer.parseInt(subtotal) + Integer.parseInt(taxAndOtherCharges));
		String amountBeingProcessedNow 								= totalPackageCost;
		String amountBeingProcessedByAirline 						= "0";
		String amountDueAtCheckin 									= "0";
		
 		printTemplate.verifyTrue(subtotal, 							paymentPageCartDetails.get("subtotal"), 						"Payment page - Cart and more details - Cart - Subtotal");
		printTemplate.verifyTrue(taxAndOtherCharges, 				paymentPageCartDetails.get("taxAndOtherCharges"), 				"Payment page - Cart and more details - Cart - Tax and other charges");
		printTemplate.verifyTrue(totalPackageCost, 					paymentPageCartDetails.get("totalPackageCost"), 				"Payment page - Cart and more details - Cart - Total package cost");
		printTemplate.verifyTrue(amountBeingProcessedNow, 			paymentPageCartDetails.get("amountBeingProcessedNow"), 			"Payment page - Cart and more details - Cart - Amount being processed now");
		printTemplate.verifyTrue(amountBeingProcessedByAirline, 	paymentPageCartDetails.get("amountBeingProcessedByAirline"), 	"Payment page - Cart and more details - Cart - Amount being processed by the airline");
		printTemplate.verifyTrue(amountDueAtCheckin, 				paymentPageCartDetails.get("amountDueAtChecckin"), 				"Payment page - Cart and more details - Cart - Amnount due t checkin");
		
	}
	
	public void paymentPageBillingInfo(HashMap<String, String> portalSpecificDetails, HashMap<String, String> currencyMap, String defaultCC, CommonBillingInfoRates rates, hotelDetails roomAvailablityRes, PriceResponse priceResponse, ReportTemplate printTemplate)
	{
		CurrencyConverter cc 					= new CurrencyConverter();
		TaxCalculator tax 						= new TaxCalculator();
		
		String hotelValue 						= cc.convert(defaultCC,		roomAvailablityRes.getPrice(), 			rates.getCurrency(), 				currencyMap, roomAvailablityRes.getCurrencyCode());
		String hotelPm							= tax.getTax(hotelValue, 	portalSpecificDetails.get("hotelpm"), 	defaultCC, rates.getCurrency(), 	currencyMap);
 		String hotelBf							= tax.getTax(hotelValue, 	portalSpecificDetails.get("hotelbf"), 	defaultCC, rates.getCurrency(), 	currencyMap);
		String flightValue						= cc.convert(defaultCC, 	priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareAmount(), 	rates.getCurrency(), currencyMap, priceResponse.getPriceinfo().getInSupplierCurrency().getBasefareCurrencyCode());
 		String flightPm							= tax.getTax(flightValue, 	portalSpecificDetails.get("flightpm"), 	defaultCC, rates.getCurrency(), 	currencyMap);
 		String flightBf							= tax.getTax(flightValue, 	portalSpecificDetails.get("flightbf"), 	defaultCC, rates.getCurrency(), 	currencyMap);
 		String flightTax						= cc.convert(defaultCC, 	priceResponse.getPriceinfo().getInSupplierCurrency().getTaxAmount(), 		rates.getCurrency(), currencyMap, priceResponse.getPriceinfo().getInSupplierCurrency().getTaxCurrencyCode());
 		
 		String subtotal 						= String.valueOf(Integer.parseInt(hotelValue) + Integer.parseInt(flightValue) + Integer.parseInt(hotelPm) + Integer.parseInt(flightPm));
		String taxAndOtherCharges 				= String.valueOf(Integer.parseInt(flightTax) + Integer.parseInt(flightBf) + Integer.parseInt(hotelBf));
		String totalPackageCost 				= String.valueOf(Integer.parseInt(subtotal) + Integer.parseInt(taxAndOtherCharges));
		String amountBeingProcessedNow 			= totalPackageCost;
 		
 		printTemplate.verifyTrue(subtotal, 					rates.getSubtotal(), 						"Payment page - Billing info - Sub total");
 		printTemplate.verifyTrue(taxAndOtherCharges, 		rates.getTotalTaxAndFees(), 				"Payment page - Billing info - Tota taxes and other fees");
 		printTemplate.verifyTrue(totalPackageCost, 			rates.getTotal(), 							"Payment page - Billing info - Total");
 		printTemplate.verifyTrue(amountBeingProcessedNow, 	rates.getAmountBeingProcessedNow(), 		"Payment page - Billing info - Amount being processed now");
	}
	
	public Vacation_Rate_Details confirmationPage(HashMap<String, String> values, hotelDetails hotelRes, ReservationResponse flightRes, HashMap<String, String> portalSpecificDetails, HashMap<String, String> currencyMap, Vacation_Search_object search, String defaultCC, ReportTemplate printTemplate, ArrayList<HashMap<String, String>> websiteDetails, HashMap<String, String> paymentDetails, String canDeadline)
	{
		Vacation_Rate_Details rates = new Vacation_Rate_Details();
		CurrencyConverter			cc 					= new CurrencyConverter();
		TaxCalculator 				tax 				= new TaxCalculator();
		String 				hotelValue	= "";
		String 				hotelpm		= "";
		String 				hotelbf		= "";
		String				flightValue	= "";
		String 				flightTax	= "";
		String				flightpm	= "";
		String				flightbf	= "";
		
		
		String 				subTotal	= "";
		String				ccfee		= "";
		String				totalTaxes	= "";
		String				total		= "";
		try {
			hotelValue					= cc.convert(defaultCC, 	hotelRes.getPrice(), 					values.get("sellingCurrency"), currencyMap, hotelRes.getCurrencyCode());
			hotelpm						= tax.getTax(hotelValue, 	portalSpecificDetails.get("hotelpm"), 	defaultCC, values.get("sellingCurrency"), currencyMap);
			hotelbf						= tax.getTax(hotelValue, 	portalSpecificDetails.get("hotelbf"), 	defaultCC, values.get("sellingCurrency"), currencyMap);
			flightValue					= cc.convert(defaultCC, 	flightRes.getFare().getInSupplierCurrency().getBasefareAmount(), values.get("sellingCurrency"), currencyMap, flightRes.getFare().getInSupplierCurrency().getBasefareCurrencyCode());
			flightTax					= cc.convert(defaultCC, 	flightRes.getFare().getInSupplierCurrency().getTaxAmount(), values.get("sellingCurrency"), currencyMap, flightRes.getFare().getInSupplierCurrency().getBasefareCurrencyCode());
			flightpm					= tax.getTax(hotelValue, 	portalSpecificDetails.get("flightpm"), 	defaultCC, values.get("sellingCurrency"), currencyMap);
			flightbf					= tax.getTax(hotelValue, 	portalSpecificDetails.get("flightbf"), 	defaultCC, values.get("sellingCurrency"), currencyMap);
			
			subTotal					= String.valueOf(Integer.parseInt(hotelValue) + Integer.parseInt(flightValue) + Integer.parseInt(hotelpm) + Integer.parseInt(flightpm));
			ccfee						= tax.getTax(subTotal, portalSpecificDetails.get("portal.ccfee"), defaultCC, values.get("sellingCurrency"), currencyMap, portalSpecificDetails.get("portal.pgCurrency"));
			totalTaxes					= String.valueOf(Integer.parseInt(flightTax) + Integer.parseInt(hotelbf) + Integer.parseInt(flightbf));
			total						= String.valueOf(Integer.parseInt(subTotal) + Integer.parseInt(totalTaxes) + Integer.parseInt(ccfee));
			
			//rates in selling currency
			rates.setHotelValueInSellingCurrency	(hotelValue);
			rates.setHotelpmInSellingCurrency		(hotelpm);
			rates.setHotelbfInSellingCurrency		(hotelbf);
			rates.setFlightValueInSellingCurrency	(flightValue);
			rates.setFlightTaxInSellingCurrency		(flightTax);
			rates.setFlightpmInSellingCurrency		(flightpm);
			rates.setFlightbfInSellingCurrency		(flightbf);
			rates.setCcfeeInSellingCurrency			(ccfee);
			rates.setSubTotalInSellingCurrency		(subTotal);
			rates.setTotalInSellingCurrency			(total);
			rates.setTaxInSellingCurrency			(totalTaxes);
			rates.setCurrencyInSellingCurrency(values.get("sellingCurrency"));
			
			//rates in portal currency
			rates.setHotelValueInPortalCurrency		(cc.convert(defaultCC, hotelValue, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setHotelpmInPortalCurrency		(cc.convert(defaultCC, hotelpm, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setHotelbfInPortalCurrency		(cc.convert(defaultCC, hotelbf, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setFlightValueInPortalCurrency	(cc.convert(defaultCC, flightValue, defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setFlightTaxInPortalCurrency		(cc.convert(defaultCC, flightTax, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setFlightpmInPortalCurrency		(cc.convert(defaultCC, flightpm, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setFlightbfInPortalCurrency		(cc.convert(defaultCC, flightbf, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setCcfeeInPortalCurrency			(cc.convert(defaultCC, ccfee, 		defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setSubTotalInPortalCurrency		(cc.convert(defaultCC, subTotal, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setTotalInPortalCurrency			(cc.convert(defaultCC, total, 		defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setTaxInPortalCurrency			(cc.convert(defaultCC, totalTaxes, 	defaultCC, currencyMap, values.get("sellingCurrency")));
			rates.setCurrencyInPortalCurrency		(defaultCC);
			
			//rates in pg currency
			String pgCurrency = "";
			if(portalSpecificDetails.get("PaymentGateway_CardType").equals("rezPay"))
			{
				pgCurrency = portalSpecificDetails.get("portal.pgCurrency");
			}
			else
			{
				pgCurrency = values.get("sellingCurrency");
			}
			rates.setHotelValueInPGCurrency			(cc.convert(defaultCC, hotelValue, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setHotelpmInPGCurrency			(cc.convert(defaultCC, hotelpm, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setHotelbfInPGCurrency			(cc.convert(defaultCC, hotelbf, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setFlightValueInPGCurrency		(cc.convert(defaultCC, flightValue, pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setFlightTaxInPGCurrency			(cc.convert(defaultCC, flightTax, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setFlightpmInPGCurrency			(cc.convert(defaultCC, flightpm, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setFlightbfInPGCurrency			(cc.convert(defaultCC, flightbf, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setCcfeeInPGCurrency				(cc.convert(defaultCC, ccfee, 		pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setSubTotalInPGCurrency			(cc.convert(defaultCC, subTotal, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setTotalInPGCurrency				(cc.convert(defaultCC, total, 		pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setTaxInPGCurrency				(cc.convert(defaultCC, totalTaxes, 	pgCurrency, currencyMap, values.get("sellingCurrency")));
			rates.setCurrencyInPGCurrency			(pgCurrency);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		try {
			printTemplate.verifyTrue("Mr " + portalSpecificDetails.get("cd.firstname") + " " + portalSpecificDetails.get("cd.lastname"), 	values.get("bookingReference"), 	"Confirmation page - Booking reference");
			printTemplate.verifyTrue(" Tel : " + websiteDetails.get(0).get("telephone"), 													values.get("portalTelNumber"), 		"Confirmation page - Telephone number");
			printTemplate.verifyTrue(" Fax : " + websiteDetails.get(0).get("fax"), 															values.get("portalFax"), 			"Confirmation page - Fax number");
			printTemplate.verifyTrue(flightRes.getItems().get(0).getTPAExt_ConfirmationNumber(), 											values.get("flightSupConNumber"), 	"Confirmation page - Flight supplier confirmation number");
			printTemplate.verifyTrue(flightRes.getItems().get(0).getFlight().getDeparture_port(), 											values.get("flightDepartureCity"), 	"Confirmation page - Flight departure city");
			printTemplate.verifyTrue(flightRes.getItems().get(flightRes.getItems().size()-1).getFlight().getArrival_port(), 				values.get("flightArrivalCity"), 	"Confirmation page - Flight arrival city");
			String adultCount 	= "0";
			String childCount 	= "0";
			String infantCount 	= "0";
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				adultCount 	= String.valueOf(Integer.parseInt(adultCount) + Integer.parseInt(search.getAdults().split("/")[i]));
				childCount 	= String.valueOf(Integer.parseInt(childCount) + Integer.parseInt(search.getChildren().split("/")[i]));
				infantCount = String.valueOf(Integer.parseInt(infantCount) + Integer.parseInt(search.getInfants().split("/")[i]));
			}
			printTemplate.verifyTrue(adultCount, values.get("flightNumOfAdults"), "Confirmation page - Flight adult count");
			printTemplate.verifyTrue(childCount, values.get("flightNumOfChildren"), "Confirmation page - Flight child count");
			
			int count = 1;
			if(flightRes.getItems().size()==4)
			{
				count = 2;
			}
			String date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(0).getFlight().getDepartureDate());
			printTemplate.verifyTrue(date, values.get("outBoundDepdateTime"), "Confirmation page - Outbound departure date 1");
			date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(0).getFlight().getArrivalDate());
			printTemplate.verifyTrue(date, values.get("outBoundArrdatetime"), "Confirmation page - Outbound arrival date 1");
			date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(count).getFlight().getDepartureDate());
			printTemplate.verifyTrue(date, values.get("inDepartureDateTime"), "Confirmation page - Inbound departure date 1");
			date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(count).getFlight().getArrivalDate());
			printTemplate.verifyTrue(date, values.get("inArrivalDateTime"), "Confirmation page - Inbound arrival date 1");
			
			String outDepLoc1 	= flightRes.getItems().get(0).getFlight().getDeparture_port() + " - (" + flightRes.getItems().get(0).getFlight().getDepartureLocationCode() + ")"; 
			String outArrloc1 	= flightRes.getItems().get(0).getFlight().getArrival_port() + " - (" + flightRes.getItems().get(0).getFlight().getArrivalLocationCode() + ")"; 
			String inDepLoc1 	= flightRes.getItems().get(count).getFlight().getDeparture_port() + " - (" + flightRes.getItems().get(count).getFlight().getDepartureLocationCode() + ")"; 
			String inArrLoc1 	= flightRes.getItems().get(count).getFlight().getArrival_port() + " - (" + flightRes.getItems().get(count).getFlight().getArrivalLocationCode() + ")"; 
			printTemplate.verifyTrue(outDepLoc1, values.get("outbounddeplocation"), "Confirmation page - Outbound departure location 1");
			printTemplate.verifyTrue(outArrloc1, values.get("outboundarrlocation"), "Confirmation page - Outbound arrival location 1");
			printTemplate.verifyTrue(inDepLoc1, values.get("inbounddeplocation"), "Confirmation page - Inbound departure location 1");
			printTemplate.verifyTrue(inArrLoc1, values.get("inboundarrlocation"), "Confirmation page - Inbound arrival location 1");
			
			String outAir1 = flightRes.getItems().get(0).getFlight().getOperatingAirline() + " (" + flightRes.getItems().get(0).getFlight().getMarketingAirline_Loc_Code() + " "  + flightRes.getItems().get(0).getFlight().getFlightNo() + ")";
			String outAir2 = flightRes.getItems().get(1).getFlight().getOperatingAirline() + " (" + flightRes.getItems().get(1).getFlight().getMarketingAirline_Loc_Code() + " " + flightRes.getItems().get(1).getFlight().getFlightNo() + ")";
			printTemplate.verifyTrue(outAir1, values.get("outboundairline"), "Confirmation page - Outbound airline 1");
			printTemplate.verifyTrue(outAir2, values.get("outboundairline2"), "Confirmation page - Outbound airline 2");
			
			if(flightRes.getItems().size()==4)
			{
				String Date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(1).getFlight().getDepartureDate());
				printTemplate.verifyTrue(Date, values.get("outBoundDepdateTime2"), "Confirmation page - Outbound departure date 2");
				Date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(1).getFlight().getArrivalDate());
				printTemplate.verifyTrue(Date, values.get("outBoundArrdatetime2"), "Confirmation page - Outbound arrival date 2");
				Date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(3).getFlight().getDepartureDate());
				printTemplate.verifyTrue(Date, values.get("inDepartureDateTime2"), "Confirmation page - Inbound departure date 2");
				Date = Calender.getDate("yyyy-MM-dd", "MMM dd yyyy", flightRes.getItems().get(3).getFlight().getArrivalDate());
				printTemplate.verifyTrue(Date, values.get("inArrivalDateTime2"), "Confirmation page - Inbound arrival date 2");
				
				outDepLoc1 	= flightRes.getItems().get(1).getFlight().getDeparture_port() + " - (" + flightRes.getItems().get(1).getFlight().getDepartureLocationCode() + ")"; 
				outArrloc1 	= flightRes.getItems().get(1).getFlight().getArrival_port() + " - (" + flightRes.getItems().get(1).getFlight().getArrivalLocationCode() + ")"; 
				inDepLoc1 	= flightRes.getItems().get(3).getFlight().getDeparture_port() + " - (" + flightRes.getItems().get(3).getFlight().getDepartureLocationCode() + ")"; 
				inArrLoc1 	= flightRes.getItems().get(3).getFlight().getArrival_port() + " - (" + flightRes.getItems().get(3).getFlight().getArrivalLocationCode() + ")"; 
				printTemplate.verifyTrue(outDepLoc1, values.get("outbounddeplocation2"), "Confirmation page - Outbound departure location 2");
				printTemplate.verifyTrue(outArrloc1, values.get("outboundarrlocation2"), "Confirmation page - Outbound arrival location 2");
				printTemplate.verifyTrue(inDepLoc1, values.get("inbounddeplocation2"), "Confirmation page - Inbound departure location 2");
				printTemplate.verifyTrue(inArrLoc1, values.get("inboundarrlocation2"), "Confirmation page - Inbound arrival location 2");
			
				outAir1 = flightRes.getItems().get(2).getFlight().getOperatingAirline() + " (" + flightRes.getItems().get(2).getFlight().getMarketingAirline_Loc_Code() + " " + flightRes.getItems().get(2).getFlight().getFlightNo() + ")";
				outAir2 = flightRes.getItems().get(3).getFlight().getOperatingAirline() + " (" + flightRes.getItems().get(3).getFlight().getMarketingAirline_Loc_Code() + " " + flightRes.getItems().get(3).getFlight().getFlightNo() + ")";
				printTemplate.verifyTrue(outAir1, values.get("inboundairline"), "Confirmation page - Inbound airline 1");
				printTemplate.verifyTrue(outAir2, values.get("inboundairline2"), "Confirmation page - Inbound airline 2");
			}
			
			printTemplate.verifyTrue(hotelRes.getHotel(), values.get("hotelName"), "Confirmation page - Hotel name");
			printTemplate.verifyTrue(hotelRes.getAvailabilityStatus(), values.get("hotelBookingStatus"), "Confirmation page - Hotel booking status");
			printTemplate.verifyTrue(hotelRes.getSupConfirmationNumber(), values.get("hotelSupConNumber"), "Confirmation page - Hotel supplier confirmation number");
			date = Calender.getDate("yyyyMMdd", "MMM dd yyyy", hotelRes.getCheckin());
			printTemplate.verifyTrue(date, values.get("checkin"), "Confirmation page - Hotel checkin date");
			date = Calender.getDate("yyyyMMdd", "MMM dd yyyy", hotelRes.getCheckout());
			printTemplate.verifyTrue(date, values.get("checkout"), "Confirmation page - Hotel checkout date");
			printTemplate.verifyTrue(hotelRes.getRoomCount(), values.get("numberOfRooms"), "Confirmation page - Hotel number of rooms");
			printTemplate.verifyTrue(hotelRes.getNights(), values.get("numberOfNights"), "Confirmation page - Hotel number of nights");
			printTemplate.verifyTrue("00:00", values.get("estimatedArrivalTime"), "Confirmation page - Hotel estimated arrival time");
			String roomDetails = "";
			String roomType = "";
			String bedType = "";
			String ratePlan = "";
			for(int i = 0 ; i < Integer.parseInt(search.getRooms()) ; i++)
			{
				if(i == 0)
				{
					roomDetails = "Room" + (i+1) + "(Adults" + search.getAdults().split("/")[i] + ", " + "Children" + search.getChildren().split("/")[i] + ")";
					roomType	= hotelRes.getRoomDetails().get(i).getRoomName();
					bedType		= hotelRes.getRoomDetails().get(i).getBedType();
					ratePlan	= hotelRes.getRoomDetails().get(i).getBoard();
				}
				else
				{
					roomDetails = roomDetails + "," + "Room" + (i+1) + " (Adults" + search.getAdults().split("/")[i] + ", " + "Children" + search.getChildren().split("/")[i] + ")";
					roomType	= roomType + "," + hotelRes.getRoomDetails().get(i).getRoomName();
					bedType		= bedType + "," + hotelRes.getRoomDetails().get(i).getBedType();
					ratePlan	= ratePlan + "," + hotelRes.getRoomDetails().get(i).getBoard();
				}
			}
			printTemplate.verifyTrue(roomDetails, values.get("hotelRooms"), "Confirmation page - Hotel room details");
			printTemplate.verifyTrue(roomType, values.get("hotelRoomType"), "Confirmation page - Hotel room type");
			printTemplate.verifyTrue(bedType, values.get("hotelBedType"), "Confirmation page - Hotel bed type");
			printTemplate.verifyTrue(ratePlan, values.get("hotelRatePlan"), "Confirmation page - Hotel rate plan");
			
			
			printTemplate.verifyTrue(subTotal, values.get("subtotal"), "Confirmation page - Subtotal");
			printTemplate.verifyTrue(totalTaxes, values.get("totalTaxes"), "Confirmation page - Total tax");
			printTemplate.verifyTrue(total, values.get("totalPackageCost"), "Confirmation page - Total package cost");
			printTemplate.verifyTrue(total, values.get("amountBnProcessednow"), "Confirmation page - Amount being processed now");
			printTemplate.verifyTrue("0.00", values.get("amountProcessedByAirline"), "Confirmation page - Amount being processed by airline");
			printTemplate.verifyTrue("0.00", values.get("amountDurAtCheckin"), "Confirmation page - Amount due at checkin");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.firstname"), values.get("customerFirstname"), "Confirmation page - Customer first name");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.lastname"), values.get("customerLastName"), "Confirmation page - Customer last name");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.address"), values.get("customerAddressId"), "Confirmation page - Customer address");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.country"), values.get("customerCountryId"), "Confirmation page - Costomer country");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.postalcode"), values.get("customerPostalCode"), "Confirmation page - Customer postal code");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.phoneNumberCode") + "-" + portalSpecificDetails.get("cd.phoneNumber"), values.get("customerEmergencyNumber"), "Confirmation page - Customer emergency number");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.city"), values.get("cusotmerCity"), "Confirmation page - Customer city");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.state"), values.get("customerState"), "Confirmation page - Customer state");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.phoneNumberCode") + "-" + portalSpecificDetails.get("cd.phoneNumber"), values.get("customerPhoneNumber"), "Confirmation page - Customer phone number");
			printTemplate.verifyTrue(portalSpecificDetails.get("cd.vacationEmail"), values.get("customerEmail"), "Confirmation page - Customer email");
			
			System.out.println(values.get("paymentId"));
			printTemplate.verifyTrue(paymentDetails.get("merchantTrackId"), values.get("merchantTrackId"), "Confirmation page - Merchant track id");
			printTemplate.verifyTrue(paymentDetails.get("authId"), values.get("authenticationReference"), "Confirmation page - Authentication reference");
			printTemplate.verifyTrue(paymentDetails.get("paymentReference"), values.get("paymentId"), "Confirmation page - Payment id");
			String pgCurrency = portalSpecificDetails.get("portal.pgCurrency");
			if(portalSpecificDetails.get("PaymentGateway_CardType").equals("wirecard"))
			{
				pgCurrency = values.get("sellingCurrency");
			}
			
			String 				totalInPG	= cc.convert(defaultCC, total, pgCurrency, currencyMap, values.get("sellingCurrency"));
			printTemplate.verifyTrue(totalInPG, values.get("paymentAmount"), "Confirmation page - PAyment amount");
			printTemplate.verifyTrue("The last date to cancel this hotel is : " + Calender.getDate("dd-MMM-yyyy", "MMM dd yyyy", canDeadline), values.get("hotelCandeadline"), "Confirmation page - Hotel cancellation deadline");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar d = Calendar.getInstance();
			System.out.println(flightRes.getTickettimelimit());
			d.setTime(sdf.parse(flightRes.getTickettimelimit()));
			d.add(Calendar.DATE, -3);
			String flightCanDead = sdf.format(d.getTime());
			printTemplate.verifyTrue(flightCanDead, values.get("flightCanDeadline"), "Confirmation page - Flight cancellation deadline");
			System.out.println(canDeadline);
			System.out.println(flightCanDead);
			String packageCanDeadline = "";
			
			printTemplate.verifyTrue("", values.get("packageCanDealine"), "Confirmation page - Package cancellation deadline");
			rates.setHotelCanDeadline(canDeadline);
			rates.setFlightCanDeadline(flightCanDead);
			rates.setCanDeadline(packageCanDeadline);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return rates;
	}
	
	public void reservationReportCriteriaButtonsAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaButtons)
	{
//		printTemplate.setTableHeading("Reservation report criteria buttons availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaButtons.get("posLocation"), 	"Reservation report criteria buttons availability - Pos Location");
			printTemplate.verifyTrue(true, resReportCriteriaButtons.get("view"), 			"Reservation report criteria buttons availability - View");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaComboboxAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaCombobox)
	{
//		printTemplate.setTableHeading("Reservation report criteria combobox availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("dateFilter"), 		"Reservation report criteria combobox availability - Date filter");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportFromDate"), 	"Reservation report criteria combobox availability - Report from date");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportFromMonth"), 	"Reservation report criteria combobox availability - Report from month");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportFromYear"), 	"Reservation report criteria combobox availability - Report from year");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportToDate"), 		"Reservation report criteria combobox availability - Report to date");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportToMonth"), 		"Reservation report criteria combobox availability - Report to month");
			printTemplate.verifyTrue(true, resReportCriteriaCombobox.get("reportToYear"), 		"Reservation report criteria combobox availability - Report to year");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaLabelsAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaLabels)
	{
//		printTemplate.setTableHeading("Reservation report criteria labels availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productType"), 					"Reservation report criteria labels availability - Product Type");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypeAll"), 				"Reservation report criteria labels availability - Product Type - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypeHotel"), 			"Reservation report criteria labels availability - Product Type - Hotel");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("ProductTypeAir"), 				"Reservation report criteria labels availability - Product Type - Air");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypeCar"), 				"Reservation report criteria labels availability - Product Type - Car");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("prodctTypeActivity"), 			"Reservation report criteria labels availability - Product Type - Activity");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("productTypePackage"), 			"Reservation report criteria labels availability - Product Type - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchBy"), 					"Reservation report criteria labels availability - Search By");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchByDate"), 				"Reservation report criteria labels availability - Search By - Date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchByReservationNumber"), 	"Reservation report criteria labels availability - Search By - Reservation number");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchbyGuestName"), 			"Reservation report criteria labels availability - Search By - Guest name");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("searchbyXmlPackageReference"), 	"Reservation report criteria labels availability - Search By - Xml package reference");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("dateFilter"), 					"Reservation report criteria labels availability - Date filter");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("reportFrom"), 					"Reservation report criteria labels availability - Report from date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("reportTo"), 					"Reservation report criteria labels availability - Report to date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedOn"), 				"Reservation report criteria labels availability - Dates based on");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedonReservationDate"), 	"Reservation report criteria labels availability - Dates based on - Reservation date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedOnArrivalDate"), 		"Reservation report criteria labels availability - Dates based on - Arrival date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("datesBasedOnDueDate"), 			"Reservation report criteria labels availability - Dates based on - Due date");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApproved"), 				"Reservation report criteria labels availability - Payment approved");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApprovedAll"), 			"Reservation report criteria labels availability - Payment approved - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApprovedYes"), 			"Reservation report criteria labels availability - Payment approved - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("paymentApprovedNo"), 			"Reservation report criteria labels availability - Payment approved - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssued"), 				"Reservation report criteria labels availability - Invoice issued");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssuedAll"), 			"Reservation report criteria labels availability - Invoice issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssuedYes"), 			"Reservation report criteria labels availability - Invoice issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("invoiceIssuedNo"), 				"Reservation report criteria labels availability - Invoice issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssued"), 				"Reservation report criteria labels availability - Voucher issued");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssuedAll"), 			"Reservation report criteria labels availability - Voucher issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssuedYes"), 			"Reservation report criteria labels availability - Voucher issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("voucherIssuedNo"), 				"Reservation report criteria labels availability - Voucher issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequired"), 					"Reservation report criteria labels availability - Lpo required");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequiredYes"), 				"Reservation report criteria labels availability - Lpo required - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequiredAll"), 				"Reservation report criteria labels availability - Lpo required - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("lpoRequiredNo"), 				"Reservation report criteria labels availability - Lpo required - No");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannel"), 				"Reservation report criteria labels availability - Booking channel");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannelAll"), 			"Reservation report criteria labels availability - Booking channel - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannelCC"), 			"Reservation report criteria labels availability - Booking channel - CC");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("bookingChannelWeb"), 			"Reservation report criteria labels availability - Booking channel - Web");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partner"), 						"Reservation report criteria labels availability - Partner");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerAll"), 					"Reservation report criteria labels availability - Partner - All");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerDirectConsumer"), 		"Reservation report criteria labels availability - Partner - Direct Consumer");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerAffiliate"), 			"Reservation report criteria labels availability - Partner - Affiliate");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("partnerB2bPartner"), 			"Reservation report criteria labels availability - Partner - B2B partner");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("posLocation"), 					"Reservation report criteria labels availability - Pos location");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("generatePDF"), 					"Reservation report criteria labels availability - Generate PDF");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("generatePDFYes"), 				"Reservation report criteria labels availability - Generate PDF - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaLabels.get("generatePDFNo"), 				"Reservation report criteria labels availability - Generate PDF - No");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaRadioButtonsAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> resReportCriteriaRadioButtons)
	{
//		printTemplate.setTableHeading("Reservation report criteria Radio buttons availability");
		try {
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeAll"), 				"Reservation report criteria Radio buttons availability - Product type - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeHotel"), 				"Reservation report criteria Radio buttons availability - Product type - Hotel");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeAir"), 				"Reservation report criteria Radio buttons availability - Product type - Air");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeActivity"), 			"Reservation report criteria Radio buttons availability - Product type - Activity");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypeCar"), 				"Reservation report criteria Radio buttons availability - Product type - Car");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("productTypePackage"), 			"Reservation report criteria Radio buttons availability - Product type - Package");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("searchByDates"), 					"Reservation report criteria Radio buttons availability - Search by - Date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("searchByReservations"), 			"Reservation report criteria Radio buttons availability - Search by - reservation number");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("searchByGuestName"), 				"Reservation report criteria Radio buttons availability - Search by - guest name");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("SearchByXmlPackageRef"), 			"Reservation report criteria Radio buttons availability - Search by - xml package reference");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("datesBasedOnReservationDate"), 	"Reservation report criteria Radio buttons availability - Dates based on - Reservation date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("datesBasedOnArrivalDate"), 		"Reservation report criteria Radio buttons availability - Dates based on - Arrival date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("datesBasedOnDueDate"), 			"Reservation report criteria Radio buttons availability - Dates based on - Due date");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("paymentApprovedAll"), 			"Reservation report criteria Radio buttons availability - Payment approved - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("paymentApprovedYes"), 			"Reservation report criteria Radio buttons availability - Payment approved - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("paymentApprovedNo"), 				"Reservation report criteria Radio buttons availability - Payment approved - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("invoiceIssuedAll"), 				"Reservation report criteria Radio buttons availability - Invoice issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("invoiceIssuedYes"), 				"Reservation report criteria Radio buttons availability - Invoice issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("invoiceIssuedNo"), 				"Reservation report criteria Radio buttons availability - Invoice issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("voucherIssuedAll"), 				"Reservation report criteria Radio buttons availability - Voucher issued - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("voucherIssuedYes"), 				"Reservation report criteria Radio buttons availability - Voucher issued - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("voucherIssuedNo"), 				"Reservation report criteria Radio buttons availability - Voucher issued - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("lpoAll"), 						"Reservation report criteria Radio buttons availability - Lpo required - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("lpoYes"),		 					"Reservation report criteria Radio buttons availability - Lpo required - Yes");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("lpoNo"), 							"Reservation report criteria Radio buttons availability - Lpo required - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("bookingChannelAll"), 				"Reservation report criteria Radio buttons availability - Booking channel - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("bookingChannelWeb"), 				"Reservation report criteria Radio buttons availability - Booking channel - Web");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("bookingChannelCC"), 				"Reservation report criteria Radio buttons availability - Booking channel - CC");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerAll"), 					"Reservation report criteria Radio buttons availability - Partner - All");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerDirectConsumer"), 			"Reservation report criteria Radio buttons availability - Partner - Direct customer");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerAffiliate"), 				"Reservation report criteria Radio buttons availability - Partner - Affiliate");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("partnerB2B"), 					"Reservation report criteria Radio buttons availability - Partner - B2B partner");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("pdfNo"), 							"Reservation report criteria Radio buttons availability - Generate PDF - No");
			printTemplate.verifyTrue(true, resReportCriteriaRadioButtons.get("pdfYes"), 						"Reservation report criteria Radio buttons availability - Generate PDF - Yes");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportCriteriaLabelsVerification(ReportTemplate printTemplate, HashMap<String, String> resReportCriteriaLabels, HashMap<String, String> labels)
	{
//		printTemplate.setTableHeading("Reservation report criteria labels verification");
		try {
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductType"), 					resReportCriteriaLabels.get("productType"), 				"Reservation report criteria labels verification - Product type");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeAll"), 				resReportCriteriaLabels.get("productTypeAll"), 				"Reservation report criteria labels verification - Product type - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeHotels"), 				resReportCriteriaLabels.get("productTypeHotel"), 			"Reservation report criteria labels verification - Product type - Hotel");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeAir"), 				resReportCriteriaLabels.get("ProductTypeAir"), 				"Reservation report criteria labels verification - Product type - Air");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeCar"), 				resReportCriteriaLabels.get("productTypeCar"), 				"Reservation report criteria labels verification - Product type - Car");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypeActivities"), 			resReportCriteriaLabels.get("prodctTypeActivity"), 			"Reservation report criteria labels verification - Product type - Activity");
			printTemplate.verifyTrue(labels.get("resReportCriteriaProductTypePackage"), 			resReportCriteriaLabels.get("productTypePackage"), 			"Reservation report criteria labels verification - Product type - Package");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchBy"), 						resReportCriteriaLabels.get("searchBy"), 					"Reservation report criteria labels verification - Search by");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByDates"), 					resReportCriteriaLabels.get("searchByDate"), 				"Reservation report criteria labels verification - Search by - Date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByResNumber"), 				resReportCriteriaLabels.get("searchByReservationNumber"), 	"Reservation report criteria labels verification - Search by - Reservation number");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByGuestname"), 				resReportCriteriaLabels.get("searchbyGuestName"), 			"Reservation report criteria labels verification - Search by - Guest name");
			printTemplate.verifyTrue(labels.get("resReportCriteriaSearchByXmlPckRef"), 				resReportCriteriaLabels.get("searchbyXmlPackageReference"), "Reservation report criteria labels verification - Search by - Xml package reference");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDateFilter"), 					resReportCriteriaLabels.get("dateFilter"), 					"Reservation report criteria labels verification - Date filter");
			printTemplate.verifyTrue(labels.get("resReportCriteriaReportFrom"), 					resReportCriteriaLabels.get("reportFrom"), 					"Reservation report criteria labels verification - Report from");
			printTemplate.verifyTrue(labels.get("resReportCriteriaReportTo"), 						resReportCriteriaLabels.get("reportTo"), 					"Reservation report criteria labels verification - Report to");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOn"), 					resReportCriteriaLabels.get("datesBasedOn"), 				"Reservation report criteria labels verification - Dates based on");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOnReservationDate"), 	resReportCriteriaLabels.get("datesBasedonReservationDate"), "Reservation report criteria labels verification - Dates based on - reservation date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOnArrivalDate"), 		resReportCriteriaLabels.get("datesBasedOnArrivalDate"), 	"Reservation report criteria labels verification - Dates based on - Arrival date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaDatesBasedOnDueDate"), 			resReportCriteriaLabels.get("datesBasedOnDueDate"), 		"Reservation report criteria labels verification - Dates based on - Due date");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApproved"), 				resReportCriteriaLabels.get("paymentApproved"), 			"Reservation report criteria labels verification - Payment approved");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApprovedAll"), 			resReportCriteriaLabels.get("paymentApprovedAll"), 			"Reservation report criteria labels verification - Payment approved - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApprovedYes"), 			resReportCriteriaLabels.get("paymentApprovedYes"), 			"Reservation report criteria labels verification - Payment approved - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPaymentApprovedNo"), 				resReportCriteriaLabels.get("paymentApprovedNo"), 			"Reservation report criteria labels verification - Payment approved - No");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssued"), 					resReportCriteriaLabels.get("invoiceIssued"), 				"Reservation report criteria labels verification - Invoice issued");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssuedAll"), 				resReportCriteriaLabels.get("invoiceIssuedAll"), 			"Reservation report criteria labels verification - Invoice issued - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssuedYes"), 				resReportCriteriaLabels.get("invoiceIssuedYes"), 			"Reservation report criteria labels verification - Invoice issued - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaInvoiceIssuedNo"), 				resReportCriteriaLabels.get("invoiceIssuedNo"), 			"Reservation report criteria labels verification - Invoice issued - No");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssued"), 					resReportCriteriaLabels.get("voucherIssued"), 				"Reservation report criteria labels verification - Voucher issued");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssuedAll"), 				resReportCriteriaLabels.get("voucherIssuedAll"), 			"Reservation report criteria labels verification - Voucher issued - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssuedYes"), 				resReportCriteriaLabels.get("voucherIssuedYes"), 			"Reservation report criteria labels verification - Voucher issued - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaVoucherIssuedNo"), 				resReportCriteriaLabels.get("voucherIssuedNo"), 			"Reservation report criteria labels verification - Voucher issued - No");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequired"), 					resReportCriteriaLabels.get("lpoRequired"), 				"Reservation report criteria labels verification - Lpo required");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequiredAll"), 				resReportCriteriaLabels.get("lpoRequiredAll"), 				"Reservation report criteria labels verification - Lpo required - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequiredYes"), 				resReportCriteriaLabels.get("lpoRequiredYes"), 				"Reservation report criteria labels verification - Lpo required - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaLpoRequiredNo"), 					resReportCriteriaLabels.get("lpoRequiredNo"), 				"Reservation report criteria labels verification - Lpo required - No,");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannel"), 				resReportCriteriaLabels.get("bookingChannel"), 				"Reservation report criteria labels verification - Booking channel");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannelAll"), 				resReportCriteriaLabels.get("bookingChannelAll"), 			"Reservation report criteria labels verification - Booking channel - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannelWeb"), 				resReportCriteriaLabels.get("bookingChannelWeb"), 			"Reservation report criteria labels verification - Booking channel - Web");
			printTemplate.verifyTrue(labels.get("resReportCriteriaBookingChannelCC"), 				resReportCriteriaLabels.get("bookingChannelCC"), 			"Reservation report criteria labels verification - Booking channel - CC");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartner"), 						resReportCriteriaLabels.get("partner"), 					"Reservation report criteria labels verification - Partner");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerAll"), 					resReportCriteriaLabels.get("partnerAll"), 					"Reservation report criteria labels verification - Partner - All");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerDirectCustomer"), 			resReportCriteriaLabels.get("partnerDirectConsumer"), 		"Reservation report criteria labels verification - Partner - Direct consumer");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerAffiliate"), 				resReportCriteriaLabels.get("partnerAffiliate"), 			"Reservation report criteria labels verification - Partner - Affiliate");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPartnerB2B"), 					resReportCriteriaLabels.get("partnerB2bPartner"), 			"Reservation report criteria labels verification - Partner - B2B");
			printTemplate.verifyTrue(labels.get("resReportCriteriaPosLocationName"), 				resReportCriteriaLabels.get("posLocation"), 				"Reservation report criteria labels verification - Pos location");
			printTemplate.verifyTrue(labels.get("resReportCriteriaGeneratePdf"), 					resReportCriteriaLabels.get("generatePDF"), 				"Reservation report criteria labels verification - Generate PDF");
			printTemplate.verifyTrue(labels.get("resReportCriteriaGeneratePdfYes"), 				resReportCriteriaLabels.get("generatePDFYes"), 				"Reservation report criteria labels verification - Generate PDF - Yes");
			printTemplate.verifyTrue(labels.get("resReportCriteriaGeneratePdfNo"), 					resReportCriteriaLabels.get("generatePDFNo"), 				"Reservation report criteria labels verification - Generate PDF - No");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void reservationReportLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> labels)
	{
		printTemplate.verifyTrue(true, labels.get("edit"), 							"Reservation report - Label availability - Edit");
		printTemplate.verifyTrue(true, labels.get("bookingNumber"), 				"Reservation report - Label availability - Booking number");
		printTemplate.verifyTrue(true, labels.get("docNumber"), 					"Reservation report - Label availability - Doc number");
		printTemplate.verifyTrue(true, labels.get("bookingDate"), 					"Reservation report - Label availability - Booking date");
		printTemplate.verifyTrue(true, labels.get("customerType"), 					"Reservation report - Label availability - Customer type");
		printTemplate.verifyTrue(true, labels.get("customerLabel"), 				"Reservation report - Label availability - Customer label");
		printTemplate.verifyTrue(true, labels.get("productType"), 					"Reservation report - Label availability - Product type");
		printTemplate.verifyTrue(true, labels.get("consultantName"), 				"Reservation report - Label availability - Consultant name");
		printTemplate.verifyTrue(true, labels.get("bookingStatus"), 				"Reservation report - Label availability - Booking status");
		printTemplate.verifyTrue(true, labels.get("bookingChannel"), 				"Reservation report - Label availability - Booking channel");
		printTemplate.verifyTrue(true, labels.get("docsIssued"), 					"Reservation report - Label availability - Docs issued");
		printTemplate.verifyTrue(true, labels.get("supplierName"), 					"Reservation report - Label availability - Supplier name");
		printTemplate.verifyTrue(true, labels.get("supplierConfirmationNumber"), 	"Reservation report - Label availability - Supplier confirmation number");
		printTemplate.verifyTrue(true, labels.get("guestName"), 					"Reservation report - Label availability - Guest name");
		printTemplate.verifyTrue(true, labels.get("paymentTypeReference"), 			"Reservation report - Label availability - Payment type reference");
		printTemplate.verifyTrue(true, labels.get("contactNumber"), 				"Reservation report - Label availability - Contact number");
		printTemplate.verifyTrue(true, labels.get("transIDauthID"), 				"Reservation report - Label availability - Transaction and auth id");
		printTemplate.verifyTrue(true, labels.get("vacationOrderID"), 				"Reservation report - Label availability - Vacation order id");
		printTemplate.verifyTrue(true, labels.get("pgCurrency"), 					"Reservation report - Label availability - PG currency");
		printTemplate.verifyTrue(true, labels.get("amountChargedByPG"), 			"Reservation report - Label availability - Amount charged by PG");
		printTemplate.verifyTrue(true, labels.get("sellingCurrency"), 				"Reservation report - Label availability - Selling currency");
		printTemplate.verifyTrue(true, labels.get("totalRate"), 					"Reservation report - Label availability - Total rate");
		printTemplate.verifyTrue(true, labels.get("ccFee"), 						"Reservation report - Label availability - CC fee");
		printTemplate.verifyTrue(true, labels.get("bookingFee"), 					"Reservation report - Label availability - Booking fee");
		printTemplate.verifyTrue(true, labels.get("amountPaid"), 					"Reservation report - Label availability - Amount paid");
		printTemplate.verifyTrue(true, labels.get("grossOrderValue"), 				"Reservation report - Label availability - Gross order value");
		printTemplate.verifyTrue(true, labels.get("agentCommision"), 				"Reservation report - Label availability - Agent commision");
		printTemplate.verifyTrue(true, labels.get("netOrderValue"), 				"Reservation report - Label availability - Net order value");
		printTemplate.verifyTrue(true, labels.get("orderValue"), 					"Reservation report - Label availability - Order value");
		printTemplate.verifyTrue(true, labels.get("grossOrderInPC"), 				"Reservation report - Label availability - Gross order in portal currency");
		printTemplate.verifyTrue(true, labels.get("netOrderValueInPC"), 			"Reservation report - Label availability - Net order in portal currency");
		printTemplate.verifyTrue(true, labels.get("orderValueInPC"), 				"Reservation report - Label availability - Order value in poral currency");
	}
	
	public void reservationReportValuesVerification(ReportTemplate printTemplate, HashMap<String, String> values, Vacation_Rate_Details rateDetails, hotelDetails reservationRes, HashMap<String, String> currencyMap, String defaultCC, HashMap<String, String> paymentDetails, HashMap<String, String> confirmationPagevalues, String reportDate, Vacation_Search_object search, HashMap<String, String> portalSpecificDetails, ReservationResponse reservationResponse, ArrayList<HotelOccupancyObject> occupancyList)
	{
		printTemplate.verifyTrue(confirmationPagevalues.get("reservationNumber"), values.get("bookingNumber"), "Reservation report - Verify values - Booking number");
		printTemplate.verifyTrue(reportDate, values.get("bookingDate"), "Reservation report - Verify values - Booking date");
		String customerType = "";
		String consultant = "";
		if(search.getChannel().equals("web"))
		{
			customerType = "DC";
			consultant = "Direct Consumer";
		}
		else
		{
			customerType = portalSpecificDetails.get("akilaTest");
			consultant = portalSpecificDetails.get("akilaTest");
		}
		printTemplate.verifyTrue(customerType, values.get("customerType"), "Reservation report - Verify values - Customer type");
		String customer = "Mr " + portalSpecificDetails.get("cd.firstname") + " " + portalSpecificDetails.get("cd.lastname");
		printTemplate.verifyTrue(customer, values.get("customer"), "Reservation report - Verify values - Customer");
		printTemplate.verifyTrue("Flight + Hotel", values.get("productType"), "Reservation report - Verify values - Product type");
		printTemplate.verifyTrue(consultant, values.get("consultantName"), "Reservation report - Verify values - Consutant name");
		printTemplate.verifyTrue("Normal", values.get("bookingStatus"), "Reservation report - Verify values - Booking status");
		String bookingChannel = "";
		if(search.getChannel().equals("web"))
		{
			bookingChannel = "Website";
		}
		else
		{
			bookingChannel = "Call Center";
		}
		printTemplate.verifyTrue(bookingChannel, values.get("bookingChannel"), "Reservation report - Verify values - Booking channel");
		printTemplate.verifyTrue("", values.get("docsIssued"), "Reservation report - Verify values - Docs issued");
		String supplierName = "";
		if(search.getHotelSupplier().equals("INV27"))
		{
			supplierName = "Flight : " + portalSpecificDetails.get("flightSupplier") + " Hotel : " + "GTA_Hotels";
		}
		else
		{
			supplierName = "Flight : " + portalSpecificDetails.get("flightSupplier") + " Hotel : " + search.getHotelSupplier();
		}
		System.out.println("paymentID - " + paymentDetails.get("paymentID"));
		System.out.println("paymentReference - " + paymentDetails.get("paymentReference"));
		System.out.println("transactionID - " + paymentDetails.get("transactionID"));
		System.out.println("authId - " + paymentDetails.get("authId"));
		printTemplate.verifyTrue(supplierName, values.get("supplierName"), "Reservation report - Verify values - Supplier name");
		String supConNumber = "Flight PNR : " + reservationResponse.getItineraryRefID() + " Hotel Conf No : " + reservationRes.getSupConfirmationNumber();
		printTemplate.verifyTrue(supConNumber, values.get("supplierConNumber"), "Reservation report - Verify values - Supplier confirmation number");
		String guestName = occupancyList.get(0).getAdultFirstName() + " " + occupancyList.get(0).getAdultLastName();
		printTemplate.verifyTrue(guestName, values.get("guestName"), "Reservation report - Verify values - Guest name");
		String payRef = "Credit Card /" + paymentDetails.get("paymentReference");
		printTemplate.verifyTrue(payRef, values.get("paymentTypeReference"), "Reservation report - Verify values - Payment reference");
		String contactNumber = portalSpecificDetails.get("cd.phoneNumberCode") + "-" + portalSpecificDetails.get("cd.phoneNumber");
		printTemplate.verifyTrue(contactNumber, values.get("contactNumber"), "Reservation report - Verify values - Contact number");
		String transAuth = paymentDetails.get("paymentID") + " / " + paymentDetails.get("authId");
		printTemplate.verifyTrue(transAuth, values.get("transactionID"), "Reservation report - Verify values - Transaction ID");
		//printTemplate.verifyTrue("", values.get("pgCurrency"), "Reservation report - Verify values - PG currency");
		
		printTemplate.verifyTrue(rateDetails.getTotalInPGCurrency(), values.get("amountChargedByPG"), "Reservation report - Verify values - Amount charged by PG");
		printTemplate.verifyTrue(rateDetails.getCurrencyInSellingCurrency(), values.get("sellingCurrency"), "Reservation report - Verify values - Selling currency");
		printTemplate.verifyTrue(rateDetails.getTotalInSellingCurrency(), values.get("totalRate"), "Reservation report - Verify values - Total rate");
		printTemplate.verifyTrue(rateDetails.getCcfeeInSellingCurrency(), values.get("ccfee"), "Reservation report - Verify values - Credit card fee");
		String bf = String.valueOf(Integer.parseInt(rateDetails.getHotelbfInSellingCurrency()) + Integer.parseInt(rateDetails.getFlightbfInSellingCurrency()));
		printTemplate.verifyTrue(bf, values.get("bookingFee"), "Reservation report - Verify values - Booking fee");
		printTemplate.verifyTrue(rateDetails.getTotalInSellingCurrency(), values.get("amountPaid"), "Reservation report - Verify values - Amount paid");
		printTemplate.verifyTrue(rateDetails.getTotalInSellingCurrency(), values.get("grossOrderValue"), "Reservation report - Verify values - Gross order value");
		printTemplate.verifyTrue("N/A", values.get("agentCommision"), "Reservation report - Verify values - Agent commision");
		printTemplate.verifyTrue(rateDetails.getTotalInSellingCurrency(), values.get("netOrderValue"), "Reservation report - Verify values - Net order value");
		printTemplate.verifyTrue(rateDetails.getTotalInSellingCurrency(), values.get("orderValue"), "Reservation report - Verify values - Order value");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("grossoRderValue"), "Reservation report - Verify values - Gross order value");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("netOrderValueInPC"), "Reservation report - Verify values - Net order value");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("orderValueInPC"), "Reservation report - Verify values - Order value in portal currency");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("pageTotalNetOrderValue"), "Reservation report - Verify values - Page total - Net order value");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("pageTotalorderValue"), "Reservation report - Verify values - Page total - Order value");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("grandTotalNetOrderValue"), "Reservation report - Verify values - Grand total - Net order value");
		printTemplate.verifyTrue(rateDetails.getTotalInPortalCurrency(), values.get("grandTotalOrderValue"), "Reservation report - Verify values - Grand total - Order value");
	}
	
	public void bookingListLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> labels)
	{
//		printTemplate.setTableHeading("Booking list report - Label availability");
		try {
			printTemplate.verifyTrue(true, labels.get("number"), 				"Booking list report - Label availability - Number");
			printTemplate.verifyTrue(true, labels.get("reservations"), 			"Booking list report - Label availability - Reservations");
			printTemplate.verifyTrue(true, labels.get("customer"), 				"Booking list report - Label availability - Customer");
			printTemplate.verifyTrue(true, labels.get("leadGuest"), 			"Booking list report - Label availability - Lead guest");
			printTemplate.verifyTrue(true, labels.get("dateOf"), 				"Booking list report - Label availability - Date of");
			printTemplate.verifyTrue(true, labels.get("date"), 					"Booking list report - Label availability - Date");
			printTemplate.verifyTrue(true, labels.get("time"), 					"Booking list report - Label availability - Time");
			printTemplate.verifyTrue(true, labels.get("reservedBy"), 			"Booking list report - Label availability - Reserved by");
			printTemplate.verifyTrue(true, labels.get("paymentType"), 			"Booking list report - Label availability - Payment type");
			printTemplate.verifyTrue(true, labels.get("profitMarkUpType"), 		"Booking list report - Label availability - Profit mark up type");
			printTemplate.verifyTrue(true, labels.get("customerName"), 			"Booking list report - Label availability - Customer name");
			printTemplate.verifyTrue(true, labels.get("leadGuestName"), 		"Booking list report - Label availability - Lead guest name");
			printTemplate.verifyTrue(true, labels.get("firstElement"), 			"Booking list report - Label availability - First element");
			printTemplate.verifyTrue(true, labels.get("cancellationDeadline"), 	"Booking list report - Label availability - Cancellation deadline");
			printTemplate.verifyTrue(true, labels.get("cityCountryName"), 		"Booking list report - Label availability - City country name");
			printTemplate.verifyTrue(true, labels.get("product"), 				"Booking list report - Label availability - Product");
			printTemplate.verifyTrue(true, labels.get("verified"), 				"Booking list report - Label availability - Verified");
			printTemplate.verifyTrue(true, labels.get("status"), 				"Booking list report - Label availability - Status");
			printTemplate.verifyTrue(true, labels.get("notifications"), 		"Booking list report - Label availability - Notifications");
			printTemplate.verifyTrue(true, labels.get("edit"), 					"Booking list report - Label availability - Edit");
			printTemplate.verifyTrue(true, labels.get("shoppingCartCode"), 		"Booking list report - Label availability - Shpping cart code");
			printTemplate.verifyTrue(true, labels.get("shoppingCart"), 			"Booking list report - Label availability - Shopping cart");
			printTemplate.verifyTrue(true, labels.get("dynamicPackageCode"), 	"Booking list report - Label availability - Dynamic package code");
			printTemplate.verifyTrue(true, labels.get("dynamicPackage"), 		"Booking list report - Label availability - Dynamic package");
			printTemplate.verifyTrue(true, labels.get("fixedPackageCode"), 		"Booking list report - Label availability - Fixed package code");
			printTemplate.verifyTrue(true, labels.get("fixedPackages"), 		"Booking list report - Label availability - Fixed package");
			printTemplate.verifyTrue(true, labels.get("dmcCode"), 				"Booking list report - Label availability - Dmc code");
			printTemplate.verifyTrue(true, labels.get("dmc"), 					"Booking list report - Label availability - DMc");
			printTemplate.verifyTrue(true, labels.get("invoice"), 				"Booking list report - Label availability - Invoice");
			printTemplate.verifyTrue(true, labels.get("invoiceIssued"), 		"Booking list report - Label availability - Invoice issued");
			printTemplate.verifyTrue(true, labels.get("payment"), 				"Booking list report - Label availability - Payment");
			printTemplate.verifyTrue(true, labels.get("paymentReceived"), 		"Booking list report - Label availability - Payment received");
			printTemplate.verifyTrue(true, labels.get("voucher"), 				"Booking list report - Label availability - Voucher");
			printTemplate.verifyTrue(true, labels.get("voucherIssued"), 		"Booking list report - Label availability - Voucher issued");
			printTemplate.verifyTrue(true, labels.get("ticket"), 				"Booking list report - Label availability - Ticket");
			printTemplate.verifyTrue(true, labels.get("ticketIssued"), 			"Booking list report - Label availability - Ticket issued");
			printTemplate.verifyTrue(true, labels.get("emd"), 					"Booking list report - Label availability - EMD");
			printTemplate.verifyTrue(true, labels.get("emdIssued"), 			"Booking list report - Label availability - EMD issued");
			printTemplate.verifyTrue(true, labels.get("failedSectorImage"), 	"Booking list report - Label availability - Failed sector Image");
			printTemplate.verifyTrue(true, labels.get("failedSector"), 			"Booking list report - Label availability - Failed sector");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListLabelVerification(ReportTemplate printTemplate, HashMap<String, String> labels, HashMap<String, String> labelProperty)
	{
//		printTemplate.setTableHeading("Booking list report - Label verification");
		try {
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservationNumber"), 		labels.get("number"), 					"Booking list report - Label verification - Reservation number");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservationDate"), 		labels.get("date"), 					"Booking list report - Label verification - Reservation date");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservationTime"), 		labels.get("time"), 					"Booking list report - Label verification - Reservation time");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservedBy"), 				labels.get("reservedBy"), 				"Booking list report - Label verification - Reserved by");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportPaymentType"), 			labels.get("paymentType"), 				"Booking list report - Label verification - Payment type");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportPMType"), 					labels.get("profitMarkUpType"), 		"Booking list report - Label verification - Profit mark up type");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCustomerName"), 			labels.get("customerName"), 			"Booking list report - Label verification - Customer name");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportLeadGuestName"), 			labels.get("leadGuestName"), 			"Booking list report - Label verification - Lead guest name");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportFirstElement"), 			labels.get("firstElement"), 			"Booking list report - Label verification - First element");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCancellationDeadline"), 	labels.get("cancellationDeadline"), 	"Booking list report - Label verification - Cancellation deadline");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCityCountry"), 			labels.get("cityCountryName"), 			"Booking list report - Label verification - City/Country name");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportProduct"), 				labels.get("product"), 					"Booking list report - Label verification - Product");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportVerified"), 				labels.get("verified"), 				"Booking list report - Label verification - Verified");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportStatus"), 					labels.get("status"), 					"Booking list report - Label verification - Status");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportNotifications"), 			labels.get("notifications"), 			"Booking list report - Label verification - Notifications");
//			printTemplate.verifyTrue(labelProperty.get("bookingListReportview"), 					labels.get(""), 						"Booking list report - Label verification - ");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportEdit"), 					labels.get("edit"), 					"Booking list report - Label verification - Edit");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportReservation"), 			labels.get("reservations"), 			"Booking list report - Label verification - Reservations");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportCustomer"), 				labels.get("customer"), 				"Booking list report - Label verification - Customer");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportLeadGuest"), 				labels.get("leadGuest"), 				"Booking list report - Label verification - Lead guest");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDateOf"), 					labels.get("dateOf"), 					"Booking list report - Label verification - Date of");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportSC"), 						labels.get("shoppingCartCode"), 		"Booking list report - Label verification - Shopping cart code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportShoppingCart"), 			labels.get("shoppingCart"), 			"Booking list report - Label verification - Shopping cart");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDP"), 						labels.get("dynamicPackageCode"), 		"Booking list report - Label verification - Dynamic package code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDynamicPackage"), 			labels.get("dynamicPackage"), 			"Booking list report - Label verification - Dynamic package");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportFP"), 						labels.get("fixedPackageCode"), 		"Booking list report - Label verification - Fixed package code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportFixedPackage"), 			labels.get("fixedPackages"), 			"Booking list report - Label verification - Fixed packages");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDMCP"), 					labels.get("dmcCode"), 					"Booking list report - Label verification - DMC code");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportDMCPackage"), 				labels.get("dmc"), 						"Booking list report - Label verification - DMC");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportInvoiceIssued"), 			labels.get("invoiceIssued"), 			"Booking list report - Label verification - Invoice issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportPaymentRecieved"), 		labels.get("paymentReceived"), 			"Booking list report - Label verification - Payment received");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportVoucherIssued"), 			labels.get("voucherIssued"), 			"Booking list report - Label verification - Voucher issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportTicketIssued"), 			labels.get("ticketIssued"), 			"Booking list report - Label verification - Ticket issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportEMDIssued"), 				labels.get("emdIssued"), 				"Booking list report - Label verification - EMD issued");
			printTemplate.verifyTrue(labelProperty.get("bookingListReportSectorFailed"), 			labels.get("failedSector"), 			"Booking list report - Label verification - Failed sector");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListValueVarification()
	{
		
	}
	
	public void bookingListReportCriteriaLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> blAvailabiltiy)
	{
//		printTemplate.setTableHeading("Booking list report - Search criteria label availability");
		{
			try {
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partner"), 						"Booking list report - Search criteria label availability - Partner");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerAll"), 					"Booking list report - Search criteria label availability - Partner - All");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerDc"), 					"Booking list report - Search criteria label availability - Partner - DC");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerb2b"), 					"Booking list report - Search criteria label availability - Partner - B2B");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerAffiliate"), 				"Booking list report - Search criteria label availability - Partner - Affiliate");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("posLocation"), 					"Booking list report - Search criteria label availability - Pos location");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("partnerBookingNo"), 				"Booking list report - Search criteria label availability - Partner booking number");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("consultantName"), 				"Booking list report - Search criteria label availability - Consultant name");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productType"), 					"Booking list report - Search criteria label availability - Product type");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeAll"), 				"Booking list report - Search criteria label availability - Product type - All");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeActivities"), 		"Booking list report - Search criteria label availability - Product type - Activities");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeHotels"), 			"Booking list report - Search criteria label availability - Product type - Hotels");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeAir"), 				"Booking list report - Search criteria label availability - Product type - Air");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeCar"), 				"Booking list report - Search criteria label availability - Product type - Car");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("productTypePackaging"), 			"Booking list report - Search criteria label availability - Product type - Packaging");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("firstAndLastName"), 				"Booking list report - Search criteria label availability - First and last name");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatus"), 				"Booking list report - Search criteria label availability - Booking status");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusAll"), 				"Booking list report - Search criteria label availability - Booking status - All");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusConfirmed"), 		"Booking list report - Search criteria label availability - Booking status - Confirmed");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusOnReq"), 			"Booking list report - Search criteria label availability - Booking status - On request");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusCancelled"), 		"Booking list report - Search criteria label availability - Booking status - Cancelled");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusModified"), 		"Booking list report - Search criteria label availability - Booking status - Modified");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusWithFailedSector"), "Booking list report - Search criteria label availability - Booking status - With fail sector");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("dateFilter"), 					"Booking list report - Search criteria label availability - Date filter");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("reportFrom"), 					"Booking list report - Search criteria label availability - Report from");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("reportTo"), 						"Booking list report - Search criteria label availability - Report to");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOn"), 					"Booking list report - Search criteria label availability - Dates based on");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnReservation"), 		"Booking list report - Search criteria label availability - Dates based on - Reservation");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnCancellation"), 		"Booking list report - Search criteria label availability - Dates based on - Cancellation");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnArrival"), 			"Booking list report - Search criteria label availability - Dates based on - Arrival");
				printTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnDeparture"), 		"Booking list report - Search criteria label availability - Dates based on - Departure");
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListReportCriteriaLabelVerification(ReportTemplate printTemplate, HashMap<String, String> report, HashMap<String, String> labelProperty)
	{
//		printTemplate.setTableHeading("Booking list report - Search criteria label verification");
		try {
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriapartner"), 							report.get("partner"), 						"Booking list report - Search criteria label verification - Partner");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaPartnerAll"), 						report.get("partnerAll"), 					"Booking list report - Search criteria label verification - Partner - All");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaPartnerDC"), 						report.get("partnerDc"), 					"Booking list report - Search criteria label verification - Partner - DC");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriapartnerB2B"), 						report.get("partnerb2b"), 					"Booking list report - Search criteria label verification - Partner - B2B");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriapartnerAffiliate"), 					report.get("partnerAffiliate"), 			"Booking list report - Search criteria label verification - Partner - Affiliate");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaPosLocationName"), 					report.get("posLocation"), 					"Booking list report - Search criteria label verification - Pos location");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingNo"), 						report.get("partnerBookingNo"), 			"Booking list report - Search criteria label verification - Partner booking number");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaConsultantName"), 					report.get("consultantName"), 				"Booking list report - Search criteria label verification - Consultant name");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductType"), 						report.get("productType"), 					"Booking list report - Search criteria label verification - Product type");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeAll"), 					report.get("productTypeAll"), 				"Booking list report - Search criteria label verification - Product type - All");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeActivities"), 			report.get("productTypeActivities"), 		"Booking list report - Search criteria label verification - Product type - Activities");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeHotels"), 				report.get("productTypeHotels"), 			"Booking list report - Search criteria label verification - Product type - Hotels");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeAir"), 					report.get("productTypeAir"), 				"Booking list report - Search criteria label verification - Product type - Air");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypeCar"), 					report.get("productTypeCar"), 				"Booking list report - Search criteria label verification - Product type - Car");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaProductTypePackaging"), 				report.get("productTypePackaging"), 		"Booking list report - Search criteria label verification - Product type - Packaging");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaFirstAndLastName"),				 	report.get("firstAndLastName"), 			"Booking list report - Search criteria label verification - First and lact name");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatus"), 					report.get("bookingStatus"), 				"Booking list report - Search criteria label verification - Booking status");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusAll"), 					report.get("bookingStatusAll"), 			"Booking list report - Search criteria label verification - Booking status - All");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusConfirmed"), 			report.get("bookingStatusConfirmed"), 		"Booking list report - Search criteria label verification - Booking status - Confirmed");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusOnReq"), 				report.get("bookingStatusOnReq"), 			"Booking list report - Search criteria label verification - Booking status - On request");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusCancelled"), 			report.get("bookingStatusCancelled"), 		"Booking list report - Search criteria label verification - Booking status - Cancelled");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusModified"), 			report.get("bookingStatusModified"), 		"Booking list report - Search criteria label verification - Booking status - Modified");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaBookingStatusWithFailedSector"), 	report.get("bookingStatusWithFailedSector"),"Booking list report - Search criteria label verification - Booking status - With failed sector");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDateFilter"), 						report.get("dateFilter"), 					"Booking list report - Search criteria label verification - Date filter");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaReportFrom"), 						report.get("reportFrom"), 					"Booking list report - Search criteria label verification - Report from");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaReportTo"), 							report.get("reportTo"), 					"Booking list report - Search criteria label verification - Report to");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOn"), 						report.get("datesBasedOn"), 				"Booking list report - Search criteria label verification - Dates based on");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnReservation"), 			report.get("datesBasedOnReservation"), 		"Booking list report - Search criteria label verification - Dates based on - Reservation");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnCancellation"), 			report.get("datesBasedOnCancellation"), 	"Booking list report - Search criteria label verification - Dates based on - candellatiom");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnArrival"), 				report.get("datesBasedOnArrival"), 			"Booking list report - Search criteria label verification - Dates based on - Arrival");
			printTemplate.verifyTrue(labelProperty.get("bookingListCriteriaDatesBasedOnDeparture"), 			report.get("datesBasedOnDeparture"), 		"Booking list report - Search criteria label verification - Dates based on - Depature");
		} catch (Exception e) {
			// TODO: handle exception
		}
//		printTemplate.markTableEnd();
	}
	
	public void bookingListLiveLabelAvailability(ReportTemplate printTemplate, HashMap<String, Boolean> availability)
	{
		printTemplate.verifyTrue(true, availability.get("hotelDetails"), 				"Booking list report - Live booking more info - Label availability - Hotel details");
		printTemplate.verifyTrue(true, availability.get("hotelCountry"), 				"Booking list report - Live booking more info - Label availability - Hotel country");
		printTemplate.verifyTrue(true, availability.get("hotelCity"), 					"Booking list report - Live booking more info - Label availability - Hotel city");
		printTemplate.verifyTrue(true, availability.get("hotelSupplier"), 				"Booking list report - Live booking more info - Label availability - Hotel supplier");
		printTemplate.verifyTrue(true, availability.get("hotelName"), 					"Booking list report - Live booking more info - Label availability - Hotel name");
		printTemplate.verifyTrue(true, availability.get("hotelCheckin"), 				"Booking list report - Live booking more info - Label availability - Hotel checkin");
		printTemplate.verifyTrue(true, availability.get("hotelTotal"), 					"Booking list report - Live booking more info - Label availability - Hotel total");
		printTemplate.verifyTrue(true, availability.get("hotelStatus"), 				"Booking list report - Live booking more info - Label availability - Hotel status");
		printTemplate.verifyTrue(true, availability.get("hotelCandeadline"), 			"Booking list report - Live booking more info - Label availability - Hotel cancellation deadline");
		printTemplate.verifyTrue(true, availability.get("flightDetails"), 				"Booking list report - Live booking more info - Label availability - Flight details");
		printTemplate.verifyTrue(true, availability.get("flightDate"), 					"Booking list report - Live booking more info - Label availability - Flight date");
		printTemplate.verifyTrue(true, availability.get("flightDepartureCountry"), 		"Booking list report - Live booking more info - Label availability - Flight departure country");
		printTemplate.verifyTrue(true, availability.get("flightDepartureCityPort"), 	"Booking list report - Live booking more info - Label availability - Flight departure city/port");
		printTemplate.verifyTrue(true, availability.get("flightDepartureTime"), 		"Booking list report - Live booking more info - Label availability - Flight dearture time");
		printTemplate.verifyTrue(true, availability.get("flightDepartureNumber"), 		"Booking list report - Live booking more info - Label availability - Flight departure number");
		printTemplate.verifyTrue(true, availability.get("flightDeparture"), 			"Booking list report - Live booking more info - Label availability - Flight departure");
		printTemplate.verifyTrue(true, availability.get("flightArrival"), 				"Booking list report - Live booking more info - Label availability - Flight arrival");
		printTemplate.verifyTrue(true, availability.get("flightArrivalCountry"), 		"Booking list report - Live booking more info - Label availability - Flight arrival country");
		printTemplate.verifyTrue(true, availability.get("flightArrivalCityPort"), 		"Booking list report - Live booking more info - Label availability - Flight arrival city/port");
		printTemplate.verifyTrue(true, availability.get("flightArrivalTime"), 			"Booking list report - Live booking more info - Label availability - Flight arrival time");
		printTemplate.verifyTrue(true, availability.get("flightpax"), 					"Booking list report - Live booking more info - Label availability - Flight pax");
		printTemplate.verifyTrue(true, availability.get("flightStatus"), 				"Booking list report - Live booking more info - Label availability - Flight status");
		printTemplate.verifyTrue(true, availability.get("flightTicketIssued"), 			"Booking list report - Live booking more info - Label availability - Flight ticket issued");
		printTemplate.verifyTrue(true, availability.get("flightTicketingDeadline"), 	"Booking list report - Live booking more info - Label availability - Flight ticketing deadline");
		printTemplate.verifyTrue(true, availability.get("flightCanDeadline"), 			"Booking list report - Live booking more info - Label availability - Flight cancellation deadline");
		
	}
	
	public void bookingListLiveLabelVerification()
	{
		
	}
	
	public void bookingListLiveValueVerification(ReportTemplate printTemplate, HashMap<String, String> values, hotelDetails hotelRes, ReservationResponse flightRes, Vacation_Search_object search, HashMap<String, String> portalSpecificData, Vacation_Rate_Details rateDetails)
	{
		printTemplate.verifyTrue(search.getHotelDestination().split("\\|")[5], values.get("hotelCountry"), "Booking list report - Live booking more info - Values verification - Hotel - Country");
		printTemplate.verifyTrue(search.getHotelDestination().split("\\|")[1], values.get("hotelCity"), "Booking list report - Live booking more info - Values verification - Hotel - City");
		printTemplate.verifyTrue(search.getHotelSupplier(), values.get("hotelSupplier"), "Booking list report - Live booking more info - Values verification - Hotel - Supplier");
		printTemplate.verifyTrue(hotelRes.getHotel(), values.get("hotelName"), "Booking list report - Live booking more info - Values verification - Hotel - name");
		String checkinCheckout = hotelRes.getCheckin() + "/" + hotelRes.getCheckout();
		printTemplate.verifyTrue(checkinCheckout, values.get("hotelCheckinCheckout"), "Booking list report - Live booking more info - Values verification - Hotel - Checkin/Checkout");
		printTemplate.verifyTrue(hotelRes.getRoomCount(), values.get("hotelTotalRooms"), "Booking list report - Live booking more info - Values verification - Hotel - Total rooms");
		printTemplate.verifyTrue(hotelRes.getAvailabilityStatus(), values.get("hotelStatus"), "Booking list report - Live booking more info - Values verification - Hotel - Status");
		printTemplate.verifyTrue(rateDetails.getHotelCanDeadline(), values.get("hotelCanDeadline"), "Booking list report - Live booking more info - Values verification - Hotel - Cancelletion deadline");
		printTemplate.verifyTrue("", values.get("flightDepartureDate"), "Booking list report - Live booking more info - Values verification - Flight - Departure date");
		printTemplate.verifyTrue("", values.get("flightDepartureCountry"), "Booking list report - Live booking more info - Values verification - Flight - Departure country");
		printTemplate.verifyTrue("", values.get("flightDepartureCityPort"), "Booking list report - Live booking more info - Values verification - Flight - Departure city/port");
		printTemplate.verifyTrue("", values.get("flightDepartureTime"), "Booking list report - Live booking more info - Values verification - Flight - Departure time");
		printTemplate.verifyTrue("", values.get("flightDepartureFlightNumber"), "Booking list report - Live booking more info - Values verification - Flight - Departure flight number");
		printTemplate.verifyTrue("", values.get("flightArrivalCountry"), "Booking list report - Live booking more info - Values verification - Flight - Arrival country");
		printTemplate.verifyTrue("", values.get("flightArrivalCity"), "Booking list report - Live booking more info - Values verification - Flight - Arrival city");
		printTemplate.verifyTrue("", values.get("flightArrivalTime"), "Booking list report - Live booking more info - Values verification - Flight - Arrival time");
		printTemplate.verifyTrue("", values.get("flightPaxCount"), "Booking list report - Live booking more info - Values verification - Flight - Pax count");
		printTemplate.verifyTrue("", values.get("flightStatus"), "Booking list report - Live booking more info - Values verification - Flight - Status");
		printTemplate.verifyTrue("", values.get("flightTicketIssued"), "Booking list report - Live booking more info - Values verification - Flight - Ticket issued");
		printTemplate.verifyTrue("", values.get("flightTicketingDeadline"), "Booking list report - Live booking more info - Values verification - Flight - Ticketing deadline");
		printTemplate.verifyTrue("", values.get("flightCancellationDeadline"), "Booking list report - Live booking more info - Values verification - Flight - Cancellation deadline");
	}
}
