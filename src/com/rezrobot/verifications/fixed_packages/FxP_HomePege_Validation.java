package com.rezrobot.verifications.fixed_packages;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.processor.fixed_package_processor.BECDropdownOptionsConstructor;
import com.rezrobot.utill.ReportTemplate;
/*
 * 
 */



public class FxP_HomePege_Validation {
	
	/*private StringBuffer PrintWriter;
	
	PrintWriter = new StringBuffer();
	PrintTemplate =ReportTemplate.getInstance(); 
	PrintTemplate.Initialize(PrintWriter);
	*/
	 
	
	
	public ReportTemplate BECAvailability_web_Homepage(Web_Com_HomePage home,ReportTemplate PrintTemplate) throws Exception {
		//
		//PrintTemplate.setTableHeading("Booking Engine Validations");
		PrintTemplate.verifyTrue(true, home.getBECAvailability(), "Booking Engine Availability");
		PrintTemplate.verifyTrue(true, home.isFPPRoductEnabled(), "Booking Engine Product availability-Fixed package");

		PrintTemplate.verifyTrue(true, home.isFPBECAvailable(), "Fixed Package Booking Engine Availability");

		//PrintTemplate.markTableEnd();
		return PrintTemplate;
		
	}
	
	public ReportTemplate generalVerifications_web_Homepage(Web_Com_HomePage home,ReportTemplate PrintTemplate) {
		
		
		
		
		
		//PrintTemplate.setTableHeading("General Home Page Validations");		
		PrintTemplate.verifyTrue(true, home.isPageAvailable(),"Home Page Availability");
		PrintTemplate.verifyTrue("Rezgateway", home.getPageTitel().trim(), "Home Page Title Validation");
		try {
			
		
		PrintTemplate.verifyTrue(true, home.getLogoAvailability(), "Home Page Company Logo availability Validation");
		PrintTemplate.verifyTrue("", home.getLogoPath(), "Home Page Company Logo path Validation");

		PrintTemplate.verifyTrue(true, home.getCustomerLoginLinkAvailability(), "Home Page customer login link availability Validation");
		PrintTemplate.verifyTrue("", home.getCustomerLoginLinkText(), "Home Page customer login link Text Validation");
		PrintTemplate.verifyTrue("", home.getCustomerLoginLinkPath(), "Home Page customer login link path Validation");
		} catch (Exception e) {
			// TODO: handle exception
		}
		//PrintTemplate.markTableEnd();
		
		return PrintTemplate;
		
		
	}
	
	public ReportTemplate BECElementsValidation_web_Homepage(Web_Com_HomePage home,ReportTemplate PrintTemplate) throws Exception {
		
		//PrintTemplate.setTableHeading("Booking Engine input Fields Validations");
		if (home.isFPBECAvailable()) {

			HashMap<String, Boolean> AvailMap=home.getAvailabilityOfInputFields_FP();

			PrintTemplate.verifyTrue(true, AvailMap.get("cor"), "Availability Of COR field");
			PrintTemplate.verifyTrue(true, AvailMap.get("includingflight"), "Availability Of Flight Inclusive field");
			PrintTemplate.verifyTrue(true, AvailMap.get("excludingflight"), "Availability Of flight Exclusive field");
			PrintTemplate.verifyTrue(true, AvailMap.get("origin"), "Availability Of origin Airport field");
			PrintTemplate.verifyTrue(true, AvailMap.get("destination"), "Availability Of Destination Airport field");

			PrintTemplate.verifyTrue(true, AvailMap.get("destination_land"), "Availability Of destination city field");
			PrintTemplate.verifyTrue(true, AvailMap.get("departuremonth"), "Availability Of departure month field");
			PrintTemplate.verifyTrue(true, AvailMap.get("duration"), "Availability Of duration field");  
			PrintTemplate.verifyTrue(true, AvailMap.get("package_type"), "Availability Of package type field");  
			PrintTemplate.verifyTrue(true, AvailMap.get("prefferedcurrency_FP"), "Availability Of Prefferred currency field");  

			//PrintTemplate.markTableEnd();
		
	}
		return PrintTemplate;
	}
	
	public ReportTemplate labelTextValidation(Web_Com_HomePage home,ReportTemplate PrintTemplate) throws Exception {
		
		//PrintTemplate.setTableHeading("Label Text  Validations");

		HashMap<String, String> labelmap=home.getLabelTexts_FP();

		HashMap<String, String> expectedlblmap=new LabelReadProperties().becLabelFPConstructor();


		PrintTemplate.verifyTrue(expectedlblmap.get("CORlabel"),labelmap.get("CORlabel"),"Country of Residence Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("Tourplanlabel"),labelmap.get("Tourplanlabel"),"Tour Plan Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("FlightInclusivelabel"),labelmap.get("FlightInclusivelabel"),"Flight inclusive option Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("Flightexclusivelabel"),labelmap.get("Flightexclusivelabel"),"flight exclusive option Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("departureairportlabel"),labelmap.get("departureairportlabel"),"departure airport Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("destinationairportlabel"),labelmap.get("destinationairportlabel"),"Destination Airport Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("destinationcitylabel"),labelmap.get("destinationcitylabel"),"Destination city Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("departurelabel"),labelmap.get("departurelabel"),"Tour plan-Time Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("departuremonthlabel"),labelmap.get("departuremonthlabel"),"Departure month Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("durationlabel"),labelmap.get("durationlabel"),"Package Duration Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("packagetypelabel"),labelmap.get("packagetypelabel"),"Package Type Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("preferredcurrencylabel"),labelmap.get("preferredcurrencylabel"),"PreferredCurrency Label Validation");  
		PrintTemplate.verifyTrue(expectedlblmap.get("showsearchoptionslabel"),labelmap.get("showsearchoptionslabel"),"Search more options Label Validation");  



//
		//PrintTemplate.markTableEnd();
		return PrintTemplate;
		
	}
	
	public ReportTemplate DepartureMonthoptionsValidation(Web_Com_HomePage home,ReportTemplate PrintTemplate) throws Exception {
		//PrintTemplate.setTableHeading("Combo Box Attributes-Departure Month Validation");

		ArrayList<String> departuremonth=home.getAvailabledeparturemonthOptions();
		ArrayList<String> departuremonthExp=new BECDropdownOptionsConstructor().departuremonthconstructor();


		PrintTemplate.verifyTrue(13, departuremonth.size(), "Number of Available departure month options validation");

		for (int i = 0; i < departuremonthExp.size(); i++) {
			PrintTemplate.verifyTrue(departuremonthExp.get(i), departuremonth.get(i), "Available departure month options-Option index "+(i+1)+" validation");

		}

		PrintTemplate.verifyTrue("Any", home.departuremonthDefaultValue(), "Departure Month Default selected option validation");

		//PrintTemplate.markTableEnd();
		return PrintTemplate;
		
	}
	
	public ReportTemplate DurationOptionsValidation(Web_Com_HomePage home,ReportTemplate PrintTemplate) throws Exception {
		ArrayList<String> durationlist=home.getAvailablepackagedurationOptions();
		ArrayList<String> durationlistExp=new BECDropdownOptionsConstructor().packageDurationconstructor();

		//PrintTemplate.setTableHeading("Combo Box Attributes-Duration Validation");
		for (int i = 0; i < 5; i++) {

			PrintTemplate.verifyTrue(durationlistExp.get(i), durationlist.get(i), "Available duration options-Option index "+(i+1)+" validation");


		}
		PrintTemplate.verifyTrue(5, home.getAvailablepackagedurationOptions().size(), "Number of Available duration options validation");

		PrintTemplate.verifyTrue("All", home.durationDefaultValue(), "Duration Default selected option validation");

		//PrintTemplate.markTableEnd();
		return PrintTemplate;

		
	}
	
	public ReportTemplate packageTypeOptionsValidation(Web_Com_HomePage home,ReportTemplate PrintTemplate) throws Exception {

		//PrintTemplate.setTableHeading("Combo Box Attributes-Package type Validation");
		ArrayList<String> packagetypelist=home.getAvailablePackageTypeOptions();
		ArrayList<String> packagetypelistExp=new BECDropdownOptionsConstructor().packageTypeconstructor();

		PrintTemplate.verifyTrue(8, home.getAvailablePackageTypeOptions().size(), "Number of Available packagetype options validation");
		for (int i = 0; i < packagetypelistExp.size(); i++) {

			PrintTemplate.verifyTrue(packagetypelistExp.get(i), packagetypelist.get(i), "Available package type options-Option index "+(i+1)+" validation");


		}
		PrintTemplate.verifyTrue("All Packages", home.packagetypeDefaultValue(), "Duration Default selected option validation");

		//PrintTemplate.markTableEnd();
		return PrintTemplate;
		
	}
		


	

}
