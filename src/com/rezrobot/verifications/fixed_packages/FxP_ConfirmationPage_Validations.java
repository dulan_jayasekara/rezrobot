package com.rezrobot.verifications.fixed_packages;

import java.util.HashMap;

import com.rezrobot.pages.web.fixed_packages.Web_FxP_ConfimationPage;
import com.rezrobot.processor.fixed_package_processor.FxP_PortalSpecifcDataProcessor;
import com.rezrobot.utill.ReportTemplate;

public class FxP_ConfirmationPage_Validations {

	
	public ReportTemplate confirmationStatusValidations(Web_FxP_ConfimationPage confPage,ReportTemplate PrintTemplate) throws Exception {
		
		HashMap<String, String> actualmap=confPage.confirmationStatusRead();

		HashMap<String, String> expectedmap=new FxP_PortalSpecifcDataProcessor().ConfirmationStatusLoader();
		
		
		//PrintTemplate.setTableHeading("Confirmation Page-Confirmation Status Validations");
		PrintTemplate.verifyTrue(expectedmap.get("BookingReference"), actualmap.get("BookingReference"), "BookingReference Validation");
		PrintTemplate.verifyTrue(expectedmap.get("ResvationNo"), actualmap.get("ResvationNo"), "COnfirmation Number Generation Validation");

		PrintTemplate.verifyTrue(expectedmap.get("Customeremail"), actualmap.get("Customeremail"), "Customer Email Validation");
		
		PrintTemplate.verifyTrue(expectedmap.get("portalWeb"), actualmap.get("portalWeb"), "Portal Web Link Validation");
		PrintTemplate.verifyTrue(expectedmap.get("portalTel"), actualmap.get("portalTel"), "Portal Tel No Validation");
		PrintTemplate.verifyTrue(expectedmap.get("portalfax"), actualmap.get("portalfax"), "Portal Fax No Validation");

		//PrintTemplate.markTableEnd();
		return PrintTemplate;
		
	}
	

}
