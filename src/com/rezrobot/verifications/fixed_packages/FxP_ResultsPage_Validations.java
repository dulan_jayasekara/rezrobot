package com.rezrobot.verifications.fixed_packages;

import java.util.HashMap;

import com.rezrobot.dataobjects.fixed_package_objects.FxP_InputObject;
import com.rezrobot.pages.web.fixed_packages.Web_FxP_ResultsPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.ReportTemplate;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

public class FxP_ResultsPage_Validations {

	public ExtentReportTemplate generalVerifications_web_ResultsPage(ExtentReportTemplate extentReportTemplate, Web_FxP_ResultsPage resultsPage) throws Exception {

		LabelReadProperties labelRead = new LabelReadProperties();

		HashMap<String, String> dmclabel = labelRead.getDMCLabelProperties();

		// PrintTemplate.setTableHeading("Results Page Validations");
		extentReportTemplate.verifyTrue("Fixed Package Results", resultsPage.getPageTitel(), "Page Title Validation");
		extentReportTemplate.verifyTrue(true, resultsPage.getLogoAvailability(), "Results Page Company Logo availability Validation");
		try {

	//		extentReportTemplate.verifyTrue("", resultsPage.getLogoPath(), "Results Page Company Logo path Validation");

			extentReportTemplate.verifyTrue(true, resultsPage.getCustomerLoginLinkAvailability(), "Results Page customer login link availability Validation");

			extentReportTemplate.verifyTrue(dmclabel.get("Customer.login.text"), resultsPage.getCustomerLoginLinkText(), "Results Page customer login link Text Validation");

	//		extentReportTemplate.verifyTrue("", resultsPage.getCustomerLoginLinkPath(), "Results Page customer login link path Validation");

		} catch (Exception e) {
			// TODO: handle exception
		}

		return extentReportTemplate;

	}

	/*
	 * public ReportTemplate name() {
	 * 
	 * }
	 */

	public ExtentReportTemplate searchAgainBECVerifications_web_ResultPage(ExtentReportTemplate extentReportTemplate, Web_FxP_ResultsPage resultsPage, LabelReadProperties label) throws Exception {
		// PrintTemplate.setTableHeading("Search Again Booking engine Validations");
		extentReportTemplate.verifyTrue(true, resultsPage.isSearchAgainBECLoaded(), "Search again booking engine Availability Validation");
		if (resultsPage.isSearchAgainBECLoaded()) {

			
			HashMap<String, String> SearchMapContent = resultsPage.getSearchagainBEC_Content();
			HashMap<String, String> dmclabel = label.getDMCLabelProperties();
			

			extentReportTemplate.verifyTrue(dmclabel.get("CORlabel"), SearchMapContent.get("cor"), "Availability Of COR field");
			extentReportTemplate.verifyTrue(dmclabel.get("FlightInclusivelabel"), SearchMapContent.get("Including Flight"), "Availability Of Flight Inclusive field");
			extentReportTemplate.verifyTrue(dmclabel.get("Flightexclusivelabel"), SearchMapContent.get("excludingflight"), "Availability Of flight Exclusive field");
			extentReportTemplate.verifyTrue(dmclabel.get("departureairportlabel"), SearchMapContent.get("origin"), "Availability Of origin Airport field");
			extentReportTemplate.verifyTrue(dmclabel.get("destinationairportlabel"), SearchMapContent.get("destination"), "Availability Of Destination Airport field");

			extentReportTemplate.verifyTrue(dmclabel.get("destinationcitylabel"), SearchMapContent.get("destination_land"), "Availability Of destination city field");
			extentReportTemplate.verifyTrue(dmclabel.get("departuremonthlabel"), SearchMapContent.get("departuremonth"), "Availability Of departure month field");
			extentReportTemplate.verifyTrue(dmclabel.get("durationlabel"), SearchMapContent.get("duration"), "Availability Of duration field");
			extentReportTemplate.verifyTrue(dmclabel.get("packagetypelabel"), SearchMapContent.get("package_type"), "Availability Of package type field");
			extentReportTemplate.verifyTrue(dmclabel.get("preferredcurrencylabel"), SearchMapContent.get("prefferedcurrency_FP"), "Availability Of Prefferred currency field");

			
		}
		// PrintTemplate.markTableEnd();
		return extentReportTemplate;

	}

	public ReportTemplate selectedPackageDisplayVerification_web(ReportTemplate PrintTemplate, Web_FxP_ResultsPage resultsPage, FxP_InputObject input) throws Exception {
		// PrintTemplate.setTableHeading("Package Display Validations");
		PrintTemplate.verifyTrue(true, resultsPage.findResultBlock(input.getSearch()), "Specific Package Availability Validation");

		PrintTemplate.verifyTrue("", resultsPage.getThumbnailImage(), "Specific Package-Thumbnail Image Availability Validation");
		PrintTemplate.verifyTrue("", resultsPage.getvalidityPeriod(), "Specific Package-validity Period Validation");
		PrintTemplate.verifyTrue("", resultsPage.getdefaultDisplayedShortDescription(), "Specific Package-short Description Display Validation");
		PrintTemplate.verifyTrue(true, resultsPage.IsReadMoreLinkAvailable(), "Specific Package-Read more link Validation");
		resultsPage.readmoreClick();
		PrintTemplate.verifyTrue("", resultsPage.gethiddenText(), "Specific Package-hidden Text Validation");
		PrintTemplate.verifyTrue(true, resultsPage.MoreInfolinkAvailability(), "Specific Package- More info Link Availability Validation");

		// PrintTemplate.markTableEnd();

		return PrintTemplate;

	}

	public ReportTemplate moreinfoPackageDetailsContentValidation(ReportTemplate PrintTemplate, Web_FxP_ResultsPage resultsPage) throws Exception {

		HashMap<String, String> packageinfoContent = resultsPage.packageDetailsLoad();

		// PrintTemplate.setTableHeading("Package Details  Validations");
		PrintTemplate.verifyTrue("", packageinfoContent.get("DestinationDetailsHeader"), "Package detail Headers - destination details validation  ");
		PrintTemplate.verifyTrue("", packageinfoContent.get("PackageInclusionHeader"), "Package detail Headers - Package Inclusion validation  ");

		PrintTemplate.verifyTrue("PackageExclusionHeader", packageinfoContent.get("PackageExclusionHeader"), "Package detail Headers - Package Exclusion validation  ");

		PrintTemplate.verifyTrue("SpecialNotesHeader", packageinfoContent.get("SpecialNotesHeader"), "Package detail Headers - Special Notes validation  ");

		PrintTemplate.verifyTrue("ContactDetailsonArrivalsHeader", packageinfoContent.get("ContactDetailsonArrivalsHeader"), "Package detail Headers - Contact Details on Arrivals validation  ");

		PrintTemplate.verifyTrue("", packageinfoContent.get("DestinationDetailsContent"), "Package detail content - destination details validation  ");
		PrintTemplate.verifyTrue("", packageinfoContent.get("PackageInclusionContent"), "Package detail content - Package Inclusion validation  ");
		PrintTemplate.verifyTrue("", packageinfoContent.get("PackageExclusionContent"), "Package detail content - Package Exclusion validation  ");
		PrintTemplate.verifyTrue("", packageinfoContent.get("SpecialNotesContent"), "Package detail content - Special Notes validation  ");
		PrintTemplate.verifyTrue("", packageinfoContent.get("ContactDetailsonArrivalsContent"), "Package detail content - Contact Details on Arrivals validation  ");

		// PrintTemplate.markTableEnd();

		return PrintTemplate;

	}

	public ReportTemplate selectedPackagemoreinfoVerification_web(ReportTemplate PrintTemplate, Web_FxP_ResultsPage resultsPage, FxP_InputObject input) throws Exception {
		// PrintTemplate.setTableHeading("Package more info details-Package Deatils Validations");
		PrintTemplate.verifyTrue(true, resultsPage.findResultBlock(input.getSearch()), "Specific Package Availability Validation");

		PrintTemplate.verifyTrue("", resultsPage.getThumbnailImage(), "More info-Thumbnail Image Availability Validation");
		PrintTemplate.verifyTrue("", resultsPage.getvalidityPeriod(), "More info-validity Period Validation");
		PrintTemplate.verifyTrue("", resultsPage.getdefaultDisplayedShortDescription(), "More info-short Description Display Validation");
		PrintTemplate.verifyTrue(true, resultsPage.IsReadMoreLinkAvailable(), "More info-Read more link Validation");
		resultsPage.readmoreClick();
		PrintTemplate.verifyTrue("", resultsPage.gethiddenText(), "More info-hidden Text Validation");
		PrintTemplate.verifyTrue(true, resultsPage.MoreInfolinkAvailability(), "More info- More info Link Availability Validation");

		// PrintTemplate.markTableEnd();
		return PrintTemplate;

	}

	public ReportTemplate ItineraryReaderValidation_web(ReportTemplate PrintTemplate, Web_FxP_ResultsPage resultsPage) throws Exception {

		// PrintTemplate.setTableHeading("Package more info details-Itinerary Validations");
		HashMap<String, String> itineraryACTReader = resultsPage.itinerarydaywiseRead();
		System.out.println(itineraryACTReader.get("title_Day_1"));
		int lastindex = Integer.valueOf(itineraryACTReader.get("lastIndex"));

		for (int i = 1; i <= lastindex; i++) {
			try {
				PrintTemplate.verifyTrue("title_Day_" + i, itineraryACTReader.get("title_Day_" + i), "Day wise Itinerary Display - Day " + i + " Title Validation");
				System.out.println(itineraryACTReader.get("title_Day_" + i));
				PrintTemplate.verifyTrue("image_Day_" + i, itineraryACTReader.get("image_Day_" + i), "Day wise Itinerary Display - Day " + i + " image Validation");
				System.out.println(itineraryACTReader.get("image_Day_" + i));
				PrintTemplate.verifyTrue("content_Day_" + i, itineraryACTReader.get("content_Day_" + i), "Day wise Itinerary Display - Day " + i + " content Validation");
				System.out.println(itineraryACTReader.get("content_Day_" + i));
			} catch (Exception e) {

			}
		}

		// PrintTemplate.markTableEnd();
		return PrintTemplate;
	}

	public ReportTemplate TermsandConditionsValidator_web(ReportTemplate PrintTemplate, Web_FxP_ResultsPage resultsPage) throws Exception {
		// PrintTemplate.setTableHeading("Package more info details-Terms and conditions Validations");

		PrintTemplate.verifyTrue("", resultsPage.termsAndConditionsRead(), "Terms and Conditions Content Validation");

		// PrintTemplate.markTableEnd();
		return PrintTemplate;

	}

}
