package com.rezrobot.verifications.fixed_packages;

import java.util.HashMap;


import com.rezrobot.pages.CC.common.CC_Com_SearchCriteria;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.ReportTemplate;

public class FxP_Search_validation {

	
	public ReportTemplate bookingengineAvailability(CC_Com_SearchCriteria home,ReportTemplate PrintTemplate) {
		
		//PrintTemplate.setTableHeading("Booking Engine Validations");
		PrintTemplate.verifyTrue(true, home.isBECframeAvailable(), "Booking Engine Availability");
		PrintTemplate.verifyTrue(true, home.isFPPRoductEnabled(), "Booking Engine Product availability-Fixed package");

	//	PrintTemplate.verifyTrue(true, home.isFPBECAvailable(), "Fixed Package Booking Engine Availability");

		//PrintTemplate.markTableEnd();
		return PrintTemplate;
		
	}
	
	public ReportTemplate MandatoryfieldValidation(CC_Com_SearchCriteria home,ReportTemplate PrintTemplate) throws Exception {
		//PrintTemplate.setTableHeading("Booking Engine Mandatory Fields Validations");
		PrintTemplate.verifyTrue("Please Enter Country of Residence", home.mandatoryfieldsValidation("CountryOfResidence"), "Mandatory Fields Validations-Country Of Residence Field ");
		Thread.sleep(4000);
		home.clearEnteredData();
		PrintTemplate.verifyTrue("Please Enter Origin Location", home.mandatoryfieldsValidation("Origin"), "Mandatory Fields Validations-Origin");
		Thread.sleep(4000);
		home.clearEnteredData();
		PrintTemplate.verifyTrue("Please Enter Destination Location", home.mandatoryfieldsValidation("Destination"), "Mandatory Fields Validations-Destination");
		Thread.sleep(4000);
		home.clearEnteredData();
		
		
		return PrintTemplate;
	}
	
	/*public ReportTemplate Duration() {
		
	}*/
	
public ReportTemplate BECElementsValidation_web_Homepage(CC_Com_SearchCriteria home,ReportTemplate PrintTemplate) throws Exception {
		
		//PrintTemplate.setTableHeading("Booking Engine input Fields Validations");
		if (home.isFPPRoductEnabled()) {

			HashMap<String, Boolean> AvailMap=home.getAvailabilityOfInputFields_FP();

			PrintTemplate.verifyTrue(true, AvailMap.get("cor"), "Availability Of COR field");
			PrintTemplate.verifyTrue(true, AvailMap.get("includingflight"), "Availability Of Flight Inclusive field");
			PrintTemplate.verifyTrue(true, AvailMap.get("excludingflight"), "Availability Of flight Exclusive field");
			PrintTemplate.verifyTrue(true, AvailMap.get("origin"), "Availability Of origin Airport field");
			PrintTemplate.verifyTrue(true, AvailMap.get("destination"), "Availability Of Destination Airport field");

			PrintTemplate.verifyTrue(true, AvailMap.get("destination_land"), "Availability Of destination city field");
			PrintTemplate.verifyTrue(true, AvailMap.get("departuremonth"), "Availability Of departure month field");
			PrintTemplate.verifyTrue(true, AvailMap.get("duration"), "Availability Of duration field");  
		//	PrintTemplate.verifyTrue(true, AvailMap.get("package_type"), "Availability Of package type field");  
			//PrintTemplate.verifyTrue(true, AvailMap.get("prefferedcurrency_FP"), "Availability Of Prefferred currency field");  

			//PrintTemplate.markTableEnd();
		
	}
		return PrintTemplate;
	}

public ReportTemplate labelTextValidation(CC_Com_SearchCriteria home,ReportTemplate PrintTemplate) throws Exception {
	
	//PrintTemplate.setTableHeading("Label Text  Validations");

	HashMap<String, String> labelmap=home.getLabelTexts_FP();

	HashMap<String, String> expectedlblmap=new LabelReadProperties().becLabelFPConstructor();


	PrintTemplate.verifyTrue(expectedlblmap.get("FlightInclusivelabel"),labelmap.get("FlightInclusivelabel"),"Flight inclusive option Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("Flightexclusivelabel"),labelmap.get("Flightexclusivelabel"),"flight exclusive option Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("departureairportlabel"),labelmap.get("departureairportlabel"),"departure airport Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("destinationairportlabel"),labelmap.get("destinationairportlabel"),"Destination Airport Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("destinationcitylabel"),labelmap.get("destinationcitylabel"),"Destination city Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("departurelabel"),labelmap.get("departurelabel"),"Tour plan-Time Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("departuremonthlabel"),labelmap.get("departuremonthlabel"),"Departure month Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("durationlabel"),labelmap.get("durationlabel"),"Package Duration Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("packagetypelabel"),labelmap.get("packagetypelabel"),"Package Type Label Validation");  
	/*PrintTemplate.verifyTrue(expectedlblmap.get("preferredcurrencylabel"),labelmap.get("preferredcurrencylabel"),"PreferredCurrency Label Validation");  
	PrintTemplate.verifyTrue(expectedlblmap.get("showsearchoptionslabel"),labelmap.get("showsearchoptionslabel"),"Search more options Label Validation");  
*/



	//PrintTemplate.markTableEnd();
	return PrintTemplate;
	
}



}
