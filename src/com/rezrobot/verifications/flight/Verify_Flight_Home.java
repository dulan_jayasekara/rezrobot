package com.rezrobot.verifications.flight;

import java.util.HashMap;

import com.relevantcodes.extentreports.ExtentReports;
import com.rezrobot.pages.web.flights.Web_Flight_HomePage;
import com.rezrobot.utill.ReportTemplate;

public class Verify_Flight_Home {

	public static void generalHomePageValidaiton(ReportTemplate PrintTemplate, Web_Flight_HomePage home) {

		PrintTemplate.verifyTrue("Rezgateway", home.getPageTitel().trim(), "Home Page Titel Validation");
		//PrintTemplate.markTableEnd();
		
	}
	
	public static void verify_ElementAvailability(ReportTemplate PrintTemplate, Web_Flight_HomePage home, HashMap<String, Boolean> AvailMap) {
		
		System.out.println("Started --> Booking Engine Validations");
		
		//PrintTemplate.setTableHeading("Booking Engine Validations");
		PrintTemplate.verifyTrue(true, home.getBECAvailability(), "Booking Engine Availability");
		PrintTemplate.verifyTrue(true, AvailMap.get("cor"), "Availability Of COR field");
		PrintTemplate.verifyTrue(true, AvailMap.get("roundtrip"), "Availability Of Round trip radio button");
		PrintTemplate.verifyTrue(true, AvailMap.get("onewaytrip"), "Availability Of One Way trip radio button");
		PrintTemplate.verifyTrue(true, AvailMap.get("multidestination"), "Availability Of Multi-Destination radiobutton");
		PrintTemplate.verifyTrue(true, AvailMap.get("from"), "Availability Of From City field");
		PrintTemplate.verifyTrue(true, AvailMap.get("to"), "Availability Of To City field");
		PrintTemplate.verifyTrue(true, AvailMap.get("departuredate"), "Availability Of Departure Date field");
		PrintTemplate.verifyTrue(true, AvailMap.get("departuredateselector"), "Availability Of Departure Date Selector");
		PrintTemplate.verifyTrue(true, AvailMap.get("departuretime"), "Availability Of Departure Time drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("returndate"), "Availability Of Return Date field");
		PrintTemplate.verifyTrue(true, AvailMap.get("returndateselector"), "Availability Of Return Date Selector");
		PrintTemplate.verifyTrue(true, AvailMap.get("returntime"), "Availability Of Return Time drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("imflex"), "Availability Of Flexible Option checkbox");
		PrintTemplate.verifyTrue(true, AvailMap.get("adults"), "Availability Of Adult Count drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("children"), "Availability Of Children Count drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("infant"), "Availability Of Infant Count drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("showadditional"), "Availability Of Additional Option Click link");
		PrintTemplate.verifyTrue(true, AvailMap.get("hideshowadditional"), "Availability Of Hide Additional Option Click link");
		/*try {
			home.getElement("label_AdditionalSearchOption_class").click();
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		PrintTemplate.verifyTrue(true, AvailMap.get("prefair"), "Availability Of Preferred Airline field");
		PrintTemplate.verifyTrue(true, AvailMap.get("cabinclass"), "Availability Of Cabin Class drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("prefcurrency"), "Availability Of Preferred Currency drop down");
		PrintTemplate.verifyTrue(true, AvailMap.get("promotioncode"), "Availability Of Promotion Code field");
		PrintTemplate.verifyTrue(true, AvailMap.get("prefnonstop"), "Availability Of Prefer Non-Stop check box");
		PrintTemplate.verifyTrue(true, AvailMap.get("searchbutton"), "Availability Of Search Flights button");
		//PrintTemplate.markTableEnd();
	}
	
	public static void verify_BECLabelAvailability(ReportTemplate PrintTemplate, Web_Flight_HomePage home, HashMap<String, String> flightLabels, HashMap<String, String> actualBecLabels) { 
		
		System.out.println("Started --> BEC Label Text  Validations");
		
		//PrintTemplate.setTableHeading("BEC Label Text  Validations");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_flighticon_id"), actualBecLabels.get("Flightslabel"), "Flights label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_hotelicon_id"), actualBecLabels.get("Hotelslabel"), "Hotels label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_caricon_id"), actualBecLabels.get("Carslabel"), "Cars label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_activityicon_id"), actualBecLabels.get("Activitieslabel"), "Activities label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_transfericon_id"), actualBecLabels.get("Trasferslabel"), "Trasfers label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_fhicon_id"), actualBecLabels.get("Holidayslabel"), "Holidays label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_fpicon_id"), actualBecLabels.get("Packageslabel"), "Packages label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_air_country_lbl_div_id"), actualBecLabels.get("CORlabel"), "COR label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_triptype_xpath"), actualBecLabels.get("TripTypelabel"), "Trip Type label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_roundtrip_xpath"), actualBecLabels.get("RoundTriplabel"), "Round Trip label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_onewaytrip_xpath"), actualBecLabels.get("OneWayTriplabel"), "OneWay Trip label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_multidestination_xpath"), actualBecLabels.get("MultiDestinationlabel"), "Multi-Destination label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_whereyougoing_xpath"), actualBecLabels.get("WhereYouGoinglabel"), "Where You Going label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_from_xpath"), actualBecLabels.get("Fromlabel"), "From label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_to_id"), actualBecLabels.get("Tolabel"), "To label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_departuredate_xpath"), actualBecLabels.get("DepartureDatelabel"), "Departure Date label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_departuretime_xpath"), actualBecLabels.get("DepartureTimelabel"), "Departure Time label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_returndate_xpath"), actualBecLabels.get("ReturnDatelabel"), "Return Date label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_returntime_xpath"), actualBecLabels.get("ReturnTimelabel"), "Return Time label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_imflexible_xpath"), actualBecLabels.get("ImFlexiblelabel"), "I'm Flexible label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_howmanypeople_xpath"), actualBecLabels.get("HowManyPeoplelabel"), "How Many People label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_adults_xpath"), actualBecLabels.get("Adultslabel"), "Adults label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_children_xpath"), actualBecLabels.get("Childrenlabel"), "Children label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_infant_xpath"), actualBecLabels.get("Infantlabel"), "Infant label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_prefflight_xpath"), actualBecLabels.get("PrefFlightlabel"), "Preferred Flight label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_cabin_xpath"), actualBecLabels.get("Cabinlabel"), "Cabin label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_preferedcurrency_xpath"), actualBecLabels.get("PreferredCurrlabel"), "Preferred Currency label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_promotioncode_xpath"), actualBecLabels.get("PromoCodelabel"), "PromoCode label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_prefernonstop_xpath"), actualBecLabels.get("PrefNonStoplabel"), "Preferred Non Stop label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_ageofchildren_xpath"), actualBecLabels.get("AgeOfChildrenlabel"), "Age Of Children label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_AdditionalSearchOption_xpath"), actualBecLabels.get("AdditionalSearchlabel"), "Additional Search label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("HomePage_label_HideAdditionalSearchOption_xpath"), actualBecLabels.get("HideAdditionalSearchlabel"), "Additional Search label");
		//PrintTemplate.markTableEnd();
		
	}
	
	public static void verify_ComboBoxAttributes(ReportTemplate PrintTemplate, HashMap<String, String> expectedPortalProp, HashMap<String, String> actualPortalProp ) {
		
		System.out.println("Started --> BEC Combo Box Attributes Validation");
		
		//PrintTemplate.setTableHeading("BEC Combo Box Attributes Validation");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightcountryList"), actualPortalProp.get("countryOfResidence").replace("\n", " ") , "Country List verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightDepTime"), actualPortalProp.get("departureTime").replace("\n", " ") , "Number of nights verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightRetTime"), actualPortalProp.get("returnTime").replace("\n", " ") , "Number of rooms verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightnumberOfAdults"), actualPortalProp.get("adults").replace("\n", " ") , "Number of adults verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightnumberOfChildren"), actualPortalProp.get("children").replace("\n", " ") , "Number of children verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightchildAge"), actualPortalProp.get("childrenAge").replace("\n", " ") , "Children age verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightcabinclass"), actualPortalProp.get("cabins").replace("\n", " ") , "Cabin class verification");
		PrintTemplate.verifyTrue(expectedPortalProp.get("flightpreferredCurrency"), actualPortalProp.get("prefCurrency").replace("\n", " ") , "Preferred currency verification");
		//PrintTemplate.markTableEnd();
		
	}
	
	public static void verify_MandatoryFields(ReportTemplate PrintTemplate, HashMap<String, Boolean> MandatoryMap) {
		
		System.out.println("Started --> BEC Mandatory Status validation");
		
		//PrintTemplate.setTableHeading("BEC Mandatory Status validation");
		PrintTemplate.verifyTrue(true, MandatoryMap.get("cor"), "Mandatory validation Of to filed");
		//PrintTemplate.markTableEnd();
	}
	
}
