package com.rezrobot.verifications.flight;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.flight_circuitry.Costs;
import com.rezrobot.flight_circuitry.SearchObject;
import com.rezrobot.flight_circuitry.UIFlightItinerary;
import com.rezrobot.flight_circuitry.XMLPriceInfo;
import com.rezrobot.flight_circuitry.XMLPriceItinerary;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.pages.web.flights.Web_Flight_ResultsPage;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.CommonValidator;
import com.rezrobot.utill.ReportTemplate;





public class Verify_Flight_ResultPage {

	
	
	public static void generalResultsPageValidaiton(ReportTemplate PrintTemplate, Web_Flight_ResultsPage resultPage) {
		
		//PrintTemplate.setTableHeading("General Results Page Validations");		
		PrintTemplate.verifyTrue(true, resultPage.isPageAvailable(),"Results Page Availability");
		PrintTemplate.verifyTrue(true, resultPage.isResultsAvailable(),"Results Availability");
		//PrintTemplate.markTableEnd();
	}

	public static void defaultLabelValidation(ReportTemplate PrintTemplate, HashMap<String, String> flightLabels, HashMap<String, String> resultPageActualLabels) {
		
		//PrintTemplate.setTableHeading("RESULTS PAGE LABEL TEXT VALIDATIONS");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_link_HideAdditionFilter_xpath"), resultPageActualLabels.get("hideadditionalfilter"), "Hide Additional Filter label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_link_ShowAdditionFilter_xpath"), resultPageActualLabels.get("ShowAdditionFilter"), "Show Addition Filter label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_cabinclass_xpath"), resultPageActualLabels.get("cabinclass"), "cabinclass label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_prefAirline_xpath"), resultPageActualLabels.get("prefairline"), "prefairline label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_prefCurrency_xpath"), resultPageActualLabels.get("prefcurrency"), "prefcurrency label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_discount_xpath"), resultPageActualLabels.get("discount"), "discount label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_prefNonStop_xpath"), resultPageActualLabels.get("prefnonstop"), "prefnonstop label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_resultsAvailableFromTo_id"), resultPageActualLabels.get("resultsavailablefromto"), "resultsavailablefromto label");
		PrintTemplate.verifyContains(flightLabels.get("Results_label_resultsAvailable_class"), resultPageActualLabels.get("resultsavailable"), "resultsavailable label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_sortPrice_xpath"), resultPageActualLabels.get("sortprice"), "sortprice label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_sortBy_xpath"), resultPageActualLabels.get("sortby"), "sortby label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_myBasket_xpath"), resultPageActualLabels.get("mybasket"), "mybasket label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_noItemsSelected_xpath"), resultPageActualLabels.get("noitemselected"), "noitemselected label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_resetFiltersTop_xpath"), resultPageActualLabels.get("resetfiltertop"), "resetfiltertop label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_priceRange_xpath"), resultPageActualLabels.get("pricerange"), "pricerange label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_fromToPrice_id"), resultPageActualLabels.get("fromtopricefilter"), "fromtopricefilter label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_airlines_xpath"), resultPageActualLabels.get("filterheadingairline"), "filterheadingairline label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_duration_xpath"), resultPageActualLabels.get("filterheadingduration"), "filterheadingduration label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_layover_xpath"), resultPageActualLabels.get("filterheadinglayover"), "filterheadinglayover label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_flightleg_xpath"), resultPageActualLabels.get("filterheadingflightleg"), "filterheadingflightleg label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_flightTimes_xpath"), resultPageActualLabels.get("filterheadingflighttimes"), "filterheadingflighttimes label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_Seg1takeOffFlight_id"), resultPageActualLabels.get("seg1takeoff"), "seg1takeoff label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_Seg1landingFlight_id"), resultPageActualLabels.get("seg1landing"), "seg1landing label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_Seg2takeOffFlight_id"), resultPageActualLabels.get("seg2takeoff"), "seg2takeoff label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_Seg2landingFlight_id"), resultPageActualLabels.get("seg2landing"), "seg2landing label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_resetFiltersBottom_xpath"), resultPageActualLabels.get("resetfiltersbottom"), "resetfiltersbottom label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_showMatrix_id"), resultPageActualLabels.get("showmatrix"), "showmatrix label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_hideMatrix_id"), resultPageActualLabels.get("hidematrix"), "hidematrix label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_nonstop_xpath"), resultPageActualLabels.get("nonstop"), "nonstop label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_1stop_xpath"), resultPageActualLabels.get("1stop"), "1stop label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_2+stop_xpath"), resultPageActualLabels.get("2stop"), "2stop label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_showingPage_xpath"), resultPageActualLabels.get("showingpage"), "showingpage label");
		PrintTemplate.verifyEqualIgnoreCase(flightLabels.get("Results_label_flightdetails_xpath"), resultPageActualLabels.get("flightdetailslink"), "flightdetailslink label");
		//PrintTemplate.markTableEnd();
		
	}
	
	public static void inputFieldValidation(ReportTemplate PrintTemplate, HashMap<String, Boolean> resutlsPageAvailMap, Web_Flight_ResultsPage resultPage) {
		
		//PrintTemplate.setTableHeading("Results Page Input Fields Availability");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("AddHotels"), "Add Hotels Menu button");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("AddFlights"), "Add Flights Menu button");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("AddActivities"), "Add Activities Menu button");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("AddCars"), "Add Cars Menu button");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("AddTransfers"), "Add Transfers Menu button");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainTripType"), "Search Again Trip Type field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainFrom"), "Search Again From field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainTo"), "Search Again To field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainDateRange"), "Search Again DateRange field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainAdultChildCount"), "Search Again Adult Child Count field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainCabinClass"), "Search Again Cabin Class field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainPrefAirline"), "Search Again Preferred Airline field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainPrefCurrency"), "Search Again Preferred Currency field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainDiscount"), "Search Again Discount field");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainPrefNonStop"), "Search Again Preferred Flight CheckBox");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("HideAdditionFilter"), "Hide Addition Filter link text");
		try {
			resultPage.getElement("link_HideAdditionFilter_linktext").click();
		} catch (Exception e) {
			e.printStackTrace();
		}
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("ShowAdditionFilter"), "Show Additional Filter link text");
		PrintTemplate.verifyTrue(true, resutlsPageAvailMap.get("SearchAgainButton"), "Search Again button");
		try {
			resultPage.getElement("link_ShowAdditionFilter_linktext").click();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//PrintTemplate.markTableEnd();
		
	}
	
	public static void searchAgainValidation(ReportTemplate PrintTemplate, HashMap<String, String> autoSetPageValues, SearchObject searchObject ) {
		
		//PrintTemplate.setTableHeading("RESULTS PAGE - VALIDATION OF SET VALUES AFTER SEARCH");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getTriptype(), autoSetPageValues.get("searchAgainTripType"), "Search Again Trip Type");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getFrom().split("\\|")[0], autoSetPageValues.get("searchAgainFrom"), "Search Again From");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getTo().split("\\|")[0], autoSetPageValues.get("searchAgainTo"), "Search Again To");
		//SEARCH AGAIN VALIDATION
		String departuredate = "";
		try {
			departuredate = Calender.getDate("MM/dd/yyyy", "dd/MMM/yyyy", searchObject.getDepartureDate());
			departuredate = departuredate.replace("/", " ");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String returndate = "";
		try {
			returndate = Calender.getDate("MM/dd/yyyy", "dd/MMM/yyyy", searchObject.getReturnDate());
			returndate = returndate.replace("/", " ");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		PrintTemplate.verifyEqualIgnoreCase(departuredate.concat(" to ".concat(returndate)), autoSetPageValues.get("searchAgainDateRange"), "Search Again Date");
		//Search again pax count validation
		String adult	= searchObject.getAdult().concat(" Adult(s)");
		String child	= "";
		String inf		= "";
		if(!searchObject.getChildren().equals("0")) {
			if(searchObject.getChildren().equals("1")){
				child = searchObject.getChildren().concat(" Child");
			} else {
				child = searchObject.getChildren().concat(" Children");
			}
		}
		if(!searchObject.getInfant().equals("0")){
			inf = searchObject.getInfant().concat(" Infant(s)");
		}
		PrintTemplate.verifyContains(adult, autoSetPageValues.get("searchAgainPax"), "Search Again Adult count");
		PrintTemplate.verifyContains(child, autoSetPageValues.get("searchAgainPax"), "Search Again Child/Children count");
		PrintTemplate.verifyContains(inf, autoSetPageValues.get("searchAgainPax"), "Search Again Infant count");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getCabinClass(), autoSetPageValues.get("searchAgainCabinClass"), "Search Again Adult count");
		String str = autoSetPageValues.get("searchAgainPrefAirline");
		if(str.equals("")) {
			
			str = "NONE";
		}
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getPreferredAirline(), str, "Search Again Pref Airline");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getPreferredCurrency(), autoSetPageValues.get("searchAgainPrefCurrency"), "Search Again Pref Currency");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getDiscountCouponNo(), autoSetPageValues.get("searchAgainDiscount"), "Search Again Discount Field text");
		//PrintTemplate.markTableEnd();
		
	}
	
	public static void resultsAvailablilty_AfterSearchAgain(ReportTemplate PrintTemplate, Web_Flight_ResultsPage resultPage) {
		
		//PrintTemplate.setTableHeading("SEARCH AGAIN RESULTS AVAILABILITY VALIDATIONS");
		PrintTemplate.verifyTrue(true, resultPage.isResultsAvailable() , "Results Availability After Search Again");
		//PrintTemplate.markTableEnd();
	}
	
	public static void resultsItineraryLabelValidation(ReportTemplate PrintTemplate, Web_Flight_ResultsPage resultPage, HashMap<String, String> flightLabels) {
		
		ArrayList<UIFlightItinerary> UIflightItinerary = resultPage.getResultlist();
		//PrintTemplate.setTableHeading("RESULTS PAGE - RESULT ITINERARY LANGUAGE LABEL VALIDATION");
		
		for(int i = 0; i<resultPage.getResultlist().size(); i++)
		{
			UIFlightItinerary UI	= UIflightItinerary.get(i);
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_OutDurationLabel_class"), UI.getOutDuration_Label(), "Outbound Duration Label in Itinerary");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_OutStopsLabel_class"), UI.getOutStops_Label(), "Outbound No. of Stops Label in Itinerary");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_OutLayoverLabel_class"), UI.getOutLayover_Label(), "Outbound Layover Label in Itinerary");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_InDurationLabel_class"), UI.getInDuration_Label(), "Inbound Duration Label in Itinerary");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_InStopsLabel_class"), UI.getInStops_Label(), "Inbound No. of Stops Label in Itinerary");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_InLayoverLabel_class"), UI.getInLayover_Label(), "Inbound Layover Label in Itinerary");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_MoreDetailsTableHeader_class"), UI.getMoreDetails().getMoreDetailsTableHeader_Label(), "Itinerary More Details Table Header Label");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_MoreDetailsTableColumnHeaders_class"), UI.getMoreDetails().getMoreDetailsTableColumnHeaders_Label(), "Itinerary More Details Table Header Label");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_MoreOutStopsLabel_id"), UI.getMoreDetails().getOutboundNonStop_Label(), "Itinerary More Details Outbound Non Stop Label");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_MoreOutCabinClass_class"), UI.getMoreDetails().getOutboundCabinClass_Label(), "Itinerary More Details Outbound Cabin Class Label");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_MoreInStopsLabel_id"), UI.getMoreDetails().getInboundNonStop_Label(), "Itinerary More Details Inbound Non Stop Label");
			PrintTemplate.verifyContains(flightLabels.get("Results_Itinerary_label_MoreInCabinClass_class"), UI.getMoreDetails().getInboundCabinClass_Label(), "Itinerary More Details Inbound Cabin Class Label");
		}
		//PrintTemplate.markTableEnd();
	}
	
	public static void resultsDataValidation(ReportTemplate PrintTemplate, Web_Flight_ResultsPage resultPage, FareResponse fareResponse, String tripType) {
		
		ArrayList<UIFlightItinerary> UIflightItinerary = resultPage.getResultlist();
		ArrayList<XMLPriceItinerary> XMLpriceItinerary = fareResponse.getList();
		
		boolean roundUp = false;
		if(tripType.equalsIgnoreCase("Round Trip"))
		{
			roundUp = true;
		}
		
		//PrintTemplate.setTableHeading("SEARCH AGAIN RESULTS AVAILABILITY VALIDATIONS");
		PrintTemplate.verifyContains(resultPage.getNumofflights(), String.valueOf(XMLpriceItinerary.size()), "Number of flights Label");
		String reportfarereqpath = "<a href = ".concat(fareResponse.getUrl()).concat(" target='_blank'>").concat(fareResponse.getUrl()).concat("</a>");
		//PrintTemplate.addToInfoTabel("XML URL", reportfarereqpath);
		
		for(int i = 0; i<resultPage.getResultlist().size(); i++)
		{
			
			UIFlightItinerary UI	= UIflightItinerary.get(i);
			XMLPriceItinerary XML	= XMLpriceItinerary.get(i);
			
			//Label Validation
			
			String OBDepFlightCode		= "";
			OBDepFlightCode				= XML.getOriginoptions().get(0).getFlightlist().get(0).getMarketingAirline_Loc_Code();
			PrintTemplate.verifyContains(OBDepFlightCode, UI.getOBDepFlightCode(), "Displaying Outbound Departure Flight Code");
			
			String OBDepLocationCode	= "";
			OBDepLocationCode			= XML.getOriginoptions().get(0).getFlightlist().get(0).getDepartureLocationCode();
			PrintTemplate.verifyContains(OBDepLocationCode, UI.getOBDepLocationCode(), "Displaying Outbound Departure Location Code");
			
			String OBDepDate			= "";
			OBDepDate					= XML.getOriginoptions().get(0).getFlightlist().get(0).getDepartureDate();
			String UIOBDepDate			= "";
			try {
				UIOBDepDate				= Calender.getDate("MMM-dd-yyyy", "yyyy-MM-dd", UI.getOBDepDate().trim().replace(" ", "-"));
			} catch (ParseException e) {
				UIOBDepDate				= e.getMessage();
			}
			PrintTemplate.verifyContains(OBDepDate, UIOBDepDate, "Displaying Outbound Departure Date");
			
			String OBDepTime			= "";
			OBDepTime					= XML.getOriginoptions().get(0).getFlightlist().get(0).getDepartureTime();
			String UIOBDepTime			= "";
			UIOBDepTime					= CommonValidator.formatTimeToCommon(UI.getOBDepTime());
			PrintTemplate.verifyContains(OBDepTime, UIOBDepTime, "Displaying Outbound Departure Time");
			
			int listLength				= 0;
			listLength					= XML.getOriginoptions().get(0).getFlightlist().size();
			
			String OBArrFlightCode		= "";
			OBArrFlightCode				= XML.getOriginoptions().get(0).getFlightlist().get(listLength-1).getMarketingAirline_Loc_Code();
			PrintTemplate.verifyContains(OBArrFlightCode, UI.getOBArrFlightCode(), "Displaying Outbound Arrival Flight Code");
			
			String OBArrLocationCode	= "";
			OBArrLocationCode			= XML.getOriginoptions().get(0).getFlightlist().get(listLength-1).getArrivalLocationCode();
			PrintTemplate.verifyContains(OBArrLocationCode, UI.getOBArrLocationCode(), "Displaying Outbound Arrival Location Code");
			
			String OBArrDate			= "";
			OBArrDate					= XML.getOriginoptions().get(0).getFlightlist().get(listLength-1).getArrivalDate();
			String UIOBArrDate			= "";
			try {
				UIOBArrDate				= Calender.getDate("MMM-dd-yyyy", "yyyy-MM-dd", UI.getOBArrDate().trim().replace(" ", "-"));
			} catch (ParseException e) {
				UIOBArrDate				= e.getMessage();
			}
			PrintTemplate.verifyContains(OBArrDate, UIOBArrDate, "Displaying Outbound Arrival Date");
			
			String OBArrTime			= "";
			OBArrTime					= XML.getOriginoptions().get(0).getFlightlist().get(listLength-1).getArrivalTime();
			String UIOBArrTime			= "";
			UIOBArrTime					= CommonValidator.formatTimeToCommon(UI.getOBArrTime());
			PrintTemplate.verifyContains(OBArrTime, UIOBArrTime, "Displaying Outbound Arrival Time");
			
			String OBDuration			= "";
			OBDuration					= XML.getOriginoptions().get(0).getJourneyDuration().split(" ")[0].trim();
			String UIOBDuration			= "";
			UIOBDuration				= CommonValidator.formatTimeToCommon(UI.getOBDuration());
			PrintTemplate.verifyContains(OBDuration, UIOBDuration, "Displaying Outbound Duration");
		
			if(roundUp)
			{	
				String IBDepFlightCode		= "";
				IBDepFlightCode				= XML.getOriginoptions().get(1).getFlightlist().get(0).getMarketingAirline_Loc_Code();
				PrintTemplate.verifyEqualIgnoreCase(IBDepFlightCode, UI.getIBDepFlightCode(), "Displaying Inbound Departure Flight Code");
					
				String IBDepLocationCode	= "";
				IBDepLocationCode			= XML.getOriginoptions().get(1).getFlightlist().get(0).getDepartureLocationCode();
				PrintTemplate.verifyEqualIgnoreCase(IBDepLocationCode, UI.getIBDepLocationCode(), "Displaying Inbound Departure Location Code");	
				
				String IBDepDate			= "";
				OBDepDate					= XML.getOriginoptions().get(1).getFlightlist().get(0).getDepartureDate();
				String UIIBDepDate			= "";
				try {
					UIIBDepDate				= Calender.getDate("MMM-dd-yyyy", "yyyy-MM-dd", UI.getIBDepDate().trim().replace(" ", "-"));
				} catch (ParseException e) {
					UIIBDepDate				= e.getMessage();
				}
				PrintTemplate.verifyContains(IBDepDate, UIIBDepDate, "Displaying Inbound Departure Date");
				
				String IBDepTime			= "";
				IBDepTime					= XML.getOriginoptions().get(1).getFlightlist().get(0).getDepartureTime();
				String UIIBDepTime			= "";
				UIIBDepTime					= CommonValidator.formatTimeToCommon(UI.getIBDepTime());
				PrintTemplate.verifyContains(IBDepTime, UIIBDepTime, "Displaying Inbound Departure Time");
				
				int inListLength	= 0;
				inListLength		= XML.getOriginoptions().get(1).getFlightlist().size();
				
				String IBArrFlightCode		= "";
				IBArrFlightCode				= XML.getOriginoptions().get(1).getFlightlist().get(inListLength-1).getMarketingAirline_Loc_Code();
				PrintTemplate.verifyContains(IBArrFlightCode, UI.getIBArrFlightCode(), "Displaying Inbound Arrival Flight Code");
				
				String IBArrLocationCode	= "";
				IBArrLocationCode			= XML.getOriginoptions().get(1).getFlightlist().get(inListLength-1).getArrivalLocationCode();
				PrintTemplate.verifyContains(IBArrLocationCode, UI.getIBArrLocationCode(), "Displaying Inbound Arrival Location Code");
				
				String IBArrDate			= "";
				IBArrDate					= XML.getOriginoptions().get(1).getFlightlist().get(listLength-1).getArrivalDate();
				String UIIBArrDate			= "";
				try {
					UIIBArrDate				= Calender.getDate("MMM-dd-yyyy", "yyyy-MM-dd", UI.getIBArrDate().trim().replace(" ", "-"));
				} catch (ParseException e) {
					UIIBArrDate				= e.getMessage();
				}
				PrintTemplate.verifyContains(IBArrDate, UIIBArrDate, "Displaying Inbound Arrival Date");
				
				String IBArrTime			= "";
				IBArrTime					= XML.getOriginoptions().get(1).getFlightlist().get(inListLength-1).getArrivalTime();
				String UIIBArrTime			= "";
				UIIBArrTime					= CommonValidator.formatTimeToCommon(UI.getIBArrTime());
				PrintTemplate.verifyContains(IBArrTime, UIIBArrTime, "Displaying Inbound Arrival Time");
				
				String IBDuration			= "";
				OBDuration					= XML.getOriginoptions().get(1).getJourneyDuration().split(" ")[0].trim();
				String UIIBDuration			= "";
				UIIBDuration				= CommonValidator.formatTimeToCommon(UI.getIBDuration());
				PrintTemplate.verifyContains(IBDuration, UIIBDuration, "Displaying Inbound Duration");
				
			}//twoway
			
			
			String responseTotalWithoutCCFee = XML.getPricinginfo().getInSellingCurrency().getTotalWithoutCCFee();
			String resultsPgTotalWithoutCCFee = UI.getTotalCost();
			PrintTemplate.verifyContains(responseTotalWithoutCCFee, resultsPgTotalWithoutCCFee, "Displaying Itinerary Cost");
			
		
		}//END OF FOR LOOP
		
		//PrintTemplate.markTableEnd();	
	}

	public static void cartValidation(ReportTemplate PrintTemplate, HashMap<String,String> actualCartDetails, Costs expectedCosts)
	{
		//PrintTemplate.setTableHeading("CART VALUES VALIDATIONS (RESULTS PAGE)");
		
		String actualBasketPrice = actualCartDetails.get("ActualBasketPrice");
		String actualBasketPriceCurrency = actualCartDetails.get("ActualBasketPriceCurrency");
		String expectedBasketPrice = expectedCosts.getTotalWithoutCCFee();
		String expectedBasketPriceCurrency = expectedCosts.getCurrencyCode();
		PrintTemplate.verifyEqualIgnoreCase(expectedBasketPrice, actualBasketPrice, "Basket Price");
		PrintTemplate.verifyEqualIgnoreCase(expectedBasketPriceCurrency, actualBasketPriceCurrency, "Basket Price Currecny");
		
		
		//PrintTemplate.markTableEnd();
	}
	
	public static void isPriceChange(ReportTemplate PrintTemplate, XMLPriceInfo selectedLowFareResponseFlight, XMLPriceInfo priceResponseFlight)
	{
		
		String baseAmountOfLowFareResponse	= selectedLowFareResponseFlight.getInSupplierCurrency().getBasefareAmount();
		String baseAmountOfPriceResponse	= priceResponseFlight.getInSupplierCurrency().getBasefareAmount();
		
		//PrintTemplate.setTableHeading("RATE CHANGE WHEN ADDING TO CART (CHECK PRICE DIFFERENECE BETWEEN LOWFARERESPONSE & PRICERESPONSE)");
		PrintTemplate.verifyEqualIgnoreCase(baseAmountOfLowFareResponse, baseAmountOfPriceResponse, "Check whether LowFare Rates and Price Response Rates are different");
		//PrintTemplate.markTableEnd();
		
		if(baseAmountOfLowFareResponse.equalsIgnoreCase(baseAmountOfPriceResponse))
		{
			selectedLowFareResponseFlight.setIschanged(true);
		}
		
	}
	
	
}
