package com.rezrobot.types;

public enum BookingType {
	
WITHINCANCELLATION,OUTSIDECANCELLATION,NONE;
	
	public static BookingType getBookingType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("WITHINCANCELLATION"))return WITHINCANCELLATION;
		else if(ChargeType.equalsIgnoreCase("OUTSIDECANCELLATION"))return OUTSIDECANCELLATION;
	    else return NONE;
		
	}

}
