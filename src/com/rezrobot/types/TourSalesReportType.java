package com.rezrobot.types;

public enum TourSalesReportType {

	SALES,REFUND, NONE;

	public static TourSalesReportType getTourOperatorType(String reporttype) {

		if (reporttype.trim().equalsIgnoreCase("SALES"))
			return TourSalesReportType.SALES;
		else if (reporttype.trim().equalsIgnoreCase("REFUND"))
			return TourSalesReportType.REFUND;
        else
			return TourSalesReportType.NONE;
	}

}
