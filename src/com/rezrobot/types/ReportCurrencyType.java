package com.rezrobot.types;

public enum ReportCurrencyType {

	SELLINGCURR,BASECURR, NONE;

	public static ReportCurrencyType getTourOperatorType(String reporttype) {

		if (reporttype.trim().equalsIgnoreCase("SELLINGCURR"))
			return ReportCurrencyType.SELLINGCURR;
		else if (reporttype.trim().equalsIgnoreCase("BASECURR"))
			return ReportCurrencyType.BASECURR;
        else
			return ReportCurrencyType.NONE;
	}

}
