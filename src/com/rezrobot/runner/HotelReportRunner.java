package com.rezrobot.runner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.HotelObject;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_BookingConfirmation_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_BookingCard;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_LiveBookingMoreInfo;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.CC.common.CC_Com_Cancellation_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_Cancellation_Report_criteria;
import com.rezrobot.pages.CC.common.CC_Com_ConfirmationMail_Report_hotel;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLossReport_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLoss_Report;
import com.rezrobot.pages.CC.common.CC_Com_ReservationReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_Reservation_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_criteria;
import com.rezrobot.pages.CC.common.CC_Com_VoucherMail_Hotel;
import com.rezrobot.pages.CC.hotel.CC_Hotel_CancellationPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
import com.rezrobot.verifications.Hotel_Verify;

public class HotelReportRunner {
	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private String Browser = "N/A";
	
	//extent report
	private 												ReportTemplate					PrintTemplate;
	private 												HtmlReportTemplate				HtmlReportTemplate;
	private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	
	private CC_Com_ReservationReport_criteria				resReportCriteria				= new CC_Com_ReservationReport_criteria();
	private CC_Com_Reservation_Report						resReport						= new CC_Com_Reservation_Report(); 
	private CC_Com_BookingListReport_criteria				bookingListCriteria				= new CC_Com_BookingListReport_criteria();
	private CC_Com_BookingList_Report						bookingList						= new CC_Com_BookingList_Report();
	private CC_Com_BookingList_LiveBookingMoreInfo			bookingListLiveBooking			= new CC_Com_BookingList_LiveBookingMoreInfo();
	private CC_Com_BookingListReport_BookingCard			bookingListBookingCard			= new CC_Com_BookingListReport_BookingCard();
	private CC_Com_ProfitAndLossReport_Criteria				profitAndLossCriteria			= new CC_Com_ProfitAndLossReport_Criteria();
	private CC_Com_ProfitAndLoss_Report						profitAndLossReport				= new CC_Com_ProfitAndLoss_Report();
	private CC_Com_ThirdPartySupplierPayable_criteria		thirdPartySupPayableCriteria	= new CC_Com_ThirdPartySupplierPayable_criteria();
	private CC_Com_ThirdPartySupplierPayable_Report			thirdPartySupPayableReport		= new CC_Com_ThirdPartySupplierPayable_Report();
	private CC_Com_BookingConfirmation_criteria 			confirmationMailCriteria		= new CC_Com_BookingConfirmation_criteria();
	private CC_Com_ConfirmationMail_Report_hotel			confirmationMail				= new CC_Com_ConfirmationMail_Report_hotel();					
	private CC_Com_LoginPage								login							= new CC_Com_LoginPage();
	private CC_Com_BO_Menu									bomenu							= new CC_Com_BO_Menu();
	private CC_Com_VoucherMail_Hotel						voucher							= new CC_Com_VoucherMail_Hotel();
	private CC_Com_Cancellation_Criteria					cancellationCriteria			= new CC_Com_Cancellation_Criteria();
	private CC_Hotel_CancellationPage						cancellation					= new CC_Hotel_CancellationPage();
	private	CC_Com_Cancellation_Report_criteria				cancellationReportCriteria		= new CC_Com_Cancellation_Report_criteria();
	
	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalDetails 					= labelProperty.getPortalSpecificData();
	
	//serialized object
	private HotelObject 									allDetails 						= new HotelObject();
	private Reservation										hotelResObject					= new Reservation();
	
	@Before
	public void setUp() throws Exception {
		// logger = Logger.getLogger(this.getClass());
		// DOMConfigurator.configure("log4j.xml");
		/*PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(PrintWriter);*/
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalDetails.get("PortalName");
		report = new ExtentReports("Reports/"+ portal+ "/Hotel-ReportOnly-"+ today +".html", NetworkMode.OFFLINE);
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
	}
	
	@Test
	public void test() throws Exception {
		
		Reservation 							object 					= Serializer.DeSerializeReservation("../Rezrobot_Details/object/date/portal/Hotel/Confirmed_Reservations/", portalDetails.get("Portal.Name"));
												allDetails 				= object.getHotelReservation();
		
									
		HashMap<String, String> 				portalSpecificData 		= allDetails.getPortalSpecificData();
		Hotel_Verify 							verify 					= new Hotel_Verify();
		HotelConfirmationPageDetails 			confirmation 			= allDetails.getConfirmation();
		hotelDetails 							reservationRes			= allDetails.getReservationRes();
		HashMap<String, String> 				paymentDetails 			= allDetails.getPaymentDetails();
		HashMap<String, String> 				currencyMap				= allDetails.getCurrencyMap();
		ArrayList<HotelOccupancyObject> 		occupancyList 			= allDetails.getOccupancyList();
		HotelSearchObject 						search 					= allDetails.getSearch();
		String									defaultCC				= allDetails.getDefaultCurrency();
		String									canDeadline				= allDetails.getCanDeadline();
		hotelDetails 							roomAvailablityRes		= allDetails.getRoomAvailablityRes();
		HashMap<String, String> 				commonLabels			= allDetails.getCommonLabels();
		HashMap<String, String> 				hotelLabels				= allDetails.getHotelLabels();
		ArrayList<HashMap<String, String>> 		websiteDetails 			= allDetails.getWebsiteDetails();
		ArrayList<hotelDetails> 				hotelAvailabilityRes	= allDetails.getHotelAvailabilityRes(); 
		HashMap<String, String> 				configDetails			= allDetails.getConfigDetails();
		String 									reportDate				= allDetails.getReportDate();
		
		
		
		//log to check reports
		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
//		PrintTemplate.setTableHeading("Check login page availability (To check reports)");
		PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
//		PrintTemplate.markTableEnd();
		if(login.isLoginPageAvailable())
		{
			login.typeUserName(portalSpecificData.get("hotelUsername"));
			login.typePassword(portalSpecificData.get("hotelPassword"));
			login.loginSubmit();
			
//			PrintTemplate.setTableHeading("Check BO menu page availability");
			PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
//			PrintTemplate.markTableEnd();
			if(bomenu.isModuleImageAvailable())
			{
				//reservation report criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("reservationReport")));
				HashMap<String, Boolean> 	resReportCriteriaComboboxAvailability 			= resReportCriteria.checkComboboxAvailability();
				HashMap<String, Boolean> 	resReportCriteriaLabelsAvailability  			= resReportCriteria.checkLabelAvailability();
				HashMap<String, Boolean> 	resReportCriteriaRadioButtonsAvailability  		= resReportCriteria.checkRadioButtonAvailability();
				HashMap<String, Boolean> 	resReportCriteriaButtonsAvailability  			= resReportCriteria.checkButtonxAvailability();
				HashMap<String, String>		resReportCriteriaLabels							= resReportCriteria.getLabels();
				
				
				//reservation report criteria check element availability
				verify.reservationReportCriteriaButtonsAvailability						(PrintTemplate, resReportCriteriaButtonsAvailability );
				verify.reservationReportCriteriaComboboxAvailability					(PrintTemplate, resReportCriteriaComboboxAvailability );
				verify.reservationReportCriteriaLabelsAvailability						(PrintTemplate, resReportCriteriaLabelsAvailability );
				verify.reservationReportCriteriaRadioButtonsAvailability				(PrintTemplate, resReportCriteriaRadioButtonsAvailability );
				verify.reservationReportCriteriaLabelsVerification						(PrintTemplate, resReportCriteriaLabels, commonLabels);
				
				//reservation report - enter criteria
				resReportCriteria.enterCriteria(confirmation.getReservationNumber(), "hotel");
				
				// reservation report
				// reservation report get labels
				HashMap<String, Boolean> 	resReportLabelAvailability 							= resReport.hotelResReportLabelAvailability();
				HashMap<String, String> 	resReportLabels										= resReport.hotelResReportGetLabels();
				
				// reservation report check element availability
				//verify.resReportLabelAvailability(PrintTemplate, resReportLabelAvailability);
				//reservation report check labels
				//verify.resReportLabelVerification(PrintTemplate, resReportLabels, commonLabels);
				
				//reservation report get values
				HashMap<String, Boolean> 	resReportValueAvailability							= resReport.hotelResReportGetValueAvailability();
				HashMap<String, String>		resReportVales										= resReport.getValuesHotel();
				
				//reservation report check values availability
				//verify.resReportValueAvailability(PrintTemplate, resReportValueAvailability);	
				
				//reservation report values verification
				verify.reservationReportVerify(PrintTemplate, resReportVales, reservationRes, paymentDetails, currencyMap, portalSpecificData, occupancyList, confirmation, search, defaultCC, reportDate);
				
				//booking list report criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("bookingListReport")));
				HashMap<String, Boolean> 	bookingListReportCriteriaLabelAvailability 			= bookingListCriteria.getLabelAvailability();
				HashMap<String, String> 	bookingListCriteriaLabels 							= bookingListCriteria.getLabels();
				verify.bookingListReportCriteriaLabelAvailability(PrintTemplate, bookingListReportCriteriaLabelAvailability);
				verify.bookingListReportCriteriaLabelVerification(PrintTemplate, bookingListCriteriaLabels, commonLabels);
				
				// booking list report enter search criteria
				bookingListCriteria.enterCriteria(confirmation.getReservationNumber());
				
				// booking list report get labels
				HashMap<String, Boolean> 	bookingListLabelAvailability 						= bookingList.getHotelLabelsAvailability();
				HashMap<String, String>	 	bookingListLabels 									= bookingList.getHotelLabels();
				HashMap<String, String> 	bookingListValues 									= bookingList.getHotelValues();
				verify.bookingListLabelAvailability(PrintTemplate, bookingListLabelAvailability);
				verify.bookingListLabelVerification(PrintTemplate, bookingListLabels, commonLabels);
				verify.bookingListValueVarification();
				
				//booking list - live booking more info
				HashMap<String, Boolean> 	bookingListLiveLabelAvailability 					= bookingListLiveBooking.getHotelLabelAvailability();
				HashMap<String, String>	 	bookingListLIveLabels 								= bookingListLiveBooking.getHotelLabels();
				HashMap<String, String>	 	bookingListLIveValues 								= bookingListLiveBooking.getHotelValues();
				bookingListLiveBooking.closeHotelMoreInfo();
				bookingList.clickHotelReservationNumber();
				verify.bookingListLiveLabelAvailability(PrintTemplate, bookingListLiveLabelAvailability);
				verify.bookingListLiveLabelVerification(PrintTemplate, bookingListLIveLabels, hotelLabels, search);
				verify.bookingListLiveValueVerification(PrintTemplate, bookingListLIveValues, reservationRes, search, defaultCC, currencyMap, bookingListLIveLabels, portalSpecificData);
				
				// booking list - booking card
				HashMap<String, Boolean> 	bookingListBookingCardlabelAvailability  			= bookingListBookingCard.getHotelLabelAvailability();
				HashMap<String, String>	 	bookingListBookingCardLabels 						= bookingListBookingCard.getHotelLabels();
				HashMap<String, String>	 	bookingListBookingCarddValues 						= bookingListBookingCard.getHotelValues();
				verify.bookingListBookingCardLabelAvailability(PrintTemplate, bookingListBookingCardlabelAvailability);
				verify.bookingListBookingCardLabelVarify(PrintTemplate, hotelLabels, bookingListBookingCardLabels, search);
				verify.bookingListBookingCardValueVerify(PrintTemplate, bookingListBookingCarddValues, search, reservationRes, defaultCC, currencyMap, portalSpecificData, confirmation, paymentDetails, canDeadline, roomAvailablityRes, occupancyList, reportDate);
				
				// profit and loss report - Enter criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("profitNlossReportUrl")));
				profitAndLossCriteria.enterCriteria(reportDate);
				// profit and loss report - get value
				HashMap<String, String> 	profitAndLossReportValues 							= profitAndLossReport.getValues(confirmation.getReservationNumber());
				//profit and loss report - value verification
				verify.profitAndLossReportValuesVerification(PrintTemplate, profitAndLossReportValues, search, portalSpecificData, defaultCC, currencyMap, reservationRes, confirmation, reportDate, occupancyList);
			
				// third party supplier payable report - enter criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("thirdPartySupplierPayableUrl")));
				thirdPartySupPayableCriteria.enterCriteria(reportDate);
				// third party supplier payable report - get values
				HashMap<String, String> 	thirdPartySupplierValues 							= thirdPartySupPayableReport.getValues(confirmation);
				// third party supplier payable report - values verifications
				verify.thirdPartySupplierPayableValuesVerification(PrintTemplate, reservationRes, thirdPartySupplierValues, search, defaultCC, currencyMap, portalSpecificData, confirmation, occupancyList, reportDate);
			
				// confirmation mail enter criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("confirmationreportUrl")));
				confirmationMailCriteria.getHotelConfirmationMail(confirmation.getReservationNumber());
				
				// confirmation mail read
				HashMap<String, String> confrmationMail = confirmationMail.readHotel();
				verify.confirmationMail(PrintTemplate, confrmationMail, portalSpecificData, reservationRes, websiteDetails, confirmation, currencyMap, defaultCC, hotelAvailabilityRes, paymentDetails, occupancyList, search, roomAvailablityRes);
			
				//voucher mail
				HashMap<String, String> voucherMail = voucher.getValues();
				verify.voucherMail(PrintTemplate, voucherMail, confirmation, occupancyList, websiteDetails, portalSpecificData, reservationRes, search, roomAvailablityRes);	
			}	
		}
	}
	
	@After
	public void tearDown() throws Exception {
		System.out.println("Quit");
	}
}
