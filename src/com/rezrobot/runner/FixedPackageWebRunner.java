package com.rezrobot.runner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.rezrobot.dataobjects.fixed_package_objects.FxP_DataReader;
import com.rezrobot.dataobjects.fixed_package_objects.FxP_InputObject;

import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.pages.web.fixed_packages.Web_FxP_ResultsPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.processor.fixed_package_processor.BECDropdownOptionsConstructor;
import com.rezrobot.processor.fixed_package_processor.LabelMapConstructor;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.verifications.fixed_packages.FxP_HomePege_Validation;
import com.rezrobot.verifications.fixed_packages.FxP_ResultsPage_Validations;

public class FixedPackageWebRunner {

	/**
	 * @param args
	 */


	ArrayList<FxP_InputObject> inputlist;
	private FxP_InputObject input;
	private String bookingChannel;
	private String partnerType;

	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";

	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalSpecificData 				= labelProperty.getPortalSpecificData();
	
	@Before
	public void setUp() throws Exception {

		// logger = Logger.getLogger(this.getClass());
		// DOMConfigurator.configure("log4j.xml");
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		//PrintTemplate.Initialize(PrintWriter);
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlWeb"));
		
	}

	@Test
	public void test() throws Exception {

		inputlist=new FxP_DataReader().inputObjectConstruct();
		Iterator<FxP_InputObject> inputiterator=inputlist.iterator();
		while (inputiterator.hasNext()) {

			input=inputiterator.next();
			bookingChannel=input.getTest().getBookingChannel();
			partnerType=input.getTest().getPartnertype();


			if (bookingChannel.contains("web") && partnerType.contains("DC")) {



				//PrintTemplate.markReportBegining("Rezgateway WEB Fixed Package Flow Smoke Test");
				//PrintTemplate.setInfoTableHeading("Test Basic Details");

				Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
				Browser = caps.getBrowserName();

				//PrintTemplate.addToInfoTabel("Browser Type", caps.getBrowserName());
				//PrintTemplate.addToInfoTabel("Browser Version", caps.getVersion());
				//PrintTemplate.addToInfoTabel("Platform", caps.getPlatform().toString());
				//PrintTemplate.markTableEnd();

				Web_Com_HomePage home=new Web_Com_HomePage();

				FxP_HomePege_Validation homePageValidations=new FxP_HomePege_Validation();
				PrintTemplate=homePageValidations.generalVerifications_web_Homepage(home,PrintTemplate);


				PrintTemplate=homePageValidations.BECAvailability_web_Homepage(home, PrintTemplate);
				PrintTemplate=homePageValidations.BECElementsValidation_web_Homepage(home, PrintTemplate);
				PrintTemplate=homePageValidations.labelTextValidation(home, PrintTemplate);
				PrintTemplate=homePageValidations.DepartureMonthoptionsValidation(home, PrintTemplate);
				PrintTemplate=homePageValidations.DurationOptionsValidation(home, PrintTemplate);
				PrintTemplate=homePageValidations.packageTypeOptionsValidation(home, PrintTemplate);

				//PrintTemplate.setTableHeading("Combo Box Attributes-preferred currency Validation");

				//PrintTemplate.markTableEnd();

				home.doSearch_FP(input.getSearch());
				home.proceedToNext_FP();

				Web_FxP_ResultsPage resultsPage=new Web_FxP_ResultsPage();
				FxP_ResultsPage_Validations resultValidation=new FxP_ResultsPage_Validations();

//				PrintTemplate=resultValidation.generalVerifications_web_ResultsPage(PrintTemplate, resultsPage);
//				PrintTemplate=resultValidation.searchAgainBECVerifications_web_ResultPage(PrintTemplate, resultsPage);
				
				resultsPage.traverseResults();

				//System.out.println(resultsPage.getnumberofPages());


				PrintWriter.append("</body></html>");
				BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/FixedPackage_" + this.getClass().getSimpleName() + "_"
						+ Browser + ".html")));
				sbwr.write(PrintWriter.toString());
				sbwr.flush();
				sbwr.close();
				PrintWriter.setLength(0);
			}
		}




	}

	@After
	public void tearDown() throws Exception {
		//DriverFactory.getInstance().getDriver().close();
		/*PrintWriter.append("</body></html>");
		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/FixedPackage_" + this.getClass().getSimpleName() + "_"
				+ Browser + ".html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();
		PrintWriter.setLength(0);*/
	}




	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
