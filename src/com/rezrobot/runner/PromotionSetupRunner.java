package com.rezrobot.runner;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.dataobjects.PromotionSetupdetails;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.PoiExcelReader;
import com.rezrobot.pages.backoffice.CC_Hotel_Promotion_Setup_Standard;


/**
 * @author ${charitha}
 * 
 *         ${tags}
 */

public class PromotionSetupRunner {
	private LabelReadProperties labelProperty = new LabelReadProperties();
	private HashMap<String, String> portalSpecificData = labelProperty.getPortalSpecificData();
	private HashMap<String, String> configproperties = labelProperty.getConfigProperties();

	private ExtentReportTemplate extentReportTemplate = null;
	private ExtentReports report = null;

	private String Browser = "N/A";

	public String url = "file://///192.168.1.250/UserFolders/charitha/hotel-promotion-setup-app/src/index.html";

	private String Promotionsetupexcel = portalSpecificData.get("PromotionSetupNew.Excel.path");

	@Before
	public void setUp() throws Exception  {
		
		
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalSpecificData.get("PortalName");
		report = new ExtentReports(new File("Reports").getAbsolutePath() + "/" + portal + "/DMC-Reservation-" + today + ".html", NetworkMode.OFFLINE);
		extentReportTemplate = ExtentReportTemplate.getInstance();
//		extentReportTemplate.Initialize(report);

		System.setProperty("http.proxyHost", "192.168.1.132");
		System.setProperty("http.proxyPort", "3128");
	}

	@Test
	public void test() throws Exception {

		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		Map<String, String> info = new HashMap<String, String>();

		info.put("Browsertype", caps.getBrowserName());
		info.put("Browserversion", caps.getVersion());
		info.put("Platform", caps.getPlatform().toString());
		info.put("promosetup Excel path", portalSpecificData.get("PromotionSetupNew.Excel.path"));
		info.put("Portal Url", portalSpecificData.get("PortalUrlCC"));
		info.put("Portal Username", portalSpecificData.get("DMCUsername"));
		info.put("Platform", caps.getPlatform().toString());
		// extentReportTemplate.setSystemInfo(info);

		DriverFactory.getInstance().getDriver().get(url);

		// Reading excel

		PoiExcelReader newReader = new PoiExcelReader();
		FileInputStream fs = new FileInputStream(new File(Promotionsetupexcel));

		ArrayList<Map<Integer, String>> contentList = newReader.reader(fs);

		Map<Integer, String> content = contentList.get(0);

		Iterator<Map.Entry<Integer, String>> mapiterator = content.entrySet().iterator();

		while (mapiterator.hasNext()) {

			Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>) mapiterator.next();
			String Content = Entry.getValue();
			String[] promoDetails = Content.split(",");

			PromotionSetupdetails Promodetailsobject = new PromotionSetupdetails();
			PromotionSetupdetails setupdetails = Promodetailsobject.setData(promoDetails);

			if (configproperties.get("Promotion.Enabled").equalsIgnoreCase("enable")) {

				CC_Hotel_Promotion_Setup_Standard promosetup = new CC_Hotel_Promotion_Setup_Standard();

				promosetup.setuppromo(setupdetails);

			}

		}

	}

	@After
	public void tearDown() throws Exception {
	}

}
