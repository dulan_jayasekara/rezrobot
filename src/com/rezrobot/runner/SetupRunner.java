package com.rezrobot.runner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.rezrobot.pages.CC.common.CC_Com_AAA;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ReportTemplate;

public class SetupRunner {
	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";
	
	@Before
	public void setUp() throws Exception {

		// logger = Logger.getLogger(this.getClass());
		// DOMConfigurator.configure("log4j.xml");
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter*/);
		DriverFactory.getInstance().initializeDriver();
		
	}
	
	@Test
	public void test() throws Exception {
		//PrintTemplate.markReportBegining("Rezgateway WEB Hotel Flow Smoke Test");
		//PrintTemplate.setInfoTableHeading("Test Basic Details");
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		
		/*PrintTemplate.addToInfoTabel("Browser Type", caps.getBrowserName());
		PrintTemplate.addToInfoTabel("Browser Version", caps.getVersion());
		PrintTemplate.addToInfoTabel("Platform", caps.getPlatform().toString());
		PrintTemplate.markTableEnd();*/
		
		CC_Com_AAA 										aaaSetup 				= new CC_Com_AAA();
		CC_Com_LoginPage								login					= new CC_Com_LoginPage();
		CC_Com_BO_Menu									bomenu					= new CC_Com_BO_Menu();
		
		//property files
		LabelReadProperties 							labelProperty 			= new LabelReadProperties();
		HashMap<String, String> 						portalSpecificData 		= labelProperty.getPortalSpecificData();
			
		//login
		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
		//PrintTemplate.setTableHeading("Check login page availability");
		PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
		//PrintTemplate.markTableEnd();
		
		if(login.isLoginPageAvailable())
		{
			login.typeUserName(portalSpecificData.get("hotelUsername"));
			login.typePassword(portalSpecificData.get("hotelPassword"));
			login.loginSubmit();
			
			//PrintTemplate.setTableHeading("Check BO menu page availability");
			PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
			//PrintTemplate.markTableEnd();
			if(bomenu.isModuleImageAvailable())
			{
				aaaSetup.setup(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("aaaSetup")), PrintTemplate);
			}
		}
	}
	
	@After
	public void tearDown() throws Exception {
		//DriverFactory.getInstance().getDriver().quit();
		System.out.println("quit");
		PrintWriter.append("</body></html>");
		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/" + this.getClass().getSimpleName() + "_"
						+ Browser + ".html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();
		PrintWriter.setLength(0);
	}
}
