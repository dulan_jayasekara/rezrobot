package com.rezrobot.runner;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlType;

import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.dataobjects.DMCSearchObject;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.enumtypes.XMLFileType;
import com.rezrobot.enumtypes.XMLLocateType;
import com.rezrobot.flight_circuitry.FlightReservation;
import com.rezrobot.flight_circuitry.Initiator;
import com.rezrobot.flight_circuitry.ReservationPassengerInfo;
import com.rezrobot.flight_circuitry.xml_object_readers.FareRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.FareResponseReader;
import com.rezrobot.flight_circuitry.xml_object_readers.PriceRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.PriceResponseReader;
import com.rezrobot.flight_circuitry.xml_objects.FareRequest;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.flight_circuitry.xml_objects.PriceRequest;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;
import com.rezrobot.pages.CC.common.CC_Com_Admin_ConfigurationPage;
import com.rezrobot.pages.CC.common.CC_Com_Admin_Home;
import com.rezrobot.pages.CC.common.CC_Com_Admin_WebSiteDetails;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_HomePage;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.web.common.Web_Com_BillingInformationPage;
import com.rezrobot.pages.web.common.Web_Com_CustomerDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.pages.web.common.Web_Com_NotesPage;
import com.rezrobot.pages.web.common.Web_Com_OccupancyDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_PaymentGateway;
import com.rezrobot.pages.web.common.Web_Com_PaymentsPage;
import com.rezrobot.pages.web.common.Web_Com_TermsPage;
import com.rezrobot.pages.web.common.Web_Com_Verify_selected_products;
import com.rezrobot.pages.web.fixed_packages.Web_FxP_ResultsPage;
import com.rezrobot.pages.web.flights.Web_Flight_PassengerDetails;
import com.rezrobot.pages.web.dmc.WEB_DMC_ConfirmationPage;
import com.rezrobot.pages.web.dmc.Web_DMC_ComponentListingPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.processor.DMC_Package_Processor.DMCPackage_RateConstructor;
import com.rezrobot.types.UserType;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.FlightJsoupDoc;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.PoiExcelReader;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.xmlPathGenerator;
import com.rezrobot.verifications.Hotel_Verify;
import com.rezrobot.verifications.DMC_Verify;
import com.rezrobot.verifications.fixed_packages.FxP_ResultsPage_Validations;
import com.rezrobot.utill.dmc_inputdataLoader_XmlReader;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;

/**
 * @author ${charitha}
 * 
 *         ${tags}
 */

public class DMCRunner {

	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private String Browser = "N/A";
	// property files
	private LabelReadProperties labelProperty = new LabelReadProperties();
	private HashMap<String, String> portalSpecificData = labelProperty.getPortalSpecificData();
	private HashMap<String, String> commonLabels = labelProperty.getCommonLabels();

	private ExtentReportTemplate extentReportTemplate = null;
	private ExtentReports report = null;

	private CC_Com_LoginPage login = new CC_Com_LoginPage();
	private CC_Com_HomePage ccHomePage = new CC_Com_HomePage();
	private CC_Com_Admin_WebSiteDetails adminWebSiteDetails = new CC_Com_Admin_WebSiteDetails();
	private DMCPackage_RateConstructor DMCRateConstrustor = new DMCPackage_RateConstructor();

	private CC_Com_BO_Menu bomenu = new CC_Com_BO_Menu();
	private CC_Com_Currency_Page currency = new CC_Com_Currency_Page();
	private Web_Com_HomePage home = new Web_Com_HomePage();
	private Web_FxP_ResultsPage resultsPage = null;

	private String PackageTotal = null;

	private Web_DMC_ComponentListingPage componentListingPage = new Web_DMC_ComponentListingPage();
	private Web_Com_PaymentsPage paymentpage = new Web_Com_PaymentsPage();

	private FxP_ResultsPage_Validations resultValidation = new FxP_ResultsPage_Validations();

	private Web_Com_CustomerDetailsPage customerDetailsPage = new Web_Com_CustomerDetailsPage();

	private Web_Com_OccupancyDetailsPage occupancyDetails = new Web_Com_OccupancyDetailsPage();

	private Web_Com_BillingInformationPage billinginfo = new Web_Com_BillingInformationPage();
	private Web_Com_Verify_selected_products verifyproducts = new Web_Com_Verify_selected_products();

	private Web_Com_TermsPage termspage = new Web_Com_TermsPage();
	private Web_Com_PaymentGateway paymentgateway = new Web_Com_PaymentGateway();
	private WEB_DMC_ConfirmationPage DMCconfirmationpage = new WEB_DMC_ConfirmationPage();

	ReservationPassengerInfo passengerNames = new ReservationPassengerInfo();
	FlightReservation flightReservation = new FlightReservation();
	Web_Flight_PassengerDetails passengerDetailsPage = new Web_Flight_PassengerDetails();
	private FareRequest fareRequest = new FareRequest();
	private FareResponse fareResponse = new FareResponse();
	private PriceRequest priceRequest = new PriceRequest();
	private PriceResponse priceResponse = new PriceResponse();

	private String DMCsearchExcel = portalSpecificData.get("DMCsearchExcel");

	// Used hotel Occupancy details
	private String occupancyExcel = portalSpecificData.get("occupancyExcel");
	private HashMap<String, String> selectedFlightDetails = new HashMap<>();
	private HashMap<String, String> SelectedPackageDetails = new HashMap<>();
	private List<dmc_Setup_DataObjects_DMCPackageObject> packageList;

	@Before
	public void setUp() throws Exception {

		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalSpecificData.get("PortalName");
		report = new ExtentReports(new File("Reports").getAbsolutePath() + "/" + portal + "/DMC-Reservation-" + today + ".html", NetworkMode.OFFLINE);
		extentReportTemplate = ExtentReportTemplate.getInstance();
		extentReportTemplate.Initialize(report);

		System.setProperty("http.proxyHost", "192.168.1.132");
		System.setProperty("http.proxyPort", "3128");

	}

	@Test
	public void test() throws Exception {

		// Initalizing Extent Report //
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		Map<String, String> info = new HashMap<String, String>();

		info.put("Browsertype", caps.getBrowserName());
		info.put("Browserversion", caps.getVersion());
		info.put("Platform", caps.getPlatform().toString());
		info.put("Serach Excel path", portalSpecificData.get("DMCsearchExcel"));
		info.put("Portal Url", portalSpecificData.get("PortalUrlCC"));
		info.put("Portal Username", portalSpecificData.get("DMCUsername"));
		info.put("Platform", caps.getPlatform().toString());
		extentReportTemplate.setSystemInfo(info);

		// END Initalizing Extent Report //

		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));

		CC_Com_Admin_Home adminHome = (CC_Com_Admin_Home) login.loginAs(UserType.ADMIN, portalSpecificData.get("adminUsername"), portalSpecificData.get("adminPassword"));
		CC_Com_Admin_ConfigurationPage configPage = adminHome.getConfigurationScreen(portalSpecificData.get("PortalUrlCC"));
		HashMap<String, String> configDetails = configPage.getConfigProperties();
		adminWebSiteDetails.getWebSiteDetailsPage(portalSpecificData.get("PortalUrlCC"));
		ArrayList<HashMap<String, String>> websiteDetails = adminWebSiteDetails.getDetails();
		portalSpecificData = labelProperty.changePortalSpecificDetails(configDetails, portalSpecificData);

		// currency details
		HashMap<String, String> currencyMap = new HashMap<>();

		// verify
		DMC_Verify verify = new DMC_Verify();

		// ExcelReader newReader = new ExcelReader();
		PoiExcelReader newReader = new PoiExcelReader();
		FileInputStream stream = new FileInputStream(new File(DMCsearchExcel));

		ArrayList<Map<Integer, String>> contentList = newReader.reader(stream);
		Map<Integer, String> content = contentList.get(0);

		Iterator<Map.Entry<Integer, String>> mapIterator = content.entrySet().iterator();

		while (mapIterator.hasNext()) {

			try {

				HashMap<String, String> xmlurls = new HashMap<>();
				Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>) mapIterator.next();
				String Content = Entry.getValue();
				String[] searchDetails = Content.split(",");

				DMCSearchObject searchObject = new DMCSearchObject();
				DMCSearchObject search = searchObject.setData(searchDetails);

				if (search.getExecuteStatus().equalsIgnoreCase("Yes")) {

					DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
					try {
						extentReportTemplate.verifyTrue(true, login.isLoginPageAvailable(), "DMC Login page Availability");

					} catch (Exception e) {
						System.out.println(e);
					}

					ArrayList<HotelOccupancyObject> occupancyList = new ArrayList<HotelOccupancyObject>();
					newReader = new PoiExcelReader();
					stream = new FileInputStream(new File(occupancyExcel));
					contentList = new ArrayList<Map<Integer, String>>();
					contentList = newReader.reader(stream);
					content = new HashMap<Integer, String>();
					content = contentList.get(0);
					Iterator<Map.Entry<Integer, String>> occupancyIterator = content.entrySet().iterator();

					while (occupancyIterator.hasNext()) {
						Entry = (Map.Entry<Integer, String>) occupancyIterator.next();
						Content = Entry.getValue();
						String[] occupancyDetails = Content.split(",");
						HotelOccupancyObject occ = new HotelOccupancyObject();
						occupancyList.add(occ.setData(occupancyDetails));
					}

					if (login.isLoginPageAvailable()) {
						login.typeUserName(portalSpecificData.get("DMCUsername"));
						login.typePassword(portalSpecificData.get("DMCPassword"));
						login.loginSubmit();
						// CC_Com_Admin_Home internalHome = (CC_Com_Admin_Home) login.loginAs(UserType.INTERNAL, portalSpecificData.get("hotelUsername"), portalSpecificData.get("hotelPassword"));

						// PrintTemplate.setTableHeading("Check BO menu page availability");
						extentReportTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "DMC Packages Bo menu page Availability");
						if (bomenu.isModuleImageAvailable()) {
							// get currency details
							DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("currencyUrl")));
							currencyMap = currency.getCCDetails();
							portalSpecificData = labelProperty.addCurrencyList(currencyMap, portalSpecificData);
							String defaultCC = currency.getDefaultCurrecny();

							System.out.println(search.getBookingChannel());

							if (search.getBookingChannel().equalsIgnoreCase("web")) {

								DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlWeb"));

								if (home.isPageAvailable()) {

									verify.webHomePage(extentReportTemplate, home, labelProperty, search);
									home.SearchForDMC(search);

									resultsPage = home.performDMCSearch();

									if (resultsPage.isPackageResultsAvailable()) {

										// extentReportTemplate.verifyTrue(true, resultsPage.isPackageresultsPageAvailable(), "Fix package Results availability verification");

										// resultValidation.generalVerifications_web_ResultsPage(extentReportTemplate, resultsPage);
										// resultValidation.searchAgainBECVerifications_web_ResultPage(extentReportTemplate, resultsPage,labelProperty);

										resultsPage.traverseResults(search);
										resultsPage.DMCfindResultBlock(search);

										resultsPage.PackageSelect(search);

										if (componentListingPage.isDMCComponentListingpageavailable()) {

											selectedFlightDetails = componentListingPage.getselectedPackageflightdetails();

											SelectedPackageDetails = componentListingPage.getPackageCommonProperties();

											// READ FAREREQUEST XML
											try {
												// PREPARE REQ/RES XML LOG PREFIX
												XMLFileType ReqType = null;
												XMLFileType ResType = null;
												if (portalSpecificData.get("vacationPreferredAirline").equals("NONE")) {
													ReqType = XMLFileType.AIR2_LowFareSearchRequest_;
													ResType = XMLFileType.AIR2_LowFareSearchResponse_;
												} else {
													ReqType = XMLFileType.AIR2_PrefAirlineSearchRequest_;
													ResType = XMLFileType.AIR2_PrefAirlineSearchResponse_;
												}

												// READ FAREREQUEST XML
												System.out.println("***** Read flight availability request *****");
												FareRequestReader fareRequestReader = new FareRequestReader();
												fareRequest.setTracer(selectedFlightDetails.get("flightTracer"));
												fareRequest.setXMLFileType(ReqType.toString());
												Document fareReqDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, ReqType, selectedFlightDetails.get("flightTracer"), portalSpecificData.get("air2XmlLogPath"), portalSpecificData.get("flightSupplier"),
														fareRequest);
												fareRequestReader.RequestReader(fareReqDoc, fareRequest);
												flightReservation.setFareRequest(fareRequest);

												// READ FARERESPONSE XML
												System.out.println("***** Read flight availability response *****");
												FareResponseReader fareResponseReader = new FareResponseReader();
												fareResponse.setTracer(selectedFlightDetails.get("flightTracer"));
												fareResponse.setXMLFileType(ResType.toString());
												Document fareResDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, ResType, selectedFlightDetails.get("flightTracer"), portalSpecificData.get("air2XmlLogPath"), portalSpecificData.get("flightSupplier"),
														fareResponse);
												fareResponseReader.ResponseReader(fareResDoc, fareResponse);
												flightReservation.setFareResponse(fareResponse);

											} catch (Exception e) {
												// TODO: handle exception
											}

											try {

												// READ PRICEREQUEST XML
												System.out.println("***** Read flight price request *****");
												PriceRequestReader priceReqReader = new PriceRequestReader();
												priceRequest.setTracer(selectedFlightDetails.get("flightTracer"));
												priceRequest.setXMLFileType(XMLFileType.AIR2_PriceRequest_.toString());
												Document priceRequestDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR2_PriceRequest_, selectedFlightDetails.get("flightTracer"), portalSpecificData.get("air2XmlLogPath"),
														portalSpecificData.get("flightSupplier"), priceRequest);
												priceReqReader.RequestReader(priceRequestDoc, priceRequest);
												flightReservation.setPriceRequest(priceRequest);

												// READ PRICERESPONSE XML
												System.out.println("***** Read flight price response *****");
												PriceResponseReader priceResReader = new PriceResponseReader();
												priceResponse.setTracer(selectedFlightDetails.get("flightTracer"));
												priceResponse.setXMLFileType(XMLFileType.AIR2_PriceResponse_.toString());
												Document priceResponseDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR2_PriceResponse_, selectedFlightDetails.get("flightTracer"), portalSpecificData.get("air2XmlLogPath"),
														portalSpecificData.get("flightSupplier"), priceResponse);
												priceResReader.AmadeusPriceResponseReader(priceResponseDoc, priceResponse);
												flightReservation.setPriceResponse(priceResponse);

											} catch (Exception e) {
												System.out.println(e + " Flight Price request or response Read failure");
											}

											// Read DMC Land end xml data

											try {

												// Load DMC Package Details

												dmc_inputdataLoader_XmlReader reader = new dmc_inputdataLoader_XmlReader();

												packageList = reader.readxml();

											}

											catch (Exception e) {
												System.out.println(e + "  DMC LAND Component xml read failure");
											}

											// Calculate DMC Package Total

											try {

												String PackageTotal = DMCRateConstrustor.DMCPakgeRateCalculator(defaultCC, currencyMap, search, flightReservation.getPriceResponse(),packageList);
												System.out.println(PackageTotal);

											} catch (Exception e) {
												// TODO: handle exception
											}

											System.out.println("********Results page Verification Starting**********");

											verify.resultsPageVerification(extentReportTemplate, defaultCC, currencyMap, portalSpecificData, selectedFlightDetails, flightReservation.getFareResponse(), search, flightReservation.getPriceResponse());

											componentListingPage.bookThepackage();

											try {

												ExtentReportTemplate.getInstance().verifyTrue(true, paymentpage.isPaymentsPageAvailable(), "DMC Package Payments page availability");

											} catch (Exception e) {

												ExtentReportTemplate.getInstance().verifyTrue(false, paymentpage.isPaymentsPageAvailable(), "DMC Package Payments page availability");

											}

											customerDetailsPage.enterCustomerDetails(portalSpecificData, "fp");

											customerDetailsPage.proceedToOccupancy();

											if (search.getFlightStatus().equalsIgnoreCase("Including Flight")) {

												passengerNames = Initiator.getDMCPassengerDetails(search);
												flightReservation.setPassengerNames(passengerNames);
												passengerDetailsPage.setDMCPassengerDetails(passengerNames);
												passengerDetailsPage.proceedToBillingInfo();
												billinginfo.isBillingInformationsAvailable();
												billinginfo.DMCfillSpecialNotes();
												termspage.isTermsAndConditionsAvailable();
												termspage.getTCMandatoryInputFields();
												termspage.checkAvailability();
												verifyproducts.isVerifyProductsAvailable();
												verifyproducts.getDetails(labelProperty);
												verifyproducts.continueToPG();

												paymentgateway.isPaymentGatewayLoaded(configDetails);
												paymentgateway.enterCardDetails(portalSpecificData);
												paymentgateway.confirm();

												try {

													Thread.sleep(20000);
													Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
													Thread.sleep(2000);
													alert.accept();

												} catch (Exception e) {
													System.out.println("Confirmation alert failure  " + e);
												}

												ExtentReportTemplate.getInstance().verifyTrue(true, DMCconfirmationpage.isconfirmationpageAvailable(), "Air inclusive DMC Package Reservation Test");

											}
										}

									}

								}

							}

						}

					}

				}

			} catch (Exception e) {

				System.out.println(e.getStackTrace());

			}

		}

	}

	@After
	public void tearDown() throws Exception {
	}

}
