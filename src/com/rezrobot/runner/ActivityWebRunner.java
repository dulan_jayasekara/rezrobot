package com.rezrobot.runner;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.ActivityReservation;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.Activity_LoadDetails;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.web.activity.Web_Act_ConfirmationPage;
import com.rezrobot.pages.web.activity.Web_Act_QuotationConfirmationPage;
import com.rezrobot.pages.web.activity.Web_Act_ResultsPage;
import com.rezrobot.pages.web.common.*;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Link;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExcelReader;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
import com.rezrobot.verifications.Activity_WEB_Validations;


public class ActivityWebRunner {

	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";
	private ArrayList<Map<Integer, String>> web_sheetlist;
	private Activity_LoadDetails web_loadDetails;
	private HashMap<String, String> labelPropertyMap;
	private LabelReadProperties labelProperty;
	private TreeMap<String, Activity_WEB_Search>   web_SearchList;
	private Map<Integer, String> web_activitySearchInfoMap 		= null;
	private Map<Integer, String> web_activityPaymentsMap 		= null;
	private Map<Integer, String> web_activityInventoryMap 		= null;
	private HashMap<String, String> currencyMap;
	private HtmlReportTemplate HtmlReportTemplate = null;
	private ExtentReportTemplate ExtentReportTemplate = null;
	private ExtentReports report = null;

	@Before
	public void setUp() throws Exception {

		labelProperty = new LabelReadProperties();
		String portal = labelProperty.getPortalSpecificData().get("PortalName");
		report = new ExtentReports("Reports/"+ portal+ "/ActivityReservationReport.html", NetworkMode.OFFLINE);
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter*/);
		DriverFactory.getInstance().initializeDriver();
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
				
		labelPropertyMap = labelProperty.getActivityLabelProperties();
		ExcelReader reader = new ExcelReader();
		web_sheetlist = new ArrayList<Map<Integer, String>>();
		web_sheetlist 	= reader.init(labelProperty.getPortalSpecificData().get("WEB.ExcelPath.ActivityReservation"));
		web_loadDetails = new Activity_LoadDetails(web_sheetlist);
		currencyMap = new HashMap<>();
		
		web_activitySearchInfoMap 	= web_sheetlist.get(0);
		web_activityPaymentsMap		= web_sheetlist.get(1);
		web_activityInventoryMap   	= web_sheetlist.get(2);
		
		try {
			web_SearchList = web_loadDetails.loadActivityReservation(web_activitySearchInfoMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			web_SearchList = web_loadDetails.loadPaymentDetails(web_activityPaymentsMap, web_SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			web_SearchList = web_loadDetails.loadActivityInventoryRecords(web_activityInventoryMap, web_SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() throws Exception {
				
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		
		Iterator<Map.Entry<String, Activity_WEB_Search>> it = (Iterator<Entry<String, Activity_WEB_Search>>) web_SearchList.entrySet().iterator();
		
		while (it.hasNext()) {
			
			Activity_WEB_Search searchObject 				= it.next().getValue();
			ActivityDetails activityDetails 				= new ActivityDetails();
			Web_Com_HomePage acthomePage 					= new Web_Com_HomePage();
			Web_Act_ResultsPage actResultsPage 				= new Web_Act_ResultsPage();
			Web_Com_PaymentsPage actPaymentsPage 			= new Web_Com_PaymentsPage();
			Web_Com_CustomerDetailsPage customerPage 		= new Web_Com_CustomerDetailsPage();
			Web_Com_OccupancyDetailsPage occupancPage 		= new Web_Com_OccupancyDetailsPage();
			Web_Com_BillingInformationPage billingPage 		= new Web_Com_BillingInformationPage();
			Web_Com_NotesPage notesPage 					= new Web_Com_NotesPage();
			Web_Com_TermsPage termsPage 					= new Web_Com_TermsPage();
			Web_Com_PaymentGateway payGateway 				= new Web_Com_PaymentGateway();
			Web_Com_Verify_selected_products verifyProducts	= new Web_Com_Verify_selected_products();
			Web_Act_ConfirmationPage confirmationPage 		= new Web_Act_ConfirmationPage();	
			Web_Act_QuotationConfirmationPage quotationConfirmationPage = new Web_Act_QuotationConfirmationPage();
			CC_Com_LoginPage loginPage						= new CC_Com_LoginPage();
			CC_Com_BO_Menu operationsPage					= new CC_Com_BO_Menu();
			CC_Com_Currency_Page currencyPage				= new CC_Com_Currency_Page();
			Reservation reservation = new Reservation();
			ActivityReservation activityReservation         = new ActivityReservation();
					
			Map<String, String> sysInfo = new HashMap<String, String>();
			sysInfo.put("BrowserType", caps.getBrowserName());
			sysInfo.put("BrowserVersion", caps.getVersion());
			sysInfo.put("Platform", caps.getPlatform().toString());
			sysInfo.put("PortalURL_CC", labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("login")));
			sysInfo.put("PortalURL_WEB", labelProperty.getPortalSpecificData().get("PortalUrlWeb"));
			ReportTemplate.getInstance().setSystemInfo(sysInfo);
			
			DriverFactory.getInstance().getDriver().get(labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("login")));
		
			boolean login = loginPage.isLoginPageAvailable();
			PrintTemplate.verifyTrue(true, login, "Login Page Availability");
			boolean operations = false;
			if(login == true){
				
				loginPage.typeUserName(labelProperty.getPortalSpecificData().get("ActivityUserName"));
				loginPage.typePassword(labelProperty.getPortalSpecificData().get("ActivityPassword"));
				loginPage.loginSubmit();
				
				operations = operationsPage.isModuleImageAvailable();
				PrintTemplate.verifyTrue(true, operations, "Login Details Checking");
				if(operations == true){
					
					DriverFactory.getInstance().getDriver().get(labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("currencyUrl")));
					currencyMap = currencyPage.getCCDetails();
					
					DriverFactory.getInstance().getDriver().get(labelProperty.getPortalSpecificData().get("PortalUrlWeb"));				
				}
			}
						
			Activity_WEB_Validations activityValidations	= new Activity_WEB_Validations(PrintTemplate, searchObject, labelPropertyMap, labelProperty, currencyMap);
			activityDetails = activityValidations.getRatesCalculations(activityDetails);
			activityDetails = activityValidations.getCancellationPolicies(activityDetails);
						
			if (login == true && operations == true) {
				
				PrintTemplate.verifyTrue(true, acthomePage.isPageAvailable(),"WEB Home Page Availability");
				PrintTemplate.verifyTrue("Rezgateway", acthomePage.getPageTitel().trim(), "Home Page Title Validation");

				if (acthomePage.isPageAvailable()) {
					
					activityDetails.setSystemLoaded(true);
					activityValidations.getActivityBookingEngineValidations(acthomePage);					
					acthomePage.doActivitySearch(searchObject);
					acthomePage.proceedToActivityResultsPage();
					Thread.sleep(5000);
					
					//results page validations	
					
					PrintTemplate.verifyTrue(true, actResultsPage.isActivityResultsPageAvailable(), "Activity Results Page Availability");
					
					if (actResultsPage.isActivityResultsPageAvailable()) {
						
						activityDetails.setResultsPageloaded(true);
						PrintTemplate.verifyTrue(true, actResultsPage.isActivityResultsAvailable(), "Activity Results Availability");
										
						if (actResultsPage.isActivityResultsAvailable()) {
							
							activityDetails.setActResultsAvailable(true);
							activityValidations.getResultsPageValidations(actResultsPage);					
							activityDetails = actResultsPage.selectActivities(searchObject, activityDetails);
						
							//Activity Payments page						
							
							PrintTemplate.verifyTrue(true, actPaymentsPage.isPaymentsPageAvailable(), "Activity Payments Page Availability");
														
							if (actPaymentsPage.isPaymentsPageAvailable()) {
								
								activityDetails.setPaymentsPageLoaded(true);
								activityDetails = actPaymentsPage.getActivity_PaymentsPageValidations(activityDetails);
								activityValidations.getPaymentsPageValidations(actPaymentsPage);	
								actPaymentsPage.fillCustomerDetails();
								
								PrintTemplate.verifyTrue(true, customerPage.isCustomerDetailsAvailable(), "Customer Details Availability");
														
								if (customerPage.isCustomerDetailsAvailable()) {
									
									activityValidations.getCustomerDetailsValidations(customerPage);
									customerPage.enterActivityCustomerDetails(searchObject);
									
									//Quotation
									if (searchObject.getQuotationReq().equalsIgnoreCase("Yes")) {
										
										customerPage.saveActivityQuotation();
										
										boolean quotationConfimPageLoaded = quotationConfirmationPage.isQuoteConfirmationPageAvailable();
										PrintTemplate.verifyTrue(true, quotationConfimPageLoaded, "Quotation Confirmation Page Availability");
										
										if (quotationConfimPageLoaded) {
											
											activityValidations.getQuotationConfirmationPageValidations(quotationConfirmationPage);
											activityDetails = quotationConfirmationPage.getConfirmationInfoDetails(activityDetails);
										}else {
											System.out.println("Quotation confirmation page loaded >> "+quotationConfimPageLoaded+"");
										}
										
									}else{
										customerPage.proceedToOccupancy();
									}
									
								}else{
									System.out.println("Payments page >> Customer details enter not loaded");
								}
															
								if (searchObject.getQuotationReq().equalsIgnoreCase("No")) {
																	
									PrintTemplate.verifyTrue(true, occupancPage.isActivity_OccupancyDetailsAvailable(), "Occupancy Details Availability");
															
									if (occupancPage.isActivity_OccupancyDetailsAvailable()) {
									
										activityValidations.getOccupancyDetailsValidations(occupancPage);					
										activityDetails = occupancPage.setActivityPickUpDropOffDetails(activityDetails, searchObject);
										occupancPage.loadBillingInformations();
							
									}else{
										System.out.println("Payments page >> Activity occupancy not loaded");
									}
													
									PrintTemplate.verifyTrue(true, billingPage.isBillingInformationsAvailable(), "Billing Informations Availability");
															
									if (billingPage.isBillingInformationsAvailable()) {
								
										activityValidations.getBillingInfoDetailsValidations(billingPage);							
										activityDetails = billingPage.getActivityBillingInfoDetails(activityDetails);
										billingPage.fillSpecialNotes();
									
									}else{
										System.out.println("Payments page >> Billing Info not loaded");
									}
														
									PrintTemplate.verifyTrue(true, notesPage.isSpecialNotesAvailable(), "Special Notes Availability");
															
									if (notesPage.isSpecialNotesAvailable()) {
										
										activityValidations.getNotesPageValidations(notesPage);							
										activityDetails = notesPage.enterActivityNotes(activityDetails);
										notesPage.loadTermsandConditions();
									
									}else{
										System.out.println("Payments page >> Notes section not loaded");
									}
																									
									PrintTemplate.verifyTrue(true, termsPage.isTermsAndConditionsAvailable(), "Terms & Conditions Availability");
															
									if (termsPage.isTermsAndConditionsAvailable()) {
									
										activityValidations.getTermsAndConditionsValidations(termsPage);							
										activityDetails = termsPage.getActivityTermsAndConditions(activityDetails);
										termsPage.checkAvailability();
									
									}else{
										System.out.println("Payments page >> Terms and Conditions not loaded");
									}
									
									//product verifications									
									PrintTemplate.verifyTrue(true, verifyProducts.isVerifyProductsAvailable(), "Check Verify selected products Availability");
									
									try {
										verifyProducts.continueToPG();	
										Thread.sleep(5000);
										
// temp error fix. config details should be taken using rezgadmin										
										HashMap<String, String> configDetails = new HashMap<>();
										PrintTemplate.verifyTrue(true, payGateway.isPaymentGatewayLoaded(configDetails), "Payment Gateway Availability");
										
										if (payGateway.isPaymentGatewayLoaded(configDetails)) {
											
											payGateway.enterCardDetails(labelProperty.getPortalSpecificData());
											payGateway.loadActivityConfirmationPage();
																						
											//Confirmation page validations
											
											boolean confirmationPageLoaded = confirmationPage.isConfirmationPageAvailable();
											PrintTemplate.verifyTrue(true, confirmationPageLoaded, "Activity Confirmation Page Availability");
											
											if (confirmationPageLoaded) {
												
												activityValidations.getConfirmationPageValidations(confirmationPage);
												activityDetails = confirmationPage.getConfirmationInfoDetails(activityDetails);
												
											}else{
												System.out.println("Confirmation page not loaded");
											}
											
										}else{
											System.out.println("Payment Gateway not loaded");
										}
										
									} catch (Exception e) {
										e.printStackTrace();
										System.out.println("Intermediate screen not loaded");
									}
															
								}
															
							}else{
								System.out.println("Payments Page not loaded");
							}
							
						}else{
							System.out.println("Activity results are not available");
						}
									
					}else{
						System.out.println("Activity Results Page not loaded");
					}
					
				} 
				
				//basic report verifications
				activityValidations.getBasicFlowValidations(activityDetails);
				
				//object saving
				activityReservation.setReservationNo(activityDetails.getReservationNo());				
				if(activityDetails.getConfirmation_BI_BookingStatus().equalsIgnoreCase("Confirmed")){
					activityReservation.setBookingConfirmed(true);
				}				
				activityReservation.setSearchObject(searchObject);
				activityReservation.setActivityDetails(activityDetails);
			
				reservation.setProduct("Activity");
				reservation.setActivityReservation(activityReservation);
				
				//Serialize Reservation and file saving
				Serializer.SerializeReservation(reservation, labelProperty.getPortalSpecificData().get("PortalName"), labelProperty.getPortalSpecificData().get("ActivitySerializePath"));
	            			
			}
					
		}
		
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Driver quit");
		DriverFactory.getInstance().getDriver().quit();
		PrintWriter.append("</body></html>");
		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/Activity_" + this.getClass().getSimpleName() + "_"
						+ Browser + ".html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();
		PrintWriter.setLength(0);
	}

}