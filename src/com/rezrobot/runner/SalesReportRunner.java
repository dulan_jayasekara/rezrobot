package com.rezrobot.runner;
import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_HomePage;
import com.rezrobot.pages.CC.common.CC_Com_OAH_SalesReport_Criteria;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.types.BookingChannelType;
import com.rezrobot.types.ReportBookingType;
import com.rezrobot.types.ReportCurrencyType;
import com.rezrobot.types.SummarizedBy;
import com.rezrobot.types.TourSalesReportType;
import com.rezrobot.types.UserType;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.ReportTemplate;


public class SalesReportRunner {
	


	
	//extent report
	private 												ReportTemplate					PrintTemplate;
 //   private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	private CC_Com_LoginPage								login							= null;
	private CC_Com_HomePage 								HomePage						= null;
    private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalDetails 					= labelProperty.getPortalSpecificData();
	private CC_Com_OAH_SalesReport_Criteria				    SalesReportCrieteria		    = null;

	
	@BeforeClass
	public void setUp() throws Exception {
		
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalDetails.get("PortalName");
		report = new ExtentReports("Reports/"+ portal+ "/Hotel-ReportOnly-"+ today +".html", NetworkMode.OFFLINE);
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		ExtentReportTemplate.getInstance().Initialize(report);
		login =  new CC_Com_LoginPage();
	}
	

	
	@Test(priority = 1)
	public void systemTest() throws Exception {
		DriverFactory.getInstance().getDriver().get(portalDetails.get("PortalUrlCC").concat(portalDetails.get("login")));
        PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
	}
	
	@Test(priority = 2,dependsOnMethods={"systemTest"})
	public void loginTest() throws Exception {
		HomePage = login.loginAs(UserType.INTERNAL,portalDetails.get("hotelUsername") ,portalDetails.get("hotelPassword"));
		Assert.assertEquals(true, HomePage.isPageAvailable(), "Home Page not available");
	}
	
	@Test(priority = 3,dependsOnMethods={"loginTest"})
	public void reportAvailabilityTest() throws Exception {
		SalesReportCrieteria = HomePage.getTourSalesReport();
	}
	
	@Test(priority = 4)
	public void reportParameterAvailability() throws Exception {
		SalesReportCrieteria.switchToReportFrame();
		Assert.assertEquals(true, SalesReportCrieteria.getParametersAvailability(), "Tour Sales Report search Paramters  not available");
	}
	
	@Test(priority = 5)
	public void checkDefaultSelectedReportType() throws Exception {
		Assert.assertEquals(TourSalesReportType.SALES, SalesReportCrieteria.getDefaultSelectedReport(), "Tour Sales Report - Sales Report not selected by default");
	}
	
	@Test(priority = 6)
	public void checkDefaultSelectedCurrency() throws Exception {
		Assert.assertEquals(ReportCurrencyType.SELLINGCURR, SalesReportCrieteria.getDefaultSelectedCurrencyType(), "Tour Sales Report - Selling Currency not selected by default");
	}
	
	@Test(priority = 7)
	public void checkDefaultSelectedFromDate() throws Exception {
		Assert.assertEquals(Calender.getToday("dd-MMM-yyy"), SalesReportCrieteria.getDefaultSelectedDates()[0], "Tour Sales Report - Default from date");
	}
	
	
	@Test(priority = 8)
	public void checkDefaultSelectedToDate() throws Exception {
		Assert.assertEquals(Calender.getToday("dd-MMM-yyy"), SalesReportCrieteria.getDefaultSelectedDates()[1], "Tour Sales Report - Default to date");
	}
	
	@Test(priority = 9)
	public void checkDefaultSelectedCountry() throws Exception {
		Assert.assertEquals("All", SalesReportCrieteria.getDefaultCountry(), "Tour Sales Report - Country \"ALL\" not selected by default");
	}
	
	@Test(priority = 10)
	public void checkDefaultSelectedBookingChannel() throws Exception {
		Assert.assertEquals(BookingChannelType.ALL, SalesReportCrieteria.getDefaultSelectedBookingChannel(), "Tour Sales Report - ALL Booking Channel not selected by default");
	}
	
	@Test(priority = 11)
	public void checkDefaultSelectedBookingsType() throws Exception {
		Assert.assertEquals(ReportBookingType.ALL, SalesReportCrieteria.getDefaultSelectedBookingType(), "Tour Sales Report - ALL Booking Type not selected by default");
	}
	
	@Test(priority = 12)
	public void checkDefaultSelectedSummerizeBy() throws Exception {
		Assert.assertEquals(SummarizedBy.NONE, SalesReportCrieteria.getDefaultSelectedSummerizeOption(), "Tour Sales Report - ALL Summerize option not selected by default");
	}
	
	@Test(priority = 13)
	public void checkExcelButtonAvailability() throws Exception {
		Assert.assertEquals(true, SalesReportCrieteria.getExcelButtonAvailability(), "Tour Sales Report - Excel Button not available");
	}
	@AfterClass
	public void tearDown() throws Exception {
		DriverFactory.getInstance().getDriver().quit();
	}
}
