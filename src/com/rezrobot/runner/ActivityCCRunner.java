/**
 * 
 */
package com.rezrobot.runner;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.rezrobot.pages.web.flights.Web_Flight_HomePage;
import com.rezrobot.pojo.Link;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.ReportTemplate;

/**
 * @author Dulan
 * 
 */
public class ActivityCCRunner {

	private HashMap<String, String> PropertySet;
	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";
	private StringBuffer					 PrintWriter			= null;
	private HtmlReportTemplate				 HtmlReportTemplate		= null;
	private ExtentReportTemplate 			 ExtentReportTemplate	= null;

	@Before
	public void setUp() throws Exception {

		// logger = Logger.getLogger(this.getClass());
		// DOMConfigurator.configure("log4j.xml");
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
	//	PrintTemplate.Initialize(PrintWriter);
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver()
				.get("http://v3proddev2.rezg.net/rezproductionReservations/");
	}

	@Test
	public void test() throws Exception {

	//	PrintTemplate.markReportBegining("Rezgateway WEB Flight Flow Smoke Test");
	//	PrintTemplate.setInfoTableHeading("Test Basic Details");
		
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		
		Map<String, String> sysInfo = new HashMap<String, String>();
		sysInfo.put("Browser Type", caps.getBrowserName());
		sysInfo.put("Browser Version", caps.getBrowserName());
		sysInfo.put("Platform", caps.getBrowserName());
		ReportTemplate.getInstance().setSystemInfo(sysInfo);

		Web_Flight_HomePage home = new Web_Flight_HomePage();
		//PrintTemplate.setTableHeading("General Home Page Validations");		
		PrintTemplate.verifyTrue(true, home.isPageAvailable(),"Home Page Availability");
		PrintTemplate.verifyTrue("Rezgateway", home.getPageTitel().trim(), "Home Page Titel Validation");
		//PrintTemplate.markTableEnd();

		//PrintTemplate.setTableHeading("Booking Engine Validations");
		PrintTemplate.verifyTrue(true, home.getBECAvailability(), "Booking Engine Availability");

		if (home.isPageAvailable()) {

			HashMap<String, Boolean> AvailMap = home.getAvailabilityOfInputFields();

			PrintTemplate.verifyTrue(true, AvailMap.get("cor"), "Availability Of COR field");
			// All the Element availability validations should go here
			//PrintTemplate.markTableEnd();

			// HashMap<String, String> labelMap = home.getLabels();
			//PrintTemplate.setTableHeading("Label Text  Validations");
			// PrintTemplate.verifyTrue("Where are you going ?",labelMap.get("Where")
			// , "Where are you going label validation");
			// Lable Validation should go here
			//PrintTemplate.markTableEnd();

			//PrintTemplate.setTableHeading("Combo Box Attributes Validation");
			// Combo box validations should go here
			//PrintTemplate.markTableEnd();

			HashMap<String, Boolean> MandatoryMap = home.getMandatoryList();
			//PrintTemplate.setTableHeading("Mandatory Status validation");
			// Mandatory filed validations should go here
			PrintTemplate.verifyTrue(true, MandatoryMap.get("cor"), "Mandatory validation Of to filed");

			//PrintTemplate.markTableEnd();

		} else {
			//PrintTemplate.markTableEnd();
		}

	}

	@After
	public void tearDown() throws Exception {
		DriverFactory.getInstance().getDriver().quit();
		PrintWriter.append("</body></html>");
		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/Flight_" + this.getClass().getSimpleName() + "_"
						+ Browser + ".html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();
		PrintWriter.setLength(0);
	}

}
