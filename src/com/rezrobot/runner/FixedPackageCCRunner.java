package com.rezrobot.runner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


import com.rezrobot.pages.CC.common.CC_Com_HomePage;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_SearchCriteria;
import com.rezrobot.pages.CC.fixed_packages.CC_FxP_ComponentListingPage;
import com.rezrobot.pages.CC.fixed_packages.CC_FxP_ResultsPage;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.verifications.fixed_packages.FxP_Search_validation;

public class FixedPackageCCRunner {
	
	
	private StringBuffer PrintWriter;
	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";
	
	@Before
	public void setUp() throws Exception {
		
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get("http://dev3.rezg.net/omanairholidays/admin/common/LoginPage.do");
	
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter*/);
		
		
		
	}
	@Test
	public void test() throws Exception{
		
		//PrintTemplate.markReportBegining("Rezgateway CC Fixed Package Flow Smoke Test");
		//PrintTemplate.setInfoTableHeading("Test Basic Details");
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();

		//PrintTemplate.addToInfoTabel("Browser Type", caps.getBrowserName());
		//PrintTemplate.addToInfoTabel("Browser Version", caps.getVersion());
		//PrintTemplate.addToInfoTabel("Platform", caps.getPlatform().toString());
		//PrintTemplate.markTableEnd();
		CC_Com_LoginPage login=new CC_Com_LoginPage();
		login.getPortalHeader();
		CC_Com_HomePage home=login.loginWithValidCredentials("KavithaP", "123456");
		CC_Com_SearchCriteria search=home.navigateDropdown();
	
		
		FxP_Search_validation searchVal=new FxP_Search_validation();
		PrintTemplate=searchVal.bookingengineAvailability(search, PrintTemplate);
		PrintTemplate=searchVal.labelTextValidation(search, PrintTemplate);
		PrintTemplate=searchVal.BECElementsValidation_web_Homepage(search, PrintTemplate);
		PrintTemplate=searchVal.MandatoryfieldValidation(search, PrintTemplate);
		search.bookingEngineFillFixedPackages();
		CC_FxP_ResultsPage resultpage=search.proceedToNext_FP();
		resultpage.pacakgenavigate("OAH Reg10");
		CC_FxP_ComponentListingPage CLPage=resultpage.readPackage();
		CLPage.RateReaders();
	//	CLPage.ActivityReader();
		CLPage.ProceedTonext();
		
		
		
	}
	@After
	public void tearDown() throws IOException{
		
		PrintWriter.append("</body></html>");
		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/FixedPackage_" + this.getClass().getSimpleName() + "_"
				+ Browser + ".html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();
		PrintWriter.setLength(0);
		
	}

}

