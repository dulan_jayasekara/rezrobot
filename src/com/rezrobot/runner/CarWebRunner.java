package com.rezrobot.runner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.dataobjects.CarSearchObject;
import com.rezrobot.pages.web.car.WEB_Car_ConfirmationPage;
import com.rezrobot.pages.web.car.WEB_Car_Payment_Occupancy;
import com.rezrobot.pages.web.car.WEB_Car_Results_Page;
import com.rezrobot.pages.web.common.Web_Com_BillingInformationPage;
import com.rezrobot.pages.web.common.Web_Com_CustomerDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.pages.web.common.Web_Com_NotesPage;
import com.rezrobot.pages.web.common.Web_Com_PaymentGateway;
import com.rezrobot.pages.web.common.Web_Com_TermsPage;
import com.rezrobot.pages.web.common.Web_Com_Verify_selected_products;
import com.rezrobot.pages.web.hotel.WEB_Hotel_Paymentpage_occupancy_details;
import com.rezrobot.pages.web.hotel.Web_Hotel_ConfirmationPage;
import com.rezrobot.pages.web.hotel.Web_Hotel_paymentOccupancyPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.PoiExcelReader;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.verifications.Car_Verify;

public class CarWebRunner {

	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private String Browser = "N/A";
	
	private Web_Com_HomePage 								home 							= new Web_Com_HomePage();
	private WEB_Car_Results_Page							resultsPAge						= new WEB_Car_Results_Page();
	private Web_Com_CustomerDetailsPage 					cusDetails 						= new Web_Com_CustomerDetailsPage();
	private Web_Com_BillingInformationPage					billinginfo						= new Web_Com_BillingInformationPage();
	private Web_Com_NotesPage								notes							= new Web_Com_NotesPage();
	private Web_Com_TermsPage								terms 							= new Web_Com_TermsPage();
	private Web_Com_Verify_selected_products				verifyProducts					= new Web_Com_Verify_selected_products();
	private Web_Com_PaymentGateway							pg								= new Web_Com_PaymentGateway();
	private WEB_Car_ConfirmationPage						confirmPage						= new WEB_Car_ConfirmationPage();
	private	WEB_Car_Payment_Occupancy						occupancyPage					= new WEB_Car_Payment_Occupancy();
	
	
	//extent report
	private 												ReportTemplate					PrintTemplate;
	private 												HtmlReportTemplate				HtmlReportTemplate;
	private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	
	//property files
	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalSpecificData 				= labelProperty.getPortalSpecificData();
	
	Car_Verify												verify							= new Car_Verify();
	
	//excels
	private String											searchExcel						= portalSpecificData.get("carSearchExcel");
	
	@BeforeClass
	public void setup() throws Exception
	{
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		report = new ExtentReports("Reports/Car-Reservation flow-"+ today +".html", NetworkMode.OFFLINE);
        ExtentReportTemplate.getInstance().Initialize(report);
	}
	
	@Test(priority=1)
	public void search() throws Exception
	{
		System.out.println(searchExcel);
		//ExcelReader 									newReader 				= 	new ExcelReader();
		PoiExcelReader									newReader				= 	new PoiExcelReader();
		FileInputStream 								stream 					=	new FileInputStream(new File(searchExcel));
		ArrayList<Map<Integer, String>> 				ContentList 			= 	newReader.reader(stream);
		Map<Integer, String>  							content 				= 	ContentList.get(0);
		Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
		
		CarSearchObject							searchObject			= 	new CarSearchObject();
		CarSearchObject							search					= 	searchObject.setData();
		if(search.getExecute().equals("yes"))
		{
			if(search.getBookingChannel().equals("web"))
			{
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlWeb"));
				PrintTemplate.verifyTrue(true, home.isPageAvailable(), "Home page availability");
				if (home.isPageAvailable()) 
				{
					
					verify.checkCarBecAvailability(PrintTemplate, home);
					home.searchForCar(search);

				}
			}
		}
		/*while(mapiterator.hasNext())
		{
			try {
				HashMap<String, String> 				xmlUrls 				= 	new HashMap<>();
				Map.Entry<Integer, String> 				Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
				String 									Content 				= 	Entry.getValue();
				String[] 								searchDetails 			= 	Content.split(",");
				CarSearchObject							searchObject			= 	new CarSearchObject();
				CarSearchObject							search					= 	searchObject.setData(searchDetails);
				if(search.getExecute().equals("yes"))
				{
					if(search.getBookingChannel().equals("web"))
					{
						DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlWeb"));
						if (home.isPageAvailable()) 
						{
							verify.checkCarBecAvailability(PrintTemplate, home);
							home.searchForCar(search);
	
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			
		}*/
	}
	
	@Test(priority=2)
	public void resultsAvailability() throws Exception
	{
		resultsPAge.checkResultsAvailability();
		Assert.assertEquals(resultsPAge.checkResultsAvailability(), true, "Check Car Results Availability");
		ExtentReportTemplate.addtoReport("Car Results Availability", "Results Should Be Available","Results Available", true);
		resultsPAge.selectCar();
	}
	
	@Test(priority=3)
	public void customerDetailsAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, cusDetails.isCustomerDetailsAvailable(), "Customer details availability");
		cusDetails.isCustomerDetailsAvailable();
		cusDetails.enterCustomerDetails(portalSpecificData, "hotel");
		cusDetails.proceedToOccupancy();
	}
	
	@Test(priority=4)
	public void occupancyAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, occupancyPage.isAvailable(), "Occupancy details availability");
		occupancyPage.isAvailable();
		occupancyPage.next();
	}
	
	@Test(priority=5)
	public void billingInfoAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, billinginfo.isBillingInformationsAvailable(), "Billing information availability");
		billinginfo.isBillingInformationsAvailable();
		billinginfo.fillSpecialNotes();
	}
	
	@Test(priority=6)
	public void notesAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, notes.isSpecialNotesAvailable(), "Notes availability");
		notes.isSpecialNotesAvailable();
		notes.loadTermsandConditions();
	}
	
	@Test(priority=7)
	public void termsAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, terms.isTermsAndConditionsAvailable(), "Terms availability");
		terms.getTCMandatoryInputFields();
		terms.isTermsAndConditionsAvailable();
		terms.checkAvailability();
	}
	
	@Test(priority=8)
	public void verifyProductsAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, verifyProducts.isVerifyProductsAvailable(), "Verify products availability");
		verifyProducts.isVerifyProductsAvailable();
		verifyProducts.continueToPG();	
	}
	
	@Test(priority=9)
	public void pgAvailability() throws Exception
	{
		HashMap<String, String> confiDetails = new HashMap<>();
		PrintTemplate.verifyTrue(true, pg.isPaymentGatewayLoaded(confiDetails), "PG availability");
		pg.isPaymentGatewayLoaded(confiDetails);
		pg.enterCardDetails(portalSpecificData);
		pg.confirm();
	}
	
	
	@Test(priority=10)
	public void confirmationPageAvailability() throws Exception
	{
		PrintTemplate.verifyTrue(true, confirmPage.isAvailable(), "Confirmation page availability");
		System.out.println(confirmPage.isAvailable());
		confirmPage.isAvailable();
		confirmPage.getConfirmationNumber();
	}
	
	@AfterMethod
	public void end(ITestResult result)
	{
		if(result.getStatus() == ITestResult.FAILURE){
			ExtentReportTemplate.addtoReport(result.getTestName(), "Should be success",result.getThrowable().toString(), false);
	
		}
	}
	
}
