package com.rezrobot.runner;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.HotelObject;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.flight_circuitry.Costs;
import com.rezrobot.flight_circuitry.FlightReservation;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_BookingConfirmation_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_BookingCard;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_LiveBookingMoreInfo;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.CC.common.CC_Com_Cancellation_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_ConfirmationMail_Report_hotel;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLossReport_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLoss_Report;
import com.rezrobot.pages.CC.common.CC_Com_ReservationReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_Reservation_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_criteria;
import com.rezrobot.pages.CC.common.CC_Com_VoucherMail_Hotel;
import com.rezrobot.pages.CC.hotel.CC_Hotel_CancellationPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;

public class FlightReportRunner {

	
	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private String Browser = "N/A";
	
	private 												ReportTemplate					PrintTemplate;
	private 												HtmlReportTemplate				HtmlReportTemplate;
	private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	
	private CC_Com_ReservationReport_criteria				resReportCriteria				= new CC_Com_ReservationReport_criteria();
	private CC_Com_Reservation_Report						resReport						= new CC_Com_Reservation_Report(); 
	private CC_Com_BookingListReport_criteria				bookingListCriteria				= new CC_Com_BookingListReport_criteria();
	private CC_Com_BookingList_Report						bookingList						= new CC_Com_BookingList_Report();
	private CC_Com_BookingList_LiveBookingMoreInfo			bookingListLiveBooking			= new CC_Com_BookingList_LiveBookingMoreInfo();
	private CC_Com_BookingListReport_BookingCard			bookingListBookingCard			= new CC_Com_BookingListReport_BookingCard();
	private CC_Com_ProfitAndLossReport_Criteria				profitAndLossCriteria			= new CC_Com_ProfitAndLossReport_Criteria();
	private CC_Com_ProfitAndLoss_Report						profitAndLossReport				= new CC_Com_ProfitAndLoss_Report();
	private CC_Com_ThirdPartySupplierPayable_criteria		thirdPartySupPayableCriteria	= new CC_Com_ThirdPartySupplierPayable_criteria();
	private CC_Com_ThirdPartySupplierPayable_Report			thirdPartySupPayableReport		= new CC_Com_ThirdPartySupplierPayable_Report();
	private CC_Com_BookingConfirmation_criteria 			confirmationMailCriteria		= new CC_Com_BookingConfirmation_criteria();
	private CC_Com_ConfirmationMail_Report_hotel			confirmationMail				= new CC_Com_ConfirmationMail_Report_hotel();					
	private CC_Com_LoginPage								login							= new CC_Com_LoginPage();
	private CC_Com_BO_Menu									bomenu							= new CC_Com_BO_Menu();
	private CC_Com_VoucherMail_Hotel						voucher							= new CC_Com_VoucherMail_Hotel();
	private CC_Com_Cancellation_Criteria					cancellationCriteria			= new CC_Com_Cancellation_Criteria();
	private CC_Hotel_CancellationPage						cancellation					= new CC_Hotel_CancellationPage();
	
	
	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalDetails 				= labelProperty.getPortalSpecificData();
	
	//serialized object
	private Reservation										hotelResObject					= new Reservation();
	
	
	@Before
	public void setUp() throws Exception {
		
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		report = new ExtentReports("../Rezrobot_Details/Hotel/Reports/ExtentHotel-ReportOnly"+ today +".html", NetworkMode.OFFLINE);
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
	}
	
	@Test
	public void test() {
		
		Reservation	reservation	= Serializer.DeSerializeReservation("../Rezrobot_Details/object/date/portal/Flight/Confirmed_Reservations/", portalDetails.get("Portal.Name"));
		FlightReservation flightReservation = reservation.getFlightReservation();
		
		Costs InPortalCurrency = flightReservation.getXMLSelectFlight().getPricinginfo().getInPortalCurrency();
		Costs InSupplierCurrency = flightReservation.getXMLSelectFlight().getPricinginfo().getInSupplierCurrency();
		Costs InSellingCurrency = flightReservation.getXMLSelectFlight().getPricinginfo().getInSellingCurrency();
		
		
		
	}
	
	
	
}
