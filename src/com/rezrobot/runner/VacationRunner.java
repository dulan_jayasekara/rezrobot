package com.rezrobot.runner;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.common.objects.VacationObject;
import com.rezrobot.common.pojo.HotelRates;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.CommonBillingInfoRates;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.dataobjects.Vacation_Rate_Details;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.enumtypes.XMLFileType;
import com.rezrobot.enumtypes.XMLLocateType;
import com.rezrobot.flight_circuitry.FlightReservation;
import com.rezrobot.flight_circuitry.Initiator;
import com.rezrobot.flight_circuitry.ReservationPassengerInfo;
import com.rezrobot.flight_circuitry.xml_object_readers.FareRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.FareResponseReader;
import com.rezrobot.flight_circuitry.xml_object_readers.PriceRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.PriceResponseReader;
import com.rezrobot.flight_circuitry.xml_object_readers.ResvRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.ResvResponseReader;
import com.rezrobot.flight_circuitry.xml_objects.FareRequest;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.flight_circuitry.xml_objects.PriceRequest;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;
import com.rezrobot.flight_circuitry.xml_objects.ReservationRequest;
import com.rezrobot.flight_circuitry.xml_objects.ReservationResponse;
import com.rezrobot.pages.CC.common.CC_Com_Admin_ConfigurationPage;
import com.rezrobot.pages.CC.common.CC_Com_Admin_Home;
import com.rezrobot.pages.CC.common.CC_Com_Admin_WebSiteDetails;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_BookingConfirmation_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_BookingCard;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_LiveBookingMoreInfo;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.CC.common.CC_Com_ConfirmationMail_Report_hotel;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLossReport_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLoss_Report;
import com.rezrobot.pages.CC.common.CC_Com_ReservationReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_Reservation_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_criteria;
import com.rezrobot.pages.web.common.Web_Com_BillingInformationPage;
import com.rezrobot.pages.web.common.Web_Com_CustomerDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.pages.web.common.Web_Com_NotesPage;
import com.rezrobot.pages.web.common.Web_Com_PaymentGateway;
import com.rezrobot.pages.web.common.Web_Com_TermsPage;
import com.rezrobot.pages.web.common.Web_Com_Verify_selected_products;
import com.rezrobot.pages.web.flights.Web_Flight_PassengerDetails;
import com.rezrobot.pages.web.hotel.Web_Hotel_paymentOccupancyPage;
import com.rezrobot.pages.web.vacation.WEB_Vacation_ConfirmationPage;
import com.rezrobot.pages.web.vacation.WEB_Vacation_PaymentPage_MoreDetails;
import com.rezrobot.pages.web.vacation.WEB_Vacation_ResultsPage;
import com.rezrobot.processor.HotelCancellationPolicyProcesser;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.types.UserType;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.CurrencyConverter;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.FlightJsoupDoc;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.PoiExcelReader;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
import com.rezrobot.utill.TaxCalculator;
import com.rezrobot.utill.xmlPathGenerator;
import com.rezrobot.verifications.Hotel_Verify;
import com.rezrobot.verifications.Vacation_Verify;
import com.rezrobot.xml_readers.GtaReader;
import com.rezrobot.xml_readers.HbReader;
import com.rezrobot.xml_readers.RezLiveReader;
import com.rezrobot.xml_readers.TouricoReader;

public class VacationRunner {

	
	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
//	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";
	
	//property files
	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalSpecificDetails 			= labelProperty.getPortalSpecificData();
	private HashMap<String, String> 						commonLabels					= labelProperty.getCommonLabels();
	private HashMap<String, String> 						hotelLabels						= labelProperty.getHotelLabelProperties();
	
	//excels
	private String											searchExcel						= portalSpecificDetails.get("vacationSearchExcel");
	private String 											customerExcel					= "Resources/excels/hotel/customer.xls"; // use property files
	private String											occupancyExcel					= portalSpecificDetails.get("occupancyExcel");
	private String											paymentExcel					= "Resources/excels/hotel/paymentDetails.xls"; // use property files
	
	
	//supplierwise readers
	private HbReader 										hb 								= new HbReader();
	private GtaReader 										inv27 							= new GtaReader();
	private TouricoReader 									inv13 							= new TouricoReader();
	private RezLiveReader									rezlive							= new RezLiveReader();
	
	//xml objects
	private hotelDetails 									hotelAvailabilityReq 			= new hotelDetails();
	private ArrayList<hotelDetails> 						hotelAvailabilityRes 			= new ArrayList<hotelDetails>();
	private hotelDetails 									roomAvailablityReq 				= new hotelDetails();
	private hotelDetails 									roomAvailablityRes 				= new hotelDetails();
	private hotelDetails 									preReservationReq 				= new hotelDetails();
	private hotelDetails 									preReservationRes 				= new hotelDetails();
	private hotelDetails 									reservationReq 					= new hotelDetails();
	private hotelDetails 									reservationRes 					= new hotelDetails();
	private HashMap<String, String> 						paymentDetails      			= new HashMap<>(); // payment log details
	private FlightReservation 								flightReservation 				= new FlightReservation();
	
	
	//extent report ---
	private 												ReportTemplate					PrintTemplate;
	private 												HtmlReportTemplate				HtmlReportTemplate;
	private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	
	
	
	//pages
	private CC_Com_LoginPage								login							= new CC_Com_LoginPage();
	private CC_Com_Admin_WebSiteDetails						adminWebSiteDetails				= new CC_Com_Admin_WebSiteDetails();
	private CC_Com_BO_Menu									bomenu							= new CC_Com_BO_Menu();
	private CC_Com_Currency_Page							currency						= new CC_Com_Currency_Page();
	private Web_Com_HomePage 								home 							= new Web_Com_HomePage();
	private WEB_Vacation_ResultsPage						resultsPage						= new WEB_Vacation_ResultsPage();
	private Web_Com_CustomerDetailsPage						customerDetailsPage				= new Web_Com_CustomerDetailsPage();
	private Web_Flight_PassengerDetails						occupancyDetails				= new Web_Flight_PassengerDetails();
	private Web_Com_BillingInformationPage					billingInfo						= new Web_Com_BillingInformationPage();
	private Web_Com_NotesPage								notesPage						= new Web_Com_NotesPage();
	private	Web_Com_TermsPage								termsAndConditions				= new Web_Com_TermsPage();
	private Web_Com_Verify_selected_products				verifyProducts					= new Web_Com_Verify_selected_products();
	private Web_Com_PaymentGateway							pg								= new Web_Com_PaymentGateway();
	private WEB_Vacation_ConfirmationPage					confirmationPage				= new WEB_Vacation_ConfirmationPage();
	private Web_Hotel_paymentOccupancyPage					occupancyPage 					= new Web_Hotel_paymentOccupancyPage();
	private WEB_Vacation_PaymentPage_MoreDetails        	paymentPageMoreDetails			= new WEB_Vacation_PaymentPage_MoreDetails();
	private CC_Com_ReservationReport_criteria				resReportCriteria				= new CC_Com_ReservationReport_criteria();
	private CC_Com_Reservation_Report						resReport						= new CC_Com_Reservation_Report(); 
	private CC_Com_BookingListReport_criteria				bookingListCriteria				= new CC_Com_BookingListReport_criteria();
	private CC_Com_BookingList_Report						bookingList						= new CC_Com_BookingList_Report();
	private CC_Com_BookingList_LiveBookingMoreInfo			bookingListLiveBooking			= new CC_Com_BookingList_LiveBookingMoreInfo();
	private CC_Com_BookingListReport_BookingCard			bookingListBookingCard			= new CC_Com_BookingListReport_BookingCard();
	private CC_Com_ProfitAndLossReport_Criteria				profitAndLossCriteria			= new CC_Com_ProfitAndLossReport_Criteria();
	private CC_Com_ProfitAndLoss_Report						profitAndLossReport				= new CC_Com_ProfitAndLoss_Report();
	private CC_Com_ThirdPartySupplierPayable_criteria		thirdPartySupPayableCriteria	= new CC_Com_ThirdPartySupplierPayable_criteria();
	private CC_Com_ThirdPartySupplierPayable_Report			thirdPartySupPayableReport		= new CC_Com_ThirdPartySupplierPayable_Report();
	private CC_Com_BookingConfirmation_criteria 			confirmationMailCriteria		= new CC_Com_BookingConfirmation_criteria();
	private CC_Com_ConfirmationMail_Report_hotel			confirmationMail				= new CC_Com_ConfirmationMail_Report_hotel();					
	
	
	//parameters needed for verifications
	private	String 											defaultCC 						= "";
	private	HashMap<String, String> 						currencyMap  					= new HashMap<>();
	private	ArrayList<HotelOccupancyObject> 				occupancyList 					= new ArrayList<HotelOccupancyObject>();
	private Vacation_Search_object							search							= new Vacation_Search_object();
	private PriceRequest 									priceRequest 					= new PriceRequest();
	private PriceResponse 									priceResponse 					= new PriceResponse();
	private ReservationRequest 								reservationRequest 				= new ReservationRequest();
	private ReservationResponse 							reservationResponse 			= new ReservationResponse();
	private FareRequest 									fareRequest 					= new FareRequest();
	private FareResponse 									fareResponse 					= new FareResponse();
	private HotelDetails									selectedHotel					= new HotelDetails();
	private HashMap<String, String> 						selectedFlightDetails			= new HashMap<>();
	private ArrayList<HotelDetails> 						hotelList						= new ArrayList<HotelDetails>();
	private CurrencyConverter 								cc 								= new CurrencyConverter();
	private TaxCalculator 									taxCal 							= new TaxCalculator();
	
	//serializable objest
	private VacationObject									allDetails						= new VacationObject();
	private Serializer										serialize						= new Serializer();
	private Reservation										vacationResObject				= new Reservation();
	
	//read and save values
	private HashMap<String, String> 						confirmationPagevalues 			= new HashMap<>();
	
	@BeforeClass
	public void setup() throws Exception
	{
		System.out.println("***** Started *****");
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalSpecificDetails.get("Portal.Name");
		report = new ExtentReports("Reports/"+ portal+ "/Hotel-Reservation-"+ today +".html", NetworkMode.OFFLINE);
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
	}
	
	@Test(priority=1)
	public void isLoginPageAvailable() throws Exception
	{
		Vacation_Verify		verify = new Vacation_Verify();
		
		
		System.out.println(portalSpecificDetails.get("hotelUsername"));
		System.out.println(portalSpecificDetails.get("searchExcel"));
		//get admin config details
		System.out.println(portalSpecificDetails.get("PortalUrlCC"));
		System.out.println("***** Initiated the driver *****");
		DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("login")));
		login.getPortalHeader();
	
	
		CC_Com_Admin_Home 					adminHome 		= 	(CC_Com_Admin_Home) login.loginAs(UserType.ADMIN, portalSpecificDetails.get("adminUsername"), portalSpecificDetails.get("adminPassword"));
		System.out.println("***** Get configuration details *****");
		CC_Com_Admin_ConfigurationPage 		configpage 		= 	adminHome.getConfigurationScreen(portalSpecificDetails.get("PortalUrlCC"));
		HashMap<String, String> 			configDetails 	= 	configpage.getConfigProperties();
																adminWebSiteDetails.getWebSiteDetailsPage(portalSpecificDetails.get("PortalUrlCC"));
		System.out.println("***** Get website details *****");
		ArrayList<HashMap<String, String>> 	websiteDetails 	= 	adminWebSiteDetails.getDetails();
		portalSpecificDetails = labelProperty.changePortalSpecificDetails(configDetails, portalSpecificDetails);
	
		
		
		
		//change portal specific details
		
		/*System.out.println("abc");
		xmlPathGeneratorConcurent abc1 = new xmlPathGeneratorConcurent("1");
		abc1.start();
		xmlPathGeneratorConcurent abc2 = new xmlPathGeneratorConcurent("2");
		abc2.start();
		abc1.run();
		abc2.run();*/
		
//		PrintTemplate.markReportBegining("Rezgateway WEB Hotel Flow Smoke Test");
//		PrintTemplate.setInfoTableHeading("Test Basic Details");
		
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		
		//get xml property details
		xmlPathGenerator 								xmlUrlGenerator 		= new xmlPathGenerator();
		String 											url 					= "";
		
		String 											canDeadline 			= "";
		Browser = caps.getBrowserName();
		
		Map<String, String> info = new HashMap<String, String>();
		info.put("BrowserType", caps.getBrowserName());
		info.put("BrowserVersion", caps.getVersion());
		info.put("Platform", caps.getPlatform().toString());
		ReportTemplate.getInstance().setSystemInfo(info);

		
		
		//ExcelReader 									newReader 				= 	new ExcelReader();
		System.out.println("***** Read search excel *****");
		PoiExcelReader									newReader				= 	new PoiExcelReader();
		FileInputStream 								stream 					=	new FileInputStream(new File(searchExcel));
		ArrayList<Map<Integer, String>> 				ContentList 			= 	newReader.reader(stream);
		Map<Integer, String>  							content 				= 	ContentList.get(0);
		Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
		ArrayList<Vacation_Search_object>				searchList				= 	new ArrayList<Vacation_Search_object>();
		
		
		
		while(mapiterator.hasNext())
		{
			try {
				System.out.println("***** Read search excel and create search object *****");
				HashMap<String, String> 				xmlUrls 				= 	new HashMap<>();
				Map.Entry<Integer, String> 				Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
				String 									Content 				= 	Entry.getValue();
				String[] 								searchDetails 			= 	Content.split(",");
														search 					= 	search.vacationSearchObject(searchDetails);
				searchList.add(search);										
			}
			catch(Exception e)
			{
				System.out.println("Excel reading error - " + e);
			}
		}
		search = searchList.get(Integer.parseInt(portalSpecificDetails.get("vacationScenario"))-1);
		if(search.getExecute().equals("yes"))
		{
			//log and get currency details
			DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("login")));
//					PrintTemplate.setTableHeading("Check login page availability");
			try {
				PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			
			
			
			newReader 		= 	new PoiExcelReader();
			stream 			=	new FileInputStream(new File(occupancyExcel));
			ContentList 	= 	new ArrayList<Map<Integer,String>>();
			ContentList 	= 	newReader.reader(stream);
			content 		= 	new HashMap<Integer, String>();
			content   		= 	ContentList.get(0);
			Iterator<Map.Entry<Integer, String>>   	occupancyIterator 	= 	content.entrySet().iterator();
			
			while(occupancyIterator.hasNext())
			{
				Map.Entry<Integer, String> 	Entry 						= 	(Map.Entry<Integer, String>)occupancyIterator.next();
				String						Content 					= 	Entry.getValue();
				String[] 					occupancyDetails 			= 	Content.split(",");
				HotelOccupancyObject 		occ 						= 	new HotelOccupancyObject();
				occupancyList.add(occ.setData(occupancyDetails));	
			}
			
			Assert.assertEquals(login.isLoginPageAvailable(), true);
			if(login.isLoginPageAvailable())
			{
				HashMap<String, String> 				xmlUrls 				= 	new HashMap<>();
				if(search.getPaymentMethod().equals("offline"))
				{
					portalSpecificDetails.put("portal.ccfee", "0");
				}	
				login.typeUserName(portalSpecificDetails.get("hotelUsername"));
				login.typePassword(portalSpecificDetails.get("hotelPassword"));
				login.loginSubmit();
				//CC_Com_Admin_Home 				internalHome 		= (CC_Com_Admin_Home) login.loginAs(UserType.INTERNAL, portalSpecificData.get("hotelUsername"), portalSpecificData.get("hotelPassword"));
				
//				PrintTemplate.setTableHeading("Check BO menu page availability");
				PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
				if(bomenu.isModuleImageAvailable())
				{
					//get currency details
					DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("currencyUrl")));
					System.out.println("***** Get currency details *****");
					currencyMap = currency.getCCDetails();
					defaultCC = currency.getDefaultCurrecny();
					
					if(search.getChannel().equals("web"))
					{
						DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlWeb"));
						if (home.isPageAvailable()) 
						{	
							// serach
							System.out.println("***** Perform search *****");
							home.searchForVacation(search);
							
							//get results page details
							System.out.println("***** Check whether results page is loaded *****");
							if(resultsPage.isPageLoaded())
							{
								if(resultsPage.isResultsAvailable())
								{
									//get selected flight details
									System.out.println("***** Get selected flight details *****");
									selectedFlightDetails 	= resultsPage.getSelectedFlightDetails();
									// get hotel list details
									System.out.println("***** Get hotel list details *****");
									hotelList 				= resultsPage.getAllHotelDetails();
									//get selected hotel details
									System.out.println("***** Get selected hotel details *****");
									selectedHotel 			= resultsPage.getSelectedHotelInfo(search, hotelList, labelProperty, PrintTemplate);
									
									// READ FAREREQUEST XML
									try {
										// PREPARE REQ/RES XML LOG PREFIX
										XMLFileType ReqType = null;
										XMLFileType ResType = null;
										if (portalSpecificDetails.get("vacationPreferredAirline").equals("NONE")) {
											ReqType = XMLFileType.AIR2_PrefAirlineSearchRequest_;
											ResType = XMLFileType.AIR2_PrefAirlineSearchResponse_;
										} else {
											ReqType = XMLFileType.AIR2_LowFareSearchRequest_;
											ResType = XMLFileType.AIR2_LowFareSearchResponse_;
										}
										
										// READ FAREREQUEST XML
										System.out.println("***** Read flight availability request *****");
										FareRequestReader fareRequestReader = new FareRequestReader();
										fareRequest.setTracer(selectedFlightDetails.get("flightTracer"));
										fareRequest.setXMLFileType(ReqType.toString());
										Document fareReqDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, ReqType, selectedFlightDetails.get("flightTracer"), portalSpecificDetails.get("air2XmlLogPath"), portalSpecificDetails.get("flightSupplier"), fareRequest);
										fareRequestReader.RequestReader(fareReqDoc, fareRequest);
										flightReservation.setFareRequest(fareRequest);
										
										// READ FARERESPONSE XML
										System.out.println("***** Read flight availability response *****");
										FareResponseReader fareResponseReader = new FareResponseReader();
										fareResponse.setTracer(selectedFlightDetails.get("flightTracer"));
										fareResponse.setXMLFileType(ResType.toString());
										Document fareResDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, ResType, selectedFlightDetails.get("flightTracer"), portalSpecificDetails.get("air2XmlLogPath"), portalSpecificDetails.get("flightSupplier"), fareResponse);
										fareResponseReader.ResponseReader(fareResDoc, fareResponse);
										flightReservation.setFareResponse(fareResponse);
										
									} catch (Exception e) {
										// TODO: handle exception
									}
									
									// get availability xmls
									System.out.println("***** Read hotel availability request/reponse *****");
									if(search.getHotelSupplier().equals("hotelbeds_v2"))
									{
										//get availability request
										try {
											url = xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability request - ".concat(url));
											hotelAvailabilityReq = hb.availabilityReq(url);
											xmlUrls.put("Availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability request - " + e);
										}
										
										//get availability response	
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
											System.out.println("availability response - ".concat(url));
											hotelAvailabilityRes = hb.availabilityRes(url);
											xmlUrls.put("Availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability response - " + e);
										}
									}
									else if(search.getHotelSupplier().equals("INV27"))
									{
										//get availability request
										try {
											url = xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability request - ".concat(url));
											hotelAvailabilityReq = inv27.availabilityReq(url);
											xmlUrls.put("Availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability request - " + e);
										}
										
										
										//get availability response	
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability response - ".concat(url));
											hotelAvailabilityRes = inv27.availabilityRes(url);
											xmlUrls.put("Availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability response - " + e);
										}	
									}
									else if(search.getHotelSupplier().equals("rezlive"))
									{
										//get availability request
										try {
											url = xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability request - ".concat(url));
											hotelAvailabilityReq = rezlive.availabilityReq(url);
											xmlUrls.put("Availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability request - " + e);
										}	
										
										//get availability response	
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability response - ".concat(url));
											hotelAvailabilityRes = rezlive.availabilityRes(url);
											xmlUrls.put("Availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability response - " + e);
										}	
									}
									
									//get room availability xmls
									System.out.println("***** Read hotel room availability request/reponse *****");
									if(search.getHotelSupplier().equals("hotelbeds_v2"))
									{
										//get room availability request
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("room availability request - ".concat(url));
											roomAvailablityReq = hb.roomAvailabilityReq(url);
											xmlUrls.put("Room availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability request - " + e);
										}
										
										
										//get room availability response
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");
											System.out.println("room availability response - ".concat(url));
											roomAvailablityRes = hb.roomAvailabilityRes(url);
											xmlUrls.put("Room availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability response - " + e);
										}
										 
									}
									else if(search.getHotelSupplier().equals("INV27"))
									{
										//get room availability request
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability request - ".concat(url));
											roomAvailablityReq = inv27.roomAvailabilityReq(url); 
											xmlUrls.put("Room availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability request - " + e);
										}
										
										
										//get room availability response
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability response - ".concat(url));
											roomAvailablityRes = inv27.roomAvailabilityRes(url); 
											xmlUrls.put("Room availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability response - " + e);
										}
										
									}
									else if(search.getHotelSupplier().equals("rezlive"))
									{
										//get room availability request
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability request - ".concat(url));
											roomAvailablityReq = rezlive.roomAvailabilityReq(url); 
											xmlUrls.put("Room availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability request - " + e);
										}
										
										
										//get room availability response
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability response - ".concat(url));
											roomAvailablityRes = rezlive.roomAvailabilityRes(url); 
											xmlUrls.put("Room availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability response - " + e);
										}
									}
									
									//get cancellation deadline
									System.out.println("***** Get hotel cancellation deadline *****");
									HotelCancellationPolicyProcesser canDeadlineCreator = new HotelCancellationPolicyProcesser();
									canDeadline = canDeadlineCreator.getCancellationDeadline(search.getHotelSupplier(), roomAvailablityRes);
									
									// READ PRICEREQUEST XML
									System.out.println("***** Read flight price request *****");
									PriceRequestReader priceReqReader = new PriceRequestReader();
									priceRequest.setTracer(selectedFlightDetails.get("flightTracer"));
									priceRequest.setXMLFileType(XMLFileType.AIR2_PriceRequest_.toString());
									Document priceRequestDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR2_PriceRequest_, selectedFlightDetails.get("flightTracer"), portalSpecificDetails.get("air2XmlLogPath"), portalSpecificDetails.get("flightSupplier"), priceRequest);
									priceReqReader.RequestReader(priceRequestDoc, priceRequest);
									flightReservation.setPriceRequest(priceRequest);
									
									// READ PRICERESPONSE XML
									System.out.println("***** Read flight price response *****");
									PriceResponseReader priceResReader = new PriceResponseReader();
									priceResponse.setTracer(selectedFlightDetails.get("flightTracer"));
									priceResponse.setXMLFileType(XMLFileType.AIR2_PriceResponse_.toString());
									Document priceResponseDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR2_PriceResponse_, selectedFlightDetails.get("flightTracer"), portalSpecificDetails.get("air2XmlLogPath"), portalSpecificDetails.get("flightSupplier"), priceResponse);
									priceResReader.AmadeusPriceResponseReader(priceResponseDoc, priceResponse);
									flightReservation.setPriceResponse(priceResponse);
									
									// verify results page
									System.out.println("***** Verify results page *****");
									try {
										verify.resultsPageVerification(PrintTemplate, defaultCC, currencyMap, portalSpecificDetails, hotelAvailabilityRes, selectedFlightDetails, selectedHotel, flightReservation.getFareResponse(), search, hotelLabels, flightReservation.getPriceResponse());
									} catch (Exception e) {
										// TODO: handle exception
										System.out.println("results page verification error - " + e);
									}
									
									
									
									
									System.out.println("***** Check payment page - Customer details availability *****");
									if(customerDetailsPage.isCustomerDetailsAvailable())
									{
										// get payment page cart and ore details
										System.out.println("***** Get payment page - hotel more details *****");
										HashMap<String, String> paymentPageHotelMoreDetails 	= paymentPageMoreDetails.getHotelDetails(search);
										System.out.println("***** Get payment page - flight more details *****");
										HashMap<String, String> paymentPageFlightMoreDetails 	= paymentPageMoreDetails.getFlightDetails();
										System.out.println("***** Get payment page - shopping cart details *****");
										HashMap<String, String> paymentPageCartDetails 			= paymentPageMoreDetails.getCartDetails();
										 
										// varify payment page cart and more details
										System.out.println("***** Verify payment page cart and more details *****");
										verify.paymentPageCartAndMoreDetails(PrintTemplate, roomAvailablityRes, flightReservation.getPriceResponse(), search, currencyMap, defaultCC, portalSpecificDetails, paymentPageCartDetails, paymentPageFlightMoreDetails, paymentPageHotelMoreDetails, canDeadline);
										
										
										
										customerDetailsPage.enterCustomerDetails(portalSpecificDetails, "vacation");
										customerDetailsPage.proceedToOccupancy();
										ReservationPassengerInfo passengerNames = new ReservationPassengerInfo();
										passengerNames = Initiator.getPassengerDetails(search);
										flightReservation.setPassengerNames(passengerNames);
										occupancyDetails.setPassengerDetails(passengerNames);
										occupancyPage.enterHotelOccupancyDetails(occupancyList);
										
										occupancyDetails.proceedToBillingInfo();
										System.out.println("***** Check payment page - Billing info availability *****");
										if(billingInfo.isBillingInformationsAvailable())
										{
											CommonBillingInfoRates rates = billingInfo.getRates();
											// verify billing onfo
											System.out.println("***** Verify billing info *****");
											verify.paymentPageBillingInfo(portalSpecificDetails, currencyMap, defaultCC, rates, roomAvailablityRes, flightReservation.getPriceResponse(), PrintTemplate);
											
											billingInfo.fillSpecialNotes();
											System.out.println("***** Check payment page - Special notes availability *****");
											if(notesPage.isSpecialNotesAvailable())
											{
												notesPage.loadTermsandConditions();
												System.out.println("***** Check payment page - terms and conditions availability *****");
												if(termsAndConditions.isTermsAndConditionsAvailable())
												{
													termsAndConditions.getTCMandatoryInputFields();
													termsAndConditions.checkAvailability();
													try {
														verifyProducts.continueToPG();	
														Thread.sleep(5000);
													} catch (Exception e) {
														// TODO: handle exception
													}
													if(search.getPaymentMethod().equals("online"))
													{
														System.out.println("***** Check payment page - PG availability *****");
														if(pg.isPaymentGatewayLoaded(configDetails))
														{
															try {		
																System.out.println("***** Enter PG details *****");
																pg.enterCardDetails(portalSpecificDetails);
																System.out.println("***** PG confirm *****");
																pg.confirm();
																try {
																	System.out.println("***** Handle PG alert *****");
																	WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), 30);
																	wait.until(ExpectedConditions.alertIsPresent());
																	Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
																	Thread.sleep(2000);
															        alert.accept(); 
															        System.out.println("Navigate to the confirmation page");
																} catch (Exception e) {
																	// TODO: handle exception
																}
															} catch (Exception e) {
																// TODO: handle exception
																System.out.println(e);				
															}	
															
															// get confirmation page values
															System.out.println("***** Get confirmation values *****");
															if(confirmationPage.getConfirmationPageAvailability())
															{
																confirmationPagevalues = confirmationPage.getValues();
															}
															
															
															
															//get pre reservation xmls
															System.out.println("***** Read hotel pre reservation request/response *****");
															if(search.getHotelSupplier().equals("hotelbeds_v2"))
															{
																//get pre reservation request
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
																	System.out.println("pre reservation request - ".concat(url));
																	preReservationReq = hb.preReservationReq(url);
																	xmlUrls.put("Pre reservation request", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Pre reservation request - " + e);
																}
																
																
																//get pre reservation response
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");	
																	System.out.println("pre reservation response - ".concat(url));
																	preReservationRes = hb.preReservationResponse(url);
																	xmlUrls.put("Pre reservation response", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Pre reservation response - " + e);
																}
																
															}
															else if(search.getHotelSupplier().equals("INV27"))
															{
																//get pre reservation request
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
																	System.out.println("pre reservation request - ".concat(url));
																	preReservationReq = inv27.preReservationReq(url);
																	xmlUrls.put("Pre reservation request", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Pre reservation request - " + e);
																}
																
																//get pre reservation response
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
																	System.out.println("pre reservation response - ".concat(url));
																	preReservationRes = inv27.preReservationResponse(url);
																	xmlUrls.put("Pre reservation response", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Pre reservation response - " + e);
																}
																
															}
															else if(search.getHotelSupplier().equals("rezlive"))
															{
																//get pre reservation request
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
																	System.out.println("pre reservation request - ".concat(url));
																	preReservationReq = rezlive.preReservationReq(url);
																	xmlUrls.put("Pre reservation request", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Pre reservation request - " + e);
																}
																
																//get pre reservation response
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
																	System.out.println("pre reservation response - ".concat(url));
																	preReservationRes = rezlive.preReservationRes(url);
																	xmlUrls.put("Pre reservation response", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Pre reservation response - " + e);
																}
																
															}
															
															
															//get reservation xmls
															System.out.println("***** Read hotel reservation request/response *****");
															if(search.getHotelSupplier().equals("hotelbeds_v2"))
															{
																//get reservation request
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
																	System.out.println("reservation request - ".concat(url));
																	reservationReq = hb.reservationReq(url);
																	xmlUrls.put("Reservation request", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Reservation request - " + e);
																}	
																
																
																//get reservation response
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");
																	System.out.println("reservation response - ".concat(url));
																	reservationRes = hb.reservationResponse(url);
																	xmlUrls.put("Reservation response", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Reservation response - " + e);
																}
																
															}
															else if(search.getHotelSupplier().equals("INV27"))
															{
																//get reservation request
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
																	System.out.println("reservation request - ".concat(url));
																	reservationReq = inv27.reservationReq(url);
																	xmlUrls.put("Reservation request", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Reservation request - " + e);
																}	
																
																
																//get reservation response
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
																	System.out.println("reservation response - ".concat(url));
																	reservationRes = inv27.reservationRes(url);
																	xmlUrls.put("Reservation response", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Reservation response - " + e);
																}
															}
															else if(search.getHotelSupplier().equals("rezlive"))
															{
																//get reservation request
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
																	System.out.println("reservation request - ".concat(url));
																	reservationReq = rezlive.reservationReq(url);
																	xmlUrls.put("Reservation request", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Reservation request - " + e);
																}	
																
																
																//get reservation response
																try {
																	url			= xmlUrlGenerator.returnUrl(portalSpecificDetails, search.getHotelSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
																	System.out.println("reservation response - ".concat(url));
																	reservationRes = rezlive.reservationRes(url);
																	xmlUrls.put("Reservation response", url);
																} catch (Exception e) {
																	// TODO: handle exception
																	System.out.println("Reservation response - " + e);
																}	
															}
															
															//read payment log
															System.out.println("***** Read payment log *****");
															try {
																paymentDetails = xmlUrlGenerator.getPaymentLog(portalSpecificDetails.get("portal.PaymentLog"), confirmationPagevalues.get("transactionID"), portalSpecificDetails);
															} catch (Exception e) {
																// TODO: handle exception
																System.out.println("Payment log error - " + e);
															}
															
															// READ RESERVATION REQUEST XML
															System.out.println("***** Read flight reservation request *****");
															ResvRequestReader reservationReqReader = new ResvRequestReader();
															reservationRequest.setTracer(selectedFlightDetails.get("flightTracer"));
															reservationRequest.setXMLFileType(XMLFileType.AIR2_ReservationRequest_.toString());
															Document reservationRequestDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR2_ReservationRequest_, selectedFlightDetails.get("flightTracer"), portalSpecificDetails.get("air2XmlLogPath"),portalSpecificDetails.get("flightSupplier"), reservationRequest);
															reservationReqReader.RequestReader(reservationRequestDoc, reservationRequest);
															flightReservation.setReservationRequest(reservationRequest);
															
															// READ RESERVATION RESPONSE XML
															System.out.println("***** Read flight reservation response *****");
															ResvResponseReader reservationResReader = new ResvResponseReader();
															reservationResponse.setTracer(selectedFlightDetails.get("flightTracer"));
															reservationResponse.setXMLFileType(XMLFileType.AIR2_ReservationResponse_.toString());
															Document reservationResponseDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR2_ReservationResponse_, selectedFlightDetails.get("flightTracer"), portalSpecificDetails.get("air2XmlLogPath"),portalSpecificDetails.get("flightSupplier"), reservationResponse);
															reservationResReader.ResponseReader(reservationResponseDoc, reservationResponse);
															flightReservation.setReservationResponse(reservationResponse);

															// verify confirmation page
															System.out.println("***** Verify confirmation page *****");
															Vacation_Rate_Details rateDetails = new Vacation_Rate_Details();
															try {
																rateDetails = 	verify.confirmationPage(confirmationPagevalues, reservationRes, flightReservation.getReservationResponse(), portalSpecificDetails, currencyMap, search, defaultCC, PrintTemplate, websiteDetails, paymentDetails, canDeadline);
															} catch (Exception e) {
																// TODO: handle exception
																System.out.println("Confirmation oage verification error - " + e);
															}
															
														
															// serilizable object
															System.out.println("***** Create serelizable object *****");
															String today = Calender.getToday("dd-MMM-yyyy");
															allDetails.setSearch(search);
															allDetails.setPortalSpecificData(portalSpecificDetails);
															allDetails.setHotelLabels(hotelLabels);
															allDetails.setCommonLabels(commonLabels);
															allDetails.setReservationRes(reservationRes);
															allDetails.setPaymentDetails(paymentDetails);
															allDetails.setConfigDetails(configDetails);
															allDetails.setWebsiteDetails(websiteDetails);
															allDetails.setHotelCanDeadline(canDeadline);
															allDetails.setFlightCanDeadline("");
															allDetails.setCanDeadline("");
															allDetails.setCurrencyMap(currencyMap);
															allDetails.setConfirmation(confirmationPagevalues);
															allDetails.setOccupancyList(occupancyList);
															allDetails.setRoomAvailablityRes(roomAvailablityRes);
															allDetails.setHotelAvailabilityRes(hotelAvailabilityRes);
															allDetails.setDefaultCurrency(defaultCC);
															allDetails.setRateDetails(rateDetails);
															if(confirmationPagevalues.get("reservationNumber").isEmpty() || confirmationPagevalues.get("reservationNumber").equals(null))
															{
																allDetails.setConfirmed(false);
															}
															else
															{
																allDetails.setConfirmed(true);
															}
															
															allDetails.setReportDate(Calender.getToday("dd-MM-yyyy"));
															allDetails.setFlightRes(reservationResponse);
															vacationResObject.setProduct("vacation");
															vacationResObject.setVacationReservation(allDetails);
															//save serialized object
															try {
																serialize.SerializeReservation(vacationResObject, portalSpecificDetails.get("Portal.Name"), "../Rezrobot_Details/object/");
															} catch (Exception e) {
																// TODO: handle exception
																System.out.println("Error while serializing - " + e);
															}
															
															
															
															checkReports(verify, currencyMap, occupancyList, search, defaultCC, search, defaultCC, today, canDeadline, websiteDetails, xmlUrls);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					else if(search.getChannel().equals("cc"))
					{
						
					}
				}
			}
		}
	}
	
	public void checkReports(Vacation_Verify verify, HashMap<String, String> currencyMap2, ArrayList<HotelOccupancyObject> occupancyList2, Vacation_Search_object search2, String defaultCC2, Vacation_Search_object search3, String defaultCC3, String today, String canDeadline, ArrayList<HashMap<String, String>> websiteDetails, HashMap<String, String> xmlUrls) throws Exception
	{
		//log to check reports
		DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("login")));
		try {
			Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
		if(login.isLoginPageAvailable())
		{
			login.typeUserName(portalSpecificDetails.get("hotelUsername"));
			login.typePassword(portalSpecificDetails.get("hotelPassword"));
			login.loginSubmit();
			
			PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
			
			if(bomenu.isModuleImageAvailable())
			{
				//reservation report criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("reservationReport")));
				HashMap<String, Boolean> 	resReportCriteriaComboboxAvailability 			= resReportCriteria.checkComboboxAvailability();
				HashMap<String, Boolean> 	resReportCriteriaLabelsAvailability  			= resReportCriteria.checkLabelAvailability();
				HashMap<String, Boolean> 	resReportCriteriaRadioButtonsAvailability  		= resReportCriteria.checkRadioButtonAvailability();
				HashMap<String, Boolean> 	resReportCriteriaButtonsAvailability  			= resReportCriteria.checkButtonxAvailability();
				HashMap<String, String>		resReportCriteriaLabels							= resReportCriteria.getLabels();
				
				
				//reservation report criteria check element availability
				verify.reservationReportCriteriaButtonsAvailability						(PrintTemplate, resReportCriteriaButtonsAvailability );
				verify.reservationReportCriteriaComboboxAvailability					(PrintTemplate, resReportCriteriaComboboxAvailability );
				verify.reservationReportCriteriaLabelsAvailability						(PrintTemplate, resReportCriteriaLabelsAvailability );
				verify.reservationReportCriteriaRadioButtonsAvailability				(PrintTemplate, resReportCriteriaRadioButtonsAvailability );
				verify.reservationReportCriteriaLabelsVerification						(PrintTemplate, resReportCriteriaLabels, labelProperty.getCommonLabels());
			
				//reservation report - enter criteria
				resReportCriteria.enterCriteria(confirmationPagevalues.get("reservationNumber"), "vacation");
				
				//reservation report - get values
			}
		}		
	}
	
	/*@Test (priority = 2)
	public void resultsPageVerification_selectedHotelName()
	{
		String hotelName = "Unavailable";
		for(int i = 0 ; i < hotelAvailabilityRes.size(); i++)
		{
			if(hotelAvailabilityRes.get(i).getHotel().equals(selectedHotel.getTitle()))
			{
				hotelName = hotelAvailabilityRes.get(i).getHotel();
				break;
			}
		}
		softas.assertEquals(selectedHotel.getTitle(), hotelName);
	}
	
	@Test (priority = 3)
	public void resultsPageVerification_selectedHotelDestination()
	{
		String destination = hotelAvailabilityRes.get(0).getLocation();
		softas.assertEquals(selectedHotel.getDestination(), hotelAvailabilityRes.get(0).getLocation());
	}
	
	@Test (priority = 4)
	public void resultsPageVerification_selectedHotelSupplier()
	{
		softas.assertEquals(selectedHotel.getSupplier(), search.getHotelSupplier());
	}
	
	@Test (priority = 5)
	public void resultsPageVerification_selectedHotelStarRating()
	{
		String starRating = "Unavailable";
		for(int i = 0 ; i < hotelAvailabilityRes.size(); i++)
		{
			if(hotelAvailabilityRes.get(i).getHotel().equals(selectedHotel.getTitle()))
			{
				starRating = hotelAvailabilityRes.get(i).getStarRating();
				break;
			}
		}
		softas.assertEquals(selectedHotel.getStarRating(), starRating);
	}
	
	@DataProvider ()
	public Object[][] resultsPageRates()
	{
		String rate = "0";
		
		for(int i = 0 ; i < hotelAvailabilityRes.size(); i++)
		{
			if(hotelAvailabilityRes.get(i).getHotel().equals(selectedHotel.getTitle()))
			{
				rate = hotelAvailabilityRes.get(i).getPrice();
				break;
			}
		}
		HashMap<String, String> rates = new HashMap<>();
		System.out.println(defaultCC);
		System.out.println(rate);
		System.out.println(selectedHotel.getCurrency());
		System.out.println(hotelAvailabilityRes.get(0).getCurrencyCode());
		String hotelRate 	= cc.convert(defaultCC, rate, selectedHotel.getCurrency(), currencyMap, hotelAvailabilityRes.get(0).getCurrencyCode());
		String pm			= taxCal.getTax(hotelRate, portalSpecificData.get("hotelpm"), defaultCC, selectedHotel.getCurrency(), currencyMap);
		String bf			= taxCal.getTax(hotelRate, portalSpecificData.get("hotelbf"), defaultCC, selectedHotel.getCurrency(), currencyMap);
		String ccFee		= taxCal.getTax(hotelRate, portalSpecificData.get("portal.ccfee"), defaultCC, selectedHotel.getCurrency(), currencyMap, portalSpecificData.get("portal.pgCurrency"));
		
		Object[][] data = new Object[4][2];
		data[0][0] = "hotelRate";
		data[1][0] = "pm";
		data[2][0] = "bf";
		data[3][0] = "ccFee";
		data[0][1] = hotelRate;
		data[1][1] = pm;
		data[2][1] = bf;
		data[3][1] = ccFee;
		
		return data;
	}
	
	@Test (priority = 6, dataProvider = "resultsPageRates")
	public void resultsPageVerification_selectedHotelRate(String name, String rates)
	{
		System.out.println(name);
		System.out.println(rates);
	}*/
				
	
	
	@AfterMethod
	public void end(ITestResult result)
	{
		System.out.println("quit");
	}
}
