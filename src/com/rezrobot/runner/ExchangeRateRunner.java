package com.rezrobot.runner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.rezrobot.dataobjects.ExchangeRateDetailsObject;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExcelReader;
import com.rezrobot.utill.ReportTemplate;

public class ExchangeRateRunner {
	
	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";

	@Before
	public void setup() throws Exception
	{
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter*/);
		DriverFactory.getInstance().initializeDriver();
	}
	
	@Test
	public void execute() throws Exception
	{
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		//PrintTemplate.addToInfoTabel("Browser Type", caps.getBrowserName());
		//PrintTemplate.addToInfoTabel("Browser Version", caps.getVersion());
		//PrintTemplate.addToInfoTabel("Platform", caps.getPlatform().toString());
		//PrintTemplate.markTableEnd();
		//Map<String, String> sysInfo = new HashMap<String, String>();
		//sysInfo.put("Browser Type", caps.getBrowserName());
		//sysInfo.put("Browser Version", caps.getBrowserName());
		//sysInfo.put("Platform", caps.getBrowserName());
		//ReportTemplate.getInstance().setSystemInfo(sysInfo);
		
		//excel
		String excepPath = "Resources/excels/hotel/CurrencyExchangeRate.xls";
		
		//pages
		CC_Com_Currency_Page							currency				= new CC_Com_Currency_Page();
		CC_Com_LoginPage								login					= new CC_Com_LoginPage();
		CC_Com_BO_Menu									bomenu					= new CC_Com_BO_Menu();
		//property files
		LabelReadProperties 							labelProperty 			= new LabelReadProperties();
		HashMap<String, String> 						portalSpecificData 		= labelProperty.getPortalSpecificData();
		
		//log 
		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
		login.typeUserName(portalSpecificData.get("hotelUsername"));
		login.typePassword(portalSpecificData.get("hotelPassword"));
		login.loginSubmit();
		if(bomenu.isModuleImageAvailable())
		{
			//get currency details
			DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("currencyUrl")));
			ArrayList<ExchangeRateDetailsObject> currentList =  currency.getAll();
			currency.uploadFile(excepPath);
			ArrayList<ExchangeRateDetailsObject> newList = currency.getNewRates();
			currency.clickConfirm();
			
			//read excel
			ExcelReader 									newReader 				= 	new ExcelReader();
			FileInputStream 								stream 					=	new FileInputStream(new File(excepPath));
			ArrayList<Map<Integer, String>> 				ContentList 			= 	new ArrayList<Map<Integer,String>>();
															ContentList 			= 	newReader.contentReading(stream);
			Map<Integer, String>  							content 				= 	new HashMap<Integer, String>();
															content   				= 	ContentList.get(0);
			Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
			
			ArrayList<ExchangeRateDetailsObject> 			rateList				= 	new ArrayList<ExchangeRateDetailsObject>();
			while(mapiterator.hasNext())
			{
				Map.Entry<Integer, String> 					Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
				String 										Content 				= 	Entry.getValue();
				String[] 									rateDetails 			= 	Content.split(",");
				ExchangeRateDetailsObject					rateObject				= 	new ExchangeRateDetailsObject();
				ExchangeRateDetailsObject 					rate 					= 	rateObject.extract(rateDetails);
				rateList.add(rate);
			}
		}

		
	}
	
	@After
	public void terminate()
	{
		
	}
}
