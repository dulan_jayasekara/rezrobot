/**
 * 
 */
package com.rezrobot.runner;

//import static org.junit.Assert.*;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Assert;
//import org.junit.AfterClass;
import org.junit.Before;
//import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.dataobjects.CommonBillingInfoRates;
import com.rezrobot.dataobjects.Common_verify_selected_products;
import com.rezrobot.enumtypes.*;
import com.rezrobot.flight_circuitry.AirConfig;
import com.rezrobot.flight_circuitry.CC_Com_AirRulesConfig;
import com.rezrobot.flight_circuitry.CalculateCosts;
import com.rezrobot.flight_circuitry.FlightReservation;
import com.rezrobot.flight_circuitry.Initiator;
import com.rezrobot.flight_circuitry.ReservationPassengerInfo;
import com.rezrobot.flight_circuitry.SearchObject;
import com.rezrobot.flight_circuitry.XMLPriceInfo;
import com.rezrobot.flight_circuitry.XMLPriceItinerary;
import com.rezrobot.flight_circuitry.xml_object_readers.FareRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.FareResponseReader;
import com.rezrobot.flight_circuitry.xml_object_readers.PriceRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.PriceResponseReader;
import com.rezrobot.flight_circuitry.xml_object_readers.ResvRequestReader;
import com.rezrobot.flight_circuitry.xml_object_readers.ResvResponseReader;
import com.rezrobot.flight_circuitry.xml_objects.FareRequest;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.flight_circuitry.xml_objects.PriceRequest;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;
import com.rezrobot.flight_circuitry.xml_objects.ReservationRequest;
import com.rezrobot.flight_circuitry.xml_objects.ReservationResponse;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_HomePage;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
//import com.rezrobot.flightCircuitry.xmlObjects.FareResponse;
import com.rezrobot.pages.web.common.Web_Com_BillingInformationPage;
import com.rezrobot.pages.web.common.Web_Com_CustomerDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_NotesPage;
import com.rezrobot.pages.web.common.Web_Com_PaymentGateway;
import com.rezrobot.pages.web.common.Web_Com_PaymentsPage;
import com.rezrobot.pages.web.common.Web_Com_TermsPage;
import com.rezrobot.pages.web.common.Web_Com_Verify_selected_products;
import com.rezrobot.pages.web.flights.Web_Flight_ConfirmationPage;
import com.rezrobot.pages.web.flights.Web_Flight_HomePage;
import com.rezrobot.pages.web.flights.Web_Flight_PassengerDetails;
import com.rezrobot.pages.web.flights.Web_Flight_ResultsPage;
import com.rezrobot.types.UserType;
import com.rezrobot.utill.Calender;
//import com.rezrobot.pojo.Link;
//import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.FlightJsoupDoc;
import com.rezrobot.utill.GlobalConfigurator;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.PropertySingleton;
import com.rezrobot.utill.ReadProperties;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
//import com.rezrobot.utill.Calender;
import com.rezrobot.verifications.flight.Verify_Flight_Home;
import com.rezrobot.verifications.flight.Verify_Flight_ResultPage;
import com.rezrobot.verifications.flight.Verify_Flight_XMLs;
import com.rezrobot.verifications.flight.Verify_Print_TestData;

/**
 * @author Dulan
 * 
 */
public class FlightWebRunner {

	// private HashMap<String, String> scriptConfig;
	private HashMap<String, String> portalInfo = new HashMap<>();
	private HashMap<String, String> flightLabels = new HashMap<>();
	private HashMap<String, String> adminConfiguration = new HashMap<>();
	private HashMap<String, String> currencyMap = new HashMap<>();
	private StringBuffer PrintWriter = null;
	private ReportTemplate ReportTemplate = null;
	private HtmlReportTemplate HtmlReportTemplate = null;
	private ExtentReportTemplate ExtentReportTemplate = null;
	private String Browser = "N/A";
	private String portalInfoPropPath = "";
	private String flightLabelsPropPath = "";
	CC_Com_Currency_Page currency = new CC_Com_Currency_Page();
	String portalCurrency = "";
	CC_Com_LoginPage login = new CC_Com_LoginPage();
	Web_Com_PaymentsPage paymentPage = new Web_Com_PaymentsPage();
	Web_Com_CustomerDetailsPage cusDetails = new Web_Com_CustomerDetailsPage();
	ReservationPassengerInfo passengerNames = new ReservationPassengerInfo();
	Web_Flight_PassengerDetails passengerDetailsPage = new Web_Flight_PassengerDetails();
	private Web_Com_BillingInformationPage billinginfo = new Web_Com_BillingInformationPage();
	private Web_Com_NotesPage notes = new Web_Com_NotesPage();
	private Web_Com_TermsPage terms = new Web_Com_TermsPage();
	private Web_Com_Verify_selected_products verifyProducts = new Web_Com_Verify_selected_products();
	private Web_Com_PaymentGateway pg = new Web_Com_PaymentGateway();
	Web_Flight_ConfirmationPage confirmationPage = new Web_Flight_ConfirmationPage();
	ExtentReports report = null;
	CalculateCosts calculate = new CalculateCosts();
	private String scriptConfigPropPath = "../Rezrobot_Details/Common/Config.properties";
	Reservation reservation = new Reservation();

	@Before
	public void setUp() throws Exception {

		// logger = Logger.getLogger(this.getClass());
		// DOMConfigurator.configure("log4j.xml");
		// scriptConfig = ReadProperties.getPropertyInMap(scriptConfigPropPath);

		// SET Config.Properties FILE PATH
		GlobalConfigurator.PROPERTY_FILE_PATH = scriptConfigPropPath;

		// GET portalspecific.properties FILE PATH
		portalInfoPropPath = PropertySingleton.getInstance().getProperty("PortalSpecificPropPath");
		System.out.println("POrtal Specific Property file path : " + portalInfoPropPath);

		// READ PORTAL SPECIFIC DETAILS
		portalInfo = ReadProperties.getPropertyInMap(portalInfoPropPath);
		System.out.println(portalInfo);

		// GET flightLabels.properties FILE PATH
		flightLabelsPropPath = portalInfo.get("flightLabelsPropPath");

		// READ EXPECTED OUTPUT FOR ALL LABEL VALUES OF FLIGHT WEB
		flightLabels = ReadProperties.getPropertyInMap(flightLabelsPropPath);

		DriverFactory.getInstance().initializeDriver();

		PrintWriter = new StringBuffer();

		String portal = portalInfo.get("PortalName");
       // String date = Calender.getToday("yyyy-MM-dd");
		// _"+date+"
		//report = new ExtentReports(PropertySingleton.getInstance().getProperty("ScreenshotPath") + "/"+ portal + "/" + date + "/FlightReservationReport.html", NetworkMode.OFFLINE);// EXTENTREPORTS INTEGRATION - BEGINING
        report = new ExtentReports("Reports/"+ portal+ "/FlightReservationReport.html", NetworkMode.OFFLINE);
		ReportTemplate = ReportTemplate.getInstance();
		ReportTemplate.Initialize(/* PrintWriter, report */);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);

		// ReportTemplate.markReportBegining("Rezgateway WEB Flight Flow Smoke Test");
	}

	@Test
	public void test() throws Exception {

		ArrayList<SearchObject> searchObjList = Initiator.getSearchObjList(portalInfo.get("flightSearchDetailsXLPath"));
		System.out.println(portalInfo.get("flightSearchDetailsXLPath"));
		System.out.println(portalInfo.get("flightAirCOnfigXLPath"));
		ArrayList<AirConfig> airConfigObjList = Initiator.getAirConfigObjList(portalInfo.get("flightAirCOnfigXLPath"));

		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();

		/*
		 * PrintTemplate.setInfoTableHeading("Test Basic Details"); PrintTemplate.addToInfoTabel("Browser Type", caps.getBrowserName()); PrintTemplate.addToInfoTabel("Browser Version",
		 * caps.getVersion()); PrintTemplate.addToInfoTabel("Platform", caps.getPlatform().toString()); String link =
		 * "<a href=".concat(portalInfo.get("flightPortalWEBUrl")).concat(">").concat(portalInfo.get("flightPortalWEBUrl")).concat("</a>"); PrintTemplate.addToInfoTabel("Portal", link);
		 * PrintTemplate.markTableEnd();
		 */

		// ADDING SYSTEM INFO
		Map<String, String> sysInfo = new HashMap<String, String>();
		sysInfo.put("Browser Type", caps.getBrowserName());
		sysInfo.put("Browser Version", caps.getVersion());
		sysInfo.put("Platform", caps.getPlatform().toString());
		sysInfo.put("Environment", caps.getPlatform().toString());
		String link = "<a href=".concat(portalInfo.get("PortalUrlWeb")).concat(">").concat(portalInfo.get("PortalUrlWeb")).concat("</a>");
		sysInfo.put("Portal", link);
		sysInfo.put("Environment", "Prod");
		ReportTemplate.getInstance().setSystemInfo(sysInfo);

		// EXTENTREPORTS INTEGRATION - END

		for (int i = 0; i < searchObjList.size(); i++) {

			FlightReservation flightReservation = new FlightReservation();

			// FIRST SEARCH
			SearchObject searchObject = searchObjList.get(i);
			flightReservation.setSearchObject(searchObject);
			AirConfig airConfigObject = airConfigObjList.get(i);

			if (searchObject.getExcecuteStatus()) {
				// BACK OFFICE URL
				String LoginUrl = portalInfo.get("PortalUrlCC") + "/admin/common/LoginPage.do";
				System.out.println("Flight Login URL : " + LoginUrl);
				DriverFactory.getInstance().getDriver().get(LoginUrl);
				// ExtentReport.addtoReport(report, "Login", "Check login page availability", "Login page should be available", login.isLoginPageAvailable()? "true":"false", true);
				// PrintTemplate.setTableHeading("Check login page availability");
				ReportTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Flight Login page Availability");
				/* PrintTemplate.markTableEnd(); */

				CC_Com_LoginPage login = new CC_Com_LoginPage();
				login.getPortalHeader();

				// CC_Com_Admin_Home adminHome = (CC_Com_Admin_Home) login.loginAs(UserType.ADMIN,"rezgadmin","rezgadmin123");
				// CC_Com_Admin_ConfigurationPage configpage = adminHome.getConfigurationScreen(portalInfo);
				// adminConfiguration = configpage.getConfigProperties();

				if (login.isLoginPageAvailable()) {
					ReportTemplate.verifyTrue(true,true, "Login Page Availability");
					// LOGIN TO BACK OFFICE
					CC_Com_HomePage Home = (CC_Com_HomePage) login.loginAs(UserType.INTERNAL, portalInfo.get("flightUserName"), portalInfo.get("flightPassword"));
					Thread.sleep(10000);
                    
					if(!Home.isPageAvailable()){
						ReportTemplate.verifyTrue(true,false, "Login Function Verification");
						Assert.fail();
	                }else {
	                	ReportTemplate.verifyTrue(true,true, "Login Function Verification");
	                 }
					
					// GET CURRENCY MAP AND PORTAL CURRENCY
					DriverFactory.getInstance().getDriver().get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("currencyUrl")));
					currencyMap = currency.getCCDetails();
					portalCurrency = currency.getDefaultCurrecny();

					// GO TO AIR CONFIGURATION PAGE
					DriverFactory.getInstance().getDriver().get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("AirConfigUrl")));
					CC_Com_AirRulesConfig Config = new CC_Com_AirRulesConfig();
					if (searchObject.getAirConfigurationStatus()) {
						// SET AIR CONFIGURATION
						Config.setConfiguration(airConfigObject);
					} else {
						// GET AIR CONFIGURATION
						airConfigObject = Config.getConfiguration();
					}
					// SET AIRCONFIGURATION OBJECT
					flightReservation.setAirConfigObject(airConfigObject);
					searchObject.setAirConfigObject(airConfigObject);
					Verify_Print_TestData.printAirConfigurationData(ReportTemplate, searchObject);

					// STARTING IN DESIRED CHANNEL - WEB OR CC
					if (searchObject.getBookingChannel().equalsIgnoreCase("Web")) {
						// OPEN WEB URL
						DriverFactory.getInstance().getDriver().get(portalInfo.get("PortalUrlWeb"));

						// PREPARE REQ/RES XML LOG PREFIX
						XMLFileType ReqType = null;
						XMLFileType ResType = null;
						if (!searchObject.getPreferredAirline().trim().equals("NONE")) {
							ReqType = XMLFileType.AIR1_PrefAirlineSearchRequest_;
							ResType = XMLFileType.AIR1_PrefAirlineSearchResponse_;
						} else {
							ReqType = XMLFileType.AIR1_LowFareSearchRequest_;
							ResType = XMLFileType.AIR1_LowFareSearchResponse_;
						}
						System.out.println("INFO -> REQ TYPE : " + ReqType.toString());
						System.out.println("INFO -> RES TYPE : " + ResType.toString());

						// INITIALIZE WEB HOME PAGE
						Web_Flight_HomePage home = new Web_Flight_HomePage();
						

						if (home.isPageAvailable()) {
							
							ReportTemplate.verifyTrue(true,true, "WEB - Home Page Availability");
                            home.selectFlightBEC();
                            
                        	if(!home.getBECAvailability()){
        						ReportTemplate.verifyTrue(true,false, "Web - BEC Availability");
        						Assert.fail();
        	                }else {
        	                	ReportTemplate.verifyTrue(true,true, "Web - BEC Availability");
        	                 }
							if (portalInfo.get("extra.validation.needed").equalsIgnoreCase("YES")) {
								// GENERAL RESULTS PAGE VALIDATIONS-------------------------------------------------------------------------------
								Verify_Flight_Home.generalHomePageValidaiton(ReportTemplate, home);
								// VALIDATION OF INPUT FIELDS
								HashMap<String, Boolean> AvailMap = home.getAvailabilityOfInputFields();
								Verify_Flight_Home.verify_ElementAvailability(ReportTemplate, home, AvailMap);
								// VALIDATION OF BEC LABELS
								HashMap<String, String> actualBecLabels = home.getFlightBECLabels();
								Verify_Flight_Home.verify_BECLabelAvailability(ReportTemplate, home, flightLabels, actualBecLabels);
								// VALIDATION OF BEC COMBOBOX ATTRIBUTE VALIDATION
								HashMap<String, String> expectedPortalProp = portalInfo;
								HashMap<String, String> actualPortalProp = home.getFlightComboBoxDataList();
								Verify_Flight_Home.verify_ComboBoxAttributes(ReportTemplate, expectedPortalProp, actualPortalProp);
								// MANDATORY FIELD VALIDATION
								HashMap<String, Boolean> MandatoryMap = home.getMandatoryList();
								Verify_Flight_Home.verify_MandatoryFields(ReportTemplate, MandatoryMap);
							}
							// DO FLIGHT SEARCH
							Web_Flight_ResultsPage resultPage = home.doFlightSearch(searchObject);

							// GENERAL RESULTS PAGE VALIDATIONS
							Verify_Flight_ResultPage.generalResultsPageValidaiton(ReportTemplate, resultPage);


							if (resultPage.isPageAvailable()) {

								System.out.println("---> RESULTS PAGE AVAILABLE");
								resultPage.setTracer();
								searchObject.setTracer(resultPage.getTracer());
								System.out.println("Tracer ID : " + resultPage.getTracer());

								if (resultPage.isResultsAvailable()) {

									// VALIDATION OF RESULTS PAGE INPUT FIELDS ----------------------------------------------------------------------------------------
									HashMap<String, Boolean> resutlsPageAvailMap = resultPage.getAvailabilityFields();
									Verify_Flight_ResultPage.inputFieldValidation(ReportTemplate, resutlsPageAvailMap, resultPage);

									// VALIDATION OF SET VALUES IN RESULTS PAGE AFTER SEARCH ---------------------------------------------------------------------------
									HashMap<String, String> setValues = resultPage.getAfterSearchSetValues();
									Verify_Flight_ResultPage.searchAgainValidation(ReportTemplate, setValues, searchObject);

									// VALIDATION OF RESULTS PAGE LABELS -----------------------------------------------------------------------------------------------
									HashMap<String, String> resultPageActualLabels1 = resultPage.getLabels();
									Verify_Flight_ResultPage.defaultLabelValidation(ReportTemplate, flightLabels, resultPageActualLabels1);

									// TEST SEARCH AGAIN FUNCTIONALITY
									if (searchObject.isSearchAgain()) {

										resultPage.searchAgain(searchObject);

										if (resultPage.isResultsAvailable()) {

											System.out.println("---> RESULTS PAGE AVAILABLE AFTER SEARCH AGAIN");
											resultPage.setTracer();
											searchObject.setTracer(resultPage.getTracer());
											System.out.println("Tracer ID : " + resultPage.getTracer());

											Verify_Flight_ResultPage.resultsAvailablilty_AfterSearchAgain(ReportTemplate, resultPage);
											Verify_Flight_ResultPage.inputFieldValidation(ReportTemplate, resutlsPageAvailMap, resultPage);
											Verify_Flight_ResultPage.searchAgainValidation(ReportTemplate, setValues, searchObject);
											Verify_Flight_ResultPage.defaultLabelValidation(ReportTemplate, flightLabels, resultPageActualLabels1);
										}
									}

									// READ FAREREQUEST XML
									FareRequest fareRequest = new FareRequest();
									FareRequestReader fareRequestReader = new FareRequestReader();
									fareRequest.setTracer(resultPage.getTracer());
									fareRequest.setXMLFileType(ReqType.toString());
									Document fareReqDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, ReqType, resultPage.getTracer(), portalInfo.get("flightXmlBaseUrl"), portalInfo.get("flightSupplier"), fareRequest);
									fareRequestReader.RequestReader(fareReqDoc, fareRequest);
									flightReservation.setFareRequest(fareRequest);

									// VALIDATION OF LOWFAREREQUEST
									Verify_Flight_XMLs.verify_LowFareSearchRequest(ReportTemplate, searchObject, airConfigObject, fareRequest);

									// READ FARERESPONSE XML
									FareResponse fareResponse = new FareResponse();
									FareResponseReader fareResponseReader = new FareResponseReader();
									fareResponse.setTracer(resultPage.getTracer());
									fareResponse.setXMLFileType(ResType.toString());
									Document fareResDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, ResType, resultPage.getTracer(), portalInfo.get("flightXmlBaseUrl"), portalInfo.get("flightSupplier"), fareResponse);
									fareResponseReader.ResponseReader(fareResDoc, fareResponse);
									flightReservation.setFareResponse(fareResponse);

									// CALCULATE COSTS
									System.out.println("==========FLIGHT ITINERARY CALCULATIONS==========");
									for (int r = 0; r < fareResponse.getList().size(); r++) {
										System.out.println("Flight Itinerary : " + (r + 1));
										CalculateCosts.calculateCosts(fareResponse.getList().get(r).getPricinginfo(), airConfigObject, searchObject.getProfitType(), searchObject.getProfit(), searchObject.getBookingFeeType(),
												searchObject.getBookingFee(), portalInfo.get("flightCCFeeType"), Double.parseDouble(portalInfo.get("flightCCFee")), portalInfo.get("flightPayGatewayCurrency"), portalInfo.get("flightPortalCurrency"),
												searchObject.getSellingCurrency(), currencyMap);
									}

									// PRIVATE FLIGHT TEST
									/*
									 * if(!searchObject.getSelectingFlightType().equalsIgnoreCase("None")) { if(searchObject.getSelectingFlightType().equalsIgnoreCase("Private")) { for(int r=0;
									 * r<fareResponse.getList().size(); r++) { //System.out.println("Flight Itinerary : "+(r+1));
									 * //CalculateCosts.calculateCosts(fareResponse.getList().get(r).getPricinginfo(), airConfigObject, searchObject.getProfitType(), searchObject.getProfit(),
									 * searchObject.getBookingFeeType(), searchObject.getBookingFee(), portalInfo.get("flightCCFeeType"), Double.parseDouble(portalInfo.get("flightCCFee")),
									 * portalInfo.get("flightPayGatewayCurrency"), portalInfo.get("flightPortalCurrency"), searchObject.getSellingCurrency(), currencyMap);
									 * 
									 * String pricingSource = ""; pricingSource = fareResponse.getList().get(r).getPricinginfo().getPricingSource(); if(pricingSource.equalsIgnoreCase("Private")) { int
									 * c = 1; c = r+1; searchObject.setSelectingFlight(String.valueOf(c)); }
									 * 
									 * } } }
									 */

									// GET SELECTING ITEM FROM RESPONSE XML
									String selection = searchObject.getSelectingFlight();
									int selectFlight = Integer.parseInt(selection);
									XMLPriceItinerary XMLSelectFlight = new XMLPriceItinerary();
									try {
										System.out.println("INFO ----> GET THE SELECTED FLIGHT FROM XML");
										XMLSelectFlight = fareResponse.getList().get(selectFlight - 1);
										flightReservation.setXMLSelectFlight(XMLSelectFlight);
									} catch (Exception e) {

									}

									// READ AND SET RESULTS
									resultPage.setResults(searchObject, Integer.valueOf(portalInfo.get("flightPageIterations")), Integer.valueOf(portalInfo.get("flightIterations")));

									// VALIDATION OF ITINERARY LANGUAGE LABELS
									Verify_Flight_ResultPage.resultsItineraryLabelValidation(ReportTemplate, resultPage, flightLabels);
									Verify_Flight_ResultPage.resultsDataValidation(ReportTemplate, resultPage, fareResponse, searchObject.getTriptype());

									// ADDING SELECTED FLIGHT TO CART
									Thread.sleep(5000);
									resultPage.addToCart(resultPage.getResultlist().size(), Integer.parseInt(searchObject.getSelectingFlight()));

									// CART LABELS VALIDATIONS
									HashMap<String, String> actualCartDetails = resultPage.getCartDetails();
									Verify_Flight_ResultPage.cartValidation(ReportTemplate, actualCartDetails, XMLSelectFlight.getPricinginfo().getInSellingCurrency());

									// READ PRICEREQUEST XML
									PriceRequest priceRequest = new PriceRequest();
									PriceRequestReader priceReqReader = new PriceRequestReader();
									priceRequest.setTracer(resultPage.getTracer());
									priceRequest.setXMLFileType(XMLFileType.AIR1_PriceRequest_.toString());
									Document priceRequestDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR1_PriceRequest_, resultPage.getTracer(), portalInfo.get("flightXmlBaseUrl"), portalInfo.get("flightSupplier"), priceRequest);
									priceReqReader.RequestReader(priceRequestDoc, priceRequest);
									flightReservation.setPriceRequest(priceRequest);

									// VALIDATION OF PRICE REQUEST
									Verify_Flight_XMLs.verify_PriceRequest(ReportTemplate, searchObject, airConfigObject, priceRequest, XMLSelectFlight);

									// READ PRICERESPONSE XML
									PriceResponse priceResponse = new PriceResponse();
									PriceResponseReader priceResReader = new PriceResponseReader();
									priceResponse.setTracer(resultPage.getTracer());
									priceResponse.setXMLFileType(XMLFileType.AIR1_PriceResponse_.toString());
									Document priceResponseDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR1_PriceResponse_, resultPage.getTracer(), portalInfo.get("flightXmlBaseUrl"), portalInfo.get("flightSupplier"), priceResponse);
									priceResReader.AmadeusPriceResponseReader(priceResponseDoc, priceResponse);
									flightReservation.setPriceResponse(priceResponse);

									// CALCULATE COSTS FOR THE SENT PRICE RESPONSE
									XMLPriceInfo priceResponseFlight = new XMLPriceInfo();
									priceResponseFlight = priceResponse.getPriceinfo();
									CalculateCosts.calculateCosts(priceResponseFlight, airConfigObject, searchObject.getProfitType(), searchObject.getProfit(), searchObject.getBookingFeeType(), searchObject.getBookingFee(),
											portalInfo.get("flightCCFeeType"), Double.parseDouble(portalInfo.get("flightCCFee")), portalInfo.get("flightPayGatewayCurrency"), portalInfo.get("flightPortalCurrency"), searchObject.getSellingCurrency(),
											currencyMap);

									// CHECK IF A PRICE CHANGE THERE...
									XMLPriceInfo selectedLowFareResponseFlight = new XMLPriceInfo();
									selectedLowFareResponseFlight = XMLSelectFlight.getPricinginfo();
									// Verify_Flight_ResultPage.isPriceChange(ReportTemplate, selectedLowFareResponseFlight, priceResponseFlight);

									// IF RATE CHANGE IN PRICE RESPONSE - SET THE RATES OF PRICE RESPOSE TO RESERVATION OBJECT
									if (selectedLowFareResponseFlight.isIschanged()) {
										XMLSelectFlight.setPricinginfo(priceResponseFlight);
									}
									flightReservation.setXMLSelectFlight(XMLSelectFlight);

									// CHECK OUT
									try {
										resultPage.checkoutFlight();
									} catch (Exception e1) {
										e1.printStackTrace();
									}

									if (!paymentPage.isPaymentsPageAvailable()) {
										try {
											resultPage.checkoutFlight();
										} catch (Exception e) {

										}
									}

									// GENERAL PAYMENT PAGE VALIDATIONS
									/* Verify_Flight_PaymentPage.generalPaymentPageValidaiton(ReportTemplate, paymentPage); */

									if (paymentPage.isPaymentsPageAvailable()) {
										System.out.println("Payment Page Available");

										// VALIDATION OF PAYMENT PAGE COMMON LABELS ----------------------------------------------------------------------------------------
										/*
										 * HashMap<String, String> actualCommonLabels = paymentPage.getPaymentsPageCommonLabels(); Verify_Flight_PaymentPage.commonLabelValidation(ReportTemplate,
										 * actualCommonLabels, flightLabels);
										 */

										paymentPage.Flight_readBasket();
										cusDetails.enterCustomerDetails(portalInfo, "flight");
										cusDetails.proceedToOccupancy();

										passengerNames = Initiator.getPassengerDetails(searchObject);
										flightReservation.setPassengerNames(passengerNames);
										passengerDetailsPage.setPassengerDetails(passengerNames);
										passengerDetailsPage.proceedToBillingInfo();

										// billing information
										/* PrintTemplate.setTableHeading("Payment Page Verifications - Billing information"); */
										/* ReportTemplate.verifyTrue(true, billinginfo.isBillingInformationsAvailable(), "Check billing information Availability"); */
										/* PrintTemplate.markTableEnd(); */

										if (billinginfo.isBillingInformationsAvailable()) {
											CommonBillingInfoRates rates = billinginfo.getRates();
											// VERIFY RATES---------->>>
											billinginfo.fillSpecialNotes();
										}

										// CHECK NOTES AVAILABILITY
										/* PrintTemplate.setTableHeading("Payment Page Verifications - Notes"); */
										/* ReportTemplate.verifyTrue(true, notes.isSpecialNotesAvailable(), "Check notes information Availability"); */
										/* PrintTemplate.markTableEnd(); */
										if (notes.isSpecialNotesAvailable()) {
											// verify.webNotes(PrintTemplate, notes, labelPropertyMap);
											// notes.enterHotelNotes(portalSpecificData);
											notes.loadTermsandConditions();
										}

										// CHECK TERMS AND CONDITIONS AVAILABILITY
										/* PrintTemplate.setTableHeading("Payment Page Verifications - Terms and conditions"); */
										/* ReportTemplate.verifyTrue(true, terms.isTermsAndConditionsAvailable(), "Check terms and conditions Availability"); */
										/* PrintTemplate.markTableEnd(); */

										if (terms.isTermsAndConditionsAvailable()) {
											terms.getTCMandatoryInputFields();
											terms.checkAvailability();
											// verify.webTermsAndConditions(PrintTemplate, terms, labelPropertyMap);
										}

										// CHECK VERIFY SELECTED PRODUCTS AVAILABILITY
										/* PrintTemplate.setTableHeading("Payment Page Verifications - Verify selected products"); */
										/* ReportTemplate.verifyTrue(true, verifyProducts.isVerifyProductsAvailable(), "Check Verify selected products Availability"); */
										/* PrintTemplate.markTableEnd(); */
										System.out.println("Verify products");
										ArrayList<Common_verify_selected_products> productList = new ArrayList<Common_verify_selected_products>();
										Thread.sleep(5000);
										if (verifyProducts.isVerifyProductsAvailable()) {
											// productList = verifyProducts.getDetails(labelProperty);
											verifyProducts.continueToPG();
											Thread.sleep(5000);

											// READ RESERVATION REQUEST XML
											ReservationRequest reservationRequest = new ReservationRequest();
											ResvRequestReader reservationReqReader = new ResvRequestReader();
											reservationRequest.setTracer(resultPage.getTracer());
											reservationRequest.setXMLFileType(XMLFileType.AIR1_ReservationRequest_.toString());
											Document reservationRequestDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR1_ReservationRequest_, resultPage.getTracer(), portalInfo.get("flightXmlBaseUrl"),
													portalInfo.get("flightSupplier"), reservationRequest);
											reservationReqReader.RequestReader(reservationRequestDoc, reservationRequest);
											flightReservation.setReservationRequest(reservationRequest);

											// VALIDATION OF PRICE REQUEST
											Verify_Flight_XMLs.verify_ReservationRequest(ReportTemplate, searchObject, airConfigObject, reservationRequest, XMLSelectFlight);

											// READ RESERVATION RESPONSE XML
											ReservationResponse reservationResponse = new ReservationResponse();
											ResvResponseReader reservationResReader = new ResvResponseReader();
											reservationResponse.setTracer(resultPage.getTracer());
											reservationResponse.setXMLFileType(XMLFileType.AIR1_ReservationResponse_.toString());
											Document reservationResponseDoc = FlightJsoupDoc.getDoc(XMLLocateType.TRACER, XMLFileType.AIR1_ReservationResponse_, resultPage.getTracer(), portalInfo.get("flightXmlBaseUrl"),
													portalInfo.get("flightSupplier"), reservationResponse);
											reservationResReader.ResponseReader(reservationResponseDoc, reservationResponse);
											flightReservation.setReservationResponse(reservationResponse);

											// Check pg availability
											if (searchObject.getPaymentMode() == PaymentMode.Online) {
												pg.setPaymentGatewayName(portalInfo.get("flightPaymentGateway"));
												System.out.println("Payment Page Check payment gateway");
												/* PrintTemplate.setTableHeading("Payment Page Check payment gateway"); */
												/* ReportTemplate.verifyTrue(true, pg.isPaymentGatewayLoaded(portalInfo.get("flightPaymentGateway")), "Check payment gateway Availability"); */
												/* PrintTemplate.markTableEnd(); */
												if (pg.isPaymentGatewayLoaded(portalInfo.get("flightPaymentGateway"))) {
													try {
														pg.enterCardDetails(portalInfo);
														pg.confirm(portalInfo.get("flightPaymentGateway"));
													} catch (Exception e) {
														System.out.println(e);
													}

													if (confirmationPage.isPageAvailable()) {
														confirmationPage.setConfirmationDetails();
														System.out.println("RESERVATION NO : " + confirmationPage.getReservationNo());
														System.out.println("SUPPLIER CONF. NO : " + confirmationPage.getSupplierConfirmationNo());
														flightReservation.setReservationNo(confirmationPage.getReservationNo());
														flightReservation.setSupplierConfirmationNo(confirmationPage.getSupplierConfirmationNo());
														// ReportTemplate.setInfoTableHeading("Reservation Details");
														// ReportTemplate.addToInfoTabel("Reservation No", reservation.getReservationNo());
														// ReportTemplate.addToInfoTabel("Supplier Confirmation No", reservation.getSupplierConfirmationNo());
														// ReportTemplate.markTableEnd();

														HashMap<String, String> reservationDetails = new HashMap<String, String>();
														reservationDetails.put("Reservation No", flightReservation.getReservationNo());
														reservationDetails.put("Supplier Confirmation No", flightReservation.getSupplierConfirmationNo());
														ReportTemplate.setInfoTable("Reservation Details", reservationDetails);
													
														flightReservation.setSearchObject(searchObject);
														reservation.setProduct("Flight");
														reservation.setFlightReservation(flightReservation);
														Serializer.SerializeReservation(reservation, portalInfo.get("PortalName"), portalInfo.get("flightSerializePath"));
											            Verify_Print_TestData.printExcelData(ReportTemplate, flightReservation);	
													}

												}
											}
										}
									}
								} else {
									System.out.println("---> Results not Available");
								}// RESULTS PAGE - RESULTS AVAILABLE END

							} else {
								System.out.println("---> Results Page Not Available");
							}// RESULT PAGE AVAILABILE END

						} else {

							ReportTemplate.verifyTrue(true,false, "WEB - Home Page Availability");
							Assert.fail("Web - Home Page Not Available");
							// ReportTemplate.markTableEnd();
						}// HOME PAGE AVAILABILITY END

						// DriverFactory.getInstance().getDriver().quit();
						// PrintTemplate.setTableHeading("Reservation Information");
						// PrintTemplate.addToInfoTabel("Tracer ID", searchObject.getTracer());
						// PrintTemplate.markTableEnd();

					} else {
						System.out.println("---> BOOKING CHANNEL NOT WEB");
						// ReportTemplate.markTableEnd();
					}// IF WEB END

				} else {
					ReportTemplate.verifyTrue(true,false, "Login Page Availability");
					Assert.fail();
				}// LOGIN PAGE AVAILABILITY

			}// SEARCH OBJECT EXECUTE - YES

		

		}// END OF SEARCH OBJECT LIST FOR LOOP
	}

	@After
	public void tearDown() throws Exception {

		System.out.println("Driver quit");
		DriverFactory.getInstance().getDriver().quit();
	}

}
