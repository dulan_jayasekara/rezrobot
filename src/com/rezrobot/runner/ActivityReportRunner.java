package com.rezrobot.runner;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.ActivityReservation;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_BookingCard;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
import com.rezrobot.verifications.Activity_WEB_Validations;

public class ActivityReportRunner {

	private StringBuffer PrintWriter;
	private LabelReadProperties labelProperty;
	private ReportTemplate PrintTemplate;
	private HtmlReportTemplate	HtmlReportTemplate;
	private ExtentReportTemplate ExtentReportTemplate;
	private ExtentReports report = null;
	private String Browser = "N/A";
	private HashMap<String, String> currencyMap;
	
	//serialized object
	private ActivityReservation activityFileDetails = new ActivityReservation();
	private Reservation	activityObject	= new Reservation();
	
	
	@Before
	public void setUp() throws Exception {
		
		DriverFactory.getInstance().initializeDriver();
		labelProperty = new LabelReadProperties();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = labelProperty.getPortalSpecificData().get("PortalName");
		report = new ExtentReports("Reports/"+ portal+ "/Activity_ReportOnly-"+ today +".html", NetworkMode.OFFLINE);
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
		currencyMap = new HashMap<>();
			
	}
	
	@Test
	public void test() throws Exception {
		
		Reservation object = Serializer.DeSerializeReservation("../Rezrobot_Details/Activity/SavedObjects/date/portal/Activity/Confirmed_Reservations", labelProperty.getPortalSpecificData().get("PortalName"));
		activityFileDetails = object.getActivityReservation();
		
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		Browser = caps.getBrowserName();
		Map<String, String> sysInfo = new HashMap<String, String>();
		sysInfo.put("BrowserType", caps.getBrowserName());
		sysInfo.put("BrowserVersion", caps.getVersion());
		sysInfo.put("Platform", caps.getPlatform().toString());
		sysInfo.put("PortalURL_CC", labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("login")));
		sysInfo.put("PortalURL_WEB", labelProperty.getPortalSpecificData().get("PortalUrlWeb"));
		ReportTemplate.getInstance().setSystemInfo(sysInfo);
		
		Activity_WEB_Search searchObject = activityFileDetails.getSearchObject();
		ActivityDetails activityDetails = activityFileDetails.getActivityDetails();
		CC_Com_LoginPage loginPage						= new CC_Com_LoginPage();
		CC_Com_BO_Menu operationsPage					= new CC_Com_BO_Menu();
		CC_Com_Currency_Page currencyPage				= new CC_Com_Currency_Page();
		CC_Com_BookingListReport_criteria	bookingListCriteria			= new CC_Com_BookingListReport_criteria();
		CC_Com_BookingList_Report bookingListReport 					= new CC_Com_BookingList_Report();
		CC_Com_BookingListReport_BookingCard bookingList_BookingCard 	= new CC_Com_BookingListReport_BookingCard();
		Activity_WEB_Validations activityValidations	= new Activity_WEB_Validations(PrintTemplate, searchObject, labelProperty.getActivityLabelProperties(), labelProperty, currencyMap);		
		
		DriverFactory.getInstance().getDriver().get(labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("login")));
	
		boolean login = loginPage.isLoginPageAvailable();
		PrintTemplate.verifyTrue(true, login, "Login Page Availability");
		boolean operations = false;
		if(login == true){
			
			loginPage.typeUserName(labelProperty.getPortalSpecificData().get("ActivityUserName"));
			loginPage.typePassword(labelProperty.getPortalSpecificData().get("ActivityPassword"));
			loginPage.loginSubmit();
		
			operations = operationsPage.isModuleImageAvailable();
			PrintTemplate.verifyTrue(true, operations, "Login Details Checking");
			if(operations == true){
				
				DriverFactory.getInstance().getDriver().get(labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("currencyUrl")));
				currencyMap = currencyPage.getCCDetails();
				
				//Booking list search
				DriverFactory.getInstance().getDriver().get(labelProperty.getPortalSpecificData().get("PortalUrlCC").concat(labelProperty.getPortalSpecificData().get("bookingListReport")));
				activityValidations.getBookingListReportCriteriaLabelAvailability(bookingListCriteria);
				activityValidations.getBookingListReportCriteriaLabelVerification(bookingListCriteria, labelProperty.getCommonLabels());
				bookingListCriteria.enterCriteria(activityFileDetails.getReservationNo());
				
				//Booking list report				
				activityValidations.getBookingListReportHeadLabelsLoaded(bookingListReport);
				activityValidations.getBookingListReportLabelVerifications(bookingListReport, labelProperty.getCommonLabels());
				activityValidations.getBookingListReportValidations(bookingListReport, activityDetails);
				bookingListReport.clickActivityReservationNumber();
				
				//Booking list verifications (Booking Card)
				activityValidations.getBookingList_BookingCardLabelAvailability(bookingList_BookingCard);
				activityValidations.getBookingList_BookingCardLabelVerifications(bookingList_BookingCard, labelProperty.getHotelLabelProperties());
				
			}
		}
		
	}
	
	@After
	public void tearDown() throws Exception {
		DriverFactory.getInstance().getDriver().quit();
	}
	
}
