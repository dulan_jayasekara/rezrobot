package com.rezrobot.runner;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.HotelObject;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.common.objects.VacationObject;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.Vacation_Rate_Details;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.flight_circuitry.xml_objects.ReservationResponse;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_LiveBookingMoreInfo;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_ReservationReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_Reservation_Report;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
import com.rezrobot.verifications.Vacation_Verify;

public class VacationReportRunner {

	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
	private String Browser = "N/A";
	
	//extent report
	private 												ReportTemplate					PrintTemplate;
	private 												HtmlReportTemplate				HtmlReportTemplate;
	private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	
	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalDetails 					= labelProperty.getPortalSpecificData();
	
	//serialized object
	private VacationObject 									allDetails 						= new VacationObject();
	private Reservation										hotelResObject					= new Reservation();
	
	
	private CC_Com_LoginPage								login							= new CC_Com_LoginPage();
	private CC_Com_BO_Menu									bomenu							= new CC_Com_BO_Menu();
	private CC_Com_ReservationReport_criteria				resReportCriteria				= new CC_Com_ReservationReport_criteria();
	private CC_Com_Reservation_Report						resReport						= new CC_Com_Reservation_Report();
	private CC_Com_BookingList_Report						bookingList						= new CC_Com_BookingList_Report();
	private CC_Com_BookingListReport_criteria				bookingListCriteria				= new CC_Com_BookingListReport_criteria();
	private CC_Com_BookingList_LiveBookingMoreInfo			bookingListLiveBooking			= new CC_Com_BookingList_LiveBookingMoreInfo();
	
	@BeforeClass
	public void setup() throws Exception
	{
		DriverFactory.getInstance().initializeDriver();
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalDetails.get("PortalName");
		report = new ExtentReports("Reports/"+ portal+ "/Hotel-ReportOnly-"+ today +".html", NetworkMode.OFFLINE);
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
	}
	
	@Test
	public void test() throws Exception
	{
		Reservation 					object 					= Serializer.DeSerializeReservation("../Rezrobot_Details/object/date/portal/Vacation/Confirmed_Reservations/", portalDetails.get("Portal.Name"));
		try {
			System.out.println(object.getVacationReservation().getCurrencyMap());
			allDetails 				= object.getVacationReservation();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
										
										
		Vacation_Search_object 				search 						= allDetails.getSearch();
		HashMap<String, String> 			portalSpecificDetails 		= allDetails.getPortalSpecificData();
		HashMap<String, String> 			hotelLabels					= allDetails.getHotelLabels();
		HashMap<String, String> 			commonLabels				= allDetails.getCommonLabels();
		hotelDetails 						reservationRes				= allDetails.getReservationRes();
		HashMap<String, String> 			paymentDetails				= allDetails.getPaymentDetails();
		HashMap<String, String> 			configDetails				= allDetails.getConfigDetails();
		ArrayList<HashMap<String, String>> 	websiteDetails				= allDetails.getWebsiteDetails();
		String 								hotelCanDeadline 			= allDetails.getHotelCanDeadline();
		String								flightCanDeadline			= allDetails.getFlightCanDeadline();
		String 								canDeadline					= allDetails.getCanDeadline();
		HashMap<String, String> 			currencyMap					= allDetails.getCurrencyMap();
		HashMap<String, String> 			confirmationPagevalues		= allDetails.getConfirmation();
		ArrayList<HotelOccupancyObject> 	occupancyList				= allDetails.getOccupancyList();
		hotelDetails 						roomAvailablityRes			= allDetails.getRoomAvailablityRes();
		ArrayList<hotelDetails> 			hotelAvailabilityRes		= allDetails.getHotelAvailabilityRes();
		String								reportDate					= allDetails.getReportDate();
		ReservationResponse 				reservationResponse			= allDetails.getFlightRes();
		Vacation_Rate_Details				rateDetails					= allDetails.getRateDetails();
		String 								defaultCC					= allDetails.getDefaultCurrency();
		
		Vacation_Verify						verify 						= new Vacation_Verify();
		
		//log to check reports
		DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("login")));

		PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
		if(login.isLoginPageAvailable())
		{
			login.typeUserName(portalSpecificDetails.get("hotelUsername"));
			login.typePassword(portalSpecificDetails.get("hotelPassword"));
			login.loginSubmit();
			
			PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
			if(bomenu.isModuleImageAvailable())
			{
				//reservation report criteria
				System.out.println("reservation report criteria");
				DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("reservationReport")));
				HashMap<String, Boolean> 	resReportCriteriaComboboxAvailability 			= resReportCriteria.checkComboboxAvailability();
				HashMap<String, Boolean> 	resReportCriteriaLabelsAvailability  			= resReportCriteria.checkLabelAvailability();
				HashMap<String, Boolean> 	resReportCriteriaRadioButtonsAvailability  		= resReportCriteria.checkRadioButtonAvailability();
				HashMap<String, Boolean> 	resReportCriteriaButtonsAvailability  			= resReportCriteria.checkButtonxAvailability();
				HashMap<String, String>		resReportCriteriaLabels							= resReportCriteria.getLabels();
				
				
				//reservation report criteria check element availability
				System.out.println("reservation report criteria check element availability");
				verify.reservationReportCriteriaButtonsAvailability						(PrintTemplate, resReportCriteriaButtonsAvailability );
				verify.reservationReportCriteriaComboboxAvailability					(PrintTemplate, resReportCriteriaComboboxAvailability );
				verify.reservationReportCriteriaLabelsAvailability						(PrintTemplate, resReportCriteriaLabelsAvailability );
				verify.reservationReportCriteriaRadioButtonsAvailability				(PrintTemplate, resReportCriteriaRadioButtonsAvailability );
				verify.reservationReportCriteriaLabelsVerification						(PrintTemplate, resReportCriteriaLabels, labelProperty.getCommonLabels());
			
				//reservation report - enter criteria
				System.out.println("reservation report - enter criteria");
				resReportCriteria.enterCriteria(confirmationPagevalues.get("reservationNumber"), "vacation");
				
				//reservation report - get label availability
				System.out.println("reservation report - get label availability");
				HashMap<String, Boolean> resReportLabelAvailability = resReport.getLabelsAvailabiltiyVacation();
				
				//reservation report - get values
				System.out.println("reservation report - get values");
				HashMap<String, String> resReportValues 			= resReport.getValuesVacation();
				
				//reservation report - verify label availability
				System.out.println("reservation report - verify label availability");
				verify.reservationReportLabelAvailability(PrintTemplate, resReportLabelAvailability);
				
				//reservation report - verify values
				System.out.println("reservation report - verify values");
				verify.reservationReportValuesVerification(PrintTemplate, resReportValues, rateDetails, reservationRes, currencyMap, defaultCC, paymentDetails, confirmationPagevalues, reportDate, search, portalSpecificDetails, reservationResponse, occupancyList);
				
				//booking list report criteria
				System.out.println("booking list report criteria");
				DriverFactory.getInstance().getDriver().get(portalSpecificDetails.get("PortalUrlCC").concat(portalSpecificDetails.get("bookingListReport")));
				HashMap<String, Boolean> 	bookingListReportCriteriaLabelAvailability 			= bookingListCriteria.getLabelAvailability();
				HashMap<String, String> 	bookingListCriteriaLabels 							= bookingListCriteria.getLabels();
				verify.bookingListReportCriteriaLabelAvailability(PrintTemplate, bookingListReportCriteriaLabelAvailability);
				verify.bookingListReportCriteriaLabelVerification(PrintTemplate, bookingListCriteriaLabels, labelProperty.getCommonLabels());
				
				// booking list report enter search criteria
				System.out.println("booking list report enter search criteria");
				bookingListCriteria.enterCriteria(confirmationPagevalues.get("reservationNumber"));
				
				// booking list report get labels
				System.out.println("booking list report get labels");
				HashMap<String, Boolean> 	bookingListLabelAvailability 						= bookingList.getHotelLabelsAvailability();
				HashMap<String, String>	 	bookingListLabels 									= bookingList.getHotelLabels();
				HashMap<String, String> 	bookingListValues 									= bookingList.getHotelValues();
				verify.bookingListLabelAvailability(PrintTemplate, bookingListLabelAvailability);
				verify.bookingListLabelVerification(PrintTemplate, bookingListLabels, labelProperty.getCommonLabels());
				verify.bookingListValueVarification(); // todo
				
				//booking list - live booking more info
				HashMap<String, Boolean> 	bookingListLiveLabelAvailability 					= bookingListLiveBooking.getVacationLabelAvailability();
		//		HashMap<String, String>	 	bookingListLIveLabels 								= bookingListLiveBooking.getVacationLabels();
				HashMap<String, String>	 	bookingListLIveValues 								= bookingListLiveBooking.getVacationValues();
				bookingListLiveBooking.closeHotelMoreInfo();
				verify.bookingListLiveLabelAvailability(PrintTemplate, bookingListLiveLabelAvailability);
				//		verify.bookingListLiveLabelVerification();
				verify.bookingListLiveValueVerification(PrintTemplate, bookingListLIveValues, reservationRes, reservationResponse, search, portalSpecificDetails, rateDetails);
				
				bookingList.clickHotelReservationNumber();
				
			}
		}
	}
	
	@AfterMethod
	public void end(ITestResult result)
	{
		System.out.println("quit");
	}
}
