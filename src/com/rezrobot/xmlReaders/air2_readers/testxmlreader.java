package com.rezrobot.xmlReaders.air2_readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import jxl.write.DateFormat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rezrobot.utill.ReadProperties;

public class testxmlreader {


	String AppID="app2.";
	String PortalURL="";
	String Date="";
	String productType="";
	String Supplier="";
	String xmlType="";
	String TracerID="";
	String linktext="";
	String URL="";
	boolean multipleApps=false;

	
	public String portalUrlBuild() throws IOException {
		String[] apparray=ReadProperties.readpropeties("Resources\\portalSpecificData\\portalInfo.properties").get("FPxml.Appnumbers").split(",");
		System.out.println(apparray[1]);
		return PortalURL=ReadProperties.readpropeties("Resources\\portalSpecificData\\portalInfo.properties").get("FPxml.PortalPath");
	}
	
	public void appLoader() throws IOException {
		
		String[] apparray=ReadProperties.readpropeties("Resources\\portalSpecificData\\portalInfo.properties").get("FPxml.Appnumbers").split(",");
		System.out.println(apparray[1]);
		
	}

	public  String dateLoader() {



		SimpleDateFormat 	dateFormat 	= new SimpleDateFormat("dd-MM-yyyy");
		Calendar 	cal 		= Calendar.getInstance();
		Date= dateFormat.format(cal.getTime());
		

		return Date;

	}
	public Document Air2XmlDocLoader(String typexml,String tracer) {

		productType="air";
		Supplier="Sabre";
		xmlType=typexml;
		TracerID=tracer;

		Date=dateLoader();
		System.out.println("Portal URL"+PortalURL);
		System.out.println();

		return UrlLocator();

	}

	public Document UrlLocator() {

		Document xmlDoc = null;

		try {

			// need http protocol

			System.out.println("http://"+AppID+PortalURL+"/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2F"+productType+"xml%2F"+Date+"%2F"+Supplier+"+-+AIR2");
			Document doc = Jsoup.connect("http://"+AppID+PortalURL+"/jsp/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2F"+productType+"xml%2F"+Date+"%2F"+Supplier+"+-+AIR2").get();



			// get all links
			Elements links = doc.select("a[href]");
			for (Element link : links) {


				if ((link.attr("href").contains(xmlType+"_"+TracerID)&&(!link.attr("href").contains("downfile")))) {
					linktext=link.attr("href").toString();
					System.out.println(linktext);
					URL="http://"+AppID+PortalURL+linktext;
					System.out.println(URL);

				}





			}

			xmlDoc=Jsoup.parse(URL);


		} catch (IOException e) {
			e.printStackTrace();
		}


		return xmlDoc;

	}

	/*public static void main(String[] args) throws IOException {

		String AppID="app1.";
		String PortalURL="res.live.oman.lon.rack.iisys.org:9099";
		//String Date="15-06-2016";
		String productType="air";
		String Supplier="Sabre";
		String xmlType="ReservationResponse";
		String TracerID="23730";
		 	String linktext="";
		 String URL="";


		http://192.168.1.129/

		 testxmlreader read=new testxmlreader();
		System.out.println("URL"+read.portalUrlBuild());
		 read.dateLoader();
		 read.Air2XmlDocLoader("PrefAirlineSearchResponse", "2217");


	}*/

	//Multiple threads
}
