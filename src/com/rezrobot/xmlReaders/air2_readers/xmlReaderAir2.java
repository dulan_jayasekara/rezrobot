package com.rezrobot.xmlReaders.air2_readers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



import com.rezrobot.dataobjects.xml_objects.air2.SearchRequest;



public class xmlReaderAir2 {
	
	//Lowfare search req
	//LowfaresearchResponse
	//PriceRequest
	//Price response
	
	public String urlGenerator(String url,String supplier)
	{

		String URL = null;

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		String DATE = dateFormat.format(cal.getTime());
		URL = url.replace("DATE", DATE).replace("SUPPLIER", supplier);



		return URL;

	}
	public Document	 ReadDoc(String ridetracer,String XMLType) throws Exception {


		Document RetDoc = null;
		//input = new File(ridetracer);

		
		String PageSource		= null;	
		String TracerPrefix		= XMLType;
		String TracerValue		= ridetracer;
		String TracerID			= TracerPrefix.concat(TracerValue);

System.out.println(TracerID);
		
		String url=("http://dev3.rezg.net:8080/Browser.jsp?sort=-3&file=%2Fvar%2Flog%2Frezg%2Fapp%2Fairxml%2F15-07-2016%2FSabre+-+AIR2%2FAIR2_PrefAirlineSearchRequest_9775_2016-07-15_14%3A39%3A08.660.xml");
		String supplier="Sabre";

		String URL = urlGenerator(url,supplier);

		//driver.get(URL);

	/*	String urlall = "";
		*/
	
		try
		{
			
		
			RetDoc = Jsoup.parse(PageSource);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}


		return RetDoc;


	}
	
	public SearchRequest searchRequestReader(String ridetracer) throws Exception {

		com.rezrobot.dataobjects.xml_objects.air2.SearchRequest request=new SearchRequest();

		Document doc=ReadDoc(ridetracer, "_LowFareSearchRequest_");

		try {
			request.setOfficeID(doc.getElementsByTag("Userid").first().text());

			request.setInOriginLocation(doc.getElementsByTag("OriginLocation").get(0).attr("LocationCode"));
			request.setInDestinationLocation(doc.getElementsByTag("DestinationLocation").get(0).attr("LocationCode"));
			request.setInboundArrivalDate(doc.getElementsByTag("ArrivalDateTime").get(0).text().split("T")[0]);
			request.setInboundArrivalTime(doc.getElementsByTag("ArrivalDateTime").get(0).text().split("T")[1]);


			request.setOutOriginLocation(doc.getElementsByTag("OriginLocation").get(1).attr("LocationCode"));
			request.setOutDestinationLocation(doc.getElementsByTag("DestinationLocation").get(1).attr("LocationCode"));
			request.setOutboundDepartureDate(doc.getElementsByTag("DepartureDateTime").get(0).text().split("T")[0]);
			request.setOutboundDepartureTime(doc.getElementsByTag("DepartureDateTime").get(0).text().split("T")[1]);

			request.setSeatsRequested(doc.getElementsByTag("SeatsRequested").first().text());
			request.setCabinType(doc.getElementsByTag("CabinPref ").first().attr("Cabin"));
			request.setPricingSource(doc.getElementsByTag("PriceRequestInformation ").first().attr("PricingSource"));

			request.setAdults(doc.getElementsByAttributeValue("Code", "ADT").first().attr("Quantity"));
			request.setChilds(doc.getElementsByAttributeValue("Code", "CHD").first().attr("Quantity"));
			request.setInfants(doc.getElementsByAttributeValue("Code", "INF").first().attr("Quantity"));

		} catch (Exception e) {
			e.printStackTrace();

		}
		return request;

	}
	
	/*public static void main(String[] args) {
		
		xmlReaderAir2 read=new xmlReaderAir2();
		
		
	//	read.searchRequestReader(ridetracer)
		
	}*/
	
	
	
	

}
