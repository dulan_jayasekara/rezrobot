package com.rezrobot.flight_circuitry;

public class FlightMoreDetails {

	String		moreDetailsTableHeader_Label		= "";
	String		moreDetailsTableColumnHeaders_Label	= "";	
	String		outboundNonStop_Label				= "";
	String		inboundNonStop_Label				= "";
	String		outboundCabinClass_Label			= "";
	String		inboundCabinClass_Label				= "";
	Outbound	outbound							= null;
	Inbound		inbound								= null;
	
	
	
	public String getMoreDetailsTableHeader_Label() {
		return moreDetailsTableHeader_Label;
	}
	public void setMoreDetailsTableHeader_Label(String moreDetailsTableHeader_Label) {
		this.moreDetailsTableHeader_Label = moreDetailsTableHeader_Label;
	}
	public String getMoreDetailsTableColumnHeaders_Label() {
		return moreDetailsTableColumnHeaders_Label;
	}
	public void setMoreDetailsTableColumnHeaders_Label(
			String moreDetailsTableColumnHeaders_Label) {
		this.moreDetailsTableColumnHeaders_Label = moreDetailsTableColumnHeaders_Label;
	}
	public String getOutboundNonStop_Label() {
		return outboundNonStop_Label;
	}
	public void setOutboundNonStop_Label(String outboundNonStop_Label) {
		this.outboundNonStop_Label = outboundNonStop_Label;
	}
	public String getInboundNonStop_Label() {
		return inboundNonStop_Label;
	}
	public void setInboundNonStop_Label(String inboundNonStop_Label) {
		this.inboundNonStop_Label = inboundNonStop_Label;
	}
	public String getOutboundCabinClass_Label() {
		return outboundCabinClass_Label;
	}
	public void setOutboundCabinClass_Label(String outboundCabinClass_Label) {
		this.outboundCabinClass_Label = outboundCabinClass_Label;
	}
	public String getInboundCabinClass_Label() {
		return inboundCabinClass_Label;
	}
	public void setInboundCabinClass_Label(String inboundCabinClass_Label) {
		this.inboundCabinClass_Label = inboundCabinClass_Label;
	}
	public Outbound getOutbound() {
		return outbound;
	}
	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}
	public Inbound getInbound() {
		return inbound;
	}
	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

}
