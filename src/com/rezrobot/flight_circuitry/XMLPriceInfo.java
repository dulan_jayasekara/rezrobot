/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;



public class XMLPriceInfo  implements Serializable
{
	private 	String			TaxCode							= "";
	private		String			EquivFare						= "";
	private		String			EquivCurrencyCode				= "";
	private		int				EquivDecimal					= 0;
	private		String			PricingSource					= "Published";
	private		boolean			ischanged						= false;
				Costs			inSupplierCurrency				= new Costs();
				Costs			inPortalCurrency				= new Costs();
				Costs			inSellingCurrency				= new Costs();
	
	
	
	
	
	public Costs getInSupplierCurrency() {
		return inSupplierCurrency;
	}

	public void setInSupplierCurrency(Costs inSupplierCurrency) {
		this.inSupplierCurrency = inSupplierCurrency;
	}

	public Costs getInPortalCurrency() {
		return inPortalCurrency;
	}

	public void setInPortalCurrency(Costs inPortalCurrency) {
		this.inPortalCurrency = inPortalCurrency;
	}

	public Costs getInSellingCurrency() {
		return inSellingCurrency;
	}

	public void setInSellingCurrency(Costs inSellingCurrency) {
		this.inSellingCurrency = inSellingCurrency;
	}
	
	/*public void setDiscount(String discountType, String value, String currency, Map<String, String> CurrencyMap)
	{
		double val = Double.parseDouble(value);
		
		if(newBasefareCurrencyCode.equalsIgnoreCase(currency))
		{
			if(discountType.equalsIgnoreCase("Percentage"))
			{
				discount			  = (newbase + profit) * (val/100); 
				discount			  = Math.ceil(discount);
				sellCostDiscount	  = SellingCost - discount;
				SellingCost			  = Math.ceil(sellCostDiscount);
			}
			else if(discountType.equalsIgnoreCase("Value"))
			{
				//discount			  = (newbase + profit) - val;
				discount			  = Math.ceil(discount);
				discountInSellingCurr = Math.ceil( CommonValidator.convert(currency, newBasefareCurrencyCode, currency, val, CurrencyMap) );
				discountInPortalCurr  = val;
				sellCostDiscount	  = SellingCost - val;
				SellingCost			  = Math.ceil(sellCostDiscount);
			}
		}
		else
		{
			if(discountType.equalsIgnoreCase("Percentage"))
			{
				discount			  = (newbase + profit) * (val/100); 
				discount			  = Math.ceil(discount);
				sellCostDiscount	  = SellingCost - discount;
				SellingCost			  = Math.ceil(sellCostDiscount);
			}
			else if(discountType.equalsIgnoreCase("Value"))
			{
				discountInSellingCurr = CommonValidator.convert(currency, newBasefareCurrencyCode, currency, val, CurrencyMap);
				discountInSellingCurr = Math.ceil(discountInSellingCurr);
				discountInPortalCurr  = val;
				sellCostDiscount	  = SellingCost - discountInSellingCurr;
				SellingCost			  = sellCostDiscount;
			}
		}	
	}*/
	
	public boolean isIschanged() {
		return ischanged;
	}
	public void setIschanged(boolean ischanged) {
		this.ischanged = ischanged;
	}
	public String getEquivFare() {
		return EquivFare;
	}
	public void setEquivFare(String equivFare) {
		EquivFare = equivFare;
	}
	public String getEquivCurrencyCode() {
		return EquivCurrencyCode;
	}
	public void setEquivCurrencyCode(String equivCurrencyCode) {
		EquivCurrencyCode = equivCurrencyCode;
	}
	public int getEquivDecimal() {
		return EquivDecimal;
	}
	public void setEquivDecimal(String equivDecimal) {
		EquivDecimal = Integer.parseInt(equivDecimal);
	}

	public String getTaxCode() {
		return TaxCode;
	}

	public void setTaxCode(String taxCode) {
		TaxCode = taxCode;
	}

	public String getPricingSource() {
		return PricingSource;
	}

	public void setPricingSource(String pricingSource) {
		PricingSource = pricingSource;
	}

	public void setEquivDecimal(int equivDecimal) {
		EquivDecimal = equivDecimal;
	}

	public void getAll()
	{
		System.out.println();
		System.out.println("-----Price info is called----");
		System.out.println();
		System.out.println("Pricing Source : "+getPricingSource());
		//System.out.println("Base fare amount : "+getBasefareAmount());
		//System.out.println("Base fare Decimal places : "+getBasefareAmountDecimal());
		//System.out.println("Basa fare currency code : "+getBasefareCurrencyCode());
		System.out.println("Tax code : "+getTaxCode());
		//System.out.println("Tax amount : "+getTaxAmount());
		//System.out.println("Tax amount decimal : "+getTaxAmountDecimal());
		//System.out.println("Tax currency code : "+getTaxCurrencyCode());
		//System.out.println("Total Fare : "+getTotalFare());
		//System.out.println("Total fare decimal : "+getTotalFareDecimal());
		System.out.println();
	}
	
}
