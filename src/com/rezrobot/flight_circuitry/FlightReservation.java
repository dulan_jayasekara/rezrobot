package com.rezrobot.flight_circuitry;

import java.io.Serializable;

import com.rezrobot.flight_circuitry.xml_objects.FareRequest;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;
import com.rezrobot.flight_circuitry.xml_objects.PriceRequest;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;
import com.rezrobot.flight_circuitry.xml_objects.ReservationRequest;
import com.rezrobot.flight_circuitry.xml_objects.ReservationResponse;

public class FlightReservation implements Serializable {
	
	String				ReservationNo			= "";
	String				supplierConfirmationNo	= "";
	boolean				confirmed				= false;
	SearchObject		searchObject			= new SearchObject();
	AirConfig			airConfigObject			= new AirConfig();
	XMLPriceItinerary	XMLSelectFlight			= new XMLPriceItinerary();
	FareRequest			fareRequest				= new FareRequest();
	FareResponse		fareResponse			= new FareResponse();
	PriceRequest		priceRequest			= new PriceRequest();
	PriceResponse		priceResponse			= new PriceResponse();
	ReservationRequest	reservationRequest		= new ReservationRequest();
	ReservationResponse reservationResponse		= new ReservationResponse();
	ReservationPassengerInfo passengerNames		= new ReservationPassengerInfo();
	
	
	public ReservationPassengerInfo getPassengerNames() {
		return passengerNames;
	}
	public void setPassengerNames(ReservationPassengerInfo passengerNames) {
		this.passengerNames = passengerNames;
	}
	public String getReservationNo() {
		return ReservationNo;
	}
	public void setReservationNo(String reservationNo) {
		ReservationNo = reservationNo;
	}
	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}
	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}
	public boolean isConfirmed() {
		return confirmed;
	}
	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
	public SearchObject getSearchObject() {
		return searchObject;
	}
	public void setSearchObject(SearchObject searchObject) {
		this.searchObject = searchObject;
	}
	public AirConfig getAirConfigObject() {
		return airConfigObject;
	}
	public void setAirConfigObject(AirConfig airConfigObject) {
		this.airConfigObject = airConfigObject;
	}
	public XMLPriceItinerary getXMLSelectFlight() {
		return XMLSelectFlight;
	}
	public void setXMLSelectFlight(XMLPriceItinerary xMLSelectFlight) {
		XMLSelectFlight = xMLSelectFlight;
	}
	public FareRequest getFareRequest() {
		return fareRequest;
	}
	public void setFareRequest(FareRequest fareRequest) {
		this.fareRequest = fareRequest;
	}
	public FareResponse getFareResponse() {
		return fareResponse;
	}
	public void setFareResponse(FareResponse fareResponse) {
		this.fareResponse = fareResponse;
	}
	public PriceRequest getPriceRequest() {
		return priceRequest;
	}
	public void setPriceRequest(PriceRequest priceRequest) {
		this.priceRequest = priceRequest;
	}
	public PriceResponse getPriceResponse() {
		return priceResponse;
	}
	public void setPriceResponse(PriceResponse priceResponse) {
		this.priceResponse = priceResponse;
	}
	public ReservationRequest getReservationRequest() {
		return reservationRequest;
	}
	public void setReservationRequest(ReservationRequest reservationRequest) {
		this.reservationRequest = reservationRequest;
	}
	public ReservationResponse getReservationResponse() {
		return reservationResponse;
	}
	public void setReservationResponse(ReservationResponse reservationResponse) {
		this.reservationResponse = reservationResponse;
	}
	
	
	
	
}
