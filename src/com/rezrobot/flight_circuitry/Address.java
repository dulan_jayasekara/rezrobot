/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;

public class Address  implements Serializable{

	String addressType = "";
	
	String addressStreetNo	= "";
	String addressStreetNo2	= "";
	
	String addressCity		= "";
	String PostalCode		= "";
	String StateProv		= "";
	String addressCountry	= "";
	String countryZip		= "924";
	String addressUseType	= "";
	
	
	
	public String getCountryZip() {
		return countryZip;
	}
	public void setCountryZip(String countryZip) {
		this.countryZip = countryZip;
	}
	public String getAddressStreetNo2() {
		return addressStreetNo2;
	}
	public void setAddressStreetNo2(String addressStreetNo2) {
		this.addressStreetNo2 = addressStreetNo2;
	}
	public String getAddressUseType() {
		return addressUseType;
	}
	public void setAddressUseType(String addressUseType) {
		this.addressUseType = addressUseType;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddressStreetNo() {
		return addressStreetNo;
	}
	public void setAddressStreetNo(String addressStreetNo) {
		this.addressStreetNo = addressStreetNo;
	}
	public String getAddressCity() {
		return addressCity;
	}
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getStateProv() {
		return StateProv;
	}
	public void setStateProv(String stateProv) {
		StateProv = stateProv;
	}
	public String getAddressCountry() {
		return addressCountry;
	}
	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

}
