/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;
import java.util.ArrayList;

public class XMLPNRData  implements Serializable {
	
	ArrayList<Traveler> passengers	= null;
	String  telephoneLocation		= "";
	String  countryAccessCode		= "";
	String  areaCityCode			= "";
	String  phoneNumber				= "";
	String  email					= "";
	String  tickettimelimit			= "";
	String  tickettype				= "";
	Address address					= null;
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getTickettimelimit() {
		return tickettimelimit;
	}
	public void setTickettimelimit(String tickettimelimit) {
		this.tickettimelimit = tickettimelimit;
	}
	public String getTickettype() {
		return tickettype;
	}
	public void setTickettype(String tickettype) {
		this.tickettype = tickettype;
	}

	
	public ArrayList<Traveler> getPassengers() {
		return passengers;
	}
	public void setPassengers(ArrayList<Traveler> passengers) {
		this.passengers = passengers;
	}
	public String getTelephoneLocation() {
		return telephoneLocation;
	}
	public void setTelephoneLocation(String telephoneLocation) {
		this.telephoneLocation = telephoneLocation;
	}
	public String getCountryAccessCode() {
		return countryAccessCode;
	}
	public void setCountryAccessCode(String countryAccessCode) {
		this.countryAccessCode = countryAccessCode;
	}
	public String getAreaCityCode() {
		return areaCityCode;
	}
	public void setAreaCityCode(String areaCityCode) {
		this.areaCityCode = areaCityCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
