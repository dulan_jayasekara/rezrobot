package com.rezrobot.flight_circuitry;

import java.util.ArrayList;
//import java.util.Map;








import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.enumtypes.ConfigFareType;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.pojo.Label;
import com.rezrobot.pojo.RadioButton;
//import com.rezrobot.utill.ExcelReader;



public class CC_Com_AirRulesConfig extends PageObject 
{
	
	public void setConfiguration(AirConfig configObj )
	{
		String airline								= "";
		boolean found								= true;
		
		System.out.println("INFO ----> AIR CONFIGURATION SETTING");
		
		try
		{
			switchToDefaultFrame();
			((InputFiled)getElement("inputfield_airLineName_id")).clearInput();
			((InputFiled)getElement("inputfield_airLineName_id")).setText(configObj.getAirLine().trim());
			
			getElement("label_airLineName_lookupIcon").click();
			//driver.findElement(By.id("airLineName_lkup")).click();
			
			switchToFrame("frame_lookup_id");
			//switchTo().frame("lookup");
			
			System.out.println(((Label)getElement("label_rezgLook0_class")).getRef());
			airline = ((Label)getElement("label_rezgLook0_class")).getWebElement().getText();
			//airline = driver.findElement(By.className("rezgLook0")).getText().trim();
			
			if(configObj.getAirLine().trim().equalsIgnoreCase(airline.trim()))
			{
				found = true;
				//getElement("label_airLineName_lookupIcon").getWebElement().findElement(By.className("rezgLook0")).click();
				getElement("label_rezgLook0_class").getWebElement().click();
				//driver.findElement(By.id("lookupDataArea")).findElement(By.className("rezgLook0")).click();
			}
		
			//driver.switchTo().defaultContent();
			switchToDefaultFrame();
		
		} catch (Exception e) {
			//Screenshot.takeScreenshot(scenarioFailedPath + "/Before Air Configure.jpg", driver);
		}
		
		if(found)
		{
			boolean set = false;
			if(configObj.isApplyAirChargesConfigurations())
			{
				while(!set)
				{
					if( !configObj.getPubFareType().equals("") )
					{
						ArrayList<WebElement> publishedRadio = new ArrayList<WebElement>();//driver.findElements(By.id("publishedFareOption")));
						try {
							publishedRadio = new ArrayList<WebElement>(getElements("radiobtnList_publishedFareOption_id"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						if( configObj.getPubFareType().equals(ConfigFareType.Booking_Fee) )
						{
							if( !publishedRadio.get(0).isSelected() )
							{	
								publishedRadio.get(0).click();
								//Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare booking Fee.jpg", driver);
							}
						}
						
						else if( configObj.getPubFareType().equals(ConfigFareType.Profit_Markup) )
						{
							if( !publishedRadio.get(1).isSelected() )
							{
								publishedRadio.get(1).click();
								//Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare profit markup.jpg", driver);
							}
						}
						
						else if( configObj.getPubFareType().equals(ConfigFareType.Both) )
						{
							if( !publishedRadio.get(2).isSelected() )
							{
								publishedRadio.get(2).click();
								//Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare both.jpg", driver);
							}
						}
						
						else if( configObj.getPubFareType().equals(ConfigFareType.None) )
						{
							if( !publishedRadio.get(3).isSelected() )
							{
								publishedRadio.get(3).click();
								//Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare none.jpg", driver);
							}
						}
						
						if( !configObj.getPvtFareType().equals("") )
						{
							ArrayList<WebElement> privateRadio = new ArrayList<WebElement>();
							try {
								privateRadio = new ArrayList<WebElement>(getElements("radiobtnList_privateFareOption_id"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							if(configObj.getPvtFareType().equals(ConfigFareType.Booking_Fee))
							{
								if( !privateRadio.get(0).isSelected())
								{
									privateRadio.get(0).click();
									//Screenshot.takeScreenshot(scenarioCommonPath + "/Private fare booking Fee.jpg", driver);
								}
							}
							
							else if( configObj.getPvtFareType().equals(ConfigFareType.Profit_Markup) )
							{
								if( !privateRadio.get(1).isSelected() )
								{
									privateRadio.get(1).click();
								}
							}
							
							else if( configObj.getPvtFareType().equals(ConfigFareType.Both) )
							{
								if( !privateRadio.get(2).isSelected())
								{
									privateRadio.get(2).click();
									//Screenshot.takeScreenshot(scenarioCommonPath + "/Private fare both.jpg", driver);
								}
							}
						}
						
						//driver.findElement(By.id("addAirConfigurationMapping")).click();
						try {
							getElement("button_addAirConfiguration_id").click();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
						if(getElement("label_dialogMsgText_id").isElementVisible())//driver.findElement(By.id("dialogMsgText")).isDisplayed())
						{
							//Screenshot.takeScreenshot(scenarioCommonPath + "/Configuration already exist.jpg", driver);
							executejavascript("javascript:closeDialogMsg(dialogMsgBox);");
							//((JavascriptExecutor)driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
							
							set = false;
							//ArrayList<WebElement> table = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='air_configuration_mapping_data']/table/tbody")));
							try {
								ArrayList<WebElement> table = new ArrayList<WebElement>(getElements("tbody_tablebody_xpath"));
								ArrayList<WebElement> tabletr = new ArrayList<WebElement>(table.get(0).findElements(By.className("reportDataRows1")));
								
								for(int y=0; y<tabletr.size(); y++)
								{
									String text = tabletr.get(y).findElements(By.className("tablegridcell")).get(0).getText();
									
									if(configObj.getAirLine().trim().equalsIgnoreCase(text))
									{
										//((JavascriptExecutor)driver).executeScript("removeAirConfigurationMapping("+(y+1)+")");
										executejavascript("removeAirConfigurationMapping("+(y+1)+")");
										//Screenshot.takeScreenshot(scenarioCommonPath + "/Removed configuration already exist.jpg", driver);
										break;
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else
						{
							set = true;
						}
					}
				}
			}
			
			
			// FARE TYPE CONFIGURATIONS 
			if(configObj.isApplyFareTypeConfigurations()) {
				if(!configObj.getFTypeConfgShopCart().equals(""))
				{
					if(configObj.getFTypeConfgShopCart().equals("Published Fares Only"))
					{
						try {
							//((RadioButton)getElement("fareTypeShoppingCart_PRO_id")).click();
							((RadioButton)getElement("fareTypeShoppingCart_PRU_id")).click();
						} catch (Exception e) {
							e.printStackTrace();
							try {
								((RadioButton)getElement("fareTypeShoppingCart_PRO_id")).click();
							} catch (Exception e2) {
								
							}
						}
					}
					else if(configObj.getFTypeConfgShopCart().contains("Private Fares and if not available then Published Fares "))
					{
						try {
							((RadioButton)getElement("fareTypeShoppingCart_PRP_id")).click();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			if(configObj.isApplyPaymentOptionConfigurations()) {
				
				// PAYMENT OPTION SET
				if( !configObj.getFlightPayOptCartBooking().equals("") )
				{
					if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
					{
						try {
							((RadioButton)getElement("payementOptionShoppingCart_PF_id")).click();
						} catch (Exception e) {
							e.printStackTrace();
						}
						//driver.findElement(By.id("payementOptionShoppingCart_PF")).click();
						//Screenshot.takeScreenshot(scenarioCommonPath + "/Pay Full at booking.jpg", driver);
					}
					else if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
					{
						try {
							((RadioButton)getElement("payementOptionShoppingCart_PB_id")).click();
						} catch (Exception e) {
							e.printStackTrace();
						}
						//driver.findElement(By.id("payementOptionShoppingCart_PB")).click();
						//Screenshot.takeScreenshot(scenarioCommonPath + "/Pay booking fee at booking.jpg", driver);
					}
					else if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge"))
					{
						//ArrayList<WebElement> buttons = new ArrayList<WebElement>(getElements("key"));
						try {
							((RadioButton)getElement("payementOptionShoppingCart_AC_id")).click();
						} catch (Exception e) {
							e.printStackTrace();
						}
						//driver.findElement(By.id("payementOptionShoppingCart_AC")).click();
						//Screenshot.takeScreenshot(scenarioCommonPath + "/Pass to airline.jpg", driver);
					}
				}
			}
			
		}//End if(found)
		
		try {
			getElement("button_SaveButton_id").click();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//driver.findElement(By.id("saveButId")).click();
		
		if(getElementVisibility("dialogMsgBox_id", 4000)/*driver.findElement(By.id("dialogMsgBox")).isDisplayed()*/)
		{
			try {
				//getElement("dialogMsgActionButtons_id", 4000).click();
				executejavascript("javascript:closeDialogMsg(dialogMsgBox);");
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			//driver.findElement(By.id("dialogMsgActionButtons")).click();
			//Screenshot.takeScreenshot(scenarioCommonPath + "/Save Configuration(After message).jpg", driver);
		}
	}
	
	public AirConfig getConfiguration()
	{
		AirConfig airConfigObject = new AirConfig();
		
		try {
			
			WebElement ConfigurationSettingRow = getElement("div_ConfigurationSettingRow_xpath", 4000);
			ArrayList<WebElement> Cells = new ArrayList<WebElement>( ConfigurationSettingRow.findElements(By.className(getElement("div_tablegridcell_class").getRef())) );
			System.out.println("Air Configurations Row Cell size = "+Cells.size());
			
			String AirLine = "";
			String PubBookingFee = "";
			String PubProfitMarkup = "";
			String PvtBookingFee = "";
			String PvtProfitMarkup = "";
			
			if(Cells.size() == 6)
			{
				AirLine			= Cells.get(0).getText().trim();
				PubBookingFee	= Cells.get(1).getText().trim();
				PubProfitMarkup = Cells.get(2).getText().trim();
				PvtBookingFee	= Cells.get(3).getText().trim();
				PvtProfitMarkup = Cells.get(4).getText().trim();
				
				if(AirLine.equalsIgnoreCase("ALL"))
				{
					airConfigObject.setAirLine(AirLine);
				}
				
				if(PubBookingFee.equalsIgnoreCase("NO"))
				{
					if(PubProfitMarkup.equalsIgnoreCase("YES"))
					{
						airConfigObject.setPubFareType(ConfigFareType.Profit_Markup);
					}
					else if(PubProfitMarkup.equalsIgnoreCase("NO"))
					{
						airConfigObject.setPubFareType(ConfigFareType.None);
					}	
				}
				else if(PubBookingFee.equalsIgnoreCase("YES"))
				{
					if(PubProfitMarkup.equalsIgnoreCase("YES"))
					{
						airConfigObject.setPubFareType(ConfigFareType.Both);
					}
					else if(PubProfitMarkup.equalsIgnoreCase("NO"))
					{
						airConfigObject.setPubFareType(ConfigFareType.Booking_Fee);
					}
					
				}
				
				//PRIVATEFARES
				if(PvtBookingFee.equalsIgnoreCase("NO"))
				{
					if(PvtProfitMarkup.equalsIgnoreCase("YES"))
					{
						airConfigObject.setPvtFareType(ConfigFareType.Profit_Markup);
					}
					else if(PvtProfitMarkup.equalsIgnoreCase("NO"))
					{
						airConfigObject.setPvtFareType(ConfigFareType.None);
					}	
				}
				else if(PvtBookingFee.equalsIgnoreCase("YES"))
				{
					if(PvtProfitMarkup.equalsIgnoreCase("YES"))
					{
						airConfigObject.setPvtFareType(ConfigFareType.Both);
					}
					else if(PvtProfitMarkup.equalsIgnoreCase("NO"))
					{
						airConfigObject.setPvtFareType(ConfigFareType.Booking_Fee);
					}
				}
				
				if( ((RadioButton)getElement("fareTypeShoppingCart_PRO_id")).isElementSelected() )
				{
					airConfigObject.setFTypeConfgShopCart("Publish Fares Only");
				}
				else
				{
					airConfigObject.setFTypeConfgShopCart("Private Fares and if not available then Published Fares ");
				}
				
				
				if( ((RadioButton)getElement("payementOptionShoppingCart_PF_id")).isElementSelected() )
				{
					airConfigObject.setFlightPayOptCartBooking("Pay_Full_Amount_at_Booking");
				}
				else if( ((RadioButton)getElement("payementOptionShoppingCart_PB_id")).isElementSelected() )
				{
					airConfigObject.setFlightPayOptCartBooking("Pay_Booking_Fee_as_Deposit");
				}
				else if( ((RadioButton)getElement("payementOptionShoppingCart_AC_id")).isElementSelected() )
				{
					airConfigObject.setFlightPayOptCartBooking("Pass_to_Air_Line_to_Charge");
				}
				
				
				
				
			}
			
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return airConfigObject;
	}
	
	

}
