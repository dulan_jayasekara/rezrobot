package com.rezrobot.flight_circuitry;

import java.util.HashMap;


import com.rezrobot.utill.CurrencyConverter;


public class CalculateCosts {

	public static void calculateCosts(XMLPriceInfo selectedItineraryCosts, AirConfig airConfigObject, String profitType, double profit, String bookingFeeType, double bookingFee, String creditFeeType, double creditFeeVal, String flightPayGatewayCurrency, String portalCurrency, String sellingCurrency, HashMap<String, String> currencyMap)
	{
		calculateProfit(selectedItineraryCosts, portalCurrency, sellingCurrency, currencyMap, profitType, profit);
		calculateBookingFee(selectedItineraryCosts, portalCurrency, sellingCurrency, currencyMap, bookingFeeType, bookingFee);
		calculateCreditCardFee(selectedItineraryCosts, portalCurrency, sellingCurrency, currencyMap, creditFeeType, creditFeeVal);
		setCostsInPortalCurrency(selectedItineraryCosts, portalCurrency, currencyMap);
		setCostsInSupplierCurrency(selectedItineraryCosts, portalCurrency, currencyMap);
		setCostsInSellingCurrency(selectedItineraryCosts, portalCurrency, sellingCurrency, currencyMap);
	}
	
	public static void setCostsInSupplierCurrency(XMLPriceInfo selectedItineraryCosts, String portalCurrency, HashMap<String, String> currencyMap)
	{
		String supplierBaseAmount				= selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount();
		String supplierTaxAmount				= selectedItineraryCosts.getInSupplierCurrency().getTaxAmount();
		String supplierProfit					= selectedItineraryCosts.getInSupplierCurrency().getProfit();
		String supplierBookingFee				= selectedItineraryCosts.getInSupplierCurrency().getBookingFee();
		String supplierCreditCFee				= selectedItineraryCosts.getInSupplierCurrency().getCreditCardFee();
		String supplierSubtotal					= "";
		String supplierTotalTaxAndOtherCharges	= "";
		String supplierSellingRate				= "";
		String suppliertotalWithoutCCFee		= "";
		
		supplierSubtotal				= String.valueOf( Double.valueOf(supplierBaseAmount) + Double.valueOf(supplierProfit) );
		supplierTotalTaxAndOtherCharges	= String.valueOf( Double.valueOf(supplierTaxAmount) + Double.valueOf(supplierBookingFee) + Double.valueOf(supplierCreditCFee) );
		supplierSellingRate				= String.valueOf( Double.valueOf(supplierBaseAmount) + Double.valueOf(supplierTaxAmount) + Double.valueOf(supplierProfit) + Double.valueOf(supplierBookingFee) + Double.valueOf(supplierCreditCFee) );
		suppliertotalWithoutCCFee		= String.valueOf( Double.valueOf(supplierBaseAmount) + Double.valueOf(supplierTaxAmount) + Double.valueOf(supplierProfit) + Double.valueOf(supplierBookingFee) );
		
		selectedItineraryCosts.getInSupplierCurrency().setSubtotal(supplierSubtotal);
		System.out.println("Supplier Subtotal : "+supplierSubtotal);
		
		selectedItineraryCosts.getInSupplierCurrency().setTotalTaxAndOtherCharges(supplierTotalTaxAndOtherCharges);
		System.out.println("Supplier Tax and Other Charges : "+supplierTotalTaxAndOtherCharges);
		
		selectedItineraryCosts.getInSupplierCurrency().setSellingRate(supplierSellingRate);
		System.out.println("Supplier Selling Rate : "+supplierSellingRate);
		
		selectedItineraryCosts.getInSupplierCurrency().setTotalWithoutCCFee(suppliertotalWithoutCCFee);
		System.out.println("Supplier Rate Without CC Fee : "+suppliertotalWithoutCCFee);
		
	}
	
	public static void setCostsInPortalCurrency(XMLPriceInfo selectedItineraryCosts, String portalCurrency, HashMap<String, String> currencyMap)
	{
		String supplierBaseAmount		= selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount();
		String supplierBaseCurrency		= selectedItineraryCosts.getInSupplierCurrency().getBasefareCurrencyCode();
		String supplierTaxAmount		= selectedItineraryCosts.getInSupplierCurrency().getTaxAmount();
		String supplierTaxCurrency		= selectedItineraryCosts.getInSupplierCurrency().getTaxCurrencyCode();
		String portalBaseAmount			= "";
		String portalBaseCurrency		= "";
		String portalTaxAmount			= "";
		String portalTaxCurrency		= "";
		
		portalBaseAmount	= String.valueOf(CurrencyConverter.convert(supplierBaseCurrency, portalCurrency, portalCurrency, Double.parseDouble(supplierBaseAmount), currencyMap, true));
		portalBaseCurrency	= portalCurrency;
		portalTaxAmount		= String.valueOf(CurrencyConverter.convert(supplierTaxCurrency, portalCurrency, portalCurrency, Double.parseDouble(supplierTaxAmount), currencyMap, true));
		portalTaxCurrency	= portalCurrency;
		
		System.out.println("Portal Base Amount : "+portalBaseAmount);
		selectedItineraryCosts.getInPortalCurrency().setBasefareAmount(portalBaseAmount);
		System.out.println("Portal Base Currency : "+portalBaseCurrency);
		selectedItineraryCosts.getInPortalCurrency().setBasefareCurrencyCode(portalBaseCurrency);
		System.out.println("Portal Tax Amount : "+portalTaxAmount);
		selectedItineraryCosts.getInPortalCurrency().setTaxAmount(portalTaxAmount);
		System.out.println("Portal Tax Currency : "+portalTaxCurrency);
		selectedItineraryCosts.getInPortalCurrency().setTaxCurrencyCode(portalTaxCurrency);
		
		// CALCULATING SUBTOTAL, TAX&OTHERCHARGES, SELLRATE 
		String portalProfit						= selectedItineraryCosts.getInSupplierCurrency().getProfit();
		String portalBookingFee					= selectedItineraryCosts.getInSupplierCurrency().getBookingFee();
		String portalCreditCFee					= selectedItineraryCosts.getInSupplierCurrency().getCreditCardFee();
		String portalSubtotal					= "";
		String portalTotalTaxAndOtherCharges	= "";
		String portalSellingRate				= "";
		String portaltotalWithoutCCFee			= "";
		
		portalSubtotal					= String.valueOf( Double.valueOf(portalBaseAmount) + Double.valueOf(portalProfit) );
		portalTotalTaxAndOtherCharges	= String.valueOf( Double.valueOf(portalTaxAmount) + Double.valueOf(portalBookingFee) + Double.valueOf(portalCreditCFee) );
		portalSellingRate				= String.valueOf( Double.valueOf(portalBaseAmount) + Double.valueOf(portalTaxAmount) + Double.valueOf(portalProfit) + Double.valueOf(portalBookingFee) + Double.valueOf(portalCreditCFee) );
		portaltotalWithoutCCFee			= String.valueOf( Double.valueOf(portalBaseAmount) + Double.valueOf(portalTaxAmount) + Double.valueOf(portalProfit) + Double.valueOf(portalBookingFee) );
		
		selectedItineraryCosts.getInPortalCurrency().setSubtotal(portalSubtotal);
		System.out.println("Portal Subtotal : "+portalSubtotal);
		selectedItineraryCosts.getInPortalCurrency().setTotalTaxAndOtherCharges(portalTotalTaxAndOtherCharges);
		System.out.println("Portal Tax and Other Charges : "+portalTotalTaxAndOtherCharges);
		selectedItineraryCosts.getInPortalCurrency().setSellingRate(portalSellingRate);
		System.out.println("Portal Selling Rate : "+portalSellingRate);
		selectedItineraryCosts.getInPortalCurrency().setTotalWithoutCCFee(portaltotalWithoutCCFee);
		System.out.println("Portal Rate Without CC Fee : "+portaltotalWithoutCCFee);
	}
	
	public static void setCostsInSellingCurrency(XMLPriceInfo selectedItineraryCosts, String portalCurrency, String sellingCurrency, HashMap<String, String> currencyMap)
	{
		String supplierBaseAmount		= selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount();
		String supplierBaseCurrency		= selectedItineraryCosts.getInSupplierCurrency().getBasefareCurrencyCode();
		String supplierTaxAmount		= selectedItineraryCosts.getInSupplierCurrency().getTaxAmount();
		String supplierTaxCurrency		= selectedItineraryCosts.getInSupplierCurrency().getTaxCurrencyCode();
		String sellingBaseAmount		= "";
		String sellingBaseCurrency		= "";
		String sellingTaxAmount			= "";
		String sellingTaxCurrency		= "";
		
		
		sellingBaseAmount	= String.valueOf(CurrencyConverter.convert(supplierBaseCurrency, portalCurrency, portalCurrency, Double.parseDouble(supplierBaseAmount), currencyMap, true));
		sellingBaseCurrency	= portalCurrency;
		sellingTaxAmount	= String.valueOf(CurrencyConverter.convert(supplierTaxCurrency, portalCurrency, portalCurrency, Double.parseDouble(supplierTaxAmount), currencyMap, true));
		sellingTaxCurrency	= portalCurrency;
		
		selectedItineraryCosts.getInSellingCurrency().setBasefareAmount(sellingBaseAmount);
		System.out.println("Selling Base Amount : "+sellingBaseAmount);
		selectedItineraryCosts.getInSellingCurrency().setBasefareCurrencyCode(sellingBaseCurrency);
		System.out.println("Selling Base Currency : "+sellingBaseCurrency);
		selectedItineraryCosts.getInSellingCurrency().setTaxAmount(sellingTaxAmount);
		System.out.println("Selling Base Tax : "+sellingTaxAmount);
		selectedItineraryCosts.getInSellingCurrency().setTaxCurrencyCode(sellingTaxCurrency);
		System.out.println("Selling Base Currency : "+sellingTaxCurrency);
		
		// CALCULATING SUBTOTAL, TAX&OTHERCHARGES, SELLRATE 
		String sellingProfit					= selectedItineraryCosts.getInSellingCurrency().getProfit();
		String sellingBookingFee				= selectedItineraryCosts.getInSellingCurrency().getBookingFee();
		String sellingCreditCFee				= selectedItineraryCosts.getInSellingCurrency().getCreditCardFee();
		String sellingSubtotal					= "";
		String sellingTotalTaxAndOtherCharges	= "";
		String sellingSellingRate				= "";
		String sellingtotalWithoutCCFee			= "";
		
		sellingSubtotal					= String.valueOf( Double.valueOf(sellingBaseAmount) + Double.valueOf(sellingProfit) );
		sellingTotalTaxAndOtherCharges	= String.valueOf( Double.valueOf(sellingTaxAmount) + Double.valueOf(sellingBookingFee) + Double.valueOf(sellingCreditCFee) );
		sellingSellingRate				= String.valueOf( Double.valueOf(sellingBaseAmount) + Double.valueOf(sellingTaxAmount) + Double.valueOf(sellingProfit) + Double.valueOf(sellingBookingFee) + Double.valueOf(sellingCreditCFee) );
		sellingtotalWithoutCCFee		= String.valueOf( Double.valueOf(sellingBaseAmount) + Double.valueOf(sellingTaxAmount) + Double.valueOf(sellingProfit) + Double.valueOf(sellingBookingFee) );
		
		selectedItineraryCosts.getInSellingCurrency().setSubtotal(sellingSubtotal);
		System.out.println("Selling Currency Subtotal : "+sellingSubtotal);
		selectedItineraryCosts.getInSellingCurrency().setTotalTaxAndOtherCharges(sellingTotalTaxAndOtherCharges);
		System.out.println("Selling Currency Charges : "+sellingTotalTaxAndOtherCharges);
		selectedItineraryCosts.getInSellingCurrency().setSellingRate(sellingSellingRate);
		System.out.println("Selling Currency Rate : "+sellingSellingRate);
		selectedItineraryCosts.getInSellingCurrency().setTotalWithoutCCFee(sellingtotalWithoutCCFee);
		System.out.println("Portal Rate Without CC Fee : "+sellingtotalWithoutCCFee);
	}
	
	public static void calculateProfit(XMLPriceInfo selectedItineraryCosts, String PortalCurrency, String SellingCurrency, HashMap<String, String> currencyMap, String ProfitType, double ProfitVal)
	{
		System.out.println("INFO ----> CALCULATING PROFIT");
		
		System.out.println(" > PROFIT TYPE       : "+ProfitType);
		System.out.println(" > PROFIT VALUE      : "+ProfitVal);
		System.out.println(" > FLIGHT BASEFARE   : "+selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount());
		
		double supplierProfit	= 0.0;
		double sellingProfit	= 0.0;
		double portalProfit		= 0.0;
		
		String supplierCurrency = selectedItineraryCosts.getInSupplierCurrency().getBasefareCurrencyCode();
		System.out.println(" > Supplier Currency : "+supplierCurrency);
		String portalCurrency	= PortalCurrency;
		System.out.println(" > Portal Currency   : "+portalCurrency);
		String sellingCurrency	= SellingCurrency;
		System.out.println(" > Selling Currency  : "+sellingCurrency);
		
		selectedItineraryCosts.getInPortalCurrency().setCurrencyCode(portalCurrency);
		selectedItineraryCosts.getInSellingCurrency().setCurrencyCode(sellingCurrency);
		
		if(ProfitType.toUpperCase().equalsIgnoreCase("VALUE"))
		{
			portalProfit	= CurrencyConverter.convert(PortalCurrency, PortalCurrency, PortalCurrency, ProfitVal, currencyMap, true);
			supplierProfit	= CurrencyConverter.convert(PortalCurrency, supplierCurrency, PortalCurrency, portalProfit, currencyMap, true);
			sellingProfit	= CurrencyConverter.convert(PortalCurrency, sellingCurrency, PortalCurrency, portalProfit, currencyMap, true);
		}
		else
		{
			supplierProfit	= Double.valueOf(selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount()) * (ProfitVal/100);
			
			supplierProfit	= CurrencyConverter.convert(supplierCurrency, supplierCurrency, PortalCurrency, supplierProfit, currencyMap, true);
			portalProfit	= CurrencyConverter.convert(supplierCurrency, PortalCurrency, PortalCurrency, supplierProfit, currencyMap, true);
			sellingProfit	= CurrencyConverter.convert(supplierCurrency, sellingCurrency, PortalCurrency, supplierProfit, currencyMap, true);
		}
		
		selectedItineraryCosts.getInSupplierCurrency().setProfit(String.valueOf(supplierProfit));
		System.out.println(" > Profit in Supplier Currency : "+supplierProfit);
		selectedItineraryCosts.getInPortalCurrency().setProfit(String.valueOf(portalProfit));
		System.out.println(" > Profit in Portal Currency   : "+portalProfit);
		selectedItineraryCosts.getInSellingCurrency().setProfit(String.valueOf(sellingProfit));
		System.out.println(" > Profit in Selling Currency  : "+sellingProfit);
		
		System.out.println("================================================");
	}
	
	public static void calculateBookingFee(XMLPriceInfo selectedItineraryCosts, String PortalCurrency, String SellingCurrency, HashMap<String, String> currencyMap,  String BookingFeeType, double BookingFeeVal)
	{
		System.out.println("INFO ----> CALCULATING BOOKING FEE");
		
		System.out.println(" > BOOKING FEE TYPE  : "+BookingFeeType);
		System.out.println(" > BOOKING FEE VALUE : "+BookingFeeVal);
		
		double supplierBookingFee	= 0.0;
		double sellingBookingFee	= 0.0;
		double portalBookingFee		= 0.0;
		
		String supplierCurrency = selectedItineraryCosts.getInSupplierCurrency().getBasefareCurrencyCode();
		System.out.println(" > Supplier Currency : "+supplierCurrency);
		String portalCurrency	= PortalCurrency;
		System.out.println(" > Portal Currency   : "+portalCurrency);
		String sellingCurrency	= SellingCurrency;
		System.out.println(" > Selling Currency  : "+sellingCurrency);
		
		System.out.println(" > BASEFARE : "+selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount()+" "+supplierCurrency);
		System.out.println(" > PROFIT   : "+selectedItineraryCosts.getInSupplierCurrency().getProfit()+" "+supplierCurrency);
		System.out.println(" > SUBTOTAL : BASEFARE + PROFIT "+supplierCurrency);
		
		selectedItineraryCosts.getInPortalCurrency().setCurrencyCode(portalCurrency);
		selectedItineraryCosts.getInSellingCurrency().setCurrencyCode(sellingCurrency);
		
		if(BookingFeeType.toUpperCase().equalsIgnoreCase("VALUE"))
		{
			portalBookingFee	= CurrencyConverter.convert(PortalCurrency, PortalCurrency, PortalCurrency, BookingFeeVal, currencyMap, true);
			supplierBookingFee	= CurrencyConverter.convert(PortalCurrency, supplierCurrency, PortalCurrency, portalBookingFee, currencyMap, true);
			sellingBookingFee	= CurrencyConverter.convert(PortalCurrency, sellingCurrency, PortalCurrency, portalBookingFee, currencyMap, true);
		}
		else
		{
			supplierBookingFee	= ( Double.valueOf(selectedItineraryCosts.getInSupplierCurrency().getBasefareAmount()) +  Double.valueOf(selectedItineraryCosts.getInSupplierCurrency().getProfit()) ) * (BookingFeeVal/100);
			
			supplierBookingFee	= CurrencyConverter.convert(supplierCurrency, supplierCurrency, PortalCurrency, supplierBookingFee, currencyMap, true);
			portalBookingFee	= CurrencyConverter.convert(supplierCurrency, PortalCurrency, PortalCurrency, supplierBookingFee, currencyMap, true);
			sellingBookingFee	= CurrencyConverter.convert(supplierCurrency, sellingCurrency, PortalCurrency, supplierBookingFee, currencyMap, true);
		}
		
		selectedItineraryCosts.getInSupplierCurrency().setBookingFee(String.valueOf(supplierBookingFee));
		System.out.println(" > Booking Fee in Supplier Currency : "+supplierBookingFee);
		selectedItineraryCosts.getInPortalCurrency().setBookingFee(String.valueOf(portalBookingFee));
		System.out.println(" > Booking Fee in Portal Currency : "+portalBookingFee);
		selectedItineraryCosts.getInSellingCurrency().setBookingFee(String.valueOf(sellingBookingFee));
		System.out.println(" > Booking Fee in Selling Currency : "+sellingBookingFee);
		
		System.out.println("================================================");
	}
	
	public static void calculateCreditCardFee(XMLPriceInfo selectedItineraryCosts, String PortalCurrency, String SellingCurrency, HashMap<String, String> currencyMap,  String creditFeeType, double creditFeeVal)
	{
		System.out.println("INFO ----> CALCULATING CREDITCARD FEE");
		
		System.out.println(" > CREDIT CARD FEE TYPE  : "+creditFeeType);
		System.out.println(" > CREDIT CARD FEE VALUE : "+creditFeeVal);
		
		double supplierCreditCardFee	= 0.0;
		double sellingCreditCardFee		= 0.0;
		double portalCreditCardFee		= 0.0;
		
		String supplierCurrency = selectedItineraryCosts.getInSupplierCurrency().getBasefareCurrencyCode();
		String portalCurrency	= PortalCurrency;
		String sellingCurrency	= SellingCurrency;
		
		System.out.println(" > Supplier Currency : "+supplierCurrency);
		System.out.println(" > Portal Currency   : "+portalCurrency);
		System.out.println(" > Selling Currency  : "+sellingCurrency);
		
		selectedItineraryCosts.getInPortalCurrency().setCurrencyCode(portalCurrency);
		selectedItineraryCosts.getInSellingCurrency().setCurrencyCode(sellingCurrency);
		
		if(creditFeeType.toUpperCase().equalsIgnoreCase("VALUE"))
		{
			portalCreditCardFee		= CurrencyConverter.convert(PortalCurrency, PortalCurrency, PortalCurrency, creditFeeVal, currencyMap, true);
			supplierCreditCardFee	= CurrencyConverter.convert(PortalCurrency, supplierCurrency, PortalCurrency, portalCreditCardFee, currencyMap, true);
			sellingCreditCardFee	= CurrencyConverter.convert(PortalCurrency, sellingCurrency, PortalCurrency, portalCreditCardFee, currencyMap, true);
		}
		else
		{
			sellingCreditCardFee	= ( Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getBasefareAmount()) + Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getProfit()) + Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getTaxAmount()) + Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getBookingFee()) ) * (creditFeeVal/100);
			
			sellingCreditCardFee	= CurrencyConverter.convert(sellingCurrency, sellingCurrency, PortalCurrency, sellingCreditCardFee, currencyMap, true);
			supplierCreditCardFee	= CurrencyConverter.convert(sellingCurrency, supplierCurrency, PortalCurrency, sellingCreditCardFee, currencyMap, true);
			portalCreditCardFee		= CurrencyConverter.convert(supplierCurrency, PortalCurrency, PortalCurrency, sellingCreditCardFee, currencyMap, true);
		}
		
		System.out.println(" > BASEFARE + PROFIT + TAX + BOOKING FEE + : "+( Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getBasefareAmount()) + Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getProfit()) + Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getTaxAmount()) + Double.valueOf(selectedItineraryCosts.getInSellingCurrency().getBookingFee()) ) +" "+supplierCurrency);
		
		selectedItineraryCosts.getInSupplierCurrency().setCreditCardFee(String.valueOf(supplierCreditCardFee));
		System.out.println(" > CC Fee in Supplier Currency : "+supplierCreditCardFee);
		selectedItineraryCosts.getInPortalCurrency().setCreditCardFee(String.valueOf(portalCreditCardFee));
		System.out.println(" > CC Fee in Portal Currency : "+portalCreditCardFee);
		selectedItineraryCosts.getInSellingCurrency().setCreditCardFee(String.valueOf(sellingCreditCardFee));
		System.out.println(" > CC Fee in Selling Currency : "+sellingCreditCardFee);
		
		System.out.println("================================================");
	}
	
	
}
