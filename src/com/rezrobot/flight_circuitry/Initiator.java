package com.rezrobot.flight_circuitry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;







import com.rezrobot.dataobjects.DMCSearchObject;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.enumtypes.*;
import com.rezrobot.pages.CC.common.CC_Com_AAA;
import com.rezrobot.pages.CC.common.CC_Com_AirOfficeID_Setup;
import com.rezrobot.pages.CC.common.CC_Com_Nego_Code_Setup;
import com.rezrobot.utill.ExcelReader;
import com.rezrobot.utill.ExcelWriter;

public class Initiator {
	
	public static  String file_Path = "../Rezrobot_Details/Flight/Excel/searchDetails.xls";
	
	public static ArrayList<SearchObject> getSearchObjList(String path) {
		System.out.println("Flight Search object list path --->" + path);
		ArrayList<Map<Integer, String>> XLtestData = new ArrayList<Map<Integer, String>>();
		
		//file_Path = path;
		
		ExcelReader Reader = new ExcelReader();
		XLtestData = Reader.init(file_Path);
		
		ArrayList<SearchObject> SearchList = new ArrayList<SearchObject>();

		for (int y = 0; y < 1; y++) {
			
			Map<Integer, String> Sheet = XLtestData.get(y);

			for (int x = 0; x < Sheet.size(); x++) {
				
				String[] testData = Sheet.get(x).split(",");
				SearchObject SearchObj = new SearchObject();
				
				SearchObj.setScenario(testData[0].trim());
				SearchObj.setExcecuteStatus(Boolean.parseBoolean(testData[1].trim()));
				SearchObj.setbookingChannel(testData[2].trim());
				SearchObj.setTOBooking(testData[3].trim());
				/*if (testData[4].trim().equalsIgnoreCase("NetCash")) {
				SearchObj.setToType(TO.NetCash);
				} else if (testData[4].trim().equalsIgnoreCase("NetCreditLpoY")) {
					SearchObj.setToType(TO.NetCreditLpoY);
				} else if (testData[4].trim().equalsIgnoreCase("NetCreditLpoN")) {
					SearchObj.setToType(TO.NetCreditLpoN);
				} else if (testData[4].trim().equalsIgnoreCase("CommCash")) {
					SearchObj.setToType(TO.CommCash);
				} else if (testData[4].trim().equalsIgnoreCase("CommCreditLpoY")) {
					SearchObj.setToType(TO.CommCreditLpoY);
				} else if (testData[4].trim().equalsIgnoreCase("CommCreditLpoN")) {
					SearchObj.setToType(TO.CommCreditLpoN);
				}*/
				
				SearchObj.setQuotation(Boolean.parseBoolean(testData[4].trim()));
				SearchObj.setAirConfigurationStatus(Boolean.parseBoolean(testData[5].trim()));
				if(testData[6].trim().equals("Online"))
				{
					SearchObj.setPaymentMode(PaymentMode.Online);
				}
				else if(testData[6].trim().equals("Offline"))
				{
					SearchObj.setPaymentMode(PaymentMode.Offline);
				}
				else if(testData[6].trim().equals("Flocash"))
				{
					SearchObj.setPaymentMode(PaymentMode.Flocash);
				}
				
				SearchObj.setDepartureDate(testData[7].trim());
				SearchObj.setDepartureTime(testData[8].trim());
				SearchObj.setReturnDate(testData[9].trim());
				SearchObj.setReturnTime(testData[10].trim());
				SearchObj.setProfitType(testData[11].toLowerCase().trim());
				SearchObj.setProfit(Double.parseDouble(testData[12].trim()));
				SearchObj.setBookingFeeType(testData[13].trim());
				SearchObj.setBookingFee(Double.parseDouble(testData[14].trim()));
				SearchObj.setCountry(testData[15].trim());
				SearchObj.setSellingCurrency(testData[16].trim());
				SearchObj.setTriptype(testData[17].trim());
				SearchObj.setFlexible(Boolean.parseBoolean(testData[18].toLowerCase().trim()));
				SearchObj.setFrom(testData[19].trim());
				SearchObj.setTo(testData[20].trim());
				SearchObj.setAdult(testData[21].trim());
				SearchObj.setChildren(testData[22].trim());
				ArrayList<String> childrenage = new ArrayList<String>();
				String[] age = testData[23].trim().split("/");

				for (int u = 0; u < age.length; u++) {
					childrenage.add(age[u].trim());
				}
				SearchObj.setChildrenAge(childrenage);
				SearchObj.setInfant(testData[24].trim());
				SearchObj.setCabinClass(testData[25].trim());
				SearchObj.setPreferredCurrency(testData[26].trim());
				SearchObj.setPreferredAirline(testData[27].trim());
				SearchObj.setNonStop(Boolean.parseBoolean(testData[28].toLowerCase().trim()));
				SearchObj.setApplyDiscount(Boolean.parseBoolean(testData[29].trim()));
				SearchObj.setSearchAgain(Boolean.parseBoolean(testData[30].trim()));
				SearchObj.setDiscountAtSearchAgain(Boolean.parseBoolean(testData[31].trim()));
				SearchObj.setSelectingFlight(testData[32].trim());
				SearchObj.setSelectingFlightType(testData[33].trim());
				SearchObj.setRemovecartStatus(Boolean.parseBoolean(testData[34].trim()));
				SearchObj.setValidateFilters(Boolean.parseBoolean(testData[35].trim()));
				SearchObj.setApplyDiscountAtPayPg(Boolean.parseBoolean(testData[36].trim()));
				SearchObj.setCancellationStatus(testData[37].trim());
				SearchObj.setSupplierPayablePayStatus(Boolean.parseBoolean(testData[38].trim()));
				
				SearchList.add(SearchObj);
			}
		}

		return SearchList;
	}
	
	public static ArrayList<AirConfig> getAirConfigObjList(String path) {
		System.out.println("Air Config Path --->" + path);
		ArrayList<AirConfig> configObjList = new ArrayList<AirConfig>();
		ExcelReader xl = new ExcelReader();
		ArrayList<Map<Integer,String>> returnlist = xl.init(path);
		
		for(int i=0; i<returnlist.size(); i++)
		{
			Map<Integer, String> sheet = returnlist.get(i);
			
			for(int j=0; j<sheet.size(); j++)
			{
				String[] data = sheet.get(j).split(",");
				
				AirConfig obj = new AirConfig();
				
				obj.setTestNo(Integer.parseInt(data[0]));
				obj.setApplyFareTypeConfigurations(Boolean.parseBoolean(data[1]));
				obj.setApplyPaymentOptionConfigurations(Boolean.parseBoolean(data[2]));
				obj.setApplyAirChargesConfigurations(Boolean.parseBoolean(data[3]));
				
				obj.setAirLine(data[4]);
				
				if(data[5].equals(ConfigFareType.Booking_Fee.toString()))
				{
					obj.setPubFareType(ConfigFareType.Booking_Fee);
				}
				else if(data[5].equals(ConfigFareType.Profit_Markup.toString()))
				{
					obj.setPubFareType(ConfigFareType.Profit_Markup);
				}
				else if(data[5].equals(ConfigFareType.Both.toString()))
				{
					obj.setPubFareType(ConfigFareType.Both);
				}
				else if(data[5].equals(ConfigFareType.None.toString()))
				{
					obj.setPubFareType(ConfigFareType.None);
				}
				
				if(data[6].equals(ConfigFareType.Booking_Fee.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Booking_Fee);
				}
				else if(data[6].equals(ConfigFareType.Profit_Markup.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Profit_Markup);
				}
				else if(data[6].equals(ConfigFareType.Both.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Both);
				}

				obj.setFTypeConfgShopCart(data[7]);
				obj.setFTypeConfgFplusH(data[8]);
				obj.setFTypeConfgFixPack(data[9]);
				obj.setFlightPayOptCartBooking(data[10]);
				obj.setFlightPayOptPayfullFplusH(data[11]);
			
				configObjList.add(obj);
			}
		}
		
		return configObjList;
	}
	
	public static ReservationPassengerInfo getPassengerDetails(Vacation_Search_object sobj)
	{
		System.out.println("SET FILLING PASSENGER DETAILS");
		
		String[] name = {"one","two","three","four","five","six","seven","eight","nine"};
		ReservationPassengerInfo resvFillobj = new ReservationPassengerInfo();
		int Noofadults = 0;
		int Noofchildren = 0;
		int Noofinfants = 0;
		for(int i = 0 ; i < Integer.parseInt(sobj.getRooms()) ; i++)
		{
			Noofadults 		= Noofadults 	+ Integer.parseInt(sobj.getAdults().split("/")[i]);
			Noofchildren 	= Noofchildren 	+ Integer.parseInt(sobj.getChildren().split("/")[i]);
			Noofinfants 	= Noofinfants 	+ Integer.parseInt(sobj.getInfants().split("/")[i]);
		}
		
		ArrayList<Traveler> adultlist = new ArrayList<Traveler>();
		ArrayList<Traveler> childrenlist = new ArrayList<Traveler>();
		ArrayList<Traveler> infantlist = new ArrayList<Traveler>(); 
		System.out.println("INFO -> Main traveller");
		String adultfname = "adf";
		String adultsurname = "ads";
		String childfname = "chf";
		String childsurname = "chs";
		String infantfname = "inff";
		String infantsurname = "infs";
		
		Traveler maincustomer = new Traveler();
		maincustomer.setGivenName("Adam");
		maincustomer.setSurname("Levine");
		
		Address add = new Address();
		add.setAddressStreetNo("California");
		add.setAddressCity("California");
		add.setAddressCountry("Christmas Island");
		add.setStateProv("California");
		add.setCountryZip("");
		maincustomer.setAddress(add);
		maincustomer.setPhoneNumber("25122015");
		maincustomer.setEmail("sanoj@colombo.rezgateway.com");
		
		resvFillobj.setMaincustomer(maincustomer);
		System.out.println("INFO -> Main traveller end");
		
		for(int a = 0; a<Noofadults; a++)
		{
			System.out.println("INFO -> adult : "+a+1);
			adultfname = "DanM";
			adultsurname = "WinM";
			adultfname = adultfname.concat(name[a]);
			adultsurname = adultsurname.concat(name[a]);
			Traveler adult = new Traveler();
			adult.setPassengertypeCode("ADT");
			adult.setNamePrefixTitle("Mr");
			adult.setGivenName(adultfname);
			adult.setSurname(adultsurname);
			adult.setPhoneNumber("2444555");
			adult.setPassportNo("111111111111");
			adult.setPassportExpDate("01-01-2020");
			adult.setPassprtIssuDate("01-01-2010");
			adult.setBirthDay("01-01-1995");
			adult.setGender("Male");
			adult.setNationality("Christmas Island");
			adult.setPassportIssuCountry("Christmas Island");
			adult.setPhoneType("HOME");
			adultlist.add(adult);
		}
		resvFillobj.setAdult(adultlist);
		
		for(int c = 0; c<Noofchildren; c++)
		{
			System.out.println("INFO -> child : "+c+1);
			childfname = "DanS";
			childsurname = "WinS";
			childfname = childfname.concat(name[c]);
			childsurname = childsurname.concat(name[c]);
			Traveler child = new Traveler();
			child.setPassengertypeCode("CHD");
			child.setNamePrefixTitle("Mst");
			child.setGivenName(childfname);
			child.setSurname(childsurname);
			child.setPhoneNumber("2444555");
			child.setPassportNo("1111111111111");
			child.setPassportExpDate("01-01-2020");
			child.setPassprtIssuDate("01-01-2010");
			child.setBirthDay("01-01-2008");
			child.setGender("Male");
			child.setNationality("Christmas Island");
			child.setPassportIssuCountry("Christmas Island");
			child.setPhoneType("HOME");
			childrenlist.add(child);
		}
		resvFillobj.setChildren(childrenlist);
		
		for(int c = 0; c<Noofinfants; c++)
		{
			System.out.println("INFO -> infant : "+c+1);
			infantfname = "DanI";
			infantsurname = "WinI";
			infantfname = infantfname.concat(name[c]);
			infantsurname = infantsurname.concat(name[c]);
			Traveler infant = new Traveler();
			infant.setPassengertypeCode("INF");
			infant.setNamePrefixTitle("Mst");
			infant.setGivenName(infantfname);
			infant.setSurname(infantsurname);
			infant.setBirthDay("01-01-2014");
			infant.setGender("Male");
			infant.setNationality("Christmas Island");
			infant.setPassportNo("1111111111111");
			infantlist.add(infant);
		}
		resvFillobj.setInfant(infantlist);
		
		System.out.println("SET FILLING PASSENGER DETAILS END");
		return resvFillobj;
	}
	
	public static ReservationPassengerInfo getDMCPassengerDetails(DMCSearchObject sobj) {
		System.out.println("SET FILLING PASSENGER DETAILS");

		String[] name = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
		ReservationPassengerInfo resvFillobj = new ReservationPassengerInfo();
		int Noofadults = 0;
		int Noofchildren = 0;
		int Noofinfants = 0;

		for (int i = 0; i < Integer.parseInt(sobj.getNoofRooms()); i++) {

			Noofadults = Noofadults + Integer.parseInt(sobj.getAdult().split("/")[i]);
			Noofchildren = Noofchildren + Integer.parseInt(sobj.getChild().split("/")[i]);
			Noofinfants = Noofinfants + Integer.parseInt(sobj.getInfant().split("/")[i]);

		}

		ArrayList<Traveler> adultlist = new ArrayList<Traveler>();
		ArrayList<Traveler> childrenlist = new ArrayList<Traveler>();
		ArrayList<Traveler> infantlist = new ArrayList<Traveler>();
		System.out.println("INFO -> Main traveller");
		String adultfname = "adf";
		String adultsurname = "ads";
		String childfname = "chf";
		String childsurname = "chs";
		String infantfname = "inff";
		String infantsurname = "infs";

		Traveler maincustomer = new Traveler();
		maincustomer.setGivenName("Adam");
		maincustomer.setSurname("Levine");

		Address add = new Address();
		add.setAddressStreetNo("California");
		add.setAddressCity("California");
		add.setAddressCountry("Christmas Island");
		add.setStateProv("California");
		add.setCountryZip("");
		maincustomer.setAddress(add);
		maincustomer.setPhoneNumber("25122015");
		maincustomer.setEmail("sanoj@colombo.rezgateway.com");

		resvFillobj.setMaincustomer(maincustomer);
		System.out.println("INFO -> Main traveller end");

		for (int a = 0; a < Noofadults; a++) {
			System.out.println("INFO -> adult : " + a + 1);
			adultfname = "DanM";
			adultsurname = "WinM";
			adultfname = adultfname.concat(name[a]);
			adultsurname = adultsurname.concat(name[a]);
			Traveler adult = new Traveler();
			adult.setPassengertypeCode("ADT");
			adult.setNamePrefixTitle("Mr");
			adult.setGivenName(adultfname);
			adult.setSurname(adultsurname);
			adult.setPhoneNumber("2444555");
			adult.setPassportNo("111111111111");
			adult.setPassportExpDate("01-01-2020");
			adult.setPassprtIssuDate("01-01-2010");
			adult.setBirthDay("01-01-1995");
			adult.setGender("Male");
			adult.setNationality("Christmas Island");
			adult.setPassportIssuCountry("Christmas Island");
			adult.setPhoneType("HOME");
			adultlist.add(adult);
		}
		resvFillobj.setAdult(adultlist);

		for (int c = 0; c < Noofchildren; c++) {
			System.out.println("INFO -> child : " + c + 1);
			childfname = "DanS";
			childsurname = "WinS";
			childfname = childfname.concat(name[c]);
			childsurname = childsurname.concat(name[c]);
			Traveler child = new Traveler();
			child.setPassengertypeCode("CHD");
			child.setNamePrefixTitle("Mst");
			child.setGivenName(childfname);
			child.setSurname(childsurname);
			child.setPhoneNumber("2444555");
			child.setPassportNo("1111111111111");
			child.setPassportExpDate("01-01-2020");
			child.setPassprtIssuDate("01-01-2010");
			child.setBirthDay("01-01-2008");
			child.setGender("Male");
			child.setNationality("Christmas Island");
			child.setPassportIssuCountry("Christmas Island");
			child.setPhoneType("HOME");
			childrenlist.add(child);
		}
		resvFillobj.setChildren(childrenlist);

		for (int c = 0; c < Noofinfants; c++) {
			System.out.println("INFO -> infant : " + c + 1);
			infantfname = "DanI";
			infantsurname = "WinI";
			infantfname = infantfname.concat(name[c]);
			infantsurname = infantsurname.concat(name[c]);
			Traveler infant = new Traveler();
			infant.setPassengertypeCode("INF");
			infant.setNamePrefixTitle("Mst");
			infant.setGivenName(infantfname);
			infant.setSurname(infantsurname);
			infant.setBirthDay("01-01-2014");
			infant.setGender("Male");
			infant.setNationality("Christmas Island");
			infant.setPassportNo("1111111111111");
			infantlist.add(infant);
		}
		resvFillobj.setInfant(infantlist);

		System.out.println("SET FILLING PASSENGER DETAILS END");
		return resvFillobj;
	}

	
	public static ReservationPassengerInfo getPassengerDetails(SearchObject sobj)
	{
		System.out.println("SET FILLING PASSENGER DETAILS");
		
		String[] name = {"one","two","three","four","five","six","seven","eight","nine"};
		ReservationPassengerInfo resvFillobj = new ReservationPassengerInfo();
		int Noofadults = Integer.parseInt(sobj.getAdult());
		int Noofchildren = Integer.parseInt(sobj.getChildren());
		int Noofinfants = Integer.parseInt(sobj.getInfant());
		ArrayList<Traveler> adultlist = new ArrayList<Traveler>();
		ArrayList<Traveler> childrenlist = new ArrayList<Traveler>();
		ArrayList<Traveler> infantlist = new ArrayList<Traveler>(); 
		System.out.println("INFO -> Main traveller");
		String adultfname = "adf";
		String adultsurname = "ads";
		String childfname = "chf";
		String childsurname = "chs";
		String infantfname = "inff";
		String infantsurname = "infs";
		
		Traveler maincustomer = new Traveler();
		maincustomer.setGivenName("Adam");
		maincustomer.setSurname("Levine");
		
		Address add = new Address();
		add.setAddressStreetNo("California");
		add.setAddressCity("California");
		add.setAddressCountry("Christmas Island");
		add.setStateProv("California");
		add.setCountryZip("");
		maincustomer.setAddress(add);
		maincustomer.setPhoneNumber("25122015");
		maincustomer.setEmail("sanoj@colombo.rezgateway.com");
		
		resvFillobj.setMaincustomer(maincustomer);
		System.out.println("INFO -> Main traveller end");
		
		for(int a = 0; a<Noofadults; a++)
		{
			System.out.println("INFO -> adult : "+a+1);
			adultfname = "DanM";
			adultsurname = "WinM";
			adultfname = adultfname.concat(name[a]);
			adultsurname = adultsurname.concat(name[a]);
			Traveler adult = new Traveler();
			adult.setPassengertypeCode("ADT");
			adult.setNamePrefixTitle("Mr");
			adult.setGivenName(adultfname);
			adult.setSurname(adultsurname);
			adult.setPhoneNumber("2444555");
			adult.setPassportNo("111111111111");
			adult.setPassportExpDate("01-01-2020");
			adult.setPassprtIssuDate("01-01-2010");
			adult.setBirthDay("01-01-1995");
			adult.setGender("Male");
			adult.setNationality("Christmas Island");
			adult.setPassportIssuCountry("Christmas Island");
			adult.setPhoneType("HOME");
			adultlist.add(adult);
		}
		resvFillobj.setAdult(adultlist);
		
		for(int c = 0; c<Noofchildren; c++)
		{
			System.out.println("INFO -> child : "+c+1);
			childfname = "DanS";
			childsurname = "WinS";
			childfname = childfname.concat(name[c]);
			childsurname = childsurname.concat(name[c]);
			Traveler child = new Traveler();
			child.setPassengertypeCode("CHD");
			child.setNamePrefixTitle("Mst");
			child.setGivenName(childfname);
			child.setSurname(childsurname);
			child.setPhoneNumber("2444555");
			child.setPassportNo("1111111111111");
			child.setPassportExpDate("01-01-2020");
			child.setPassprtIssuDate("01-01-2010");
			child.setBirthDay("01-01-2008");
			child.setGender("Male");
			child.setNationality("Christmas Island");
			child.setPassportIssuCountry("Christmas Island");
			child.setPhoneType("HOME");
			childrenlist.add(child);
		}
		resvFillobj.setChildren(childrenlist);
		
		for(int c = 0; c<Noofinfants; c++)
		{
			System.out.println("INFO -> infant : "+c+1);
			infantfname = "DanI";
			infantsurname = "WinI";
			infantfname = infantfname.concat(name[c]);
			infantsurname = infantsurname.concat(name[c]);
			Traveler infant = new Traveler();
			infant.setPassengertypeCode("INF");
			infant.setNamePrefixTitle("Mst");
			infant.setGivenName(infantfname);
			infant.setSurname(infantsurname);
			infant.setBirthDay("01-01-2014");
			infant.setGender("Male");
			infant.setNationality("Christmas Island");
			infant.setPassportNo("1111111111111");
			infantlist.add(infant);
		}
		resvFillobj.setInfant(infantlist);
		
		System.out.println("SET FILLING PASSENGER DETAILS END");
		return resvFillobj;
	}

	public ArrayList<CC_Com_AirOfficeID_Setup> getStationDetails() throws FileNotFoundException
	{
		ExcelReader 									newReader 				= 	new ExcelReader();
		FileInputStream 								stream 					=	new FileInputStream(new File("Resources/excels/flight/aaa_nego_officeid.xls"));
		ArrayList<Map<Integer, String>> 				ContentList 			= 	new ArrayList<Map<Integer,String>>();
														ContentList 			= 	newReader.contentReading(stream);
		Map<Integer, String>  							content 				= 	new HashMap<Integer, String>();
														content   				= 	ContentList.get(2);
		Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
		ArrayList<CC_Com_AirOfficeID_Setup> stationList = new ArrayList<CC_Com_AirOfficeID_Setup>();
		while(mapiterator.hasNext())
		{
			Map.Entry<Integer, String> 					Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
			String 										Content 				= 	Entry.getValue();
			String[] 									details		 			= 	Content.split(",");
			CC_Com_AirOfficeID_Setup station = new CC_Com_AirOfficeID_Setup();
			station.setCountry			(details[0]);
			station.setState			(details[1]);
			station.setAirport			(details[2]);
			station.setOfficeid			(details[3]);
			station.setStationReq		(details[4]);
			station.setPaymentFloType	(details[5]);
			station.setVoucherType		(details[6]);
			station.setStationid		(details[7]);
			station.setAdd				(details[8]);
			station.setEdit				(details[9]);
			station.setRemove			(details[10]);
			
			stationList.add(station);
		}
		return stationList;
	}
	
	public ArrayList<CC_Com_AAA> getAAADetails() throws FileNotFoundException
	{
		ExcelReader 									newReader 				= 	new ExcelReader();
		FileInputStream 								stream 					=	new FileInputStream(new File("Resources/excels/flight/aaa_nego_officeid.xls"));
		ArrayList<Map<Integer, String>> 				ContentList 			= 	new ArrayList<Map<Integer,String>>();
														ContentList 			= 	newReader.contentReading(stream);
		Map<Integer, String>  							content 				= 	new HashMap<Integer, String>();
														content   				= 	ContentList.get(1);
		Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
		ArrayList<CC_Com_AAA> aaaList = new ArrayList<CC_Com_AAA>();
		while(mapiterator.hasNext())
		{
			Map.Entry<Integer, String> 					Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
			String 										Content 				= 	Entry.getValue();
			String[] 									details		 			= 	Content.split(",");
			CC_Com_AAA aaa = new CC_Com_AAA();
			aaa.setOfficeid	(details[0]);
			aaa.setSetAAA	(details[1]);
			aaa.setAction	(details[2]);
			aaaList.add(aaa);
		}
		
		return aaaList;
	}
	
	public ArrayList<CC_Com_Nego_Code_Setup> getNegoDetails() throws FileNotFoundException
	{
		ExcelReader 									newReader 				= 	new ExcelReader();
		FileInputStream 								stream 					=	new FileInputStream(new File("Resources/excels/flight/aaa_nego_officeid.xls"));
		ArrayList<Map<Integer, String>> 				ContentList 			= 	new ArrayList<Map<Integer,String>>();
														ContentList 			= 	newReader.contentReading(stream);
		Map<Integer, String>  							content 				= 	new HashMap<Integer, String>();
														content   				= 	ContentList.get(0);
		Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
		ArrayList<CC_Com_Nego_Code_Setup> 				negoList 				= 	new ArrayList<CC_Com_Nego_Code_Setup>();
		while(mapiterator.hasNext())
		{
			Map.Entry<Integer, String> 					Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
			String 										Content 				= 	Entry.getValue();
			String[] 									details		 			= 	Content.split(",");
			CC_Com_Nego_Code_Setup 						nego 					= 	new CC_Com_Nego_Code_Setup();
			nego.setOfficeID	(details[0]);
			nego.setAll			(details[1]);
			nego.setAvailability(details[2]);
			nego.setPrice		(details[3]);
			nego.setReservation	(details[4]);
			nego.setEticket		(details[5]);
			nego.setPnrRead		(details[6]);
			nego.setPnrUpdate	(details[7]);
			nego.setFareRules	(details[8]);
			nego.setCancellation(details[9]);
			nego.setAdd			(details[10]);
			nego.setRemove		(details[11]);
			nego.setEdit		(details[12]);
			negoList.add(nego);
		}
		return negoList;
	}
	
	public static void setRervationDetailsExcel(FlightReservation reservation)
	{
		ExcelWriter Writer = new ExcelWriter();
		Writer.init(reservation);
	}
	
}
