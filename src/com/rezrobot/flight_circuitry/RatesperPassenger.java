/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;

public class RatesperPassenger  implements Serializable
{
	String Currency			= "";
	String passengertype	= "";
	String rateperpsngr		= "";
	String Noofpassengers	= "";
	String taxperpsngr		= "";
	String total			= "";
	
	
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getTaxperpsngr() {
		return taxperpsngr;
	}
	public void setTaxperpsngr(String taxperpsngr) {
		this.taxperpsngr = taxperpsngr;
	}
	public String getPassengertype() {
		return passengertype;
	}
	public void setPassengertype(String passengertype) {
		this.passengertype = passengertype;
	}
	public String getRateperpsngr() {
		return rateperpsngr;
	}
	public void setRateperpsngr(String rateperpsngr) {
		this.rateperpsngr = rateperpsngr;
	}
	public String getNoofpassengers() {
		return Noofpassengers;
	}
	public void setNoofpassengers(String noofpassengers) {
		Noofpassengers = noofpassengers;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

}
