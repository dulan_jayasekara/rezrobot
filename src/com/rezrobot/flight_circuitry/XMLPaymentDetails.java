/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;

public class XMLPaymentDetails  implements Serializable {
	
	String DirectBillID = "";
	String formofpayRPH = "";
	

	public String getFormofpayRPH() {
		return formofpayRPH;
	}

	public void setFormofpayRPH(String formofpayRPH) {
		this.formofpayRPH = formofpayRPH;
	}

	public String getDirectBillID() {
		return DirectBillID;
	}

	public void setDirectBillID(String directBillID) {
		DirectBillID = directBillID;
	}
	
}
