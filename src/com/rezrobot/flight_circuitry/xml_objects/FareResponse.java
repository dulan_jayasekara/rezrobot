/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_objects;

import java.io.Serializable;
import java.util.ArrayList;

import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.XMLPriceItinerary;


public class FareResponse  extends XML  implements Serializable
{
	XMLError error						= null;
	
	ArrayList<XMLPriceItinerary> list	= new ArrayList<XMLPriceItinerary>();
	
	public XMLError getError() {
		return error;
	}

	public void setError(XMLError error) {
		this.error = error;
	}

	public ArrayList<XMLPriceItinerary> getList() {
		return list;
	}

	public void setList(ArrayList<XMLPriceItinerary> list) {
		this.list = list;
	}
}
