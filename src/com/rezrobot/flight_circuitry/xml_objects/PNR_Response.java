package com.rezrobot.flight_circuitry.xml_objects;

import java.util.ArrayList;

import com.rezrobot.flight_circuitry.Traveler;
import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.XMLPriceInfo;
import com.rezrobot.flight_circuitry.XMLResvRespItem;
import com.rezrobot.flight_circuitry.XMLSpecialRemark;
import com.rezrobot.flight_circuitry.XMLSpecialService;


public class PNR_Response 
{
	String ItineraryRefType = "";
	String ItineraryRefID = "";
	String ItineraryRefID_Context = "";
	
	ArrayList<Traveler> Customers = null;
	ArrayList<XMLResvRespItem> Items = null;
	
	String pricinsource = "";
	XMLPriceInfo fare = null;
	
	String tickettimelimit = "";
	String ticketType = "";
	
	ArrayList<XMLSpecialService> SpServiceRequests = null;
	
	ArrayList<String> Remark = null;
	ArrayList<XMLSpecialRemark> specialRemark = null;
	
	ArrayList<String> issuedTickets = null;
	
	String UpdatedCreatedate = "";
	String UpdatedCreatedTime = "";
	String AgencyCommission = "";

	XMLError error = null;
	
	public XMLError getError() {
		return error;
	}
	public void setError(XMLError error) {
		this.error = error;
	}
	public String getItineraryRefType() {
		return ItineraryRefType;
	}
	public void setItineraryRefType(String itineraryRefType) {
		ItineraryRefType = itineraryRefType;
	}
	public String getItineraryRefID() {
		return ItineraryRefID;
	}
	public void setItineraryRefID(String itineraryRefID) {
		ItineraryRefID = itineraryRefID;
	}
	public String getItineraryRefID_Context() {
		return ItineraryRefID_Context;
	}
	public void setItineraryRefID_Context(String itineraryRefID_Context) {
		ItineraryRefID_Context = itineraryRefID_Context;
	}
	public ArrayList<Traveler> getCustomers() {
		return Customers;
	}
	public void setCustomers(ArrayList<Traveler> customers) {
		Customers = customers;
	}
	public ArrayList<XMLResvRespItem> getItems() {
		return Items;
	}
	public void setItems(ArrayList<XMLResvRespItem> items) {
		Items = items;
	}
	public String getPricinsource() {
		return pricinsource;
	}
	public void setPricinsource(String pricinsource) {
		this.pricinsource = pricinsource;
	}
	public XMLPriceInfo getFare() {
		return fare;
	}
	public void setFare(XMLPriceInfo fare) {
		this.fare = fare;
	}
	public String getTickettimelimit() {
		return tickettimelimit;
	}
	public void setTickettimelimit(String tickettimelimit) {
		this.tickettimelimit = tickettimelimit;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public ArrayList<XMLSpecialService> getSpServiceRequests() {
		return SpServiceRequests;
	}
	public void setSpServiceRequests(ArrayList<XMLSpecialService> spServiceRequests) {
		SpServiceRequests = spServiceRequests;
	}
	public ArrayList<String> getRemark() {
		return Remark;
	}
	public void setRemark(ArrayList<String> remark) {
		Remark = remark;
	}
	public ArrayList<XMLSpecialRemark> getSpecialRemark() {
		return specialRemark;
	}
	public void setSpecialRemark(ArrayList<XMLSpecialRemark> specialRemark) {
		this.specialRemark = specialRemark;
	}
	public ArrayList<String> getIssuedTickets() {
		return issuedTickets;
	}
	public void setIssuedTickets(ArrayList<String> issuedTickets) {
		this.issuedTickets = issuedTickets;
	}
	public String getUpdatedCreatedate() {
		return UpdatedCreatedate;
	}
	public void setUpdatedCreatedate(String updatedCreatedate) {
		UpdatedCreatedate = updatedCreatedate;
	}
	public String getUpdatedCreatedTime() {
		return UpdatedCreatedTime;
	}
	public void setUpdatedCreatedTime(String updatedCreatedTime) {
		UpdatedCreatedTime = updatedCreatedTime;
	}
	public String getAgencyCommission() {
		return AgencyCommission;
	}
	public void setAgencyCommission(String agencyCommission) {
		AgencyCommission = agencyCommission;
	}
	
}
