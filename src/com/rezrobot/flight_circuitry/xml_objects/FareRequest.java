/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FareRequest extends XML  implements Serializable {
	
	String ISOCurrency = "";
	String Provider = "";
	String OriginDates1 = "";
	String Origin1 = "";
	String Destination1 = "";
	String OriginDates2 = "";
	String Origin2 = "";
	String Destination2 = "";
	String seatsRequired = "";
	HashMap<String, String> AirTravelers = new HashMap<String, String>();
	ArrayList<String> OriginDestinationInfo = new ArrayList<String>();
	String fareclass = "";
	String FlightType = "";
	String prefFlight = "";
	String negoCode	= "-";
	
	public String getNegoCode() {
		return negoCode;
	}
	public void setNegoCode(String negoCode) {
		this.negoCode = negoCode;
	}
	public String getOriginDates1Time() {
		return OriginDates1Time;
	}
	public void setOriginDates1Time(String originDates1Time) {
		OriginDates1Time = originDates1Time;
	}
	public String getOriginDates2Time() {
		return OriginDates2Time;
	}
	public void setOriginDates2Time(String originDates2Time) {
		OriginDates2Time = originDates2Time;
	}
	String OriginDates1Time = null;
	String OriginDates2Time = null;
	
	
	public String getPrefFlight() {
		return prefFlight;
	}
	public void setPrefFlight(String prefFlight) {
		this.prefFlight = prefFlight;
	}
	public String getFlightType() {
		return FlightType;
	}
	public void setFlightType(String flightType) {
		FlightType = flightType;
	}
	public String getISOCurrency() {
		return ISOCurrency;
	}
	public void setISOCurrency(String iSOCurrency) {
		ISOCurrency = iSOCurrency;
	}
	public String getProvider() {
		return Provider;
	}
	public void setProvider(String provider) {
		Provider = provider;
	}

	public String getOriginDates1() {
		return OriginDates1;
	}
	public void setOriginDates1(String originDates1) {
		OriginDates1 = originDates1;
	}
	public String getOrigin1() {
		return Origin1;
	}
	public void setOrigin1(String origin1) {
		Origin1 = origin1;
	}
	public String getDestination1() {
		return Destination1;
	}
	public void setDestination1(String destination1) {
		Destination1 = destination1;
	}
	public String getOriginDates2() {
		return OriginDates2;
	}
	public void setOriginDates2(String originDates2) {
		OriginDates2 = originDates2;
	}
	public String getOrigin2() {
		return Origin2;
	}
	public void setOrigin2(String origin2) {
		Origin2 = origin2;
	}
	public String getDestination2() {
		return Destination2;
	}
	public void setDestination2(String destination2) {
		Destination2 = destination2;
	}
	public String getSeatsRequired() {
		return seatsRequired;
	}
	public void setSeatsRequired(String seatsRequired) {
		this.seatsRequired = seatsRequired;
	}
	public Map<String, String> getAirTravelers() {
		return AirTravelers;
	}
	public void setAirTravelers(HashMap<String, String> airTravelers) {
		AirTravelers = airTravelers;
	}
	String PricingSource = null;
	public String getPricingSource() {
		return PricingSource;
	}
	public void setPricingSource(String pricingSource) {
		PricingSource = pricingSource;
	}

	
	public String getFareclass() {
		return fareclass;
	}
	public void setFareclass(String fareclass) {
		this.fareclass = fareclass;
	}
	public ArrayList<String> getOriginDestinationInfo() {
		return OriginDestinationInfo;
	}
	
	
	public void setOriginDestinationInfo(ArrayList<String> originDestinationInfo) {
		OriginDestinationInfo = originDestinationInfo;
		
		for(int y=0; y<originDestinationInfo.size(); y++)
		{
			String all = originDestinationInfo.get(y).toString();
			if(y==0)
			{
				setOriginDates1(all.split(",")[0].split("T")[0]);
				setOriginDates1Time(all.split(",")[0].split("T")[1]);
				setOrigin1(all.split(",")[1]);
				setDestination1(all.split(",")[2]);
			}
			if(y==1)
			{
				setOriginDates2(all.split(",")[0].split("T")[0]);
				setOriginDates2Time(all.split(",")[0].split("T")[1]);
				setOrigin2(all.split(",")[1]);
				setDestination2(all.split(",")[2]);
			}
		}
		
	}
	
	
	public void getAll()
	{
		System.out.println(getISOCurrency());
		System.out.println(getProvider());
		System.out.println(getOriginDates1());
		System.out.println(getOrigin1());
		System.out.println(getDestination1());
		System.out.println(getOriginDates2());
		System.out.println(getOrigin2());
		System.out.println(getDestination2());
		System.out.println(getFareclass());
		System.out.println(getSeatsRequired());
		System.out.println(getPricingSource());
		System.out.println(getAirTravelers());
	}

}
