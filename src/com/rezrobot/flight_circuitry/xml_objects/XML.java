package com.rezrobot.flight_circuitry.xml_objects;

public class XML {

	private String url			= "";
	private String tracer		= "";
	private String XMLFileType	= "";
	private String tracer2		= "";
	
	public String getTracer2() {
		return tracer2;
	}
	public void setTracer2(String tracer2) {
		this.tracer2 = tracer2;
	}
	public String getXMLFileType() {
		return XMLFileType;
	}
	public void setXMLFileType(String xMLFileType) {
		XMLFileType = xMLFileType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTracer() {
		return tracer;
	}
	public void setTracer(String tracer) {
		this.tracer = tracer;
	}
	
}
