package com.rezrobot.flight_circuitry.xml_objects;

import com.rezrobot.flight_circuitry.XMLError;



public class CancellationResponse {

	XMLError	error		= new XMLError();
	String		Status		= "";
	String		uniqueID	= "";
	
	public XMLError getError() {
		return error;
	}
	public void setError(XMLError error) {
		this.error = error;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

}
