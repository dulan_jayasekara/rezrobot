package com.rezrobot.flight_circuitry.xml_objects;

import java.util.ArrayList;

import com.rezrobot.flight_circuitry.Eticket;
import com.rezrobot.flight_circuitry.XMLError;


public class EticketResponse 
{
	XMLError Error					= null;
	
	String UniqueID					= "";
	
	String TicketingControlType		= "";
	String TicketType				= "";
	
	ArrayList<Eticket> eticketList	= null;
	
	public String getTicketType() {
		return TicketType;
	}
	public void setTicketType(String ticketType) {
		TicketType = ticketType;
	}	
	public ArrayList<Eticket> getEticketList() {
		return eticketList;
	}
	public void setEticketList(ArrayList<Eticket> eticketList) {
		this.eticketList = eticketList;
	}
	public XMLError getError() {
		return Error;
	}
	public void setError(XMLError error) {
		Error = error;
	}
	public String getUniqueID() {
		return UniqueID;
	}
	public void setUniqueID(String uniqueID) {
		UniqueID = uniqueID;
	}
	public String getTicketingControlType() {
		return TicketingControlType;
	}
	public void setTicketingControlType(String ticketingControlType) {
		TicketingControlType = ticketingControlType;
	}
	
}
