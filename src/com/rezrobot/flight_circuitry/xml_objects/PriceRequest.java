/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import com.rezrobot.flight_circuitry.XMLOriginOptions;




public class PriceRequest extends XML  implements Serializable {
	
	
	private		String						ISOCurrency		 = "";
	private		String						provider		 = "";
	private		ArrayList<XMLOriginOptions>	originOptionList = new ArrayList<XMLOriginOptions>();
	private		String						seatRequested	 = "";
	private		Map<String, String>			AirTravelers	 = null;
	private		String						PricingSource	 = "";
	String negoCode	= "";
	
	
	

	public String getNegoCode() {
		return negoCode;
	}
	public void setNegoCode(String negoCode) {
		this.negoCode = negoCode;
	}
	public String getPricingSource() {
		return PricingSource;
	}
	public void setPricingSource(String pricingSource) {
		PricingSource = pricingSource;
	}
	public String getISOCurrency() {
		return ISOCurrency;
	}
	public void setISOCurrency(String iSOCurrentcy) {
		ISOCurrency = iSOCurrentcy;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public ArrayList<XMLOriginOptions> getOriginOptionList() {
		return originOptionList;
	}
	public void setOriginOptionList(ArrayList<XMLOriginOptions> originOptionList) {
		this.originOptionList = originOptionList;
	}
	public String getSeatRequested() {
		return seatRequested;
	}
	public void setSeatRequested(String seatRequested) {
		this.seatRequested = seatRequested;
	}
	public Map<String, String> getAirTravelers() {
		return AirTravelers;
	}
	public void setAirTravelers(Map<String, String> airTravelers) {
		AirTravelers = airTravelers;
	}
	
	public void getAll()
	{
		System.out.println(getISOCurrency());
		System.out.println(getPricingSource());
		System.out.println(getProvider());
		System.out.println(getSeatRequested());
		System.out.println(getAirTravelers());
		//System.out.println(getOriginOptionList());
	}

}
