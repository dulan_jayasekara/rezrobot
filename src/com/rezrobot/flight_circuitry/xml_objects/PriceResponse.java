/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_objects;

import java.io.Serializable;

import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.XMLPriceInfo;




public class PriceResponse extends XML  implements Serializable {
	

	XMLPriceInfo priceinfo			= null;
	String PriceItineraryNumber		= "";
	String pricingInfo				= "";
	String feeCode					= "";
	String feeAmount				= "";
	String feeCurrencyCode			= "";
	String tickettimelimit			= "";
	/*String Errortype				= "";
	String Errorcode				= "";
	String ErrorMessage				= "";*/
	XMLError error					= null;
	boolean available				= false;
	
	
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public XMLError getError() {
		return error;
	}
	public void setError(XMLError error) {
		this.error = error;
	}
	public String getPricingInfo() {
		return pricingInfo;
	}
	public void setPricingInfo(String pricingInfo) {
		this.pricingInfo = pricingInfo;
	}
	
	public String getFeeCode() {
		return feeCode;
	}
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	public String getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getFeeCurrencyCode() {
		return feeCurrencyCode;
	}
	public void setFeeCurrencyCode(String feeCurrencyCode) {
		this.feeCurrencyCode = feeCurrencyCode;
	}
	
	public String getPriceItineraryNumber() {
		return PriceItineraryNumber;
	}
	public void setPriceItineraryNumber(String priceItineraryNumber) {
		PriceItineraryNumber = priceItineraryNumber;
	}
	
	public String getTickettimelimit() {
		return tickettimelimit;
	}
	public void setTickettimelimit(String tickettimelimit) {
		this.tickettimelimit = tickettimelimit;
	}
	public XMLPriceInfo getPriceinfo() {
		return priceinfo;
	}
	public void setPriceinfo(XMLPriceInfo priceinfo) {
		this.priceinfo = priceinfo;
	}
	
	/*public boolean isIschanged() {
		return ischanged;
	}
	public void setIschanged(boolean ischanged) {
		this.ischanged = ischanged;
	}*/
/*	
	public String getErrorcode() {
		return Errorcode;
	}
	public void setErrorcode(String errorcode) {
		Errorcode = errorcode;
	}
	public String getErrorMessage() {
		return ErrorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getErrortype() {
		return Errortype;
	}
	public void setErrortype(String errortype) {
		Errortype = errortype;
	}
	
	*/
	
}
