package com.rezrobot.flight_circuitry.xml_objects;

import java.io.Serializable;
import java.util.ArrayList;

import com.rezrobot.flight_circuitry.XMLOriginOptions;
import com.rezrobot.flight_circuitry.XMLPNRData;
import com.rezrobot.flight_circuitry.XMLPaymentDetails;
import com.rezrobot.flight_circuitry.XMLSpecialService;


public class ReservationRequest extends XML  implements Serializable {

	String psudoCityCode = "";
	String agentSine = "";
	String IsoCurrency = "";	
	String RequesterType = "";
	String RequesterID = "";
	String provider = "";
	String direction = "";
	ArrayList<XMLOriginOptions> OriginDestinationInfo = new ArrayList<XMLOriginOptions>();
	XMLPaymentDetails paymentdetails = null;
	
	String DeliveryStreetNo = "";
	String DeliveryCityName = "";
	String DeliveryPostal = "";
	String DeliveryCountryName = "";
	String DeliveryStateCode = "";
	
	ArrayList<String> Remark = null;
	ArrayList<XMLSpecialService> SpServiceRequests = null;
	XMLPNRData PNR = null;
	String PriceType = "";
	String TicketType = "";
	String AgencyCommission = "";
	String autoTicketing = "";
	String negoCode	= "";
	
	
	

	public String getNegoCode() {
		return negoCode;
	}
	public void setNegoCode(String negoCode) {
		this.negoCode = negoCode;
	}
	
	
	
	public String getAutoTicketing() {
		return autoTicketing;
	}
	public void setAutoTicketing(String autoTicketing) {
		this.autoTicketing = autoTicketing;
	}
	boolean available = true;
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public String getAgencyCommission() {
		return AgencyCommission;
	}
	public void setAgencyCommission(String agencyCommission) {
		AgencyCommission = agencyCommission;
	}
	public String getPriceType() {
		return PriceType;
	}
	public void setPriceType(String priceType) {
		PriceType = priceType;
	}
	public String getTicketType() {
		return TicketType;
	}
	public void setTicketType(String ticketType) {
		TicketType = ticketType;
	}

	public XMLPNRData getPNR() {
		return PNR;
	}
	public void setPNR(XMLPNRData pNR) {
		PNR = pNR;
	}
	public String getDeliveryPostal() {
		return DeliveryPostal;
	}
	public void setDeliveryPostal(String deliveryPostal) {
		DeliveryPostal = deliveryPostal;
	}
	public String getDeliveryStateCode() {
		return DeliveryStateCode;
	}
	public void setDeliveryStateCode(String deliveryStateCode) {
		DeliveryStateCode = deliveryStateCode;
	}
	

	public ArrayList<XMLSpecialService> getSpServiceRequests() {
		return SpServiceRequests;
	}
	public void setSpServiceRequests(ArrayList<XMLSpecialService> spServiceRequests) {
		SpServiceRequests = spServiceRequests;
	}
	public ArrayList<String> getRemark() {
		return Remark;
	}
	public void setRemark(ArrayList<String> remark) {
		Remark = remark;
	}
	public String getDeliveryStreetNo() {
		return DeliveryStreetNo;
	}
	public void setDeliveryStreetNo(String deliveryStreetNo) {
		DeliveryStreetNo = deliveryStreetNo;
	}
	public String getDeliveryCityName() {
		return DeliveryCityName;
	}
	public void setDeliveryCityName(String deliveryCityName) {
		DeliveryCityName = deliveryCityName;
	}
	public String getDeliveryCountryName() {
		return DeliveryCountryName;
	}
	public void setDeliveryCountryName(String deliveryCountryName) {
		DeliveryCountryName = deliveryCountryName;
	}
	
	
	
	public XMLPaymentDetails getPaymentdetails() {
		return paymentdetails;
	}
	public void setPaymentdetails(XMLPaymentDetails paymentdetails) {
		this.paymentdetails = paymentdetails;
	}
	public ArrayList<XMLOriginOptions> getOriginDestinationInfo() {
		return OriginDestinationInfo;
	}
	public void setOriginDestinationInfo(
			ArrayList<XMLOriginOptions> originDestinationInfo) {
		OriginDestinationInfo = originDestinationInfo;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getRequesterType() {
		return RequesterType;
	}
	public void setRequesterType(String requesterType) {
		RequesterType = requesterType;
	}

	public String getRequesterID() {
		return RequesterID;
	}
	public void setRequesterID(String requesterID) {
		RequesterID = requesterID;
	}

	public String getPsudoCityCode() {
		return psudoCityCode;
	}
	public void setPsudoCityCode(String psudoCityCode) {
		this.psudoCityCode = psudoCityCode;
	}
	public String getAgentSine() {
		return agentSine;
	}
	public void setAgentSine(String agentSine) {
		this.agentSine = agentSine;
	}
	public String getIsoCurrency() {
		return IsoCurrency;
	}
	public void setIsoCurrency(String isoCurrency) {
		IsoCurrency = isoCurrency;
	}

	
}
