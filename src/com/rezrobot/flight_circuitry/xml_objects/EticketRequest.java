package com.rezrobot.flight_circuitry.xml_objects;

import com.rezrobot.flight_circuitry.Provider;


public class EticketRequest 
{
	String		PseudoCityCode		= "";
	String		RequestorID			= "";
	
	String		RequestorIDType		= "";
	Provider	provider			= null;
	
	String		UniqueID			= "";
	String		TicketType			= "";
	String		OmitInfant			= "";
	String		NotificatioByEmail	= "";
	
	
	public String getRequestorID() {
		return RequestorID;
	}
	public void setRequestorID(String requestorID) {
		RequestorID = requestorID;
	}
	public String getRequestorIDType() {
		return RequestorIDType;
	}
	public void setRequestorIDType(String requestorIDType) {
		RequestorIDType = requestorIDType;
	}
	public String getPseudoCityCode() {
		return PseudoCityCode;
	}
	public void setPseudoCityCode(String pseudoCityCode) {
		PseudoCityCode = pseudoCityCode;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(com.rezrobot.flight_circuitry.Provider provider2) {
		this.provider = provider2;
	}
	public String getUniqueID() {
		return UniqueID;
	}
	public void setUniqueID(String uniqueID) {
		UniqueID = uniqueID;
	}
	public String getTicketType() {
		return TicketType;
	}
	public void setTicketType(String ticketType) {
		TicketType = ticketType;
	}
	public String getOmitInfant() {
		return OmitInfant;
	}
	public void setOmitInfant(String omitInfant) {
		OmitInfant = omitInfant;
	}
	public String getNotificatioByEmail() {
		return NotificatioByEmail;
	}
	public void setNotificatioByEmail(String notificatioByEmail) {
		NotificatioByEmail = notificatioByEmail;
	}
	
}
