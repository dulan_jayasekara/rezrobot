package com.rezrobot.flight_circuitry.xml_objects;

import com.rezrobot.flight_circuitry.Provider;


public class PNR_Request 
{
	String UniqueID = "";
	
	String PseudoCityCode = "";
	
	Provider provider = null;
	
	/*String RequestorIDType = "";
	String RequestorID = "";*/

	public String getUniqueID() {
		return UniqueID;
	}
	public void setUniqueID(String uniqueID) {
		UniqueID = uniqueID;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	public String getPseudoCityCode() {
		return PseudoCityCode;
	}
	public void setPseudoCityCode(String pseudoCityCode) {
		PseudoCityCode = pseudoCityCode;
	}
	
/*	public String getRequestorIDType() {
		return RequestorIDType;
	}
	public void setRequestorIDType(String requestorIDType) {
		RequestorIDType = requestorIDType;
	}
	public String getRequestorID() {
		return RequestorID;
	}
	public void setRequestorID(String requestorID) {
		RequestorID = requestorID;
	}*/
	
}
