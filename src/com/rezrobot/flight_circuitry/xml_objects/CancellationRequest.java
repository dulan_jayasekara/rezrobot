package com.rezrobot.flight_circuitry.xml_objects;

import com.rezrobot.flight_circuitry.XMLError;


public class CancellationRequest {

	String echoToken	= "";
	String uniqueID		= "";
	XMLError error		= new XMLError();
	
	
	
	public XMLError getError() {
		return error;
	}
	public void setError(XMLError error) {
		this.error = error;
	}
	public String getEchoToken() {
		return echoToken;
	}
	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	
	
}
