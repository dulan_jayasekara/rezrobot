package com.rezrobot.flight_circuitry;

import java.io.Serializable;

public class Costs  implements Serializable{

	String	currencyCode			= "";
	String	basefareAmount			= "0";
	String	basefareCurrencyCode	= "";
	String	taxAmount				= "0";
	String	taxCurrencyCode			= "";
	String	profit					= "";
	String	subtotal				= "";
	String	totalTaxAndOtherCharges	= "";
	String	totalWithoutCCFee		= "";
	String	bookingFee				= "";
	String	discount				= "";				
	String	creditCardFee			= "";
	String	sellingRate				= "";
	String	totalFare				= "0";
	String	totalFareCurrencyCode	= "";
	int		totalFareDecimal		= 2;
	int		taxAmountDecimal		= 2;
	int		basefareAmountDecimal	= 2;
	String	equivFare				= "";
	String	equivCurrencyCode		= "";
	int		equivDecimal			= 0;
	RatesperPassenger ratesPerPassenger = new RatesperPassenger();
	
	
	public String getTotalWithoutCCFee() {
		return totalWithoutCCFee;
	}
	public void setTotalWithoutCCFee(String totalWithoutCCFee) {
		this.totalWithoutCCFee = totalWithoutCCFee;
	}
	public String getTotalTaxAndOtherCharges() {
		return totalTaxAndOtherCharges;
	}
	public void setTotalTaxAndOtherCharges(String totalTaxAndOtherCharges) {
		this.totalTaxAndOtherCharges = totalTaxAndOtherCharges;
	}
	public String getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getBasefareAmount() {
		return basefareAmount;
	}
	public void setBasefareAmount(String basefareAmount) {
		this.basefareAmount = basefareAmount;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public void setTotalFareDecimal(int totalFareDecimal) {
		this.totalFareDecimal = totalFareDecimal;
	}
	public void setTaxAmountDecimal(int taxAmountDecimal) {
		this.taxAmountDecimal = taxAmountDecimal;
	}
	public void setBasefareAmountDecimal(int basefareAmountDecimal) {
		this.basefareAmountDecimal = basefareAmountDecimal;
	}
	public void setEquivDecimal(int equivDecimal) {
		this.equivDecimal = equivDecimal;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getCreditCardFee() {
		return creditCardFee;
	}
	public void setCreditCardFee(String creditCardFee) {
		this.creditCardFee = creditCardFee;
	}
	public String getSellingRate() {
		return sellingRate;
	}
	public void setSellingRate(String sellingRate) {
		this.sellingRate = sellingRate;
	}
	public RatesperPassenger getRatesPerPassenger() {
		return ratesPerPassenger;
	}
	public void setRatesPerPassenger(RatesperPassenger ratesPerPassenger) {
		this.ratesPerPassenger = ratesPerPassenger;
	}
	public String getBasefareCurrencyCode() {
		return basefareCurrencyCode;
	}
	public void setBasefareCurrencyCode(String basefareCurrencyCode) {
		this.basefareCurrencyCode = basefareCurrencyCode;
	}
	public String getTaxCurrencyCode() {
		return taxCurrencyCode;
	}
	public void setTaxCurrencyCode(String taxCurrencyCode) {
		this.taxCurrencyCode = taxCurrencyCode;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getTotalFareCurrencyCode() {
		return totalFareCurrencyCode;
	}
	public void setTotalFareCurrencyCode(String totalFareCurrencyCode) {
		this.totalFareCurrencyCode = totalFareCurrencyCode;
	}
	public int getTotalFareDecimal() {
		return totalFareDecimal;
	}
	public void setTotalFareDecimal(String TotalFareDecimal) {
		//this.totalFareDecimal = totalFareDecimal;
		try
		{
			totalFareDecimal = Integer.parseInt(TotalFareDecimal);
		}
		catch(Exception e)
		{
			
		}
		
	}
	public int getTaxAmountDecimal() {
		return taxAmountDecimal;
	}
	public void setTaxAmountDecimal(String TaxAmountDecimal) {
		//this.taxAmountDecimal = taxAmountDecimal;
		try
		{
			taxAmountDecimal = Integer.parseInt(TaxAmountDecimal);
		}
		catch(Exception e){
			
		}
	}
	public int getBasefareAmountDecimal() {
		return basefareAmountDecimal;
	}
	public void setBasefareAmountDecimal(String BasefareAmountDecimal) {
		//this.basefareAmountDecimal = basefareAmountDecimal;
		try
		{
			basefareAmountDecimal = Integer.parseInt(BasefareAmountDecimal.trim());
		}
		catch(Exception e)
		{
			
		}
	}
	public String getEquivFare() {
		return equivFare;
	}
	public void setEquivFare(String equivFare) {
		this.equivFare = equivFare;
	}
	public String getEquivCurrencyCode() {
		return equivCurrencyCode;
	}
	public void setEquivCurrencyCode(String equivCurrencyCode) {
		this.equivCurrencyCode = equivCurrencyCode;
	}
	public int getEquivDecimal() {
		return equivDecimal;
	}
	public void setEquivDecimal(String EquivDecimal) {
		//this.equivDecimal = equivDecimal;
		try
		{
			equivDecimal = Integer.parseInt(EquivDecimal);
		}
		catch(Exception e)
		{
			
		}
	}
	
}
