/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;

public class XMLSpecialRemark  implements Serializable{
	
	String remarkType       = "";
	String travelerRefNoRPH = "";
	String flgithrefNoRPH   = "";
	String text             = "";
	String remarkOrigin     = "";
	
	
	public String getRemarkType() {
		return remarkType;
	}
	public void setRemarkType(String remarkType) {
		this.remarkType = remarkType;
	}
	public String getTravelerRefNoRPH() {
		return travelerRefNoRPH;
	}
	public void setTravelerRefNoRPH(String travelerRefNoRPH) {
		this.travelerRefNoRPH = travelerRefNoRPH;
	}
	public String getFlgithrefNoRPH() {
		return flgithrefNoRPH;
	}
	public void setFlgithrefNoRPH(String flgithrefNoRPH) {
		this.flgithrefNoRPH = flgithrefNoRPH;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRemarkOrigin() {
		return remarkOrigin;
	}
	public void setRemarkOrigin(String remarkOrigin) {
		this.remarkOrigin = remarkOrigin;
	}

}
