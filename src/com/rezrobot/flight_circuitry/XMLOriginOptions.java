/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;
import java.util.ArrayList;


public class XMLOriginOptions  implements Serializable  
{
	private 	ArrayList<Flight>		flightlist			= null;
	private 	String					departuredate		= "";
	private 	String					arrivaldate			= "";
	private 	String					flightNo			= "";
	private		String					journeyDuration		= "";
	
	
	

	public String getJourneyDuration() {
		return journeyDuration;
	}

	public void setJourneyDuration(String journeyDuration) {
		this.journeyDuration = journeyDuration;
	}

	public 		String getDeparturedate() {
		return departuredate;
	}

	public void setDeparturedate(String departuredate) {
		this.departuredate = departuredate;
	}

	public String getArrivaldate() {
		return arrivaldate;
	}

	public void setArrivaldate(String arrivaldate) {
		this.arrivaldate = arrivaldate;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public ArrayList<Flight> getFlightlist() 
	{
		return flightlist;
	}

	public void setFlightlist(ArrayList<Flight> flightlist) 
	{
		this.flightlist = flightlist;
	}
	
	public void getAll()
	{
		System.out.println("-----Origin Option object is called----");
		System.out.println();
		System.out.println("Departure Date : "+getDeparturedate());
		System.out.println("Arrival Date : "+getArrivaldate());
		System.out.println("Flight No : "+getFlightNo());
		System.out.println();
		for(int u = 0; u<flightlist.size(); u++)
		{
			System.out.println("Flight List : "+(u+1));
			flightlist.get(u).getAll();
		}
		System.out.println();
	}

	
}

