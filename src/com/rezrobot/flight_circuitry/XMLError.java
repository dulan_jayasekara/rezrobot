package com.rezrobot.flight_circuitry;

import java.io.Serializable;

public class XMLError  implements Serializable
{
	String Errortype					= "";
	String Errorcode					= "";
	String ErrorMessage					= "";
	
	
	public String getErrortype() {
		return Errortype;
	}
	public void setErrortype(String errortype) {
		Errortype = errortype;
	}
	public String getErrorcode() {
		return Errorcode;
	}
	public void setErrorcode(String errorcode) {
		Errorcode = errorcode;
	}
	public String getErrorMessage() {
		return ErrorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}
	
}
