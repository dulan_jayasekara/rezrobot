/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_object_readers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rezrobot.flight_circuitry.Costs;
import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.XMLOriginOptions;
import com.rezrobot.flight_circuitry.XMLPriceInfo;
import com.rezrobot.flight_circuitry.xml_objects.PriceResponse;

public class PriceResponseReader {
	
	
	//Logger 							logger				= null;
	File							input				= null;
	ArrayList<XMLOriginOptions> 	AirItinerary		= new ArrayList<XMLOriginOptions>();
	Map<String, String> 			AirTravelers 		= new HashMap<String, String>();	
	boolean							err					= false;
	
	public PriceResponseReader()
	{
		
	}
	
	public PriceResponseReader(HashMap<String, String> Propymap)
	{
		
	}

	public PriceResponse AmadeusPriceResponseReader(Document doc, PriceResponse priceiterobj)
	{
		System.out.println("========================================");
		System.out.println("PRICE RESPONSE XML READ");
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").get(0).text());
			err = true;
			error.setErrortype(doc.getElementsByTag("Error").get(0).attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").get(0).attr("Code"));
		}
		catch(Exception exx)
		{
			//exx.printStackTrace();
			
			try {
				if(!err/*priceiterobj.getError().getErrorMessage().equals("")*/)
				{
					XMLPriceInfo priceinfo = new XMLPriceInfo();
					Costs costsInSupplierCurrency = new Costs();
					try
					{
						priceiterobj.setPriceItineraryNumber(doc.getElementsByTag("PricedItinerary").attr("SequenceNumber"));
						
						String pricingSource = "Published";
						pricingSource = doc.getElementsByTag("AirItineraryPricingInfo ").attr("PricingSource");
						if(!pricingSource.equals(""))
						{
							priceinfo.setPricingSource(pricingSource);
						}
						else
						{
							priceinfo.setPricingSource("Published");
						}
						
						
						String BaseFare = "";
						BaseFare = doc.getElementsByTag("BaseFare").attr("Amount");
						costsInSupplierCurrency.setBasefareAmountDecimal(doc.getElementsByTag("BaseFare").attr("DecimalPlaces"));
						
						try {
							if(costsInSupplierCurrency.getBasefareAmountDecimal() != 0)
							{
								BaseFare = BaseFare.substring(0, BaseFare.length() - costsInSupplierCurrency.getBasefareAmountDecimal() ).concat(".").concat(BaseFare.substring(BaseFare.length() - costsInSupplierCurrency.getBasefareAmountDecimal(), BaseFare.length()));
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						costsInSupplierCurrency.setBasefareAmount(BaseFare);
						costsInSupplierCurrency.setBasefareCurrencyCode(doc.getElementsByTag("BaseFare").attr("CurrencyCode"));
						
						
						String Tax = "";
						Tax = doc.getElementsByTag("Tax").attr("Amount");
						costsInSupplierCurrency.setTaxAmountDecimal(doc.getElementsByTag("Tax").attr("DecimalPlaces"));
						try {
							if(costsInSupplierCurrency.getTaxAmountDecimal() != 0)
							{
								Tax = Tax.substring(0, Tax.length() - costsInSupplierCurrency.getTaxAmountDecimal() ).concat(".").concat(Tax.substring(Tax.length() - costsInSupplierCurrency.getTaxAmountDecimal(), Tax.length()));
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						costsInSupplierCurrency.setTaxCurrencyCode(doc.getElementsByTag("Tax").attr("CurrencyCode"));
						costsInSupplierCurrency.setTaxAmount(Tax);
						priceinfo.setTaxCode(doc.getElementsByTag("Tax").attr("TaxCode"));
						
						String Total = "";
						Total = doc.getElementsByTag("TotalFare").attr("Amount");
						costsInSupplierCurrency.setTotalFareDecimal(doc.getElementsByTag("TotalFare").attr("DecimalPlaces"));
						try {
							if(costsInSupplierCurrency.getTotalFareDecimal() != 0)
							{
								Total = Total.substring(0, Total.length() - costsInSupplierCurrency.getTotalFareDecimal() ).concat(".").concat(Total.substring(Total.length() - costsInSupplierCurrency.getTotalFareDecimal(), Total.length()));
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						costsInSupplierCurrency.setTotalFare(Total);
						costsInSupplierCurrency.setTotalFareCurrencyCode(doc.getElementsByTag("TotalFare").attr("CurrencyCode"));
						priceinfo.setInSupplierCurrency(costsInSupplierCurrency);
						priceiterobj.setPriceinfo(priceinfo);
						
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
					
					
					try
					{
								
						Elements farebreakdown = doc.getElementsByTag("PTC_FareBreakdown");
						//System.out.println(farebreakdown.size());
						
						Iterator<Element> fareiter = farebreakdown.iterator();
						
						while(fareiter.hasNext())
						{
							Element ele1 = fareiter.next();
							
							Elements farebasiscode = ele1.getElementsByTag("FareBasisCode");
							Iterator<Element> farebasisiter = farebasiscode.iterator();
							
							ArrayList<String> farebasisCODE = new ArrayList<String>();
							
							while(farebasisiter.hasNext())
							{
								Element ele2 = farebasisiter.next();
								farebasisCODE.add(ele2.getElementsByTag("FareBasisCode").text());
							}
			
						}
					}
					catch(Exception ep)
					{
						ep.printStackTrace();
					}
					
					try
					{
						Elements fareInfo = doc.getElementsByTag("FareInfo");
						
						Iterator<Element> fareInfoiter = fareInfo.iterator();
						ArrayList<String> FareInfo = new ArrayList<String>();
						
						while(fareInfoiter.hasNext())
						{
							Element ele4 = fareInfoiter.next();
							String a = ele4.getElementsByTag("DepartureDate").text();
							String b = a.split("T")[1];
							a = a.split("T")[0];
							String c = ele4.getElementsByTag("FareReference").text();
							String d = ele4.getElementsByTag("FilingAirLine").attr("Code");
							String e = ele4.getElementsByTag("DepartureAirport").attr("LocationCode");
							String f = ele4.getElementsByTag("ArrivalAirport").attr("LocationCode");
							
							String fareinfo = a.concat(",").concat(b).concat(",").concat(c).concat(",").concat(d).concat(",").concat(e).concat(",").concat(f);
							FareInfo.add(fareinfo);
						}
						
						priceiterobj.setTickettimelimit(doc.getElementsByTag("TicketingInfo ").attr("TicketTimeLimit").split("T")[0]);
					}
					catch(Exception er)
					{
						er.printStackTrace();
						
					}
					
					priceiterobj.setAvailable(true);
					System.out.println("Price response reading successful..!!");
				}
			} catch (Exception e) {
				priceiterobj.setAvailable(false);
			}
		}

		System.out.println("PRICE RESPONSE XML READ END");
		System.out.println("====================================");
		
		priceiterobj.setError(error);
		
		return priceiterobj;
		
	}
/*	
	
	public void SabrePriceRequestReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		Document doc = getdoc.createDoc(Value, Val, type);
		PriceResponse priceiterobj = new PriceResponse();
		
		priceiterobj.setBaseDecimalPlaces(doc.getElementsByTag("BaseFare").attr("DecimalPlaces"));
		priceiterobj.setBaseCurrencyCode(doc.getElementsByTag("BaseFare").attr("CurrencyCode"));
		priceiterobj.setBaseFareAmount(doc.getElementsByTag("BaseFare").attr("Amount"));
		priceiterobj.setTaxDecimalPlaces(doc.getElementsByTag("Tax").attr("DecimalPlaces"));
		priceiterobj.setTaxCurrencyCode(doc.getElementsByTag("Tax").attr("CurrencyCode"));
		priceiterobj.setTaxAmount(doc.getElementsByTag("Tax").attr("Amount"));
		priceiterobj.setTaxCode(doc.getElementsByTag("Tax").attr("TaxCode"));
		
		Elements farebreakdown = doc.getElementsByTag("PTC_FareBreakdown");
		
		Iterator<Element> fareiter = farebreakdown.iterator();
		
		while(fareiter.hasNext())
		{
			Element ele1 = fareiter.next();
			
			System.out.println(ele1.getElementsByTag("PassengerTypeQuantity").attr("Code"));
			System.out.println(ele1.getElementsByTag("PassengerTypeQuantity").attr("Quantity"));
			
			Elements farebasiscode = ele1.getElementsByTag("FareBasisCode");
			Iterator<Element> farebasisiter = farebasiscode.iterator();
			
			ArrayList<String> farebasisCODE = new ArrayList<String>();
			
			while(farebasisiter.hasNext())
			{
				Element ele2 = farebasisiter.next();
				farebasisCODE.add(ele2.getElementsByTag("FareBasisCode").text());
				System.out.println(farebasisCODE);
			}

			System.out.println(ele1.getElementsByTag("TotalFare").attr("Amount"));
			System.out.println(ele1.getElementsByTag("TotalFare").attr("CurrencyCode"));
			System.out.println(ele1.getElementsByTag("TotalFare").attr("DecimalPlaces"));
			
		}
		
		Elements fareInfo = doc.getElementsByTag("FareInfo");
		
		Iterator<Element> fareInfoiter = fareInfo.iterator();
		ArrayList<String> FareInfo = new ArrayList<String>();
		
		while(fareInfoiter.hasNext())
		{
			Element ele4 = fareInfoiter.next();
			String a = ele4.getElementsByTag("DepartureDate").text();
			String b = ele4.getElementsByTag("FareReference").text();
			String c = ele4.getElementsByTag("RuleInfo").text();
			String d = ele4.getElementsByTag("FilingAirLine").attr("Code");
			String e = ele4.getElementsByTag("DepartureAirport").attr("LocationCode");
			String f = ele4.getElementsByTag("ArrivalAirport").attr("LocationCode");
			
			String fareinfo = a.concat(",").concat(b).concat(",").concat(c).concat(",").concat(d).concat(",").concat(e).concat(",").concat(f);
			FareInfo.add(fareinfo);
		}
	}*/
}
