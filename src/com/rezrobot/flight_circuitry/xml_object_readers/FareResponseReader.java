/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_object_readers;

import java.io.File;
import java.util.ArrayList;
//import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rezrobot.flight_circuitry.Costs;
import com.rezrobot.flight_circuitry.Flight;
import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.XMLOriginOptions;
import com.rezrobot.flight_circuitry.XMLPriceInfo;
import com.rezrobot.flight_circuitry.XMLPriceItinerary;
import com.rezrobot.flight_circuitry.xml_objects.FareResponse;




public class FareResponseReader 
{
	Elements originoptions                  	= null;
	Elements flightsegments                 	= null;
	ArrayList<XMLPriceItinerary> priceiterlist 	= new ArrayList<XMLPriceItinerary>();
	ArrayList<Flight> flightobjlist         	= null;
	ArrayList<XMLOriginOptions> originobjlist  	= null;
	File input                              	= null;
	//Logger logger                           	= null;

	public FareResponse ResponseReader(Document doc, FareResponse resobj )
	{
		System.out.println("==================================");
		System.out.println("FARE RESPONSE READING STARTED..!!");
		
		XMLError error = new XMLError();
		try
		{
			
			Thread.sleep(3000);
			error.setErrorMessage(doc.getElementsByTag("Error").text());
			error.setErrortype(doc.getElementsByTag("Error").attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").attr("Code"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		resobj.setError(error);
		if(resobj.getError().getErrorMessage().equals(""))
		{		
			try 
			{
				//logger.debug("Loading PriceItinerary elements to a list");
				
			    Elements nodes = doc.getElementsByTag("PricedItinerary");
				Iterator<Element>   it = nodes.iterator();
				int c = 0;
				while(it.hasNext())
				{
					Element ele = it.next();
					
					/*PRICEITINERARY*/
					//logger.info("Initializing PriceItinerary object");
					XMLPriceItinerary priceitobj = new XMLPriceItinerary();
					
					/*Total journey duration in PriceItinerary object*/
					//priceitobj.setJournyDuration(ele.getElementsByTag("JourneyTotalDuration").text());
					
					/*Total journey duration in PriceItinerary object*/
					priceitobj.setDirectiontype(ele.getElementsByTag("AirItinerary").attr("DirectionInd"));
					
					/*Ticket time limit info in PriceItinerary object*/
					priceitobj.setTicketTimeLimit(ele.getElementsByTag("TicketingInfo").attr("TicketTimeLimit"));
					
					/*Fill a Price object to put in to PriceItinerary object*/
					//logger.info("Initializing PriceItinerary Rate object");
					XMLPriceInfo priceinfo = new XMLPriceInfo();
					Costs costsInSupplierCurrency = new Costs();
					priceinfo.setPricingSource(ele.getElementsByTag("AirItineraryPricingInfo").attr("PricingSource"));
					
	
					String BaseFare = "";
					BaseFare = ele.getElementsByTag("BaseFare").attr("Amount");
					String decimal	= "0";
					decimal  = ele.getElementsByTag("BaseFare").attr("DecimalPlaces");
					//priceinfo.setBasefareAmountDecimal(decimal);
					costsInSupplierCurrency.setBasefareAmountDecimal(decimal);
					try {
						if(costsInSupplierCurrency.getBasefareAmountDecimal() != 0)
						{
							BaseFare = BaseFare.substring(0, BaseFare.length() - costsInSupplierCurrency.getBasefareAmountDecimal() ).concat(".").concat(BaseFare.substring(BaseFare.length() - costsInSupplierCurrency.getBasefareAmountDecimal(), BaseFare.length()));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					//priceinfo.setBasefareAmount(BaseFare);
					costsInSupplierCurrency.setBasefareAmount(BaseFare);
					
					//priceinfo.setBasefareCurrencyCode(ele.getElementsByTag("BaseFare").attr("CurrencyCode"));
					costsInSupplierCurrency.setBasefareCurrencyCode(ele.getElementsByTag("BaseFare").attr("CurrencyCode"));
					
					String Tax = "";
					Tax = ele.getElementsByTag("Tax").attr("Amount");
					String taxDecimal = "";
					taxDecimal = ele.getElementsByTag("Tax").attr("DecimalPlaces");
					//priceinfo.setTaxAmountDecimal(taxDecimal);
					costsInSupplierCurrency.setTaxAmountDecimal(taxDecimal);
					try {
						if(costsInSupplierCurrency.getTaxAmountDecimal() != 0)
						{
							Tax = Tax.substring(0, Tax.length() - costsInSupplierCurrency.getTaxAmountDecimal() ).concat(".").concat(Tax.substring(Tax.length() - costsInSupplierCurrency.getTaxAmountDecimal(), Tax.length()));
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					//priceinfo.setTaxAmount(Tax);
					costsInSupplierCurrency.setTaxAmount(Tax);
					
					priceinfo.setTaxCode(ele.getElementsByTag("Tax").attr("TaxCode"));
					
					//priceinfo.setTaxCurrencyCode(ele.getElementsByTag("Tax").attr("CurrencyCode"));
					costsInSupplierCurrency.setTaxCurrencyCode(ele.getElementsByTag("Tax").attr("CurrencyCode"));
					
					String Total = "";
					Total = ele.getElementsByTag("TotalFare").attr("Amount");
					String totDecimal = "";
					totDecimal = ele.getElementsByTag("TotalFare").attr("DecimalPlaces");
					
					//priceinfo.setTotalFareDecimal(totDecimal);
					costsInSupplierCurrency.setTotalFareDecimal(totDecimal);
					
					try {
						if(costsInSupplierCurrency.getTotalFareDecimal() != 0)
						{
							Total = Total.substring(0, Total.length() - costsInSupplierCurrency.getTotalFareDecimal() ).concat(".").concat(Total.substring(Total.length() - costsInSupplierCurrency.getTotalFareDecimal(), Total.length()));
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					//priceinfo.setTotalFare(Total);
					costsInSupplierCurrency.setTotalFare(Total);
					
					//priceinfo.setTotalFareCurrencyCode(ele.getElementsByTag("TotalFare").attr("CurrencyCode"));
					costsInSupplierCurrency.setTotalFareCurrencyCode(ele.getElementsByTag("TotalFare").attr("CurrencyCode"));
					
					
					/*Pass price info object to PriceItinerary object*/
					priceinfo.setInSupplierCurrency(costsInSupplierCurrency);
					priceitobj.setPricinginfo(priceinfo);
					//logger.info("Rate details inserted successfully");
					
					/*Find how many Origin Options are there to create OriginOption objects*/
					originoptions					= ele.getElementsByTag("OriginDestinationOption");
					Iterator<Element>   originiter	= originoptions.iterator();
					
					//logger.info("Initializing Origin option object");
					originobjlist					= new ArrayList<XMLOriginOptions>();
					
					while(originiter.hasNext())
					{
						//logger.info("Starting Origin option loop");
						/*Filling OriginOption object to pass to the PriceItinerary  object*/
						
						Element originele = originiter.next();
						int originoptcount = originoptions.size();
						priceitobj.setNoOforigins(originoptcount);
						
						
						XMLOriginOptions originobject = new XMLOriginOptions();
						
						originobject.setJourneyDuration(originele.getElementsByTag("JourneyTotalDuration").text().trim());
						
						/*Filling flight segments*/
						flightsegments = originele.getElementsByTag("FlightSegment");
						Iterator<Element> flightsegmentiter = flightsegments.iterator();
						
						//logger.info("Initializing flight object list");
						flightobjlist = new ArrayList<Flight>();
						
						while(flightsegmentiter.hasNext())
						{
							//logger.info("Starting Flight segment (flight object) loop ");
							Element flightele = flightsegmentiter.next();
							
							/*Creating flight objects*/
							Flight flightobject = new Flight();
							flightobject.setDepartureDate(flightele.getElementsByTag("FlightSegment").attr("DepartureDateTime").split("T")[0]);
							flightobject.setDepartureTime(flightele.getElementsByTag("FlightSegment").attr("DepartureDateTime").split("T")[1]);
							flightobject.setArrivalDate(flightele.getElementsByTag("FlightSegment").attr("ArrivalDateTime").split("T")[0]);
							flightobject.setArrivalTime(flightele.getElementsByTag("FlightSegment").attr("ArrivalDateTime").split("T")[1]);
							flightobject.setFlightNo(flightele.getElementsByTag("FlightSegment").attr("FlightNumber"));
							flightobject.setDeparture_port(flightele.getElementsByTag("DepartureAirport").text());
							flightobject.setDepartureLocationCode(flightele.getElementsByTag("DepartureAirport").attr("LocationCode"));						
							flightobject.setArrival_port(flightele.getElementsByTag("ArrivalAirport").text());
							flightobject.setArrivalLocationCode(flightele.getElementsByTag("ArrivalAirport").attr("LocationCode"));
							flightobject.setOperatingAirlineCode(flightele.getElementsByTag("OperatingAirline").attr("Code"));
							flightobject.setOperatingAirline(flightele.getElementsByTag("OperatingAirline").text());
							flightobject.setMarketingAirline(flightele.getElementsByTag("MarketingAirline").text());
							flightobject.setMarketingAirline_Loc_Code(flightele.getElementsByTag("MarketingAirline").attr("Code"));
							flightobject.setCabintype(flightele.getElementsByTag("CabinType").attr("Cabin"));
							
							//logger.info("Add object to the list");
							flightobjlist.add(flightobject);
							
						}
						
						//logger.info("Ends Flight segment (flight object) loop ");
						//logger.info("Set flight object list to OriginOtion object");
						originobject.setDeparturedate(flightobjlist.get(0).getDepartureDate());
						originobject.setArrivaldate(flightobjlist.get((flightobjlist.size()-1)).getDepartureDate());
						originobject.setFlightlist(flightobjlist);
						//logger.info("Add OriginOption object to OriginOptions list");
						originobjlist.add(originobject);
					}
					//logger.info("Set Originoptions list to PriceItinerary object");
					priceitobj.setOriginoptions(originobjlist);
					//logger.info("Add PriceItinerary object to PriceItinerary object list");
					priceiterlist.add(priceitobj);
	
					//int c = 0;
					c = c+1;
					System.out.println("Price Itinerary Objct "+c+" added");
				}
				
				resobj.setList(priceiterlist);
	          
				System.out.println("FARE RESPONSE XML READ SUCCESFULLY..!!!");
				System.out.println("=========================================");
			} 
			catch (Exception e) 
			{
				//logger.fatal("Error occurred while loading data to objects");
				e.printStackTrace();
			}
			
			
		}

	     return resobj;
	}
	
}
