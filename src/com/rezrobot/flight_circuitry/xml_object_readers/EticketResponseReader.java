package com.rezrobot.flight_circuitry.xml_object_readers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rezrobot.flight_circuitry.Eticket;
import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.xml_objects.EticketResponse;

public class EticketResponseReader 
{
	public EticketResponseReader(HashMap<String, String> Propmap)
	{  
		
	}
	
	public EticketResponse ResponseReader(Document doc, EticketResponse eticketres)
	{
		ArrayList<Eticket> eticketList = new ArrayList<Eticket>();
		
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		eticketres.setError(error);
		if(eticketres.getError().getErrorMessage().equals(""))
		{
			try
			{
				eticketres.setUniqueID(doc.getElementsByTag("UniqueID").attr("ID"));
				eticketres.setTicketingControlType(doc.getElementsByTag("TicketingControl").attr("Type"));
				
				Elements nodes = doc.getElementsByTag("Ticket");
				Iterator<Element> nodesiter = nodes.iterator();
				
				while(nodesiter.hasNext())
				{
					Element node = nodesiter.next();
					Eticket eticket = new Eticket();
					eticket.setType(node.getElementsByTag("Ticket").attr("Type"));
					eticket.setNumber(node.getElementsByTag("Ticket").attr("Number"));
					eticketres.setTicketType(node.getElementsByTag("Ticket").attr("Number"));
					eticketList.add(eticket);
				}
				eticketres.setEticketList(eticketList);
				
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		return eticketres;
	}
}
