package com.rezrobot.flight_circuitry.xml_object_readers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



//import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;

import com.rezrobot.flight_circuitry.Provider;
import com.rezrobot.flight_circuitry.xml_objects.EticketRequest;


public class EticketRequestReader 
{
	//Logger 							logger				= null;
	private HashMap<String, String> Propertymap 		= null;
	
	public EticketRequestReader(HashMap<String, String> Propmap)
	{
		
	}
	
	public EticketRequest RequestReader(Document doc, EticketRequest eticketReq)
	{
		
		try
		{
			eticketReq.setPseudoCityCode(doc.getElementsByTag("Source").attr("PseudoCityCode"));
			eticketReq.setRequestorID(doc.getElementsByTag("RequestorID ").attr("ID"));
			eticketReq.setRequestorIDType(doc.getElementsByTag("RequestorID ").attr("Type"));
			
			Provider provider = new Provider();
			provider.setName(doc.getElementsByTag("Name").text());
			provider.setSystem(doc.getElementsByTag("System").text());
			provider.setUserid(doc.getElementsByTag("Userid").text());
			provider.setPassword(doc.getElementsByTag("Password").text());
			eticketReq.setProvider(provider);
			
			eticketReq.setUniqueID(doc.getElementsByTag("UniqueID").attr("ID"));
			eticketReq.setTicketType(doc.getElementsByTag("Ticketing").attr("TicketType"));
			eticketReq.setOmitInfant(doc.getElementsByTag("Ticketing").attr("OmitInfant"));
			
			eticketReq.setNotificatioByEmail(doc.getElementsByTag("Notification").attr("ByEmail"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
															
		return eticketReq;
	}
	
}
