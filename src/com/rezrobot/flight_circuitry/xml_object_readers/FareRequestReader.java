/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_object_readers;


//import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

//import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rezrobot.flight_circuitry.xml_objects.FareRequest;


public class FareRequestReader 
{
	//Logger 							logger				= null;
	
	public FareRequest RequestReader(Document doc, FareRequest fareRequest)
	{
		System.out.println("================================");
		System.out.println("FARE REQUEST READING STARTED..!!");
		
		try
		{
			Thread.sleep(3000);
			fareRequest.setISOCurrency(doc.getElementsByTag("Source").attr("ISOCurrency"));
			fareRequest.setProvider(doc.getElementsByTag("Name").text());
			
			Elements nodes = doc.getElementsByTag("OriginDestinationInformation");
			
			Iterator<Element> iterator = nodes.iterator();
			
			String originDate = "";
			String origin = "";
			String destination = "";
			ArrayList<String> OriginDestinationInfo = new ArrayList<String>();
			
			while(iterator.hasNext())
			{
				//System.out.println("Iterator started origin destination");
				//logger.info("Starting Origin Destination departure iteration");
				Element ele = iterator.next();
				originDate = ele.getElementsByTag("DepartureDateTime").text();
				origin = ele.getElementsByTag("OriginLocation").attr("LocationCode");
				destination = ele.getElementsByTag("DestinationLocation").attr("LocationCode");
				
				String all = originDate.concat(",").concat(origin).concat(",").concat(destination);
				OriginDestinationInfo.add(all);
			}
			fareRequest.setOriginDestinationInfo(OriginDestinationInfo);
			fareRequest.setSeatsRequired(doc.getElementsByTag("SeatsRequested").text());
			fareRequest.setFareclass(doc.getElementsByTag("CabinPref").attr("Cabin"));
			fareRequest.setPrefFlight(doc.getElementsByTag("VendorPref").attr("Code"));
			fareRequest.setPricingSource(doc.getElementsByTag("PriceRequestInformation").attr("PricingSource"));
			try {
				fareRequest.setNegoCode(doc.getElementsByTag("NegotiatedFareCode").attr("Code"));
			} catch (Exception e) {
				fareRequest.setNegoCode("Not Applicable");
			}
			
			Elements elements = doc.getElementsByTag("PassengerTypeQuantity");
			
			Iterator<Element> iter = elements.iterator();
			
			HashMap<String, String> AirTravelers = new HashMap<String, String>();
			
			while(iter.hasNext())
			{
				System.out.println("passenger reading started..!!");
				Element ele = iter.next();
				String Code = ele.getElementsByTag("PassengerTypeQuantity").attr("Code");
				String Quantity = ele.getElementsByTag("PassengerTypeQuantity").attr("Quantity");
				AirTravelers.put(Code, Quantity);
			}
			
			fareRequest.setAirTravelers(AirTravelers);
			
			fareRequest.getAll();
			System.out.println("FARE REQUEST XML READ SUCCESFULLY..!!!");
			System.out.println("=======================================");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return fareRequest;
	}


}