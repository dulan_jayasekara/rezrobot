package com.rezrobot.flight_circuitry.xml_object_readers;

import java.util.HashMap;



//import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;

import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.xml_objects.CancellationRequest;

public class CancellationRequestReader
{
	//Logger 							logger				= null;
	
	public CancellationRequestReader(HashMap<String, String> Propmap)
	{
		
	}
	
	public CancellationRequest RequestReader(Document doc, CancellationRequest cancellationRequest)
	{
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
			error.setErrortype(doc.getElementsByTag("Error").attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").attr("Code"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		cancellationRequest.setError(error);
		
		if(cancellationRequest.getError().getErrorMessage().equals(""))
		{
			try {
							
				String general = "";
				
				general = doc.getElementsByTag("OTA_CancelRQ").attr("EchoToken");
				cancellationRequest.setEchoToken(general);
				
				general = doc.getElementsByTag("UniqueID").attr("ID");
				cancellationRequest.setUniqueID(general);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return cancellationRequest;
	}
}
