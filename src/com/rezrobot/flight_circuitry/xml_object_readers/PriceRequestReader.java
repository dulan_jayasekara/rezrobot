/*Sanoj*/
package com.rezrobot.flight_circuitry.xml_object_readers;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;



//import org.apache.log4j.Logger;
//import org.apache.log4j.xml.DOMConfigurator;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rezrobot.flight_circuitry.Flight;
import com.rezrobot.flight_circuitry.XMLOriginOptions;
import com.rezrobot.flight_circuitry.xml_objects.PriceRequest;


public class PriceRequestReader {
	
	//Logger 							logger				= null;
	/*String						propfilepath		= "E:\\workspace\\ReadFlightXML\\Config.properties";*/
	File							input				= null;
	ArrayList<XMLOriginOptions> 	AirItinerary		= new ArrayList<XMLOriginOptions>();
	Map<String, String> 			AirTravelers 		= new HashMap<String, String>();
	ArrayList<PriceRequest>			priceReqList		= new ArrayList<PriceRequest>();
	
	public PriceRequest RequestReader(Document doc, PriceRequest sabreReqPrice)
	{
		try
		{
			//logger.debug("Loading Elements to a list");
			
			sabreReqPrice.setISOCurrency(doc.getElementsByTag("Source").attr("ISOCurrency"));
			sabreReqPrice.setProvider(doc.getElementsByTag("Name").text());
			
			Elements nodes = doc.getElementsByTag("OriginDestinationOption");
			
			Iterator<Element> origiterator = nodes.iterator();
			//System.out.println("Origin Elements size : "+nodes.size());
			
			while(origiterator.hasNext())
			{
				XMLOriginOptions originoption = new XMLOriginOptions();
				Element originele = origiterator.next();
				
				Elements flightsegments = originele.getElementsByTag("FlightSegment");
				Iterator<Element> flightsegmentiter = flightsegments.iterator();
				System.out.println("Flight segments : "+flightsegments.size());
				
				//logger.info("Initializing flight object list");
				ArrayList<Flight> flightobjlist = new ArrayList<Flight>();
				
				while(flightsegmentiter.hasNext())
				{
					//logger.info("Starting Flight segment (flight object) loop ");
					Element flightele = flightsegmentiter.next();
					
					/*Creating flight objects*/
					Flight flightobject = new Flight();
					flightobject.setFlightNo(flightele.getElementsByTag("FlightSegment").attr("FlightNumber"));
					flightobject.setxmlArrivalDate(flightele.getElementsByTag("FlightSegment").attr("ArrivalDateTime"));
					flightobject.setxmlDepartureDate(flightele.getElementsByTag("FlightSegment").attr("DepartureDateTime"));
					flightobject.setDepartureLocationCode(flightele.getElementsByTag("DepartureAireport").attr("LocationCode"));
					flightobject.setArrivalLocationCode(flightele.getElementsByTag("ArrivalAirport").attr("LocationCode"));
					flightobject.setMarketingAirline_Loc_Code(flightele.getElementsByTag("MarketingAirLine").attr("Code"));
					
					flightobjlist.add(flightobject);	
				}
				originoption.setFlightlist(flightobjlist);
				AirItinerary.add(originoption);
			}
			
			sabreReqPrice.setSeatRequested(doc.getElementsByTag("SeatsRequested").text());
			
			Elements airtraveleravail = doc.getElementsByTag("PassengerTypeQuantity");
			Iterator<Element> passengersele = airtraveleravail.iterator();
			
			while(passengersele.hasNext())
			{
				Element e = passengersele.next();
				AirTravelers.put(e.getElementsByTag("PassengerTypeQuantity").attr("Code"), e.getElementsByTag("PassengerTypeQuantity").attr("Quantity"));
				System.out.println(AirTravelers);
			}
			sabreReqPrice.setAirTravelers(AirTravelers);
			sabreReqPrice.setPricingSource(doc.getElementsByTag("PriceRequestInformation").attr("PricingSource"));
			try {
				sabreReqPrice.setNegoCode(doc.getElementsByTag("NegotiatedFareCode").attr("Code"));
			} catch (Exception e) {
				sabreReqPrice.setNegoCode("Not Applicable");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return sabreReqPrice;
	}

}
