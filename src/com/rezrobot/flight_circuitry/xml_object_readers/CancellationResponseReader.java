package com.rezrobot.flight_circuitry.xml_object_readers;


import java.util.HashMap;

import org.jsoup.nodes.Document;

import com.rezrobot.flight_circuitry.XMLError;
import com.rezrobot.flight_circuitry.xml_objects.CancellationResponse;



public class CancellationResponseReader
{
	//Logger 							logger				= null;
		
	public CancellationResponseReader(HashMap<String, String> Propmap)
	{
		
	}
		
	public CancellationResponse RequestReader(Document doc, CancellationResponse cancellationResponse)
	{
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
			error.setErrortype(doc.getElementsByTag("Error").attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").attr("Code"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			
		cancellationResponse.setError(error);
		
		if(cancellationResponse.getError().getErrorMessage().equals(""))
		{
			try {
								
				String general = "";
				
				
				//general = list.get(0).attr("Status");
				//org.jsoup.select.Elements ele = doc.getElementsByAttributeValue("Status", "Cancelled");
				//general = ele.get(0).attr("Status");
				general = doc.getElementsByTag("OTA_CancelRS").attr("Status");
				cancellationResponse.setStatus(general);
					
				general = doc.getElementsByTag("UniqueID").attr("ID");
				cancellationResponse.setUniqueID(general);
					
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return cancellationResponse;
	}
}
