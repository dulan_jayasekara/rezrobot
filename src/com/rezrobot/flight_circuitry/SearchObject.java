/*Sanoj*/
package com.rezrobot.flight_circuitry;

import java.io.Serializable;
import java.util.ArrayList;

import com.rezrobot.enumtypes.PaymentMode;

//import system.enumtypes.TO;

public class SearchObject  implements Serializable {
	
	String				bookingChannel				= "";
	String				SellingCurrency 			= "";
	String				Country						= "";
	String				Triptype					= "";
	String				From						= "";
	String				To							= "";
	String				DepartureDate				= "";
	String				DepartureTime				= "";
	String				ReturnDate					= "";
	String				ReturnTime					= "";
	boolean				Flexible					= false;
	String				Adult						= "";
	String				Children					= "";
	ArrayList<String>	ChildrenAge					= null;
	String				infant						= "";
	String				CabinClass					= "";
	String				PreferredCurrency			= "";
	String				PreferredAirline			= "";
	boolean				NonStop						= false;
	String				tracer						= "";	
	String				ProfitType					= "";
	double				Profit						= 0;
	String				selectingFlight				= "";
	String				BookingFeeType				= "";
	double				BookingFee					= 0;
	String				ccCostType					= "";
	double				ccFee						= 0;
	boolean				ExcecuteStatus				= false;
	PaymentMode			paymentMode					= null;
	String				CancellationStatus			= "";
	boolean				SupplierPayablePayStatus	= false;
	boolean				airConfigurationStatus		= false;
	String				promotionCode				= "";
	boolean				removecartStatus			= false;

	boolean				Quotation					= false;
	boolean				searchAgain					= false;
	boolean				validateFilters				= false;
	String				TOBooking					= "";
	//TO					ToType						= null ;				
	boolean				applyDiscount				= false;
	boolean				applyDiscountAtPayPg		= false;
	String				scenario					= "null";
	String				TOCashCredit				= "";
	String				DiscountCouponNo			= "";
	boolean				discountAtSearchAgain		= false;
	AirConfig			airConfigObject				= new AirConfig();
	String				selectingFlightType			= "";
	
	
	
	
	public String getSelectingFlightType() {
		return selectingFlightType;
	}

	public void setSelectingFlightType(String selectingFlightType) {
		this.selectingFlightType = selectingFlightType;
	}

	public AirConfig getAirConfigObject() {
		return airConfigObject;
	}

	public void setAirConfigObject(AirConfig airConfigObject) {
		this.airConfigObject = airConfigObject;
	}

	public String getCcCostType() {
		return ccCostType;
	}

	public void setCcCostType(String ccCostType) {
		this.ccCostType = ccCostType;
	}

	public double getCcFee() {
		return ccFee;
	}

	public void setCcFee(double ccFee) {
		this.ccFee = ccFee;
	}

	public String getBookingChannel() {
		return bookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}

	public boolean isDiscountAtSearchAgain() {
		return discountAtSearchAgain;
	}

	public void setDiscountAtSearchAgain(boolean discountAtSearchAgain) {
		this.discountAtSearchAgain = discountAtSearchAgain;
	}

	public SearchObject(String departureDate, String returnDate) {
		super();
		DepartureDate = departureDate;
		ReturnDate = returnDate;
		bookingChannel = "Web";
		SellingCurrency = "USD";
		Country = "USA";
		Triptype = "Round Trip";
		From = "London|LHR|1112|Heathrow|-|54||United Kingdom";
		To = "Muscat|MCT|872|Muscat International Airport|-|231||Oman";
		DepartureTime = "AnyTime";
		ReturnTime = "AnyTime";
		Flexible = false;
		Adult = "2";
		Children = "1";
		ChildrenAge = new ArrayList<>();
		ChildrenAge.add("5");
		this.infant = "1";
		CabinClass = "Economy";
		PreferredCurrency = "Select Currency";
		PreferredAirline = "NONE";
		NonStop = false;
		this.tracer = "";
		ProfitType = "PERCENTAGE";
		Profit = 10.0;
		this.selectingFlight = "3";
		BookingFeeType = "VALUE";
		BookingFee = 0.0;
		ExcecuteStatus = true;
		this.paymentMode = PaymentMode.Online;
		CancellationStatus = "YES";
		SupplierPayablePayStatus = false;
		this.airConfigurationStatus = false;
		this.promotionCode = "";
		this.removecartStatus = false;
		Quotation = false;
		this.searchAgain = false;
		this.validateFilters = false;
		TOBooking = "NO";
		this.applyDiscount = false;
		this.applyDiscountAtPayPg = false;
		this.scenario = "Test1";
		//TOCashCredit = tOCashCredit;
		DiscountCouponNo = "";
		discountAtSearchAgain		= false;
	}

	public SearchObject()
	{
		bookingChannel = "Web";
		SellingCurrency = "USD";
		Country = "USA";
		Triptype = "Round Trip";
		From = "London|LHR|1112|Heathrow|-|54||United Kingdom";
		To = "Muscat|MCT|872|Muscat International Airport|-|231||Oman";
		DepartureDate = "08/10/2016";
		DepartureTime = "AnyTime";
		ReturnDate = "08/14/2016";
		ReturnTime = "AnyTime";
		Flexible = false;
		Adult = "2";
		Children = "1";
		ChildrenAge = new ArrayList<>();
		ChildrenAge.add("5");
		this.infant = "1";
		CabinClass = "Economy";
		PreferredCurrency = "Select Currency";
		PreferredAirline = "NONE";
		NonStop = false;
		this.tracer = "";
		ProfitType = "PERCENTAGE";
		Profit = 10.0;
		this.selectingFlight = "3";
		BookingFeeType = "VALUE";
		BookingFee = 0.0;
		ExcecuteStatus = true;
		this.paymentMode = PaymentMode.Online;
		CancellationStatus = "YES";
		SupplierPayablePayStatus = false;
		this.airConfigurationStatus = false;
		this.promotionCode = "";
		this.removecartStatus = false;
		Quotation = false;
		this.searchAgain = false;
		this.validateFilters = false;
		TOBooking = "NO";
		this.applyDiscount = false;
		this.applyDiscountAtPayPg = false;
		this.scenario = "Test1";
		//TOCashCredit = tOCashCredit;
		DiscountCouponNo = "";
		discountAtSearchAgain		= false;
	}
	
	public SearchObject(String searchType, String sellingCurrency,
			String country, String triptype, String from, String to,
			String departureDate, String departureTime, String returnDate,
			String returnTime, boolean flexible, String adult, String children,
			ArrayList<String> childrenAge, String infant, String cabinClass,
			String preferredCurrency, String preferredAirline, boolean nonStop,
			String tracer, String profitType, double profit,
			String selectingFlight, String bookingFeeType, double bookingFee,
			boolean excecuteStatus, String paymentMode,
			String cancellationStatus, boolean supplierPayablePayStatus,
			boolean airConfigurationStatus, String promotionCode,
			boolean removecartStatus, boolean quotation, boolean searchAgain,
			boolean validateFilters, String tOBooking, boolean applyDiscount,
			boolean applyDiscountAtPayPg, String scenario,/* String tOCashCredit,*/
			String discountCouponNo, boolean dicountAtSearchAgain) {
		super();
		bookingChannel = searchType;
		SellingCurrency = sellingCurrency;
		Country = country;
		Triptype = triptype;
		From = from;
		To = to;
		DepartureDate = departureDate;
		DepartureTime = departureTime;
		ReturnDate = returnDate;
		ReturnTime = returnTime;
		Flexible = flexible;
		Adult = adult;
		Children = children;
		ChildrenAge = childrenAge;
		this.infant = infant;
		CabinClass = cabinClass;
		PreferredCurrency = preferredCurrency;
		PreferredAirline = preferredAirline;
		NonStop = nonStop;
		this.tracer = tracer;
		ProfitType = profitType;
		Profit = profit;
		this.selectingFlight = selectingFlight;
		BookingFeeType = bookingFeeType;
		BookingFee = bookingFee;
		ExcecuteStatus = excecuteStatus;
		this.paymentMode = PaymentMode.Online;
		CancellationStatus = cancellationStatus;
		SupplierPayablePayStatus = supplierPayablePayStatus;
		this.airConfigurationStatus = airConfigurationStatus;
		this.promotionCode = promotionCode;
		this.removecartStatus = removecartStatus;
		Quotation = quotation;
		this.searchAgain = searchAgain;
		this.validateFilters = validateFilters;
		TOBooking = tOBooking;
		this.applyDiscount = applyDiscount;
		this.applyDiscountAtPayPg = applyDiscountAtPayPg;
		this.scenario = scenario;
		//TOCashCredit = tOCashCredit;
		DiscountCouponNo = discountCouponNo;
		this.discountAtSearchAgain = dicountAtSearchAgain;
	}
	
	public String getDiscountCouponNo() {
		return DiscountCouponNo;
	}
	
	public void setDiscountCouponNo(String discountCouponNo) {
		DiscountCouponNo = discountCouponNo;
	}
	
	public String getTOCashCredit() {
		return TOCashCredit;
	}
	public void setTOCashCredit(String tOCashCredit) {
		TOCashCredit = tOCashCredit;
	}
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	/*public TO getToType() {
		return ToType;
	}
	public void setToType(TO toType) {
		ToType = toType;
	}*/
	public boolean isApplyDiscountAtPayPg() {
		return applyDiscountAtPayPg;
	}
	public void setApplyDiscountAtPayPg(boolean applyDiscountAtPayPg) {
		this.applyDiscountAtPayPg = applyDiscountAtPayPg;
	}
	public boolean isApplyDiscount() {
		return applyDiscount;
	}
	public void setApplyDiscount(boolean applyDiscount) {
		this.applyDiscount = applyDiscount;
	}
	public String getTOBooking() {
		return TOBooking;
	}
	public void setTOBooking(String tOBooking) {
		TOBooking = tOBooking;
	}
	public boolean isValidateFilters() {
		return validateFilters;
	}
	public void setValidateFilters(boolean validateFilters) {
		this.validateFilters = validateFilters;
	}
	public boolean isSearchAgain() {
		return searchAgain;
	}
	public void setSearchAgain(boolean searchAgain) {
		this.searchAgain = searchAgain;
	}
	public boolean isQuotation() {
		return Quotation;
	}
	public void setQuotation(boolean quotation) {
		Quotation = quotation;
	}
	public boolean isRemovecartStatus() {
		return removecartStatus;
	}
	public void setRemovecartStatus(boolean Status) {
		removecartStatus = Status;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public boolean getAirConfigurationStatus() {
		return airConfigurationStatus;
	}
	public void setAirConfigurationStatus(boolean airConfigurationStatus) {
		this.airConfigurationStatus = airConfigurationStatus;
	}
	public boolean getSupplierPayablePayStatus() {
		return SupplierPayablePayStatus;
	}
	public void setSupplierPayablePayStatus(boolean supplierPayablePayStatus) {
		SupplierPayablePayStatus = supplierPayablePayStatus;
	}
	public String getCancellationStatus() {
		return CancellationStatus;
	}
	public void setCancellationStatus(String cancellationStatus) {
		CancellationStatus = cancellationStatus;
	}
	public PaymentMode getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}
	public boolean getExcecuteStatus() {
		return ExcecuteStatus;
	}
	public void setExcecuteStatus(boolean excecuteStatus) {
		ExcecuteStatus = excecuteStatus;
	}
	public String getBookingFeeType() {
		return BookingFeeType;
	}
	public void setBookingFeeType(String bookingFeeType) {
		BookingFeeType = bookingFeeType;
	}
	public double getBookingFee() {
		return BookingFee;
	}
	public void setBookingFee(double bookingFee) {
		BookingFee = bookingFee;
	}

	public String getSelectingFlight() {
		return selectingFlight;
	}
	public void setSelectingFlight(String selectingFlight) {
		this.selectingFlight = selectingFlight;
	}
	public String getProfitType() {
		return ProfitType;
	}
	public void setProfitType(String profitType) {
		ProfitType = profitType;
	}
	public double getProfit() {
		return Profit;
	}
	public void setProfit(double profit) {
		Profit = profit;
	}

	public String getTracer() {
		return tracer;
	}
	public void setTracer(String tracer) {
		this.tracer = tracer;
	}
	public String getbookingChannel() {
		return bookingChannel;
	}
	public void setbookingChannel(String searchType) {
		bookingChannel = searchType;
	}
	public String getSellingCurrency() {
		return SellingCurrency;
	}
	public void setSellingCurrency(String sellingCurrency) {
		SellingCurrency = sellingCurrency;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getTriptype() {
		return Triptype;
	}
	public void setTriptype(String triptype) {
		Triptype = triptype;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getDepartureDate() {
		return DepartureDate;
	}
	public void setDepartureDate(String departureDate) {
		DepartureDate = departureDate;
	}
	public String getDepartureTime() {
		return DepartureTime;
	}
	public void setDepartureTime(String departureTime) {
		DepartureTime = departureTime;
	}
	public String getReturnDate() {
		return ReturnDate;
	}
	public void setReturnDate(String returnDate) {
		ReturnDate = returnDate;
	}
	public String getReturnTime() {
		return ReturnTime;
	}
	public void setReturnTime(String returnTime) {
		ReturnTime = returnTime;
	}
	public boolean isFlexible() {
		return Flexible;
	}
	public void setFlexible(boolean flexible) {
		Flexible = flexible;
	}
	public String getAdult() {
		return Adult;
	}
	public void setAdult(String adult) {
		Adult = adult;
	}
	public String getChildren() {
		return Children;
	}
	public void setChildren(String children) {
		Children = children;
	}
	public ArrayList<String> getChildrenAge() {
		return ChildrenAge;
	}
	public void setChildrenAge(ArrayList<String> childrenAge) {
		ChildrenAge = childrenAge;
	}
	public String getInfant() {
		return infant;
	}
	public void setInfant(String infant) {
		this.infant = infant;
	}
	public String getCabinClass() {
		return CabinClass;
	}
	public void setCabinClass(String cabinClass) {
		CabinClass = cabinClass;
	}
	public String getPreferredCurrency() {
		return PreferredCurrency;
	}
	public void setPreferredCurrency(String preferredCurrency) {
		PreferredCurrency = preferredCurrency;
	}
	public String getPreferredAirline() {
		return PreferredAirline;
	}
	public void setPreferredAirline(String preferredAirline) {
		PreferredAirline = preferredAirline;
	}
	public boolean isNonStop() {
		return NonStop;
	}
	public void setNonStop(boolean nonStop) {
		NonStop = nonStop;
	}
	
	public void setDummyValues()
	{
		bookingChannel				= "";
		SellingCurrency 			= "";
		Country						= "";
		Triptype					= "";
		From						= "";
		To							= "";
		DepartureDate				= "";
		DepartureTime				= "";
		ReturnDate					= "";
		ReturnTime					= "";
		Flexible					= false;
		Adult						= "";
		Children					= "";
		ChildrenAge					= null;
		infant						= "";
		CabinClass					= "";
		PreferredCurrency			= "";
		PreferredAirline			= "";
		NonStop						= false;
		tracer						= "";	
		ProfitType					= "";
		Profit						= 0;
		selectingFlight				= "";
		BookingFeeType				= "";
		BookingFee					= 0;
		ExcecuteStatus				= true;
		paymentMode					= PaymentMode.Online;
		CancellationStatus			= "";
		SupplierPayablePayStatus	= false;
		airConfigurationStatus		= false;
		promotionCode				= "";
		removecartStatus			= false;
		Quotation					= false;
		searchAgain					= false;
		validateFilters				= false;
		TOBooking					= "";
		//TO					ToType						= null ;				
		applyDiscount				= false;
		applyDiscountAtPayPg		= false;
		scenario					= "null";
		TOCashCredit				= "";
	}
}
