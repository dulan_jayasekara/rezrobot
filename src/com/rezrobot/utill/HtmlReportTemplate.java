package com.rezrobot.utill;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import bsh.This;
//import java.util.HashMap;

//import org.openqa.jetty.html.Break;


public class HtmlReportTemplate {
	
	private StringBuffer    PrintWriter         = null;
	private static int      TestCaseCount;
	//private HashMap<String, String>   PropertySet = null;
	    
	public void Initialize(StringBuffer printer)
	{
		this.PrintWriter = printer;
	    // TestCaseCount    = ReservationScenarioRunner.TestCaseCount;
	}
	
	private static class SingletonHolder { 
	    private static final HtmlReportTemplate INSTANCE = new HtmlReportTemplate();
	}
	
	public static HtmlReportTemplate getInstance() {
	    return SingletonHolder.INSTANCE;
	}
	
	
	  
	public void verifyTrue(String Expected,String Actual,String Message) {
		  
	  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			
			if(Actual.trim().equalsIgnoreCase(Expected.trim()))
				PrintWriter.append("<td class='Passed'>PASS</td>");
		else
		    PrintWriter.append("<td class='Failed'>FAIL</td>");
		
		  PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	  }
	
	public void verifyArrayList(ArrayList<String> Expected, ArrayList<String> Actual, String Message)
	{
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		
		try 
		{
			if(Expected.containsAll(Actual) || Actual.containsAll(Expected))
				PrintWriter.append("<td class='Passed'>PASS</td>");
			else
		    PrintWriter.append("<td class='Failed'>FAIL</td>");
		
		  PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}
	
	public void verifyContains(String Expected,String Actual,String Message){
	  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			
			if((Actual.trim().contains(Expected.trim()))||(Expected.trim().contains(Actual.trim())))
				PrintWriter.append("<td class='Passed'>PASS</td>");
		else
		    PrintWriter.append("<td class='Failed'>FAIL</td>");
		
		  PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}
	
	public void verifyEqualIgnoreCase(String Expected,String Actual,String Message){
	  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			
			if((Actual.trim().equalsIgnoreCase(Expected.trim()))||(Expected.trim().equalsIgnoreCase(Actual.trim())))
				PrintWriter.append("<td class='Passed'>PASS</td>");
		else
		    PrintWriter.append("<td class='Failed'>FAIL</td>");
		
		  PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}
	
	public void verifyTrue(double Expected,double Actual,String Message){
		
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			if(Actual == Expected)
				PrintWriter.append("<td class='Passed'>PASS</td>");
		else
		    PrintWriter.append("<td class='Failed'>FAIL</td>");
		
		  PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}
	
	public void verifyTrue(boolean Expected,boolean Actual,String Message)
	  {
	  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			if(Actual == Expected)
				PrintWriter.append("<td class='Passed'>PASS</td>");
		else
		    PrintWriter.append("<td class='Failed'>FAIL</td>");
		
		  PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}
	
	public void setTableHeading(String TableHeader){
		  
		PrintWriter.append("<br>");
		PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
		PrintWriter.append("<table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			  
	}
	  
	public void markTableEnd(){
		  
		PrintWriter.append("</table>");
		  
	}
	  
	public void markReportBegining(String reportHeadding){
		  
	    PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
	    PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>"+reportHeadding+"(" + Calender.getDate(Calendar.DATE, 0) + ")</p></div>");
	    PrintWriter.append("<body>");
		  
	}
	  
	public void setInfoTableHeading(String TableHeader){
		  
		PrintWriter.append("<br>");
		PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
		PrintWriter.append("<table ><tr><th>Attribute</th><th>Parameter</th></tr>");
		  
	}
	  
	public void setInfoTabel(String TableHeader, Map<String, String> sysInfo){
	
		Iterator<Map.Entry<String, String>>   itr = sysInfo.entrySet().iterator();
        
		setInfoTableHeading(TableHeader);
		
        while (itr.hasNext()) {
            Map.Entry<String,String> entry = (Map.Entry<String,String>) itr.next();
            
            PrintWriter.append("<tr><td>"+entry.getKey()+"</th><td>"+entry.getValue()+"</th></tr>");
            
        }
        
        markTableEnd();
		
		  
	}

}
