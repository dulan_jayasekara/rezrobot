package com.rezrobot.utill;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;

import com.rezrobot.common.objects.Reservation;
import com.rezrobot.flight_circuitry.FlightReservation;



public class Serializer {

	public static void SerializeReservation(Reservation reservation, String portal, String localPath) {
		
		String writePath = "";
		String datePath = Calender.getDate(Calendar.DATE, 0); 
		writePath = localPath+"/"+datePath+"/"+portal+"/";
		boolean isConfirmed = false;
		String resNo = "";
		
		try {
		
			if(reservation.getProduct().trim().equalsIgnoreCase("Flight"))
			{
				writePath = writePath.concat("Flight/");
				isConfirmed = reservation.getFlightReservation().isConfirmed();
				resNo = reservation.getFlightReservation().getReservationNo();
				//System.out.println(writePath);
			}
			if(reservation.getProduct().trim().equalsIgnoreCase("Hotel"))
			{
				writePath = writePath.concat("Hotel/");
				isConfirmed = reservation.getHotelReservation().isConfirmed();
				resNo = reservation.getHotelReservation().getConfirmation().getReservationNumber();
			}
			if(reservation.getProduct().trim().equalsIgnoreCase("vacation"))
			{
				writePath = writePath.concat("Vacation/");
				isConfirmed = reservation.getVacationReservation().isConfirmed();
				resNo = reservation.getVacationReservation().getConfirmation().get("reservationNumber");
			}
			if(reservation.getProduct().trim().equalsIgnoreCase("Activity"))
			{
				writePath = writePath.concat("Activity/");
				isConfirmed = reservation.getActivityReservation().isBookingConfirmed();
				resNo = reservation.getActivityReservation().getReservationNo();
			}
			if(reservation.getProduct().equalsIgnoreCase("Car"))
			{
				writePath = writePath.concat("Car/");
			}
			if(reservation.getProduct().equalsIgnoreCase("Transfer"))
			{
				writePath = writePath.concat("Transfer/");
			}
			if(reservation.getProduct().equalsIgnoreCase("Fix_Pacakge"))
			{
				writePath = writePath.concat("Fix_Pacakge/");
			}
			
			if(isConfirmed)
			{
				writePath = writePath.concat("Confirmed_Reservations/");
			}
			else
			{
				writePath = writePath.concat("Failed_Reservations/");
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			writePath = writePath.concat("Failed_Reservations/");
		}
		
		
		File dir = new File(writePath);
		dir.mkdirs();
		
		writePath = writePath.concat(resNo);
		
		serialize(reservation, writePath);
		
	}
	
	private static void serialize(Reservation reservation, String writePath)
	{
		try {
			// write object to file
			System.out.println(writePath);
			FileOutputStream fos = new FileOutputStream(new File(writePath+".ser"));
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(reservation);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Reservation DeSerializeReservation(String dir, String portal) {
		
		Reservation result = null;
		
		File latest = lastFileModified(dir, portal);
		try {
			
			// read object from file
			FileInputStream fis = new FileInputStream(latest);
			ObjectInputStream ois = new ObjectInputStream(fis);
			result = (Reservation) ois.readObject();
			ois.close();
			
			//System.out.println("Portal: " + result.getProduct() + "\nResNo : " + result.getFlightReservation().getReservationNo());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		Reservation res = new Reservation();
		FlightReservation fres = new FlightReservation();
		res.setProduct("Flight");
		fres.setReservationNo("FFa12314d0G");
		fres.setConfirmed(true);
		res.setFlightReservation(fres);
		Serializer ser = new Serializer();
		ser.SerializeReservation(res, "RezPack", "../");
		//File latest = lastFileModified("../23-Sep-2016/RezPack/Flight/Confirmed_Reservations/");
		//ser.DeSerializeReservation(latest);
	}
	
	public static File lastFileModified(String dir, String portal) {
		String dir2 = dir;
		String date = "";
		File choice = null;
		for(int i = 0 ; i < 5 ; i++)
		{
			if(i == 0)
			{
				date = Calender.getToday("dd-MMM-yyyy");
			}
			else
			{
				date = Calender.changeToday("dd-MMM-yyyy", -i);
			}
			dir2 = dir.replace("portal", portal).replace("date", date);
			try {
				File fl = new File(dir2);
			    File[] files = fl.listFiles(new FileFilter() 
			    {          
			        public boolean accept(File file)
			        {
			            return file.isFile();
			        }
			    }
			    );
			    
			   // System.out.println(files.length);
			    
			    long lastMod = Long.MIN_VALUE;  
			    for (File file : files) {
			        if (file.lastModified() > lastMod) {
			            choice = file;
			            lastMod = file.lastModified();
			        }
			    }
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	    System.out.println(choice.getName());
	    return choice;
	}
	
}
