package com.rezrobot.utill;

import java.io.File;
import java.io.IOException;
import java.util.Date;
//import com.rezrobot.flightCircuitry.Reservation;


import com.rezrobot.flight_circuitry.FlightReservation;

import jxl.*;
import jxl.write.*;
import jxl.write.Boolean;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;

public class ExcelWriter {

	
	public static void init(FlightReservation reservation)
	{
		try {
			
	        File exlFile = new File("E:/write_test.xls");
	        WritableWorkbook writableWorkbook = Workbook.createWorkbook(exlFile);

	        WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        
	        Label label1 = new Label(0, 0, "ReservationNo");
	        Label label2 = new Label(1, 0, "SuppConfNo");
	        Label label3 = new Label(2, 0, "Booking Date");
	        Label label4 = new Label(3, 0, "Confirmed Status");
	        Label label5 = new Label(4, 0, "Total Cost");
	        
	        Label label6 = new Label(0, 1, reservation.getReservationNo());
	        Label label7 = new Label(1, 1, reservation.getSupplierConfirmationNo());
	        DateTime date = new DateTime(2, 1, new Date());
	        Boolean bool = new Boolean(3, 1, true);
	        Number num = new Number(4, 1, 9.99);
	        
	        /*WritableCell c = writableSheet.getWritableCell(0,0);
	        WritableCellFormat newFormat = new WritableCellFormat();
	        newFormat.setBackground(Colour.BLACK);

	        c.setCellFormat(newFormat);*/

	        //Add Cells to the sheet
	        writableSheet.addCell(label1);
	        writableSheet.addCell(label2);
	        writableSheet.addCell(label3);
	        writableSheet.addCell(label4);
	        writableSheet.addCell(label5);
	        writableSheet.addCell(label6);
	        writableSheet.addCell(label7);
	        writableSheet.addCell(date);
	        writableSheet.addCell(bool);
	        writableSheet.addCell(num);
	        
	        //write
	        writableWorkbook.write();
	        writableWorkbook.close();

	    } catch (IOException e1) {
	        e1.printStackTrace();
	    } catch (RowsExceededException e2) {
	        e2.printStackTrace();
	    } catch (WriteException e3) {
	        e3.printStackTrace();
	    }
	}
	
	public static void main(String[] args) {
		
		FlightReservation reservation = new FlightReservation();
		reservation.setReservationNo("F09377293");
		reservation.setSupplierConfirmationNo("SDLKSH");
		ExcelWriter.init(reservation);
	}
	
}
