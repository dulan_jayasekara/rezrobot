package com.rezrobot.utill;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.rezrobot.core.PageObject;

public class xmlPathGenerator {
	
	public String returnUrl(HashMap<String, String> xmlDetails, String supplier, String xmltype, String tracerID, String productType) throws IOException, InterruptedException
	{
 		String url 		= "";
		if(productType.equals("hotel"))
		{
			String link 	= this.getLink(xmlDetails, supplier, xmltype, tracerID);
			Thread.sleep(1000);
			String Postfix  = "%2F"+xmlDetails.get("test.environment")+"%2F"+xmlDetails.get("Portal.Name");
			Thread.sleep(1000);
			String homeUrl = "";
			if(xmlDetails.get("numOfApps").equals("1"))
			{
				homeUrl 	= this.getHomeUrl(xmlDetails.get("hotelXmlHomeUrl1") + "/jsp/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F",Postfix);
				Thread.sleep(1000);
				url 			= this.urlCreator(link, xmlDetails.get("hotelXmlHomeUrl1") + "/jsp/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F".replace("dir", "file"), Postfix.concat("%2F"), homeUrl);
			}
			else
			{
				for(int i = 1  ; i <= Integer.parseInt(xmlDetails.get("numOfApps")) ; i++)
				{
					try {
						homeUrl 	= this.getHomeUrl(xmlDetails.get("hotelXmlHomeUrl1").replace("app1", "app" + String.valueOf(i)) + "/jsp/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F",Postfix);
						Thread.sleep(1000);
						url 			= this.urlCreator(link, xmlDetails.get("hotelXmlHomeUrl1") + "/jsp/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F".replace("dir", "file"), Postfix.concat("%2F"), homeUrl);
						if(url.equals(null) || url.isEmpty() || url.equals(""))
						{
							
						}
						else
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
			
			
		}
		//System.out.println(url);
		return url;
	}
	
	public String getLink(HashMap<String, String> file, String supplier, String xmlType, String tracerID)
	{
		String portal = file.get("Portal.Name");
		
		String link = "";
		if(supplier.equals("hotelbeds_v2") && (xmlType.equals("AvailabilityRequest") || xmlType.equals("AvailabilityResponse")))
		{
			link = file.get("Portal.Name").concat("_").concat(supplier).concat("_").concat("Page_1_").concat(xmlType).concat("_").concat(tracerID);
		}
		else
		{
			link = file.get("Portal.Name").concat("_").concat(supplier).concat("_").concat(xmlType).concat("_").concat(tracerID);
		}
		
		return link;
	}
	
	public String getHomeUrl(String url1, String url2)
	{
		String homeUrl = "";
		String date = this.getToday();
		homeUrl = url1.concat(date).concat(url2);
		return homeUrl;
	}
	
	public String urlCreator (String link, String innerUrl1, String innerUrl2, String xmlHomeUrl) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(xmlHomeUrl));
		
		DriverFactory.getInstance().getDriver().manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		Iterator<Element> aaa = doc.getElementsByTag("a").iterator();
		String url = "";
		String today = this.getToday();
		while(aaa.hasNext())
		{
			Element						currentElement			 = aaa.next();
			if(currentElement.getElementsByTag("a").first().text().contains(link))
			{
				url = innerUrl1.concat(today).concat(innerUrl2).concat(currentElement.getElementsByTag("a").first().text());
				break;
			}
		}
		if(url.equals("") || url.isEmpty() || url.equals(null))
		{
			while(aaa.hasNext())
			{
				Element						currentElement			 = aaa.next();
				if(currentElement.getElementsByTag("a").first().text().contains(link))
				{
					url = innerUrl1.concat(today).concat(innerUrl2).concat(currentElement.getElementsByTag("a").first().text());
					break;
				}
			}
		}
		if(url.equals("") || url.isEmpty() || url.equals(null))
		{
			System.out.println("url - Unavailable");
			url = "url - Unavailable";
		}
		return url;
	}
	
	public String getToday()
	{
		DateFormat 	dateFormat 	= new SimpleDateFormat("dd-MM-yyyy");
		Calendar 	cal 		= Calendar.getInstance();
		String 		date 		= dateFormat.format(cal.getTime());
		//System.out.println(date);
		return date;
	}
	
	public HashMap<String, String> getPaymentLog(String url, String transactionId, HashMap<String, String> portalSpecificData) throws IOException, InterruptedException
	{
		String outerUrl = url.concat(this.getToday());
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(outerUrl));
		Iterator<Element> aaa = doc.getElementsByTag("a").iterator();
		HashMap<String, String> paymentDetails = new HashMap<>();
		while(aaa.hasNext())
		{
			Element						currentElement			 = aaa.next();
			if(currentElement.getElementsByTag("a").first().text().contains("INFO_Response"))
			{
				String log = currentElement.getElementsByTag("a").first().text();
				String innerUrl = portalSpecificData.get("portal.PaymentLog").replace("dir", "file").concat(this.getToday()).concat("%2F").concat(log);
				Document logDoc = Jsoup.parse(proxyAndPort.setProxy(innerUrl));
				try {
					if(transactionId.contains(logDoc.getElementsByTag("returnurl").first().text()) || logDoc.getElementsByTag("returnurl").first().text().contains(transactionId))
					{
						//System.out.println("payment log Info response - " + innerUrl);
						try {
							//System.out.println(logDoc.getElementsByTag("paymentid").first().text());
							paymentDetails.put("paymentID", logDoc.getElementsByTag("paymentid").first().text());
							System.out.println("payment details - PaymentID - " + paymentDetails.get("paymentID"));
							paymentDetails.put("paymentReference", logDoc.getElementsByTag("ordernumber").first().text());
							System.out.println("payment details - paymentReference - " + paymentDetails.get("paymentReference"));
							Element trans = logDoc.getElementsByTag("auth").first();
							paymentDetails.put("transactionID", trans.getElementsByTag("transactionid").first().text());
							System.out.println("payment details - transactionID - " + paymentDetails.get("transactionID"));
							break;
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					}
				} catch (Exception e) {
					// TODO: handle exception
					try {
						paymentDetails.put("errorStatus", logDoc.getElementsByTag("status").first().text());
						System.out.println("payment details - errorStatus - " + paymentDetails.get("errorStatus"));
						paymentDetails.put("errorDescription", logDoc.getElementsByTag("description").first().text());
						System.out.println("payment details - errorDescription - " + paymentDetails.get("errorDescription"));
						break;
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			}
		}
								proxyAndPort	= new SetProxyPort();
		 						doc  			= Jsoup.parse(proxyAndPort.setProxy(outerUrl));
		aaa = doc.getElementsByTag("a").iterator();
		while(aaa.hasNext())
		{
			Element						currentElement			 = aaa.next();
			try {
				if(currentElement.getElementsByTag("a").first().text().contains("AUTH_Response"))
				{
					String log = currentElement.getElementsByTag("a").first().text();
					String innerUrl = portalSpecificData.get("portal.PaymentLog").replace("dir", "file").concat(this.getToday()).concat("%2F").concat(log);
					Document logDoc = Jsoup.parse(proxyAndPort.setProxy(innerUrl));
					if(paymentDetails.get("paymentID").equals(logDoc.getElementsByTag("paymentid").first().text()))
					{
						System.out.println("payment log Auth response - " + innerUrl);
						paymentDetails.put("authId", logDoc.getElementsByTag("authno").first().text());
						System.out.println("payment details - authId - " + paymentDetails.get("authId"));
						break;
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		return paymentDetails;
	}
}
