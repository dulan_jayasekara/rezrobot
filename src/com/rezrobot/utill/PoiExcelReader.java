package com.rezrobot.utill;
import java.io.FileInputStream; 
import java.io.PrintStream; 
import java.util.ArrayList; 
import java.util.HashMap;
import java.util.Iterator; 
import java.util.Map;

import org.apache.poi.hssf.usermodel.*; 

public class PoiExcelReader 
{ 
	public PoiExcelReader() 
	{ 
		x = 0; 
	} 
	public ArrayList<Map<Integer, String>> reader(FileInputStream stream) 
		{ 
			ArrayList<Map<Integer, String>> bigArr = new ArrayList<Map<Integer, String>>();
			try 
			{ 
				HSSFWorkbook workbook = new HSSFWorkbook(stream); 
				int numSheets = workbook.getNumberOfSheets(); 
				System.out.println((new StringBuilder("---------")).append(numSheets).toString()); 
				for(int k = 0 ; k < numSheets ; k++)
				{
					Map<Integer, String> excelDetails = new HashMap<Integer, String>();
					//System.out.println((new StringBuilder("Sheet number : ")).append(k).toString()); 
					HSSFSheet sheet = workbook.getSheetAt(k); 
					sheet.getRow(0).getLastCellNum(); 
					Iterator rowIterator = sheet.iterator(); 
					int i = 1; 
					while(rowIterator.hasNext()) 
						{ 
						int j = 0; 
						ArrayList<String> al = new ArrayList<String>();
						try 
						{ 	
							for(; sheet.getRow(i).getCell(j) != null; j++)
							{
								HSSFCell temp = sheet.getRow(i).getCell(j); 
								int a = temp.getCellType();
								try {
									if(a == 1)
									{
										al.add(temp.getStringCellValue()); 
									}
									else
									{
										al.add(String.valueOf(temp.getNumericCellValue()));
									}	
								} catch (Exception e) {
									// TODO: handle exception
									al.add("----");
								}
								
							} 
							String list = null; 
							list = (String)al.get(0); 
							for(int x = 1; x < al.size(); x++) 
								list = list.concat(",").concat((String)al.get(x)); 
							excelDetails.put((i-1), list);
							i++; 
							continue; 
						} 
						catch(Exception e) 
						{
							//System.out.println(e);
						} 
						break;
					} 
					bigArr.add(excelDetails);
				}
				
			stream.close(); 
		} catch(Exception e) 
		{
			e.printStackTrace(); 
		} 
		return bigArr; 
	} 
	int x; 
}

