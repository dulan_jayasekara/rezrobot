package com.rezrobot.utill;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import bsh.This;
//import java.util.HashMap;

//import org.openqa.jetty.html.Break;


public class ReportTemplate {
	
	//private StringBuffer    PrintWriter         = null;
	
	//private HashMap<String, String>   PropertySet = null;
	String printHtmlReport = "false";
	String printExtentReport = "false";
	private HtmlReportTemplate HtmlReportTemplate;
	private ExtentReportTemplate ExtentReportTemplate;
	    
	public void Initialize(/*StringBuffer PrintWriter, ExtentReports report*/)
	{
		//HtmlReportTemplate.Initialize(PrintWriter);
		//ExtentReportTemplate.Initialize(report);
		try {
			printHtmlReport = PropertySingleton.getInstance().getProperty("HtmlReport");
			printExtentReport = PropertySingleton.getInstance().getProperty("ExtentReport");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static class SingletonHolder { 
	    private static final ReportTemplate INSTANCE = new ReportTemplate();
	}
	
	public static ReportTemplate getInstance() {
	    return SingletonHolder.INSTANCE;
	}
	
	public String getKey(String key)
	{
		try {
			return PropertySingleton.getInstance().getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
			return "No Value";
		}
		
	}
	  
	public void verifyTrue(String Expected,String Actual,String Message) {
		try {

			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				HtmlReportTemplate.getInstance().verifyTrue(Expected.trim(), Actual.trim(), Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyTrue(Expected.trim(), Actual.trim(), Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
	  }
	
	public void verifyArrayList(ArrayList<String> Expected, ArrayList<String> Actual, String Message)
	{
		try {
			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				HtmlReportTemplate.getInstance().verifyArrayList(Expected, Actual, Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyArrayList(Expected, Actual, Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
	}
	
	public void verifyIntArrayList(ArrayList<Integer> Expected, ArrayList<Integer> Actual, String Message)
	{
		try {
			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				//HtmlReportTemplate.getInstance().verifyArrayList(Expected, Actual, Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyIntArrayList(Expected, Actual, Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
	}
	public void verifyContains(String Expected,String Actual,String Message){
		try {
			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				HtmlReportTemplate.getInstance().verifyContains(Expected, Actual, Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyContains(Expected, Actual, Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
		
	}
	
	public void verifyEqualIgnoreCase(String Expected,String Actual,String Message){
		try {
			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				HtmlReportTemplate.getInstance().verifyEqualIgnoreCase(Expected, Actual, Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyEqualIgnoreCase(Expected, Actual, Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
		
	}
	
	public void verifyTrue(double Expected,double Actual,String Message){
		try {
			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				HtmlReportTemplate.getInstance().verifyTrue(Expected, Actual, Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyTrue(Expected, Actual, Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
		
	}
	
	public void verifyTrue(boolean Expected,boolean Actual,String Message)
	  {
		try {
			if( printHtmlReport.equalsIgnoreCase("true") )
			{
				HtmlReportTemplate.getInstance().verifyTrue(Expected, Actual, Message);
			}
			if( printExtentReport.equalsIgnoreCase("true") )
			{
				ExtentReportTemplate.getInstance().verifyTrue(Expected, Actual, Message);
			}
		} catch (Exception e) {
			// TODO: handle exception
			ExtentReportTemplate.getInstance().verifyTrue("", String.valueOf(e), Message);
		}
		
	}
	
	/*public void setTableHeading(String TableHeader){
		  
		PrintWriter.append("<br>");
		PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
		PrintWriter.append("<table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			  
	}
	  
	public void markTableEnd(){
		  
		PrintWriter.append("</table>");
		  
	}
	  
	public void markReportBegining(String reportHeadding){
		  
	    PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
	    PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>"+reportHeadding+"(" + Calender.getDate(Calendar.DATE, 0) + ")</p></div>");
	    PrintWriter.append("<body>");
		  
	}
	*/  
	public void setInfoTable(String tableHeader, Map<String, String> infoMap){
		
		if( printHtmlReport.equalsIgnoreCase("true") )
		{
			HtmlReportTemplate.getInstance().setInfoTabel(tableHeader, infoMap);
			
		}
		if( printExtentReport.equalsIgnoreCase("true") )
		{
			ExtentReportTemplate.getInstance().setInfoTest(tableHeader, infoMap);
		}
		
		/*PrintWriter.append("<br>");
		PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
		PrintWriter.append("<table ><tr><th>Attribute</th><th>Parameter</th></tr>");*/
		  
	}
	
	public void setSystemInfo(Map sysInfo){
		
		if( printHtmlReport.equalsIgnoreCase("true") )
		{
			//HtmlReportTemplate.getInstance().addToInfoTabel(tableHeader, info);
			
		}
		if( printExtentReport.equalsIgnoreCase("true") )
		{
			System.out.println();
			ExtentReportTemplate.getInstance().setSystemInfo(sysInfo);
		}
		  
	}
	  
	/*public void addToInfoTabel(String Attr,String Parameter){
		
		PrintWriter.append("<tr><td>"+Attr+"</th><td>"+Parameter+"</th></tr>");
		  
	}*/

}
