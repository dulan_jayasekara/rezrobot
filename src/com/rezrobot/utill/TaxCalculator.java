package com.rezrobot.utill;

import java.math.BigDecimal;
import java.util.Map;

public class TaxCalculator {

	public String getTax(String value, String pm, String defaultCC, String portalCC, Map<String, String> currency)
	{
		int rtnValue = 0;
		if(pm.contains("%"))
		{
			String[] pmPercentage = pm.split("%");
			float percentage = Float.parseFloat(pmPercentage[0]);
			float fvalue	= Float.parseFloat(value);
			fvalue = (fvalue/100)*percentage;
			BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
			value = String.valueOf(roundfinalPrice);
			int pmAddedPrice	= Integer.parseInt(value);
			rtnValue		= pmAddedPrice;
			//System.out.println(pmAddedPrice);
		}
		else
		{
			CurrencyConverter cc = new CurrencyConverter();
			rtnValue = Integer.parseInt(cc.convert(defaultCC, pm, portalCC, currency, defaultCC));
			
		}
		return String.valueOf(rtnValue);
	}
	
	public String getTax(String value, String pm, String defaultCC, String portalCC, Map<String, String> currency, String pgCurrency)
	{
		int rtnValue = 0;
		if(pm.contains("%"))
		{
			String[] pmPercentage = pm.split("%");
			float percentage = Float.parseFloat(pmPercentage[0]);
			float fvalue	= Float.parseFloat(value);
			fvalue = (fvalue/100)*percentage;
			BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
			value = String.valueOf(roundfinalPrice);
			int pmAddedPrice	= Integer.parseInt(value);
			rtnValue		= pmAddedPrice;
			//System.out.println(pmAddedPrice);
		}
		else
		{
			CurrencyConverter cc = new CurrencyConverter();
			rtnValue = Integer.parseInt(cc.convert(defaultCC, pm, portalCC, currency, pgCurrency));
		}
		return String.valueOf(rtnValue);
	}
	
	public String getTaxWithoutRoundUp(String value, String pm, String defaultCC, String portalCC, Map<String, String> currency)
	{
		float rtnValue = 0;
		try {
			if(pm.contains("%"))
			{
				String[] pmPercentage = pm.split("%");
				float percentage = Float.parseFloat(pmPercentage[0]);
				float fvalue	= Float.parseFloat(value);
				fvalue = (fvalue/100)*percentage;
				rtnValue		= fvalue;
				//System.out.println(pmAddedPrice);
			}
			else
			{
				CurrencyConverter cc = new CurrencyConverter();
				rtnValue = Integer.parseInt(cc.convert(defaultCC, pm, portalCC, currency, defaultCC));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
		 BigDecimal bd = new BigDecimal(rtnValue);
	     bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
	     return String.valueOf(bd.floatValue());
	}
	
	public String getTaxWithoutRoundUp(String value, String pm, String defaultCC, String portalCC, Map<String, String> currency, String pgCurrency)
	{
		float rtnValue = 0;
		try {
			if(pm.contains("%"))
			{
				String[] pmPercentage = pm.split("%");
				float percentage = Float.parseFloat(pmPercentage[0]);
				float fvalue	= Float.parseFloat(value);
				fvalue = (fvalue/100)*percentage;
				rtnValue		= fvalue;
				//System.out.println(pmAddedPrice);
			}
			else
			{
				CurrencyConverter cc = new CurrencyConverter();
				rtnValue = Integer.parseInt(cc.convert(defaultCC, pm, portalCC, currency, pgCurrency));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
		 BigDecimal bd = new BigDecimal(rtnValue);
	     bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
	     return String.valueOf(bd.floatValue());
	}
	
}
