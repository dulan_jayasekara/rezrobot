package com.rezrobot.utill;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public  class xmlPathGeneratorConcurent extends Thread{
		
		Thread runner ; 
		public xmlPathGeneratorConcurent()
		{
			
		}
		
		public void run(String url1, String url2, String supplier, String xmltype, String tracerID, String productType, HashMap<String, String> xmlDetails) throws IOException
		{
			String url 		= "";
			if(productType.equals("hotel"))
			{
				String link 	= this.getLink(xmlDetails, supplier, xmltype, tracerID);
				String homeUrl 	= this.getHomeUrl(url1, url2);
				url 			= this.urlCreator(link, url1, url2, homeUrl);
				System.out.println(url);
			}
		}
		
		public String getLink(HashMap<String, String> file, String supplier, String xmlType, String tracerID)
		{
			String portal = file.get("hotelPortalPreFix");
			
			String link = "";
			if(supplier.equals("hotelbeds_v2") && (xmlType.equals("AvailabilityRequest") || xmlType.equals("AvailabilityResponse")))
			{
				link = file.get("hotelPortalPreFix").concat("_").concat(supplier).concat("_").concat("Page_1_").concat(xmlType).concat("_").concat(tracerID);
			}
			else
			{
				link = file.get("hotelPortalPreFix").concat("_").concat(supplier).concat("_").concat(xmlType).concat("_").concat(tracerID);
			}
			
			return link;
		}
		
		public String getHomeUrl(String url1, String url2)
		{
			String homeUrl = "";
			String date = this.getToday();
			homeUrl = url1.concat(date).concat(url2);
			return homeUrl;
		}
		
		public String urlCreator (String link, String innerUrl1, String innerUrl2, String xmlHomeUrl) throws IOException
		{
			Document doc = Jsoup.connect(xmlHomeUrl).get();
			
			Iterator<Element> aaa = doc.getElementsByTag("a").iterator();
			String url = "";
			String today = this.getToday();
			while(aaa.hasNext())
			{
				Element						currentElement			 = aaa.next();
				if(currentElement.getElementsByTag("a").first().text().contains(link))
				{
					url = innerUrl1.concat(today).concat(innerUrl2).concat(currentElement.getElementsByTag("a").first().text());
					break;
				}
			}
			return url;
		}
		
		public String getToday()
		{
			DateFormat 	dateFormat 	= new SimpleDateFormat("dd-MM-yyyy");
			Calendar 	cal 		= Calendar.getInstance();
			String 		date 		= dateFormat.format(cal.getTime());
			//System.out.println(date);
			return date;
		}
		
		
		
	}
