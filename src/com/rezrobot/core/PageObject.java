/**
 * 
 */
package com.rezrobot.core;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.rezrobot.pojo.CheckBox;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Frame;
import com.rezrobot.pojo.Image;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.pojo.Label;
import com.rezrobot.pojo.Link;
import com.rezrobot.pojo.RadioButton;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.PageReader;

/**
 * @author Dulan
 *
 */
public abstract class PageObject {
	
	protected Map<String,UIElement> objectMap = new HashMap<String, UIElement>();
	
	public PageObject() {
		new PageReader().getPage(this.getClass().getSimpleName()).loadElements(objectMap);
		
		
    }
	
	public boolean isPageAvailable(String Key) {
		try {
		    return this.objectMap.get(Key).getWebElement().isDisplayed();
		} catch (Exception e) {
		    return false;
		}
	}
	
	public boolean isCheckBoxSelected(String Key)
	{
		try {
			return ((CheckBox)this.objectMap.get(Key)).isElementSelected();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean isRadioBoxSelected(String Key)
	{
		try {
			return ( (RadioButton)this.objectMap.get(Key)).isElementSelected();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public void addobjects(){
		
		Map<String,UIElement> objectNewMap = new HashMap<String, UIElement>();
		new PageReader().getPage(this.getClass().getSimpleName()).loadElements(objectNewMap);
		
		Map<String,UIElement> temp = new HashMap<String, UIElement>(); 
		temp.putAll(objectMap);
		objectMap.clear();
		objectMap.putAll(objectNewMap);
		objectMap.putAll(temp);
		
		
		
		
	}
	
	
	
	public String getPageTitel(){
		try {
		return DriverFactory.getInstance().getDriver().getTitle();
	    } catch (Exception var2) {
	        return null;
	    }
	}
   
	public boolean getElementVisibility(String Key){
		return  this.objectMap.get(Key).isElementVisible();
	}

	public boolean getElementVisibility(String Key,int timeout){
 	
		return  this.objectMap.get(Key).isElementVisible(timeout);
	}
	
	public void getElementClick(String Key) throws Exception{
	 	
		try {
			this.objectMap.get(Key).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getComboBoxDataList(String Key) {
		
		try {
			return this.objectMap.get(Key).getText();
		} catch (Exception e) {
			return "Data List Not Loaded";
		}
	}
	
	public void keyType(String key,String text){
		
		try {
         this.objectMap.get(key).setText(text);
    	} catch (Exception e) {
    	    
    	}
		
	}
    
    public String  getColor(String key){
    	try {
    		return  this.objectMap.get(key).getColor();
    	} catch (Exception e) {
    	   return null; 
    	}
    	
    }
    
    public void actionKeyType(String key,String text){
    	try {
    		this.objectMap.get(key).setText(text);
    	} catch (Exception e) {
    	    
    	}

    }
    
    public void selectOptionByText(String key,String text){
 	
    	try {
    		( (ComboBox)this.objectMap.get(key)).selectOptionByText(text);
    	} catch (Exception e) {
    	    
    	}
    	
    }
    
    
    
    public UIElement getElement(String key){
 	
    	try {
    		return this.objectMap.get(key);
    	} catch (Exception e) {
    	   
    	    throw e;
    	}
    }
    
public WebElement getElement(String key,int index) throws Exception{
	WebElement elem;
    	ArrayList<WebElement> list = new ArrayList<WebElement>();
    	
    	try {
    		
    		By by;
    		if (RefType.getType(objectMap.get(key).getRefType()) == RefType.ID)
    			by = By.id(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.XPATH)
    			by = By.xpath(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.CLASSNAME)
    			by = By.className(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.CSS)
    			by = By.cssSelector(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.LINKTEXT)
    			by = By.linkText(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.PARTIALLINKTEXT)
    			by = By.partialLinkText(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.NAME)
    			by = By.name(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.TAGNAME)
    			by = By.tagName(objectMap.get(key).getRef());
    		else {
    			throw new Exception(
    					"Ref type not defined in the xml or invalid ref type :"
    							+ objectMap.get(key).getRefType());
    		}
    		
    		list = new ArrayList<WebElement>(DriverFactory.getInstance().getDriver().findElements(by));
    		elem=list.get(index);
    	} catch (Exception e) {
    	   
    	    throw e;
    	}
    	
    	return elem;
    }

public void addUiElement(String Key,WebElement elem) {
	
	
	
}
    
    public ArrayList<WebElement> getElements(String key) throws Exception{
     	
    	ArrayList<WebElement> list = new ArrayList<WebElement>();
    	
    	try {
    		
    		By by;
    		if (RefType.getType(objectMap.get(key).getRefType()) == RefType.ID)
    			by = By.id(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.XPATH)
    			by = By.xpath(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.CLASSNAME)
    			by = By.className(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.CSS)
    			by = By.cssSelector(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.LINKTEXT)
    			by = By.linkText(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.PARTIALLINKTEXT)
    			by = By.partialLinkText(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.NAME)
    			by = By.name(objectMap.get(key).getRef());
    		else if (RefType.getType(objectMap.get(key).getRefType()) == RefType.TAGNAME)
    			by = By.tagName(objectMap.get(key).getRef());
    		else {
    			throw new Exception(
    					"Ref type not defined in the xml or invalid ref type :"
    							+ objectMap.get(key).getRefType());
    		}
    		
    		list = new ArrayList<WebElement>(DriverFactory.getInstance().getDriver().findElements(by));
    	} catch (Exception e) {
    	   
    	    throw e;
    	}
    	
    	return list;
    }
   

    
    public void switchToDefaultFrame(){
    	
    	DriverFactory.getInstance().getDriver().switchTo().defaultContent();
    	
    }
    
    public void switchToFrame(String Key) throws Exception{
    	((Frame)this.objectMap.get(Key)).switchToFrame();
    	
    }
	
    public void uploadFile(String ImagePath,String Key) throws Exception {

		try {
			try {
				((JavascriptExecutor) DriverFactory.getInstance().getDriver()).executeScript("document.getElementById('"+Key+"').click();");
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(Key);
				System.out.println(e);
			}
			
			
			String imagePath = new File(ImagePath).getAbsolutePath();
			StringSelection ss = new StringSelection(imagePath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	
	public String getLabelName(String Key) {
		try {
			return ( (Label)this.objectMap.get(Key)).getText();
		} catch (Exception e) {
			return "N/A";
		}
	}
	
	
	public String getPlaceHolder(String Key) {
		try {
			return this.objectMap.get(Key).getPlaceHolder();
		} catch (Exception e) {
			return "N/A";
		}
	}
	
	public boolean getMandatoryStatus(String Key){
		return ( (InputFiled)this.objectMap.get(Key)).getMandatoryStatus();
	}
	
	public boolean getComboBoxMandatoryStatus(String Key){
		try {
			return ( (ComboBox)this.objectMap.get(Key)).getComboBoxMandatoryStatus();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean getRadioButtonMandatoryStatus(String Key){
		try {
			return ( (RadioButton)this.objectMap.get(Key)).getRadioButtonMandatoryStatus();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean getLabelMandatoryStatus(String Key){
		try {
			return ((Label)this.objectMap.get(Key)).getLabelMandatoryStatus();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean getMandatoryStatusFromDataConstraints(String Key){		
		try {
			return ((InputFiled)this.objectMap.get(Key)).getMandatoryStatusFromDataConstraints();
		} catch (Exception e) {
			return false;
		}		
	}
	
	public boolean getComboBoxMandatoryStatusFromDataConstraints(String Key){
		try {
			return ((ComboBox)this.objectMap.get(Key)).getComboBoxMandatoryStatusFromDataConstraints();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean getRadioButtonMandatoryStatusFromDataConstraints(String Key){
		try {
			return ((RadioButton)this.objectMap.get(Key)).getRadioButtonMandatoryStatusFromDataConstraints();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean getCheckBoxMandatoryStatusFromDataConstraints(String Key){
		try {
			return ((CheckBox)this.objectMap.get(Key)).getCheckBoxMandatoryStatusFromDataConstraints();
		} catch (Exception e) {
			return false;
		}
	}
	
	public void executejavascript(String script) {
		try {
			
				
					if (DriverFactory.getInstance().getDriver() instanceof JavascriptExecutor) {
						((JavascriptExecutor) DriverFactory.getInstance().getDriver())
								.executeScript(script, new Object[0]);
					}
				
			
		} catch (Exception e) {
			/*logger.fatal("Framework Error : Java Script Executed" + script);
			logger.fatal("Framework Error :Unable to Execute Java Script", var3);*/
		}
		
	}
	
	public void getinnerUI(String Key,String innerKey) throws Exception {
		
		
		
		
		WebElement parentele=getElement(Key).getWebElement();
		addobjects();
		//System.out.println(parentele.getText());
		
		
		//System.out.println(objectMap);
	
		
		
	}
	
	public boolean getelementAvailability(String Key) {
		
		return this.objectMap.get(Key).isElementAvailable();
		
	}
	
	public boolean getelementPresence(UIElement element) {
		
		return element.isElementAvailable();
		
	}
	
	
	public void waitElementLoad(int time) {
		
		DriverFactory.getInstance().getDriver().manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
		
	}
	
   public void waitForFrameAndSwitchToIt(String Key,int time) throws Exception {
		
	   By by = this.objectMap.get(Key).getByType();
	   new WebDriverWait(DriverFactory.getInstance().getDriver(), time).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
		
	}
	
public void addTomap(UIElement target) {
		
		objectMap.put(target.getKey(), target);
		
	}
	

	
}

