package com.rezrobot.dataobjects;

import java.io.Serializable;

public class hotelOccupancy implements Serializable{

	String room = ""; 
	String title = ""; 
	String firstname = ""; 
	String lastname = ""; 
	String smoking = ""; 
	String handicap = ""; 
	String wheelchair = "";
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getHandicap() {
		return handicap;
	}
	public void setHandicap(String handicap) {
		this.handicap = handicap;
	}
	public String getWheelchair() {
		return wheelchair;
	}
	public void setWheelchair(String wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	
}
