package com.rezrobot.dataobjects.xml_objects.air2;

public class SearchRequest {

	private String	InboundArrivalDate;
	private String	InboundArrivalTime;
	private String	OutOriginLocation;
	private String	OutDestinationLocation; 
	private String	OutboundDepartureDate;
	private String	OutboundDepartureTime;
	private String	InOriginLocation;
	private String  InDestinationLocation; 
	private String	CabinType;
	private String	SeatsRequested;
	private String	Adults;
	private String	Childs;
	private String	Infants;
	private String	PricingSource;
	private String	OfficeID;
    private String  URL;
    
    
	public String getInboundArrivalDate() {
		return InboundArrivalDate;
	}
	public void setInboundArrivalDate(String inboundArrivalDate) {
		InboundArrivalDate = inboundArrivalDate;
	}
	public String getInboundArrivalTime() {
		return InboundArrivalTime;
	}
	public void setInboundArrivalTime(String inboundArrivalTime) {
		InboundArrivalTime = inboundArrivalTime;
	}
	public String getOutOriginLocation() {
		return OutOriginLocation;
	}
	public void setOutOriginLocation(String outOriginLocation) {
		OutOriginLocation = outOriginLocation;
	}
	public String getOutDestinationLocation() {
		return OutDestinationLocation;
	}
	public void setOutDestinationLocation(String outDestinationLocation) {
		OutDestinationLocation = outDestinationLocation;
	}
	public String getOutboundDepartureDate() {
		return OutboundDepartureDate;
	}
	public void setOutboundDepartureDate(String outboundDepartureDate) {
		OutboundDepartureDate = outboundDepartureDate;
	}
	public String getOutboundDepartureTime() {
		return OutboundDepartureTime;
	}
	public void setOutboundDepartureTime(String outboundDepartureTime) {
		OutboundDepartureTime = outboundDepartureTime;
	}
	public String getInOriginLocation() {
		return InOriginLocation;
	}
	public void setInOriginLocation(String inOriginLocation) {
		InOriginLocation = inOriginLocation;
	}
	public String getInDestinationLocation() {
		return InDestinationLocation;
	}
	public void setInDestinationLocation(String inDestinationLocation) {
		InDestinationLocation = inDestinationLocation;
	}
	public String getCabinType() {
		return CabinType;
	}
	public void setCabinType(String cabinType) {
		CabinType = cabinType;
	}
	public String getSeatsRequested() {
		return SeatsRequested;
	}
	public void setSeatsRequested(String seatsRequested) {
		SeatsRequested = seatsRequested;
	}
	public String getAdults() {
		return Adults;
	}
	public void setAdults(String adults) {
		Adults = adults;
	}
	public String getChilds() {
		return Childs;
	}
	public void setChilds(String childs) {
		Childs = childs;
	}
	public String getInfants() {
		return Infants;
	}
	public void setInfants(String infants) {
		Infants = infants;
	}
	public String getPricingSource() {
		return PricingSource;
	}
	public void setPricingSource(String pricingSource) {
		PricingSource = pricingSource;
	}
	public String getOfficeID() {
		return OfficeID;
	}
	public void setOfficeID(String officeID) {
		OfficeID = officeID;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}

}
