package com.rezrobot.dataobjects;

public class ExchangeRateDetailsObject {

	String date 			= "";
	String defaultCurrency 	= "";
	String currency 		= "";
	String buyingRate 		= "";
	String sellingRate 		= "";
	String validity			= "";
	
	
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDefaultCurrency() {
		return defaultCurrency;
	}
	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBuyingRate() {
		return buyingRate;
	}
	public void setBuyingRate(String buyingRate) {
		this.buyingRate = buyingRate;
	}
	public String getSellingRate() {
		return sellingRate;
	}
	public void setSellingRate(String sellingRate) {
		this.sellingRate = sellingRate;
	}
	
	public ExchangeRateDetailsObject extract(String[] rate)
	{
		this.setDate			(rate[0]);
		this.setDefaultCurrency	(rate[1]);
		this.setCurrency		(rate[2]);
		this.setBuyingRate		(rate[3]);
		this.setSellingRate		(rate[4]);
		
		return this;
	}
}
