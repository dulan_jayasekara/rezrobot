package com.rezrobot.dataobjects;

public class dmc_Setup_DataObjects_cancellationObject {
	
	
	String fromDate;
	String toDate;
	String packageRefundable;
	String applyifarrivaldateislessthan;
	String basedon;
	String noShowCancellationBasedon;
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getPackageRefundable() {
		return packageRefundable;
	}
	public void setPackageRefundable(String packageRefundable) {
		this.packageRefundable = packageRefundable;
	}
	public String getApplyifarrivaldateislessthan() {
		return applyifarrivaldateislessthan;
	}
	public void setApplyifarrivaldateislessthan(String applyifarrivaldateislessthan) {
		this.applyifarrivaldateislessthan = applyifarrivaldateislessthan;
	}
	public String getBasedon() {
		return basedon;
	}
	public void setBasedon(String basedon) {
		this.basedon = basedon;
	}
	public String getNoShowCancellationBasedon() {
		return noShowCancellationBasedon;
	}
	public void setNoShowCancellationBasedon(String noShowCancellationBasedon) {
		this.noShowCancellationBasedon = noShowCancellationBasedon;
	}
	
	

}
