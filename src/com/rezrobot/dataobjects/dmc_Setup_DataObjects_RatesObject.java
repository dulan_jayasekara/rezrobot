package com.rezrobot.dataobjects;

public class dmc_Setup_DataObjects_RatesObject {
	
	String  paxGroup;
	String  flightrate;
	String  singleRoomRate;
	String  twinRoomRate;
	String  tripleRoomRate;
	String  childWithoutBedRate;
	String  childWithBedRate;
	String  infantSupplement;
	String  landComponentTaxBy;
	String  landComponentTaxValue;
	
	
	
	public String getPaxGroup() {
		return paxGroup;
	}
	public void setPaxGroup(String paxGroup) {
		this.paxGroup = paxGroup;
	}
	
	public String getFlightrate() {
		return flightrate;
	}
	public void setFlightrate(String flightrate) {
		this.flightrate = flightrate;
	}
	public String getSingleRoomRate() {
		return singleRoomRate;
	}
	public void setSingleRoomRate(String singleRoomRate) {
		this.singleRoomRate = singleRoomRate;
	}
	public String getTwinRoomRate() {
		return twinRoomRate;
	}
	public void setTwinRoomRate(String twinRoomRate) {
		this.twinRoomRate = twinRoomRate;
	}
	public String getTripleRoomRate() {
		return tripleRoomRate;
	}
	public void setTripleRoomRate(String tripleRoomRate) {
		this.tripleRoomRate = tripleRoomRate;
	}
	public String getChildWithoutBedRate() {
		return childWithoutBedRate;
	}
	public void setChildWithoutBedRate(String childWithoutBedRate) {
		this.childWithoutBedRate = childWithoutBedRate;
	}
	public String getChildWithBedRate() {
		return childWithBedRate;
	}
	public void setChildWithBedRate(String childWithBedRate) {
		this.childWithBedRate = childWithBedRate;
	}
	public String getInfantSupplement() {
		return infantSupplement;
	}
	public void setInfantSupplement(String infantSupplement) {
		this.infantSupplement = infantSupplement;
	}
	public String getLandComponentTaxBy() {
		return landComponentTaxBy;
	}
	public void setLandComponentTaxBy(String landComponentTaxBy) {
		this.landComponentTaxBy = landComponentTaxBy;
	}
	public String getLandComponentTaxValue() {
		return landComponentTaxValue;
	}
	public void setLandComponentTaxValue(String landComponentTaxValue) {
		this.landComponentTaxValue = landComponentTaxValue;
	}
	
	
	


}
