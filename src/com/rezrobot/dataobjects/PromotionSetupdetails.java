package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import com.rezrobot.enumtypes.DaysOftheWeek;
import com.sun.xml.internal.ws.api.config.management.policy.ManagementAssertion.Setting;

/**
 * @author charitha
 * 
 */

public class PromotionSetupdetails implements Serializable {

	private String SelectOption;
	private String HotelName;
	private String PromotionCode;
	private String PromotionName;
	private String AddDescription;
	private String DescriptionText;
	private String PromotionType;
	private String PromotionStatus;
	private String BookingdateFrom;
	private String BookingdateTo;
	private String AddMultilingual;
	private String Language;
	private String mulPromoName;

	private String mulDescrepscrition;

	public String getDescriptionText() {
		return DescriptionText;
	}

	public void setDescriptionText(String descriptionText) {
		DescriptionText = descriptionText;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getMulPromoName() {
		return mulPromoName;
	}

	public void setMulPromoName(String mulPromoName) {
		this.mulPromoName = mulPromoName;
	}

	public String getMulDescrepscrition() {
		return mulDescrepscrition;
	}

	public void setMulDescrepscrition(String mulDescrepscrition) {
		this.mulDescrepscrition = mulDescrepscrition;
	}

	// private DaysOftheWeek ApplicableDays;
	private ArrayList<DaysOftheWeek> ApplicableDays = null;

	public String getHotelName() {
		return HotelName;
	}

	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}

	public String getSelectOption() {
		return SelectOption;
	}

	public void setSelectOption(String selectOption) {
		SelectOption = selectOption;
	}

	public String getSelectIOption() {
		return SelectOption;
	}

	public String getPromotionCode() {
		return PromotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		PromotionCode = promotionCode;
	}

	public String getPromotionName() {
		return PromotionName;
	}

	public void setPromotionName(String promotionName) {
		PromotionName = promotionName;
	}

	public String getAddDescription() {
		return AddDescription;
	}

	public void setAddDescription(String addDescription) {
		AddDescription = addDescription;
	}

	public String getAddMultilingual() {
		return AddMultilingual;
	}

	public void setAddMultilingual(String addMultilingual) {
		AddMultilingual = addMultilingual;
	}

	public String getPromotionType() {
		return PromotionType;
	}

	public void setPromotionType(String promotionType) {
		PromotionType = promotionType;
	}

	public String getPromotionStatus() {
		return PromotionStatus;
	}

	public void setPromotionStatus(String promotionStatus) {
		PromotionStatus = promotionStatus;
	}

	public String getBookingdateFrom() {
		return BookingdateFrom;
	}

	public void setBookingdateFrom(String bookingdateFrom) {
		BookingdateFrom = bookingdateFrom;
	}

	public String getBookingdateTo() {
		return BookingdateTo;
	}

	public void setBookingdateTo(String bookingdateTo) {
		BookingdateTo = bookingdateTo;
	}

	public ArrayList<DaysOftheWeek> getApplicableDays() {
		return ApplicableDays;
	}

	public void addToApplicableDays(DaysOftheWeek applicableDay) {
		if (ApplicableDays == null) {
			ApplicableDays = new ArrayList<DaysOftheWeek>();
			ApplicableDays.add(applicableDay);
		} else {
			ApplicableDays.add(applicableDay);
		}
	}

	public PromotionSetupdetails setData(String[] setupDetails) throws ParseException {

		this.setSelectOption(setupDetails[0]);
		this.setHotelName(setupDetails[1]);
		this.setPromotionCode(setupDetails[2]);

		this.setPromotionName(setupDetails[3]);

		this.setAddDescription(setupDetails[4]);
		this.setDescriptionText(setupDetails[5]);

		this.setAddMultilingual(setupDetails[6]);
		this.setLanguage(setupDetails[7]);
		this.setMulPromoName(setupDetails[8]);
		this.setMulDescrepscrition(setupDetails[9]);

		this.setPromotionType(setupDetails[10]);

		this.setPromotionStatus(setupDetails[11]);

		this.setBookingdateFrom(setupDetails[12]);
		this.setBookingdateTo(setupDetails[13]);

		

	//	 ArrayList<String> DayList = (ArrayList<String>) Arrays.asList(setupDetails[14].split("/"));
		 ArrayList<String> DayList = new ArrayList<String>(Arrays.asList(setupDetails[14].split("/")));
		 for (Iterator<String> iterator = DayList.iterator(); iterator.hasNext();) {
		 String string = (String) iterator.next();
		 addToApplicableDays(DaysOftheWeek.getDaysOftheWeek(string));
		 }
		 
		 return this;

	}
}
