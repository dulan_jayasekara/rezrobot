package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author charitha
 * 
 */

public class DMCSearchObject implements Serializable {

	String testID = "";
	String BookingChannel = "";
	String CountryOfResidence = "";
	String FlightStatus = "";
	String Departure = "";
	String DepartureValue = "";
	String Arrival = "";
	String ArrivalValue = "";
	String DepartureMonth = "";
	String Duration = "";
	String PackageType = "";
	String PackageID = "";
	String DepartureDate = "";
	String NoofRooms = "";

	String Adult = "";
	String Child = "";
	String Infant = "";

	String Ages = "";
	String PaymentType = "";
	String PortalCurrency = "";
	String PaymentGatewayCurrency = "";
	String SellingCurrencymode = "";
	String Paymentmode = "";
	String ExecuteStatus = "";
	String PackageName = "";

	public DMCSearchObject setData(String[] searchDetails) throws ParseException {

		this.setTestID(searchDetails[0]);
		this.setBookingChannel(searchDetails[1]);
		this.setCountryOfResidence(searchDetails[2]);
		this.setFlightStatus(searchDetails[3]);

		this.setDeparture(searchDetails[4]);
		this.setDepartureValue(searchDetails[5]);
		this.setArrival(searchDetails[6]);
		this.setArrivalValue(searchDetails[7]);
		this.setDepartureMonth(searchDetails[8]);
		this.setDuration(searchDetails[9]);
		this.setPackageType(searchDetails[10]);
		this.setPackageID(searchDetails[11]);
		this.setDepartureDate(searchDetails[12]);
		this.setNoofRooms(searchDetails[13]);
		this.setAdult(searchDetails[14]);
		this.setChild(searchDetails[15]);
		this.setInfant(searchDetails[16]);
		this.setAges(searchDetails[17]);

		this.setPaymentType(searchDetails[18]);
		this.setPortalCurrency(searchDetails[19]);
		this.setPaymentGatewayCurrency(searchDetails[20]);
		this.setSellingCurrencymode(searchDetails[21]);
		this.setPaymentmode(searchDetails[22]);
		this.setExecuteStatus(searchDetails[23]);
		this.setPackageName(searchDetails[24]);

		return this;

	}

	public String getInfant() {
		return Infant;
	}

	public void setInfant(String infant) {
		Infant = infant;
	}

	public String getPackageName() {
		return PackageName;
	}

	public void setPackageName(String packageName) {
		PackageName = packageName;
	}

	public String getTestID() {
		return testID;
	}

	public void setTestID(String testID) {
		this.testID = testID;
	}

	public String getBookingChannel() {
		return BookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}

	public String getCountryOfResidence() {
		return CountryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		CountryOfResidence = countryOfResidence;
	}

	public String getFlightStatus() {
		return FlightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		FlightStatus = flightStatus;
	}

	public String getDeparture() {
		return Departure;
	}

	public void setDeparture(String departure) {
		Departure = departure;
	}

	public String getDepartureValue() {
		return DepartureValue;
	}

	public void setDepartureValue(String departureValue) {
		DepartureValue = departureValue;
	}

	public String getArrival() {
		return Arrival;
	}

	public void setArrival(String arrival) {
		Arrival = arrival;
	}

	public String getArrivalValue() {
		return ArrivalValue;
	}

	public void setArrivalValue(String arrivalValue) {
		ArrivalValue = arrivalValue;
	}

	public String getDepartureMonth() {
		return DepartureMonth;
	}

	public void setDepartureMonth(String departureMonth) {
		DepartureMonth = departureMonth;
	}

	public String getDuration() {
		return Duration;
	}

	public void setDuration(String duration) {
		Duration = duration;
	}

	public String getPackageType() {
		return PackageType;
	}

	public void setPackageType(String packageType) {
		PackageType = packageType;
	}

	public String getPackageID() {
		return PackageID;
	}

	public void setPackageID(String packageID) {
		PackageID = packageID;
	}

	public String getDepartureDate() {
		return DepartureDate;
	}

	public void setDepartureDate(String departureDate) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/mm/yyyy");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-mm-yyyy");
		Date originalDate = sdf1.parse(departureDate.trim());
		DepartureDate = sdf2.format(originalDate);
		System.out.println(DepartureDate);
	}

	public String getNoofRooms() {
		return NoofRooms;
	}

	public void setNoofRooms(String noofRooms) {
		NoofRooms = noofRooms;
	}

	public String getPaymentType() {
		return PaymentType;
	}

	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	public String getPortalCurrency() {
		return PortalCurrency;
	}

	public void setPortalCurrency(String portalCurrency) {
		PortalCurrency = portalCurrency;
	}

	public String getPaymentGatewayCurrency() {
		return PaymentGatewayCurrency;
	}

	public void setPaymentGatewayCurrency(String paymentGatewayCurrency) {
		PaymentGatewayCurrency = paymentGatewayCurrency;
	}

	public String getSellingCurrencymode() {
		return SellingCurrencymode;
	}

	public void setSellingCurrencymode(String sellingCurrencymode) {
		SellingCurrencymode = sellingCurrencymode;
	}

	public String getPaymentmode() {
		return Paymentmode;
	}

	public void setPaymentmode(String paymentmode) {
		Paymentmode = paymentmode;
	}

	public String getExecuteStatus() {
		return ExecuteStatus;
	}

	public void setExecuteStatus(String executeStatus) {
		ExecuteStatus = executeStatus;
	}

	public String getAdult() {
		return Adult;
	}

	public void setAdult(String adult) {
		Adult = adult;
	}

	public String getChild() {
		return Child;
	}

	public void setChild(String child) {
		Child = child;
	}

	public String getAges() {
		return Ages;
	}

	public void setAges(String ages) {
		Ages = ages;
	}

}
