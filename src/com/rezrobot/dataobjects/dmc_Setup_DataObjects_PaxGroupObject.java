package com.rezrobot.dataobjects;

public class dmc_Setup_DataObjects_PaxGroupObject {
	
	String paxGroup;
	String minNoOfPax;
	String maxNoOfPax;
	String combinationActive;
	
	
	public String getPaxGroup() {
		return paxGroup;
	}
	public void setPaxGroup(String paxGroup) {
		this.paxGroup = paxGroup;
	}
	public String getMinNoOfPax() {
		return minNoOfPax;
	}
	public void setMinNoOfPax(String minNoOfPax) {
		this.minNoOfPax = minNoOfPax;
	}
	public String getMaxNoOfPax() {
		return maxNoOfPax;
	}
	public void setMaxNoOfPax(String maxNoOfPax) {
		this.maxNoOfPax = maxNoOfPax;
	}
	public String getCombinationActive() {
		return combinationActive;
	}
	public void setCombinationActive(String combinationActive) {
		this.combinationActive = combinationActive;
	}
	
	
	

}
