package com.rezrobot.dataobjects;

public class dmc_Setup_DataObjects_partnerMarkupObject {
	
	String profitMarkupType;
	double profitMarkup;
	String commissionType;
	double commission;
	String partnerName;
	
	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getProfitMarkupType() {
		return profitMarkupType;
	}
	public void setProfitMarkupType(String profitMarkupType) {
		this.profitMarkupType = profitMarkupType;
	}
	public double getProfitMarkup() {
		return profitMarkup;
	}
	public void setProfitMarkup(String profitMarkup) {
		this.profitMarkup = Double.parseDouble(profitMarkup);
	}
	public String getCommissionType() {
		return commissionType;
	}
	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = Double.parseDouble(commission);
	}
	
	
}
