package com.rezrobot.dataobjects;

import java.util.ArrayList;
import java.util.List;


public class dmc_Setup_DataObjects_DMCPackageObject {
	
	
	String portal;
	String url;
	
	dmc_Setup_DataObjects_StandardInfoObject standardInfo=new dmc_Setup_DataObjects_StandardInfoObject();
	dmc_Setup_DataObjects_ContractObject contract=new dmc_Setup_DataObjects_ContractObject();
	dmc_Setup_DataObjects_MarkupObject profitMarkup=new dmc_Setup_DataObjects_MarkupObject();
	dmc_Setup_DataObjects_ContentObject content=new dmc_Setup_DataObjects_ContentObject();
	dmc_Setup_DataObjects_InventoryObject inventory=new dmc_Setup_DataObjects_InventoryObject();
	List<dmc_Setup_DataObjects_RatesObject>  ratelist=new ArrayList<dmc_Setup_DataObjects_RatesObject>();
	
	
	
	

	

	public List<dmc_Setup_DataObjects_RatesObject> getRatelist() {
		return ratelist;
	}

	public void setRatelist(List<dmc_Setup_DataObjects_RatesObject> ratelist) {
		this.ratelist = ratelist;
	}

	public dmc_Setup_DataObjects_InventoryObject getInventory() {
		return inventory;
	}

	public void setInventory(dmc_Setup_DataObjects_InventoryObject inventory) {
		this.inventory = inventory;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public dmc_Setup_DataObjects_ContentObject getContent() {
		return content;
	}

	public void setContent(dmc_Setup_DataObjects_ContentObject content) {
		this.content = content;
	}

	public dmc_Setup_DataObjects_MarkupObject getProfitMarkup() {
		return profitMarkup;
	}

	public void setProfitMarkup(dmc_Setup_DataObjects_MarkupObject profitMarkup) {
		this.profitMarkup = profitMarkup;
	}

	public dmc_Setup_DataObjects_StandardInfoObject getStandardInfo() {
		return standardInfo;
	}

	public void setStandardInfo(dmc_Setup_DataObjects_StandardInfoObject standardInfo) {
		this.standardInfo = standardInfo;
	}

	public dmc_Setup_DataObjects_ContractObject getContract() {
		return contract;
	}

	public void setContract(dmc_Setup_DataObjects_ContractObject contract) {
		this.contract = contract;
	}
	
	
	

}
