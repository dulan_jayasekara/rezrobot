package com.rezrobot.dataobjects;

import java.io.Serializable;

public class CommonBillingInfoRates implements Serializable{

	String currency = "";
	String subtotal = "";
	String totalTaxAndFees = "";
	String amountBeingProcessedNow = "";
	String AmountBeingProcessedByAirline = "";
	String amountDueCheckin = "";
	String totalPG = "";
	String total = "";
	
	
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTotalPG() {
		return totalPG;
	}
	public void setTotalPG(String totalPG) {
		this.totalPG = totalPG;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getTotalTaxAndFees() {
		return totalTaxAndFees;
	}
	public void setTotalTaxAndFees(String totalTaxAndFees) {
		this.totalTaxAndFees = totalTaxAndFees;
	}
	public String getAmountBeingProcessedNow() {
		return amountBeingProcessedNow;
	}
	public void setAmountBeingProcessedNow(String amountBeingProcessedNow) {
		this.amountBeingProcessedNow = amountBeingProcessedNow;
	}
	public String getAmountBeingProcessedByAirline() {
		return AmountBeingProcessedByAirline;
	}
	public void setAmountBeingProcessedByAirline(
			String amountBeingProcessedByAirline) {
		AmountBeingProcessedByAirline = amountBeingProcessedByAirline;
	}
	public String getAmountDueCheckin() {
		return amountDueCheckin;
	}
	public void setAmountDueCheckin(String amountDueCheckin) {
		this.amountDueCheckin = amountDueCheckin;
	}
	
	
}
