package com.rezrobot.dataobjects;

public class CarSearchObject {

	String pickupLocation = "";
	String dropoffLocation = "";
	String pickupDate = "";
	String dropoffDate = "";
	String pickupTime = "";
	String dropoffTime = "";
	String returnToPickUpLocation = "";
	String execute = "";
	String bookingChannel = "";
	
	public CarSearchObject setData()
	{
		/*this.setPickupLocation(searchDetails[0]);
		this.setDropoffLocation(searchDetails[1]);
		this.setPickupDate(searchDetails[2]);
		this.setDropoffDate(searchDetails[3]);
		this.setPickupTime(searchDetails[4]);
		this.setDropoffTime(searchDetails[5]);
		this.setReturnToPickUpLocation(searchDetails[6]);
		this.setExecute(searchDetails[7]);
		this.setBookingChannel(searchDetails[8]);*/
		
		this.setPickupLocation("London Airport Gatwick|1482|1482|London Airport Gatwick||54||United Kingdom");
		this.setDropoffLocation("London Airport Heathrow|1483|1483|London Airport Heathrow||54||United Kingdom");
		this.setPickupDate("11/13/2016");
		this.setDropoffDate("11/14/2016");
		this.setPickupTime("AnyTime");
		this.setDropoffTime("AnyTime");
		this.setReturnToPickUpLocation("no");
		this.setExecute("yes");
		this.setBookingChannel("web");
		
		return this;
	}
	
	
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public String getExecute() {
		return execute;
	}
	public void setExecute(String execute) {
		this.execute = execute;
	}
	public String getPickupLocation() {
		return pickupLocation;
	}
	public void setPickupLocation(String pickupLocation) {
		this.pickupLocation = pickupLocation;
	}
	public String getDropoffLocation() {
		return dropoffLocation;
	}
	public void setDropoffLocation(String dropoffLocation) {
		this.dropoffLocation = dropoffLocation;
	}
	public String getPickupDate() {
		return pickupDate;
	}
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	public String getDropoffDate() {
		return dropoffDate;
	}
	public void setDropoffDate(String dropoffDate) {
		this.dropoffDate = dropoffDate;
	}
	public String getPickupTime() {
		return pickupTime;
	}
	public void setPickupTime(String pickupTime) {
		this.pickupTime = pickupTime;
	}
	public String getDropoffTime() {
		return dropoffTime;
	}
	public void setDropoffTime(String dropoffTime) {
		this.dropoffTime = dropoffTime;
	}
	public String getReturnToPickUpLocation() {
		return returnToPickUpLocation;
	}
	public void setReturnToPickUpLocation(String returnToPickUpLocation) {
		this.returnToPickUpLocation = returnToPickUpLocation;
	}
	
	
}
