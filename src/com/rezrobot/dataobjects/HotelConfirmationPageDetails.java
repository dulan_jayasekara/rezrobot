package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

import com.rezrobot.common.pojo.occupancyDetails;
import com.rezrobot.common.pojo.roomDetails;

public class HotelConfirmationPageDetails implements Serializable{

	String bookingReference = ""; 
	String reservationNumber = ""; 
	String Status = ""; 
	String supplierConfirmationNumber = ""; 
	String chekin = ""; 
	String checkout = ""; 
	String numberOfRooms = ""; 
	String numberOfNights = ""; 
	String estimatedArrivalTime = ""; 
	String hotelSubtotal = ""; 
	String hotelTax = ""; 
	String hotelTotal = ""; 
	String subtotal = ""; 
	String tax = ""; 
	String total = ""; 
	String amountBeingProcessedNow = ""; 
	String amountDueAtCheckin = ""; 
	String firstName = ""; 
	String address1 = ""; 
	String country = ""; 
	String postalCode = ""; 
	String emergencyNumber = ""; 
	String lastName = ""; 
	String city = ""; 
	String state = ""; 
	String phoneNumber = ""; 
	String email = ""; 
	String merchantTrackId = ""; 
	String authenticationReference = ""; 
	String paymentID = "";
	String amountInPGCurrency = ""; 
	String hotelName = ""; 
	String hotelAddress = "";
	String cancellationDeadline = "";
	String currencyCode = "";
	String transactionID = "";
	
	ArrayList<HotelRoomDetails> room = new ArrayList<HotelRoomDetails>();
	ArrayList<hotelOccupancy> adultOccupancy = new ArrayList<hotelOccupancy>();
	ArrayList<hotelOccupancy> childOccupancy = new ArrayList<hotelOccupancy>();
	
	
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCancellationDeadline() {
		return cancellationDeadline;
	}
	public void setCancellationDeadline(String cancellationDeadline) {
		this.cancellationDeadline = cancellationDeadline;
	}
	public ArrayList<hotelOccupancy> getAdultOccupancy() {
		return adultOccupancy;
	}
	public void setAdultOccupancy(ArrayList<hotelOccupancy> adultOccupancy) {
		this.adultOccupancy = adultOccupancy;
	}
	public ArrayList<hotelOccupancy> getChildOccupancy() {
		return childOccupancy;
	}
	public void setChildOccupancy(ArrayList<hotelOccupancy> childOccupancy) {
		this.childOccupancy = childOccupancy;
	}
	public ArrayList<HotelRoomDetails> getRoom() {
		return room;
	}
	public void setRoom(ArrayList<HotelRoomDetails> room) {
		this.room = room;
	}
	public String getBookingReference() {
		return bookingReference;
	}
	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}
	public String getReservationNumber() {
		return reservationNumber;
	}
	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getSupplierConfirmationNumber() {
		return supplierConfirmationNumber;
	}
	public void setSupplierConfirmationNumber(String supplierConfirmationNumber) {
		this.supplierConfirmationNumber = supplierConfirmationNumber;
	}
	public String getChekin() {
		return chekin;
	}
	public void setChekin(String chekin) {
		this.chekin = chekin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getNumberOfRooms() {
		return numberOfRooms;
	}
	public void setNumberOfRooms(String numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}
	public String getNumberOfNights() {
		return numberOfNights;
	}
	public void setNumberOfNights(String numberOfNights) {
		this.numberOfNights = numberOfNights;
	}
	public String getEstimatedArrivalTime() {
		return estimatedArrivalTime;
	}
	public void setEstimatedArrivalTime(String estimatedArrivalTime) {
		this.estimatedArrivalTime = estimatedArrivalTime;
	}
	public String getHotelSubtotal() {
		return hotelSubtotal;
	}
	public void setHotelSubtotal(String hotelSubtotal) {
		this.hotelSubtotal = hotelSubtotal;
	}
	public String getHotelTax() {
		return hotelTax;
	}
	public void setHotelTax(String hotelTax) {
		this.hotelTax = hotelTax;
	}
	public String getHotelTotal() {
		return hotelTotal;
	}
	public void setHotelTotal(String hotelTotal) {
		this.hotelTotal = hotelTotal;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getAmountBeingProcessedNow() {
		return amountBeingProcessedNow;
	}
	public void setAmountBeingProcessedNow(String amountBeingProcessedNow) {
		this.amountBeingProcessedNow = amountBeingProcessedNow;
	}
	public String getAmountDueAtCheckin() {
		return amountDueAtCheckin;
	}
	public void setAmountDueAtCheckin(String amountDueAtCheckin) {
		this.amountDueAtCheckin = amountDueAtCheckin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getEmergencyNumber() {
		return emergencyNumber;
	}
	public void setEmergencyNumber(String emergencyNumber) {
		this.emergencyNumber = emergencyNumber;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMerchantTrackId() {
		return merchantTrackId;
	}
	public void setMerchantTrackId(String merchantTrackId) {
		this.merchantTrackId = merchantTrackId;
	}
	public String getAuthenticationReference() {
		return authenticationReference;
	}
	public void setAuthenticationReference(String authenticationReference) {
		this.authenticationReference = authenticationReference;
	}
	public String getPaymentID() {
		return paymentID;
	}
	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}
	public String getAmountInPGCurrency() {
		return amountInPGCurrency;
	}
	public void setAmountInPGCurrency(String amountInPGCurrency) {
		this.amountInPGCurrency = amountInPGCurrency;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelAddress() {
		return hotelAddress;
	}
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}
	
	
}
