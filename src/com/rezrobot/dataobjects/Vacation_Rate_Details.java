package com.rezrobot.dataobjects;

import java.io.Serializable;

public class Vacation_Rate_Details implements Serializable{

	String hotelValueInSellingCurrency = "";
	String hotelpmInSellingCurrency  = ""; 
	String hotelbfInSellingCurrency = ""; 
	String flightValueInSellingCurrency = ""; 
	String flightTaxInSellingCurrency = ""; 
	String flightpmInSellingCurrency = ""; 
	String flightbfInSellingCurrency = ""; 
	String ccfeeInSellingCurrency = ""; 
	String currencyInSellingCurrency = ""; 
	String subTotalInSellingCurrency = "";
	String totalInSellingCurrency = "";
	String taxInSellingCurrency = "";
	
	String hotelValueInPortalCurrency = "";
	String hotelpmInPortalCurrency  = ""; 
	String hotelbfInPortalCurrency = ""; 
	String flightValueInPortalCurrency = ""; 
	String flightTaxInPortalCurrency = ""; 
	String flightpmInPortalCurrency = ""; 
	String flightbfInPortalCurrency = ""; 
	String ccfeeInPortalCurrency = ""; 
	String currencyInPortalCurrency = ""; 
	String subTotalInPortalCurrency = "";
	String totalInPortalCurrency = "";
	String taxInPortalCurrency = "";
	
	String hotelValueInPGCurrency = "";
	String hotelpmInPGCurrency  = ""; 
	String hotelbfInPGCurrency = ""; 
	String flightValueInPGCurrency = ""; 
	String flightTaxInPGCurrency = ""; 
	String flightpmInPGCurrency = ""; 
	String flightbfInPGCurrency = ""; 
	String ccfeeInPGCurrency = ""; 
	String currencyInPGCurrency = ""; 
	String subTotalInPGCurrency = "";
	String totalInPGCurrency = "";
	String taxInPGCurrency = "";
	
	String hotelCanDeadline = ""; 
	String flightCanDeadline = ""; 
	String canDeadline  = "";
	
	
	
	
	public String getTaxInSellingCurrency() {
		return taxInSellingCurrency;
	}
	public void setTaxInSellingCurrency(String taxInSellingCurrency) {
		this.taxInSellingCurrency = taxInSellingCurrency;
	}
	public String getTaxInPortalCurrency() {
		return taxInPortalCurrency;
	}
	public void setTaxInPortalCurrency(String taxInPortalCurrency) {
		this.taxInPortalCurrency = taxInPortalCurrency;
	}
	public String getTaxInPGCurrency() {
		return taxInPGCurrency;
	}
	public void setTaxInPGCurrency(String taxInPGCurrency) {
		this.taxInPGCurrency = taxInPGCurrency;
	}
	public String getSubTotalInSellingCurrency() {
		return subTotalInSellingCurrency;
	}
	public void setSubTotalInSellingCurrency(String subTotalInSellingCurrency) {
		this.subTotalInSellingCurrency = subTotalInSellingCurrency;
	}
	public String getTotalInSellingCurrency() {
		return totalInSellingCurrency;
	}
	public void setTotalInSellingCurrency(String totalInSellingCurrency) {
		this.totalInSellingCurrency = totalInSellingCurrency;
	}
	public String getSubTotalInPortalCurrency() {
		return subTotalInPortalCurrency;
	}
	public void setSubTotalInPortalCurrency(String subTotalInPortalCurrency) {
		this.subTotalInPortalCurrency = subTotalInPortalCurrency;
	}
	public String getTotalInPortalCurrency() {
		return totalInPortalCurrency;
	}
	public void setTotalInPortalCurrency(String totalInPortalCurrency) {
		this.totalInPortalCurrency = totalInPortalCurrency;
	}
	public String getSubTotalInPGCurrency() {
		return subTotalInPGCurrency;
	}
	public void setSubTotalInPGCurrency(String subTotalInPGCurrency) {
		this.subTotalInPGCurrency = subTotalInPGCurrency;
	}
	public String getTotalInPGCurrency() {
		return totalInPGCurrency;
	}
	public void setTotalInPGCurrency(String totalInPGCurrency) {
		this.totalInPGCurrency = totalInPGCurrency;
	}
	public String getHotelValueInSellingCurrency() {
		return hotelValueInSellingCurrency;
	}
	public void setHotelValueInSellingCurrency(String hotelValueInSellingCurrency) {
		this.hotelValueInSellingCurrency = hotelValueInSellingCurrency;
	}
	public String getHotelpmInSellingCurrency() {
		return hotelpmInSellingCurrency;
	}
	public void setHotelpmInSellingCurrency(String hotelpmInSellingCurrency) {
		this.hotelpmInSellingCurrency = hotelpmInSellingCurrency;
	}
	public String getHotelbfInSellingCurrency() {
		return hotelbfInSellingCurrency;
	}
	public void setHotelbfInSellingCurrency(String hotelbfInSellingCurrency) {
		this.hotelbfInSellingCurrency = hotelbfInSellingCurrency;
	}
	public String getFlightValueInSellingCurrency() {
		return flightValueInSellingCurrency;
	}
	public void setFlightValueInSellingCurrency(String flightValueInSellingCurrency) {
		this.flightValueInSellingCurrency = flightValueInSellingCurrency;
	}
	public String getFlightTaxInSellingCurrency() {
		return flightTaxInSellingCurrency;
	}
	public void setFlightTaxInSellingCurrency(String flightTaxInSellingCurrency) {
		this.flightTaxInSellingCurrency = flightTaxInSellingCurrency;
	}
	public String getFlightpmInSellingCurrency() {
		return flightpmInSellingCurrency;
	}
	public void setFlightpmInSellingCurrency(String flightpmInSellingCurrency) {
		this.flightpmInSellingCurrency = flightpmInSellingCurrency;
	}
	public String getFlightbfInSellingCurrency() {
		return flightbfInSellingCurrency;
	}
	public void setFlightbfInSellingCurrency(String flightbfInSellingCurrency) {
		this.flightbfInSellingCurrency = flightbfInSellingCurrency;
	}
	public String getCcfeeInSellingCurrency() {
		return ccfeeInSellingCurrency;
	}
	public void setCcfeeInSellingCurrency(String ccfeeInSellingCurrency) {
		this.ccfeeInSellingCurrency = ccfeeInSellingCurrency;
	}
	public String getCurrencyInSellingCurrency() {
		return currencyInSellingCurrency;
	}
	public void setCurrencyInSellingCurrency(String currencyInSellingCurrency) {
		this.currencyInSellingCurrency = currencyInSellingCurrency;
	}
	public String getHotelValueInPortalCurrency() {
		return hotelValueInPortalCurrency;
	}
	public void setHotelValueInPortalCurrency(String hotelValueInPortalCurrency) {
		this.hotelValueInPortalCurrency = hotelValueInPortalCurrency;
	}
	public String getHotelpmInPortalCurrency() {
		return hotelpmInPortalCurrency;
	}
	public void setHotelpmInPortalCurrency(String hotelpmInPortalCurrency) {
		this.hotelpmInPortalCurrency = hotelpmInPortalCurrency;
	}
	public String getHotelbfInPortalCurrency() {
		return hotelbfInPortalCurrency;
	}
	public void setHotelbfInPortalCurrency(String hotelbfInPortalCurrency) {
		this.hotelbfInPortalCurrency = hotelbfInPortalCurrency;
	}
	public String getFlightValueInPortalCurrency() {
		return flightValueInPortalCurrency;
	}
	public void setFlightValueInPortalCurrency(String flightValueInPortalCurrency) {
		this.flightValueInPortalCurrency = flightValueInPortalCurrency;
	}
	public String getFlightTaxInPortalCurrency() {
		return flightTaxInPortalCurrency;
	}
	public void setFlightTaxInPortalCurrency(String flightTaxInPortalCurrency) {
		this.flightTaxInPortalCurrency = flightTaxInPortalCurrency;
	}
	public String getFlightpmInPortalCurrency() {
		return flightpmInPortalCurrency;
	}
	public void setFlightpmInPortalCurrency(String flightpmInPortalCurrency) {
		this.flightpmInPortalCurrency = flightpmInPortalCurrency;
	}
	public String getFlightbfInPortalCurrency() {
		return flightbfInPortalCurrency;
	}
	public void setFlightbfInPortalCurrency(String flightbfInPortalCurrency) {
		this.flightbfInPortalCurrency = flightbfInPortalCurrency;
	}
	public String getCcfeeInPortalCurrency() {
		return ccfeeInPortalCurrency;
	}
	public void setCcfeeInPortalCurrency(String ccfeeInPortalCurrency) {
		this.ccfeeInPortalCurrency = ccfeeInPortalCurrency;
	}
	public String getCurrencyInPortalCurrency() {
		return currencyInPortalCurrency;
	}
	public void setCurrencyInPortalCurrency(String currencyInPortalCurrency) {
		this.currencyInPortalCurrency = currencyInPortalCurrency;
	}
	public String getHotelValueInPGCurrency() {
		return hotelValueInPGCurrency;
	}
	public void setHotelValueInPGCurrency(String hotelValueInPGCurrency) {
		this.hotelValueInPGCurrency = hotelValueInPGCurrency;
	}
	public String getHotelpmInPGCurrency() {
		return hotelpmInPGCurrency;
	}
	public void setHotelpmInPGCurrency(String hotelpmInPGCurrency) {
		this.hotelpmInPGCurrency = hotelpmInPGCurrency;
	}
	public String getHotelbfInPGCurrency() {
		return hotelbfInPGCurrency;
	}
	public void setHotelbfInPGCurrency(String hotelbfInPGCurrency) {
		this.hotelbfInPGCurrency = hotelbfInPGCurrency;
	}
	public String getFlightValueInPGCurrency() {
		return flightValueInPGCurrency;
	}
	public void setFlightValueInPGCurrency(String flightValueInPGCurrency) {
		this.flightValueInPGCurrency = flightValueInPGCurrency;
	}
	public String getFlightTaxInPGCurrency() {
		return flightTaxInPGCurrency;
	}
	public void setFlightTaxInPGCurrency(String flightTaxInPGCurrency) {
		this.flightTaxInPGCurrency = flightTaxInPGCurrency;
	}
	public String getFlightpmInPGCurrency() {
		return flightpmInPGCurrency;
	}
	public void setFlightpmInPGCurrency(String flightpmInPGCurrency) {
		this.flightpmInPGCurrency = flightpmInPGCurrency;
	}
	public String getFlightbfInPGCurrency() {
		return flightbfInPGCurrency;
	}
	public void setFlightbfInPGCurrency(String flightbfInPGCurrency) {
		this.flightbfInPGCurrency = flightbfInPGCurrency;
	}
	public String getCcfeeInPGCurrency() {
		return ccfeeInPGCurrency;
	}
	public void setCcfeeInPGCurrency(String ccfeeInPGCurrency) {
		this.ccfeeInPGCurrency = ccfeeInPGCurrency;
	}
	public String getCurrencyInPGCurrency() {
		return currencyInPGCurrency;
	}
	public void setCurrencyInPGCurrency(String currencyInPGCurrency) {
		this.currencyInPGCurrency = currencyInPGCurrency;
	}
	public String getHotelCanDeadline() {
		return hotelCanDeadline;
	}
	public void setHotelCanDeadline(String hotelCanDeadline) {
		this.hotelCanDeadline = hotelCanDeadline;
	}
	public String getFlightCanDeadline() {
		return flightCanDeadline;
	}
	public void setFlightCanDeadline(String flightCanDeadline) {
		this.flightCanDeadline = flightCanDeadline;
	}
	public String getCanDeadline() {
		return canDeadline;
	}
	public void setCanDeadline(String canDeadline) {
		this.canDeadline = canDeadline;
	}
	
	
	
	
}
