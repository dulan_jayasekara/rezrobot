package com.rezrobot.dataobjects;

import java.io.Serializable;



public class HotelSearchObject implements Serializable{

	String country = ""; 
	String destination = ""; 
	String checkin = ""; 
	String nights = ""; 
	String rooms = ""; 
	String adults = ""; 
	String children = ""; 
	String age = ""; 
	String channel = ""; 
	String hotel = ""; 
	String supplier = ""; 
	String executeStatus = ""; 
	String quotation = ""; 
	String searchAgain = ""; 
	String to = ""; 
	String onlineOrOffline = ""; 
	String cancel = ""; 
	String searchAgainNights = ""; 
	String searchAgainRooms = ""; 
	String searchAgainAdults = ""; 
	String searchAgainChildren = ""; 
	String searchAgainCheckin = ""; 
	String discount = ""; 
	String discountNumber = ""; 
	String value = ""; 
	String discountApplyIn = ""; 
	String toType = ""; 
	String checkOtherProducts = ""; 
	String payOffline = ""; 
	String continueIfRateChange = ""; 
	String filter = ""; 
	String customerLogin;
	
	public HotelSearchObject setData(String[] searchDetails)
	{
		this.setCountry(searchDetails[0]);
		this.setDestination(searchDetails[1]); 
		this.setCheckin(searchDetails[2]); 
		this.setNights(searchDetails[3]); 
		this.setRooms(searchDetails[4]);
		this.setAdults(searchDetails[5]); 
		this.setChildren(searchDetails[6]); 
		this.setAge(searchDetails[7]); 
		this.setChannel(searchDetails[8]); 
		this.setHotel(searchDetails[9]); 
		this.setSupplier(searchDetails[10]);
		this.setExecuteStatus(searchDetails[11]); 
		this.setQuotation(searchDetails[12]); 
		this.setSearchAgain(searchDetails[13]); 
		this.setTo(searchDetails[14]); 
		this.setOnlineOrOffline(searchDetails[15]);
		this.setCancel(searchDetails[16]); 
		this.setSearchAgainNights(searchDetails[17]); 
		this.setSearchAgainRooms(searchDetails[18]); 
		this.setSearchAgainAdults(searchDetails[19]);
		this.setSearchAgainChildren(searchDetails[20]); 
		this.setSearchAgainCheckin(searchDetails[21]); 
		this.setDiscount(searchDetails[22]); 
		this.setDiscountNumber(searchDetails[23]);
		this.setValue(searchDetails[24]); 
		this.setDiscountApplyIn(searchDetails[25]); 
		this.setToType(searchDetails[26]); 
		this.setCheckOtherProducts(searchDetails[27]); 
		this.setPayOffline(searchDetails[28]);
		this.setContinueIfRateChange(searchDetails[29]); 
		this.setFilter(searchDetails[30]); 
		this.setCustomerLogin(searchDetails[31]);
		
		return this;
	}
	

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getHotel() {
		return hotel;
	}
	public void setHotel(String hotel) {
		this.hotel = hotel;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getExecuteStatus() {
		return executeStatus;
	}
	public void setExecuteStatus(String executeStatus) {
		this.executeStatus = executeStatus;
	}
	public String getQuotation() {
		return quotation;
	}
	public void setQuotation(String quotation) {
		this.quotation = quotation;
	}
	public String getSearchAgain() {
		return searchAgain;
	}
	public void setSearchAgain(String searchAgain) {
		this.searchAgain = searchAgain;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getOnlineOrOffline() {
		return onlineOrOffline;
	}
	public void setOnlineOrOffline(String onlineOrOffline) {
		this.onlineOrOffline = onlineOrOffline;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getSearchAgainNights() {
		return searchAgainNights;
	}
	public void setSearchAgainNights(String searchAgainNights) {
		this.searchAgainNights = searchAgainNights;
	}
	public String getSearchAgainRooms() {
		return searchAgainRooms;
	}
	public void setSearchAgainRooms(String searchAgainRooms) {
		this.searchAgainRooms = searchAgainRooms;
	}
	public String getSearchAgainAdults() {
		return searchAgainAdults;
	}
	public void setSearchAgainAdults(String searchAgainAdults) {
		this.searchAgainAdults = searchAgainAdults;
	}
	public String getSearchAgainChildren() {
		return searchAgainChildren;
	}
	public void setSearchAgainChildren(String searchAgainChildren) {
		this.searchAgainChildren = searchAgainChildren;
	}
	public String getSearchAgainCheckin() {
		return searchAgainCheckin;
	}
	public void setSearchAgainCheckin(String searchAgainCheckin) {
		this.searchAgainCheckin = searchAgainCheckin;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getDiscountNumber() {
		return discountNumber;
	}
	public void setDiscountNumber(String discountNumber) {
		this.discountNumber = discountNumber;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDiscountApplyIn() {
		return discountApplyIn;
	}
	public void setDiscountApplyIn(String discountApplyIn) {
		this.discountApplyIn = discountApplyIn;
	}
	public String getToType() {
		return toType;
	}
	public void setToType(String toType) {
		this.toType = toType;
	}
	public String getCheckOtherProducts() {
		return checkOtherProducts;
	}
	public void setCheckOtherProducts(String checkOtherProducts) {
		this.checkOtherProducts = checkOtherProducts;
	}
	public String getPayOffline() {
		return payOffline;
	}
	public void setPayOffline(String payOffline) {
		this.payOffline = payOffline;
	}
	public String getContinueIfRateChange() {
		return continueIfRateChange;
	}
	public void setContinueIfRateChange(String continueIfRateChange) {
		this.continueIfRateChange = continueIfRateChange;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getCustomerLogin() {
		return customerLogin;
	}
	public void setCustomerLogin(String customerLogin) {
		this.customerLogin = customerLogin;
	} 
	
	
}
