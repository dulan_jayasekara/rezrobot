package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class Common_upselling implements Serializable{

	String location  	= ""; 
	String checkin   	= ""; 
	String checkout  	= ""; 
	String nights 		= ""; 
	String rooms 		= "";
	ArrayList<String> hotelAdults 	= new ArrayList<String>();
	ArrayList<String> hotelChildren = new ArrayList<String>();
	ArrayList<String> hotelAge 		= new ArrayList<String>();
	
	String from 			= ""; 
	String to 				= ""; 
	String departureDate 	= ""; 
	String departureTime 	= ""; 
	String returnDate 		= ""; 
	String returnTime 		= ""; 
	String adults 			= ""; 
	String children 		= ""; 
	String infants 			= "";
	
	boolean roundTrip   = false;
	boolean onewayTrip  = false;
	boolean multiTrip   = false;
	
	public boolean isRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}
	public boolean isOnewayTrip() {
		return onewayTrip;
	}
	public void setOnewayTrip(boolean onewayTrip) {
		this.onewayTrip = onewayTrip;
	}
	public boolean isMultiTrip() {
		return multiTrip;
	}
	public void setMultiTrip(boolean multiTrip) {
		this.multiTrip = multiTrip;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public ArrayList<String> getHotelAdults() {
		return hotelAdults;
	}
	public void setHotelAdults(ArrayList<String> hotelAdults) {
		this.hotelAdults = hotelAdults;
	}
	public ArrayList<String> getHotelChildren() {
		return hotelChildren;
	}
	public void setHotelChildren(ArrayList<String> hotelChildren) {
		this.hotelChildren = hotelChildren;
	}
	public ArrayList<String> getHotelAge() {
		return hotelAge;
	}
	public void setHotelAge(ArrayList<String> hotelAge) {
		this.hotelAge = hotelAge;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getReturnTime() {
		return returnTime;
	}
	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getInfants() {
		return infants;
	}
	public void setInfants(String infants) {
		this.infants = infants;
	}
	
	
}
