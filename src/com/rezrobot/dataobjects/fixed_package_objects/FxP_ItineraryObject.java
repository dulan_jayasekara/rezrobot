package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_ItineraryObject {

	/**
	 * @param args
	 */
	
	String day;
	String image;
	String itineraryDetail;
	String title;
	
	
	
	public String getDay() {
		return day;
	}



	public void setDay(String day) {
		this.day = day;
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



	public String getItineraryDetail() {
		return itineraryDetail;
	}



	public void setItineraryDetail(String itineraryDetail) {
		this.itineraryDetail = itineraryDetail;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



}
