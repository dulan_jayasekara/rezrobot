package com.rezrobot.dataobjects.fixed_package_objects;

import java.util.ArrayList;

public class FxP_Program {

	/**
	 * @param args
	 */
	
	String ProgramID;
	String ProgramName;
	String SupplierName ;
	String Currency	;
	String Country;
	String City	;
	String ContractType	;
	double CommissionAmount;
	double SalesTax	;
	double Miscellnus;
	int CancellationDays;
	double Cancellationamount;
	double NoshowFee;
	
	
	ArrayList<FxP_Activity> activityList=new ArrayList<>();
	
	FxP_Activity assignedActivity=new FxP_Activity();
	

	public String getProgramID() {
		return ProgramID;
	}



	public void setProgramID(String programID) {
		ProgramID = programID;
	}



	public String getProgramName() {
		return ProgramName;
	}



	public void setProgramName(String programName) {
		ProgramName = programName;
	}



	public String getSupplierName() {
		return SupplierName;
	}



	public void setSupplierName(String supplierName) {
		SupplierName = supplierName;
	}



	public String getCurrency() {
		return Currency;
	}



	public void setCurrency(String currency) {
		Currency = currency;
	}



	public String getCountry() {
		return Country;
	}



	public void setCountry(String country) {
		Country = country;
	}



	public String getCity() {
		return City;
	}



	public void setCity(String city) {
		City = city;
	}



	public String getContractType() {
		return ContractType;
	}



	public void setContractType(String contractType) {
		ContractType = contractType;
	}



	public double getCommissionAmount() {
		return CommissionAmount;
	}



	public void setCommissionAmount(String commissionAmount) {
		CommissionAmount = Double.parseDouble(commissionAmount);
	}



	public double getSalesTax() {
		return SalesTax;
	}



	public void setSalesTax(String salesTax) {
		SalesTax = Double.parseDouble(salesTax);
	}



	public double getMiscellnus() {
		return Miscellnus;
	}



	public void setMiscellnus(String miscellnus) {
		Miscellnus = Double.parseDouble(miscellnus);
	}



	public int getCancellationDays() {
		return CancellationDays;
	}



	public void setCancellationDays(String cancellationDays) {
		CancellationDays = Integer.parseInt(cancellationDays);
	}



	public double getCancellationamount() {
		return Cancellationamount;
	}



	public void setCancellationamount(String cancellationamount) {
		Cancellationamount = Double.parseDouble(cancellationamount);
	}



	public double getNoshowFee() {
		return NoshowFee;
	}



	public void setNoshowFee(String noshowFee) {
		NoshowFee = Double.parseDouble(noshowFee);
	}

	


	public ArrayList<FxP_Activity> getActivityList() {
		return activityList;
	}



	public void setActivityList(ArrayList<FxP_Activity> activityList) {
		this.activityList = activityList;
	}

	
	


	public FxP_Activity getAssignedActivity() {
		return assignedActivity;
	}



	public void setAssignedActivity(FxP_Activity assignedActivity) {
		this.assignedActivity = assignedActivity;
	}



}
