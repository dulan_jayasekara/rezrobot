package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_testObject {

	/**
	 * @param args
	 */
	
	String testid;
	String partnertype;
	String bookingChannel;
	String Url;
	String UserName;
	String Password;	
	String partnerName;
	String DefaultPassword;
	
	
	String BookingChannel;
	String BookingEngineTests;
	String PackageInfoPopUps;
	String FlightSpecificTests;
	String HotelSpecificTests;
	String ItineraryTests;
	String Mails;
	String Reports;
	String Cancellation;
	
	
	
	public String getTestid() {
		return testid;
	}



	public void setTestid(String testid) {
		this.testid = testid;
	}



	public String getPartnertype() {
		return partnertype;
	}



	public void setPartnertype(String partnertype) {
		this.partnertype = partnertype;
	}



	public String getBookingChannel() {
		return bookingChannel;
	}



	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}



	public String getUrl() {
		return Url;
	}



	public void setUrl(String url) {
		Url = url;
	}



	public String getUserName() {
		return UserName;
	}



	public void setUserName(String userName) {
		UserName = userName;
	}



	public String getPassword() {
		return Password;
	}



	public void setPassword(String password) {
		Password = password;
	}



	public String getPartnerName() {
		return partnerName;
	}



	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}



	public String getDefaultPassword() {
		return DefaultPassword;
	}



	public void setDefaultPassword(String defaultPassword) {
		DefaultPassword = defaultPassword;
	}



	


	public String getBookingEngineTests() {
		return BookingEngineTests;
	}



	public void setBookingEngineTests(String bookingEngineTests) {
		BookingEngineTests = bookingEngineTests;
	}



	public String getPackageInfoPopUps() {
		return PackageInfoPopUps;
	}



	public void setPackageInfoPopUps(String packageInfoPopUps) {
		PackageInfoPopUps = packageInfoPopUps;
	}



	public String getFlightSpecificTests() {
		return FlightSpecificTests;
	}



	public void setFlightSpecificTests(String flightSpecificTests) {
		FlightSpecificTests = flightSpecificTests;
	}



	public String getHotelSpecificTests() {
		return HotelSpecificTests;
	}



	public void setHotelSpecificTests(String hotelSpecificTests) {
		HotelSpecificTests = hotelSpecificTests;
	}



	public String getItineraryTests() {
		return ItineraryTests;
	}



	public void setItineraryTests(String itineraryTests) {
		ItineraryTests = itineraryTests;
	}



	public String getMails() {
		return Mails;
	}



	public void setMails(String mails) {
		Mails = mails;
	}



	public String getReports() {
		return Reports;
	}



	public void setReports(String reports) {
		Reports = reports;
	}



	public String getCancellation() {
		return Cancellation;
	}



	public void setCancellation(String cancellation) {
		Cancellation = cancellation;
	}



}
