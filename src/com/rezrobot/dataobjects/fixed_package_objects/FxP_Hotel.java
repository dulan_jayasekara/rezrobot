package com.rezrobot.dataobjects.fixed_package_objects;

import java.util.ArrayList;

public class FxP_Hotel {

	/**
	 * @param args
	 */
	
	String HotelID;
	String HotelName;
	String SupplierName;
	String Currency;
	String Country;
	String City;
	String Address1; 
	String Address2	;
	String RateType	;
	String ContractType;	
	double CommissionAmount;
	double SalesTax;
	double EnergyTax	;
	double OccupancyTax	;
	double Miscellonus ;
	String CancellationDays;
	double Cancellationamount	;
	double NoshowFee;
	
	FxP_Room RoomSelected=new FxP_Room();
	
	ArrayList<FxP_Room> roomlist=new ArrayList<>();
	
	
	
	
	
	
	

	public String getHotelID() {
		return HotelID;
	}








	public void setHotelID(String hotelID) {
		HotelID = hotelID;
	}








	public String getHotelName() {
		return HotelName;
	}








	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}








	public String getSupplierName() {
		return SupplierName;
	}








	public void setSupplierName(String supplierName) {
		SupplierName = supplierName;
	}








	public String getCurrency() {
		return Currency;
	}








	public void setCurrency(String currency) {
		Currency = currency;
	}








	public String getCountry() {
		return Country;
	}








	public void setCountry(String country) {
		Country = country;
	}








	public String getCity() {
		return City;
	}








	public void setCity(String city) {
		City = city;
	}








	public String getAddress1() {
		return Address1;
	}








	public void setAddress1(String address1) {
		Address1 = address1;
	}








	public String getAddress2() {
		return Address2;
	}








	public void setAddress2(String address2) {
		Address2 = address2;
	}








	public String getRateType() {
		return RateType;
	}








	public void setRateType(String rateType) {
		RateType = rateType;
	}








	public String getContractType() {
		return ContractType;
	}








	public void setContractType(String contractType) {
		ContractType = contractType;
	}








	public double getCommissionAmount() {
		return CommissionAmount;
	}








	public void setCommissionAmount(String commissionAmount) {
		CommissionAmount = Double.parseDouble(commissionAmount);
	}








	public double getSalesTax() {
		return SalesTax;
	}








	public void setSalesTax(String salesTax) {
		SalesTax = Double.parseDouble(salesTax);
	}








	public double getEnergyTax() {
		return EnergyTax;
	}








	public void setEnergyTax(String energyTax) {
		EnergyTax = Double.parseDouble(energyTax);
	}








	public double getOccupancyTax() {
		return OccupancyTax;
	}








	public void setOccupancyTax(String occupancyTax) {
		OccupancyTax = Double.parseDouble(occupancyTax);
	}








	public double getMiscellonus() {
		return Miscellonus;
	}








	public void setMiscellonus(String miscellonus) {
		Miscellonus = Double.parseDouble(miscellonus);
	}








	public String getCancellationDays() {
		return CancellationDays;
	}








	public void setCancellationDays(String cancellationDays) {
		CancellationDays = cancellationDays;
	}








	public double getCancellationamount() {
		return Cancellationamount;
	}








	public void setCancellationamount(String cancellationamount) {
		Cancellationamount = Double.parseDouble(cancellationamount);
	}








	public double getNoshowFee() {
		return NoshowFee;
	}








	public void setNoshowFee(String noshowFee) {
		NoshowFee = Double.parseDouble(noshowFee);
	}








	public ArrayList<FxP_Room> getRoomlist() {
		return roomlist;
	}








	public void setRoomlist(ArrayList<FxP_Room> roomlist) {
		this.roomlist = roomlist;
	}








	public FxP_Room getRoomSelected() {
		return RoomSelected;
	}








	public void setRoomSelected(FxP_Room roomSelected) {
		RoomSelected = roomSelected;
	}


}
