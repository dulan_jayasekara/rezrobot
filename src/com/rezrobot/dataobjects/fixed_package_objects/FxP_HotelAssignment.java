package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_HotelAssignment {

	/**
	 * @param args
	 */
	
	String PackageName;
	String GroupID;
	String Assigneddate	;
	int NumberOfDays;
	String HotelID;
	String RoomID;	
	String hotelchange;
	String roomchange;
	
	
	
	FxP_Hotel assignedHotel=new FxP_Hotel();
	
	

	public String getPackageName() {
		return PackageName;
	}



	public void setPackageName(String packageName) {
		PackageName = packageName;
	}



	public String getGroupID() {
		return GroupID;
	}



	public void setGroupID(String groupID) {
		GroupID = groupID;
	}



	public String getAssigneddate() {
		return Assigneddate;
	}



	public void setAssigneddate(String assigneddate) {
		Assigneddate = assigneddate;
	}



	public int getNumberOfDays() {
		return NumberOfDays;
	}



	public void setNumberOfDays(String numberOfDays) {
		NumberOfDays = Integer.parseInt(numberOfDays);
	}



	public String getHotelID() {
		return HotelID;
	}



	public void setHotelID(String hotelID) {
		HotelID = hotelID;
	}



	public String getRoomID() {
		return RoomID;
	}



	public void setRoomID(String roomID) {
		RoomID = roomID;
	}



	public String getHotelchange() {
		return hotelchange;
	}



	public void setHotelchange(String hotelchange) {
		this.hotelchange = hotelchange;
	}



	public String getRoomchange() {
		return roomchange;
	}



	public void setRoomchange(String roomchange) {
		this.roomchange = roomchange;
	}



	public FxP_Hotel getAssignedHotel() {
		return assignedHotel;
	}



	public void setAssignedHotel(FxP_Hotel assignedHotel) {
		this.assignedHotel = assignedHotel;
	}

}
