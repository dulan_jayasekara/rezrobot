package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_Room {

	/**
	 * @param args
	 */
	String HotelID;
	String RoomID	;
	String RoomType	;
	String BedType	;
	String Rateplan	;
	int stdAdultCount;
	int AddAdultCount;
	int ChildCount;
	double stdRate;
	double addAdultRate	;
	double childRate;
	
	
	

	public String getHotelID() {
		return HotelID;
	}




	public void setHotelID(String hotelID) {
		HotelID = hotelID;
	}




	public String getRoomID() {
		return RoomID;
	}




	public void setRoomID(String roomID) {
		RoomID = roomID;
	}




	public String getRoomType() {
		return RoomType;
	}




	public void setRoomType(String roomType) {
		RoomType = roomType;
	}




	public String getBedType() {
		return BedType;
	}




	public void setBedType(String bedType) {
		BedType = bedType;
	}




	public String getRateplan() {
		return Rateplan;
	}




	public void setRateplan(String rateplan) {
		Rateplan = rateplan;
	}




	public int getStdAdultCount() {
		return stdAdultCount;
	}




	public void setStdAdultCount(String stdAdultCount) {
		this.stdAdultCount = Integer.parseInt(stdAdultCount);
	}




	public int getAddAdultCount() {
		return AddAdultCount;
	}




	public void setAddAdultCount(String addAdultCount) {
		AddAdultCount =Integer.parseInt(addAdultCount);
	}




	public int getChildCount() {
		return ChildCount;
	}




	public void setChildCount(String childCount) {
		ChildCount = Integer.parseInt(childCount);
	}




	public double getStdRate() {
		return stdRate;
	}




	public void setStdRate(String stdRate) {
		this.stdRate = Double.parseDouble(stdRate);
	}




	public double getAddAdultRate() {
		return addAdultRate;
	}




	public void setAddAdultRate(String addAdultRate) {
		this.addAdultRate = Double.parseDouble(addAdultRate);
	}




	public double getChildRate() {
		return childRate;
	}




	public void setChildRate(String childRate) {
		this.childRate = Double.parseDouble(childRate);
	}



}
