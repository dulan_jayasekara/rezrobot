package com.rezrobot.dataobjects.fixed_package_objects;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.rezrobot.utill.ExcelReader;





public class FxP_DataReader {

	private static  String file_Path = "Resources/Input/textTest2.xls";

	public ArrayList<FxP_InputObject> inputObjectConstruct() {

		ArrayList<FxP_InputObject> inputArray=new ArrayList<>();

		ArrayList<FxP_testObject> testList=testScenarioRead();

		Iterator<FxP_testObject> testiterator=testList.iterator();

		while (testiterator.hasNext()) {
			String testID="";
			FxP_InputObject input=new FxP_InputObject();
			input.setTest(testiterator.next());
			testID=input.getTest().getTestid();
			input.setSearch(searchObjectReader(testID));



			inputArray.add(input);


		}


		return inputArray;
	}

	public FxP_Moreinfo moreinfoReader(String packageName) {

		FxP_Moreinfo moreinfo=new FxP_Moreinfo();
		Map<Integer,String>testMap=new ExcelReader().init("Resources/Input/textTest.xls").get(14);

		//Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(14);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");

			System.out.println(AllValues[0]);
			if (AllValues[0].contains(packageName)) {

				moreinfo.setPackageName(AllValues[0]);
				moreinfo.setShortDescription(AllValues[1]);
				moreinfo.setPackageInclusion(AllValues[2]);
				moreinfo.setPackageExclusion(AllValues[3]);
				moreinfo.setDestinationDetails(AllValues[4]);
				moreinfo.setSpecialnotes(AllValues[5]);
				moreinfo.setContactDetails(AllValues[6]);
				moreinfo.setTermsandconditions(AllValues[7]);
				moreinfo.setThumbnailpicture(AllValues[8]);
				moreinfo.setImagearray(AllValues[9]);



			}

		}

		return moreinfo;


	}

	public ArrayList<FxP_ItineraryObject> itineraryReader(int NumberofpackageDays) {

		ArrayList<FxP_ItineraryObject> itineraryList=new ArrayList<>();
		int t=0;
		Map<Integer,String>testMap=new ExcelReader().init("Resources/Input/textTest.xls").get(15);

		//Map<Integer,String>testMap=new ExcelReader().init(file_Path)get(15);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext() && t<NumberofpackageDays)
		{
			String[] AllValues=it.next().getValue().split(",");

			FxP_ItineraryObject itinerary=new FxP_ItineraryObject();
			itinerary.setDay(AllValues[0]);
			itinerary.setImage(AllValues[1]);
			itinerary.setItineraryDetail(AllValues[2]);
			itinerary.setTitle(AllValues[3]);

			itineraryList.add(t, itinerary);
			t++;

		}
		return itineraryList;

	}


	public ArrayList<FxP_testObject> testScenarioRead() {

		ArrayList<FxP_testObject> testList=new ArrayList<FxP_testObject>();

		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(0);

		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");
			FxP_testObject testobject=new FxP_testObject();

			testobject.setTestid(AllValues[0]);
			testobject.setPartnertype(AllValues[1]);
			testobject.setBookingChannel(AllValues[2]);
			testobject.setUrl(AllValues[3]);
			testobject.setUserName(AllValues[4]);
			testobject.setPassword(AllValues[5]);
			testobject.setPartnerName(AllValues[6]);
			testobject.setBookingEngineTests(AllValues[7]);
			testobject.setPackageInfoPopUps(AllValues[8]);
			testobject.setFlightSpecificTests(AllValues[9]);
			testobject.setHotelSpecificTests(AllValues[10]);
			testobject.setItineraryTests(AllValues[11]);
			testobject.setMails(AllValues[12]);
			testobject.setReports(AllValues[13]);
			testobject.setCancellation(AllValues[14]);



			testList.add(testobject);

		}


		return testList;


	}


	public FxP_SearchObject searchObjectReader(String TestID) {


		FxP_SearchObject searchobj=new FxP_SearchObject();

		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(1);

		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");
			
			if (AllValues[0].equals(TestID)) {
				
			
			
//testID	Booking Channel	Country Of Residence	Flight Status	 Departure	Departure Value	Arrival	Arrival Value	Departure Month	Duration	Package Type	Package ID	Departure Date	No of Rooms	Occupancy	PaymentType	PortalCurrency	PaymentGateway Currency	Selling Currency mode 	Payment mode 

			searchobj.setTestId(AllValues[0]);
			searchobj.setBookingChannel(AllValues[1]);
			searchobj.setCountryOfResidence(AllValues[2]);
			searchobj.setFlightStatus(AllValues[3]);
			searchobj.setDeparture(AllValues[4]);
			searchobj.setDepartureValue(AllValues[5]);
			searchobj.setArrival(AllValues[6]);
			searchobj.setArrivalValue(AllValues[7]);
			searchobj.setDepartureMonth(AllValues[8]);
			searchobj.setDuration(AllValues[9]);
			searchobj.setPackageType(AllValues[10]);
			searchobj.setPackageID(AllValues[11]);
			searchobj.setDepartureDate(AllValues[12]);
			searchobj.setNoofRooms(AllValues[13]);

				int adultCount=0;
				int childCount=0;
				int infantCount=0;

				int T=AllValues[14].split("R").length;
				System.out.println(T);
				ArrayList<FxP_RoomOccupancy> Roomlist=new ArrayList<FxP_RoomOccupancy>();
				
				for (int i = 0; i < T; i++) {

					FxP_RoomOccupancy OCCObj=new FxP_RoomOccupancy();
				
					OCCObj.setAdultCount(AllValues[14].split("R")[i].split("/")[0]);
					OCCObj.setChildcount(AllValues[14].split("R")[i].split("/")[1]);
					OCCObj.setInfantcount(AllValues[14].split("R")[i].split("/")[2]);

					adultCount=adultCount+OCCObj.getAdultCount();
					childCount=childCount+OCCObj.getChildcount();
					infantCount=infantCount+OCCObj.getInfantcount();


					Roomlist.add(OCCObj);
				}
System.out.println(Roomlist.size());
				searchobj.setRoomOccupancy(Roomlist);

				searchobj.setAdultcount(adultCount);
				searchobj.setChildCount(childCount);
				searchobj.setInfantcount(infantCount);
				/*
				searchobj.setPaymentType(AllValues[15]);

				searchobj.setPortalCurrency(AllValues[16]);
				searchobj.setSellingCurrency(AllValues[17]);
				searchobj.setPaymentGatewayCurrency(AllValues[18]);
				Calendar calendarobject=Calendar.getInstance();
				Date date=calendarobject.getTime();
				searchobj.setReservationDate(date);

				String selectedOrigin;
				if (searchobj.getFlightStatus().contains("Including Flight")) {
					selectedOrigin=searchobj.getDeparture();
				}else {
					selectedOrigin="Land orgin";
				}
				String countryOfResidence=searchobj.getDeparture();

				String selectedRegion;

				if (countryOfResidence.contains("Sri Lanka")) {
					selectedRegion="Asia";
				}else {
					selectedRegion="All";
				}
				searchobj.setSelectedOrigin(selectedOrigin);
				searchobj.setSelectedRegion(selectedRegion);*/



			}



		}


		//	System.out.println(search.getSelectedRegion());
		return searchobj;	


	}


	public FxP_PackageObject packageObjectConstruct(String packageID) {

		FxP_PackageObject selectedPackage=new FxP_PackageObject();
		FxP_PackageObject PackageQ=new FxP_PackageObject();
		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(2);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");

			if (AllValues[0].equals(packageID)) {




				PackageQ.setPackageID(AllValues[0]);
				PackageQ.setPackageName(AllValues[1]);
				PackageQ.setPackageOrigin(AllValues[2]);
				PackageQ.setPackageDestination(AllValues[3]);
				PackageQ.setPackageType(AllValues[4]);
				PackageQ.setBookingChannel(AllValues[5]);
				PackageQ.setActiveStatus(AllValues[6]);
				PackageQ.setProductTypes(AllValues[7]);
				PackageQ.setBookingperiodFrom(AllValues[8]);
				PackageQ.setBookingPeriodTo(AllValues[9]);
				PackageQ.setStayperiodFrom(AllValues[10]);
				PackageQ.setStayperiodTo(AllValues[11]);
				PackageQ.setRefundableStatus(AllValues[12]);
				PackageQ.setStdcancellationdates(AllValues[13]);
				PackageQ.setStdcancellation(AllValues[14]);
				/*	PackageQ.setNoshowcancellation(AllValues[15]);
			PackageQ.setDCMarkup(AllValues[16]);
			PackageQ.setTOProfitMarkup(AllValues[17]);
			PackageQ.setTOComCommission(AllValues[18]);
			PackageQ.setAFFMarkup(AllValues[19]);
			PackageQ.setNumberofDays(AllValues[20]);
			PackageQ.setNumberofGroups(AllValues[21]);
			PackageQ.setHotelbookingFee(AllValues[22]);
			PackageQ.setAirbookingFee(AllValues[23]);
			PackageQ.setCreditCardFee(AllValues[24]);
			PackageQ.setPredefinedType(AllValues[25]);*/


				PackageQ.setHotelList(HotelAssignmentRead(PackageQ.getPackageID()));
PackageQ.setActivitylist(ActivityAssignmentRead(PackageQ.getPackageID()));
				/*PredefObjectRead predefreader=new PredefObjectRead();*/



				/*try {
				if (selectedPackage.getPackageType().contains("Pre Defined")) {

					selectedPackage.setPredefObject(predefreader.predefdataReader(filePath, packageID));

				}
			} catch (Exception e) {
				// TODO: handle exception
			}*/

			}

		}


		return PackageQ;

	}

	public ArrayList<FxP_HotelAssignment> HotelAssignmentRead(String packageID) {


		ArrayList<FxP_HotelAssignment> hot_assignmentList=new ArrayList<>();

		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(3);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");


			if (AllValues[0].equals(packageID)) {
				FxP_HotelAssignment hotAssigned=new FxP_HotelAssignment();


				hotAssigned.setPackageName(AllValues[0]);
				hotAssigned.setGroupID(AllValues[1]);
				hotAssigned.setAssigneddate(AllValues[2]);
				hotAssigned.setNumberOfDays(AllValues[3]);
				hotAssigned.setHotelID(AllValues[4]);
				hotAssigned.setRoomID(AllValues[5]);
				hotAssigned.setHotelchange(AllValues[6]);
				hotAssigned.setRoomchange(AllValues[7]);

				hotAssigned.setAssignedHotel(HotelRead(hotAssigned.getPackageName(), hotAssigned.getHotelID(), hotAssigned.getRoomID()));

				hot_assignmentList.add(hotAssigned);

			}


		}
		return hot_assignmentList;

	}


	public FxP_Hotel HotelRead(String packageID,String hotelId, String RoomId) {

		FxP_Hotel assignedHotel=new FxP_Hotel();
		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(5);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");
			if (AllValues[0].equalsIgnoreCase(hotelId)) {

				assignedHotel.setHotelID(AllValues[0]);
				assignedHotel.setHotelName(AllValues[1]);
				assignedHotel.setSupplierName(AllValues[2]) 	;
				assignedHotel.setCurrency(AllValues[3])	;
				assignedHotel.setCountry(AllValues[4]);
				assignedHotel.setCity(AllValues[5]);
				assignedHotel.setAddress1(AllValues[6]);
				assignedHotel.setAddress2(AllValues[7]);
				assignedHotel.setRateType(AllValues[8]);
				assignedHotel.setContractType(AllValues[9]);
				assignedHotel.setCommissionAmount(AllValues[10]);
				assignedHotel.setSalesTax(AllValues[11]);
				assignedHotel.setEnergyTax(AllValues[12]);	
				assignedHotel.setOccupancyTax(AllValues[13]);
				assignedHotel.setMiscellonus(AllValues[14]);
				assignedHotel.setCancellationDays(AllValues[15])	;
				assignedHotel.setCancellationamount(AllValues[16]);
				assignedHotel.setNoshowFee(AllValues[17]);

				assignedHotel.setRoomSelected(Roomread(hotelId, RoomId));


			}



		}
		return assignedHotel;
	}

	public FxP_Room Roomread(String hotelId, String RoomId) {

		FxP_Room assignedroom=new FxP_Room();
		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(6);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();

		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");

			if ((AllValues[0].equals(hotelId))&&(AllValues[1].contains(RoomId))) {

				assignedroom.setHotelID(AllValues[0]);
				assignedroom.setRoomID(AllValues[1]);

				assignedroom.setRoomType(AllValues[2]);
				assignedroom.setBedType(AllValues[3]);
				assignedroom.setRateplan(AllValues[4]);
				assignedroom.setStdAdultCount(AllValues[5]);
				assignedroom.setAddAdultCount(AllValues[6]);
				assignedroom.setChildCount(AllValues[7]);
				assignedroom.setStdRate(AllValues[8]);
				assignedroom.setAddAdultRate(AllValues[9]);
				assignedroom.setChildRate(AllValues[10]);

			}
		}

		return assignedroom;
	}


	public ArrayList<FxP_Activityassignment> ActivityAssignmentRead(String packageID) {
		ArrayList<FxP_Activityassignment> Act_assignmentList=new ArrayList<>();

		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(4);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");

			if (AllValues[0].equalsIgnoreCase(packageID)) {


				FxP_Activityassignment act=new FxP_Activityassignment();
				act.setPackagename(AllValues[0]);
				act.setAssignedDate(AllValues[1]);
				act.setProgramID(AllValues[2]);
				act.setActivityID(AllValues[3]);

System.out.println(act.getProgramID()+ act.getActivityID());
				act.setProgram(ProgramRead(act.getProgramID(), act.getActivityID()));


				Act_assignmentList.add(act);

			}





		}

		return Act_assignmentList;



	}

	public FxP_Program ProgramRead(String ProgramID, String ActivityID) {

		FxP_Program program=new FxP_Program();

		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(7);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();


		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");
			
			if (AllValues[0].equals(ProgramID)) {


				program.setProgramID(AllValues[0]);
				program.setProgramName(AllValues[1]);
				program.setSupplierName(AllValues[2]);
				program.setCurrency(AllValues[3]);
				program.setCountry(AllValues[4]);
				program.setCity(AllValues[5]);
				program.setContractType(AllValues[6]);
				program.setCommissionAmount(AllValues[7]);
				program.setSalesTax(AllValues[8]);
				program.setMiscellnus(AllValues[9]);
				program.setCancellationDays(AllValues[10]);
				program.setCancellationamount(AllValues[11]);
				program.setNoshowFee(AllValues[12]);

				program.setAssignedActivity(ActivityRead(program.getProgramID(), ActivityID));

			}

		}


		return program;



	}

	public FxP_Activity ActivityRead(String ProgramID, String ActivityID) {
		FxP_Activity act=new FxP_Activity();
		Map<Integer,String>testMap=new ExcelReader().init(file_Path).get(8);
		Iterator<Map.Entry<Integer, String>> it = testMap.entrySet().iterator();

		while(it.hasNext())
		{
			String[] AllValues=it.next().getValue().split(",");
		
			if ((AllValues[0].equals(ProgramID))&&( AllValues[1].equals(ActivityID))) {
				act.setProgramID(AllValues[0]);
				act.setActivityID(AllValues[1]);
				act.setActivityType(AllValues[2]);
				act.setPeriod(AllValues[3]);
				act.setRateplan(AllValues[4]);
				act.setRate(AllValues[5]);
			}



		}
		return act;




	}

}
