package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_RoomOccupancy {
	
	int adultCount;
	int childcount;
	int childage1;
	int childage2;
	int childage3;
	int childage4;
	int infantcount;
	
	
	
	public int getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = Integer.parseInt(adultCount);
	}
	public int getChildcount() {
		return childcount;
	}
	public void setChildcount(String childcount) {
		this.childcount = Integer.parseInt(childcount);
	}
	public int getChildage1() {
		return childage1;
	}
	public void setChildage1(String childage1) {
		this.childage1 = Integer.parseInt(childage1);
	}
	public int getChildage2() {
		return childage2;
	}
	public void setChildage2(String childage2) {
		this.childage2 = Integer.parseInt(childage2);
	}
	public int getChildage3() {
		return childage3;
	}
	public void setChildage3(String childage3) {
		this.childage3 = Integer.parseInt(childage3);
	}
	public int getChildage4() {
		return childage4;
	}
	public void setChildage4(String childage4) {
		this.childage4 = Integer.parseInt(childage4);
	}
	public int getInfantcount() {
		return infantcount;
	}
	public void setInfantcount(String infantcount) {
		this.infantcount = Integer.parseInt(infantcount);
	}
	
	

}
