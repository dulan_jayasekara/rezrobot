package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_CustomerObject {

	String PartnerType;
	String Title	;
	String Firstname;
	String LastName	;
	String AddressOne;
	String AddressTwo	;
	String City	;
	String Country	;
	String State;
	String PostalCode	;
	String PhoneCode;
	String PhoneNumber;
	String Email;
	String Email2	;
	String Fax;
	
	

	public String getPartnerType() {
		return PartnerType;
	}



	public void setPartnerType(String partnerType) {
		PartnerType = partnerType;
	}



	public String getTitle() {
		return Title;
	}



	public void setTitle(String title) {
		Title = title;
	}



	public String getFirstname() {
		return Firstname;
	}



	public void setFirstname(String firstname) {
		Firstname = firstname;
	}



	public String getLastName() {
		return LastName;
	}



	public void setLastName(String lastName) {
		LastName = lastName;
	}



	public String getAddressOne() {
		return AddressOne;
	}



	public void setAddressOne(String addressOne) {
		AddressOne = addressOne;
	}



	public String getAddressTwo() {
		return AddressTwo;
	}



	public void setAddressTwo(String addressTwo) {
		AddressTwo = addressTwo;
	}



	public String getCity() {
		return City;
	}



	public void setCity(String city) {
		City = city;
	}



	public String getCountry() {
		return Country;
	}



	public void setCountry(String country) {
		Country = country;
	}



	public String getState() {
		return State;
	}



	public void setState(String state) {
		State = state;
	}



	public String getPostalCode() {
		return PostalCode;
	}



	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}



	public String getPhoneCode() {
		return PhoneCode;
	}



	public void setPhoneCode(String phoneCode) {
		PhoneCode = phoneCode;
	}



	public String getPhoneNumber() {
		return PhoneNumber;
	}



	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}



	public String getEmail() {
		return Email;
	}



	public void setEmail(String email) {
		Email = email;
	}



	public String getEmail2() {
		return Email2;
	}



	public void setEmail2(String email2) {
		Email2 = email2;
	}



	public String getFax() {
		return Fax;
	}



	public void setFax(String fax) {
		Fax = fax;
	}




}
