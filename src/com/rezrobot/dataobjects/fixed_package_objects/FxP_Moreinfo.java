package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_Moreinfo {

	/**
	 * @param args
	 */
	
	String packageName;
	String shortDescription;
	String 	PackageInclusion;
	String PackageExclusion;
	String DestinationDetails;
	String Specialnotes;
	String ContactDetails;
	String Termsandconditions;
	String Thumbnailpicture;
	String Imagearray;
	
	
	

	public String getPackageName() {
		return packageName;
	}




	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}




	public String getShortDescription() {
		return shortDescription;
	}




	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}




	public String getPackageInclusion() {
		return PackageInclusion;
	}




	public void setPackageInclusion(String packageInclusion) {
		PackageInclusion = packageInclusion;
	}




	public String getPackageExclusion() {
		return PackageExclusion;
	}




	public void setPackageExclusion(String packageExclusion) {
		PackageExclusion = packageExclusion;
	}




	public String getDestinationDetails() {
		return DestinationDetails;
	}




	public void setDestinationDetails(String destinationDetails) {
		DestinationDetails = destinationDetails;
	}




	public String getSpecialnotes() {
		return Specialnotes;
	}




	public void setSpecialnotes(String specialnotes) {
		Specialnotes = specialnotes;
	}




	public String getContactDetails() {
		return ContactDetails;
	}




	public void setContactDetails(String contactDetails) {
		ContactDetails = contactDetails;
	}




	public String getTermsandconditions() {
		return Termsandconditions;
	}




	public void setTermsandconditions(String termsandconditions) {
		Termsandconditions = termsandconditions;
	}




	public String getThumbnailpicture() {
		return Thumbnailpicture;
	}




	public void setThumbnailpicture(String thumbnailpicture) {
		Thumbnailpicture = thumbnailpicture;
	}




	public String getImagearray() {
		return Imagearray;
	}




	public void setImagearray(String imagearray) {
		Imagearray = imagearray;
	}


}
