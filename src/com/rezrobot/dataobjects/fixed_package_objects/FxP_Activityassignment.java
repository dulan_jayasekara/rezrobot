package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_Activityassignment {

	/**
	 * @param args
	 */
	
	String Packagename;
	String AssignedDate	;
	String ProgramID	;
	String ActivityID;
	
	FxP_Program Program=new FxP_Program();
	
	

	public String getPackagename() {
		return Packagename;
	}



	public void setPackagename(String packagename) {
		Packagename = packagename;
	}



	public String getAssignedDate() {
		return AssignedDate;
	}



	public void setAssignedDate(String assignedDate) {
		AssignedDate = assignedDate;
	}



	public String getProgramID() {
		return ProgramID;
	}



	public void setProgramID(String programID) {
		ProgramID = programID;
	}



	public String getActivityID() {
		return ActivityID;
	}



	public void setActivityID(String activityID) {
		ActivityID = activityID;
	}

	
	


	public FxP_Program getProgram() {
		return Program;
	}



	public void setProgram(FxP_Program program) {
		Program = program;
	}




}
