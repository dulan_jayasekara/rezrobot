package com.rezrobot.dataobjects.fixed_package_objects;

public class FxP_OccupantObject {

	String Title;
	String FirstName;
	String LastName;
	String Passport;
	String PassportExpire;
	String PassportIssue;
	String Dob;
	String Sex;
	String Nationality;
	String IssueCountry;
	

	public String getTitle() {
		return Title;
	}


	public void setTitle(String title) {
		Title = title;
	}


	public String getFirstName() {
		return FirstName;
	}


	public void setFirstName(String firstName) {
		FirstName = firstName;
	}


	public String getLastName() {
		return LastName;
	}


	public void setLastName(String lastName) {
		LastName = lastName;
	}


	public String getPassport() {
		return Passport;
	}


	public void setPassport(String passport) {
		Passport = passport;
	}


	public String getPassportExpire() {
		return PassportExpire;
	}


	public void setPassportExpire(String passportExpire) {
		PassportExpire = passportExpire;
	}


	public String getPassportIssue() {
		return PassportIssue;
	}


	public void setPassportIssue(String passportIssue) {
		PassportIssue = passportIssue;
	}


	public String getDob() {
		return Dob;
	}


	public void setDob(String dob) {
		Dob = dob;
	}


	public String getSex() {
		return Sex;
	}


	public void setSex(String sex) {
		Sex = sex;
	}


	public String getNationality() {
		return Nationality;
	}


	public void setNationality(String nationality) {
		Nationality = nationality;
	}


	public String getIssueCountry() {
		return IssueCountry;
	}


	public void setIssueCountry(String issueCountry) {
		IssueCountry = issueCountry;
	}


}
