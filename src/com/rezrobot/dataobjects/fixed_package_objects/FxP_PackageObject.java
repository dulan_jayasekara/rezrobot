package com.rezrobot.dataobjects.fixed_package_objects;

import java.util.ArrayList;

//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class FxP_PackageObject {
	
	String packageID;
	String packageName;
	String packageOrigin;
	String packageDestination;
	String packageType;
	String BookingChannel;
	String ActiveStatus;
	String ProductTypes;
	String bookingperiodFrom;
	String BookingPeriodTo;
	String StayperiodFrom;
	String StayperiodTo;
	String RefundableStatus;
	String stdcancellationdates;
	String stdcancellation;
	double noshowcancellation;
	double DCMarkup;
	double TOProfitMarkup;
	double TOComCommission;
	double AFFMarkup;
	
	double hotelbookingFee;
	double airbookingFee;
	double creditCardFee;
	
	
	int numberofDays;
	int numberofGroups;
	
	ArrayList<FxP_HotelAssignment> hotelList=new ArrayList<>();
	ArrayList<FxP_Activityassignment> activitylist= new ArrayList<>();
	
	
	
	

	public String getPackageID() {
		return packageID;
	}




	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}




	public String getPackageName() {
		return packageName;
	}




	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}




	public String getPackageOrigin() {
		return packageOrigin;
	}




	public void setPackageOrigin(String packageOrigin) {
		this.packageOrigin = packageOrigin;
	}




	public String getPackageDestination() {
		return packageDestination;
	}




	public void setPackageDestination(String packageDestination) {
		this.packageDestination = packageDestination;
	}




	public String getPackageType() {
		return packageType;
	}




	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}




	public String getBookingChannel() {
		return BookingChannel;
	}




	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}




	public String getActiveStatus() {
		return ActiveStatus;
	}




	public void setActiveStatus(String activeStatus) {
		ActiveStatus = activeStatus;
	}




	public String getProductTypes() {
		return ProductTypes;
	}




	public void setProductTypes(String productTypes) {
		ProductTypes = productTypes;
	}




	public String getBookingperiodFrom() {
		return bookingperiodFrom;
	}




	public void setBookingperiodFrom(String bookingperiodFrom) {
		this.bookingperiodFrom = bookingperiodFrom;
	}




	public String getBookingPeriodTo() {
		return BookingPeriodTo;
	}




	public void setBookingPeriodTo(String bookingPeriodTo) {
		BookingPeriodTo = bookingPeriodTo;
	}




	public String getStayperiodFrom() {
		return StayperiodFrom;
	}




	public void setStayperiodFrom(String stayperiodFrom) {
		StayperiodFrom = stayperiodFrom;
	}




	public String getStayperiodTo() {
		return StayperiodTo;
	}




	public void setStayperiodTo(String stayperiodTo) {
		StayperiodTo = stayperiodTo;
	}




	public String getRefundableStatus() {
		return RefundableStatus;
	}




	public void setRefundableStatus(String refundableStatus) {
		RefundableStatus = refundableStatus;
	}




	public String getStdcancellationdates() {
		return stdcancellationdates;
	}




	public void setStdcancellationdates(String stdcancellationdates) {
		this.stdcancellationdates = stdcancellationdates;
	}




	public String getStdcancellation() {
		return stdcancellation;
	}




	public void setStdcancellation(String stdcancellation) {
		this.stdcancellation = stdcancellation;
	}




	public double getNoshowcancellation() {
		return noshowcancellation;
	}




	public void setNoshowcancellation(double noshowcancellation) {
		this.noshowcancellation = noshowcancellation;
	}




	public double getDCMarkup() {
		return DCMarkup;
	}




	public void setDCMarkup(double dCMarkup) {
		DCMarkup = dCMarkup;
	}




	public double getTOProfitMarkup() {
		return TOProfitMarkup;
	}




	public void setTOProfitMarkup(double tOProfitMarkup) {
		TOProfitMarkup = tOProfitMarkup;
	}




	public double getTOComCommission() {
		return TOComCommission;
	}




	public void setTOComCommission(double tOComCommission) {
		TOComCommission = tOComCommission;
	}




	public double getAFFMarkup() {
		return AFFMarkup;
	}




	public void setAFFMarkup(double aFFMarkup) {
		AFFMarkup = aFFMarkup;
	}




	public double getHotelbookingFee() {
		return hotelbookingFee;
	}




	public void setHotelbookingFee(double hotelbookingFee) {
		this.hotelbookingFee = hotelbookingFee;
	}




	public double getAirbookingFee() {
		return airbookingFee;
	}




	public void setAirbookingFee(double airbookingFee) {
		this.airbookingFee = airbookingFee;
	}




	public double getCreditCardFee() {
		return creditCardFee;
	}




	public void setCreditCardFee(double creditCardFee) {
		this.creditCardFee = creditCardFee;
	}




	public int getNumberofDays() {
		return numberofDays;
	}




	public void setNumberofDays(int numberofDays) {
		this.numberofDays = numberofDays;
	}




	public int getNumberofGroups() {
		return numberofGroups;
	}




	public void setNumberofGroups(int numberofGroups) {
		this.numberofGroups = numberofGroups;
	}




	public ArrayList<FxP_HotelAssignment> getHotelList() {
		return hotelList;
	}




	public void setHotelList(ArrayList<FxP_HotelAssignment> hotelList) {
		this.hotelList = hotelList;
	}




	public ArrayList<FxP_Activityassignment> getActivitylist() {
		return activitylist;
	}




	public void setActivitylist(ArrayList<FxP_Activityassignment> activitylist) {
		this.activitylist = activitylist;
	}



}
