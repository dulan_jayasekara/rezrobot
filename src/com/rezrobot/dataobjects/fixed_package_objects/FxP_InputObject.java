package com.rezrobot.dataobjects.fixed_package_objects;


public class FxP_InputObject {
	
	
	FxP_testObject test=new FxP_testObject();
	FxP_SearchObject search=new FxP_SearchObject();
	FxP_PackageObject packageObj=new FxP_PackageObject();
	
	
	
	
	
	public FxP_testObject getTest() {
		return test;
	}




	public void setTest(FxP_testObject test) {
		this.test = test;
	}




	public FxP_SearchObject getSearch() {
		return search;
	}




	public void setSearch(FxP_SearchObject search) {
		this.search = search;
	}




	public FxP_PackageObject getPackageObj() {
		return packageObj;
	}




	public void setPackageObj(FxP_PackageObject packageObj) {
		this.packageObj = packageObj;
	}



}
