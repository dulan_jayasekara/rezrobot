package com.rezrobot.dataobjects.fixed_package_objects;

import java.util.ArrayList;
import java.util.Date;



public class FxP_SearchObject {
	
	String testId;
	String BookingChannel;
	String CountryOfResidence;
	String FlightStatus;
	String Departure;
	String DepartureValue;
	String Arrival;
	String ArrivalValue;
	String DepartureMonth;
	String Duration;
	String PackageType;
	String PackageID;
	String DepartureDate;
	int NoofRooms;
	Date reservationDate;
	
	String selectedOrigin;
	String selectedRegion;
	
	
	int Adultcount;
	int childCount;
	int infantcount;
	
	ArrayList<FxP_RoomOccupancy> RoomOccupancy=new ArrayList<>();
	
	String PaymentType;
	String PortalCurrency;
	String SellingCurrency;
	String PaymentGatewayCurrency;
	
	

	public String getTestId() {
		return testId;
	}



	public void setTestId(String testId) {
		this.testId = testId;
	}



	public String getBookingChannel() {
		return BookingChannel;
	}



	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}



	public String getCountryOfResidence() {
		return CountryOfResidence;
	}



	public void setCountryOfResidence(String countryOfResidence) {
		CountryOfResidence = countryOfResidence;
	}



	public String getFlightStatus() {
		return FlightStatus;
	}



	public void setFlightStatus(String flightStatus) {
		FlightStatus = flightStatus;
	}



	public String getDeparture() {
		return Departure;
	}



	public void setDeparture(String departure) {
		Departure = departure;
	}



	public String getDepartureValue() {
		return DepartureValue;
	}



	public void setDepartureValue(String departureValue) {
		DepartureValue = departureValue;
	}



	public String getArrival() {
		return Arrival;
	}



	public void setArrival(String arrival) {
		Arrival = arrival;
	}



	public String getArrivalValue() {
		return ArrivalValue;
	}



	public void setArrivalValue(String arrivalValue) {
		ArrivalValue = arrivalValue;
	}



	public String getDepartureMonth() {
		return DepartureMonth;
	}



	public void setDepartureMonth(String departureMonth) {
		DepartureMonth = departureMonth;
	}



	public String getDuration() {
		return Duration;
	}



	public void setDuration(String duration) {
		Duration = duration;
	}



	public String getPackageType() {
		return PackageType;
	}



	public void setPackageType(String packageType) {
		PackageType = packageType;
	}



	public String getPackageID() {
		return PackageID;
	}



	public void setPackageID(String packageID) {
		PackageID = packageID;
	}



	public String getDepartureDate() {
		return DepartureDate;
	}



	public void setDepartureDate(String departureDate) {
		DepartureDate = departureDate;
	}



	public int getNoofRooms() {
		return NoofRooms;
	}



	public void setNoofRooms(String noofRooms) {
		NoofRooms = Integer.parseInt(noofRooms);
	}



	public Date getReservationDate() {
		return reservationDate;
	}



	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}



	public String getSelectedOrigin() {
		return selectedOrigin;
	}



	public void setSelectedOrigin(String selectedOrigin) {
		this.selectedOrigin = selectedOrigin;
	}



	public String getSelectedRegion() {
		return selectedRegion;
	}



	public void setSelectedRegion(String selectedRegion) {
		this.selectedRegion = selectedRegion;
	}



	public int getAdultcount() {
		return Adultcount;
	}



	public void setAdultcount(int adultcount) {
		Adultcount = adultcount;
	}



	public int getChildCount() {
		return childCount;
	}



	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}



	public int getInfantcount() {
		return infantcount;
	}



	public void setInfantcount(int infantcount) {
		this.infantcount = infantcount;
	}



	public String getPaymentType() {
		return PaymentType;
	}



	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}



	public String getPortalCurrency() {
		return PortalCurrency;
	}



	public void setPortalCurrency(String portalCurrency) {
		PortalCurrency = portalCurrency;
	}



	public String getSellingCurrency() {
		return SellingCurrency;
	}



	public void setSellingCurrency(String sellingCurrency) {
		SellingCurrency = sellingCurrency;
	}



	public String getPaymentGatewayCurrency() {
		return PaymentGatewayCurrency;
	}



	public void setPaymentGatewayCurrency(String paymentGatewayCurrency) {
		PaymentGatewayCurrency = paymentGatewayCurrency;
	}
	
	
	



	public ArrayList<FxP_RoomOccupancy> getRoomOccupancy() {
		return RoomOccupancy;
	}



	public void setRoomOccupancy(ArrayList<FxP_RoomOccupancy> roomOccupancy) {
		RoomOccupancy = roomOccupancy;
	}


}
