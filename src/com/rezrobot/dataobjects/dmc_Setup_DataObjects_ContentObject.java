package com.rezrobot.dataobjects;

import java.util.ArrayList;
import java.util.List;

public class dmc_Setup_DataObjects_ContentObject {
	
	String shortDescription; 	
	String packageInclusion ;	
	String packageExclusion ;	
	String destinationDetails ;	
	String specialNotes 	;
	String contactDetailsonArrival 	;
	String termsandConditions;
	String packageCurrencyCode;
	String landSectorPrice ;
	String airSectorPrice;
	
	List<dmc_Setup_DataObjects_ItineraryObject> itineraryDayWise=new ArrayList<dmc_Setup_DataObjects_ItineraryObject>();
	List<dmc_Setup_DataObjects_Image> imagelist=new ArrayList<dmc_Setup_DataObjects_Image>();
	
	
	
	
	
	public List<dmc_Setup_DataObjects_Image> getImagelist() {
		return imagelist;
	}
	public void setImagelist(List<dmc_Setup_DataObjects_Image> imagelist) {
		this.imagelist = imagelist;
	}
	public String getAirSectorPrice() {
		return airSectorPrice;
	}
	public void setAirSectorPrice(String airSectorPrice) {
		this.airSectorPrice = airSectorPrice;
	}
	public List<dmc_Setup_DataObjects_ItineraryObject> getItineraryDayWise() {
		return itineraryDayWise;
	}
	public void setItineraryDayWise(List<dmc_Setup_DataObjects_ItineraryObject> itineraryDayWise) {
		this.itineraryDayWise = itineraryDayWise;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getPackageInclusion() {
		return packageInclusion;
	}
	public void setPackageInclusion(String packageInclusion) {
		this.packageInclusion = packageInclusion;
	}
	public String getPackageExclusion() {
		return packageExclusion;
	}
	public void setPackageExclusion(String packageExclusion) {
		this.packageExclusion = packageExclusion;
	}
	public String getDestinationDetails() {
		return destinationDetails;
	}
	public void setDestinationDetails(String destinationDetails) {
		this.destinationDetails = destinationDetails;
	}
	public String getSpecialNotes() {
		return specialNotes;
	}
	public void setSpecialNotes(String specialNotes) {
		this.specialNotes = specialNotes;
	}
	public String getContactDetailsonArrival() {
		return contactDetailsonArrival;
	}
	public void setContactDetailsonArrival(String contactDetailsonArrival) {
		this.contactDetailsonArrival = contactDetailsonArrival;
	}
	public String getTermsandConditions() {
		return termsandConditions;
	}
	public void setTermsandConditions(String termsandConditions) {
		this.termsandConditions = termsandConditions;
	}
	public String getPackageCurrencyCode() {
		return packageCurrencyCode;
	}
	public void setPackageCurrencyCode(String packageCurrencyCode) {
		this.packageCurrencyCode = packageCurrencyCode;
	}
	public String getLandSectorPrice() {
		return landSectorPrice;
	}
	public void setLandSectorPrice(String landSectorPrice) {
		this.landSectorPrice = landSectorPrice;
	}
	
	

}
