package com.rezrobot.dataobjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Activity_LoadDetails {
	
	Map<Integer, String> SearchMap 		= null;
	Map<Integer, String> CustomerMap 	= null;
	Map<Integer, String> InventoryTypeMap 	= null;

	public Activity_LoadDetails(ArrayList<Map<Integer, String>> sheetlist){
		
		SearchMap          		= sheetlist.get(0);
		CustomerMap          	= sheetlist.get(1);
		InventoryTypeMap		= sheetlist.get(2);
	}
	
	public TreeMap<String, Activity_WEB_Search> loadActivityReservation(Map<Integer, String> searchInfoDeMap){
		
		Iterator<Map.Entry<Integer, String>> it = searchInfoDeMap.entrySet().iterator();
		TreeMap<String, Activity_WEB_Search> activitySearchInfoMap = new TreeMap<String, Activity_WEB_Search>();
		
		while(it.hasNext()) {
			
			Activity_WEB_Search search = new Activity_WEB_Search();
			String[] values = it.next().getValue().split(",");
			
			search.setScenarioCount(values[0]);
			search.setSellingCurrency(values[1]);
			search.setCountry(values[2]);
			search.setDestination(values[3]);
			search.setDateFrom(values[4]);
			search.setDateTo(values[5]);
			search.setAdults(values[6]);
			search.setChildren(values[7]);
			search.setAgeOfChildren(values[8]);
			search.setProgramCategory(values[9]);
			search.setPreferCurrency(values[10]);
			search.setPromotionCode(values[11]);
			search.setActivityName(values[12]);
			search.setActivityDate(values[13]);
			search.setScenariotype(values[14]);
			search.setBooking_channel(values[15]);
			search.setQuotationReq(values[16]);
			
			activitySearchInfoMap.put(values[0], search);
		}
		
		return activitySearchInfoMap;
	}	
	
	public TreeMap<String, Activity_WEB_Search> loadPaymentDetails(Map<Integer, String> paymentsMapDe, TreeMap<String, Activity_WEB_Search> searchInfoDeMap) {

		Iterator<Map.Entry<Integer, String>> it = paymentsMapDe.entrySet().iterator();
		
		while (it.hasNext()) {
			
			Activity_WEB_PaymentDetails paymentDetails = new Activity_WEB_PaymentDetails();
			String[] values = it.next().getValue().split(",");

			paymentDetails.setScenarioId(values[0]);
			paymentDetails.setCustomerTitle(values[1]);
			paymentDetails.setCustomerName(values[2]);
			paymentDetails.setCustomerLastName(values[3]);
			paymentDetails.setTel(values[4]);
			paymentDetails.setEmail(values[5]);
			paymentDetails.setAddress(values[6]);
			paymentDetails.setAddress_1(values[7]);
			paymentDetails.setCountry(values[8]);
			paymentDetails.setCity(values[9]);
			paymentDetails.setState(values[10]);
			paymentDetails.setPostalCode(values[11]);
						
			try {				
				searchInfoDeMap.get(values[0]).addPaymentInfo(paymentDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return searchInfoDeMap;
	}
	
	
	public TreeMap<String, Activity_WEB_Search> loadActivityInventoryRecords(Map<Integer, String> inventoryMap, TreeMap<String, Activity_WEB_Search> searchInfoDeMap) {
		
		Iterator<Map.Entry<Integer, String>> it = inventoryMap.entrySet().iterator();
		
		while (it.hasNext()) {
			
			Activity_WEB_ActivityInfo activityInventory = new Activity_WEB_ActivityInfo();
			String[] values = it.next().getValue().split(",");
			
			activityInventory.setScenarioCount(values[0]);
			activityInventory.setActivityName(values[1]);
			activityInventory.setSupplier_Currency(values[2]);
			activityInventory.setActivityType(values[3]);
						
			activityInventory.setPaymentType(values[4]);
			activityInventory.setCreditCardFee(values[5]);
			activityInventory.setProfir_Markup(values[6]);	
			activityInventory.setStandardCancell_Type(values[7]);
			activityInventory.setStandardCancell_Value(values[8]);	
			activityInventory.setNoShow_Type(values[9]);
			activityInventory.setNoShow_Value(values[10]);
			
			activityInventory.setApplyIfLessThan(values[11]);	
			activityInventory.setCancelBuffer(values[12]);
			activityInventory.setActivityNetRate(values[13]);
			activityInventory.setActivity_SalesTaxType(values[14]);
			activityInventory.setActivity_SalesTaxValue(values[15]);
			activityInventory.setActivity_MisFeeType(values[16]);
			activityInventory.setActivity_MisFeeValue(values[17]);
			
			activityInventory.setSupplierName(values[18]);	
			activityInventory.setSupplierAddress(values[19]);	
			activityInventory.setSupplierTP(values[20]);	
			activityInventory.setInventoryType(values[21]);	
			activityInventory.setActivityDescription(values[22]);
			activityInventory.setUserTypeDC_B2B(values[23]);
			activityInventory.setToActivityPM(values[24]);
			activityInventory.setDiscountYesORNo(values[25]);
			
			
			try {
				searchInfoDeMap.get(values[0]).addInventoryInfo(activityInventory);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return searchInfoDeMap;
	}
	
	public static Map<Integer,Activity_WEB_ActivityInfo> convertIntoMap(java.util.List<Activity_WEB_ActivityInfo> inventoryList){
		
		Map<Integer,Activity_WEB_ActivityInfo> inventoryListMap=new HashMap<Integer, Activity_WEB_ActivityInfo>();
		
		for(Activity_WEB_ActivityInfo r:inventoryList){			
			inventoryListMap.put(Integer.parseInt(r.getScenarioCount()),r);
		}
		
		return inventoryListMap;
		
	}
	
	
}
