package com.rezrobot.dataobjects;

import java.util.HashMap;
import java.util.Map;

public class dmc_Setup_DataObjects_MarkupObject {
	
	dmc_Setup_DataObjects_partnerMarkupObject directCustomerMarkup=new dmc_Setup_DataObjects_partnerMarkupObject();
	Map<String, dmc_Setup_DataObjects_partnerMarkupObject> toNetMarkupmap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
	Map<String, dmc_Setup_DataObjects_partnerMarkupObject> toComMarkupmap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
	Map<String, dmc_Setup_DataObjects_partnerMarkupObject> affMarkupmap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
	public dmc_Setup_DataObjects_partnerMarkupObject getDirectCustomerMarkup() {
		return directCustomerMarkup;
	}
	public void setDirectCustomerMarkup(dmc_Setup_DataObjects_partnerMarkupObject directCustomerMarkup) {
		this.directCustomerMarkup = directCustomerMarkup;
	}
	public Map<String, dmc_Setup_DataObjects_partnerMarkupObject> getToNetMarkupmap() {
		return toNetMarkupmap;
	}
	public void setToNetMarkupmap(Map<String, dmc_Setup_DataObjects_partnerMarkupObject> toNetMarkupmap) {
		this.toNetMarkupmap = toNetMarkupmap;
	}
	public Map<String, dmc_Setup_DataObjects_partnerMarkupObject> getToComMarkupmap() {
		return toComMarkupmap;
	}
	public void setToComMarkupmap(Map<String, dmc_Setup_DataObjects_partnerMarkupObject> toComMarkupmap) {
		this.toComMarkupmap = toComMarkupmap;
	}
	public Map<String, dmc_Setup_DataObjects_partnerMarkupObject> getAffMarkupmap() {
		return affMarkupmap;
	}
	public void setAffMarkupmap(Map<String, dmc_Setup_DataObjects_partnerMarkupObject> affMarkupmap) {
		this.affMarkupmap = affMarkupmap;
	}
	
	
	
	
	

}
