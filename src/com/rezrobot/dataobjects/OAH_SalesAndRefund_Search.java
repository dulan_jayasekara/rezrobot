package com.rezrobot.dataobjects;

import com.rezrobot.types.ReportBookingChannel;
import com.rezrobot.types.ReportBookingType;
import com.rezrobot.types.ReportCurrencyType;
import com.rezrobot.types.SummarizedBy;
import com.rezrobot.types.TourSalesReportType;
import com.rezrobot.types.UserType;
import com.rezrobot.utill.Calender;

public class OAH_SalesAndRefund_Search {

private UserType             ReportUserType = UserType.NONE;
private TourSalesReportType  ReportType = TourSalesReportType.NONE;
private String               ReportFrom = "N/A";
private String               ReportTo = "N/A";
private ReportCurrencyType   CurrencyType  = ReportCurrencyType.NONE;
private String               Country = "ALL";
private ReportBookingChannel BookingChannel = ReportBookingChannel.NONE;
private String               City    = "N/A";
private String               PosName = "N/A";
private ReportBookingType    BookingType = ReportBookingType.NONE;
private SummarizedBy         SummerizedBy = SummarizedBy.NONE;
private boolean              GenerateExcel = false;


public UserType getReportUserType() {
	return ReportUserType;
}
public void setReportUserType(UserType reportUserType) {
	ReportUserType = reportUserType;
}
public TourSalesReportType getReportType() {
	return ReportType;
}
public void setReportType(TourSalesReportType reportType) {
	ReportType = reportType;
}
public String getReportFrom() {
	return ReportFrom;
}
public void setReportFrom(String reportFrom) {
	ReportFrom = reportFrom;
}
public String getReportTo() {
	return ReportTo;
}
public void setReportTo(String reportTo) {
	ReportTo = reportTo;
}
public ReportCurrencyType getCurrencyType() {
	return CurrencyType;
}
public void setCurrencyType(ReportCurrencyType currencyType) {
	CurrencyType = currencyType;
}
public String getCountry() {
	return Country;
}
public void setCountry(String country) {
	Country = country;
}
public ReportBookingChannel getBookingChannel() {
	return BookingChannel;
}
public void setBookingChannel(ReportBookingChannel bookingChannel) {
	BookingChannel = bookingChannel;
}
public String getCity() {
	return City;
}
public void setCity(String city) {
	City = city;
}
public String getPosName() {
	return PosName;
}
public void setPosName(String posName) {
	PosName = posName;
}
public ReportBookingType getBookingType() {
	return BookingType;
}
public void setBookingType(ReportBookingType bookingType) {
	BookingType = bookingType;
}
public SummarizedBy getSummerizedBy() {
	return SummerizedBy;
}
public void setSummerizedBy(SummarizedBy summerizedBy) {
	SummerizedBy = summerizedBy;
}
public boolean isGenerateExcel() {
	return GenerateExcel;
}
public void setGenerateExcel(boolean generateExcel) {
	GenerateExcel = generateExcel;
}

public void withInternalDefaultValues(){
	ReportUserType = UserType.INTERNAL;
    ReportType = TourSalesReportType.SALES;
    ReportFrom = Calender.getDate(java.util.Calendar.DATE, 0);
    ReportTo =   Calender.getDate(java.util.Calendar.DATE, 1);
    CurrencyType  = ReportCurrencyType.SELLINGCURR;
    Country = "ALL";
	BookingChannel = ReportBookingChannel.ALL;
    City    = "N/A";
	PosName = "N/A";
	BookingType = ReportBookingType.ALL;
	SummerizedBy = SummarizedBy.NONE;
	GenerateExcel = true;
	
}

public void withPosDefaultValues(String PosCountry , String PosCity ,String PosName ){
	ReportUserType = UserType.POSUSER;
    ReportType = TourSalesReportType.SALES;
    ReportFrom = Calender.getDate(java.util.Calendar.DATE, 0);
    ReportTo =   Calender.getDate(java.util.Calendar.DATE, 1);
    CurrencyType  = ReportCurrencyType.NONE;
    Country = PosCountry;
	BookingChannel = ReportBookingChannel.NONE;
    City    = PosCity;
	this.PosName = PosName;
	BookingType = ReportBookingType.ALL;
	SummerizedBy = SummarizedBy.NONE;
	GenerateExcel = true;
	
}

}
