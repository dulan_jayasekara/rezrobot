package com.rezrobot.dataobjects;

public class dmc_Setup_DataObjects_PackageMarkupObject {
	
	double dcPM;
	double b2bNetPM;
	double b2bComPM;
	double b2bCommission;
	double affPM;
	double affCommission;
	
	
	public double getDcPM() {
		return dcPM;
	}
	public void setDcPM(String dcPM) {
		this.dcPM = Double.parseDouble(dcPM);
	}
	public double getB2bNetPM() {
		return b2bNetPM;
	}
	public void setB2bNetPM(String b2bNetPM) {
		this.b2bNetPM = Double.parseDouble(b2bNetPM);
	}
	public double getB2bComPM() {
		return b2bComPM;
	}
	public void setB2bComPM(String b2bComPM) {
		this.b2bComPM = Double.parseDouble(b2bComPM);
	}
	public double getB2bCommission() {
		return b2bCommission;
	}
	public void setB2bCommission(String b2bCommission) {
		this.b2bCommission = Double.parseDouble(b2bCommission);
	}
	public double getAffPM() {
		return affPM;
	}
	public void setAffPM(String affPM) {
		this.affPM = Double.parseDouble(affPM);
	}
	public double getAffCommission() {
		return affCommission;
	}
	public void setAffCommission(String affCommission) {
		this.affCommission = Double.parseDouble(affCommission);
	}
	
	

}
