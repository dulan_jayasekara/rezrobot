package com.rezrobot.dataobjects;

import java.io.Serializable;


public class HotelOccupancyObject implements Serializable{

	String adultFirstName = "";
	String adultLastName = "";
	String adultSmoking = "";
	String adultHandicap = "";
	String adultWheelchari = "";
	String childFirstName = "";
	String childLastName = "";
	String childSmoke = "";
	String childHandicap = "";
	String childWheelchair = "";
	
	public  HotelOccupancyObject setData(String[] occupancyDetails)
	{
		this.setAdultFirstName	(occupancyDetails[0]);
		this.setAdultLastName	(occupancyDetails[1]);
		this.setAdultSmoking	(occupancyDetails[2]);
		this.setAdultHandicap	(occupancyDetails[3]);
		this.setAdultWheelchari	(occupancyDetails[4]);
		this.setChildFirstName	(occupancyDetails[5]);
		this.setChildLastName	(occupancyDetails[6]);
		this.setChildSmoke		(occupancyDetails[7]);
		this.setChildHandicap	(occupancyDetails[8]);
		this.setChildWheelchair	(occupancyDetails[9]);
		
		return this;
	}
	
	public String getAdultFirstName() {
		return adultFirstName;
	}
	public void setAdultFirstName(String adultFirstName) {
		this.adultFirstName = adultFirstName;
	}
	public String getAdultLastName() {
		return adultLastName;
	}
	public void setAdultLastName(String adultChildName) {
		this.adultLastName = adultChildName;
	}
	public String getAdultSmoking() {
		return adultSmoking;
	}
	public void setAdultSmoking(String adultSmoking) {
		this.adultSmoking = adultSmoking;
	}
	public String getAdultHandicap() {
		return adultHandicap;
	}
	public void setAdultHandicap(String adultHandicap) {
		this.adultHandicap = adultHandicap;
	}
	public String getAdultWheelchari() {
		return adultWheelchari;
	}
	public void setAdultWheelchari(String adultWheelchari) {
		this.adultWheelchari = adultWheelchari;
	}
	public String getChildFirstName() {
		return childFirstName;
	}
	public void setChildFirstName(String childFirstName) {
		this.childFirstName = childFirstName;
	}
	public String getChildLastName() {
		return childLastName;
	}
	public void setChildLastName(String childLastName) {
		this.childLastName = childLastName;
	}
	public String getChildSmoke() {
		return childSmoke;
	}
	public void setChildSmoke(String childSmoke) {
		this.childSmoke = childSmoke;
	}
	public String getChildHandicap() {
		return childHandicap;
	}
	public void setChildHandicap(String childHandicap) {
		this.childHandicap = childHandicap;
	}
	public String getChildWheelchair() {
		return childWheelchair;
	}
	public void setChildWheelchair(String childWheelchair) {
		this.childWheelchair = childWheelchair;
	}
	
	
}
