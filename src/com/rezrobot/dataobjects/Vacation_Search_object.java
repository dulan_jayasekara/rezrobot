package com.rezrobot.dataobjects;

import java.io.Serializable;

public class Vacation_Search_object implements Serializable{

	String country = ""; 
	String departureAirport = ""; 
	String arrivalAirport = ""; 
	String hotelDestination = "";
	String checkin = ""; 
	String nights = ""; 
	String rooms = ""; 
	String adults = ""; 
	String children = ""; 
	String infants = ""; 
	String age = ""; 
	String channel = ""; 
	String hotelSupplier = ""; 
	String execute = ""; 
	String quotation = ""; 
	String searchAgain = ""; 
	String to = ""; 
	String paymentMethod = ""; 
	String cancel = ""; 
	String discount = ""; 
	String discountnumber = ""; 
	String discountAmount = ""; 
	String whereToApplyDiscount = ""; 
	String toType = ""; 
	String continueIfRatesGotChanged = ""; 
	String customerLogin = "";
	
	public Vacation_Search_object vacationSearchObject(String[] searchDetails)
	{
		this.setCountry(searchDetails[0]);
		this.setDepartureAirport(searchDetails[1]);
		this.setArrivalAirport(searchDetails[2]);
		this.setHotelDestination(searchDetails[3]);
		this.setCheckin(searchDetails[4]);
		this.setNights(searchDetails[5]);
		this.setRooms(searchDetails[6]);
		this.setAdults(searchDetails[7]);
		this.setChildren(searchDetails[8]);
		this.setInfants(searchDetails[9]);
		this.setAge(searchDetails[10]);
		this.setChannel(searchDetails[11]);
		this.setHotelSupplier(searchDetails[12]);
		this.setExecute(searchDetails[13]);
		this.setQuotation(searchDetails[14]);
		this.setSearchAgain(searchDetails[15]);
		this.setTo(searchDetails[16]);
		this.setPaymentMethod(searchDetails[17]);
		this.setCancel(searchDetails[18]);
		this.setDiscount(searchDetails[19]);
		this.setDiscountnumber(searchDetails[20]);
		this.setDiscountAmount(searchDetails[21]);
		this.setWhereToApplyDiscount(searchDetails[22]);
		this.setToType(searchDetails[23]);
		this.setContinueIfRatesGotChanged(searchDetails[24]);
		this.setCustomerLogin(searchDetails[25]);
		
		return this;
	}
	
	
	public String getHotelDestination() {
		return hotelDestination;
	}
	public void setHotelDestination(String hotelDestination) {
		this.hotelDestination = hotelDestination;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDepartureAirport() {
		return departureAirport;
	}
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	public String getArrivalAirport() {
		return arrivalAirport;
	}
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String chickin) {
		this.checkin = chickin;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getInfants() {
		return infants;
	}
	public void setInfants(String infants) {
		this.infants = infants;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getHotelSupplier() {
		return hotelSupplier;
	}
	public void setHotelSupplier(String hotelSupplier) {
		this.hotelSupplier = hotelSupplier;
	}
	public String getExecute() {
		return execute;
	}
	public void setExecute(String execute) {
		this.execute = execute;
	}
	public String getQuotation() {
		return quotation;
	}
	public void setQuotation(String quotation) {
		this.quotation = quotation;
	}
	public String getSearchAgain() {
		return searchAgain;
	}
	public void setSearchAgain(String searchAgain) {
		this.searchAgain = searchAgain;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getDiscountnumber() {
		return discountnumber;
	}
	public void setDiscountnumber(String discountnumber) {
		this.discountnumber = discountnumber;
	}
	public String getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}
	public String getWhereToApplyDiscount() {
		return whereToApplyDiscount;
	}
	public void setWhereToApplyDiscount(String whereToApplyDiscount) {
		this.whereToApplyDiscount = whereToApplyDiscount;
	}
	public String getToType() {
		return toType;
	}
	public void setToType(String toType) {
		this.toType = toType;
	}
	public String getContinueIfRatesGotChanged() {
		return continueIfRatesGotChanged;
	}
	public void setContinueIfRatesGotChanged(String continueIfRatesGotChanged) {
		this.continueIfRatesGotChanged = continueIfRatesGotChanged;
	}
	public String getCustomerLogin() {
		return customerLogin;
	}
	public void setCustomerLogin(String customerLogin) {
		this.customerLogin = customerLogin;
	}
	
	
	
}
