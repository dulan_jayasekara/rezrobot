package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class ActivityDetails implements Serializable{
	
	private String paxQTY;
	private int activityRate;
	private int subTotalValue;
	private int tax;
	private int creditCardFee;
	private int totalBookingValue;
	private int totalWToutCCFee;	
	private int amountProcessedNow;
	private int amountDue;
	private int com_TOsAgentCommission;
	private int discountedValue;
	private String cancellationDeadline;
	private String reservationDateToday;
	private ArrayList<String> cancellationPoliciesList;
	private boolean isSystemLoaded;
	private boolean isBookingEngineLoaded;
	private boolean isResultsPageloaded;
	private boolean isActResultsAvailable;
	private boolean isPaymentsPageLoaded;
	private boolean isConfirmationPageLoaded;
	private boolean isQuotationConfirmationPageLoaded;
	
	private String resultsPage_ActivityName = "";
	private String resultsPage_ActivityDescription = "";
	private String resultsPage_ActCurrency = "";
	private String resultsPage_SubTotal = "";
	private int resultsPage_daysCount;
	private boolean isResultsPage_cancellationLoaded;
	private boolean isResultsPage_MoreInfoLoaded;
	
	private ArrayList<String> resultsPage_ActivityTypes;
	private ArrayList<String> resultsPage_Period;
	private ArrayList<String> resultsPage_RateType;
	private ArrayList<String> resultsPage_DailyRate;
	private ArrayList<Integer> resultsPage_ascendingOrder;
	private ArrayList<Integer> resultsPage_descendingOrder;
	private ArrayList<Integer> resultsPage_After_ascendingOrder;
	private ArrayList<Integer> resultsPage_After_descendingOrder;
	private boolean isPriceSlider_middle;
	private boolean isPriceSlider_last;
	
	private boolean isAddAndContinue;
	private String resultsPage_cartCurrencyValue = "";
	private boolean isResultsPageMybasketLoaded;
	
	private boolean isPaymentsPageCartLoaded;
	private String myBasket_Currency = "";
	private String myBasket_TotalActivityBookingValue = "";
	
	private String paymentsPage_cart_TotalWTOCCfee = "";
	private String paymentsPage_cart_SubTotal = "";
	private String paymentsPage_cart_TotalTax = "";
	private String paymentsPage_cart_Total = "";
	private String paymentsPage_cart_AmountNow = "";
	private String paymentsPage_cart_AmountDue = "";
	
	private boolean isPaymentsActivityMoreDetailsInfo;
	private String paymentsPage_AMD_ActiivtyName = "";
	private String paymentsPage_AMD_ActivityTypeName = "";	
	private String paymentsPage_AMD_UsableOnDate;
	private String paymentsPage_AMD_Period;
	private String paymentsPage_AMD_RateType;
	private String paymentsPage_AMD_DailyRate;
	private String paymentsPage_AMD_QTY;
	private String paymentsPage_AMD_TotalWithCurrency;	
	
	private String paymentsPage_AMD_Status = "";
	private String paymentsPage_AMD_SubTotal = "";
	private String paymentsPage_AMD_Tax = "";
	private String paymentsPage_AMD_Total = "";
	private String paymentsPage_AMD_CancellationDealine = "";
	private ArrayList<String> paymentsPage_AMD_CancelPolicy;
	
	private String paymentsPage_OD_FirstName;
	private String paymentsPage_OD_LastName;
	private String paymentsPage_OD_PickPort = "";
	private String paymentsPage_OD_PickInfo = "";
	private String paymentsPage_OD_DropPort = "";
	private String paymentsPage_OD_DropInfo = "";
	
	private String paymentsPage_BI_CardCurrency = "";
	private String paymentsPage_BI_CardTotal = "";
	private String paymentsPage_BI_Currency = "";
	private String paymentsPage_BI_SubTotal = "";
	private String paymentsPage_BI_TotalTax = "";
	private String paymentsPage_BI_Total = "";
	private String paymentsPage_BI_AmountNow = "";
	private String paymentsPage_BI_AmountDue = "";
	
	private String paymentsPage_N_CusNotes = "";
	private String paymentsPage_N_ActivityNotes = "";
	
	private String paymentsPage_TC_ActName = "";
	private String paymentsPage_TC_CancellationDead_1 = "";
	private ArrayList<String> paymentsPage_TC_CancelPolicy;
	private String paymentsPage_TC_CancellationDead_2 = "";
	private String paymentsPage_TC_portalName = "";
	private String paymentsPage_TC_portalPhone = "";
	
	private String availability_ActivityName = "";
	private String availability_Status = "";
	private String availability_Error = "";
	
	private String paymentGateway_Total = "";
	
	private String confirmation_BookingRefference = "";
	private String reservationNo = "Not Available";
	private String confirmation_CusMailAddress = "";
	private String confirmation_PortalTel = "";
	private String confirmation_PortalFax = "";
	private String confirmation_PortalEmail = "";
	
	private String confirmation_BI_ActivityName = "";
	private String confirmation_BI_City = "";
	private String confirmation_BI_ActType = "";
	private String confirmation_BI_BookingStatus = "";
	private String confirmation_BI_RateType;
	private String confirmation_BI_DailyRate;
	private String confirmation_BI_QTY;
	private String confirmation_BI_UsableOnDate;
	
	private String confirmation_BI_Currency1 = "";
	private String confirmation_BI_Currency2 = "";
	private String confirmation_BI_Currency3 = "";
	private String confirmation_BI_ActiivtyRate = "";
	private String confirmation_BI_SubTotal_1 = "";
	private String confirmation_BI_Tax = "";
	private String confirmation_BI_Total = "";
	private String confirmation_BI_SubTotal_2 = "";
	private String confirmation_BI_TotalTaxWTTax = "";
	private String confirmation_BI_TotalBooking = "";
	private String confirmation_BI_AmountNow = "";
	private String confirmation_BI_AmountDue = "";
	
	private String confirmation_FName = "Not Available";
	private String confirmation_LName = "Not Available";
	private String confirmation_TP = "Not Available";
	private String confirmation_Email = "Not Available";
	private String confirmation_address = "Not Available";
	private String confirmation_country = "Not Available";
	private String confirmation_city = "Not Available";
	private String confirmation_State = "Not Available";
	private String confirmation_postalCode = "Not Available";
	
	private String confirmation_AODActivityName = "";
	private String confirmationPage_cusTitle;
	private String confirmationPage_cusFName;
	private String confirmationPage_cusLName;
	
	private String confirmation_CardDetails_MerchantTrackID = "";
	private String confirmation_CardDetails_AuthRef = "";
	private String confirmation_CardDetails_PayId = "";
	private String confirmation_CardDetails_Amount = "";
	
	private String confirmation_Cancel_ActName = "";
	private String confirmation_Cancel_CancellationDead_1 = "";
	private ArrayList<String> confirmation_Cancel_CancelPolicy;
	private String confirmation_Cancel_CancellationDead_2 = "";
	
	private String quote_confirmation_BookingRefference = "";
	private String quotationNo = "Not Available";
	private String quote_confirmation_CusMailAddress = "";
	private String quote_confirmation_PortalTel = "";
	private String quote_confirmation_PortalFax = "";
	private String quote_confirmation_PortalEmail = "";
	
	private String quote_confirmation_BI_ActivityName = "";
	private String quote_confirmation_BI_City = "";
	private String quote_confirmation_BI_ActType = "";
	private String quote_confirmation_BI_BookingStatus = "";
	private String quote_confirmation_BI_RateType;
	private String quote_confirmation_BI_DailyRate;
	private String quote_confirmation_BI_QTY;
	private String quote_confirmation_BI_UsableOnDate;
	
	private String quote_confirmation_BI_Currency1 = "";
	private String quote_confirmation_BI_Currency2 = "";
	private String quote_confirmation_BI_Currency3 = "";
	private String quote_confirmation_BI_ActiivtyRate = "";
	private String quote_confirmation_BI_SubTotal_1 = "";
	private String quote_confirmation_BI_Tax = "";
	private String quote_confirmation_BI_Total = "";
	private String quote_confirmation_BI_SubTotal_2 = "";
	private String quote_confirmation_BI_TotalTaxWTTax = "";
	private String quote_confirmation_BI_TotalBooking = "";
	private String quote_confirmation_BI_AmountNow = "";
	
	private String quote_confirmation_FName = "Not Available";
	private String quote_confirmation_LName = "Not Available";
	private String quote_confirmation_TP = "Not Available";
	private String quote_confirmation_Email = "Not Available";
	private String quote_confirmation_address = "Not Available";
	private String quote_confirmation_country = "Not Available";
	private String quote_confirmation_city = "Not Available";
	private String quote_confirmation_State = "Not Available";
	private String quote_confirmation_postalCode = "Not Available";
	
	
	
	
	
	public String getReservationDateToday() {
		return reservationDateToday;
	}
	public void setReservationDateToday(String reservationDateToday) {
		this.reservationDateToday = reservationDateToday;
	}
	public boolean isPriceSlider_middle() {
		return isPriceSlider_middle;
	}
	public void setPriceSlider_middle(boolean isPriceSlider_middle) {
		this.isPriceSlider_middle = isPriceSlider_middle;
	}
	public boolean isPriceSlider_last() {
		return isPriceSlider_last;
	}
	public void setPriceSlider_last(boolean isPriceSlider_last) {
		this.isPriceSlider_last = isPriceSlider_last;
	}
	public ArrayList<Integer> getResultsPage_ascendingOrder() {
		return resultsPage_ascendingOrder;
	}
	public void setResultsPage_ascendingOrder(
			ArrayList<Integer> resultsPage_ascendingOrder) {
		this.resultsPage_ascendingOrder = resultsPage_ascendingOrder;
	}
	public ArrayList<Integer> getResultsPage_descendingOrder() {
		return resultsPage_descendingOrder;
	}
	public void setResultsPage_descendingOrder(
			ArrayList<Integer> resultsPage_descendingOrder) {
		this.resultsPage_descendingOrder = resultsPage_descendingOrder;
	}
	public ArrayList<Integer> getResultsPage_After_ascendingOrder() {
		return resultsPage_After_ascendingOrder;
	}
	public void setResultsPage_After_ascendingOrder(
			ArrayList<Integer> resultsPage_After_ascendingOrder) {
		this.resultsPage_After_ascendingOrder = resultsPage_After_ascendingOrder;
	}
	public ArrayList<Integer> getResultsPage_After_descendingOrder() {
		return resultsPage_After_descendingOrder;
	}
	public void setResultsPage_After_descendingOrder(
			ArrayList<Integer> resultsPage_After_descendingOrder) {
		this.resultsPage_After_descendingOrder = resultsPage_After_descendingOrder;
	}
	public boolean isQuotationConfirmationPageLoaded() {
		return isQuotationConfirmationPageLoaded;
	}
	public void setQuotationConfirmationPageLoaded(
			boolean isQuotationConfirmationPageLoaded) {
		this.isQuotationConfirmationPageLoaded = isQuotationConfirmationPageLoaded;
	}
	public String getQuote_confirmation_BookingRefference() {
		return quote_confirmation_BookingRefference;
	}
	public void setQuote_confirmation_BookingRefference(
			String quote_confirmation_BookingRefference) {
		this.quote_confirmation_BookingRefference = quote_confirmation_BookingRefference;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	public String getQuote_confirmation_CusMailAddress() {
		return quote_confirmation_CusMailAddress;
	}
	public void setQuote_confirmation_CusMailAddress(
			String quote_confirmation_CusMailAddress) {
		this.quote_confirmation_CusMailAddress = quote_confirmation_CusMailAddress;
	}
	public String getQuote_confirmation_PortalTel() {
		return quote_confirmation_PortalTel;
	}
	public void setQuote_confirmation_PortalTel(String quote_confirmation_PortalTel) {
		this.quote_confirmation_PortalTel = quote_confirmation_PortalTel;
	}
	public String getQuote_confirmation_PortalFax() {
		return quote_confirmation_PortalFax;
	}
	public void setQuote_confirmation_PortalFax(String quote_confirmation_PortalFax) {
		this.quote_confirmation_PortalFax = quote_confirmation_PortalFax;
	}
	public String getQuote_confirmation_PortalEmail() {
		return quote_confirmation_PortalEmail;
	}
	public void setQuote_confirmation_PortalEmail(
			String quote_confirmation_PortalEmail) {
		this.quote_confirmation_PortalEmail = quote_confirmation_PortalEmail;
	}
	public String getQuote_confirmation_BI_ActivityName() {
		return quote_confirmation_BI_ActivityName;
	}
	public void setQuote_confirmation_BI_ActivityName(
			String quote_confirmation_BI_ActivityName) {
		this.quote_confirmation_BI_ActivityName = quote_confirmation_BI_ActivityName;
	}
	public String getQuote_confirmation_BI_City() {
		return quote_confirmation_BI_City;
	}
	public void setQuote_confirmation_BI_City(String quote_confirmation_BI_City) {
		this.quote_confirmation_BI_City = quote_confirmation_BI_City;
	}
	public String getQuote_confirmation_BI_ActType() {
		return quote_confirmation_BI_ActType;
	}
	public void setQuote_confirmation_BI_ActType(
			String quote_confirmation_BI_ActType) {
		this.quote_confirmation_BI_ActType = quote_confirmation_BI_ActType;
	}
	public String getQuote_confirmation_BI_BookingStatus() {
		return quote_confirmation_BI_BookingStatus;
	}
	public void setQuote_confirmation_BI_BookingStatus(
			String quote_confirmation_BI_BookingStatus) {
		this.quote_confirmation_BI_BookingStatus = quote_confirmation_BI_BookingStatus;
	}
	public String getQuote_confirmation_BI_RateType() {
		return quote_confirmation_BI_RateType;
	}
	public void setQuote_confirmation_BI_RateType(
			String quote_confirmation_BI_RateType) {
		this.quote_confirmation_BI_RateType = quote_confirmation_BI_RateType;
	}
	public String getQuote_confirmation_BI_DailyRate() {
		return quote_confirmation_BI_DailyRate;
	}
	public void setQuote_confirmation_BI_DailyRate(
			String quote_confirmation_BI_DailyRate) {
		this.quote_confirmation_BI_DailyRate = quote_confirmation_BI_DailyRate;
	}
	public String getQuote_confirmation_BI_QTY() {
		return quote_confirmation_BI_QTY;
	}
	public void setQuote_confirmation_BI_QTY(String quote_confirmation_BI_QTY) {
		this.quote_confirmation_BI_QTY = quote_confirmation_BI_QTY;
	}
	public String getQuote_confirmation_BI_UsableOnDate() {
		return quote_confirmation_BI_UsableOnDate;
	}
	public void setQuote_confirmation_BI_UsableOnDate(
			String quote_confirmation_BI_UsableOnDate) {
		this.quote_confirmation_BI_UsableOnDate = quote_confirmation_BI_UsableOnDate;
	}
	public String getQuote_confirmation_BI_Currency1() {
		return quote_confirmation_BI_Currency1;
	}
	public void setQuote_confirmation_BI_Currency1(
			String quote_confirmation_BI_Currency1) {
		this.quote_confirmation_BI_Currency1 = quote_confirmation_BI_Currency1;
	}
	public String getQuote_confirmation_BI_Currency2() {
		return quote_confirmation_BI_Currency2;
	}
	public void setQuote_confirmation_BI_Currency2(
			String quote_confirmation_BI_Currency2) {
		this.quote_confirmation_BI_Currency2 = quote_confirmation_BI_Currency2;
	}
	public String getQuote_confirmation_BI_Currency3() {
		return quote_confirmation_BI_Currency3;
	}
	public void setQuote_confirmation_BI_Currency3(
			String quote_confirmation_BI_Currency3) {
		this.quote_confirmation_BI_Currency3 = quote_confirmation_BI_Currency3;
	}
	public String getQuote_confirmation_BI_ActiivtyRate() {
		return quote_confirmation_BI_ActiivtyRate;
	}
	public void setQuote_confirmation_BI_ActiivtyRate(
			String quote_confirmation_BI_ActiivtyRate) {
		this.quote_confirmation_BI_ActiivtyRate = quote_confirmation_BI_ActiivtyRate;
	}
	public String getQuote_confirmation_BI_SubTotal_1() {
		return quote_confirmation_BI_SubTotal_1;
	}
	public void setQuote_confirmation_BI_SubTotal_1(
			String quote_confirmation_BI_SubTotal_1) {
		this.quote_confirmation_BI_SubTotal_1 = quote_confirmation_BI_SubTotal_1;
	}
	public String getQuote_confirmation_BI_Tax() {
		return quote_confirmation_BI_Tax;
	}
	public void setQuote_confirmation_BI_Tax(String quote_confirmation_BI_Tax) {
		this.quote_confirmation_BI_Tax = quote_confirmation_BI_Tax;
	}
	public String getQuote_confirmation_BI_Total() {
		return quote_confirmation_BI_Total;
	}
	public void setQuote_confirmation_BI_Total(String quote_confirmation_BI_Total) {
		this.quote_confirmation_BI_Total = quote_confirmation_BI_Total;
	}
	public String getQuote_confirmation_BI_SubTotal_2() {
		return quote_confirmation_BI_SubTotal_2;
	}
	public void setQuote_confirmation_BI_SubTotal_2(
			String quote_confirmation_BI_SubTotal_2) {
		this.quote_confirmation_BI_SubTotal_2 = quote_confirmation_BI_SubTotal_2;
	}
	public String getQuote_confirmation_BI_TotalTaxWTTax() {
		return quote_confirmation_BI_TotalTaxWTTax;
	}
	public void setQuote_confirmation_BI_TotalTaxWTTax(
			String quote_confirmation_BI_TotalTaxWTTax) {
		this.quote_confirmation_BI_TotalTaxWTTax = quote_confirmation_BI_TotalTaxWTTax;
	}
	public String getQuote_confirmation_BI_TotalBooking() {
		return quote_confirmation_BI_TotalBooking;
	}
	public void setQuote_confirmation_BI_TotalBooking(
			String quote_confirmation_BI_TotalBooking) {
		this.quote_confirmation_BI_TotalBooking = quote_confirmation_BI_TotalBooking;
	}
	public String getQuote_confirmation_BI_AmountNow() {
		return quote_confirmation_BI_AmountNow;
	}
	public void setQuote_confirmation_BI_AmountNow(
			String quote_confirmation_BI_AmountNow) {
		this.quote_confirmation_BI_AmountNow = quote_confirmation_BI_AmountNow;
	}
	public String getQuote_confirmation_FName() {
		return quote_confirmation_FName;
	}
	public void setQuote_confirmation_FName(String quote_confirmation_FName) {
		this.quote_confirmation_FName = quote_confirmation_FName;
	}
	public String getQuote_confirmation_LName() {
		return quote_confirmation_LName;
	}
	public void setQuote_confirmation_LName(String quote_confirmation_LName) {
		this.quote_confirmation_LName = quote_confirmation_LName;
	}
	public String getQuote_confirmation_TP() {
		return quote_confirmation_TP;
	}
	public void setQuote_confirmation_TP(String quote_confirmation_TP) {
		this.quote_confirmation_TP = quote_confirmation_TP;
	}
	public String getQuote_confirmation_Email() {
		return quote_confirmation_Email;
	}
	public void setQuote_confirmation_Email(String quote_confirmation_Email) {
		this.quote_confirmation_Email = quote_confirmation_Email;
	}
	public String getQuote_confirmation_address() {
		return quote_confirmation_address;
	}
	public void setQuote_confirmation_address(String quote_confirmation_address) {
		this.quote_confirmation_address = quote_confirmation_address;
	}
	public String getQuote_confirmation_country() {
		return quote_confirmation_country;
	}
	public void setQuote_confirmation_country(String quote_confirmation_country) {
		this.quote_confirmation_country = quote_confirmation_country;
	}
	public String getQuote_confirmation_city() {
		return quote_confirmation_city;
	}
	public void setQuote_confirmation_city(String quote_confirmation_city) {
		this.quote_confirmation_city = quote_confirmation_city;
	}
	public String getQuote_confirmation_State() {
		return quote_confirmation_State;
	}
	public void setQuote_confirmation_State(String quote_confirmation_State) {
		this.quote_confirmation_State = quote_confirmation_State;
	}
	public String getQuote_confirmation_postalCode() {
		return quote_confirmation_postalCode;
	}
	public void setQuote_confirmation_postalCode(
			String quote_confirmation_postalCode) {
		this.quote_confirmation_postalCode = quote_confirmation_postalCode;
	}
	public String getPaymentsPage_OD_FirstName() {
		return paymentsPage_OD_FirstName;
	}
	public void setPaymentsPage_OD_FirstName(String paymentsPage_OD_FirstName) {
		this.paymentsPage_OD_FirstName = paymentsPage_OD_FirstName;
	}
	public String getPaymentsPage_OD_LastName() {
		return paymentsPage_OD_LastName;
	}
	public void setPaymentsPage_OD_LastName(String paymentsPage_OD_LastName) {
		this.paymentsPage_OD_LastName = paymentsPage_OD_LastName;
	}
	public boolean isResultsPageMybasketLoaded() {
		return isResultsPageMybasketLoaded;
	}
	public void setResultsPageMybasketLoaded(boolean isResultsPageMybasketLoaded) {
		this.isResultsPageMybasketLoaded = isResultsPageMybasketLoaded;
	}
	public boolean isPaymentsPageCartLoaded() {
		return isPaymentsPageCartLoaded;
	}
	public void setPaymentsPageCartLoaded(boolean isPaymentsPageCartLoaded) {
		this.isPaymentsPageCartLoaded = isPaymentsPageCartLoaded;
	}
	
	public boolean isPaymentsActivityMoreDetailsInfo() {
		return isPaymentsActivityMoreDetailsInfo;
	}
	public void setPaymentsActivityMoreDetailsInfo(
			boolean isPaymentsActivityMoreDetailsInfo) {
		this.isPaymentsActivityMoreDetailsInfo = isPaymentsActivityMoreDetailsInfo;
	}
	public boolean isAddAndContinue() {
		return isAddAndContinue;
	}
	public void setAddAndContinue(boolean isAddAndContinue) {
		this.isAddAndContinue = isAddAndContinue;
	}
	
	public ArrayList<String> getCancellationPoliciesList() {
		return cancellationPoliciesList;
	}
	public void setCancellationPoliciesList(
			ArrayList<String> cancellationPoliciesList) {
		this.cancellationPoliciesList = cancellationPoliciesList;
	}
	public boolean isResultsPage_cancellationLoaded() {
		return isResultsPage_cancellationLoaded;
	}
	public void setResultsPage_cancellationLoaded(
			boolean isResultsPage_cancellationLoaded) {
		this.isResultsPage_cancellationLoaded = isResultsPage_cancellationLoaded;
	}
	public boolean isResultsPage_MoreInfoLoaded() {
		return isResultsPage_MoreInfoLoaded;
	}
	public void setResultsPage_MoreInfoLoaded(boolean isResultsPage_MoreInfoLoaded) {
		this.isResultsPage_MoreInfoLoaded = isResultsPage_MoreInfoLoaded;
	}
	public int getResultsPage_daysCount() {
		return resultsPage_daysCount;
	}
	
	public int getActivityRate() {
		return activityRate;
	}
	public void setActivityRate(int activityRate) {
		this.activityRate = activityRate;
	}
	public int getSubTotalValue() {
		return subTotalValue;
	}
	public void setSubTotalValue(int subTotalValue) {
		this.subTotalValue = subTotalValue;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}
	public int getCreditCardFee() {
		return creditCardFee;
	}
	public void setCreditCardFee(int creditCardFee) {
		this.creditCardFee = creditCardFee;
	}
	public int getTotalBookingValue() {
		return totalBookingValue;
	}
	public void setTotalBookingValue(int totalBookingValue) {
		this.totalBookingValue = totalBookingValue;
	}
	public int getTotalWToutCCFee() {
		return totalWToutCCFee;
	}
	public void setTotalWToutCCFee(int totalWToutCCFee) {
		this.totalWToutCCFee = totalWToutCCFee;
	}
	public int getAmountProcessedNow() {
		return amountProcessedNow;
	}
	public void setAmountProcessedNow(int amountProcessedNow) {
		this.amountProcessedNow = amountProcessedNow;
	}
	public int getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(int amountDue) {
		this.amountDue = amountDue;
	}
	public int getCom_TOsAgentCommission() {
		return com_TOsAgentCommission;
	}
	public void setCom_TOsAgentCommission(int com_TOsAgentCommission) {
		this.com_TOsAgentCommission = com_TOsAgentCommission;
	}
	public int getDiscountedValue() {
		return discountedValue;
	}
	public void setDiscountedValue(int discountedValue) {
		this.discountedValue = discountedValue;
	}
	public void setResultsPage_daysCount(int resultsPage_daysCount) {
		this.resultsPage_daysCount = resultsPage_daysCount;
	}
	public String getCancellationDeadline() {
		return cancellationDeadline;
	}
	public void setCancellationDeadline(String cancellationDeadline) {
		this.cancellationDeadline = cancellationDeadline;
	}
	public String getPaxQTY() {
		return paxQTY;
	}
	public void setPaxQTY(String paxQTY) {
		this.paxQTY = paxQTY;
	}
	public boolean isSystemLoaded() {
		return isSystemLoaded;
	}
	public void setSystemLoaded(boolean isSystemLoaded) {
		this.isSystemLoaded = isSystemLoaded;
	}
	public boolean isBookingEngineLoaded() {
		return isBookingEngineLoaded;
	}
	public void setBookingEngineLoaded(boolean isBookingEngineLoaded) {
		this.isBookingEngineLoaded = isBookingEngineLoaded;
	}
	public boolean isResultsPageloaded() {
		return isResultsPageloaded;
	}
	public void setResultsPageloaded(boolean isResultsPageloaded) {
		this.isResultsPageloaded = isResultsPageloaded;
	}
	public boolean isActResultsAvailable() {
		return isActResultsAvailable;
	}
	public void setActResultsAvailable(boolean isActResultsAvailable) {
		this.isActResultsAvailable = isActResultsAvailable;
	}
	public boolean isPaymentsPageLoaded() {
		return isPaymentsPageLoaded;
	}
	public void setPaymentsPageLoaded(boolean isPaymentsPageLoaded) {
		this.isPaymentsPageLoaded = isPaymentsPageLoaded;
	}
	public boolean isConfirmationPageLoaded() {
		return isConfirmationPageLoaded;
	}
	public void setConfirmationPageLoaded(boolean isConfirmationPageLoaded) {
		this.isConfirmationPageLoaded = isConfirmationPageLoaded;
	}
	public String getResultsPage_ActivityName() {
		return resultsPage_ActivityName;
	}
	public void setResultsPage_ActivityName(String resultsPage_ActivityName) {
		this.resultsPage_ActivityName = resultsPage_ActivityName;
	}
	public String getResultsPage_ActivityDescription() {
		return resultsPage_ActivityDescription;
	}
	public void setResultsPage_ActivityDescription(
			String resultsPage_ActivityDescription) {
		this.resultsPage_ActivityDescription = resultsPage_ActivityDescription;
	}
	public String getResultsPage_ActCurrency() {
		return resultsPage_ActCurrency;
	}
	public void setResultsPage_ActCurrency(String resultsPage_ActCurrency) {
		this.resultsPage_ActCurrency = resultsPage_ActCurrency;
	}
	public String getResultsPage_SubTotal() {
		return resultsPage_SubTotal;
	}
	public void setResultsPage_SubTotal(String resultsPage_SubTotal) {
		this.resultsPage_SubTotal = resultsPage_SubTotal;
	}
	public ArrayList<String> getResultsPage_ActivityTypes() {
		return resultsPage_ActivityTypes;
	}
	public void setResultsPage_ActivityTypes(
			ArrayList<String> resultsPage_ActivityTypes) {
		this.resultsPage_ActivityTypes = resultsPage_ActivityTypes;
	}
	public ArrayList<String> getResultsPage_Period() {
		return resultsPage_Period;
	}
	public void setResultsPage_Period(ArrayList<String> resultsPage_Period) {
		this.resultsPage_Period = resultsPage_Period;
	}
	public ArrayList<String> getResultsPage_RateType() {
		return resultsPage_RateType;
	}
	public void setResultsPage_RateType(ArrayList<String> resultsPage_RateType) {
		this.resultsPage_RateType = resultsPage_RateType;
	}
	public ArrayList<String> getResultsPage_DailyRate() {
		return resultsPage_DailyRate;
	}
	public void setResultsPage_DailyRate(ArrayList<String> resultsPage_DailyRate) {
		this.resultsPage_DailyRate = resultsPage_DailyRate;
	}
	public String getResultsPage_cartCurrencyValue() {
		return resultsPage_cartCurrencyValue;
	}
	public void setResultsPage_cartCurrencyValue(
			String resultsPage_cartCurrencyValue) {
		this.resultsPage_cartCurrencyValue = resultsPage_cartCurrencyValue;
	}
	public String getMyBasket_Currency() {
		return myBasket_Currency;
	}
	public void setMyBasket_Currency(String myBasket_Currency) {
		this.myBasket_Currency = myBasket_Currency;
	}
	public String getMyBasket_TotalActivityBookingValue() {
		return myBasket_TotalActivityBookingValue;
	}
	public void setMyBasket_TotalActivityBookingValue(
			String myBasket_TotalActivityBookingValue) {
		this.myBasket_TotalActivityBookingValue = myBasket_TotalActivityBookingValue;
	}	
	public String getPaymentsPage_cart_TotalWTOCCfee() {
		return paymentsPage_cart_TotalWTOCCfee;
	}
	public void setPaymentsPage_cart_TotalWTOCCfee(
			String paymentsPage_cart_TotalWTOCCfee) {
		this.paymentsPage_cart_TotalWTOCCfee = paymentsPage_cart_TotalWTOCCfee;
	}
	public String getPaymentsPage_cart_SubTotal() {
		return paymentsPage_cart_SubTotal;
	}
	public void setPaymentsPage_cart_SubTotal(String paymentsPage_cart_SubTotal) {
		this.paymentsPage_cart_SubTotal = paymentsPage_cart_SubTotal;
	}
	public String getPaymentsPage_cart_TotalTax() {
		return paymentsPage_cart_TotalTax;
	}
	public void setPaymentsPage_cart_TotalTax(String paymentsPage_cart_TotalTax) {
		this.paymentsPage_cart_TotalTax = paymentsPage_cart_TotalTax;
	}
	public String getPaymentsPage_cart_Total() {
		return paymentsPage_cart_Total;
	}
	public void setPaymentsPage_cart_Total(String paymentsPage_cart_Total) {
		this.paymentsPage_cart_Total = paymentsPage_cart_Total;
	}
	public String getPaymentsPage_cart_AmountNow() {
		return paymentsPage_cart_AmountNow;
	}
	public void setPaymentsPage_cart_AmountNow(String paymentsPage_cart_AmountNow) {
		this.paymentsPage_cart_AmountNow = paymentsPage_cart_AmountNow;
	}
	public String getPaymentsPage_cart_AmountDue() {
		return paymentsPage_cart_AmountDue;
	}
	public void setPaymentsPage_cart_AmountDue(String paymentsPage_cart_AmountDue) {
		this.paymentsPage_cart_AmountDue = paymentsPage_cart_AmountDue;
	}
	public String getPaymentsPage_AMD_ActiivtyName() {
		return paymentsPage_AMD_ActiivtyName;
	}
	public void setPaymentsPage_AMD_ActiivtyName(
			String paymentsPage_AMD_ActiivtyName) {
		this.paymentsPage_AMD_ActiivtyName = paymentsPage_AMD_ActiivtyName;
	}
	public String getPaymentsPage_AMD_ActivityTypeName() {
		return paymentsPage_AMD_ActivityTypeName;
	}
	public void setPaymentsPage_AMD_ActivityTypeName(
			String paymentsPage_AMD_ActivityTypeName) {
		this.paymentsPage_AMD_ActivityTypeName = paymentsPage_AMD_ActivityTypeName;
	}
	
	public String getPaymentsPage_AMD_UsableOnDate() {
		return paymentsPage_AMD_UsableOnDate;
	}
	public void setPaymentsPage_AMD_UsableOnDate(
			String paymentsPage_AMD_UsableOnDate) {
		this.paymentsPage_AMD_UsableOnDate = paymentsPage_AMD_UsableOnDate;
	}
	public String getPaymentsPage_AMD_Period() {
		return paymentsPage_AMD_Period;
	}
	public void setPaymentsPage_AMD_Period(String paymentsPage_AMD_Period) {
		this.paymentsPage_AMD_Period = paymentsPage_AMD_Period;
	}
	public String getPaymentsPage_AMD_RateType() {
		return paymentsPage_AMD_RateType;
	}
	public void setPaymentsPage_AMD_RateType(String paymentsPage_AMD_RateType) {
		this.paymentsPage_AMD_RateType = paymentsPage_AMD_RateType;
	}
	public String getPaymentsPage_AMD_DailyRate() {
		return paymentsPage_AMD_DailyRate;
	}
	public void setPaymentsPage_AMD_DailyRate(String paymentsPage_AMD_DailyRate) {
		this.paymentsPage_AMD_DailyRate = paymentsPage_AMD_DailyRate;
	}
	public String getPaymentsPage_AMD_QTY() {
		return paymentsPage_AMD_QTY;
	}
	public void setPaymentsPage_AMD_QTY(String paymentsPage_AMD_QTY) {
		this.paymentsPage_AMD_QTY = paymentsPage_AMD_QTY;
	}
	public String getPaymentsPage_AMD_TotalWithCurrency() {
		return paymentsPage_AMD_TotalWithCurrency;
	}
	public void setPaymentsPage_AMD_TotalWithCurrency(
			String paymentsPage_AMD_TotalWithCurrency) {
		this.paymentsPage_AMD_TotalWithCurrency = paymentsPage_AMD_TotalWithCurrency;
	}
	public String getPaymentsPage_AMD_Status() {
		return paymentsPage_AMD_Status;
	}
	public void setPaymentsPage_AMD_Status(String paymentsPage_AMD_Status) {
		this.paymentsPage_AMD_Status = paymentsPage_AMD_Status;
	}
	public String getPaymentsPage_AMD_SubTotal() {
		return paymentsPage_AMD_SubTotal;
	}
	public void setPaymentsPage_AMD_SubTotal(String paymentsPage_AMD_SubTotal) {
		this.paymentsPage_AMD_SubTotal = paymentsPage_AMD_SubTotal;
	}
	public String getPaymentsPage_AMD_Tax() {
		return paymentsPage_AMD_Tax;
	}
	public void setPaymentsPage_AMD_Tax(String paymentsPage_AMD_Tax) {
		this.paymentsPage_AMD_Tax = paymentsPage_AMD_Tax;
	}
	public String getPaymentsPage_AMD_Total() {
		return paymentsPage_AMD_Total;
	}
	public void setPaymentsPage_AMD_Total(String paymentsPage_AMD_Total) {
		this.paymentsPage_AMD_Total = paymentsPage_AMD_Total;
	}
	public String getPaymentsPage_AMD_CancellationDealine() {
		return paymentsPage_AMD_CancellationDealine;
	}
	public void setPaymentsPage_AMD_CancellationDealine(
			String paymentsPage_AMD_CancellationDealine) {
		this.paymentsPage_AMD_CancellationDealine = paymentsPage_AMD_CancellationDealine;
	}
	public ArrayList<String> getPaymentsPage_AMD_CancelPolicy() {
		return paymentsPage_AMD_CancelPolicy;
	}
	public void setPaymentsPage_AMD_CancelPolicy(
			ArrayList<String> paymentsPage_AMD_CancelPolicy) {
		this.paymentsPage_AMD_CancelPolicy = paymentsPage_AMD_CancelPolicy;
	}
	public String getPaymentsPage_OD_PickPort() {
		return paymentsPage_OD_PickPort;
	}
	public void setPaymentsPage_OD_PickPort(String paymentsPage_OD_PickPort) {
		this.paymentsPage_OD_PickPort = paymentsPage_OD_PickPort;
	}
	public String getPaymentsPage_OD_PickInfo() {
		return paymentsPage_OD_PickInfo;
	}
	public void setPaymentsPage_OD_PickInfo(String paymentsPage_OD_PickInfo) {
		this.paymentsPage_OD_PickInfo = paymentsPage_OD_PickInfo;
	}
	public String getPaymentsPage_OD_DropPort() {
		return paymentsPage_OD_DropPort;
	}
	public void setPaymentsPage_OD_DropPort(String paymentsPage_OD_DropPort) {
		this.paymentsPage_OD_DropPort = paymentsPage_OD_DropPort;
	}
	public String getPaymentsPage_OD_DropInfo() {
		return paymentsPage_OD_DropInfo;
	}
	public void setPaymentsPage_OD_DropInfo(String paymentsPage_OD_DropInfo) {
		this.paymentsPage_OD_DropInfo = paymentsPage_OD_DropInfo;
	}
	public String getPaymentsPage_BI_CardCurrency() {
		return paymentsPage_BI_CardCurrency;
	}
	public void setPaymentsPage_BI_CardCurrency(String paymentsPage_BI_CardCurrency) {
		this.paymentsPage_BI_CardCurrency = paymentsPage_BI_CardCurrency;
	}
	public String getPaymentsPage_BI_CardTotal() {
		return paymentsPage_BI_CardTotal;
	}
	public void setPaymentsPage_BI_CardTotal(String paymentsPage_BI_CardTotal) {
		this.paymentsPage_BI_CardTotal = paymentsPage_BI_CardTotal;
	}
	public String getPaymentsPage_BI_Currency() {
		return paymentsPage_BI_Currency;
	}
	public void setPaymentsPage_BI_Currency(String paymentsPage_BI_Currency) {
		this.paymentsPage_BI_Currency = paymentsPage_BI_Currency;
	}
	public String getPaymentsPage_BI_SubTotal() {
		return paymentsPage_BI_SubTotal;
	}
	public void setPaymentsPage_BI_SubTotal(String paymentsPage_BI_SubTotal) {
		this.paymentsPage_BI_SubTotal = paymentsPage_BI_SubTotal;
	}
	public String getPaymentsPage_BI_TotalTax() {
		return paymentsPage_BI_TotalTax;
	}
	public void setPaymentsPage_BI_TotalTax(String paymentsPage_BI_TotalTax) {
		this.paymentsPage_BI_TotalTax = paymentsPage_BI_TotalTax;
	}
	public String getPaymentsPage_BI_Total() {
		return paymentsPage_BI_Total;
	}
	public void setPaymentsPage_BI_Total(String paymentsPage_BI_Total) {
		this.paymentsPage_BI_Total = paymentsPage_BI_Total;
	}
	public String getPaymentsPage_BI_AmountNow() {
		return paymentsPage_BI_AmountNow;
	}
	public void setPaymentsPage_BI_AmountNow(String paymentsPage_BI_AmountNow) {
		this.paymentsPage_BI_AmountNow = paymentsPage_BI_AmountNow;
	}
	public String getPaymentsPage_BI_AmountDue() {
		return paymentsPage_BI_AmountDue;
	}
	public void setPaymentsPage_BI_AmountDue(String paymentsPage_BI_AmountDue) {
		this.paymentsPage_BI_AmountDue = paymentsPage_BI_AmountDue;
	}
	public String getPaymentsPage_N_CusNotes() {
		return paymentsPage_N_CusNotes;
	}
	public void setPaymentsPage_N_CusNotes(String paymentsPage_N_CusNotes) {
		this.paymentsPage_N_CusNotes = paymentsPage_N_CusNotes;
	}
	public String getPaymentsPage_N_ActivityNotes() {
		return paymentsPage_N_ActivityNotes;
	}
	public void setPaymentsPage_N_ActivityNotes(String paymentsPage_N_ActivityNotes) {
		this.paymentsPage_N_ActivityNotes = paymentsPage_N_ActivityNotes;
	}
	public String getPaymentsPage_TC_ActName() {
		return paymentsPage_TC_ActName;
	}
	public void setPaymentsPage_TC_ActName(String paymentsPage_TC_ActName) {
		this.paymentsPage_TC_ActName = paymentsPage_TC_ActName;
	}
	public String getPaymentsPage_TC_CancellationDead_1() {
		return paymentsPage_TC_CancellationDead_1;
	}
	public void setPaymentsPage_TC_CancellationDead_1(
			String paymentsPage_TC_CancellationDead_1) {
		this.paymentsPage_TC_CancellationDead_1 = paymentsPage_TC_CancellationDead_1;
	}
	public ArrayList<String> getPaymentsPage_TC_CancelPolicy() {
		return paymentsPage_TC_CancelPolicy;
	}
	public void setPaymentsPage_TC_CancelPolicy(
			ArrayList<String> paymentsPage_TC_CancelPolicy) {
		this.paymentsPage_TC_CancelPolicy = paymentsPage_TC_CancelPolicy;
	}
	public String getPaymentsPage_TC_CancellationDead_2() {
		return paymentsPage_TC_CancellationDead_2;
	}
	public void setPaymentsPage_TC_CancellationDead_2(
			String paymentsPage_TC_CancellationDead_2) {
		this.paymentsPage_TC_CancellationDead_2 = paymentsPage_TC_CancellationDead_2;
	}
	public String getPaymentsPage_TC_portalName() {
		return paymentsPage_TC_portalName;
	}
	public void setPaymentsPage_TC_portalName(String paymentsPage_TC_portalName) {
		this.paymentsPage_TC_portalName = paymentsPage_TC_portalName;
	}
	public String getPaymentsPage_TC_portalPhone() {
		return paymentsPage_TC_portalPhone;
	}
	public void setPaymentsPage_TC_portalPhone(String paymentsPage_TC_portalPhone) {
		this.paymentsPage_TC_portalPhone = paymentsPage_TC_portalPhone;
	}
	public String getAvailability_ActivityName() {
		return availability_ActivityName;
	}
	public void setAvailability_ActivityName(String availability_ActivityName) {
		this.availability_ActivityName = availability_ActivityName;
	}
	public String getAvailability_Status() {
		return availability_Status;
	}
	public void setAvailability_Status(String availability_Status) {
		this.availability_Status = availability_Status;
	}
	public String getAvailability_Error() {
		return availability_Error;
	}
	public void setAvailability_Error(String availability_Error) {
		this.availability_Error = availability_Error;
	}
	public String getPaymentGateway_Total() {
		return paymentGateway_Total;
	}
	public void setPaymentGateway_Total(String paymentGateway_Total) {
		this.paymentGateway_Total = paymentGateway_Total;
	}
	public String getConfirmation_BookingRefference() {
		return confirmation_BookingRefference;
	}
	public void setConfirmation_BookingRefference(
			String confirmation_BookingRefference) {
		this.confirmation_BookingRefference = confirmation_BookingRefference;
	}
	public String getReservationNo() {
		return reservationNo;
	}
	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}
	public String getConfirmation_CusMailAddress() {
		return confirmation_CusMailAddress;
	}
	public void setConfirmation_CusMailAddress(String confirmation_CusMailAddress) {
		this.confirmation_CusMailAddress = confirmation_CusMailAddress;
	}
	public String getConfirmation_PortalTel() {
		return confirmation_PortalTel;
	}
	public void setConfirmation_PortalTel(String confirmation_PortalTel) {
		this.confirmation_PortalTel = confirmation_PortalTel;
	}
	public String getConfirmation_PortalFax() {
		return confirmation_PortalFax;
	}
	public void setConfirmation_PortalFax(String confirmation_PortalFax) {
		this.confirmation_PortalFax = confirmation_PortalFax;
	}
	public String getConfirmation_PortalEmail() {
		return confirmation_PortalEmail;
	}
	public void setConfirmation_PortalEmail(String confirmation_PortalEmail) {
		this.confirmation_PortalEmail = confirmation_PortalEmail;
	}
	public String getConfirmation_BI_ActivityName() {
		return confirmation_BI_ActivityName;
	}
	public void setConfirmation_BI_ActivityName(String confirmation_BI_ActivityName) {
		this.confirmation_BI_ActivityName = confirmation_BI_ActivityName;
	}
	public String getConfirmation_BI_City() {
		return confirmation_BI_City;
	}
	public void setConfirmation_BI_City(String confirmation_BI_City) {
		this.confirmation_BI_City = confirmation_BI_City;
	}
	public String getConfirmation_BI_ActType() {
		return confirmation_BI_ActType;
	}
	public void setConfirmation_BI_ActType(String confirmation_BI_ActType) {
		this.confirmation_BI_ActType = confirmation_BI_ActType;
	}
	public String getConfirmation_BI_BookingStatus() {
		return confirmation_BI_BookingStatus;
	}
	public void setConfirmation_BI_BookingStatus(
			String confirmation_BI_BookingStatus) {
		this.confirmation_BI_BookingStatus = confirmation_BI_BookingStatus;
	}
	
	public String getConfirmation_BI_RateType() {
		return confirmation_BI_RateType;
	}
	public void setConfirmation_BI_RateType(String confirmation_BI_RateType) {
		this.confirmation_BI_RateType = confirmation_BI_RateType;
	}
	public String getConfirmation_BI_DailyRate() {
		return confirmation_BI_DailyRate;
	}
	public void setConfirmation_BI_DailyRate(String confirmation_BI_DailyRate) {
		this.confirmation_BI_DailyRate = confirmation_BI_DailyRate;
	}
	public String getConfirmation_BI_QTY() {
		return confirmation_BI_QTY;
	}
	public void setConfirmation_BI_QTY(String confirmation_BI_QTY) {
		this.confirmation_BI_QTY = confirmation_BI_QTY;
	}
	public String getConfirmation_BI_UsableOnDate() {
		return confirmation_BI_UsableOnDate;
	}
	public void setConfirmation_BI_UsableOnDate(String confirmation_BI_UsableOnDate) {
		this.confirmation_BI_UsableOnDate = confirmation_BI_UsableOnDate;
	}
	public String getConfirmation_BI_Currency1() {
		return confirmation_BI_Currency1;
	}
	public void setConfirmation_BI_Currency1(String confirmation_BI_Currency1) {
		this.confirmation_BI_Currency1 = confirmation_BI_Currency1;
	}
	public String getConfirmation_BI_Currency2() {
		return confirmation_BI_Currency2;
	}
	public void setConfirmation_BI_Currency2(String confirmation_BI_Currency2) {
		this.confirmation_BI_Currency2 = confirmation_BI_Currency2;
	}
	public String getConfirmation_BI_Currency3() {
		return confirmation_BI_Currency3;
	}
	public void setConfirmation_BI_Currency3(String confirmation_BI_Currency3) {
		this.confirmation_BI_Currency3 = confirmation_BI_Currency3;
	}
	public String getConfirmation_BI_ActiivtyRate() {
		return confirmation_BI_ActiivtyRate;
	}
	public void setConfirmation_BI_ActiivtyRate(String confirmation_BI_ActiivtyRate) {
		this.confirmation_BI_ActiivtyRate = confirmation_BI_ActiivtyRate;
	}
	public String getConfirmation_BI_SubTotal_1() {
		return confirmation_BI_SubTotal_1;
	}
	public void setConfirmation_BI_SubTotal_1(String confirmation_BI_SubTotal_1) {
		this.confirmation_BI_SubTotal_1 = confirmation_BI_SubTotal_1;
	}
	public String getConfirmation_BI_Tax() {
		return confirmation_BI_Tax;
	}
	public void setConfirmation_BI_Tax(String confirmation_BI_Tax) {
		this.confirmation_BI_Tax = confirmation_BI_Tax;
	}
	public String getConfirmation_BI_Total() {
		return confirmation_BI_Total;
	}
	public void setConfirmation_BI_Total(String confirmation_BI_Total) {
		this.confirmation_BI_Total = confirmation_BI_Total;
	}
	public String getConfirmation_BI_SubTotal_2() {
		return confirmation_BI_SubTotal_2;
	}
	public void setConfirmation_BI_SubTotal_2(String confirmation_BI_SubTotal_2) {
		this.confirmation_BI_SubTotal_2 = confirmation_BI_SubTotal_2;
	}
	public String getConfirmation_BI_TotalTaxWTTax() {
		return confirmation_BI_TotalTaxWTTax;
	}
	public void setConfirmation_BI_TotalTaxWTTax(
			String confirmation_BI_TotalTaxWTTax) {
		this.confirmation_BI_TotalTaxWTTax = confirmation_BI_TotalTaxWTTax;
	}
	public String getConfirmation_BI_TotalBooking() {
		return confirmation_BI_TotalBooking;
	}
	public void setConfirmation_BI_TotalBooking(String confirmation_BI_TotalBooking) {
		this.confirmation_BI_TotalBooking = confirmation_BI_TotalBooking;
	}
	public String getConfirmation_BI_AmountNow() {
		return confirmation_BI_AmountNow;
	}
	public void setConfirmation_BI_AmountNow(String confirmation_BI_AmountNow) {
		this.confirmation_BI_AmountNow = confirmation_BI_AmountNow;
	}
	public String getConfirmation_BI_AmountDue() {
		return confirmation_BI_AmountDue;
	}
	public void setConfirmation_BI_AmountDue(String confirmation_BI_AmountDue) {
		this.confirmation_BI_AmountDue = confirmation_BI_AmountDue;
	}
	public String getConfirmation_FName() {
		return confirmation_FName;
	}
	public void setConfirmation_FName(String confirmation_FName) {
		this.confirmation_FName = confirmation_FName;
	}
	public String getConfirmation_LName() {
		return confirmation_LName;
	}
	public void setConfirmation_LName(String confirmation_LName) {
		this.confirmation_LName = confirmation_LName;
	}
	public String getConfirmation_TP() {
		return confirmation_TP;
	}
	public void setConfirmation_TP(String confirmation_TP) {
		this.confirmation_TP = confirmation_TP;
	}
	public String getConfirmation_Email() {
		return confirmation_Email;
	}
	public void setConfirmation_Email(String confirmation_Email) {
		this.confirmation_Email = confirmation_Email;
	}
	public String getConfirmation_address() {
		return confirmation_address;
	}
	public void setConfirmation_address(String confirmation_address) {
		this.confirmation_address = confirmation_address;
	}
	public String getConfirmation_country() {
		return confirmation_country;
	}
	public void setConfirmation_country(String confirmation_country) {
		this.confirmation_country = confirmation_country;
	}
	public String getConfirmation_city() {
		return confirmation_city;
	}
	public void setConfirmation_city(String confirmation_city) {
		this.confirmation_city = confirmation_city;
	}
	public String getConfirmation_State() {
		return confirmation_State;
	}
	public void setConfirmation_State(String confirmation_State) {
		this.confirmation_State = confirmation_State;
	}
	public String getConfirmation_postalCode() {
		return confirmation_postalCode;
	}
	public void setConfirmation_postalCode(String confirmation_postalCode) {
		this.confirmation_postalCode = confirmation_postalCode;
	}
	public String getConfirmation_AODActivityName() {
		return confirmation_AODActivityName;
	}
	public void setConfirmation_AODActivityName(String confirmation_AODActivityName) {
		this.confirmation_AODActivityName = confirmation_AODActivityName;
	}
	
	public String getConfirmationPage_cusTitle() {
		return confirmationPage_cusTitle;
	}
	public void setConfirmationPage_cusTitle(String confirmationPage_cusTitle) {
		this.confirmationPage_cusTitle = confirmationPage_cusTitle;
	}
	public String getConfirmationPage_cusFName() {
		return confirmationPage_cusFName;
	}
	public void setConfirmationPage_cusFName(String confirmationPage_cusFName) {
		this.confirmationPage_cusFName = confirmationPage_cusFName;
	}
	public String getConfirmationPage_cusLName() {
		return confirmationPage_cusLName;
	}
	public void setConfirmationPage_cusLName(String confirmationPage_cusLName) {
		this.confirmationPage_cusLName = confirmationPage_cusLName;
	}
	public String getConfirmation_CardDetails_MerchantTrackID() {
		return confirmation_CardDetails_MerchantTrackID;
	}
	public void setConfirmation_CardDetails_MerchantTrackID(
			String confirmation_CardDetails_MerchantTrackID) {
		this.confirmation_CardDetails_MerchantTrackID = confirmation_CardDetails_MerchantTrackID;
	}
	public String getConfirmation_CardDetails_AuthRef() {
		return confirmation_CardDetails_AuthRef;
	}
	public void setConfirmation_CardDetails_AuthRef(
			String confirmation_CardDetails_AuthRef) {
		this.confirmation_CardDetails_AuthRef = confirmation_CardDetails_AuthRef;
	}
	public String getConfirmation_CardDetails_PayId() {
		return confirmation_CardDetails_PayId;
	}
	public void setConfirmation_CardDetails_PayId(
			String confirmation_CardDetails_PayId) {
		this.confirmation_CardDetails_PayId = confirmation_CardDetails_PayId;
	}
	public String getConfirmation_CardDetails_Amount() {
		return confirmation_CardDetails_Amount;
	}
	public void setConfirmation_CardDetails_Amount(
			String confirmation_CardDetails_Amount) {
		this.confirmation_CardDetails_Amount = confirmation_CardDetails_Amount;
	}
	public String getConfirmation_Cancel_ActName() {
		return confirmation_Cancel_ActName;
	}
	public void setConfirmation_Cancel_ActName(String confirmation_Cancel_ActName) {
		this.confirmation_Cancel_ActName = confirmation_Cancel_ActName;
	}
	public String getConfirmation_Cancel_CancellationDead_1() {
		return confirmation_Cancel_CancellationDead_1;
	}
	public void setConfirmation_Cancel_CancellationDead_1(
			String confirmation_Cancel_CancellationDead_1) {
		this.confirmation_Cancel_CancellationDead_1 = confirmation_Cancel_CancellationDead_1;
	}
	public ArrayList<String> getConfirmation_Cancel_CancelPolicy() {
		return confirmation_Cancel_CancelPolicy;
	}
	public void setConfirmation_Cancel_CancelPolicy(
			ArrayList<String> confirmation_Cancel_CancelPolicy) {
		this.confirmation_Cancel_CancelPolicy = confirmation_Cancel_CancelPolicy;
	}
	public String getConfirmation_Cancel_CancellationDead_2() {
		return confirmation_Cancel_CancellationDead_2;
	}
	public void setConfirmation_Cancel_CancellationDead_2(
			String confirmation_Cancel_CancellationDead_2) {
		this.confirmation_Cancel_CancellationDead_2 = confirmation_Cancel_CancellationDead_2;
	}
	
	
	
	
	

}
