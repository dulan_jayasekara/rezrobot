package com.rezrobot.dataobjects;

import java.util.ArrayList;
import java.util.List;


public class dmc_Setup_DataObjects_StandardInfoObject {
	
	dmc_Setup_DataObjects_PackageInfoObject packageinfo=new dmc_Setup_DataObjects_PackageInfoObject();
	dmc_Setup_DataObjects_ContactInfo contactInfo=new dmc_Setup_DataObjects_ContactInfo();
	List<dmc_Setup_DataObjects_cancellationObject> cxlList=new ArrayList<dmc_Setup_DataObjects_cancellationObject>();
	
	

	public dmc_Setup_DataObjects_PackageInfoObject getPackageinfo() {
		return packageinfo;
	}

	public void setPackageinfo(dmc_Setup_DataObjects_PackageInfoObject packageinfo) {
		this.packageinfo = packageinfo;
	}

	public dmc_Setup_DataObjects_ContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(dmc_Setup_DataObjects_ContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<dmc_Setup_DataObjects_cancellationObject> getCxlList() {
		return cxlList;
	}

	public void setCxlList(List<dmc_Setup_DataObjects_cancellationObject> cxlList) {
		this.cxlList = cxlList;
	}
	
	
	

}
